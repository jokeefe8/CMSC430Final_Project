# John O'Keefe CMSC430 Final Project README

#### Honor Statement: I, John O'Keefe, pledge on my honor that I have not given or received any unauthorized assistance on this assignment.

## I. Project Overview

## Input Language:
###### e ::= (define x e)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (define (x x ... . x) e ...+)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (letrec* ([x e] ...) e ...+)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (letrec ([x e] ...) e ...+)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (let* ([x e] ...) e ...+)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (let ([x e] ...) e ...+)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (let x ([x e] ...) e ...+)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (lambda (x ... defaultparam ...) e ...+)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (lambda x e ...+)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (lambda (x ...+ . x) e ...+)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (dynamic-wind e e e)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (guard (x cond-clause ...) e ...+)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (raise e)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (force e)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (or e ...)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (match e match-clause ...)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (cond cond-clause ...)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (case e case-clause ...)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (if e e e)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (when e e ...+)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (unless e e ...+)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (set! x e)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (begin e ...+)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (call/cc e)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (apply e e)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (e e ...)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| x
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| op
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (quasiquote qq)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| (quote dat)
###### &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| nat | string | #t | #f

###### cond-clause ::= (e) | (e e e ...) | (else e e ...)
###### case-clause ::= ((dat ...) e e ...) | (else e e ...)
###### match-clause ::= (pat e e ...) | (else e e ...)
###### dat is a datum satisfying datum? from utils.rkt
###### x is a variable (satisfies symbol?)
###### defaultparam ::= (x e)
###### op is a symbol satisfying prim? from utils.rkt (if not otherwise in scope)
###### op ::= promise? | null? | cons | car | + | ...  (see utils.rkt)
###### qq ::= e | dat | (unquote qq) | (unquote e) | (quasiquote qq) | (qq ...+) | (qq ...+ . qq)
###### pat ::= nat | string | #t | #f | (quote dat) | x | (? e pat) | (cons pat pat) | (quasiquote qqpat)
###### qqpat ::= e | dat | (unquote qqpat) | (unquote pat) | (quasiquote qq) | (qq ...+) | (qq ...+ . qq)
<br></br>

### A. Top-Level Interpretation
The top-level interpretation adds implicit begin forms explicitly, quotes
datums, desugars defines nested within begin forms to letrec*,
desugars quasiquote and unquote, and implements a simple pattern matcher. This
interpretation pass changes programs from a series of defines, begins, and
expressions into one letrec* statement containing bindings for all variables
and procedures defined at the top level of the program and within nested
defines.
<br></br>

### B. Desugaring
The desugaring stage takes the output language from the top-level interpretation
 and desugars the forms into a small core language including only a let form,
 the lambda-calculus, conditionals, set!, call/cc, and explicit
 primitive-operation forms. Within this pass, letrec*, letrec, let*, and let x
 are desugared into simple let bindings, exceptions are transformed into lambda
  expressions using dynamic-wind, and and promises are transformed into
 explicit promise structures. contained within desugar.rkt, the desugaring
 pass simplifies programs satisfying scheme-exp? into programs satisfying
 ir-exp? that are semantically equivalent.
<br></br>

### C. Simplifying the top-level expression
After desugaring the top-level expression, my compiler then passes the desugared output to a function named simplify-ir that  removes list, vector->apply, vector, map foldl, foldr, drop, memv, >, >=, and many other primitives from the input language. This reduction from separate primitives to predefined lambda expressions simplifies the handling of primitives within later passes of the compiler and allows more complex expressions to make it through separate assignment passes.
<br></br>

### D. Assignment Conversion
The assignment conversion phase removes set! from the language by boxing all
mutable local variables (using make-vector, vector-set!, and vector-ref prims).
These produced vectors are allocated with a size of 1 to store the previously
mutation dependant variable.
<br></br>

### E. Alphatizing
The alphatize function removes all instances of variable shadowing from the
assignment converted expression by making variable names unique throughout
the program. This is done by creating an environment that contains all
variables that have been declared elsewhere through the program and allocates
new variable names when any instances of shadowing are discovered. Since this
 particular pass only makes all names unique to a single binding point, it
 does not alter the grammar output from the assignment conversion stage.
<br></br>

### F. ANF Conversion
ANF conversion gives an explicit order of evaluation to evaluation of any
subexpressions by administratively let-binding them to a temporary name. Let
forms with multiple right-hand sides are also flattened into multiple let
forms. This partitions the grammar into atomic expressions (ae), which may be
 evaluated immediately (datums, variables, and lambdas), and complex
 expressions (e), which are not known trivially to terminate. After this phase,
only if and let contain multiple true subexpressions. other forms such as
primitive operations and applications now contain only atomic sub-expressions.
<br></br>

### G. CPS Conversion
The CPS conversion stage converts the program expression in Administrative
Normal Form to continuation-passing style. This means continuations are
explicitly passed and no function call ever returns, instead the current
continuation is invoked at return points. After the CPS Conversion stage all
expressions are let bound except for apply, function application, and if
statements.  

### H. Closure Conversion
This phase removes all lambda abstractions and replaces them with make-closure
and env-ref forms. Remaining atomic expressions other than variable references
are lifted to their own let bindings. All function calls are redefined to be
non-variadic using lambdas whose parameter is a list containing all of the
previous parameters. The output of Closure conversion is in the proc-exp?
language: a list of first-order procedures that can be turned into LLVM
functions directly. The outermost program term is encoded in a main procedure
(proc (main) ...)
<br></br>

### I. Parsing into LLVM
The final pass of the compiler converts the first-order procedural language
output from the Closure Conversion stage into LLVM IR that can be combined with
a runtime written in C to produce the program binary. This is done by folding
over the list of procedures created in the Closure Conversion stage and
creating LLVM methods for each procedure, using pre-defined functions and
macros contained within the header.cpp file to flesh out the syntax of
variable declaration and function application. The program handles strings
and symbols specially by predefining space outside of the containing procedures
allocating (string length + 1) bytes for each declared string. Specially
handled the main procedure is transformed into a main method that returns an
integer status code and a proc-main method that has no return value. The output
from this method can be run directly as a binary encoding producing the same
value as providing the original program input to the eval-ir method.
<br></br>

### Output language
The output language of the compiler is a LLVM string that is attached to the
compiled header.ll file to form a valid llvm string. The compiler makes sure
the runtime, header.cpp, is compiled to a file header.ll before it concatenates
the compiler produced llvm code onto this compiled LLVM IR and saves the result
 to combined.ll. This file is then compiled and run using clang++ combined.ll
 -o filename; ./filename.
<br></br>

### Example Output
All public tests as well as the created run-time tests work successfully on my machine. Two of the runtime tests involving uninitilized variables return Error: instead of (void) but this behavior is acceptable within the compiler.

Make tests<br></br>
`racket tests.rkt all`<br></br>

`./fib-cont`<br></br>
`233`<br></br>

`./guard-continuations`<br></br>
`3317863`<br></br>

`./exceptions-continuations`<br></br>
`26`<br></br>

# II. Supported Primative Operations

### (Initial Type definitions for primative methods)
###### v := (datum? => true) (i.e lst | datum | pair) (dynamic type that evaluates to a value)
###### e := procedure | lambda expression | v (dynamic type)
###### proc := procedure | lambda expression. (dynamic type)
###### pair := (cons? => #t) (fixed type)
###### lst := (cons? => #t) (fixed type)
###### hash := (hash? => #t) (fixed type)
###### key := v (fixed type)

<br></br>
## A. ) Basic Arithmetic and Comaprison Operations
#### a. (+ vs ...)
- \+ or the addition operator takes 0 or more arguments and adds them together.
- 0 arguments - returns 0.
- 1 argument - returns the value of the single argument.
- 2 or more arguments - returns the value of the provided arguments added
together.

#### b. (= v1 v2 ...)
- = requires 2 or more arguments. = returns true if all provided arguments are
equal to the same value and false otherwise.

#### c. (> v1 v2 ...)
- The greater than operator \> takes 2 or more arguments and returns #t if the arguments in the given
order are strictly decreasing, #f otherwise.

#### d. (< v1 v2 ...)
- The less than operator \< takes 2 or more arguments. \< returns #t if the arguments in the given
order are strictly increasing, #f otherwise.

#### e. (<= v1 v2 ...)
- The less than or equal to operator \<\= takes 2 or more arguments. \<\= returns #t if the arguments in the given
order are strictly non-decreasing, #f otherwise.

#### f. (>= v1 v2 ...)
- The greater than or equal to operator \>\= takes 2 or more arguments. \>\= returns #t if the arguments in the given
order are strictly non-increasing, #f otherwise.

#### g. (- v vs ...)
- \- or the subtraction operator takes 1 or more arguments and subtracts all
following arguments from the first argument.
- 1 argument: returns the value of (\- 0 v)
- 2 or more arguments: subtraction of vs from v pairwise from left to right.  

#### h. (* vs ...)
- The multiplication operator takes 0 or more arguments and produces
the pairwise product of all vs.
- 0 arguments: returns 1
- 1 argument: returns value of v
- 2 or more arguments: returns product of all vs evaluating from left to right.

#### i. (/ v vs ...)
- / or the division operator takes 1 or more arguments and divides all
following arguments from the first argument.
- 1 argument: returns (/ 1 v)
- 2 or more arguments: returns the division of v by the vs from left to right.

<br></br>
## B.) List / Pair Operations
#### a. (cons? v)
- The cons? function takes one argument e and returns #t if e is a pair, #f
 otherwise.

#### b. (null? v)
- The null? function takes one argument e and returns #t if e is the empty
 list, #f otherwise.

#### c. (cons e1 e2)
- The cons function takes two arguments e1 and e2 and returns a newly allocated
pair whose first element is e1 and the second element is e2.

#### d. (car pair) : (pair : pair?)
- The car function takes one argument pair and returns the first element of the
specified pair. If (pair? pair => #f) then car will throw a contract violation
error.

#### e. (cdr pair) : (pair : pair?)
- The cdr function takes one argument pair and returns the second element of
the specified pair. If (pair? pair => #f) then cdr will throw a contract
violation error.

#### f. (list es ...)
- The list method takes 0 or more arguments and returns a newly allocated List
containing the es as its arguments.
- 0 arguments: returns the empty list mimicking the (null) methods
- 1 or more arguments: returns list enclosing provided arguments.

#### g. (length lst) : (lst : pair?)
- The length method takes one argument that satisfies the pair? predicate and
returns the length of the specified list. If (pair? lst => #f) then a contract
violation error is thrown.

#### h. (member e lst) : (lst : pair?)
- The member method takes 2 arguments, a lst that satisfies the pair? predicate
and a value e to be searched for within the list lst. Member locates the first
 element of lst that is equal? to v. If such an element exists, the tail of
 lst starting with that element is returned. Otherwise, the result is #f.

#### i. (map proc lst ...) : (each lst : pair?)
- The map funciton applies proc to the elements of the lists from the first  
element to the last. The proc argument accepts the same number of arguments
as the number of supplied lists and all lists must have the same number of
elements, otherwise a contract violation error is thrown. The result is a
list containing each result of proc in order.

#### j. (foldl proc acc lst ...) (each lst : pair?)
-  foldl applies a procedure to the elements of one or more lists. foldl
combines the return values in a way determined by the procedure proc. If foldl
is called with n lists, then proc takes n+1 arguments including the accumulator
 acc. The procedure is initially invoked with the first item of each list, and
the final argument is init. In foldl the lists are traversed from left to
right. If the lists do not satisfy pair? a contract violation error is thrown.

#### k. (foldr proc acc lst ...)
-  foldl applies a procedure to the elements of one or more lists. foldl
combines the return values in a way determined by the procedure proc. If foldl
is called with n lists, then proc takes n+1 arguments including the accumulator
 acc. The procedure is initially invoked with the first item of each list, and
the final argument is init. In foldl the lists are traversed from right to
left. If the lists do not satisfy pair? a contract violation error is thrown.

## C.) HashSet methods
#### a. (make-hash [assocs]) : (assocs : (listof pair?) | NULL)
- The make-hash procedure creates a hash table where the keys are compared with the equal? method. The hash created using this method is mutable and is initialized with the key-value combinations contained within the assocs list. If the assocs list is null, then an empty hash is created. For this project, make-hash uses the uthash c library to encode hash tables as a my_hash struct with a u64 key, u64 value, and a marker field used to distinguish elements of the hash.

#### b. (hash-ref hash key)
- The hash-ref procedure looks up the key value "key" in the hash-table "hash". If the hash-table "hash" does not contain the key "key", then a reference to NULL is returned. If the hash-table "hash" is not a hash-table, then a run-time error or compilation error is thrown. Otherwise, the value mapped to the key "key" in the hash-table "hash" is returned.

#### c. (hash-set! hash key val)
- hash-set! takes a hash "hash", u64 key, and u64 value and updates the mapping for key "key" with the value "val" within the hash "hash." If the key "key" is not contained within the hash "hash," then a new mapping from key to val in the hash "hash" is created. If hash is not a hashmap, then a runtime / compilation contract violation error is thrown.

## D.) Miscellaneous
#### a. (void? e)
- Operates on one argument. void? returns #t if v is the empty list, #f
otherwise.

#### b. (promise? e)
- Operates on one argument. void? returns #t if v is a promise, #f otherwise.
In this compiler, a promise is represented as a size 3 list consisting of
('promise (expression bound to promise by delay) and a size 2 vector consisting
of (0 -> has been forced? 1 -> value of evaluated expression)).

#### c. (number? v)
- Operates on one argument. void? returns #t if v is a number, #f otherwise.

#### d. (display v [out])
- The display function takes one argument which is the output to be printed
out to the default printer in display mode. This output is written such that
byte or character based datatypes are written in their raw format.

# III. Documentation of Runtime Fixes
## Testing
All tests for functionality are contained within the ./tests/public directory.
All tests for the runtime errors are contained within the ./tests/release directory.
All of the tests for HashMap functionality are contained within the ./tests/secret directory.

The runtime error handling tests include
- div-by-0-1.scm
- div-by-0-2.scm
- int-overflow-test.scm
- int-overflow1.scm
- int-overflow2.scm
- int-overflow3.scm
- int-overflow4.scm
- mem-overload1.scm
- mem-overload2.scm
- uninitilized1.scm
- uninitilized1.scm
- vec-out-of-bounds1.scm
- vec-out-of-bounds2.scm

All new tests that expect runtime failure must be added to the ./tests/release directory to make sure that the compilation process does not catch compilation errors


## A. ) Vector references out of bounds
Within header.cpp, it is easy to check whether references to vector fields are valid or
not. This is done by utilizing the first field within the vectors themselves. Since the
first field in any vector according to header.cpp is the length of the vector with a
vector tag appended to the end. To make sure that vector references are in bounds, every
vector reference is checked to make sure that the number referenced or set is less than
or equal to the given length of the vector. If the index provided is out of bounds then
an error "Error: vector-ref attempted to access element outside of vector bounds." is
returned.

Tests:
- vec-out-of-bounds1.scm: tests for index greater than size with vector ref
- vec-out-of-bounds2.scm: tests for index greater than size made with make-vector

Example execution:<br></br>
`racket tests.rkt vec-out-of-bounds1`<br></br>
`./vec-out-of-bounds1`<br></br>
`library run-time error: Error: vector-ref attempted to access element outside of vector bounds.`

## B. ) Uninitialized variable references
Within the alphatize function of cps.rkt, whenever symbols are referenced from the hash
environment, an extra check is implemented to see if the symbol is defined within the
environment. If the symbol does not exist, then an error "Error: Symbol
"(symbol->string x) " is not defined within current context. Uninitialized variable!"
is returned.  

Tests:
- uninitilized1.scm: Simple example of addition with undeclared variable.
- uninitilized2.scm: Undeclared variable at the end of begin statement.

Example execution:<br></br>
`racket tests.rkt uninitialized2`<br></br>
`./uninitialized2`<br></br>
`'Error: Symbol c is not defined within current context. Uninitialized variable!`

## C. ) Memory Overloading above 256MB
Within header.cpp, my compiler checks memory limits through the use of a bytes counter
called MEMCOUNT. MEMCOUNT is initialized to zero before any llvm code is evaluated and
every time that a call to alloc to allocate new memory within the compiler is made, the
counter is incremented by the amount of memory allocated. After the counter is
incremented, then the total allocated memory is checked with the program limit of 256MB
to see if the memory limit is surpassed, if the limit is surpassed, then an error is
thrown containing "Error: Program exceeded memory cap of 256 MB!", else continue
execution.  

Tests:
- mem-overload1.scm: Runs fibonacci(50) on a non-tail-recursive fibonacci function.
- mem-overload2.scm: Runs factorial(100000000000000000)  on a non-tail-recursive factorial function.

Example execution:<br></br>
`racket tests.rkt mem-overload1`<br></br>
`./mem-overload1`<br></br>
`library run-time error: Error: Program exceeded memory cap of 256 MB!`

## D. ) Divide by Zero Exceptions
Within header.cpp, my compiler checks divide-by-0 errors by performing a check within the
prim__47 method to see if the divisor of the division function is equal to zero. If the
divisor is equal to zero, then an error titled "Tried to divide by zero, division by
zero error!" is thrown, otherwise division continues as planned.

Tests:
- div-by-0-1.scm: Division with denominator of bound variable to zero.
- div-by-0-2.scm: Division over multiple args with function evaluation returning 0.

Example execution:<br></br>
`racket tests.rkt div-by-0-1`<br></br>
`./div-by-0-1`<br></br>
`library run-time error: Tried to divide by zero, division by zero error!`

## E. ) Integer Overflow
In the context of my compiler MAX_INT is defined as 2147483647 and min int is defined as -2147483648. Integer overflow occurs when a resulting product of primitive arithmetic operations would be either greater than MAX_INT or less than MIN_INT. Normally integer overflows cause integer values to wrap around which generally is not too much of an issue unless dealing with extremely sensitive values like memory allocation. An integer overflow in bytes allocated to a particular struct can result in a buffer overflow. For this reason it is very important to address Integer overflow vulnerabilities within the compiler.

Within header.cpp integer overflow is handled within all of the primitive operations.
Within the plus and apply plus methods, the computed sum is checked to determine if the
sum of two positive numbers is less than either argument and the sum of 2 negative
numbers is greater than either number. For subtraction, the compiler checks to see
subtraction of two positive numbers is greater than either argument and two negative
numbers is less than either number. For multiplication, the compiler checks to see if
the correct signs of multiplication are preserved. For division, we check to see if MIN
integer is divided by -1 to produce a number that isn't able to be represented as a
positive number. If any of these properties aren't preserved then an error is thrown
containing "Error: potential integer overflow error!".

Tests:
- int-overflow1.scm: Checks for multiple overflow conditions concerning integer addition.
- int-overflow2.scm: Checks for overflow concerning integer multiplication.
- int-overflow3.scm: Checks for integer underflow concerning integer subtraction.
- int-overflow4.scm: Checks for overflow concerning integer division.
- int-overflow-test.scm: Contains checks for each of the 4 primitive arithmetic operations.

Example execution:<br></br>
`racket tests.rkt int-overflow-test`<br></br>
`./int-overflow-test`<br></br>
`library run-time error: Error: potential integer overflow of C integer type through subtraction.`

## G. ) Errors still not being handled
The main class of run-time errors still not being handled by my compiler is run-time
errors involving the application of functions. Currently my compiler does not check if
functions are provided the incorrect number of arguments and if a non-function value
is applied. That being said, during evaluation, if a function is provided too few or too
many arguments, then the program will generally not necessarily return correctly depending
upon the context of the function application.

# IV. Documentation of HashSet Implementation
For this project, HashMaps are implemented using the uthash.h header file. Documentation for uthash methods can be located [here](http://troydhanson.github.io/uthash/) and the associated source code can be found [here](https://github.com/troydhanson/uthash). The methods implemented for uthash are make-hash, hash-ref, and hash-set!. The required parameters and functionality of these methods is explained in the HashMap sections of the Supported Primitive Operations portion of the README.

struct my_hash {
<br></br>  &nbsp;&nbsp;&nbsp;&nbsp;u64 key;
<br></br>  &nbsp;&nbsp;&nbsp;&nbsp;u64 val;
<br></br>  &nbsp;&nbsp;&nbsp;&nbsp;UT_hash_handle hh; // name of hash identifier field
<br></br>  };

This struct represents my compiler's encoding of a hash object. When initializing a new hash object, create a my_hash pointer and set it to null, then call HASH_ADD(hh, head, key, sizeof(u64), new hash object).

### Used macros
- HASH_FIND(hash identifier, hash head, memory address of key to find, sizeof(u64), result): searches for key within hash and stores the resulting hash object or NULL if not found in result.
- HASH_ADD(hash identifier, head of hash, key field, sizeof(u64), new hash object to be added): adds hash object to the head of hash, key must not be already located within the hash.

### make-hash hash-ref and hash-set!
make-hash verifies the argument list and then creates a vector to contain the HASH_OTHERTAG and the encoded hash object. Then a buffer is created to read in the key-value pairs from the argument list and create a counter of how many hash elements should be added. Then a new hash object is made for each key-value pair and is added to the hash. If a key is redeclared, then the specific hash element os updated. Once all key-value pairs have evaluated, then a vector containing the HASH_OTHERTAG and the hash object linked to all the mapped keys is encoded using ENCODE_OTHER and is returned. hash-ref and hash-set! function much like the racket hash-ref and hash-set! methods.

### Attempting to perform Hash methods on non-hash objects
Within header.cpp within hash method implementations, hash objects are created as a size 2 vector consisting of the hash object itself and an associated HASH_OTHERTAG. When any method performing hash operations is called, then the provided hash object parameter is decoded and checked to see if the 1st field is equal to the HASH_OTHERTAG. If the decoded tags are not equal, then llvm is attempting to perform hash operations on a non-hashable object. If this is the case then an error "Error: (hash-ref or hash-set!) not given a valid hash as first parameter!" is returned.

### Issues:
- One important compiler specific aspect of these HashMaps is that the keys and values of these HashMaps can only be u64 integer values. I ran out of time trying to add support for different data types other than integers.

### Tests
All tests for the HashMap implementation are contained within ./tests/secret
- hash-ref.scm
- hash-set.scm
- make-hash.scm

Example execution:<br></br>
`racket tests.rkt hash-ref`<br></br>
`./hash-ref`<br></br>
`343`

`racket tests.rkt hash-set`<br></br>
`./hash-set`<br></br>
`-3`

`racket tests.rkt make-hash`<br></br>
`./make-hash`<br></br>
`1036`
