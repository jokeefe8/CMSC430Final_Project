; ModuleID = 'header.cpp'
source_filename = "header.cpp"
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.13.0"

%struct.my_hash = type { i64, i64, %struct.UT_hash_handle }
%struct.UT_hash_handle = type { %struct.UT_hash_table*, i8*, i8*, %struct.UT_hash_handle*, %struct.UT_hash_handle*, i8*, i32, i32 }
%struct.UT_hash_table = type { %struct.UT_hash_bucket*, i32, i32, i32, %struct.UT_hash_handle*, i64, i32, i32, i32, i32, i32 }
%struct.UT_hash_bucket = type { %struct.UT_hash_handle*, i32, i32 }

@MEMLIMIT = global i64 268435456, align 8
@MEMCOUNT = global i64 0, align 8
@.str = private unnamed_addr constant [25 x i8] c"library run-time error: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%s\00", align 1
@.str.2 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.3 = private unnamed_addr constant [46 x i8] c"Error: Program exceeded memory cap of 256 MB!\00", align 1
@.str.4 = private unnamed_addr constant [6 x i8] c"%llu\0A\00", align 1
@.str.5 = private unnamed_addr constant [68 x i8] c"Expected value: null (in expect_args0). Prim cannot take arguments.\00", align 1
@.str.6 = private unnamed_addr constant [79 x i8] c"Expected cons value (in expect_args1). Prim applied on an empty argument list.\00", align 1
@.str.7 = private unnamed_addr constant [70 x i8] c"Expected null value (in expect_args1). Prim can only take 1 argument.\00", align 1
@.str.8 = private unnamed_addr constant [37 x i8] c"Expected a cons value. (expect_cons)\00", align 1
@.str.9 = private unnamed_addr constant [51 x i8] c"Expected a vector or special value. (expect_other)\00", align 1
@.str.10 = private unnamed_addr constant [3 x i8] c"()\00", align 1
@.str.11 = private unnamed_addr constant [13 x i8] c"#<procedure>\00", align 1
@.str.12 = private unnamed_addr constant [2 x i8] c"(\00", align 1
@.str.13 = private unnamed_addr constant [4 x i8] c" . \00", align 1
@.str.14 = private unnamed_addr constant [2 x i8] c")\00", align 1
@.str.15 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.16 = private unnamed_addr constant [5 x i8] c"\22%s\22\00", align 1
@.str.17 = private unnamed_addr constant [3 x i8] c"#(\00", align 1
@.str.18 = private unnamed_addr constant [2 x i8] c",\00", align 1
@.str.19 = private unnamed_addr constant [7 x i8] c"#hash(\00", align 1
@.str.20 = private unnamed_addr constant [10 x i8] c"(%d . %d)\00", align 1
@.str.21 = private unnamed_addr constant [11 x i8] c" (%d . %d)\00", align 1
@.str.22 = private unnamed_addr constant [37 x i8] c"(print.. v); unrecognized value %llu\00", align 1
@.str.23 = private unnamed_addr constant [4 x i8] c"'()\00", align 1
@.str.24 = private unnamed_addr constant [3 x i8] c"'(\00", align 1
@.str.25 = private unnamed_addr constant [4 x i8] c"'%s\00", align 1
@.str.26 = private unnamed_addr constant [35 x i8] c"(print v); unrecognized value %llu\00", align 1
@.str.27 = private unnamed_addr constant [49 x i8] c"first argument to make-vector must be an integer\00", align 1
@.str.28 = private unnamed_addr constant [39 x i8] c"prim applied on more than 2 arguments.\00", align 1
@.str.29 = private unnamed_addr constant [49 x i8] c"second argument to vector-ref must be an integer\00", align 1
@.str.30 = private unnamed_addr constant [46 x i8] c"first argument to vector-ref must be a vector\00", align 1
@.str.31 = private unnamed_addr constant [46 x i8] c"vector-ref not given a properly formed vector\00", align 1
@.str.32 = private unnamed_addr constant [72 x i8] c"Error: vector-ref attempted to access element outside of vector bounds.\00", align 1
@.str.33 = private unnamed_addr constant [49 x i8] c"second argument to vector-set must be an integer\00", align 1
@.str.34 = private unnamed_addr constant [48 x i8] c"first argument to vector-set must be an integer\00", align 1
@.str.35 = private unnamed_addr constant [46 x i8] c"vector-set not given a properly formed vector\00", align 1
@.str.36 = private unnamed_addr constant [72 x i8] c"Error: vector-set attempted to access element outside of vector bounds.\00", align 1
@.str.37 = private unnamed_addr constant [34 x i8] c"(prim + a b); a is not an integer\00", align 1
@.str.38 = private unnamed_addr constant [34 x i8] c"(prim + a b); b is not an integer\00", align 1
@.str.39 = private unnamed_addr constant [71 x i8] c"Error: potential integer overflow of C integer type through addition. \00", align 1
@.str.40 = private unnamed_addr constant [36 x i8] c"Tried to apply + on non list value.\00", align 1
@.str.41 = private unnamed_addr constant [34 x i8] c"(prim - a b); a is not an integer\00", align 1
@.str.42 = private unnamed_addr constant [34 x i8] c"(prim - a b); b is not an integer\00", align 1
@.str.43 = private unnamed_addr constant [74 x i8] c"Error: potential integer overflow of C integer type through subtraction. \00", align 1
@.str.44 = private unnamed_addr constant [36 x i8] c"Tried to apply - on non list value.\00", align 1
@.str.45 = private unnamed_addr constant [34 x i8] c"(prim * a b); a is not an integer\00", align 1
@.str.46 = private unnamed_addr constant [34 x i8] c"(prim * a b); b is not an integer\00", align 1
@.str.47 = private unnamed_addr constant [77 x i8] c"Error: potential integer overflow of C integer type through multiplication. \00", align 1
@.str.48 = private unnamed_addr constant [36 x i8] c"Tried to apply * on non list value.\00", align 1
@.str.49 = private unnamed_addr constant [34 x i8] c"(prim / a b); a is not an integer\00", align 1
@.str.50 = private unnamed_addr constant [34 x i8] c"(prim / a b); b is not an integer\00", align 1
@.str.51 = private unnamed_addr constant [49 x i8] c"Tried to divide by zero, division by zero error!\00", align 1
@.str.52 = private unnamed_addr constant [71 x i8] c"Error: potential integer overflow of C integer type through division. \00", align 1
@.str.53 = private unnamed_addr constant [34 x i8] c"(prim = a b); a is not an integer\00", align 1
@.str.54 = private unnamed_addr constant [34 x i8] c"(prim = a b); b is not an integer\00", align 1
@.str.55 = private unnamed_addr constant [34 x i8] c"(prim < a b); a is not an integer\00", align 1
@.str.56 = private unnamed_addr constant [34 x i8] c"(prim < a b); b is not an integer\00", align 1
@.str.57 = private unnamed_addr constant [35 x i8] c"(prim <= a b); a is not an integer\00", align 1
@.str.58 = private unnamed_addr constant [35 x i8] c"(prim <= a b); b is not an integer\00", align 1
@.str.59 = private unnamed_addr constant [47 x i8] c"Optional argument to make-hash must be a list.\00", align 1
@.str.60 = private unnamed_addr constant [59 x i8] c"Error: hash-ref not given a valid hash as first parameter!\00", align 1
@.str.61 = private unnamed_addr constant [60 x i8] c"Error: hash-set! not given a valid hash as first parameter!\00", align 1

; Function Attrs: noinline ssp uwtable
define void @fatal_err(i8*) #0 {
  %2 = alloca i8*, align 8
  store i8* %0, i8** %2, align 8
  %3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i32 0, i32 0))
  %4 = load i8*, i8** %2, align 8
  %5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i8* %4)
  %6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.2, i32 0, i32 0))
  call void @exit(i32 1) #7
  unreachable
                                                  ; No predecessors!
  ret void
}

declare i32 @printf(i8*, ...) #1

; Function Attrs: noreturn
declare void @exit(i32) #2

; Function Attrs: noinline ssp uwtable
define i64* @alloc(i64) #0 {
  %2 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %3 = load i64, i64* @MEMCOUNT, align 8
  %4 = load i64, i64* %2, align 8
  %5 = add i64 %3, %4
  store i64 %5, i64* @MEMCOUNT, align 8
  %6 = load i64, i64* @MEMCOUNT, align 8
  %7 = load i64, i64* @MEMLIMIT, align 8
  %8 = icmp uge i64 %6, %7
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.3, i32 0, i32 0))
  br label %10

; <label>:10:                                     ; preds = %9, %1
  %11 = load i64, i64* %2, align 8
  %12 = call i8* @malloc(i64 %11) #8
  %13 = bitcast i8* %12 to i64*
  ret i64* %13
}

; Function Attrs: allocsize(0)
declare i8* @malloc(i64) #3

; Function Attrs: noinline ssp uwtable
define void @print_u64(i64) #0 {
  %2 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %3 = load i64, i64* %2, align 8
  %4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.4, i32 0, i32 0), i64 %3)
  ret void
}

; Function Attrs: noinline ssp uwtable
define i64 @expect_args0(i64) #0 {
  %2 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %3 = load i64, i64* %2, align 8
  %4 = icmp ne i64 %3, 0
  br i1 %4, label %5, label %6

; <label>:5:                                      ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.5, i32 0, i32 0))
  br label %6

; <label>:6:                                      ; preds = %5, %1
  ret i64 0
}

; Function Attrs: noinline ssp uwtable
define i64 @expect_args1(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64*, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = and i64 %4, 7
  %6 = icmp ne i64 %5, 1
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([79 x i8], [79 x i8]* @.str.6, i32 0, i32 0))
  br label %8

; <label>:8:                                      ; preds = %7, %1
  %9 = load i64, i64* %2, align 8
  %10 = and i64 %9, -8
  %11 = inttoptr i64 %10 to i64*
  store i64* %11, i64** %3, align 8
  %12 = load i64*, i64** %3, align 8
  %13 = getelementptr inbounds i64, i64* %12, i64 1
  %14 = load i64, i64* %13, align 8
  %15 = icmp ne i64 %14, 0
  br i1 %15, label %16, label %17

; <label>:16:                                     ; preds = %8
  call void @fatal_err(i8* getelementptr inbounds ([70 x i8], [70 x i8]* @.str.7, i32 0, i32 0))
  br label %17

; <label>:17:                                     ; preds = %16, %8
  %18 = load i64*, i64** %3, align 8
  %19 = getelementptr inbounds i64, i64* %18, i64 0
  %20 = load i64, i64* %19, align 8
  ret i64 %20
}

; Function Attrs: noinline ssp uwtable
define i64 @expect_cons(i64, i64*) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64*, align 8
  %5 = alloca i64*, align 8
  store i64 %0, i64* %3, align 8
  store i64* %1, i64** %4, align 8
  %6 = load i64, i64* %3, align 8
  %7 = and i64 %6, 7
  %8 = icmp ne i64 %7, 1
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.8, i32 0, i32 0))
  br label %10

; <label>:10:                                     ; preds = %9, %2
  %11 = load i64, i64* %3, align 8
  %12 = and i64 %11, -8
  %13 = inttoptr i64 %12 to i64*
  store i64* %13, i64** %5, align 8
  %14 = load i64*, i64** %5, align 8
  %15 = getelementptr inbounds i64, i64* %14, i64 1
  %16 = load i64, i64* %15, align 8
  %17 = load i64*, i64** %4, align 8
  store i64 %16, i64* %17, align 8
  %18 = load i64*, i64** %5, align 8
  %19 = getelementptr inbounds i64, i64* %18, i64 0
  %20 = load i64, i64* %19, align 8
  ret i64 %20
}

; Function Attrs: noinline ssp uwtable
define i64 @expect_other(i64, i64*) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64*, align 8
  %5 = alloca i64*, align 8
  store i64 %0, i64* %3, align 8
  store i64* %1, i64** %4, align 8
  %6 = load i64, i64* %3, align 8
  %7 = and i64 %6, 7
  %8 = icmp ne i64 %7, 6
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.9, i32 0, i32 0))
  br label %10

; <label>:10:                                     ; preds = %9, %2
  %11 = load i64, i64* %3, align 8
  %12 = and i64 %11, -8
  %13 = inttoptr i64 %12 to i64*
  store i64* %13, i64** %5, align 8
  %14 = load i64*, i64** %5, align 8
  %15 = getelementptr inbounds i64, i64* %14, i64 1
  %16 = load i64, i64* %15, align 8
  %17 = load i64*, i64** %4, align 8
  store i64 %16, i64* %17, align 8
  %18 = load i64*, i64** %5, align 8
  %19 = getelementptr inbounds i64, i64* %18, i64 0
  %20 = load i64, i64* %19, align 8
  ret i64 %20
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @const_init_int(i64) #4 {
  %2 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %3 = load i64, i64* %2, align 8
  %4 = trunc i64 %3 to i32
  %5 = zext i32 %4 to i64
  %6 = shl i64 %5, 32
  %7 = or i64 %6, 2
  ret i64 %7
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @const_init_void() #4 {
  ret i64 39
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @const_init_null() #4 {
  ret i64 0
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @const_init_true() #4 {
  ret i64 31
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @const_init_false() #4 {
  ret i64 15
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @const_init_string(i8*) #4 {
  %2 = alloca i8*, align 8
  store i8* %0, i8** %2, align 8
  %3 = load i8*, i8** %2, align 8
  %4 = ptrtoint i8* %3 to i64
  %5 = or i64 %4, 3
  ret i64 %5
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @const_init_symbol(i8*) #4 {
  %2 = alloca i8*, align 8
  store i8* %0, i8** %2, align 8
  %3 = load i8*, i8** %2, align 8
  %4 = ptrtoint i8* %3 to i64
  %5 = or i64 %4, 4
  ret i64 %5
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_print_aux(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64*, align 8
  %4 = alloca i64*, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64, align 8
  %7 = alloca i64*, align 8
  %8 = alloca %struct.my_hash*, align 8
  %9 = alloca %struct.my_hash*, align 8
  store i64 %0, i64* %2, align 8
  %10 = load i64, i64* %2, align 8
  %11 = icmp eq i64 %10, 0
  br i1 %11, label %12, label %14

; <label>:12:                                     ; preds = %1
  %13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.10, i32 0, i32 0))
  br label %187

; <label>:14:                                     ; preds = %1
  %15 = load i64, i64* %2, align 8
  %16 = and i64 %15, 7
  %17 = icmp eq i64 %16, 0
  br i1 %17, label %18, label %20

; <label>:18:                                     ; preds = %14
  %19 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.11, i32 0, i32 0))
  br label %186

; <label>:20:                                     ; preds = %14
  %21 = load i64, i64* %2, align 8
  %22 = and i64 %21, 7
  %23 = icmp eq i64 %22, 1
  br i1 %23, label %24, label %39

; <label>:24:                                     ; preds = %20
  %25 = load i64, i64* %2, align 8
  %26 = and i64 %25, -8
  %27 = inttoptr i64 %26 to i64*
  store i64* %27, i64** %3, align 8
  %28 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.12, i32 0, i32 0))
  %29 = load i64*, i64** %3, align 8
  %30 = getelementptr inbounds i64, i64* %29, i64 0
  %31 = load i64, i64* %30, align 8
  %32 = call i64 @prim_print_aux(i64 %31)
  %33 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.13, i32 0, i32 0))
  %34 = load i64*, i64** %3, align 8
  %35 = getelementptr inbounds i64, i64* %34, i64 1
  %36 = load i64, i64* %35, align 8
  %37 = call i64 @prim_print_aux(i64 %36)
  %38 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.14, i32 0, i32 0))
  br label %185

; <label>:39:                                     ; preds = %20
  %40 = load i64, i64* %2, align 8
  %41 = and i64 %40, 7
  %42 = icmp eq i64 %41, 2
  br i1 %42, label %43, label %48

; <label>:43:                                     ; preds = %39
  %44 = load i64, i64* %2, align 8
  %45 = lshr i64 %44, 32
  %46 = trunc i64 %45 to i32
  %47 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.15, i32 0, i32 0), i32 %46)
  br label %184

; <label>:48:                                     ; preds = %39
  %49 = load i64, i64* %2, align 8
  %50 = and i64 %49, 7
  %51 = icmp eq i64 %50, 3
  br i1 %51, label %52, label %57

; <label>:52:                                     ; preds = %48
  %53 = load i64, i64* %2, align 8
  %54 = and i64 %53, -8
  %55 = inttoptr i64 %54 to i8*
  %56 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.16, i32 0, i32 0), i8* %55)
  br label %183

; <label>:57:                                     ; preds = %48
  %58 = load i64, i64* %2, align 8
  %59 = and i64 %58, 7
  %60 = icmp eq i64 %59, 4
  br i1 %60, label %61, label %66

; <label>:61:                                     ; preds = %57
  %62 = load i64, i64* %2, align 8
  %63 = and i64 %62, -8
  %64 = inttoptr i64 %63 to i8*
  %65 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i8* %64)
  br label %182

; <label>:66:                                     ; preds = %57
  %67 = load i64, i64* %2, align 8
  %68 = and i64 %67, 7
  %69 = icmp eq i64 %68, 6
  br i1 %69, label %70, label %107

; <label>:70:                                     ; preds = %66
  %71 = load i64, i64* %2, align 8
  %72 = and i64 %71, -8
  %73 = inttoptr i64 %72 to i64*
  %74 = getelementptr inbounds i64, i64* %73, i64 0
  %75 = load i64, i64* %74, align 8
  %76 = and i64 %75, 7
  %77 = icmp eq i64 1, %76
  br i1 %77, label %78, label %107

; <label>:78:                                     ; preds = %70
  %79 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.17, i32 0, i32 0))
  %80 = load i64, i64* %2, align 8
  %81 = and i64 %80, -8
  %82 = inttoptr i64 %81 to i64*
  store i64* %82, i64** %4, align 8
  %83 = load i64*, i64** %4, align 8
  %84 = getelementptr inbounds i64, i64* %83, i64 0
  %85 = load i64, i64* %84, align 8
  %86 = lshr i64 %85, 3
  store i64 %86, i64* %5, align 8
  %87 = load i64*, i64** %4, align 8
  %88 = getelementptr inbounds i64, i64* %87, i64 1
  %89 = load i64, i64* %88, align 8
  %90 = call i64 @prim_print_aux(i64 %89)
  store i64 2, i64* %6, align 8
  br label %91

; <label>:91:                                     ; preds = %102, %78
  %92 = load i64, i64* %6, align 8
  %93 = load i64, i64* %5, align 8
  %94 = icmp ule i64 %92, %93
  br i1 %94, label %95, label %105

; <label>:95:                                     ; preds = %91
  %96 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.18, i32 0, i32 0))
  %97 = load i64*, i64** %4, align 8
  %98 = load i64, i64* %6, align 8
  %99 = getelementptr inbounds i64, i64* %97, i64 %98
  %100 = load i64, i64* %99, align 8
  %101 = call i64 @prim_print_aux(i64 %100)
  br label %102

; <label>:102:                                    ; preds = %95
  %103 = load i64, i64* %6, align 8
  %104 = add i64 %103, 1
  store i64 %104, i64* %6, align 8
  br label %91

; <label>:105:                                    ; preds = %91
  %106 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.14, i32 0, i32 0))
  br label %181

; <label>:107:                                    ; preds = %70, %66
  %108 = load i64, i64* %2, align 8
  %109 = and i64 %108, 7
  %110 = icmp eq i64 %109, 6
  br i1 %110, label %111, label %177

; <label>:111:                                    ; preds = %107
  %112 = load i64, i64* %2, align 8
  %113 = and i64 %112, -8
  %114 = inttoptr i64 %113 to i64*
  %115 = getelementptr inbounds i64, i64* %114, i64 0
  %116 = load i64, i64* %115, align 8
  %117 = icmp eq i64 2, %116
  br i1 %117, label %118, label %177

; <label>:118:                                    ; preds = %111
  %119 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.19, i32 0, i32 0))
  %120 = load i64, i64* %2, align 8
  %121 = and i64 %120, -8
  %122 = inttoptr i64 %121 to i64*
  store i64* %122, i64** %7, align 8
  %123 = load i64*, i64** %7, align 8
  %124 = getelementptr inbounds i64, i64* %123, i64 1
  %125 = load i64, i64* %124, align 8
  %126 = and i64 %125, -8
  %127 = inttoptr i64 %126 to i64*
  %128 = bitcast i64* %127 to %struct.my_hash*
  store %struct.my_hash* %128, %struct.my_hash** %8, align 8
  %129 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  store %struct.my_hash* %129, %struct.my_hash** %9, align 8
  %130 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %131 = icmp ne %struct.my_hash* %130, null
  br i1 %131, label %132, label %151

; <label>:132:                                    ; preds = %118
  %133 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %134 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %133, i32 0, i32 0
  %135 = load i64, i64* %134, align 8
  %136 = and i64 %135, -8
  %137 = lshr i64 %136, 32
  %138 = trunc i64 %137 to i32
  %139 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %140 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %139, i32 0, i32 1
  %141 = load i64, i64* %140, align 8
  %142 = and i64 %141, -8
  %143 = lshr i64 %142, 32
  %144 = trunc i64 %143 to i32
  %145 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.20, i32 0, i32 0), i32 %138, i32 %144)
  %146 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %147 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %146, i32 0, i32 2
  %148 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %147, i32 0, i32 2
  %149 = load i8*, i8** %148, align 8
  %150 = bitcast i8* %149 to %struct.my_hash*
  store %struct.my_hash* %150, %struct.my_hash** %9, align 8
  br label %151

; <label>:151:                                    ; preds = %132, %118
  br label %152

; <label>:152:                                    ; preds = %169, %151
  %153 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %154 = icmp ne %struct.my_hash* %153, null
  br i1 %154, label %155, label %175

; <label>:155:                                    ; preds = %152
  %156 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %157 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %156, i32 0, i32 0
  %158 = load i64, i64* %157, align 8
  %159 = and i64 %158, -8
  %160 = lshr i64 %159, 32
  %161 = trunc i64 %160 to i32
  %162 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %163 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %162, i32 0, i32 1
  %164 = load i64, i64* %163, align 8
  %165 = and i64 %164, -8
  %166 = lshr i64 %165, 32
  %167 = trunc i64 %166 to i32
  %168 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.21, i32 0, i32 0), i32 %161, i32 %167)
  br label %169

; <label>:169:                                    ; preds = %155
  %170 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %171 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %170, i32 0, i32 2
  %172 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %171, i32 0, i32 2
  %173 = load i8*, i8** %172, align 8
  %174 = bitcast i8* %173 to %struct.my_hash*
  store %struct.my_hash* %174, %struct.my_hash** %9, align 8
  br label %152

; <label>:175:                                    ; preds = %152
  %176 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.14, i32 0, i32 0))
  br label %180

; <label>:177:                                    ; preds = %111, %107
  %178 = load i64, i64* %2, align 8
  %179 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.22, i32 0, i32 0), i64 %178)
  br label %180

; <label>:180:                                    ; preds = %177, %175
  br label %181

; <label>:181:                                    ; preds = %180, %105
  br label %182

; <label>:182:                                    ; preds = %181, %61
  br label %183

; <label>:183:                                    ; preds = %182, %52
  br label %184

; <label>:184:                                    ; preds = %183, %43
  br label %185

; <label>:185:                                    ; preds = %184, %24
  br label %186

; <label>:186:                                    ; preds = %185, %18
  br label %187

; <label>:187:                                    ; preds = %186, %12
  ret i64 39
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_print(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64*, align 8
  %4 = alloca i64*, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64, align 8
  %7 = alloca i64*, align 8
  %8 = alloca %struct.my_hash*, align 8
  %9 = alloca %struct.my_hash*, align 8
  store i64 %0, i64* %2, align 8
  %10 = load i64, i64* %2, align 8
  %11 = icmp eq i64 %10, 0
  br i1 %11, label %12, label %14

; <label>:12:                                     ; preds = %1
  %13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.23, i32 0, i32 0))
  br label %187

; <label>:14:                                     ; preds = %1
  %15 = load i64, i64* %2, align 8
  %16 = and i64 %15, 7
  %17 = icmp eq i64 %16, 0
  br i1 %17, label %18, label %20

; <label>:18:                                     ; preds = %14
  %19 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.11, i32 0, i32 0))
  br label %186

; <label>:20:                                     ; preds = %14
  %21 = load i64, i64* %2, align 8
  %22 = and i64 %21, 7
  %23 = icmp eq i64 %22, 1
  br i1 %23, label %24, label %39

; <label>:24:                                     ; preds = %20
  %25 = load i64, i64* %2, align 8
  %26 = and i64 %25, -8
  %27 = inttoptr i64 %26 to i64*
  store i64* %27, i64** %3, align 8
  %28 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.24, i32 0, i32 0))
  %29 = load i64*, i64** %3, align 8
  %30 = getelementptr inbounds i64, i64* %29, i64 0
  %31 = load i64, i64* %30, align 8
  %32 = call i64 @prim_print_aux(i64 %31)
  %33 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.13, i32 0, i32 0))
  %34 = load i64*, i64** %3, align 8
  %35 = getelementptr inbounds i64, i64* %34, i64 1
  %36 = load i64, i64* %35, align 8
  %37 = call i64 @prim_print_aux(i64 %36)
  %38 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.14, i32 0, i32 0))
  br label %185

; <label>:39:                                     ; preds = %20
  %40 = load i64, i64* %2, align 8
  %41 = and i64 %40, 7
  %42 = icmp eq i64 %41, 2
  br i1 %42, label %43, label %48

; <label>:43:                                     ; preds = %39
  %44 = load i64, i64* %2, align 8
  %45 = lshr i64 %44, 32
  %46 = trunc i64 %45 to i32
  %47 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.15, i32 0, i32 0), i32 %46)
  br label %184

; <label>:48:                                     ; preds = %39
  %49 = load i64, i64* %2, align 8
  %50 = and i64 %49, 7
  %51 = icmp eq i64 %50, 3
  br i1 %51, label %52, label %57

; <label>:52:                                     ; preds = %48
  %53 = load i64, i64* %2, align 8
  %54 = and i64 %53, -8
  %55 = inttoptr i64 %54 to i8*
  %56 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.16, i32 0, i32 0), i8* %55)
  br label %183

; <label>:57:                                     ; preds = %48
  %58 = load i64, i64* %2, align 8
  %59 = and i64 %58, 7
  %60 = icmp eq i64 %59, 4
  br i1 %60, label %61, label %66

; <label>:61:                                     ; preds = %57
  %62 = load i64, i64* %2, align 8
  %63 = and i64 %62, -8
  %64 = inttoptr i64 %63 to i8*
  %65 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.25, i32 0, i32 0), i8* %64)
  br label %182

; <label>:66:                                     ; preds = %57
  %67 = load i64, i64* %2, align 8
  %68 = and i64 %67, 7
  %69 = icmp eq i64 %68, 6
  br i1 %69, label %70, label %107

; <label>:70:                                     ; preds = %66
  %71 = load i64, i64* %2, align 8
  %72 = and i64 %71, -8
  %73 = inttoptr i64 %72 to i64*
  %74 = getelementptr inbounds i64, i64* %73, i64 0
  %75 = load i64, i64* %74, align 8
  %76 = and i64 %75, 7
  %77 = icmp eq i64 1, %76
  br i1 %77, label %78, label %107

; <label>:78:                                     ; preds = %70
  %79 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.17, i32 0, i32 0))
  %80 = load i64, i64* %2, align 8
  %81 = and i64 %80, -8
  %82 = inttoptr i64 %81 to i64*
  store i64* %82, i64** %4, align 8
  %83 = load i64*, i64** %4, align 8
  %84 = getelementptr inbounds i64, i64* %83, i64 0
  %85 = load i64, i64* %84, align 8
  %86 = lshr i64 %85, 3
  store i64 %86, i64* %5, align 8
  %87 = load i64*, i64** %4, align 8
  %88 = getelementptr inbounds i64, i64* %87, i64 1
  %89 = load i64, i64* %88, align 8
  %90 = call i64 @prim_print(i64 %89)
  store i64 2, i64* %6, align 8
  br label %91

; <label>:91:                                     ; preds = %102, %78
  %92 = load i64, i64* %6, align 8
  %93 = load i64, i64* %5, align 8
  %94 = icmp ule i64 %92, %93
  br i1 %94, label %95, label %105

; <label>:95:                                     ; preds = %91
  %96 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.18, i32 0, i32 0))
  %97 = load i64*, i64** %4, align 8
  %98 = load i64, i64* %6, align 8
  %99 = getelementptr inbounds i64, i64* %97, i64 %98
  %100 = load i64, i64* %99, align 8
  %101 = call i64 @prim_print(i64 %100)
  br label %102

; <label>:102:                                    ; preds = %95
  %103 = load i64, i64* %6, align 8
  %104 = add i64 %103, 1
  store i64 %104, i64* %6, align 8
  br label %91

; <label>:105:                                    ; preds = %91
  %106 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.14, i32 0, i32 0))
  br label %181

; <label>:107:                                    ; preds = %70, %66
  %108 = load i64, i64* %2, align 8
  %109 = and i64 %108, 7
  %110 = icmp eq i64 %109, 6
  br i1 %110, label %111, label %177

; <label>:111:                                    ; preds = %107
  %112 = load i64, i64* %2, align 8
  %113 = and i64 %112, -8
  %114 = inttoptr i64 %113 to i64*
  %115 = getelementptr inbounds i64, i64* %114, i64 0
  %116 = load i64, i64* %115, align 8
  %117 = icmp eq i64 2, %116
  br i1 %117, label %118, label %177

; <label>:118:                                    ; preds = %111
  %119 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.19, i32 0, i32 0))
  %120 = load i64, i64* %2, align 8
  %121 = and i64 %120, -8
  %122 = inttoptr i64 %121 to i64*
  store i64* %122, i64** %7, align 8
  %123 = load i64*, i64** %7, align 8
  %124 = getelementptr inbounds i64, i64* %123, i64 1
  %125 = load i64, i64* %124, align 8
  %126 = and i64 %125, -8
  %127 = inttoptr i64 %126 to i64*
  %128 = bitcast i64* %127 to %struct.my_hash*
  store %struct.my_hash* %128, %struct.my_hash** %8, align 8
  %129 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  store %struct.my_hash* %129, %struct.my_hash** %9, align 8
  %130 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %131 = icmp ne %struct.my_hash* %130, null
  br i1 %131, label %132, label %151

; <label>:132:                                    ; preds = %118
  %133 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %134 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %133, i32 0, i32 0
  %135 = load i64, i64* %134, align 8
  %136 = and i64 %135, -8
  %137 = lshr i64 %136, 32
  %138 = trunc i64 %137 to i32
  %139 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %140 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %139, i32 0, i32 1
  %141 = load i64, i64* %140, align 8
  %142 = and i64 %141, -8
  %143 = lshr i64 %142, 32
  %144 = trunc i64 %143 to i32
  %145 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.20, i32 0, i32 0), i32 %138, i32 %144)
  %146 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %147 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %146, i32 0, i32 2
  %148 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %147, i32 0, i32 2
  %149 = load i8*, i8** %148, align 8
  %150 = bitcast i8* %149 to %struct.my_hash*
  store %struct.my_hash* %150, %struct.my_hash** %9, align 8
  br label %151

; <label>:151:                                    ; preds = %132, %118
  br label %152

; <label>:152:                                    ; preds = %169, %151
  %153 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %154 = icmp ne %struct.my_hash* %153, null
  br i1 %154, label %155, label %175

; <label>:155:                                    ; preds = %152
  %156 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %157 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %156, i32 0, i32 0
  %158 = load i64, i64* %157, align 8
  %159 = and i64 %158, -8
  %160 = lshr i64 %159, 32
  %161 = trunc i64 %160 to i32
  %162 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %163 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %162, i32 0, i32 1
  %164 = load i64, i64* %163, align 8
  %165 = and i64 %164, -8
  %166 = lshr i64 %165, 32
  %167 = trunc i64 %166 to i32
  %168 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.21, i32 0, i32 0), i32 %161, i32 %167)
  br label %169

; <label>:169:                                    ; preds = %155
  %170 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %171 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %170, i32 0, i32 2
  %172 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %171, i32 0, i32 2
  %173 = load i8*, i8** %172, align 8
  %174 = bitcast i8* %173 to %struct.my_hash*
  store %struct.my_hash* %174, %struct.my_hash** %9, align 8
  br label %152

; <label>:175:                                    ; preds = %152
  %176 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.14, i32 0, i32 0))
  br label %180

; <label>:177:                                    ; preds = %111, %107
  %178 = load i64, i64* %2, align 8
  %179 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.26, i32 0, i32 0), i64 %178)
  br label %180

; <label>:180:                                    ; preds = %177, %175
  br label %181

; <label>:181:                                    ; preds = %180, %105
  br label %182

; <label>:182:                                    ; preds = %181, %61
  br label %183

; <label>:183:                                    ; preds = %182, %52
  br label %184

; <label>:184:                                    ; preds = %183, %43
  br label %185

; <label>:185:                                    ; preds = %184, %24
  br label %186

; <label>:186:                                    ; preds = %185, %18
  br label %187

; <label>:187:                                    ; preds = %186, %12
  ret i64 39
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_print(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_print(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_halt(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = call i64 @prim_print(i64 %4)
  %6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.2, i32 0, i32 0))
  call void @exit(i32 0) #7
  unreachable
                                                  ; No predecessors!
  %8 = load i64, i64* %2, align 8
  ret i64 %8
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_vector(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64*, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64*, align 8
  %6 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %7 = load i64, i64* @MEMCOUNT, align 8
  %8 = add i64 %7, 4096
  store i64 %8, i64* @MEMCOUNT, align 8
  %9 = load i64, i64* @MEMCOUNT, align 8
  %10 = load i64, i64* @MEMLIMIT, align 8
  %11 = icmp uge i64 %9, %10
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.3, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %1
  %14 = call i8* @malloc(i64 4096) #8
  %15 = bitcast i8* %14 to i64*
  store i64* %15, i64** %3, align 8
  store i64 0, i64* %4, align 8
  br label %16

; <label>:16:                                     ; preds = %25, %13
  %17 = load i64, i64* %2, align 8
  %18 = and i64 %17, 7
  %19 = icmp eq i64 %18, 1
  br i1 %19, label %20, label %23

; <label>:20:                                     ; preds = %16
  %21 = load i64, i64* %4, align 8
  %22 = icmp ult i64 %21, 512
  br label %23

; <label>:23:                                     ; preds = %20, %16
  %24 = phi i1 [ false, %16 ], [ %22, %20 ]
  br i1 %24, label %25, label %32

; <label>:25:                                     ; preds = %23
  %26 = load i64, i64* %2, align 8
  %27 = call i64 @expect_cons(i64 %26, i64* %2)
  %28 = load i64*, i64** %3, align 8
  %29 = load i64, i64* %4, align 8
  %30 = add i64 %29, 1
  store i64 %30, i64* %4, align 8
  %31 = getelementptr inbounds i64, i64* %28, i64 %29
  store i64 %27, i64* %31, align 8
  br label %16

; <label>:32:                                     ; preds = %23
  %33 = load i64, i64* %4, align 8
  %34 = add i64 %33, 1
  %35 = mul i64 %34, 8
  %36 = call i64* @alloc(i64 %35)
  store i64* %36, i64** %5, align 8
  %37 = load i64, i64* %4, align 8
  %38 = shl i64 %37, 3
  %39 = or i64 %38, 1
  %40 = load i64*, i64** %5, align 8
  %41 = getelementptr inbounds i64, i64* %40, i64 0
  store i64 %39, i64* %41, align 8
  store i64 0, i64* %6, align 8
  br label %42

; <label>:42:                                     ; preds = %55, %32
  %43 = load i64, i64* %6, align 8
  %44 = load i64, i64* %4, align 8
  %45 = icmp ult i64 %43, %44
  br i1 %45, label %46, label %58

; <label>:46:                                     ; preds = %42
  %47 = load i64*, i64** %3, align 8
  %48 = load i64, i64* %6, align 8
  %49 = getelementptr inbounds i64, i64* %47, i64 %48
  %50 = load i64, i64* %49, align 8
  %51 = load i64*, i64** %5, align 8
  %52 = load i64, i64* %6, align 8
  %53 = add i64 %52, 1
  %54 = getelementptr inbounds i64, i64* %51, i64 %53
  store i64 %50, i64* %54, align 8
  br label %55

; <label>:55:                                     ; preds = %46
  %56 = load i64, i64* %6, align 8
  %57 = add i64 %56, 1
  store i64 %57, i64* %6, align 8
  br label %42

; <label>:58:                                     ; preds = %42
  %59 = load i64*, i64** %3, align 8
  %60 = icmp eq i64* %59, null
  br i1 %60, label %63, label %61

; <label>:61:                                     ; preds = %58
  %62 = bitcast i64* %59 to i8*
  call void @_ZdaPv(i8* %62) #9
  br label %63

; <label>:63:                                     ; preds = %61, %58
  %64 = load i64, i64* @MEMCOUNT, align 8
  %65 = sub i64 %64, 4096
  store i64 %65, i64* @MEMCOUNT, align 8
  %66 = load i64*, i64** %5, align 8
  %67 = ptrtoint i64* %66 to i64
  %68 = or i64 %67, 6
  ret i64 %68
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdaPv(i8*) #5

; Function Attrs: noinline ssp uwtable
define i64 @prim_make_45vector(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64*, align 8
  %7 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  store i64 %1, i64* %4, align 8
  %8 = load i64, i64* %3, align 8
  %9 = and i64 %8, 7
  %10 = icmp ne i64 %9, 2
  br i1 %10, label %11, label %12

; <label>:11:                                     ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.27, i32 0, i32 0))
  br label %12

; <label>:12:                                     ; preds = %11, %2
  %13 = load i64, i64* %3, align 8
  %14 = and i64 %13, -8
  %15 = lshr i64 %14, 32
  %16 = trunc i64 %15 to i32
  %17 = sext i32 %16 to i64
  store i64 %17, i64* %5, align 8
  %18 = load i64, i64* %5, align 8
  %19 = add i64 %18, 1
  %20 = mul i64 %19, 8
  %21 = call i64* @alloc(i64 %20)
  store i64* %21, i64** %6, align 8
  %22 = load i64, i64* %5, align 8
  %23 = shl i64 %22, 3
  %24 = or i64 %23, 1
  %25 = load i64*, i64** %6, align 8
  %26 = getelementptr inbounds i64, i64* %25, i64 0
  store i64 %24, i64* %26, align 8
  store i64 1, i64* %7, align 8
  br label %27

; <label>:27:                                     ; preds = %36, %12
  %28 = load i64, i64* %7, align 8
  %29 = load i64, i64* %5, align 8
  %30 = icmp ule i64 %28, %29
  br i1 %30, label %31, label %39

; <label>:31:                                     ; preds = %27
  %32 = load i64, i64* %4, align 8
  %33 = load i64*, i64** %6, align 8
  %34 = load i64, i64* %7, align 8
  %35 = getelementptr inbounds i64, i64* %33, i64 %34
  store i64 %32, i64* %35, align 8
  br label %36

; <label>:36:                                     ; preds = %31
  %37 = load i64, i64* %7, align 8
  %38 = add i64 %37, 1
  store i64 %38, i64* %7, align 8
  br label %27

; <label>:39:                                     ; preds = %27
  %40 = load i64*, i64** %6, align 8
  %41 = ptrtoint i64* %40 to i64
  %42 = or i64 %41, 6
  ret i64 %42
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_make_45vector(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %6 = load i64, i64* %2, align 8
  %7 = call i64 @expect_cons(i64 %6, i64* %3)
  store i64 %7, i64* %4, align 8
  %8 = load i64, i64* %3, align 8
  %9 = call i64 @expect_cons(i64 %8, i64* %3)
  store i64 %9, i64* %5, align 8
  %10 = load i64, i64* %3, align 8
  %11 = icmp ne i64 %10, 0
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %1
  %14 = load i64, i64* %4, align 8
  %15 = load i64, i64* %5, align 8
  %16 = call i64 @prim_make_45vector(i64 %14, i64 %15)
  ret i64 %16
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_vector_45ref(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  store i64 %1, i64* %4, align 8
  %5 = load i64, i64* %4, align 8
  %6 = and i64 %5, 7
  %7 = icmp ne i64 %6, 2
  br i1 %7, label %8, label %9

; <label>:8:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.29, i32 0, i32 0))
  br label %9

; <label>:9:                                      ; preds = %8, %2
  %10 = load i64, i64* %3, align 8
  %11 = and i64 %10, 7
  %12 = icmp ne i64 %11, 6
  br i1 %12, label %13, label %14

; <label>:13:                                     ; preds = %9
  call void @fatal_err(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.30, i32 0, i32 0))
  br label %14

; <label>:14:                                     ; preds = %13, %9
  %15 = load i64, i64* %3, align 8
  %16 = and i64 %15, -8
  %17 = inttoptr i64 %16 to i64*
  %18 = getelementptr inbounds i64, i64* %17, i64 0
  %19 = load i64, i64* %18, align 8
  %20 = and i64 %19, 7
  %21 = icmp ne i64 %20, 1
  br i1 %21, label %22, label %23

; <label>:22:                                     ; preds = %14
  call void @fatal_err(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.31, i32 0, i32 0))
  br label %23

; <label>:23:                                     ; preds = %22, %14
  %24 = load i64, i64* %3, align 8
  %25 = and i64 %24, -8
  %26 = inttoptr i64 %25 to i64*
  %27 = getelementptr inbounds i64, i64* %26, i64 0
  %28 = load i64, i64* %27, align 8
  %29 = lshr i64 %28, 3
  %30 = load i64, i64* %4, align 8
  %31 = and i64 %30, -8
  %32 = lshr i64 %31, 32
  %33 = trunc i64 %32 to i32
  %34 = sext i32 %33 to i64
  %35 = icmp ult i64 %29, %34
  br i1 %35, label %36, label %45

; <label>:36:                                     ; preds = %23
  %37 = load i64, i64* %3, align 8
  %38 = and i64 %37, -8
  %39 = inttoptr i64 %38 to i64*
  %40 = getelementptr inbounds i64, i64* %39, i64 0
  %41 = load i64, i64* %40, align 8
  %42 = lshr i64 %41, 3
  %43 = icmp ugt i64 %42, 0
  br i1 %43, label %44, label %45

; <label>:44:                                     ; preds = %36
  call void @fatal_err(i8* getelementptr inbounds ([72 x i8], [72 x i8]* @.str.32, i32 0, i32 0))
  br label %45

; <label>:45:                                     ; preds = %44, %36, %23
  %46 = load i64, i64* %3, align 8
  %47 = and i64 %46, -8
  %48 = inttoptr i64 %47 to i64*
  %49 = load i64, i64* %4, align 8
  %50 = and i64 %49, -8
  %51 = lshr i64 %50, 32
  %52 = trunc i64 %51 to i32
  %53 = add nsw i32 1, %52
  %54 = sext i32 %53 to i64
  %55 = getelementptr inbounds i64, i64* %48, i64 %54
  %56 = load i64, i64* %55, align 8
  ret i64 %56
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_vector_45ref(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %6 = load i64, i64* %2, align 8
  %7 = call i64 @expect_cons(i64 %6, i64* %3)
  store i64 %7, i64* %4, align 8
  %8 = load i64, i64* %3, align 8
  %9 = call i64 @expect_cons(i64 %8, i64* %3)
  store i64 %9, i64* %5, align 8
  %10 = load i64, i64* %3, align 8
  %11 = icmp ne i64 %10, 0
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %1
  %14 = load i64, i64* %4, align 8
  %15 = load i64, i64* %5, align 8
  %16 = call i64 @prim_vector_45ref(i64 %14, i64 %15)
  ret i64 %16
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_vector_45set_33(i64, i64, i64) #0 {
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64, align 8
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  store i64 %2, i64* %6, align 8
  %7 = load i64, i64* %5, align 8
  %8 = and i64 %7, 7
  %9 = icmp ne i64 %8, 2
  br i1 %9, label %10, label %11

; <label>:10:                                     ; preds = %3
  call void @fatal_err(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.33, i32 0, i32 0))
  br label %11

; <label>:11:                                     ; preds = %10, %3
  %12 = load i64, i64* %4, align 8
  %13 = and i64 %12, 7
  %14 = icmp ne i64 %13, 6
  br i1 %14, label %15, label %16

; <label>:15:                                     ; preds = %11
  call void @fatal_err(i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.34, i32 0, i32 0))
  br label %16

; <label>:16:                                     ; preds = %15, %11
  %17 = load i64, i64* %4, align 8
  %18 = and i64 %17, -8
  %19 = inttoptr i64 %18 to i64*
  %20 = getelementptr inbounds i64, i64* %19, i64 0
  %21 = load i64, i64* %20, align 8
  %22 = and i64 %21, 7
  %23 = icmp ne i64 %22, 1
  br i1 %23, label %24, label %25

; <label>:24:                                     ; preds = %16
  call void @fatal_err(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.35, i32 0, i32 0))
  br label %25

; <label>:25:                                     ; preds = %24, %16
  %26 = load i64, i64* %4, align 8
  %27 = and i64 %26, -8
  %28 = inttoptr i64 %27 to i64*
  %29 = getelementptr inbounds i64, i64* %28, i64 0
  %30 = load i64, i64* %29, align 8
  %31 = lshr i64 %30, 3
  %32 = load i64, i64* %5, align 8
  %33 = and i64 %32, -8
  %34 = lshr i64 %33, 32
  %35 = trunc i64 %34 to i32
  %36 = sext i32 %35 to i64
  %37 = icmp ult i64 %31, %36
  br i1 %37, label %38, label %47

; <label>:38:                                     ; preds = %25
  %39 = load i64, i64* %6, align 8
  %40 = and i64 %39, -8
  %41 = inttoptr i64 %40 to i64*
  %42 = getelementptr inbounds i64, i64* %41, i64 0
  %43 = load i64, i64* %42, align 8
  %44 = lshr i64 %43, 3
  %45 = icmp ugt i64 %44, 0
  br i1 %45, label %46, label %47

; <label>:46:                                     ; preds = %38
  call void @fatal_err(i8* getelementptr inbounds ([72 x i8], [72 x i8]* @.str.36, i32 0, i32 0))
  br label %47

; <label>:47:                                     ; preds = %46, %38, %25
  %48 = load i64, i64* %6, align 8
  %49 = load i64, i64* %4, align 8
  %50 = and i64 %49, -8
  %51 = inttoptr i64 %50 to i64*
  %52 = load i64, i64* %5, align 8
  %53 = and i64 %52, -8
  %54 = lshr i64 %53, 32
  %55 = trunc i64 %54 to i32
  %56 = add nsw i32 1, %55
  %57 = sext i32 %56 to i64
  %58 = getelementptr inbounds i64, i64* %51, i64 %57
  store i64 %48, i64* %58, align 8
  ret i64 39
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_vector_45set_33(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %7 = load i64, i64* %2, align 8
  %8 = call i64 @expect_cons(i64 %7, i64* %3)
  store i64 %8, i64* %4, align 8
  %9 = load i64, i64* %3, align 8
  %10 = call i64 @expect_cons(i64 %9, i64* %3)
  store i64 %10, i64* %5, align 8
  %11 = load i64, i64* %3, align 8
  %12 = call i64 @expect_cons(i64 %11, i64* %3)
  store i64 %12, i64* %6, align 8
  %13 = load i64, i64* %3, align 8
  %14 = icmp ne i64 %13, 0
  br i1 %14, label %15, label %16

; <label>:15:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %16

; <label>:16:                                     ; preds = %15, %1
  %17 = load i64, i64* %4, align 8
  %18 = load i64, i64* %5, align 8
  %19 = load i64, i64* %6, align 8
  %20 = call i64 @prim_vector_45set_33(i64 %17, i64 %18, i64 %19)
  ret i64 %20
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_void() #4 {
  ret i64 39
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_eq_63(i64, i64) #4 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  %6 = load i64, i64* %4, align 8
  %7 = load i64, i64* %5, align 8
  %8 = icmp eq i64 %6, %7
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %2
  store i64 31, i64* %3, align 8
  br label %11

; <label>:10:                                     ; preds = %2
  store i64 15, i64* %3, align 8
  br label %11

; <label>:11:                                     ; preds = %10, %9
  %12 = load i64, i64* %3, align 8
  ret i64 %12
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_eq_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %6 = load i64, i64* %2, align 8
  %7 = call i64 @expect_cons(i64 %6, i64* %3)
  store i64 %7, i64* %4, align 8
  %8 = load i64, i64* %3, align 8
  %9 = call i64 @expect_cons(i64 %8, i64* %3)
  store i64 %9, i64* %5, align 8
  %10 = load i64, i64* %3, align 8
  %11 = icmp ne i64 %10, 0
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %1
  %14 = load i64, i64* %4, align 8
  %15 = load i64, i64* %5, align 8
  %16 = call i64 @prim_eq_63(i64 %14, i64 %15)
  ret i64 %16
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_eqv_63(i64, i64) #4 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  %6 = load i64, i64* %4, align 8
  %7 = load i64, i64* %5, align 8
  %8 = icmp eq i64 %6, %7
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %2
  store i64 31, i64* %3, align 8
  br label %11

; <label>:10:                                     ; preds = %2
  store i64 15, i64* %3, align 8
  br label %11

; <label>:11:                                     ; preds = %10, %9
  %12 = load i64, i64* %3, align 8
  ret i64 %12
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_eqv_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %6 = load i64, i64* %2, align 8
  %7 = call i64 @expect_cons(i64 %6, i64* %3)
  store i64 %7, i64* %4, align 8
  %8 = load i64, i64* %3, align 8
  %9 = call i64 @expect_cons(i64 %8, i64* %3)
  store i64 %9, i64* %5, align 8
  %10 = load i64, i64* %3, align 8
  %11 = icmp ne i64 %10, 0
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %1
  %14 = load i64, i64* %4, align 8
  %15 = load i64, i64* %5, align 8
  %16 = call i64 @prim_eqv_63(i64 %14, i64 %15)
  ret i64 %16
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_number_63(i64) #4 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = and i64 %4, 7
  %6 = icmp eq i64 %5, 2
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  store i64 31, i64* %2, align 8
  br label %9

; <label>:8:                                      ; preds = %1
  store i64 15, i64* %2, align 8
  br label %9

; <label>:9:                                      ; preds = %8, %7
  %10 = load i64, i64* %2, align 8
  ret i64 %10
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_number_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_number_63(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_integer_63(i64) #4 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = and i64 %4, 7
  %6 = icmp eq i64 %5, 2
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  store i64 31, i64* %2, align 8
  br label %9

; <label>:8:                                      ; preds = %1
  store i64 15, i64* %2, align 8
  br label %9

; <label>:9:                                      ; preds = %8, %7
  %10 = load i64, i64* %2, align 8
  ret i64 %10
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_integer_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_integer_63(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_void_63(i64) #4 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = icmp eq i64 %4, 39
  br i1 %5, label %6, label %7

; <label>:6:                                      ; preds = %1
  store i64 31, i64* %2, align 8
  br label %8

; <label>:7:                                      ; preds = %1
  store i64 15, i64* %2, align 8
  br label %8

; <label>:8:                                      ; preds = %7, %6
  %9 = load i64, i64* %2, align 8
  ret i64 %9
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_void_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_void_63(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_procedure_63(i64) #4 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = and i64 %4, 7
  %6 = icmp eq i64 %5, 0
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  store i64 31, i64* %2, align 8
  br label %9

; <label>:8:                                      ; preds = %1
  store i64 15, i64* %2, align 8
  br label %9

; <label>:9:                                      ; preds = %8, %7
  %10 = load i64, i64* %2, align 8
  ret i64 %10
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_procedure_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_procedure_63(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_null_63(i64) #4 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = icmp eq i64 %4, 0
  br i1 %5, label %6, label %7

; <label>:6:                                      ; preds = %1
  store i64 31, i64* %2, align 8
  br label %8

; <label>:7:                                      ; preds = %1
  store i64 15, i64* %2, align 8
  br label %8

; <label>:8:                                      ; preds = %7, %6
  %9 = load i64, i64* %2, align 8
  ret i64 %9
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_null_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_null_63(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_cons_63(i64) #4 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = and i64 %4, 7
  %6 = icmp eq i64 %5, 1
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  store i64 31, i64* %2, align 8
  br label %9

; <label>:8:                                      ; preds = %1
  store i64 15, i64* %2, align 8
  br label %9

; <label>:9:                                      ; preds = %8, %7
  %10 = load i64, i64* %2, align 8
  ret i64 %10
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_cons_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_cons_63(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_cons(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64*, align 8
  store i64 %0, i64* %3, align 8
  store i64 %1, i64* %4, align 8
  %6 = call i64* @alloc(i64 16)
  store i64* %6, i64** %5, align 8
  %7 = load i64, i64* %3, align 8
  %8 = load i64*, i64** %5, align 8
  %9 = getelementptr inbounds i64, i64* %8, i64 0
  store i64 %7, i64* %9, align 8
  %10 = load i64, i64* %4, align 8
  %11 = load i64*, i64** %5, align 8
  %12 = getelementptr inbounds i64, i64* %11, i64 1
  store i64 %10, i64* %12, align 8
  %13 = load i64*, i64** %5, align 8
  %14 = ptrtoint i64* %13 to i64
  %15 = or i64 %14, 1
  ret i64 %15
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_cons(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %6 = load i64, i64* %2, align 8
  %7 = call i64 @expect_cons(i64 %6, i64* %3)
  store i64 %7, i64* %4, align 8
  %8 = load i64, i64* %3, align 8
  %9 = call i64 @expect_cons(i64 %8, i64* %3)
  store i64 %9, i64* %5, align 8
  %10 = load i64, i64* %3, align 8
  %11 = icmp ne i64 %10, 0
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %1
  %14 = load i64, i64* %4, align 8
  %15 = load i64, i64* %5, align 8
  %16 = call i64 @prim_cons(i64 %14, i64 %15)
  ret i64 %16
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_car(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %5 = load i64, i64* %2, align 8
  %6 = call i64 @expect_cons(i64 %5, i64* %3)
  store i64 %6, i64* %4, align 8
  %7 = load i64, i64* %4, align 8
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_car(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_car(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_cdr(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %5 = load i64, i64* %2, align 8
  %6 = call i64 @expect_cons(i64 %5, i64* %3)
  store i64 %6, i64* %4, align 8
  %7 = load i64, i64* %3, align 8
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_cdr(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_cdr(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @prim__43(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  store i64 %1, i64* %4, align 8
  %5 = load i64, i64* %3, align 8
  %6 = and i64 %5, 7
  %7 = icmp ne i64 %6, 2
  br i1 %7, label %8, label %9

; <label>:8:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.37, i32 0, i32 0))
  br label %9

; <label>:9:                                      ; preds = %8, %2
  %10 = load i64, i64* %4, align 8
  %11 = and i64 %10, 7
  %12 = icmp ne i64 %11, 2
  br i1 %12, label %13, label %14

; <label>:13:                                     ; preds = %9
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.38, i32 0, i32 0))
  br label %14

; <label>:14:                                     ; preds = %13, %9
  %15 = load i64, i64* %3, align 8
  %16 = and i64 %15, -8
  %17 = lshr i64 %16, 32
  %18 = trunc i64 %17 to i32
  %19 = icmp sgt i32 %18, 0
  br i1 %19, label %20, label %31

; <label>:20:                                     ; preds = %14
  %21 = load i64, i64* %4, align 8
  %22 = and i64 %21, -8
  %23 = lshr i64 %22, 32
  %24 = trunc i64 %23 to i32
  %25 = load i64, i64* %3, align 8
  %26 = and i64 %25, -8
  %27 = lshr i64 %26, 32
  %28 = trunc i64 %27 to i32
  %29 = sub nsw i32 2147483647, %28
  %30 = icmp sgt i32 %24, %29
  br i1 %30, label %50, label %31

; <label>:31:                                     ; preds = %20, %14
  %32 = load i64, i64* %3, align 8
  %33 = and i64 %32, -8
  %34 = lshr i64 %33, 32
  %35 = trunc i64 %34 to i32
  %36 = icmp slt i32 %35, 0
  br i1 %36, label %37, label %51

; <label>:37:                                     ; preds = %31
  %38 = load i64, i64* %4, align 8
  %39 = and i64 %38, -8
  %40 = lshr i64 %39, 32
  %41 = trunc i64 %40 to i32
  %42 = sext i32 %41 to i64
  %43 = load i64, i64* %3, align 8
  %44 = and i64 %43, -8
  %45 = lshr i64 %44, 32
  %46 = trunc i64 %45 to i32
  %47 = sext i32 %46 to i64
  %48 = sub nsw i64 -2147483648, %47
  %49 = icmp slt i64 %42, %48
  br i1 %49, label %50, label %51

; <label>:50:                                     ; preds = %37, %20
  call void @fatal_err(i8* getelementptr inbounds ([71 x i8], [71 x i8]* @.str.39, i32 0, i32 0))
  br label %51

; <label>:51:                                     ; preds = %50, %37, %31
  %52 = load i64, i64* %3, align 8
  %53 = and i64 %52, -8
  %54 = lshr i64 %53, 32
  %55 = trunc i64 %54 to i32
  %56 = load i64, i64* %4, align 8
  %57 = and i64 %56, -8
  %58 = lshr i64 %57, 32
  %59 = trunc i64 %58 to i32
  %60 = add nsw i32 %55, %59
  %61 = zext i32 %60 to i64
  %62 = shl i64 %61, 32
  %63 = or i64 %62, 2
  ret i64 %63
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim__43(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64*, align 8
  store i64 %0, i64* %3, align 8
  %5 = load i64, i64* %3, align 8
  %6 = icmp eq i64 %5, 0
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  store i64 2, i64* %2, align 8
  br label %85

; <label>:8:                                      ; preds = %1
  %9 = load i64, i64* %3, align 8
  %10 = and i64 %9, 7
  %11 = icmp ne i64 %10, 1
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %8
  call void @fatal_err(i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.40, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %8
  %14 = load i64, i64* %3, align 8
  %15 = and i64 %14, -8
  %16 = inttoptr i64 %15 to i64*
  store i64* %16, i64** %4, align 8
  %17 = load i64*, i64** %4, align 8
  %18 = getelementptr inbounds i64, i64* %17, i64 0
  %19 = load i64, i64* %18, align 8
  %20 = and i64 %19, -8
  %21 = lshr i64 %20, 32
  %22 = trunc i64 %21 to i32
  %23 = icmp sgt i32 %22, 0
  br i1 %23, label %24, label %40

; <label>:24:                                     ; preds = %13
  %25 = load i64*, i64** %4, align 8
  %26 = getelementptr inbounds i64, i64* %25, i64 1
  %27 = load i64, i64* %26, align 8
  %28 = call i64 @applyprim__43(i64 %27)
  %29 = and i64 %28, -8
  %30 = lshr i64 %29, 32
  %31 = trunc i64 %30 to i32
  %32 = load i64*, i64** %4, align 8
  %33 = getelementptr inbounds i64, i64* %32, i64 0
  %34 = load i64, i64* %33, align 8
  %35 = and i64 %34, -8
  %36 = lshr i64 %35, 32
  %37 = trunc i64 %36 to i32
  %38 = sub nsw i32 2147483647, %37
  %39 = icmp sgt i32 %31, %38
  br i1 %39, label %66, label %40

; <label>:40:                                     ; preds = %24, %13
  %41 = load i64*, i64** %4, align 8
  %42 = getelementptr inbounds i64, i64* %41, i64 0
  %43 = load i64, i64* %42, align 8
  %44 = and i64 %43, -8
  %45 = lshr i64 %44, 32
  %46 = trunc i64 %45 to i32
  %47 = icmp slt i32 %46, 0
  br i1 %47, label %48, label %67

; <label>:48:                                     ; preds = %40
  %49 = load i64*, i64** %4, align 8
  %50 = getelementptr inbounds i64, i64* %49, i64 1
  %51 = load i64, i64* %50, align 8
  %52 = call i64 @applyprim__43(i64 %51)
  %53 = and i64 %52, -8
  %54 = lshr i64 %53, 32
  %55 = trunc i64 %54 to i32
  %56 = sext i32 %55 to i64
  %57 = load i64*, i64** %4, align 8
  %58 = getelementptr inbounds i64, i64* %57, i64 0
  %59 = load i64, i64* %58, align 8
  %60 = and i64 %59, -8
  %61 = lshr i64 %60, 32
  %62 = trunc i64 %61 to i32
  %63 = sext i32 %62 to i64
  %64 = sub nsw i64 -2147483648, %63
  %65 = icmp slt i64 %56, %64
  br i1 %65, label %66, label %67

; <label>:66:                                     ; preds = %48, %24
  call void @fatal_err(i8* getelementptr inbounds ([71 x i8], [71 x i8]* @.str.39, i32 0, i32 0))
  br label %67

; <label>:67:                                     ; preds = %66, %48, %40
  %68 = load i64*, i64** %4, align 8
  %69 = getelementptr inbounds i64, i64* %68, i64 0
  %70 = load i64, i64* %69, align 8
  %71 = and i64 %70, -8
  %72 = lshr i64 %71, 32
  %73 = trunc i64 %72 to i32
  %74 = load i64*, i64** %4, align 8
  %75 = getelementptr inbounds i64, i64* %74, i64 1
  %76 = load i64, i64* %75, align 8
  %77 = call i64 @applyprim__43(i64 %76)
  %78 = and i64 %77, -8
  %79 = lshr i64 %78, 32
  %80 = trunc i64 %79 to i32
  %81 = add nsw i32 %73, %80
  %82 = zext i32 %81 to i64
  %83 = shl i64 %82, 32
  %84 = or i64 %83, 2
  store i64 %84, i64* %2, align 8
  br label %85

; <label>:85:                                     ; preds = %67, %7
  %86 = load i64, i64* %2, align 8
  ret i64 %86
}

; Function Attrs: noinline ssp uwtable
define i64 @prim__45(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  store i64 %1, i64* %4, align 8
  %5 = load i64, i64* %3, align 8
  %6 = and i64 %5, 7
  %7 = icmp ne i64 %6, 2
  br i1 %7, label %8, label %9

; <label>:8:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.41, i32 0, i32 0))
  br label %9

; <label>:9:                                      ; preds = %8, %2
  %10 = load i64, i64* %4, align 8
  %11 = and i64 %10, 7
  %12 = icmp ne i64 %11, 2
  br i1 %12, label %13, label %14

; <label>:13:                                     ; preds = %9
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.42, i32 0, i32 0))
  br label %14

; <label>:14:                                     ; preds = %13, %9
  %15 = load i64, i64* %3, align 8
  %16 = and i64 %15, -8
  %17 = lshr i64 %16, 32
  %18 = trunc i64 %17 to i32
  %19 = icmp sgt i32 %18, 0
  br i1 %19, label %20, label %33

; <label>:20:                                     ; preds = %14
  %21 = load i64, i64* %4, align 8
  %22 = and i64 %21, -8
  %23 = lshr i64 %22, 32
  %24 = trunc i64 %23 to i32
  %25 = sext i32 %24 to i64
  %26 = load i64, i64* %3, align 8
  %27 = and i64 %26, -8
  %28 = lshr i64 %27, 32
  %29 = trunc i64 %28 to i32
  %30 = sext i32 %29 to i64
  %31 = add nsw i64 -2147483648, %30
  %32 = icmp slt i64 %25, %31
  br i1 %32, label %50, label %33

; <label>:33:                                     ; preds = %20, %14
  %34 = load i64, i64* %3, align 8
  %35 = and i64 %34, -8
  %36 = lshr i64 %35, 32
  %37 = trunc i64 %36 to i32
  %38 = icmp slt i32 %37, 0
  br i1 %38, label %39, label %51

; <label>:39:                                     ; preds = %33
  %40 = load i64, i64* %4, align 8
  %41 = and i64 %40, -8
  %42 = lshr i64 %41, 32
  %43 = trunc i64 %42 to i32
  %44 = load i64, i64* %3, align 8
  %45 = and i64 %44, -8
  %46 = lshr i64 %45, 32
  %47 = trunc i64 %46 to i32
  %48 = add nsw i32 2147483647, %47
  %49 = icmp sgt i32 %43, %48
  br i1 %49, label %50, label %51

; <label>:50:                                     ; preds = %39, %20
  call void @fatal_err(i8* getelementptr inbounds ([74 x i8], [74 x i8]* @.str.43, i32 0, i32 0))
  br label %51

; <label>:51:                                     ; preds = %50, %39, %33
  %52 = load i64, i64* %3, align 8
  %53 = and i64 %52, -8
  %54 = lshr i64 %53, 32
  %55 = trunc i64 %54 to i32
  %56 = load i64, i64* %4, align 8
  %57 = and i64 %56, -8
  %58 = lshr i64 %57, 32
  %59 = trunc i64 %58 to i32
  %60 = sub nsw i32 %55, %59
  %61 = zext i32 %60 to i64
  %62 = shl i64 %61, 32
  %63 = or i64 %62, 2
  ret i64 %63
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim__45(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64*, align 8
  store i64 %0, i64* %3, align 8
  %5 = load i64, i64* %3, align 8
  %6 = icmp eq i64 %5, 0
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  store i64 2, i64* %2, align 8
  br label %111

; <label>:8:                                      ; preds = %1
  %9 = load i64, i64* %3, align 8
  %10 = and i64 %9, 7
  %11 = icmp ne i64 %10, 1
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %8
  call void @fatal_err(i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.44, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %8
  %14 = load i64, i64* %3, align 8
  %15 = and i64 %14, -8
  %16 = inttoptr i64 %15 to i64*
  store i64* %16, i64** %4, align 8
  %17 = load i64*, i64** %4, align 8
  %18 = getelementptr inbounds i64, i64* %17, i64 1
  %19 = load i64, i64* %18, align 8
  %20 = icmp eq i64 %19, 0
  br i1 %20, label %21, label %42

; <label>:21:                                     ; preds = %13
  %22 = load i64*, i64** %4, align 8
  %23 = getelementptr inbounds i64, i64* %22, i64 0
  %24 = load i64, i64* %23, align 8
  %25 = and i64 %24, -8
  %26 = lshr i64 %25, 32
  %27 = trunc i64 %26 to i32
  %28 = sext i32 %27 to i64
  %29 = icmp eq i64 %28, -2147483648
  br i1 %29, label %30, label %31

; <label>:30:                                     ; preds = %21
  call void @fatal_err(i8* getelementptr inbounds ([74 x i8], [74 x i8]* @.str.43, i32 0, i32 0))
  br label %31

; <label>:31:                                     ; preds = %30, %21
  %32 = load i64*, i64** %4, align 8
  %33 = getelementptr inbounds i64, i64* %32, i64 0
  %34 = load i64, i64* %33, align 8
  %35 = and i64 %34, -8
  %36 = lshr i64 %35, 32
  %37 = trunc i64 %36 to i32
  %38 = sub nsw i32 0, %37
  %39 = zext i32 %38 to i64
  %40 = shl i64 %39, 32
  %41 = or i64 %40, 2
  store i64 %41, i64* %2, align 8
  br label %111

; <label>:42:                                     ; preds = %13
  %43 = load i64*, i64** %4, align 8
  %44 = getelementptr inbounds i64, i64* %43, i64 0
  %45 = load i64, i64* %44, align 8
  %46 = and i64 %45, -8
  %47 = lshr i64 %46, 32
  %48 = trunc i64 %47 to i32
  %49 = icmp sgt i32 %48, 0
  br i1 %49, label %50, label %68

; <label>:50:                                     ; preds = %42
  %51 = load i64*, i64** %4, align 8
  %52 = getelementptr inbounds i64, i64* %51, i64 1
  %53 = load i64, i64* %52, align 8
  %54 = call i64 @applyprim__43(i64 %53)
  %55 = and i64 %54, -8
  %56 = lshr i64 %55, 32
  %57 = trunc i64 %56 to i32
  %58 = sext i32 %57 to i64
  %59 = load i64*, i64** %4, align 8
  %60 = getelementptr inbounds i64, i64* %59, i64 0
  %61 = load i64, i64* %60, align 8
  %62 = and i64 %61, -8
  %63 = lshr i64 %62, 32
  %64 = trunc i64 %63 to i32
  %65 = sext i32 %64 to i64
  %66 = add nsw i64 -2147483648, %65
  %67 = icmp slt i64 %58, %66
  br i1 %67, label %92, label %68

; <label>:68:                                     ; preds = %50, %42
  %69 = load i64*, i64** %4, align 8
  %70 = getelementptr inbounds i64, i64* %69, i64 0
  %71 = load i64, i64* %70, align 8
  %72 = and i64 %71, -8
  %73 = lshr i64 %72, 32
  %74 = trunc i64 %73 to i32
  %75 = icmp slt i32 %74, 0
  br i1 %75, label %76, label %93

; <label>:76:                                     ; preds = %68
  %77 = load i64*, i64** %4, align 8
  %78 = getelementptr inbounds i64, i64* %77, i64 1
  %79 = load i64, i64* %78, align 8
  %80 = call i64 @applyprim__43(i64 %79)
  %81 = and i64 %80, -8
  %82 = lshr i64 %81, 32
  %83 = trunc i64 %82 to i32
  %84 = load i64*, i64** %4, align 8
  %85 = getelementptr inbounds i64, i64* %84, i64 0
  %86 = load i64, i64* %85, align 8
  %87 = and i64 %86, -8
  %88 = lshr i64 %87, 32
  %89 = trunc i64 %88 to i32
  %90 = add nsw i32 2147483647, %89
  %91 = icmp sgt i32 %83, %90
  br i1 %91, label %92, label %93

; <label>:92:                                     ; preds = %76, %50
  call void @fatal_err(i8* getelementptr inbounds ([74 x i8], [74 x i8]* @.str.43, i32 0, i32 0))
  br label %93

; <label>:93:                                     ; preds = %92, %76, %68
  %94 = load i64*, i64** %4, align 8
  %95 = getelementptr inbounds i64, i64* %94, i64 0
  %96 = load i64, i64* %95, align 8
  %97 = and i64 %96, -8
  %98 = lshr i64 %97, 32
  %99 = trunc i64 %98 to i32
  %100 = load i64*, i64** %4, align 8
  %101 = getelementptr inbounds i64, i64* %100, i64 1
  %102 = load i64, i64* %101, align 8
  %103 = call i64 @applyprim__43(i64 %102)
  %104 = and i64 %103, -8
  %105 = lshr i64 %104, 32
  %106 = trunc i64 %105 to i32
  %107 = sub nsw i32 %99, %106
  %108 = zext i32 %107 to i64
  %109 = shl i64 %108, 32
  %110 = or i64 %109, 2
  store i64 %110, i64* %2, align 8
  br label %111

; <label>:111:                                    ; preds = %93, %31, %7
  %112 = load i64, i64* %2, align 8
  ret i64 %112
}

; Function Attrs: noinline ssp uwtable
define i64 @prim__42(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  store i64 %1, i64* %4, align 8
  %5 = load i64, i64* %3, align 8
  %6 = and i64 %5, 7
  %7 = icmp ne i64 %6, 2
  br i1 %7, label %8, label %9

; <label>:8:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.45, i32 0, i32 0))
  br label %9

; <label>:9:                                      ; preds = %8, %2
  %10 = load i64, i64* %4, align 8
  %11 = and i64 %10, 7
  %12 = icmp ne i64 %11, 2
  br i1 %12, label %13, label %14

; <label>:13:                                     ; preds = %9
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.46, i32 0, i32 0))
  br label %14

; <label>:14:                                     ; preds = %13, %9
  %15 = load i64, i64* %4, align 8
  %16 = and i64 %15, -8
  %17 = lshr i64 %16, 32
  %18 = trunc i64 %17 to i32
  %19 = icmp ne i32 %18, 0
  br i1 %19, label %20, label %70

; <label>:20:                                     ; preds = %14
  %21 = load i64, i64* %3, align 8
  %22 = and i64 %21, -8
  %23 = lshr i64 %22, 32
  %24 = trunc i64 %23 to i32
  %25 = icmp sgt i32 %24, 0
  br i1 %25, label %26, label %37

; <label>:26:                                     ; preds = %20
  %27 = load i64, i64* %4, align 8
  %28 = and i64 %27, -8
  %29 = lshr i64 %28, 32
  %30 = trunc i64 %29 to i32
  %31 = load i64, i64* %3, align 8
  %32 = and i64 %31, -8
  %33 = lshr i64 %32, 32
  %34 = trunc i64 %33 to i32
  %35 = sdiv i32 2147483647, %34
  %36 = icmp sgt i32 %30, %35
  br i1 %36, label %69, label %37

; <label>:37:                                     ; preds = %26, %20
  %38 = load i64, i64* %3, align 8
  %39 = and i64 %38, -8
  %40 = lshr i64 %39, 32
  %41 = trunc i64 %40 to i32
  %42 = icmp slt i32 %41, -1
  br i1 %42, label %43, label %56

; <label>:43:                                     ; preds = %37
  %44 = load i64, i64* %4, align 8
  %45 = and i64 %44, -8
  %46 = lshr i64 %45, 32
  %47 = trunc i64 %46 to i32
  %48 = sext i32 %47 to i64
  %49 = load i64, i64* %3, align 8
  %50 = and i64 %49, -8
  %51 = lshr i64 %50, 32
  %52 = trunc i64 %51 to i32
  %53 = sext i32 %52 to i64
  %54 = sdiv i64 -2147483648, %53
  %55 = icmp slt i64 %48, %54
  br i1 %55, label %69, label %56

; <label>:56:                                     ; preds = %43, %37
  %57 = load i64, i64* %3, align 8
  %58 = and i64 %57, -8
  %59 = lshr i64 %58, 32
  %60 = trunc i64 %59 to i32
  %61 = icmp eq i32 %60, -1
  br i1 %61, label %62, label %70

; <label>:62:                                     ; preds = %56
  %63 = load i64, i64* %4, align 8
  %64 = and i64 %63, -8
  %65 = lshr i64 %64, 32
  %66 = trunc i64 %65 to i32
  %67 = sext i32 %66 to i64
  %68 = icmp eq i64 %67, -2147483648
  br i1 %68, label %69, label %70

; <label>:69:                                     ; preds = %62, %43, %26
  call void @fatal_err(i8* getelementptr inbounds ([77 x i8], [77 x i8]* @.str.47, i32 0, i32 0))
  br label %70

; <label>:70:                                     ; preds = %69, %62, %56, %14
  %71 = load i64, i64* %3, align 8
  %72 = and i64 %71, -8
  %73 = lshr i64 %72, 32
  %74 = trunc i64 %73 to i32
  %75 = load i64, i64* %4, align 8
  %76 = and i64 %75, -8
  %77 = lshr i64 %76, 32
  %78 = trunc i64 %77 to i32
  %79 = mul nsw i32 %74, %78
  %80 = zext i32 %79 to i64
  %81 = shl i64 %80, 32
  %82 = or i64 %81, 2
  ret i64 %82
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim__42(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64*, align 8
  store i64 %0, i64* %3, align 8
  %5 = load i64, i64* %3, align 8
  %6 = icmp eq i64 %5, 0
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  store i64 4294967298, i64* %2, align 8
  br label %112

; <label>:8:                                      ; preds = %1
  %9 = load i64, i64* %3, align 8
  %10 = and i64 %9, 7
  %11 = icmp ne i64 %10, 1
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %8
  call void @fatal_err(i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.48, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %8
  %14 = load i64, i64* %3, align 8
  %15 = and i64 %14, -8
  %16 = inttoptr i64 %15 to i64*
  store i64* %16, i64** %4, align 8
  %17 = load i64*, i64** %4, align 8
  %18 = getelementptr inbounds i64, i64* %17, i64 1
  %19 = load i64, i64* %18, align 8
  %20 = call i64 @applyprim__42(i64 %19)
  %21 = and i64 %20, -8
  %22 = lshr i64 %21, 32
  %23 = trunc i64 %22 to i32
  %24 = icmp ne i32 %23, 0
  br i1 %24, label %25, label %94

; <label>:25:                                     ; preds = %13
  %26 = load i64*, i64** %4, align 8
  %27 = getelementptr inbounds i64, i64* %26, i64 0
  %28 = load i64, i64* %27, align 8
  %29 = and i64 %28, -8
  %30 = lshr i64 %29, 32
  %31 = trunc i64 %30 to i32
  %32 = icmp sgt i32 %31, 0
  br i1 %32, label %33, label %49

; <label>:33:                                     ; preds = %25
  %34 = load i64*, i64** %4, align 8
  %35 = getelementptr inbounds i64, i64* %34, i64 1
  %36 = load i64, i64* %35, align 8
  %37 = call i64 @applyprim__42(i64 %36)
  %38 = and i64 %37, -8
  %39 = lshr i64 %38, 32
  %40 = trunc i64 %39 to i32
  %41 = load i64*, i64** %4, align 8
  %42 = getelementptr inbounds i64, i64* %41, i64 0
  %43 = load i64, i64* %42, align 8
  %44 = and i64 %43, -8
  %45 = lshr i64 %44, 32
  %46 = trunc i64 %45 to i32
  %47 = sdiv i32 2147483647, %46
  %48 = icmp sgt i32 %40, %47
  br i1 %48, label %93, label %49

; <label>:49:                                     ; preds = %33, %25
  %50 = load i64*, i64** %4, align 8
  %51 = getelementptr inbounds i64, i64* %50, i64 0
  %52 = load i64, i64* %51, align 8
  %53 = and i64 %52, -8
  %54 = lshr i64 %53, 32
  %55 = trunc i64 %54 to i32
  %56 = icmp slt i32 %55, -1
  br i1 %56, label %57, label %75

; <label>:57:                                     ; preds = %49
  %58 = load i64*, i64** %4, align 8
  %59 = getelementptr inbounds i64, i64* %58, i64 1
  %60 = load i64, i64* %59, align 8
  %61 = call i64 @applyprim__42(i64 %60)
  %62 = and i64 %61, -8
  %63 = lshr i64 %62, 32
  %64 = trunc i64 %63 to i32
  %65 = sext i32 %64 to i64
  %66 = load i64*, i64** %4, align 8
  %67 = getelementptr inbounds i64, i64* %66, i64 0
  %68 = load i64, i64* %67, align 8
  %69 = and i64 %68, -8
  %70 = lshr i64 %69, 32
  %71 = trunc i64 %70 to i32
  %72 = sext i32 %71 to i64
  %73 = sdiv i64 -2147483648, %72
  %74 = icmp slt i64 %65, %73
  br i1 %74, label %93, label %75

; <label>:75:                                     ; preds = %57, %49
  %76 = load i64*, i64** %4, align 8
  %77 = getelementptr inbounds i64, i64* %76, i64 0
  %78 = load i64, i64* %77, align 8
  %79 = and i64 %78, -8
  %80 = lshr i64 %79, 32
  %81 = trunc i64 %80 to i32
  %82 = icmp eq i32 %81, -1
  br i1 %82, label %83, label %94

; <label>:83:                                     ; preds = %75
  %84 = load i64*, i64** %4, align 8
  %85 = getelementptr inbounds i64, i64* %84, i64 1
  %86 = load i64, i64* %85, align 8
  %87 = call i64 @applyprim__42(i64 %86)
  %88 = and i64 %87, -8
  %89 = lshr i64 %88, 32
  %90 = trunc i64 %89 to i32
  %91 = sext i32 %90 to i64
  %92 = icmp eq i64 %91, -2147483648
  br i1 %92, label %93, label %94

; <label>:93:                                     ; preds = %83, %57, %33
  call void @fatal_err(i8* getelementptr inbounds ([77 x i8], [77 x i8]* @.str.47, i32 0, i32 0))
  br label %94

; <label>:94:                                     ; preds = %93, %83, %75, %13
  %95 = load i64*, i64** %4, align 8
  %96 = getelementptr inbounds i64, i64* %95, i64 0
  %97 = load i64, i64* %96, align 8
  %98 = and i64 %97, -8
  %99 = lshr i64 %98, 32
  %100 = trunc i64 %99 to i32
  %101 = load i64*, i64** %4, align 8
  %102 = getelementptr inbounds i64, i64* %101, i64 1
  %103 = load i64, i64* %102, align 8
  %104 = call i64 @applyprim__42(i64 %103)
  %105 = and i64 %104, -8
  %106 = lshr i64 %105, 32
  %107 = trunc i64 %106 to i32
  %108 = mul nsw i32 %100, %107
  %109 = zext i32 %108 to i64
  %110 = shl i64 %109, 32
  %111 = or i64 %110, 2
  store i64 %111, i64* %2, align 8
  br label %112

; <label>:112:                                    ; preds = %94, %7
  %113 = load i64, i64* %2, align 8
  ret i64 %113
}

; Function Attrs: noinline ssp uwtable
define i64 @prim__47(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  store i64 %1, i64* %4, align 8
  %5 = load i64, i64* %3, align 8
  %6 = and i64 %5, 7
  %7 = icmp ne i64 %6, 2
  br i1 %7, label %8, label %9

; <label>:8:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.49, i32 0, i32 0))
  br label %9

; <label>:9:                                      ; preds = %8, %2
  %10 = load i64, i64* %4, align 8
  %11 = and i64 %10, 7
  %12 = icmp ne i64 %11, 2
  br i1 %12, label %13, label %14

; <label>:13:                                     ; preds = %9
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.50, i32 0, i32 0))
  br label %14

; <label>:14:                                     ; preds = %13, %9
  %15 = load i64, i64* %4, align 8
  %16 = and i64 %15, -8
  %17 = lshr i64 %16, 32
  %18 = trunc i64 %17 to i32
  %19 = icmp eq i32 %18, 0
  br i1 %19, label %20, label %21

; <label>:20:                                     ; preds = %14
  call void @fatal_err(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.51, i32 0, i32 0))
  br label %21

; <label>:21:                                     ; preds = %20, %14
  %22 = load i64, i64* %3, align 8
  %23 = and i64 %22, -8
  %24 = lshr i64 %23, 32
  %25 = trunc i64 %24 to i32
  %26 = sext i32 %25 to i64
  %27 = icmp eq i64 %26, -2147483648
  br i1 %27, label %28, label %34

; <label>:28:                                     ; preds = %21
  %29 = load i64, i64* %4, align 8
  %30 = and i64 %29, -8
  %31 = lshr i64 %30, 32
  %32 = trunc i64 %31 to i32
  %33 = icmp eq i32 %32, -1
  br i1 %33, label %47, label %34

; <label>:34:                                     ; preds = %28, %21
  %35 = load i64, i64* %3, align 8
  %36 = and i64 %35, -8
  %37 = lshr i64 %36, 32
  %38 = trunc i64 %37 to i32
  %39 = icmp eq i32 %38, -1
  br i1 %39, label %40, label %48

; <label>:40:                                     ; preds = %34
  %41 = load i64, i64* %4, align 8
  %42 = and i64 %41, -8
  %43 = lshr i64 %42, 32
  %44 = trunc i64 %43 to i32
  %45 = sext i32 %44 to i64
  %46 = icmp eq i64 %45, -2147483648
  br i1 %46, label %47, label %48

; <label>:47:                                     ; preds = %40, %28
  call void @fatal_err(i8* getelementptr inbounds ([71 x i8], [71 x i8]* @.str.52, i32 0, i32 0))
  br label %48

; <label>:48:                                     ; preds = %47, %40, %34
  %49 = load i64, i64* %3, align 8
  %50 = and i64 %49, -8
  %51 = lshr i64 %50, 32
  %52 = trunc i64 %51 to i32
  %53 = load i64, i64* %4, align 8
  %54 = and i64 %53, -8
  %55 = lshr i64 %54, 32
  %56 = trunc i64 %55 to i32
  %57 = sdiv i32 %52, %56
  %58 = zext i32 %57 to i64
  %59 = shl i64 %58, 32
  %60 = or i64 %59, 2
  ret i64 %60
}

; Function Attrs: noinline ssp uwtable
define i64 @prim__61(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  %6 = load i64, i64* %4, align 8
  %7 = and i64 %6, 7
  %8 = icmp ne i64 %7, 2
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.53, i32 0, i32 0))
  br label %10

; <label>:10:                                     ; preds = %9, %2
  %11 = load i64, i64* %5, align 8
  %12 = and i64 %11, 7
  %13 = icmp ne i64 %12, 2
  br i1 %13, label %14, label %15

; <label>:14:                                     ; preds = %10
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.54, i32 0, i32 0))
  br label %15

; <label>:15:                                     ; preds = %14, %10
  %16 = load i64, i64* %4, align 8
  %17 = and i64 %16, -8
  %18 = lshr i64 %17, 32
  %19 = trunc i64 %18 to i32
  %20 = load i64, i64* %5, align 8
  %21 = and i64 %20, -8
  %22 = lshr i64 %21, 32
  %23 = trunc i64 %22 to i32
  %24 = icmp eq i32 %19, %23
  br i1 %24, label %25, label %26

; <label>:25:                                     ; preds = %15
  store i64 31, i64* %3, align 8
  br label %27

; <label>:26:                                     ; preds = %15
  store i64 15, i64* %3, align 8
  br label %27

; <label>:27:                                     ; preds = %26, %25
  %28 = load i64, i64* %3, align 8
  ret i64 %28
}

; Function Attrs: noinline ssp uwtable
define i64 @prim__60(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  %6 = load i64, i64* %4, align 8
  %7 = and i64 %6, 7
  %8 = icmp ne i64 %7, 2
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.55, i32 0, i32 0))
  br label %10

; <label>:10:                                     ; preds = %9, %2
  %11 = load i64, i64* %5, align 8
  %12 = and i64 %11, 7
  %13 = icmp ne i64 %12, 2
  br i1 %13, label %14, label %15

; <label>:14:                                     ; preds = %10
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.56, i32 0, i32 0))
  br label %15

; <label>:15:                                     ; preds = %14, %10
  %16 = load i64, i64* %4, align 8
  %17 = and i64 %16, -8
  %18 = lshr i64 %17, 32
  %19 = trunc i64 %18 to i32
  %20 = load i64, i64* %5, align 8
  %21 = and i64 %20, -8
  %22 = lshr i64 %21, 32
  %23 = trunc i64 %22 to i32
  %24 = icmp slt i32 %19, %23
  br i1 %24, label %25, label %26

; <label>:25:                                     ; preds = %15
  store i64 31, i64* %3, align 8
  br label %27

; <label>:26:                                     ; preds = %15
  store i64 15, i64* %3, align 8
  br label %27

; <label>:27:                                     ; preds = %26, %25
  %28 = load i64, i64* %3, align 8
  ret i64 %28
}

; Function Attrs: noinline ssp uwtable
define i64 @prim__60_61(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  %6 = load i64, i64* %4, align 8
  %7 = and i64 %6, 7
  %8 = icmp ne i64 %7, 2
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.57, i32 0, i32 0))
  br label %10

; <label>:10:                                     ; preds = %9, %2
  %11 = load i64, i64* %5, align 8
  %12 = and i64 %11, 7
  %13 = icmp ne i64 %12, 2
  br i1 %13, label %14, label %15

; <label>:14:                                     ; preds = %10
  call void @fatal_err(i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.58, i32 0, i32 0))
  br label %15

; <label>:15:                                     ; preds = %14, %10
  %16 = load i64, i64* %4, align 8
  %17 = and i64 %16, -8
  %18 = lshr i64 %17, 32
  %19 = trunc i64 %18 to i32
  %20 = load i64, i64* %5, align 8
  %21 = and i64 %20, -8
  %22 = lshr i64 %21, 32
  %23 = trunc i64 %22 to i32
  %24 = icmp sle i32 %19, %23
  br i1 %24, label %25, label %26

; <label>:25:                                     ; preds = %15
  store i64 31, i64* %3, align 8
  br label %27

; <label>:26:                                     ; preds = %15
  store i64 15, i64* %3, align 8
  br label %27

; <label>:27:                                     ; preds = %26, %25
  %28 = load i64, i64* %3, align 8
  ret i64 %28
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_not(i64) #4 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = icmp eq i64 %4, 15
  br i1 %5, label %6, label %7

; <label>:6:                                      ; preds = %1
  store i64 31, i64* %2, align 8
  br label %8

; <label>:7:                                      ; preds = %1
  store i64 15, i64* %2, align 8
  br label %8

; <label>:8:                                      ; preds = %7, %6
  %9 = load i64, i64* %2, align 8
  ret i64 %9
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_not(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_not(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_make_45hash(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca %struct.my_hash*, align 8
  %4 = alloca %struct.my_hash*, align 8
  %5 = alloca i64*, align 8
  %6 = alloca i64, align 8
  %7 = alloca i64*, align 8
  %8 = alloca i64, align 8
  %9 = alloca i64, align 8
  %10 = alloca i64, align 8
  %11 = alloca i32, align 4
  %12 = alloca i32, align 4
  %13 = alloca i32, align 4
  %14 = alloca i32, align 4
  %15 = alloca i8*, align 8
  %16 = alloca i32, align 4
  %17 = alloca i32, align 4
  %18 = alloca i32, align 4
  %19 = alloca i32, align 4
  %20 = alloca i32, align 4
  %21 = alloca i8*, align 8
  %22 = alloca i32, align 4
  %23 = alloca %struct.UT_hash_bucket*, align 8
  %24 = alloca i32, align 4
  %25 = alloca i32, align 4
  %26 = alloca %struct.UT_hash_handle*, align 8
  %27 = alloca %struct.UT_hash_handle*, align 8
  %28 = alloca %struct.UT_hash_bucket*, align 8
  %29 = alloca %struct.UT_hash_bucket*, align 8
  store i64 %0, i64* %2, align 8
  %30 = load i64, i64* %2, align 8
  %31 = and i64 %30, 7
  %32 = icmp ne i64 %31, 1
  br i1 %32, label %33, label %34

; <label>:33:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.59, i32 0, i32 0))
  br label %34

; <label>:34:                                     ; preds = %33, %1
  store %struct.my_hash* null, %struct.my_hash** %3, align 8
  store %struct.my_hash* null, %struct.my_hash** %4, align 8
  %35 = load i64, i64* @MEMCOUNT, align 8
  %36 = add i64 %35, 4112
  store i64 %36, i64* @MEMCOUNT, align 8
  %37 = load i64, i64* @MEMCOUNT, align 8
  %38 = load i64, i64* @MEMLIMIT, align 8
  %39 = icmp uge i64 %37, %38
  br i1 %39, label %40, label %41

; <label>:40:                                     ; preds = %34
  call void @fatal_err(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.3, i32 0, i32 0))
  br label %41

; <label>:41:                                     ; preds = %40, %34
  %42 = call i8* @malloc(i64 4096) #8
  %43 = bitcast i8* %42 to i64*
  store i64* %43, i64** %5, align 8
  store i64 0, i64* %6, align 8
  %44 = call i8* @malloc(i64 16) #8
  %45 = bitcast i8* %44 to i64*
  store i64* %45, i64** %7, align 8
  %46 = load i64*, i64** %7, align 8
  %47 = getelementptr inbounds i64, i64* %46, i64 0
  store i64 2, i64* %47, align 8
  br label %48

; <label>:48:                                     ; preds = %57, %41
  %49 = load i64, i64* %2, align 8
  %50 = and i64 %49, 7
  %51 = icmp eq i64 %50, 1
  br i1 %51, label %52, label %55

; <label>:52:                                     ; preds = %48
  %53 = load i64, i64* %6, align 8
  %54 = icmp ult i64 %53, 512
  br label %55

; <label>:55:                                     ; preds = %52, %48
  %56 = phi i1 [ false, %48 ], [ %54, %52 ]
  br i1 %56, label %57, label %64

; <label>:57:                                     ; preds = %55
  %58 = load i64, i64* %2, align 8
  %59 = call i64 @expect_cons(i64 %58, i64* %2)
  %60 = load i64*, i64** %5, align 8
  %61 = load i64, i64* %6, align 8
  %62 = add i64 %61, 1
  store i64 %62, i64* %6, align 8
  %63 = getelementptr inbounds i64, i64* %60, i64 %61
  store i64 %59, i64* %63, align 8
  br label %48

; <label>:64:                                     ; preds = %55
  store i64 0, i64* %8, align 8
  br label %65

; <label>:65:                                     ; preds = %1414, %64
  %66 = load i64, i64* %8, align 8
  %67 = load i64, i64* %6, align 8
  %68 = icmp ult i64 %66, %67
  br i1 %68, label %69, label %1417

; <label>:69:                                     ; preds = %65
  %70 = load i64*, i64** %5, align 8
  %71 = load i64, i64* %8, align 8
  %72 = getelementptr inbounds i64, i64* %70, i64 %71
  %73 = load i64, i64* %72, align 8
  store i64 %73, i64* %9, align 8
  %74 = load i64, i64* %9, align 8
  %75 = call i64 @expect_cons(i64 %74, i64* %9)
  store i64 %75, i64* %10, align 8
  br label %76

; <label>:76:                                     ; preds = %69
  br label %77

; <label>:77:                                     ; preds = %76
  br label %78

; <label>:78:                                     ; preds = %77
  %79 = bitcast i64* %10 to i8*
  store i8* %79, i8** %15, align 8
  store i32 -17973521, i32* %11, align 4
  store i32 -1640531527, i32* %13, align 4
  store i32 -1640531527, i32* %12, align 4
  store i32 8, i32* %14, align 4
  br label %80

; <label>:80:                                     ; preds = %247, %78
  %81 = load i32, i32* %14, align 4
  %82 = icmp uge i32 %81, 12
  br i1 %82, label %83, label %252

; <label>:83:                                     ; preds = %80
  %84 = load i8*, i8** %15, align 8
  %85 = getelementptr inbounds i8, i8* %84, i64 0
  %86 = load i8, i8* %85, align 1
  %87 = zext i8 %86 to i32
  %88 = load i8*, i8** %15, align 8
  %89 = getelementptr inbounds i8, i8* %88, i64 1
  %90 = load i8, i8* %89, align 1
  %91 = zext i8 %90 to i32
  %92 = shl i32 %91, 8
  %93 = add i32 %87, %92
  %94 = load i8*, i8** %15, align 8
  %95 = getelementptr inbounds i8, i8* %94, i64 2
  %96 = load i8, i8* %95, align 1
  %97 = zext i8 %96 to i32
  %98 = shl i32 %97, 16
  %99 = add i32 %93, %98
  %100 = load i8*, i8** %15, align 8
  %101 = getelementptr inbounds i8, i8* %100, i64 3
  %102 = load i8, i8* %101, align 1
  %103 = zext i8 %102 to i32
  %104 = shl i32 %103, 24
  %105 = add i32 %99, %104
  %106 = load i32, i32* %12, align 4
  %107 = add i32 %106, %105
  store i32 %107, i32* %12, align 4
  %108 = load i8*, i8** %15, align 8
  %109 = getelementptr inbounds i8, i8* %108, i64 4
  %110 = load i8, i8* %109, align 1
  %111 = zext i8 %110 to i32
  %112 = load i8*, i8** %15, align 8
  %113 = getelementptr inbounds i8, i8* %112, i64 5
  %114 = load i8, i8* %113, align 1
  %115 = zext i8 %114 to i32
  %116 = shl i32 %115, 8
  %117 = add i32 %111, %116
  %118 = load i8*, i8** %15, align 8
  %119 = getelementptr inbounds i8, i8* %118, i64 6
  %120 = load i8, i8* %119, align 1
  %121 = zext i8 %120 to i32
  %122 = shl i32 %121, 16
  %123 = add i32 %117, %122
  %124 = load i8*, i8** %15, align 8
  %125 = getelementptr inbounds i8, i8* %124, i64 7
  %126 = load i8, i8* %125, align 1
  %127 = zext i8 %126 to i32
  %128 = shl i32 %127, 24
  %129 = add i32 %123, %128
  %130 = load i32, i32* %13, align 4
  %131 = add i32 %130, %129
  store i32 %131, i32* %13, align 4
  %132 = load i8*, i8** %15, align 8
  %133 = getelementptr inbounds i8, i8* %132, i64 8
  %134 = load i8, i8* %133, align 1
  %135 = zext i8 %134 to i32
  %136 = load i8*, i8** %15, align 8
  %137 = getelementptr inbounds i8, i8* %136, i64 9
  %138 = load i8, i8* %137, align 1
  %139 = zext i8 %138 to i32
  %140 = shl i32 %139, 8
  %141 = add i32 %135, %140
  %142 = load i8*, i8** %15, align 8
  %143 = getelementptr inbounds i8, i8* %142, i64 10
  %144 = load i8, i8* %143, align 1
  %145 = zext i8 %144 to i32
  %146 = shl i32 %145, 16
  %147 = add i32 %141, %146
  %148 = load i8*, i8** %15, align 8
  %149 = getelementptr inbounds i8, i8* %148, i64 11
  %150 = load i8, i8* %149, align 1
  %151 = zext i8 %150 to i32
  %152 = shl i32 %151, 24
  %153 = add i32 %147, %152
  %154 = load i32, i32* %11, align 4
  %155 = add i32 %154, %153
  store i32 %155, i32* %11, align 4
  br label %156

; <label>:156:                                    ; preds = %83
  %157 = load i32, i32* %13, align 4
  %158 = load i32, i32* %12, align 4
  %159 = sub i32 %158, %157
  store i32 %159, i32* %12, align 4
  %160 = load i32, i32* %11, align 4
  %161 = load i32, i32* %12, align 4
  %162 = sub i32 %161, %160
  store i32 %162, i32* %12, align 4
  %163 = load i32, i32* %11, align 4
  %164 = lshr i32 %163, 13
  %165 = load i32, i32* %12, align 4
  %166 = xor i32 %165, %164
  store i32 %166, i32* %12, align 4
  %167 = load i32, i32* %11, align 4
  %168 = load i32, i32* %13, align 4
  %169 = sub i32 %168, %167
  store i32 %169, i32* %13, align 4
  %170 = load i32, i32* %12, align 4
  %171 = load i32, i32* %13, align 4
  %172 = sub i32 %171, %170
  store i32 %172, i32* %13, align 4
  %173 = load i32, i32* %12, align 4
  %174 = shl i32 %173, 8
  %175 = load i32, i32* %13, align 4
  %176 = xor i32 %175, %174
  store i32 %176, i32* %13, align 4
  %177 = load i32, i32* %12, align 4
  %178 = load i32, i32* %11, align 4
  %179 = sub i32 %178, %177
  store i32 %179, i32* %11, align 4
  %180 = load i32, i32* %13, align 4
  %181 = load i32, i32* %11, align 4
  %182 = sub i32 %181, %180
  store i32 %182, i32* %11, align 4
  %183 = load i32, i32* %13, align 4
  %184 = lshr i32 %183, 13
  %185 = load i32, i32* %11, align 4
  %186 = xor i32 %185, %184
  store i32 %186, i32* %11, align 4
  %187 = load i32, i32* %13, align 4
  %188 = load i32, i32* %12, align 4
  %189 = sub i32 %188, %187
  store i32 %189, i32* %12, align 4
  %190 = load i32, i32* %11, align 4
  %191 = load i32, i32* %12, align 4
  %192 = sub i32 %191, %190
  store i32 %192, i32* %12, align 4
  %193 = load i32, i32* %11, align 4
  %194 = lshr i32 %193, 12
  %195 = load i32, i32* %12, align 4
  %196 = xor i32 %195, %194
  store i32 %196, i32* %12, align 4
  %197 = load i32, i32* %11, align 4
  %198 = load i32, i32* %13, align 4
  %199 = sub i32 %198, %197
  store i32 %199, i32* %13, align 4
  %200 = load i32, i32* %12, align 4
  %201 = load i32, i32* %13, align 4
  %202 = sub i32 %201, %200
  store i32 %202, i32* %13, align 4
  %203 = load i32, i32* %12, align 4
  %204 = shl i32 %203, 16
  %205 = load i32, i32* %13, align 4
  %206 = xor i32 %205, %204
  store i32 %206, i32* %13, align 4
  %207 = load i32, i32* %12, align 4
  %208 = load i32, i32* %11, align 4
  %209 = sub i32 %208, %207
  store i32 %209, i32* %11, align 4
  %210 = load i32, i32* %13, align 4
  %211 = load i32, i32* %11, align 4
  %212 = sub i32 %211, %210
  store i32 %212, i32* %11, align 4
  %213 = load i32, i32* %13, align 4
  %214 = lshr i32 %213, 5
  %215 = load i32, i32* %11, align 4
  %216 = xor i32 %215, %214
  store i32 %216, i32* %11, align 4
  %217 = load i32, i32* %13, align 4
  %218 = load i32, i32* %12, align 4
  %219 = sub i32 %218, %217
  store i32 %219, i32* %12, align 4
  %220 = load i32, i32* %11, align 4
  %221 = load i32, i32* %12, align 4
  %222 = sub i32 %221, %220
  store i32 %222, i32* %12, align 4
  %223 = load i32, i32* %11, align 4
  %224 = lshr i32 %223, 3
  %225 = load i32, i32* %12, align 4
  %226 = xor i32 %225, %224
  store i32 %226, i32* %12, align 4
  %227 = load i32, i32* %11, align 4
  %228 = load i32, i32* %13, align 4
  %229 = sub i32 %228, %227
  store i32 %229, i32* %13, align 4
  %230 = load i32, i32* %12, align 4
  %231 = load i32, i32* %13, align 4
  %232 = sub i32 %231, %230
  store i32 %232, i32* %13, align 4
  %233 = load i32, i32* %12, align 4
  %234 = shl i32 %233, 10
  %235 = load i32, i32* %13, align 4
  %236 = xor i32 %235, %234
  store i32 %236, i32* %13, align 4
  %237 = load i32, i32* %12, align 4
  %238 = load i32, i32* %11, align 4
  %239 = sub i32 %238, %237
  store i32 %239, i32* %11, align 4
  %240 = load i32, i32* %13, align 4
  %241 = load i32, i32* %11, align 4
  %242 = sub i32 %241, %240
  store i32 %242, i32* %11, align 4
  %243 = load i32, i32* %13, align 4
  %244 = lshr i32 %243, 15
  %245 = load i32, i32* %11, align 4
  %246 = xor i32 %245, %244
  store i32 %246, i32* %11, align 4
  br label %247

; <label>:247:                                    ; preds = %156
  %248 = load i8*, i8** %15, align 8
  %249 = getelementptr inbounds i8, i8* %248, i64 12
  store i8* %249, i8** %15, align 8
  %250 = load i32, i32* %14, align 4
  %251 = sub i32 %250, 12
  store i32 %251, i32* %14, align 4
  br label %80

; <label>:252:                                    ; preds = %80
  %253 = load i32, i32* %11, align 4
  %254 = add i32 %253, 8
  store i32 %254, i32* %11, align 4
  %255 = load i32, i32* %14, align 4
  switch i32 %255, label %342 [
    i32 11, label %256
    i32 10, label %264
    i32 9, label %272
    i32 8, label %280
    i32 7, label %288
    i32 6, label %296
    i32 5, label %304
    i32 4, label %311
    i32 3, label %319
    i32 2, label %327
    i32 1, label %335
  ]

; <label>:256:                                    ; preds = %252
  %257 = load i8*, i8** %15, align 8
  %258 = getelementptr inbounds i8, i8* %257, i64 10
  %259 = load i8, i8* %258, align 1
  %260 = zext i8 %259 to i32
  %261 = shl i32 %260, 24
  %262 = load i32, i32* %11, align 4
  %263 = add i32 %262, %261
  store i32 %263, i32* %11, align 4
  br label %264

; <label>:264:                                    ; preds = %252, %256
  %265 = load i8*, i8** %15, align 8
  %266 = getelementptr inbounds i8, i8* %265, i64 9
  %267 = load i8, i8* %266, align 1
  %268 = zext i8 %267 to i32
  %269 = shl i32 %268, 16
  %270 = load i32, i32* %11, align 4
  %271 = add i32 %270, %269
  store i32 %271, i32* %11, align 4
  br label %272

; <label>:272:                                    ; preds = %252, %264
  %273 = load i8*, i8** %15, align 8
  %274 = getelementptr inbounds i8, i8* %273, i64 8
  %275 = load i8, i8* %274, align 1
  %276 = zext i8 %275 to i32
  %277 = shl i32 %276, 8
  %278 = load i32, i32* %11, align 4
  %279 = add i32 %278, %277
  store i32 %279, i32* %11, align 4
  br label %280

; <label>:280:                                    ; preds = %252, %272
  %281 = load i8*, i8** %15, align 8
  %282 = getelementptr inbounds i8, i8* %281, i64 7
  %283 = load i8, i8* %282, align 1
  %284 = zext i8 %283 to i32
  %285 = shl i32 %284, 24
  %286 = load i32, i32* %13, align 4
  %287 = add i32 %286, %285
  store i32 %287, i32* %13, align 4
  br label %288

; <label>:288:                                    ; preds = %252, %280
  %289 = load i8*, i8** %15, align 8
  %290 = getelementptr inbounds i8, i8* %289, i64 6
  %291 = load i8, i8* %290, align 1
  %292 = zext i8 %291 to i32
  %293 = shl i32 %292, 16
  %294 = load i32, i32* %13, align 4
  %295 = add i32 %294, %293
  store i32 %295, i32* %13, align 4
  br label %296

; <label>:296:                                    ; preds = %252, %288
  %297 = load i8*, i8** %15, align 8
  %298 = getelementptr inbounds i8, i8* %297, i64 5
  %299 = load i8, i8* %298, align 1
  %300 = zext i8 %299 to i32
  %301 = shl i32 %300, 8
  %302 = load i32, i32* %13, align 4
  %303 = add i32 %302, %301
  store i32 %303, i32* %13, align 4
  br label %304

; <label>:304:                                    ; preds = %252, %296
  %305 = load i8*, i8** %15, align 8
  %306 = getelementptr inbounds i8, i8* %305, i64 4
  %307 = load i8, i8* %306, align 1
  %308 = zext i8 %307 to i32
  %309 = load i32, i32* %13, align 4
  %310 = add i32 %309, %308
  store i32 %310, i32* %13, align 4
  br label %311

; <label>:311:                                    ; preds = %252, %304
  %312 = load i8*, i8** %15, align 8
  %313 = getelementptr inbounds i8, i8* %312, i64 3
  %314 = load i8, i8* %313, align 1
  %315 = zext i8 %314 to i32
  %316 = shl i32 %315, 24
  %317 = load i32, i32* %12, align 4
  %318 = add i32 %317, %316
  store i32 %318, i32* %12, align 4
  br label %319

; <label>:319:                                    ; preds = %252, %311
  %320 = load i8*, i8** %15, align 8
  %321 = getelementptr inbounds i8, i8* %320, i64 2
  %322 = load i8, i8* %321, align 1
  %323 = zext i8 %322 to i32
  %324 = shl i32 %323, 16
  %325 = load i32, i32* %12, align 4
  %326 = add i32 %325, %324
  store i32 %326, i32* %12, align 4
  br label %327

; <label>:327:                                    ; preds = %252, %319
  %328 = load i8*, i8** %15, align 8
  %329 = getelementptr inbounds i8, i8* %328, i64 1
  %330 = load i8, i8* %329, align 1
  %331 = zext i8 %330 to i32
  %332 = shl i32 %331, 8
  %333 = load i32, i32* %12, align 4
  %334 = add i32 %333, %332
  store i32 %334, i32* %12, align 4
  br label %335

; <label>:335:                                    ; preds = %252, %327
  %336 = load i8*, i8** %15, align 8
  %337 = getelementptr inbounds i8, i8* %336, i64 0
  %338 = load i8, i8* %337, align 1
  %339 = zext i8 %338 to i32
  %340 = load i32, i32* %12, align 4
  %341 = add i32 %340, %339
  store i32 %341, i32* %12, align 4
  br label %342

; <label>:342:                                    ; preds = %335, %252
  br label %343

; <label>:343:                                    ; preds = %342
  %344 = load i32, i32* %13, align 4
  %345 = load i32, i32* %12, align 4
  %346 = sub i32 %345, %344
  store i32 %346, i32* %12, align 4
  %347 = load i32, i32* %11, align 4
  %348 = load i32, i32* %12, align 4
  %349 = sub i32 %348, %347
  store i32 %349, i32* %12, align 4
  %350 = load i32, i32* %11, align 4
  %351 = lshr i32 %350, 13
  %352 = load i32, i32* %12, align 4
  %353 = xor i32 %352, %351
  store i32 %353, i32* %12, align 4
  %354 = load i32, i32* %11, align 4
  %355 = load i32, i32* %13, align 4
  %356 = sub i32 %355, %354
  store i32 %356, i32* %13, align 4
  %357 = load i32, i32* %12, align 4
  %358 = load i32, i32* %13, align 4
  %359 = sub i32 %358, %357
  store i32 %359, i32* %13, align 4
  %360 = load i32, i32* %12, align 4
  %361 = shl i32 %360, 8
  %362 = load i32, i32* %13, align 4
  %363 = xor i32 %362, %361
  store i32 %363, i32* %13, align 4
  %364 = load i32, i32* %12, align 4
  %365 = load i32, i32* %11, align 4
  %366 = sub i32 %365, %364
  store i32 %366, i32* %11, align 4
  %367 = load i32, i32* %13, align 4
  %368 = load i32, i32* %11, align 4
  %369 = sub i32 %368, %367
  store i32 %369, i32* %11, align 4
  %370 = load i32, i32* %13, align 4
  %371 = lshr i32 %370, 13
  %372 = load i32, i32* %11, align 4
  %373 = xor i32 %372, %371
  store i32 %373, i32* %11, align 4
  %374 = load i32, i32* %13, align 4
  %375 = load i32, i32* %12, align 4
  %376 = sub i32 %375, %374
  store i32 %376, i32* %12, align 4
  %377 = load i32, i32* %11, align 4
  %378 = load i32, i32* %12, align 4
  %379 = sub i32 %378, %377
  store i32 %379, i32* %12, align 4
  %380 = load i32, i32* %11, align 4
  %381 = lshr i32 %380, 12
  %382 = load i32, i32* %12, align 4
  %383 = xor i32 %382, %381
  store i32 %383, i32* %12, align 4
  %384 = load i32, i32* %11, align 4
  %385 = load i32, i32* %13, align 4
  %386 = sub i32 %385, %384
  store i32 %386, i32* %13, align 4
  %387 = load i32, i32* %12, align 4
  %388 = load i32, i32* %13, align 4
  %389 = sub i32 %388, %387
  store i32 %389, i32* %13, align 4
  %390 = load i32, i32* %12, align 4
  %391 = shl i32 %390, 16
  %392 = load i32, i32* %13, align 4
  %393 = xor i32 %392, %391
  store i32 %393, i32* %13, align 4
  %394 = load i32, i32* %12, align 4
  %395 = load i32, i32* %11, align 4
  %396 = sub i32 %395, %394
  store i32 %396, i32* %11, align 4
  %397 = load i32, i32* %13, align 4
  %398 = load i32, i32* %11, align 4
  %399 = sub i32 %398, %397
  store i32 %399, i32* %11, align 4
  %400 = load i32, i32* %13, align 4
  %401 = lshr i32 %400, 5
  %402 = load i32, i32* %11, align 4
  %403 = xor i32 %402, %401
  store i32 %403, i32* %11, align 4
  %404 = load i32, i32* %13, align 4
  %405 = load i32, i32* %12, align 4
  %406 = sub i32 %405, %404
  store i32 %406, i32* %12, align 4
  %407 = load i32, i32* %11, align 4
  %408 = load i32, i32* %12, align 4
  %409 = sub i32 %408, %407
  store i32 %409, i32* %12, align 4
  %410 = load i32, i32* %11, align 4
  %411 = lshr i32 %410, 3
  %412 = load i32, i32* %12, align 4
  %413 = xor i32 %412, %411
  store i32 %413, i32* %12, align 4
  %414 = load i32, i32* %11, align 4
  %415 = load i32, i32* %13, align 4
  %416 = sub i32 %415, %414
  store i32 %416, i32* %13, align 4
  %417 = load i32, i32* %12, align 4
  %418 = load i32, i32* %13, align 4
  %419 = sub i32 %418, %417
  store i32 %419, i32* %13, align 4
  %420 = load i32, i32* %12, align 4
  %421 = shl i32 %420, 10
  %422 = load i32, i32* %13, align 4
  %423 = xor i32 %422, %421
  store i32 %423, i32* %13, align 4
  %424 = load i32, i32* %12, align 4
  %425 = load i32, i32* %11, align 4
  %426 = sub i32 %425, %424
  store i32 %426, i32* %11, align 4
  %427 = load i32, i32* %13, align 4
  %428 = load i32, i32* %11, align 4
  %429 = sub i32 %428, %427
  store i32 %429, i32* %11, align 4
  %430 = load i32, i32* %13, align 4
  %431 = lshr i32 %430, 15
  %432 = load i32, i32* %11, align 4
  %433 = xor i32 %432, %431
  store i32 %433, i32* %11, align 4
  br label %434

; <label>:434:                                    ; preds = %343
  br label %435

; <label>:435:                                    ; preds = %434
  br label %436

; <label>:436:                                    ; preds = %435
  br label %437

; <label>:437:                                    ; preds = %436
  store %struct.my_hash* null, %struct.my_hash** %4, align 8
  %438 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %439 = icmp ne %struct.my_hash* %438, null
  br i1 %439, label %440, label %545

; <label>:440:                                    ; preds = %437
  br label %441

; <label>:441:                                    ; preds = %440
  %442 = load i32, i32* %11, align 4
  %443 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %444 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %443, i32 0, i32 2
  %445 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %444, i32 0, i32 0
  %446 = load %struct.UT_hash_table*, %struct.UT_hash_table** %445, align 8
  %447 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %446, i32 0, i32 1
  %448 = load i32, i32* %447, align 8
  %449 = sub i32 %448, 1
  %450 = and i32 %442, %449
  store i32 %450, i32* %16, align 4
  br label %451

; <label>:451:                                    ; preds = %441
  br label %452

; <label>:452:                                    ; preds = %451
  %453 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %454 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %453, i32 0, i32 2
  %455 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %454, i32 0, i32 0
  %456 = load %struct.UT_hash_table*, %struct.UT_hash_table** %455, align 8
  %457 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %456, i32 0, i32 0
  %458 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %457, align 8
  %459 = load i32, i32* %16, align 4
  %460 = zext i32 %459 to i64
  %461 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %458, i64 %460
  %462 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %461, i32 0, i32 0
  %463 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %462, align 8
  %464 = icmp ne %struct.UT_hash_handle* %463, null
  br i1 %464, label %465, label %489

; <label>:465:                                    ; preds = %452
  br label %466

; <label>:466:                                    ; preds = %465
  %467 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %468 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %467, i32 0, i32 2
  %469 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %468, i32 0, i32 0
  %470 = load %struct.UT_hash_table*, %struct.UT_hash_table** %469, align 8
  %471 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %470, i32 0, i32 0
  %472 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %471, align 8
  %473 = load i32, i32* %16, align 4
  %474 = zext i32 %473 to i64
  %475 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %472, i64 %474
  %476 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %475, i32 0, i32 0
  %477 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %476, align 8
  %478 = bitcast %struct.UT_hash_handle* %477 to i8*
  %479 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %480 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %479, i32 0, i32 2
  %481 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %480, i32 0, i32 0
  %482 = load %struct.UT_hash_table*, %struct.UT_hash_table** %481, align 8
  %483 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %482, i32 0, i32 5
  %484 = load i64, i64* %483, align 8
  %485 = sub i64 0, %484
  %486 = getelementptr inbounds i8, i8* %478, i64 %485
  %487 = bitcast i8* %486 to %struct.my_hash*
  store %struct.my_hash* %487, %struct.my_hash** %4, align 8
  br label %488

; <label>:488:                                    ; preds = %466
  br label %490

; <label>:489:                                    ; preds = %452
  store %struct.my_hash* null, %struct.my_hash** %4, align 8
  br label %490

; <label>:490:                                    ; preds = %489, %488
  br label %491

; <label>:491:                                    ; preds = %542, %490
  %492 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %493 = icmp ne %struct.my_hash* %492, null
  br i1 %493, label %494, label %543

; <label>:494:                                    ; preds = %491
  %495 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %496 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %495, i32 0, i32 2
  %497 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %496, i32 0, i32 7
  %498 = load i32, i32* %497, align 4
  %499 = load i32, i32* %11, align 4
  %500 = icmp eq i32 %498, %499
  br i1 %500, label %501, label %518

; <label>:501:                                    ; preds = %494
  %502 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %503 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %502, i32 0, i32 2
  %504 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %503, i32 0, i32 6
  %505 = load i32, i32* %504, align 8
  %506 = zext i32 %505 to i64
  %507 = icmp eq i64 %506, 8
  br i1 %507, label %508, label %518

; <label>:508:                                    ; preds = %501
  %509 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %510 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %509, i32 0, i32 2
  %511 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %510, i32 0, i32 5
  %512 = load i8*, i8** %511, align 8
  %513 = bitcast i64* %10 to i8*
  %514 = call i32 @memcmp(i8* %512, i8* %513, i64 8)
  %515 = icmp eq i32 %514, 0
  br i1 %515, label %516, label %517

; <label>:516:                                    ; preds = %508
  br label %543

; <label>:517:                                    ; preds = %508
  br label %518

; <label>:518:                                    ; preds = %517, %501, %494
  %519 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %520 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %519, i32 0, i32 2
  %521 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %520, i32 0, i32 4
  %522 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %521, align 8
  %523 = icmp ne %struct.UT_hash_handle* %522, null
  br i1 %523, label %524, label %541

; <label>:524:                                    ; preds = %518
  br label %525

; <label>:525:                                    ; preds = %524
  %526 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %527 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %526, i32 0, i32 2
  %528 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %527, i32 0, i32 4
  %529 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %528, align 8
  %530 = bitcast %struct.UT_hash_handle* %529 to i8*
  %531 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %532 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %531, i32 0, i32 2
  %533 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %532, i32 0, i32 0
  %534 = load %struct.UT_hash_table*, %struct.UT_hash_table** %533, align 8
  %535 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %534, i32 0, i32 5
  %536 = load i64, i64* %535, align 8
  %537 = sub i64 0, %536
  %538 = getelementptr inbounds i8, i8* %530, i64 %537
  %539 = bitcast i8* %538 to %struct.my_hash*
  store %struct.my_hash* %539, %struct.my_hash** %4, align 8
  br label %540

; <label>:540:                                    ; preds = %525
  br label %542

; <label>:541:                                    ; preds = %518
  store %struct.my_hash* null, %struct.my_hash** %4, align 8
  br label %542

; <label>:542:                                    ; preds = %541, %540
  br label %491

; <label>:543:                                    ; preds = %516, %491
  br label %544

; <label>:544:                                    ; preds = %543
  br label %545

; <label>:545:                                    ; preds = %544, %437
  br label %546

; <label>:546:                                    ; preds = %545
  br label %547

; <label>:547:                                    ; preds = %546
  %548 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %549 = icmp eq %struct.my_hash* %548, null
  br i1 %549, label %550, label %1409

; <label>:550:                                    ; preds = %547
  %551 = load i64, i64* @MEMCOUNT, align 8
  %552 = add i64 %551, 72
  store i64 %552, i64* @MEMCOUNT, align 8
  %553 = load i64, i64* @MEMCOUNT, align 8
  %554 = load i64, i64* @MEMLIMIT, align 8
  %555 = icmp uge i64 %553, %554
  br i1 %555, label %556, label %557

; <label>:556:                                    ; preds = %550
  call void @fatal_err(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.3, i32 0, i32 0))
  br label %557

; <label>:557:                                    ; preds = %556, %550
  %558 = call i8* @malloc(i64 72) #8
  %559 = bitcast i8* %558 to %struct.my_hash*
  store %struct.my_hash* %559, %struct.my_hash** %4, align 8
  %560 = load i64, i64* %10, align 8
  %561 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %562 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %561, i32 0, i32 0
  store i64 %560, i64* %562, align 8
  %563 = load i64, i64* %9, align 8
  %564 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %565 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %564, i32 0, i32 1
  store i64 %563, i64* %565, align 8
  br label %566

; <label>:566:                                    ; preds = %557
  br label %567

; <label>:567:                                    ; preds = %566
  br label %568

; <label>:568:                                    ; preds = %567
  %569 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %570 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %569, i32 0, i32 0
  %571 = bitcast i64* %570 to i8*
  store i8* %571, i8** %21, align 8
  store i32 -17973521, i32* %17, align 4
  store i32 -1640531527, i32* %19, align 4
  store i32 -1640531527, i32* %18, align 4
  store i32 8, i32* %20, align 4
  br label %572

; <label>:572:                                    ; preds = %739, %568
  %573 = load i32, i32* %20, align 4
  %574 = icmp uge i32 %573, 12
  br i1 %574, label %575, label %744

; <label>:575:                                    ; preds = %572
  %576 = load i8*, i8** %21, align 8
  %577 = getelementptr inbounds i8, i8* %576, i64 0
  %578 = load i8, i8* %577, align 1
  %579 = zext i8 %578 to i32
  %580 = load i8*, i8** %21, align 8
  %581 = getelementptr inbounds i8, i8* %580, i64 1
  %582 = load i8, i8* %581, align 1
  %583 = zext i8 %582 to i32
  %584 = shl i32 %583, 8
  %585 = add i32 %579, %584
  %586 = load i8*, i8** %21, align 8
  %587 = getelementptr inbounds i8, i8* %586, i64 2
  %588 = load i8, i8* %587, align 1
  %589 = zext i8 %588 to i32
  %590 = shl i32 %589, 16
  %591 = add i32 %585, %590
  %592 = load i8*, i8** %21, align 8
  %593 = getelementptr inbounds i8, i8* %592, i64 3
  %594 = load i8, i8* %593, align 1
  %595 = zext i8 %594 to i32
  %596 = shl i32 %595, 24
  %597 = add i32 %591, %596
  %598 = load i32, i32* %18, align 4
  %599 = add i32 %598, %597
  store i32 %599, i32* %18, align 4
  %600 = load i8*, i8** %21, align 8
  %601 = getelementptr inbounds i8, i8* %600, i64 4
  %602 = load i8, i8* %601, align 1
  %603 = zext i8 %602 to i32
  %604 = load i8*, i8** %21, align 8
  %605 = getelementptr inbounds i8, i8* %604, i64 5
  %606 = load i8, i8* %605, align 1
  %607 = zext i8 %606 to i32
  %608 = shl i32 %607, 8
  %609 = add i32 %603, %608
  %610 = load i8*, i8** %21, align 8
  %611 = getelementptr inbounds i8, i8* %610, i64 6
  %612 = load i8, i8* %611, align 1
  %613 = zext i8 %612 to i32
  %614 = shl i32 %613, 16
  %615 = add i32 %609, %614
  %616 = load i8*, i8** %21, align 8
  %617 = getelementptr inbounds i8, i8* %616, i64 7
  %618 = load i8, i8* %617, align 1
  %619 = zext i8 %618 to i32
  %620 = shl i32 %619, 24
  %621 = add i32 %615, %620
  %622 = load i32, i32* %19, align 4
  %623 = add i32 %622, %621
  store i32 %623, i32* %19, align 4
  %624 = load i8*, i8** %21, align 8
  %625 = getelementptr inbounds i8, i8* %624, i64 8
  %626 = load i8, i8* %625, align 1
  %627 = zext i8 %626 to i32
  %628 = load i8*, i8** %21, align 8
  %629 = getelementptr inbounds i8, i8* %628, i64 9
  %630 = load i8, i8* %629, align 1
  %631 = zext i8 %630 to i32
  %632 = shl i32 %631, 8
  %633 = add i32 %627, %632
  %634 = load i8*, i8** %21, align 8
  %635 = getelementptr inbounds i8, i8* %634, i64 10
  %636 = load i8, i8* %635, align 1
  %637 = zext i8 %636 to i32
  %638 = shl i32 %637, 16
  %639 = add i32 %633, %638
  %640 = load i8*, i8** %21, align 8
  %641 = getelementptr inbounds i8, i8* %640, i64 11
  %642 = load i8, i8* %641, align 1
  %643 = zext i8 %642 to i32
  %644 = shl i32 %643, 24
  %645 = add i32 %639, %644
  %646 = load i32, i32* %17, align 4
  %647 = add i32 %646, %645
  store i32 %647, i32* %17, align 4
  br label %648

; <label>:648:                                    ; preds = %575
  %649 = load i32, i32* %19, align 4
  %650 = load i32, i32* %18, align 4
  %651 = sub i32 %650, %649
  store i32 %651, i32* %18, align 4
  %652 = load i32, i32* %17, align 4
  %653 = load i32, i32* %18, align 4
  %654 = sub i32 %653, %652
  store i32 %654, i32* %18, align 4
  %655 = load i32, i32* %17, align 4
  %656 = lshr i32 %655, 13
  %657 = load i32, i32* %18, align 4
  %658 = xor i32 %657, %656
  store i32 %658, i32* %18, align 4
  %659 = load i32, i32* %17, align 4
  %660 = load i32, i32* %19, align 4
  %661 = sub i32 %660, %659
  store i32 %661, i32* %19, align 4
  %662 = load i32, i32* %18, align 4
  %663 = load i32, i32* %19, align 4
  %664 = sub i32 %663, %662
  store i32 %664, i32* %19, align 4
  %665 = load i32, i32* %18, align 4
  %666 = shl i32 %665, 8
  %667 = load i32, i32* %19, align 4
  %668 = xor i32 %667, %666
  store i32 %668, i32* %19, align 4
  %669 = load i32, i32* %18, align 4
  %670 = load i32, i32* %17, align 4
  %671 = sub i32 %670, %669
  store i32 %671, i32* %17, align 4
  %672 = load i32, i32* %19, align 4
  %673 = load i32, i32* %17, align 4
  %674 = sub i32 %673, %672
  store i32 %674, i32* %17, align 4
  %675 = load i32, i32* %19, align 4
  %676 = lshr i32 %675, 13
  %677 = load i32, i32* %17, align 4
  %678 = xor i32 %677, %676
  store i32 %678, i32* %17, align 4
  %679 = load i32, i32* %19, align 4
  %680 = load i32, i32* %18, align 4
  %681 = sub i32 %680, %679
  store i32 %681, i32* %18, align 4
  %682 = load i32, i32* %17, align 4
  %683 = load i32, i32* %18, align 4
  %684 = sub i32 %683, %682
  store i32 %684, i32* %18, align 4
  %685 = load i32, i32* %17, align 4
  %686 = lshr i32 %685, 12
  %687 = load i32, i32* %18, align 4
  %688 = xor i32 %687, %686
  store i32 %688, i32* %18, align 4
  %689 = load i32, i32* %17, align 4
  %690 = load i32, i32* %19, align 4
  %691 = sub i32 %690, %689
  store i32 %691, i32* %19, align 4
  %692 = load i32, i32* %18, align 4
  %693 = load i32, i32* %19, align 4
  %694 = sub i32 %693, %692
  store i32 %694, i32* %19, align 4
  %695 = load i32, i32* %18, align 4
  %696 = shl i32 %695, 16
  %697 = load i32, i32* %19, align 4
  %698 = xor i32 %697, %696
  store i32 %698, i32* %19, align 4
  %699 = load i32, i32* %18, align 4
  %700 = load i32, i32* %17, align 4
  %701 = sub i32 %700, %699
  store i32 %701, i32* %17, align 4
  %702 = load i32, i32* %19, align 4
  %703 = load i32, i32* %17, align 4
  %704 = sub i32 %703, %702
  store i32 %704, i32* %17, align 4
  %705 = load i32, i32* %19, align 4
  %706 = lshr i32 %705, 5
  %707 = load i32, i32* %17, align 4
  %708 = xor i32 %707, %706
  store i32 %708, i32* %17, align 4
  %709 = load i32, i32* %19, align 4
  %710 = load i32, i32* %18, align 4
  %711 = sub i32 %710, %709
  store i32 %711, i32* %18, align 4
  %712 = load i32, i32* %17, align 4
  %713 = load i32, i32* %18, align 4
  %714 = sub i32 %713, %712
  store i32 %714, i32* %18, align 4
  %715 = load i32, i32* %17, align 4
  %716 = lshr i32 %715, 3
  %717 = load i32, i32* %18, align 4
  %718 = xor i32 %717, %716
  store i32 %718, i32* %18, align 4
  %719 = load i32, i32* %17, align 4
  %720 = load i32, i32* %19, align 4
  %721 = sub i32 %720, %719
  store i32 %721, i32* %19, align 4
  %722 = load i32, i32* %18, align 4
  %723 = load i32, i32* %19, align 4
  %724 = sub i32 %723, %722
  store i32 %724, i32* %19, align 4
  %725 = load i32, i32* %18, align 4
  %726 = shl i32 %725, 10
  %727 = load i32, i32* %19, align 4
  %728 = xor i32 %727, %726
  store i32 %728, i32* %19, align 4
  %729 = load i32, i32* %18, align 4
  %730 = load i32, i32* %17, align 4
  %731 = sub i32 %730, %729
  store i32 %731, i32* %17, align 4
  %732 = load i32, i32* %19, align 4
  %733 = load i32, i32* %17, align 4
  %734 = sub i32 %733, %732
  store i32 %734, i32* %17, align 4
  %735 = load i32, i32* %19, align 4
  %736 = lshr i32 %735, 15
  %737 = load i32, i32* %17, align 4
  %738 = xor i32 %737, %736
  store i32 %738, i32* %17, align 4
  br label %739

; <label>:739:                                    ; preds = %648
  %740 = load i8*, i8** %21, align 8
  %741 = getelementptr inbounds i8, i8* %740, i64 12
  store i8* %741, i8** %21, align 8
  %742 = load i32, i32* %20, align 4
  %743 = sub i32 %742, 12
  store i32 %743, i32* %20, align 4
  br label %572

; <label>:744:                                    ; preds = %572
  %745 = load i32, i32* %17, align 4
  %746 = add i32 %745, 8
  store i32 %746, i32* %17, align 4
  %747 = load i32, i32* %20, align 4
  switch i32 %747, label %834 [
    i32 11, label %748
    i32 10, label %756
    i32 9, label %764
    i32 8, label %772
    i32 7, label %780
    i32 6, label %788
    i32 5, label %796
    i32 4, label %803
    i32 3, label %811
    i32 2, label %819
    i32 1, label %827
  ]

; <label>:748:                                    ; preds = %744
  %749 = load i8*, i8** %21, align 8
  %750 = getelementptr inbounds i8, i8* %749, i64 10
  %751 = load i8, i8* %750, align 1
  %752 = zext i8 %751 to i32
  %753 = shl i32 %752, 24
  %754 = load i32, i32* %17, align 4
  %755 = add i32 %754, %753
  store i32 %755, i32* %17, align 4
  br label %756

; <label>:756:                                    ; preds = %744, %748
  %757 = load i8*, i8** %21, align 8
  %758 = getelementptr inbounds i8, i8* %757, i64 9
  %759 = load i8, i8* %758, align 1
  %760 = zext i8 %759 to i32
  %761 = shl i32 %760, 16
  %762 = load i32, i32* %17, align 4
  %763 = add i32 %762, %761
  store i32 %763, i32* %17, align 4
  br label %764

; <label>:764:                                    ; preds = %744, %756
  %765 = load i8*, i8** %21, align 8
  %766 = getelementptr inbounds i8, i8* %765, i64 8
  %767 = load i8, i8* %766, align 1
  %768 = zext i8 %767 to i32
  %769 = shl i32 %768, 8
  %770 = load i32, i32* %17, align 4
  %771 = add i32 %770, %769
  store i32 %771, i32* %17, align 4
  br label %772

; <label>:772:                                    ; preds = %744, %764
  %773 = load i8*, i8** %21, align 8
  %774 = getelementptr inbounds i8, i8* %773, i64 7
  %775 = load i8, i8* %774, align 1
  %776 = zext i8 %775 to i32
  %777 = shl i32 %776, 24
  %778 = load i32, i32* %19, align 4
  %779 = add i32 %778, %777
  store i32 %779, i32* %19, align 4
  br label %780

; <label>:780:                                    ; preds = %744, %772
  %781 = load i8*, i8** %21, align 8
  %782 = getelementptr inbounds i8, i8* %781, i64 6
  %783 = load i8, i8* %782, align 1
  %784 = zext i8 %783 to i32
  %785 = shl i32 %784, 16
  %786 = load i32, i32* %19, align 4
  %787 = add i32 %786, %785
  store i32 %787, i32* %19, align 4
  br label %788

; <label>:788:                                    ; preds = %744, %780
  %789 = load i8*, i8** %21, align 8
  %790 = getelementptr inbounds i8, i8* %789, i64 5
  %791 = load i8, i8* %790, align 1
  %792 = zext i8 %791 to i32
  %793 = shl i32 %792, 8
  %794 = load i32, i32* %19, align 4
  %795 = add i32 %794, %793
  store i32 %795, i32* %19, align 4
  br label %796

; <label>:796:                                    ; preds = %744, %788
  %797 = load i8*, i8** %21, align 8
  %798 = getelementptr inbounds i8, i8* %797, i64 4
  %799 = load i8, i8* %798, align 1
  %800 = zext i8 %799 to i32
  %801 = load i32, i32* %19, align 4
  %802 = add i32 %801, %800
  store i32 %802, i32* %19, align 4
  br label %803

; <label>:803:                                    ; preds = %744, %796
  %804 = load i8*, i8** %21, align 8
  %805 = getelementptr inbounds i8, i8* %804, i64 3
  %806 = load i8, i8* %805, align 1
  %807 = zext i8 %806 to i32
  %808 = shl i32 %807, 24
  %809 = load i32, i32* %18, align 4
  %810 = add i32 %809, %808
  store i32 %810, i32* %18, align 4
  br label %811

; <label>:811:                                    ; preds = %744, %803
  %812 = load i8*, i8** %21, align 8
  %813 = getelementptr inbounds i8, i8* %812, i64 2
  %814 = load i8, i8* %813, align 1
  %815 = zext i8 %814 to i32
  %816 = shl i32 %815, 16
  %817 = load i32, i32* %18, align 4
  %818 = add i32 %817, %816
  store i32 %818, i32* %18, align 4
  br label %819

; <label>:819:                                    ; preds = %744, %811
  %820 = load i8*, i8** %21, align 8
  %821 = getelementptr inbounds i8, i8* %820, i64 1
  %822 = load i8, i8* %821, align 1
  %823 = zext i8 %822 to i32
  %824 = shl i32 %823, 8
  %825 = load i32, i32* %18, align 4
  %826 = add i32 %825, %824
  store i32 %826, i32* %18, align 4
  br label %827

; <label>:827:                                    ; preds = %744, %819
  %828 = load i8*, i8** %21, align 8
  %829 = getelementptr inbounds i8, i8* %828, i64 0
  %830 = load i8, i8* %829, align 1
  %831 = zext i8 %830 to i32
  %832 = load i32, i32* %18, align 4
  %833 = add i32 %832, %831
  store i32 %833, i32* %18, align 4
  br label %834

; <label>:834:                                    ; preds = %827, %744
  br label %835

; <label>:835:                                    ; preds = %834
  %836 = load i32, i32* %19, align 4
  %837 = load i32, i32* %18, align 4
  %838 = sub i32 %837, %836
  store i32 %838, i32* %18, align 4
  %839 = load i32, i32* %17, align 4
  %840 = load i32, i32* %18, align 4
  %841 = sub i32 %840, %839
  store i32 %841, i32* %18, align 4
  %842 = load i32, i32* %17, align 4
  %843 = lshr i32 %842, 13
  %844 = load i32, i32* %18, align 4
  %845 = xor i32 %844, %843
  store i32 %845, i32* %18, align 4
  %846 = load i32, i32* %17, align 4
  %847 = load i32, i32* %19, align 4
  %848 = sub i32 %847, %846
  store i32 %848, i32* %19, align 4
  %849 = load i32, i32* %18, align 4
  %850 = load i32, i32* %19, align 4
  %851 = sub i32 %850, %849
  store i32 %851, i32* %19, align 4
  %852 = load i32, i32* %18, align 4
  %853 = shl i32 %852, 8
  %854 = load i32, i32* %19, align 4
  %855 = xor i32 %854, %853
  store i32 %855, i32* %19, align 4
  %856 = load i32, i32* %18, align 4
  %857 = load i32, i32* %17, align 4
  %858 = sub i32 %857, %856
  store i32 %858, i32* %17, align 4
  %859 = load i32, i32* %19, align 4
  %860 = load i32, i32* %17, align 4
  %861 = sub i32 %860, %859
  store i32 %861, i32* %17, align 4
  %862 = load i32, i32* %19, align 4
  %863 = lshr i32 %862, 13
  %864 = load i32, i32* %17, align 4
  %865 = xor i32 %864, %863
  store i32 %865, i32* %17, align 4
  %866 = load i32, i32* %19, align 4
  %867 = load i32, i32* %18, align 4
  %868 = sub i32 %867, %866
  store i32 %868, i32* %18, align 4
  %869 = load i32, i32* %17, align 4
  %870 = load i32, i32* %18, align 4
  %871 = sub i32 %870, %869
  store i32 %871, i32* %18, align 4
  %872 = load i32, i32* %17, align 4
  %873 = lshr i32 %872, 12
  %874 = load i32, i32* %18, align 4
  %875 = xor i32 %874, %873
  store i32 %875, i32* %18, align 4
  %876 = load i32, i32* %17, align 4
  %877 = load i32, i32* %19, align 4
  %878 = sub i32 %877, %876
  store i32 %878, i32* %19, align 4
  %879 = load i32, i32* %18, align 4
  %880 = load i32, i32* %19, align 4
  %881 = sub i32 %880, %879
  store i32 %881, i32* %19, align 4
  %882 = load i32, i32* %18, align 4
  %883 = shl i32 %882, 16
  %884 = load i32, i32* %19, align 4
  %885 = xor i32 %884, %883
  store i32 %885, i32* %19, align 4
  %886 = load i32, i32* %18, align 4
  %887 = load i32, i32* %17, align 4
  %888 = sub i32 %887, %886
  store i32 %888, i32* %17, align 4
  %889 = load i32, i32* %19, align 4
  %890 = load i32, i32* %17, align 4
  %891 = sub i32 %890, %889
  store i32 %891, i32* %17, align 4
  %892 = load i32, i32* %19, align 4
  %893 = lshr i32 %892, 5
  %894 = load i32, i32* %17, align 4
  %895 = xor i32 %894, %893
  store i32 %895, i32* %17, align 4
  %896 = load i32, i32* %19, align 4
  %897 = load i32, i32* %18, align 4
  %898 = sub i32 %897, %896
  store i32 %898, i32* %18, align 4
  %899 = load i32, i32* %17, align 4
  %900 = load i32, i32* %18, align 4
  %901 = sub i32 %900, %899
  store i32 %901, i32* %18, align 4
  %902 = load i32, i32* %17, align 4
  %903 = lshr i32 %902, 3
  %904 = load i32, i32* %18, align 4
  %905 = xor i32 %904, %903
  store i32 %905, i32* %18, align 4
  %906 = load i32, i32* %17, align 4
  %907 = load i32, i32* %19, align 4
  %908 = sub i32 %907, %906
  store i32 %908, i32* %19, align 4
  %909 = load i32, i32* %18, align 4
  %910 = load i32, i32* %19, align 4
  %911 = sub i32 %910, %909
  store i32 %911, i32* %19, align 4
  %912 = load i32, i32* %18, align 4
  %913 = shl i32 %912, 10
  %914 = load i32, i32* %19, align 4
  %915 = xor i32 %914, %913
  store i32 %915, i32* %19, align 4
  %916 = load i32, i32* %18, align 4
  %917 = load i32, i32* %17, align 4
  %918 = sub i32 %917, %916
  store i32 %918, i32* %17, align 4
  %919 = load i32, i32* %19, align 4
  %920 = load i32, i32* %17, align 4
  %921 = sub i32 %920, %919
  store i32 %921, i32* %17, align 4
  %922 = load i32, i32* %19, align 4
  %923 = lshr i32 %922, 15
  %924 = load i32, i32* %17, align 4
  %925 = xor i32 %924, %923
  store i32 %925, i32* %17, align 4
  br label %926

; <label>:926:                                    ; preds = %835
  br label %927

; <label>:927:                                    ; preds = %926
  br label %928

; <label>:928:                                    ; preds = %927
  br label %929

; <label>:929:                                    ; preds = %928
  %930 = load i32, i32* %17, align 4
  %931 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %932 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %931, i32 0, i32 2
  %933 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %932, i32 0, i32 7
  store i32 %930, i32* %933, align 4
  %934 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %935 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %934, i32 0, i32 0
  %936 = bitcast i64* %935 to i8*
  %937 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %938 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %937, i32 0, i32 2
  %939 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %938, i32 0, i32 5
  store i8* %936, i8** %939, align 8
  %940 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %941 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %940, i32 0, i32 2
  %942 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %941, i32 0, i32 6
  store i32 8, i32* %942, align 8
  %943 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %944 = icmp ne %struct.my_hash* %943, null
  br i1 %944, label %1032, label %945

; <label>:945:                                    ; preds = %929
  %946 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %947 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %946, i32 0, i32 2
  %948 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %947, i32 0, i32 2
  store i8* null, i8** %948, align 8
  %949 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %950 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %949, i32 0, i32 2
  %951 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %950, i32 0, i32 1
  store i8* null, i8** %951, align 8
  br label %952

; <label>:952:                                    ; preds = %945
  %953 = call i8* @malloc(i64 64) #8
  %954 = bitcast i8* %953 to %struct.UT_hash_table*
  %955 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %956 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %955, i32 0, i32 2
  %957 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %956, i32 0, i32 0
  store %struct.UT_hash_table* %954, %struct.UT_hash_table** %957, align 8
  %958 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %959 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %958, i32 0, i32 2
  %960 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %959, i32 0, i32 0
  %961 = load %struct.UT_hash_table*, %struct.UT_hash_table** %960, align 8
  %962 = icmp ne %struct.UT_hash_table* %961, null
  br i1 %962, label %964, label %963

; <label>:963:                                    ; preds = %952
  call void @exit(i32 -1) #7
  unreachable

; <label>:964:                                    ; preds = %952
  %965 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %966 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %965, i32 0, i32 2
  %967 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %966, i32 0, i32 0
  %968 = load %struct.UT_hash_table*, %struct.UT_hash_table** %967, align 8
  %969 = bitcast %struct.UT_hash_table* %968 to i8*
  call void @llvm.memset.p0i8.i64(i8* %969, i8 0, i64 64, i32 8, i1 false)
  %970 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %971 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %970, i32 0, i32 2
  %972 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %973 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %972, i32 0, i32 2
  %974 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %973, i32 0, i32 0
  %975 = load %struct.UT_hash_table*, %struct.UT_hash_table** %974, align 8
  %976 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %975, i32 0, i32 4
  store %struct.UT_hash_handle* %971, %struct.UT_hash_handle** %976, align 8
  %977 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %978 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %977, i32 0, i32 2
  %979 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %978, i32 0, i32 0
  %980 = load %struct.UT_hash_table*, %struct.UT_hash_table** %979, align 8
  %981 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %980, i32 0, i32 1
  store i32 32, i32* %981, align 8
  %982 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %983 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %982, i32 0, i32 2
  %984 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %983, i32 0, i32 0
  %985 = load %struct.UT_hash_table*, %struct.UT_hash_table** %984, align 8
  %986 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %985, i32 0, i32 2
  store i32 5, i32* %986, align 4
  %987 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %988 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %987, i32 0, i32 2
  %989 = bitcast %struct.UT_hash_handle* %988 to i8*
  %990 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %991 = bitcast %struct.my_hash* %990 to i8*
  %992 = ptrtoint i8* %989 to i64
  %993 = ptrtoint i8* %991 to i64
  %994 = sub i64 %992, %993
  %995 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %996 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %995, i32 0, i32 2
  %997 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %996, i32 0, i32 0
  %998 = load %struct.UT_hash_table*, %struct.UT_hash_table** %997, align 8
  %999 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %998, i32 0, i32 5
  store i64 %994, i64* %999, align 8
  %1000 = call i8* @malloc(i64 512) #8
  %1001 = bitcast i8* %1000 to %struct.UT_hash_bucket*
  %1002 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1003 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1002, i32 0, i32 2
  %1004 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1003, i32 0, i32 0
  %1005 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1004, align 8
  %1006 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1005, i32 0, i32 0
  store %struct.UT_hash_bucket* %1001, %struct.UT_hash_bucket** %1006, align 8
  %1007 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1008 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1007, i32 0, i32 2
  %1009 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1008, i32 0, i32 0
  %1010 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1009, align 8
  %1011 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1010, i32 0, i32 10
  store i32 -1609490463, i32* %1011, align 8
  %1012 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1013 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1012, i32 0, i32 2
  %1014 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1013, i32 0, i32 0
  %1015 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1014, align 8
  %1016 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1015, i32 0, i32 0
  %1017 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1016, align 8
  %1018 = icmp ne %struct.UT_hash_bucket* %1017, null
  br i1 %1018, label %1020, label %1019

; <label>:1019:                                   ; preds = %964
  call void @exit(i32 -1) #7
  unreachable

; <label>:1020:                                   ; preds = %964
  %1021 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1022 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1021, i32 0, i32 2
  %1023 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1022, i32 0, i32 0
  %1024 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1023, align 8
  %1025 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1024, i32 0, i32 0
  %1026 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1025, align 8
  %1027 = bitcast %struct.UT_hash_bucket* %1026 to i8*
  call void @llvm.memset.p0i8.i64(i8* %1027, i8 0, i64 512, i32 8, i1 false)
  br label %1028

; <label>:1028:                                   ; preds = %1020
  br label %1029

; <label>:1029:                                   ; preds = %1028
  br label %1030

; <label>:1030:                                   ; preds = %1029
  %1031 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  store %struct.my_hash* %1031, %struct.my_hash** %3, align 8
  br label %1079

; <label>:1032:                                   ; preds = %929
  %1033 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1034 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1033, i32 0, i32 2
  %1035 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1034, i32 0, i32 0
  %1036 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1035, align 8
  %1037 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1038 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1037, i32 0, i32 2
  %1039 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1038, i32 0, i32 0
  store %struct.UT_hash_table* %1036, %struct.UT_hash_table** %1039, align 8
  br label %1040

; <label>:1040:                                   ; preds = %1032
  %1041 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1042 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1041, i32 0, i32 2
  %1043 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1042, i32 0, i32 2
  store i8* null, i8** %1043, align 8
  %1044 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1045 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1044, i32 0, i32 2
  %1046 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1045, i32 0, i32 0
  %1047 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1046, align 8
  %1048 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1047, i32 0, i32 4
  %1049 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1048, align 8
  %1050 = bitcast %struct.UT_hash_handle* %1049 to i8*
  %1051 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1052 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1051, i32 0, i32 2
  %1053 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1052, i32 0, i32 0
  %1054 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1053, align 8
  %1055 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1054, i32 0, i32 5
  %1056 = load i64, i64* %1055, align 8
  %1057 = sub i64 0, %1056
  %1058 = getelementptr inbounds i8, i8* %1050, i64 %1057
  %1059 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1060 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1059, i32 0, i32 2
  %1061 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1060, i32 0, i32 1
  store i8* %1058, i8** %1061, align 8
  %1062 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1063 = bitcast %struct.my_hash* %1062 to i8*
  %1064 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1065 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1064, i32 0, i32 2
  %1066 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1065, i32 0, i32 0
  %1067 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1066, align 8
  %1068 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1067, i32 0, i32 4
  %1069 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1068, align 8
  %1070 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1069, i32 0, i32 2
  store i8* %1063, i8** %1070, align 8
  %1071 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1072 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1071, i32 0, i32 2
  %1073 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1074 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1073, i32 0, i32 2
  %1075 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1074, i32 0, i32 0
  %1076 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1075, align 8
  %1077 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1076, i32 0, i32 4
  store %struct.UT_hash_handle* %1072, %struct.UT_hash_handle** %1077, align 8
  br label %1078

; <label>:1078:                                   ; preds = %1040
  br label %1079

; <label>:1079:                                   ; preds = %1078, %1030
  br label %1080

; <label>:1080:                                   ; preds = %1079
  %1081 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1082 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1081, i32 0, i32 2
  %1083 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1082, i32 0, i32 0
  %1084 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1083, align 8
  %1085 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1084, i32 0, i32 3
  %1086 = load i32, i32* %1085, align 8
  %1087 = add i32 %1086, 1
  store i32 %1087, i32* %1085, align 8
  br label %1088

; <label>:1088:                                   ; preds = %1080
  %1089 = load i32, i32* %17, align 4
  %1090 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1091 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1090, i32 0, i32 2
  %1092 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1091, i32 0, i32 0
  %1093 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1092, align 8
  %1094 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1093, i32 0, i32 1
  %1095 = load i32, i32* %1094, align 8
  %1096 = sub i32 %1095, 1
  %1097 = and i32 %1089, %1096
  store i32 %1097, i32* %22, align 4
  br label %1098

; <label>:1098:                                   ; preds = %1088
  br label %1099

; <label>:1099:                                   ; preds = %1098
  %1100 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1101 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1100, i32 0, i32 2
  %1102 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1101, i32 0, i32 0
  %1103 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1102, align 8
  %1104 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1103, i32 0, i32 0
  %1105 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1104, align 8
  %1106 = load i32, i32* %22, align 4
  %1107 = zext i32 %1106 to i64
  %1108 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1105, i64 %1107
  store %struct.UT_hash_bucket* %1108, %struct.UT_hash_bucket** %23, align 8
  %1109 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %23, align 8
  %1110 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1109, i32 0, i32 1
  %1111 = load i32, i32* %1110, align 8
  %1112 = add i32 %1111, 1
  store i32 %1112, i32* %1110, align 8
  %1113 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %23, align 8
  %1114 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1113, i32 0, i32 0
  %1115 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1114, align 8
  %1116 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1117 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1116, i32 0, i32 2
  %1118 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1117, i32 0, i32 4
  store %struct.UT_hash_handle* %1115, %struct.UT_hash_handle** %1118, align 8
  %1119 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1120 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1119, i32 0, i32 2
  %1121 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1120, i32 0, i32 3
  store %struct.UT_hash_handle* null, %struct.UT_hash_handle** %1121, align 8
  %1122 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %23, align 8
  %1123 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1122, i32 0, i32 0
  %1124 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1123, align 8
  %1125 = icmp ne %struct.UT_hash_handle* %1124, null
  br i1 %1125, label %1126, label %1133

; <label>:1126:                                   ; preds = %1099
  %1127 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1128 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1127, i32 0, i32 2
  %1129 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %23, align 8
  %1130 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1129, i32 0, i32 0
  %1131 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1130, align 8
  %1132 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1131, i32 0, i32 3
  store %struct.UT_hash_handle* %1128, %struct.UT_hash_handle** %1132, align 8
  br label %1133

; <label>:1133:                                   ; preds = %1126, %1099
  %1134 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1135 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1134, i32 0, i32 2
  %1136 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %23, align 8
  %1137 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1136, i32 0, i32 0
  store %struct.UT_hash_handle* %1135, %struct.UT_hash_handle** %1137, align 8
  %1138 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %23, align 8
  %1139 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1138, i32 0, i32 1
  %1140 = load i32, i32* %1139, align 8
  %1141 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %23, align 8
  %1142 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1141, i32 0, i32 2
  %1143 = load i32, i32* %1142, align 4
  %1144 = add i32 %1143, 1
  %1145 = mul i32 %1144, 10
  %1146 = icmp uge i32 %1140, %1145
  br i1 %1146, label %1147, label %1404

; <label>:1147:                                   ; preds = %1133
  %1148 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1149 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1148, i32 0, i32 2
  %1150 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1149, i32 0, i32 0
  %1151 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1150, align 8
  %1152 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1151, i32 0, i32 9
  %1153 = load i32, i32* %1152, align 4
  %1154 = icmp ne i32 %1153, 0
  br i1 %1154, label %1404, label %1155

; <label>:1155:                                   ; preds = %1147
  br label %1156

; <label>:1156:                                   ; preds = %1155
  %1157 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1158 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1157, i32 0, i32 2
  %1159 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1158, i32 0, i32 0
  %1160 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1159, align 8
  %1161 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1160, i32 0, i32 1
  %1162 = load i32, i32* %1161, align 8
  %1163 = zext i32 %1162 to i64
  %1164 = mul i64 2, %1163
  %1165 = mul i64 %1164, 16
  %1166 = call i8* @malloc(i64 %1165) #8
  %1167 = bitcast i8* %1166 to %struct.UT_hash_bucket*
  store %struct.UT_hash_bucket* %1167, %struct.UT_hash_bucket** %28, align 8
  %1168 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1169 = icmp ne %struct.UT_hash_bucket* %1168, null
  br i1 %1169, label %1171, label %1170

; <label>:1170:                                   ; preds = %1156
  call void @exit(i32 -1) #7
  unreachable

; <label>:1171:                                   ; preds = %1156
  %1172 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1173 = bitcast %struct.UT_hash_bucket* %1172 to i8*
  %1174 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1175 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1174, i32 0, i32 2
  %1176 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1175, i32 0, i32 0
  %1177 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1176, align 8
  %1178 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1177, i32 0, i32 1
  %1179 = load i32, i32* %1178, align 8
  %1180 = zext i32 %1179 to i64
  %1181 = mul i64 2, %1180
  %1182 = mul i64 %1181, 16
  call void @llvm.memset.p0i8.i64(i8* %1173, i8 0, i64 %1182, i32 8, i1 false)
  %1183 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1184 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1183, i32 0, i32 2
  %1185 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1184, i32 0, i32 0
  %1186 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1185, align 8
  %1187 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1186, i32 0, i32 3
  %1188 = load i32, i32* %1187, align 8
  %1189 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1190 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1189, i32 0, i32 2
  %1191 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1190, i32 0, i32 0
  %1192 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1191, align 8
  %1193 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1192, i32 0, i32 2
  %1194 = load i32, i32* %1193, align 4
  %1195 = add i32 %1194, 1
  %1196 = lshr i32 %1188, %1195
  %1197 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1198 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1197, i32 0, i32 2
  %1199 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1198, i32 0, i32 0
  %1200 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1199, align 8
  %1201 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1200, i32 0, i32 3
  %1202 = load i32, i32* %1201, align 8
  %1203 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1204 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1203, i32 0, i32 2
  %1205 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1204, i32 0, i32 0
  %1206 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1205, align 8
  %1207 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1206, i32 0, i32 1
  %1208 = load i32, i32* %1207, align 8
  %1209 = mul i32 %1208, 2
  %1210 = sub i32 %1209, 1
  %1211 = and i32 %1202, %1210
  %1212 = icmp ne i32 %1211, 0
  %1213 = zext i1 %1212 to i64
  %1214 = select i1 %1212, i32 1, i32 0
  %1215 = add i32 %1196, %1214
  %1216 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1217 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1216, i32 0, i32 2
  %1218 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1217, i32 0, i32 0
  %1219 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1218, align 8
  %1220 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1219, i32 0, i32 6
  store i32 %1215, i32* %1220, align 8
  %1221 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1222 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1221, i32 0, i32 2
  %1223 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1222, i32 0, i32 0
  %1224 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1223, align 8
  %1225 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1224, i32 0, i32 7
  store i32 0, i32* %1225, align 4
  store i32 0, i32* %25, align 4
  br label %1226

; <label>:1226:                                   ; preds = %1327, %1171
  %1227 = load i32, i32* %25, align 4
  %1228 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1229 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1228, i32 0, i32 2
  %1230 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1229, i32 0, i32 0
  %1231 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1230, align 8
  %1232 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1231, i32 0, i32 1
  %1233 = load i32, i32* %1232, align 8
  %1234 = icmp ult i32 %1227, %1233
  br i1 %1234, label %1235, label %1330

; <label>:1235:                                   ; preds = %1226
  %1236 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1237 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1236, i32 0, i32 2
  %1238 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1237, i32 0, i32 0
  %1239 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1238, align 8
  %1240 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1239, i32 0, i32 0
  %1241 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1240, align 8
  %1242 = load i32, i32* %25, align 4
  %1243 = zext i32 %1242 to i64
  %1244 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1241, i64 %1243
  %1245 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1244, i32 0, i32 0
  %1246 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1245, align 8
  store %struct.UT_hash_handle* %1246, %struct.UT_hash_handle** %26, align 8
  br label %1247

; <label>:1247:                                   ; preds = %1321, %1235
  %1248 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  %1249 = icmp ne %struct.UT_hash_handle* %1248, null
  br i1 %1249, label %1250, label %1326

; <label>:1250:                                   ; preds = %1247
  %1251 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  %1252 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1251, i32 0, i32 4
  %1253 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1252, align 8
  store %struct.UT_hash_handle* %1253, %struct.UT_hash_handle** %27, align 8
  br label %1254

; <label>:1254:                                   ; preds = %1250
  %1255 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  %1256 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1255, i32 0, i32 7
  %1257 = load i32, i32* %1256, align 4
  %1258 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1259 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1258, i32 0, i32 2
  %1260 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1259, i32 0, i32 0
  %1261 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1260, align 8
  %1262 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1261, i32 0, i32 1
  %1263 = load i32, i32* %1262, align 8
  %1264 = mul i32 %1263, 2
  %1265 = sub i32 %1264, 1
  %1266 = and i32 %1257, %1265
  store i32 %1266, i32* %24, align 4
  br label %1267

; <label>:1267:                                   ; preds = %1254
  %1268 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1269 = load i32, i32* %24, align 4
  %1270 = zext i32 %1269 to i64
  %1271 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1268, i64 %1270
  store %struct.UT_hash_bucket* %1271, %struct.UT_hash_bucket** %29, align 8
  %1272 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %29, align 8
  %1273 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1272, i32 0, i32 1
  %1274 = load i32, i32* %1273, align 8
  %1275 = add i32 %1274, 1
  store i32 %1275, i32* %1273, align 8
  %1276 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1277 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1276, i32 0, i32 2
  %1278 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1277, i32 0, i32 0
  %1279 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1278, align 8
  %1280 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1279, i32 0, i32 6
  %1281 = load i32, i32* %1280, align 8
  %1282 = icmp ugt i32 %1275, %1281
  br i1 %1282, label %1283, label %1303

; <label>:1283:                                   ; preds = %1267
  %1284 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1285 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1284, i32 0, i32 2
  %1286 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1285, i32 0, i32 0
  %1287 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1286, align 8
  %1288 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1287, i32 0, i32 7
  %1289 = load i32, i32* %1288, align 4
  %1290 = add i32 %1289, 1
  store i32 %1290, i32* %1288, align 4
  %1291 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %29, align 8
  %1292 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1291, i32 0, i32 1
  %1293 = load i32, i32* %1292, align 8
  %1294 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1295 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1294, i32 0, i32 2
  %1296 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1295, i32 0, i32 0
  %1297 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1296, align 8
  %1298 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1297, i32 0, i32 6
  %1299 = load i32, i32* %1298, align 8
  %1300 = udiv i32 %1293, %1299
  %1301 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %29, align 8
  %1302 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1301, i32 0, i32 2
  store i32 %1300, i32* %1302, align 4
  br label %1303

; <label>:1303:                                   ; preds = %1283, %1267
  %1304 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  %1305 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1304, i32 0, i32 3
  store %struct.UT_hash_handle* null, %struct.UT_hash_handle** %1305, align 8
  %1306 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %29, align 8
  %1307 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1306, i32 0, i32 0
  %1308 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1307, align 8
  %1309 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  %1310 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1309, i32 0, i32 4
  store %struct.UT_hash_handle* %1308, %struct.UT_hash_handle** %1310, align 8
  %1311 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %29, align 8
  %1312 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1311, i32 0, i32 0
  %1313 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1312, align 8
  %1314 = icmp ne %struct.UT_hash_handle* %1313, null
  br i1 %1314, label %1315, label %1321

; <label>:1315:                                   ; preds = %1303
  %1316 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  %1317 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %29, align 8
  %1318 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1317, i32 0, i32 0
  %1319 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1318, align 8
  %1320 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1319, i32 0, i32 3
  store %struct.UT_hash_handle* %1316, %struct.UT_hash_handle** %1320, align 8
  br label %1321

; <label>:1321:                                   ; preds = %1315, %1303
  %1322 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  %1323 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %29, align 8
  %1324 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1323, i32 0, i32 0
  store %struct.UT_hash_handle* %1322, %struct.UT_hash_handle** %1324, align 8
  %1325 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %27, align 8
  store %struct.UT_hash_handle* %1325, %struct.UT_hash_handle** %26, align 8
  br label %1247

; <label>:1326:                                   ; preds = %1247
  br label %1327

; <label>:1327:                                   ; preds = %1326
  %1328 = load i32, i32* %25, align 4
  %1329 = add i32 %1328, 1
  store i32 %1329, i32* %25, align 4
  br label %1226

; <label>:1330:                                   ; preds = %1226
  %1331 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1332 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1331, i32 0, i32 2
  %1333 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1332, i32 0, i32 0
  %1334 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1333, align 8
  %1335 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1334, i32 0, i32 0
  %1336 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1335, align 8
  %1337 = bitcast %struct.UT_hash_bucket* %1336 to i8*
  call void @free(i8* %1337)
  %1338 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1339 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1338, i32 0, i32 2
  %1340 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1339, i32 0, i32 0
  %1341 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1340, align 8
  %1342 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1341, i32 0, i32 1
  %1343 = load i32, i32* %1342, align 8
  %1344 = mul i32 %1343, 2
  store i32 %1344, i32* %1342, align 8
  %1345 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1346 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1345, i32 0, i32 2
  %1347 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1346, i32 0, i32 0
  %1348 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1347, align 8
  %1349 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1348, i32 0, i32 2
  %1350 = load i32, i32* %1349, align 4
  %1351 = add i32 %1350, 1
  store i32 %1351, i32* %1349, align 4
  %1352 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1353 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1354 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1353, i32 0, i32 2
  %1355 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1354, i32 0, i32 0
  %1356 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1355, align 8
  %1357 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1356, i32 0, i32 0
  store %struct.UT_hash_bucket* %1352, %struct.UT_hash_bucket** %1357, align 8
  %1358 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1359 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1358, i32 0, i32 2
  %1360 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1359, i32 0, i32 0
  %1361 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1360, align 8
  %1362 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1361, i32 0, i32 7
  %1363 = load i32, i32* %1362, align 4
  %1364 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1365 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1364, i32 0, i32 2
  %1366 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1365, i32 0, i32 0
  %1367 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1366, align 8
  %1368 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1367, i32 0, i32 3
  %1369 = load i32, i32* %1368, align 8
  %1370 = lshr i32 %1369, 1
  %1371 = icmp ugt i32 %1363, %1370
  br i1 %1371, label %1372, label %1380

; <label>:1372:                                   ; preds = %1330
  %1373 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1374 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1373, i32 0, i32 2
  %1375 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1374, i32 0, i32 0
  %1376 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1375, align 8
  %1377 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1376, i32 0, i32 8
  %1378 = load i32, i32* %1377, align 8
  %1379 = add i32 %1378, 1
  br label %1381

; <label>:1380:                                   ; preds = %1330
  br label %1381

; <label>:1381:                                   ; preds = %1380, %1372
  %1382 = phi i32 [ %1379, %1372 ], [ 0, %1380 ]
  %1383 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1384 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1383, i32 0, i32 2
  %1385 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1384, i32 0, i32 0
  %1386 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1385, align 8
  %1387 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1386, i32 0, i32 8
  store i32 %1382, i32* %1387, align 8
  %1388 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1389 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1388, i32 0, i32 2
  %1390 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1389, i32 0, i32 0
  %1391 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1390, align 8
  %1392 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1391, i32 0, i32 8
  %1393 = load i32, i32* %1392, align 8
  %1394 = icmp ugt i32 %1393, 1
  br i1 %1394, label %1395, label %1401

; <label>:1395:                                   ; preds = %1381
  %1396 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1397 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1396, i32 0, i32 2
  %1398 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1397, i32 0, i32 0
  %1399 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1398, align 8
  %1400 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1399, i32 0, i32 9
  store i32 1, i32* %1400, align 4
  br label %1401

; <label>:1401:                                   ; preds = %1395, %1381
  br label %1402

; <label>:1402:                                   ; preds = %1401
  br label %1403

; <label>:1403:                                   ; preds = %1402
  br label %1404

; <label>:1404:                                   ; preds = %1403, %1147, %1133
  br label %1405

; <label>:1405:                                   ; preds = %1404
  br label %1406

; <label>:1406:                                   ; preds = %1405
  br label %1407

; <label>:1407:                                   ; preds = %1406
  br label %1408

; <label>:1408:                                   ; preds = %1407
  br label %1413

; <label>:1409:                                   ; preds = %547
  %1410 = load i64, i64* %9, align 8
  %1411 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1412 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1411, i32 0, i32 1
  store i64 %1410, i64* %1412, align 8
  br label %1413

; <label>:1413:                                   ; preds = %1409, %1408
  br label %1414

; <label>:1414:                                   ; preds = %1413
  %1415 = load i64, i64* %8, align 8
  %1416 = add i64 %1415, 1
  store i64 %1416, i64* %8, align 8
  br label %65

; <label>:1417:                                   ; preds = %65
  %1418 = load i64*, i64** %5, align 8
  %1419 = icmp eq i64* %1418, null
  br i1 %1419, label %1422, label %1420

; <label>:1420:                                   ; preds = %1417
  %1421 = bitcast i64* %1418 to i8*
  call void @_ZdaPv(i8* %1421) #9
  br label %1422

; <label>:1422:                                   ; preds = %1420, %1417
  %1423 = load i64, i64* @MEMCOUNT, align 8
  %1424 = sub i64 %1423, 4096
  store i64 %1424, i64* @MEMCOUNT, align 8
  %1425 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1426 = ptrtoint %struct.my_hash* %1425 to i64
  %1427 = or i64 %1426, 6
  %1428 = load i64*, i64** %7, align 8
  %1429 = getelementptr inbounds i64, i64* %1428, i64 1
  store i64 %1427, i64* %1429, align 8
  %1430 = load i64*, i64** %7, align 8
  %1431 = ptrtoint i64* %1430 to i64
  %1432 = or i64 %1431, 6
  ret i64 %1432
}

declare i32 @memcmp(i8*, i8*, i64) #1

; Function Attrs: argmemonly nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i32, i1) #6

declare void @free(i8*) #1

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_make_45hash(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_make_45hash(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_hash_45ref(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64*, align 8
  %7 = alloca %struct.my_hash*, align 8
  %8 = alloca %struct.my_hash*, align 8
  %9 = alloca i32, align 4
  %10 = alloca i32, align 4
  %11 = alloca i32, align 4
  %12 = alloca i32, align 4
  %13 = alloca i8*, align 8
  %14 = alloca i32, align 4
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  %15 = load i64, i64* %4, align 8
  %16 = and i64 %15, 7
  %17 = icmp ne i64 %16, 6
  br i1 %17, label %25, label %18

; <label>:18:                                     ; preds = %2
  %19 = load i64, i64* %4, align 8
  %20 = and i64 %19, -8
  %21 = inttoptr i64 %20 to i64*
  %22 = getelementptr inbounds i64, i64* %21, i64 0
  %23 = load i64, i64* %22, align 8
  %24 = icmp ne i64 2, %23
  br i1 %24, label %25, label %26

; <label>:25:                                     ; preds = %18, %2
  call void @fatal_err(i8* getelementptr inbounds ([59 x i8], [59 x i8]* @.str.60, i32 0, i32 0))
  br label %26

; <label>:26:                                     ; preds = %25, %18
  %27 = load i64, i64* %4, align 8
  %28 = and i64 %27, -8
  %29 = inttoptr i64 %28 to i64*
  store i64* %29, i64** %6, align 8
  %30 = load i64*, i64** %6, align 8
  %31 = getelementptr inbounds i64, i64* %30, i64 1
  %32 = load i64, i64* %31, align 8
  %33 = and i64 %32, -8
  %34 = inttoptr i64 %33 to i64*
  %35 = bitcast i64* %34 to %struct.my_hash*
  store %struct.my_hash* %35, %struct.my_hash** %8, align 8
  br label %36

; <label>:36:                                     ; preds = %26
  br label %37

; <label>:37:                                     ; preds = %36
  br label %38

; <label>:38:                                     ; preds = %37
  %39 = bitcast i64* %5 to i8*
  store i8* %39, i8** %13, align 8
  store i32 -17973521, i32* %9, align 4
  store i32 -1640531527, i32* %11, align 4
  store i32 -1640531527, i32* %10, align 4
  store i32 8, i32* %12, align 4
  br label %40

; <label>:40:                                     ; preds = %207, %38
  %41 = load i32, i32* %12, align 4
  %42 = icmp uge i32 %41, 12
  br i1 %42, label %43, label %212

; <label>:43:                                     ; preds = %40
  %44 = load i8*, i8** %13, align 8
  %45 = getelementptr inbounds i8, i8* %44, i64 0
  %46 = load i8, i8* %45, align 1
  %47 = zext i8 %46 to i32
  %48 = load i8*, i8** %13, align 8
  %49 = getelementptr inbounds i8, i8* %48, i64 1
  %50 = load i8, i8* %49, align 1
  %51 = zext i8 %50 to i32
  %52 = shl i32 %51, 8
  %53 = add i32 %47, %52
  %54 = load i8*, i8** %13, align 8
  %55 = getelementptr inbounds i8, i8* %54, i64 2
  %56 = load i8, i8* %55, align 1
  %57 = zext i8 %56 to i32
  %58 = shl i32 %57, 16
  %59 = add i32 %53, %58
  %60 = load i8*, i8** %13, align 8
  %61 = getelementptr inbounds i8, i8* %60, i64 3
  %62 = load i8, i8* %61, align 1
  %63 = zext i8 %62 to i32
  %64 = shl i32 %63, 24
  %65 = add i32 %59, %64
  %66 = load i32, i32* %10, align 4
  %67 = add i32 %66, %65
  store i32 %67, i32* %10, align 4
  %68 = load i8*, i8** %13, align 8
  %69 = getelementptr inbounds i8, i8* %68, i64 4
  %70 = load i8, i8* %69, align 1
  %71 = zext i8 %70 to i32
  %72 = load i8*, i8** %13, align 8
  %73 = getelementptr inbounds i8, i8* %72, i64 5
  %74 = load i8, i8* %73, align 1
  %75 = zext i8 %74 to i32
  %76 = shl i32 %75, 8
  %77 = add i32 %71, %76
  %78 = load i8*, i8** %13, align 8
  %79 = getelementptr inbounds i8, i8* %78, i64 6
  %80 = load i8, i8* %79, align 1
  %81 = zext i8 %80 to i32
  %82 = shl i32 %81, 16
  %83 = add i32 %77, %82
  %84 = load i8*, i8** %13, align 8
  %85 = getelementptr inbounds i8, i8* %84, i64 7
  %86 = load i8, i8* %85, align 1
  %87 = zext i8 %86 to i32
  %88 = shl i32 %87, 24
  %89 = add i32 %83, %88
  %90 = load i32, i32* %11, align 4
  %91 = add i32 %90, %89
  store i32 %91, i32* %11, align 4
  %92 = load i8*, i8** %13, align 8
  %93 = getelementptr inbounds i8, i8* %92, i64 8
  %94 = load i8, i8* %93, align 1
  %95 = zext i8 %94 to i32
  %96 = load i8*, i8** %13, align 8
  %97 = getelementptr inbounds i8, i8* %96, i64 9
  %98 = load i8, i8* %97, align 1
  %99 = zext i8 %98 to i32
  %100 = shl i32 %99, 8
  %101 = add i32 %95, %100
  %102 = load i8*, i8** %13, align 8
  %103 = getelementptr inbounds i8, i8* %102, i64 10
  %104 = load i8, i8* %103, align 1
  %105 = zext i8 %104 to i32
  %106 = shl i32 %105, 16
  %107 = add i32 %101, %106
  %108 = load i8*, i8** %13, align 8
  %109 = getelementptr inbounds i8, i8* %108, i64 11
  %110 = load i8, i8* %109, align 1
  %111 = zext i8 %110 to i32
  %112 = shl i32 %111, 24
  %113 = add i32 %107, %112
  %114 = load i32, i32* %9, align 4
  %115 = add i32 %114, %113
  store i32 %115, i32* %9, align 4
  br label %116

; <label>:116:                                    ; preds = %43
  %117 = load i32, i32* %11, align 4
  %118 = load i32, i32* %10, align 4
  %119 = sub i32 %118, %117
  store i32 %119, i32* %10, align 4
  %120 = load i32, i32* %9, align 4
  %121 = load i32, i32* %10, align 4
  %122 = sub i32 %121, %120
  store i32 %122, i32* %10, align 4
  %123 = load i32, i32* %9, align 4
  %124 = lshr i32 %123, 13
  %125 = load i32, i32* %10, align 4
  %126 = xor i32 %125, %124
  store i32 %126, i32* %10, align 4
  %127 = load i32, i32* %9, align 4
  %128 = load i32, i32* %11, align 4
  %129 = sub i32 %128, %127
  store i32 %129, i32* %11, align 4
  %130 = load i32, i32* %10, align 4
  %131 = load i32, i32* %11, align 4
  %132 = sub i32 %131, %130
  store i32 %132, i32* %11, align 4
  %133 = load i32, i32* %10, align 4
  %134 = shl i32 %133, 8
  %135 = load i32, i32* %11, align 4
  %136 = xor i32 %135, %134
  store i32 %136, i32* %11, align 4
  %137 = load i32, i32* %10, align 4
  %138 = load i32, i32* %9, align 4
  %139 = sub i32 %138, %137
  store i32 %139, i32* %9, align 4
  %140 = load i32, i32* %11, align 4
  %141 = load i32, i32* %9, align 4
  %142 = sub i32 %141, %140
  store i32 %142, i32* %9, align 4
  %143 = load i32, i32* %11, align 4
  %144 = lshr i32 %143, 13
  %145 = load i32, i32* %9, align 4
  %146 = xor i32 %145, %144
  store i32 %146, i32* %9, align 4
  %147 = load i32, i32* %11, align 4
  %148 = load i32, i32* %10, align 4
  %149 = sub i32 %148, %147
  store i32 %149, i32* %10, align 4
  %150 = load i32, i32* %9, align 4
  %151 = load i32, i32* %10, align 4
  %152 = sub i32 %151, %150
  store i32 %152, i32* %10, align 4
  %153 = load i32, i32* %9, align 4
  %154 = lshr i32 %153, 12
  %155 = load i32, i32* %10, align 4
  %156 = xor i32 %155, %154
  store i32 %156, i32* %10, align 4
  %157 = load i32, i32* %9, align 4
  %158 = load i32, i32* %11, align 4
  %159 = sub i32 %158, %157
  store i32 %159, i32* %11, align 4
  %160 = load i32, i32* %10, align 4
  %161 = load i32, i32* %11, align 4
  %162 = sub i32 %161, %160
  store i32 %162, i32* %11, align 4
  %163 = load i32, i32* %10, align 4
  %164 = shl i32 %163, 16
  %165 = load i32, i32* %11, align 4
  %166 = xor i32 %165, %164
  store i32 %166, i32* %11, align 4
  %167 = load i32, i32* %10, align 4
  %168 = load i32, i32* %9, align 4
  %169 = sub i32 %168, %167
  store i32 %169, i32* %9, align 4
  %170 = load i32, i32* %11, align 4
  %171 = load i32, i32* %9, align 4
  %172 = sub i32 %171, %170
  store i32 %172, i32* %9, align 4
  %173 = load i32, i32* %11, align 4
  %174 = lshr i32 %173, 5
  %175 = load i32, i32* %9, align 4
  %176 = xor i32 %175, %174
  store i32 %176, i32* %9, align 4
  %177 = load i32, i32* %11, align 4
  %178 = load i32, i32* %10, align 4
  %179 = sub i32 %178, %177
  store i32 %179, i32* %10, align 4
  %180 = load i32, i32* %9, align 4
  %181 = load i32, i32* %10, align 4
  %182 = sub i32 %181, %180
  store i32 %182, i32* %10, align 4
  %183 = load i32, i32* %9, align 4
  %184 = lshr i32 %183, 3
  %185 = load i32, i32* %10, align 4
  %186 = xor i32 %185, %184
  store i32 %186, i32* %10, align 4
  %187 = load i32, i32* %9, align 4
  %188 = load i32, i32* %11, align 4
  %189 = sub i32 %188, %187
  store i32 %189, i32* %11, align 4
  %190 = load i32, i32* %10, align 4
  %191 = load i32, i32* %11, align 4
  %192 = sub i32 %191, %190
  store i32 %192, i32* %11, align 4
  %193 = load i32, i32* %10, align 4
  %194 = shl i32 %193, 10
  %195 = load i32, i32* %11, align 4
  %196 = xor i32 %195, %194
  store i32 %196, i32* %11, align 4
  %197 = load i32, i32* %10, align 4
  %198 = load i32, i32* %9, align 4
  %199 = sub i32 %198, %197
  store i32 %199, i32* %9, align 4
  %200 = load i32, i32* %11, align 4
  %201 = load i32, i32* %9, align 4
  %202 = sub i32 %201, %200
  store i32 %202, i32* %9, align 4
  %203 = load i32, i32* %11, align 4
  %204 = lshr i32 %203, 15
  %205 = load i32, i32* %9, align 4
  %206 = xor i32 %205, %204
  store i32 %206, i32* %9, align 4
  br label %207

; <label>:207:                                    ; preds = %116
  %208 = load i8*, i8** %13, align 8
  %209 = getelementptr inbounds i8, i8* %208, i64 12
  store i8* %209, i8** %13, align 8
  %210 = load i32, i32* %12, align 4
  %211 = sub i32 %210, 12
  store i32 %211, i32* %12, align 4
  br label %40

; <label>:212:                                    ; preds = %40
  %213 = load i32, i32* %9, align 4
  %214 = add i32 %213, 8
  store i32 %214, i32* %9, align 4
  %215 = load i32, i32* %12, align 4
  switch i32 %215, label %302 [
    i32 11, label %216
    i32 10, label %224
    i32 9, label %232
    i32 8, label %240
    i32 7, label %248
    i32 6, label %256
    i32 5, label %264
    i32 4, label %271
    i32 3, label %279
    i32 2, label %287
    i32 1, label %295
  ]

; <label>:216:                                    ; preds = %212
  %217 = load i8*, i8** %13, align 8
  %218 = getelementptr inbounds i8, i8* %217, i64 10
  %219 = load i8, i8* %218, align 1
  %220 = zext i8 %219 to i32
  %221 = shl i32 %220, 24
  %222 = load i32, i32* %9, align 4
  %223 = add i32 %222, %221
  store i32 %223, i32* %9, align 4
  br label %224

; <label>:224:                                    ; preds = %212, %216
  %225 = load i8*, i8** %13, align 8
  %226 = getelementptr inbounds i8, i8* %225, i64 9
  %227 = load i8, i8* %226, align 1
  %228 = zext i8 %227 to i32
  %229 = shl i32 %228, 16
  %230 = load i32, i32* %9, align 4
  %231 = add i32 %230, %229
  store i32 %231, i32* %9, align 4
  br label %232

; <label>:232:                                    ; preds = %212, %224
  %233 = load i8*, i8** %13, align 8
  %234 = getelementptr inbounds i8, i8* %233, i64 8
  %235 = load i8, i8* %234, align 1
  %236 = zext i8 %235 to i32
  %237 = shl i32 %236, 8
  %238 = load i32, i32* %9, align 4
  %239 = add i32 %238, %237
  store i32 %239, i32* %9, align 4
  br label %240

; <label>:240:                                    ; preds = %212, %232
  %241 = load i8*, i8** %13, align 8
  %242 = getelementptr inbounds i8, i8* %241, i64 7
  %243 = load i8, i8* %242, align 1
  %244 = zext i8 %243 to i32
  %245 = shl i32 %244, 24
  %246 = load i32, i32* %11, align 4
  %247 = add i32 %246, %245
  store i32 %247, i32* %11, align 4
  br label %248

; <label>:248:                                    ; preds = %212, %240
  %249 = load i8*, i8** %13, align 8
  %250 = getelementptr inbounds i8, i8* %249, i64 6
  %251 = load i8, i8* %250, align 1
  %252 = zext i8 %251 to i32
  %253 = shl i32 %252, 16
  %254 = load i32, i32* %11, align 4
  %255 = add i32 %254, %253
  store i32 %255, i32* %11, align 4
  br label %256

; <label>:256:                                    ; preds = %212, %248
  %257 = load i8*, i8** %13, align 8
  %258 = getelementptr inbounds i8, i8* %257, i64 5
  %259 = load i8, i8* %258, align 1
  %260 = zext i8 %259 to i32
  %261 = shl i32 %260, 8
  %262 = load i32, i32* %11, align 4
  %263 = add i32 %262, %261
  store i32 %263, i32* %11, align 4
  br label %264

; <label>:264:                                    ; preds = %212, %256
  %265 = load i8*, i8** %13, align 8
  %266 = getelementptr inbounds i8, i8* %265, i64 4
  %267 = load i8, i8* %266, align 1
  %268 = zext i8 %267 to i32
  %269 = load i32, i32* %11, align 4
  %270 = add i32 %269, %268
  store i32 %270, i32* %11, align 4
  br label %271

; <label>:271:                                    ; preds = %212, %264
  %272 = load i8*, i8** %13, align 8
  %273 = getelementptr inbounds i8, i8* %272, i64 3
  %274 = load i8, i8* %273, align 1
  %275 = zext i8 %274 to i32
  %276 = shl i32 %275, 24
  %277 = load i32, i32* %10, align 4
  %278 = add i32 %277, %276
  store i32 %278, i32* %10, align 4
  br label %279

; <label>:279:                                    ; preds = %212, %271
  %280 = load i8*, i8** %13, align 8
  %281 = getelementptr inbounds i8, i8* %280, i64 2
  %282 = load i8, i8* %281, align 1
  %283 = zext i8 %282 to i32
  %284 = shl i32 %283, 16
  %285 = load i32, i32* %10, align 4
  %286 = add i32 %285, %284
  store i32 %286, i32* %10, align 4
  br label %287

; <label>:287:                                    ; preds = %212, %279
  %288 = load i8*, i8** %13, align 8
  %289 = getelementptr inbounds i8, i8* %288, i64 1
  %290 = load i8, i8* %289, align 1
  %291 = zext i8 %290 to i32
  %292 = shl i32 %291, 8
  %293 = load i32, i32* %10, align 4
  %294 = add i32 %293, %292
  store i32 %294, i32* %10, align 4
  br label %295

; <label>:295:                                    ; preds = %212, %287
  %296 = load i8*, i8** %13, align 8
  %297 = getelementptr inbounds i8, i8* %296, i64 0
  %298 = load i8, i8* %297, align 1
  %299 = zext i8 %298 to i32
  %300 = load i32, i32* %10, align 4
  %301 = add i32 %300, %299
  store i32 %301, i32* %10, align 4
  br label %302

; <label>:302:                                    ; preds = %295, %212
  br label %303

; <label>:303:                                    ; preds = %302
  %304 = load i32, i32* %11, align 4
  %305 = load i32, i32* %10, align 4
  %306 = sub i32 %305, %304
  store i32 %306, i32* %10, align 4
  %307 = load i32, i32* %9, align 4
  %308 = load i32, i32* %10, align 4
  %309 = sub i32 %308, %307
  store i32 %309, i32* %10, align 4
  %310 = load i32, i32* %9, align 4
  %311 = lshr i32 %310, 13
  %312 = load i32, i32* %10, align 4
  %313 = xor i32 %312, %311
  store i32 %313, i32* %10, align 4
  %314 = load i32, i32* %9, align 4
  %315 = load i32, i32* %11, align 4
  %316 = sub i32 %315, %314
  store i32 %316, i32* %11, align 4
  %317 = load i32, i32* %10, align 4
  %318 = load i32, i32* %11, align 4
  %319 = sub i32 %318, %317
  store i32 %319, i32* %11, align 4
  %320 = load i32, i32* %10, align 4
  %321 = shl i32 %320, 8
  %322 = load i32, i32* %11, align 4
  %323 = xor i32 %322, %321
  store i32 %323, i32* %11, align 4
  %324 = load i32, i32* %10, align 4
  %325 = load i32, i32* %9, align 4
  %326 = sub i32 %325, %324
  store i32 %326, i32* %9, align 4
  %327 = load i32, i32* %11, align 4
  %328 = load i32, i32* %9, align 4
  %329 = sub i32 %328, %327
  store i32 %329, i32* %9, align 4
  %330 = load i32, i32* %11, align 4
  %331 = lshr i32 %330, 13
  %332 = load i32, i32* %9, align 4
  %333 = xor i32 %332, %331
  store i32 %333, i32* %9, align 4
  %334 = load i32, i32* %11, align 4
  %335 = load i32, i32* %10, align 4
  %336 = sub i32 %335, %334
  store i32 %336, i32* %10, align 4
  %337 = load i32, i32* %9, align 4
  %338 = load i32, i32* %10, align 4
  %339 = sub i32 %338, %337
  store i32 %339, i32* %10, align 4
  %340 = load i32, i32* %9, align 4
  %341 = lshr i32 %340, 12
  %342 = load i32, i32* %10, align 4
  %343 = xor i32 %342, %341
  store i32 %343, i32* %10, align 4
  %344 = load i32, i32* %9, align 4
  %345 = load i32, i32* %11, align 4
  %346 = sub i32 %345, %344
  store i32 %346, i32* %11, align 4
  %347 = load i32, i32* %10, align 4
  %348 = load i32, i32* %11, align 4
  %349 = sub i32 %348, %347
  store i32 %349, i32* %11, align 4
  %350 = load i32, i32* %10, align 4
  %351 = shl i32 %350, 16
  %352 = load i32, i32* %11, align 4
  %353 = xor i32 %352, %351
  store i32 %353, i32* %11, align 4
  %354 = load i32, i32* %10, align 4
  %355 = load i32, i32* %9, align 4
  %356 = sub i32 %355, %354
  store i32 %356, i32* %9, align 4
  %357 = load i32, i32* %11, align 4
  %358 = load i32, i32* %9, align 4
  %359 = sub i32 %358, %357
  store i32 %359, i32* %9, align 4
  %360 = load i32, i32* %11, align 4
  %361 = lshr i32 %360, 5
  %362 = load i32, i32* %9, align 4
  %363 = xor i32 %362, %361
  store i32 %363, i32* %9, align 4
  %364 = load i32, i32* %11, align 4
  %365 = load i32, i32* %10, align 4
  %366 = sub i32 %365, %364
  store i32 %366, i32* %10, align 4
  %367 = load i32, i32* %9, align 4
  %368 = load i32, i32* %10, align 4
  %369 = sub i32 %368, %367
  store i32 %369, i32* %10, align 4
  %370 = load i32, i32* %9, align 4
  %371 = lshr i32 %370, 3
  %372 = load i32, i32* %10, align 4
  %373 = xor i32 %372, %371
  store i32 %373, i32* %10, align 4
  %374 = load i32, i32* %9, align 4
  %375 = load i32, i32* %11, align 4
  %376 = sub i32 %375, %374
  store i32 %376, i32* %11, align 4
  %377 = load i32, i32* %10, align 4
  %378 = load i32, i32* %11, align 4
  %379 = sub i32 %378, %377
  store i32 %379, i32* %11, align 4
  %380 = load i32, i32* %10, align 4
  %381 = shl i32 %380, 10
  %382 = load i32, i32* %11, align 4
  %383 = xor i32 %382, %381
  store i32 %383, i32* %11, align 4
  %384 = load i32, i32* %10, align 4
  %385 = load i32, i32* %9, align 4
  %386 = sub i32 %385, %384
  store i32 %386, i32* %9, align 4
  %387 = load i32, i32* %11, align 4
  %388 = load i32, i32* %9, align 4
  %389 = sub i32 %388, %387
  store i32 %389, i32* %9, align 4
  %390 = load i32, i32* %11, align 4
  %391 = lshr i32 %390, 15
  %392 = load i32, i32* %9, align 4
  %393 = xor i32 %392, %391
  store i32 %393, i32* %9, align 4
  br label %394

; <label>:394:                                    ; preds = %303
  br label %395

; <label>:395:                                    ; preds = %394
  br label %396

; <label>:396:                                    ; preds = %395
  br label %397

; <label>:397:                                    ; preds = %396
  store %struct.my_hash* null, %struct.my_hash** %7, align 8
  %398 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %399 = icmp ne %struct.my_hash* %398, null
  br i1 %399, label %400, label %505

; <label>:400:                                    ; preds = %397
  br label %401

; <label>:401:                                    ; preds = %400
  %402 = load i32, i32* %9, align 4
  %403 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %404 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %403, i32 0, i32 2
  %405 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %404, i32 0, i32 0
  %406 = load %struct.UT_hash_table*, %struct.UT_hash_table** %405, align 8
  %407 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %406, i32 0, i32 1
  %408 = load i32, i32* %407, align 8
  %409 = sub i32 %408, 1
  %410 = and i32 %402, %409
  store i32 %410, i32* %14, align 4
  br label %411

; <label>:411:                                    ; preds = %401
  br label %412

; <label>:412:                                    ; preds = %411
  %413 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %414 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %413, i32 0, i32 2
  %415 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %414, i32 0, i32 0
  %416 = load %struct.UT_hash_table*, %struct.UT_hash_table** %415, align 8
  %417 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %416, i32 0, i32 0
  %418 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %417, align 8
  %419 = load i32, i32* %14, align 4
  %420 = zext i32 %419 to i64
  %421 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %418, i64 %420
  %422 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %421, i32 0, i32 0
  %423 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %422, align 8
  %424 = icmp ne %struct.UT_hash_handle* %423, null
  br i1 %424, label %425, label %449

; <label>:425:                                    ; preds = %412
  br label %426

; <label>:426:                                    ; preds = %425
  %427 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %428 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %427, i32 0, i32 2
  %429 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %428, i32 0, i32 0
  %430 = load %struct.UT_hash_table*, %struct.UT_hash_table** %429, align 8
  %431 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %430, i32 0, i32 0
  %432 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %431, align 8
  %433 = load i32, i32* %14, align 4
  %434 = zext i32 %433 to i64
  %435 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %432, i64 %434
  %436 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %435, i32 0, i32 0
  %437 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %436, align 8
  %438 = bitcast %struct.UT_hash_handle* %437 to i8*
  %439 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %440 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %439, i32 0, i32 2
  %441 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %440, i32 0, i32 0
  %442 = load %struct.UT_hash_table*, %struct.UT_hash_table** %441, align 8
  %443 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %442, i32 0, i32 5
  %444 = load i64, i64* %443, align 8
  %445 = sub i64 0, %444
  %446 = getelementptr inbounds i8, i8* %438, i64 %445
  %447 = bitcast i8* %446 to %struct.my_hash*
  store %struct.my_hash* %447, %struct.my_hash** %7, align 8
  br label %448

; <label>:448:                                    ; preds = %426
  br label %450

; <label>:449:                                    ; preds = %412
  store %struct.my_hash* null, %struct.my_hash** %7, align 8
  br label %450

; <label>:450:                                    ; preds = %449, %448
  br label %451

; <label>:451:                                    ; preds = %502, %450
  %452 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %453 = icmp ne %struct.my_hash* %452, null
  br i1 %453, label %454, label %503

; <label>:454:                                    ; preds = %451
  %455 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %456 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %455, i32 0, i32 2
  %457 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %456, i32 0, i32 7
  %458 = load i32, i32* %457, align 4
  %459 = load i32, i32* %9, align 4
  %460 = icmp eq i32 %458, %459
  br i1 %460, label %461, label %478

; <label>:461:                                    ; preds = %454
  %462 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %463 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %462, i32 0, i32 2
  %464 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %463, i32 0, i32 6
  %465 = load i32, i32* %464, align 8
  %466 = zext i32 %465 to i64
  %467 = icmp eq i64 %466, 8
  br i1 %467, label %468, label %478

; <label>:468:                                    ; preds = %461
  %469 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %470 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %469, i32 0, i32 2
  %471 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %470, i32 0, i32 5
  %472 = load i8*, i8** %471, align 8
  %473 = bitcast i64* %5 to i8*
  %474 = call i32 @memcmp(i8* %472, i8* %473, i64 8)
  %475 = icmp eq i32 %474, 0
  br i1 %475, label %476, label %477

; <label>:476:                                    ; preds = %468
  br label %503

; <label>:477:                                    ; preds = %468
  br label %478

; <label>:478:                                    ; preds = %477, %461, %454
  %479 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %480 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %479, i32 0, i32 2
  %481 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %480, i32 0, i32 4
  %482 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %481, align 8
  %483 = icmp ne %struct.UT_hash_handle* %482, null
  br i1 %483, label %484, label %501

; <label>:484:                                    ; preds = %478
  br label %485

; <label>:485:                                    ; preds = %484
  %486 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %487 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %486, i32 0, i32 2
  %488 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %487, i32 0, i32 4
  %489 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %488, align 8
  %490 = bitcast %struct.UT_hash_handle* %489 to i8*
  %491 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %492 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %491, i32 0, i32 2
  %493 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %492, i32 0, i32 0
  %494 = load %struct.UT_hash_table*, %struct.UT_hash_table** %493, align 8
  %495 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %494, i32 0, i32 5
  %496 = load i64, i64* %495, align 8
  %497 = sub i64 0, %496
  %498 = getelementptr inbounds i8, i8* %490, i64 %497
  %499 = bitcast i8* %498 to %struct.my_hash*
  store %struct.my_hash* %499, %struct.my_hash** %7, align 8
  br label %500

; <label>:500:                                    ; preds = %485
  br label %502

; <label>:501:                                    ; preds = %478
  store %struct.my_hash* null, %struct.my_hash** %7, align 8
  br label %502

; <label>:502:                                    ; preds = %501, %500
  br label %451

; <label>:503:                                    ; preds = %476, %451
  br label %504

; <label>:504:                                    ; preds = %503
  br label %505

; <label>:505:                                    ; preds = %504, %397
  br label %506

; <label>:506:                                    ; preds = %505
  br label %507

; <label>:507:                                    ; preds = %506
  %508 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %509 = icmp eq %struct.my_hash* %508, null
  br i1 %509, label %510, label %511

; <label>:510:                                    ; preds = %507
  store i64 0, i64* %3, align 8
  br label %515

; <label>:511:                                    ; preds = %507
  %512 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %513 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %512, i32 0, i32 1
  %514 = load i64, i64* %513, align 8
  store i64 %514, i64* %3, align 8
  br label %515

; <label>:515:                                    ; preds = %511, %510
  %516 = load i64, i64* %3, align 8
  ret i64 %516
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_hash_45ref(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %6 = load i64, i64* %2, align 8
  %7 = call i64 @expect_cons(i64 %6, i64* %3)
  store i64 %7, i64* %4, align 8
  %8 = load i64, i64* %3, align 8
  %9 = call i64 @expect_cons(i64 %8, i64* %3)
  store i64 %9, i64* %5, align 8
  %10 = load i64, i64* %3, align 8
  %11 = icmp ne i64 %10, 0
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %1
  %14 = load i64, i64* %4, align 8
  %15 = load i64, i64* %5, align 8
  %16 = call i64 @prim_hash_45ref(i64 %14, i64 %15)
  ret i64 %16
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_hash_45set_33(i64, i64, i64) #0 {
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64, align 8
  %7 = alloca i64*, align 8
  %8 = alloca %struct.my_hash*, align 8
  %9 = alloca %struct.my_hash*, align 8
  %10 = alloca i32, align 4
  %11 = alloca i32, align 4
  %12 = alloca i32, align 4
  %13 = alloca i32, align 4
  %14 = alloca i8*, align 8
  %15 = alloca i32, align 4
  %16 = alloca i32, align 4
  %17 = alloca i32, align 4
  %18 = alloca i32, align 4
  %19 = alloca i32, align 4
  %20 = alloca i8*, align 8
  %21 = alloca i32, align 4
  %22 = alloca %struct.UT_hash_bucket*, align 8
  %23 = alloca i32, align 4
  %24 = alloca i32, align 4
  %25 = alloca %struct.UT_hash_handle*, align 8
  %26 = alloca %struct.UT_hash_handle*, align 8
  %27 = alloca %struct.UT_hash_bucket*, align 8
  %28 = alloca %struct.UT_hash_bucket*, align 8
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  store i64 %2, i64* %6, align 8
  %29 = load i64, i64* %4, align 8
  %30 = and i64 %29, 7
  %31 = icmp ne i64 %30, 6
  br i1 %31, label %39, label %32

; <label>:32:                                     ; preds = %3
  %33 = load i64, i64* %4, align 8
  %34 = and i64 %33, -8
  %35 = inttoptr i64 %34 to i64*
  %36 = getelementptr inbounds i64, i64* %35, i64 0
  %37 = load i64, i64* %36, align 8
  %38 = icmp ne i64 2, %37
  br i1 %38, label %39, label %40

; <label>:39:                                     ; preds = %32, %3
  call void @fatal_err(i8* getelementptr inbounds ([60 x i8], [60 x i8]* @.str.61, i32 0, i32 0))
  br label %40

; <label>:40:                                     ; preds = %39, %32
  %41 = load i64, i64* %4, align 8
  %42 = and i64 %41, -8
  %43 = inttoptr i64 %42 to i64*
  store i64* %43, i64** %7, align 8
  %44 = load i64*, i64** %7, align 8
  %45 = getelementptr inbounds i64, i64* %44, i64 1
  %46 = load i64, i64* %45, align 8
  %47 = and i64 %46, -8
  %48 = inttoptr i64 %47 to i64*
  %49 = bitcast i64* %48 to %struct.my_hash*
  store %struct.my_hash* %49, %struct.my_hash** %9, align 8
  br label %50

; <label>:50:                                     ; preds = %40
  br label %51

; <label>:51:                                     ; preds = %50
  br label %52

; <label>:52:                                     ; preds = %51
  %53 = bitcast i64* %5 to i8*
  store i8* %53, i8** %14, align 8
  store i32 -17973521, i32* %10, align 4
  store i32 -1640531527, i32* %12, align 4
  store i32 -1640531527, i32* %11, align 4
  store i32 8, i32* %13, align 4
  br label %54

; <label>:54:                                     ; preds = %221, %52
  %55 = load i32, i32* %13, align 4
  %56 = icmp uge i32 %55, 12
  br i1 %56, label %57, label %226

; <label>:57:                                     ; preds = %54
  %58 = load i8*, i8** %14, align 8
  %59 = getelementptr inbounds i8, i8* %58, i64 0
  %60 = load i8, i8* %59, align 1
  %61 = zext i8 %60 to i32
  %62 = load i8*, i8** %14, align 8
  %63 = getelementptr inbounds i8, i8* %62, i64 1
  %64 = load i8, i8* %63, align 1
  %65 = zext i8 %64 to i32
  %66 = shl i32 %65, 8
  %67 = add i32 %61, %66
  %68 = load i8*, i8** %14, align 8
  %69 = getelementptr inbounds i8, i8* %68, i64 2
  %70 = load i8, i8* %69, align 1
  %71 = zext i8 %70 to i32
  %72 = shl i32 %71, 16
  %73 = add i32 %67, %72
  %74 = load i8*, i8** %14, align 8
  %75 = getelementptr inbounds i8, i8* %74, i64 3
  %76 = load i8, i8* %75, align 1
  %77 = zext i8 %76 to i32
  %78 = shl i32 %77, 24
  %79 = add i32 %73, %78
  %80 = load i32, i32* %11, align 4
  %81 = add i32 %80, %79
  store i32 %81, i32* %11, align 4
  %82 = load i8*, i8** %14, align 8
  %83 = getelementptr inbounds i8, i8* %82, i64 4
  %84 = load i8, i8* %83, align 1
  %85 = zext i8 %84 to i32
  %86 = load i8*, i8** %14, align 8
  %87 = getelementptr inbounds i8, i8* %86, i64 5
  %88 = load i8, i8* %87, align 1
  %89 = zext i8 %88 to i32
  %90 = shl i32 %89, 8
  %91 = add i32 %85, %90
  %92 = load i8*, i8** %14, align 8
  %93 = getelementptr inbounds i8, i8* %92, i64 6
  %94 = load i8, i8* %93, align 1
  %95 = zext i8 %94 to i32
  %96 = shl i32 %95, 16
  %97 = add i32 %91, %96
  %98 = load i8*, i8** %14, align 8
  %99 = getelementptr inbounds i8, i8* %98, i64 7
  %100 = load i8, i8* %99, align 1
  %101 = zext i8 %100 to i32
  %102 = shl i32 %101, 24
  %103 = add i32 %97, %102
  %104 = load i32, i32* %12, align 4
  %105 = add i32 %104, %103
  store i32 %105, i32* %12, align 4
  %106 = load i8*, i8** %14, align 8
  %107 = getelementptr inbounds i8, i8* %106, i64 8
  %108 = load i8, i8* %107, align 1
  %109 = zext i8 %108 to i32
  %110 = load i8*, i8** %14, align 8
  %111 = getelementptr inbounds i8, i8* %110, i64 9
  %112 = load i8, i8* %111, align 1
  %113 = zext i8 %112 to i32
  %114 = shl i32 %113, 8
  %115 = add i32 %109, %114
  %116 = load i8*, i8** %14, align 8
  %117 = getelementptr inbounds i8, i8* %116, i64 10
  %118 = load i8, i8* %117, align 1
  %119 = zext i8 %118 to i32
  %120 = shl i32 %119, 16
  %121 = add i32 %115, %120
  %122 = load i8*, i8** %14, align 8
  %123 = getelementptr inbounds i8, i8* %122, i64 11
  %124 = load i8, i8* %123, align 1
  %125 = zext i8 %124 to i32
  %126 = shl i32 %125, 24
  %127 = add i32 %121, %126
  %128 = load i32, i32* %10, align 4
  %129 = add i32 %128, %127
  store i32 %129, i32* %10, align 4
  br label %130

; <label>:130:                                    ; preds = %57
  %131 = load i32, i32* %12, align 4
  %132 = load i32, i32* %11, align 4
  %133 = sub i32 %132, %131
  store i32 %133, i32* %11, align 4
  %134 = load i32, i32* %10, align 4
  %135 = load i32, i32* %11, align 4
  %136 = sub i32 %135, %134
  store i32 %136, i32* %11, align 4
  %137 = load i32, i32* %10, align 4
  %138 = lshr i32 %137, 13
  %139 = load i32, i32* %11, align 4
  %140 = xor i32 %139, %138
  store i32 %140, i32* %11, align 4
  %141 = load i32, i32* %10, align 4
  %142 = load i32, i32* %12, align 4
  %143 = sub i32 %142, %141
  store i32 %143, i32* %12, align 4
  %144 = load i32, i32* %11, align 4
  %145 = load i32, i32* %12, align 4
  %146 = sub i32 %145, %144
  store i32 %146, i32* %12, align 4
  %147 = load i32, i32* %11, align 4
  %148 = shl i32 %147, 8
  %149 = load i32, i32* %12, align 4
  %150 = xor i32 %149, %148
  store i32 %150, i32* %12, align 4
  %151 = load i32, i32* %11, align 4
  %152 = load i32, i32* %10, align 4
  %153 = sub i32 %152, %151
  store i32 %153, i32* %10, align 4
  %154 = load i32, i32* %12, align 4
  %155 = load i32, i32* %10, align 4
  %156 = sub i32 %155, %154
  store i32 %156, i32* %10, align 4
  %157 = load i32, i32* %12, align 4
  %158 = lshr i32 %157, 13
  %159 = load i32, i32* %10, align 4
  %160 = xor i32 %159, %158
  store i32 %160, i32* %10, align 4
  %161 = load i32, i32* %12, align 4
  %162 = load i32, i32* %11, align 4
  %163 = sub i32 %162, %161
  store i32 %163, i32* %11, align 4
  %164 = load i32, i32* %10, align 4
  %165 = load i32, i32* %11, align 4
  %166 = sub i32 %165, %164
  store i32 %166, i32* %11, align 4
  %167 = load i32, i32* %10, align 4
  %168 = lshr i32 %167, 12
  %169 = load i32, i32* %11, align 4
  %170 = xor i32 %169, %168
  store i32 %170, i32* %11, align 4
  %171 = load i32, i32* %10, align 4
  %172 = load i32, i32* %12, align 4
  %173 = sub i32 %172, %171
  store i32 %173, i32* %12, align 4
  %174 = load i32, i32* %11, align 4
  %175 = load i32, i32* %12, align 4
  %176 = sub i32 %175, %174
  store i32 %176, i32* %12, align 4
  %177 = load i32, i32* %11, align 4
  %178 = shl i32 %177, 16
  %179 = load i32, i32* %12, align 4
  %180 = xor i32 %179, %178
  store i32 %180, i32* %12, align 4
  %181 = load i32, i32* %11, align 4
  %182 = load i32, i32* %10, align 4
  %183 = sub i32 %182, %181
  store i32 %183, i32* %10, align 4
  %184 = load i32, i32* %12, align 4
  %185 = load i32, i32* %10, align 4
  %186 = sub i32 %185, %184
  store i32 %186, i32* %10, align 4
  %187 = load i32, i32* %12, align 4
  %188 = lshr i32 %187, 5
  %189 = load i32, i32* %10, align 4
  %190 = xor i32 %189, %188
  store i32 %190, i32* %10, align 4
  %191 = load i32, i32* %12, align 4
  %192 = load i32, i32* %11, align 4
  %193 = sub i32 %192, %191
  store i32 %193, i32* %11, align 4
  %194 = load i32, i32* %10, align 4
  %195 = load i32, i32* %11, align 4
  %196 = sub i32 %195, %194
  store i32 %196, i32* %11, align 4
  %197 = load i32, i32* %10, align 4
  %198 = lshr i32 %197, 3
  %199 = load i32, i32* %11, align 4
  %200 = xor i32 %199, %198
  store i32 %200, i32* %11, align 4
  %201 = load i32, i32* %10, align 4
  %202 = load i32, i32* %12, align 4
  %203 = sub i32 %202, %201
  store i32 %203, i32* %12, align 4
  %204 = load i32, i32* %11, align 4
  %205 = load i32, i32* %12, align 4
  %206 = sub i32 %205, %204
  store i32 %206, i32* %12, align 4
  %207 = load i32, i32* %11, align 4
  %208 = shl i32 %207, 10
  %209 = load i32, i32* %12, align 4
  %210 = xor i32 %209, %208
  store i32 %210, i32* %12, align 4
  %211 = load i32, i32* %11, align 4
  %212 = load i32, i32* %10, align 4
  %213 = sub i32 %212, %211
  store i32 %213, i32* %10, align 4
  %214 = load i32, i32* %12, align 4
  %215 = load i32, i32* %10, align 4
  %216 = sub i32 %215, %214
  store i32 %216, i32* %10, align 4
  %217 = load i32, i32* %12, align 4
  %218 = lshr i32 %217, 15
  %219 = load i32, i32* %10, align 4
  %220 = xor i32 %219, %218
  store i32 %220, i32* %10, align 4
  br label %221

; <label>:221:                                    ; preds = %130
  %222 = load i8*, i8** %14, align 8
  %223 = getelementptr inbounds i8, i8* %222, i64 12
  store i8* %223, i8** %14, align 8
  %224 = load i32, i32* %13, align 4
  %225 = sub i32 %224, 12
  store i32 %225, i32* %13, align 4
  br label %54

; <label>:226:                                    ; preds = %54
  %227 = load i32, i32* %10, align 4
  %228 = add i32 %227, 8
  store i32 %228, i32* %10, align 4
  %229 = load i32, i32* %13, align 4
  switch i32 %229, label %316 [
    i32 11, label %230
    i32 10, label %238
    i32 9, label %246
    i32 8, label %254
    i32 7, label %262
    i32 6, label %270
    i32 5, label %278
    i32 4, label %285
    i32 3, label %293
    i32 2, label %301
    i32 1, label %309
  ]

; <label>:230:                                    ; preds = %226
  %231 = load i8*, i8** %14, align 8
  %232 = getelementptr inbounds i8, i8* %231, i64 10
  %233 = load i8, i8* %232, align 1
  %234 = zext i8 %233 to i32
  %235 = shl i32 %234, 24
  %236 = load i32, i32* %10, align 4
  %237 = add i32 %236, %235
  store i32 %237, i32* %10, align 4
  br label %238

; <label>:238:                                    ; preds = %226, %230
  %239 = load i8*, i8** %14, align 8
  %240 = getelementptr inbounds i8, i8* %239, i64 9
  %241 = load i8, i8* %240, align 1
  %242 = zext i8 %241 to i32
  %243 = shl i32 %242, 16
  %244 = load i32, i32* %10, align 4
  %245 = add i32 %244, %243
  store i32 %245, i32* %10, align 4
  br label %246

; <label>:246:                                    ; preds = %226, %238
  %247 = load i8*, i8** %14, align 8
  %248 = getelementptr inbounds i8, i8* %247, i64 8
  %249 = load i8, i8* %248, align 1
  %250 = zext i8 %249 to i32
  %251 = shl i32 %250, 8
  %252 = load i32, i32* %10, align 4
  %253 = add i32 %252, %251
  store i32 %253, i32* %10, align 4
  br label %254

; <label>:254:                                    ; preds = %226, %246
  %255 = load i8*, i8** %14, align 8
  %256 = getelementptr inbounds i8, i8* %255, i64 7
  %257 = load i8, i8* %256, align 1
  %258 = zext i8 %257 to i32
  %259 = shl i32 %258, 24
  %260 = load i32, i32* %12, align 4
  %261 = add i32 %260, %259
  store i32 %261, i32* %12, align 4
  br label %262

; <label>:262:                                    ; preds = %226, %254
  %263 = load i8*, i8** %14, align 8
  %264 = getelementptr inbounds i8, i8* %263, i64 6
  %265 = load i8, i8* %264, align 1
  %266 = zext i8 %265 to i32
  %267 = shl i32 %266, 16
  %268 = load i32, i32* %12, align 4
  %269 = add i32 %268, %267
  store i32 %269, i32* %12, align 4
  br label %270

; <label>:270:                                    ; preds = %226, %262
  %271 = load i8*, i8** %14, align 8
  %272 = getelementptr inbounds i8, i8* %271, i64 5
  %273 = load i8, i8* %272, align 1
  %274 = zext i8 %273 to i32
  %275 = shl i32 %274, 8
  %276 = load i32, i32* %12, align 4
  %277 = add i32 %276, %275
  store i32 %277, i32* %12, align 4
  br label %278

; <label>:278:                                    ; preds = %226, %270
  %279 = load i8*, i8** %14, align 8
  %280 = getelementptr inbounds i8, i8* %279, i64 4
  %281 = load i8, i8* %280, align 1
  %282 = zext i8 %281 to i32
  %283 = load i32, i32* %12, align 4
  %284 = add i32 %283, %282
  store i32 %284, i32* %12, align 4
  br label %285

; <label>:285:                                    ; preds = %226, %278
  %286 = load i8*, i8** %14, align 8
  %287 = getelementptr inbounds i8, i8* %286, i64 3
  %288 = load i8, i8* %287, align 1
  %289 = zext i8 %288 to i32
  %290 = shl i32 %289, 24
  %291 = load i32, i32* %11, align 4
  %292 = add i32 %291, %290
  store i32 %292, i32* %11, align 4
  br label %293

; <label>:293:                                    ; preds = %226, %285
  %294 = load i8*, i8** %14, align 8
  %295 = getelementptr inbounds i8, i8* %294, i64 2
  %296 = load i8, i8* %295, align 1
  %297 = zext i8 %296 to i32
  %298 = shl i32 %297, 16
  %299 = load i32, i32* %11, align 4
  %300 = add i32 %299, %298
  store i32 %300, i32* %11, align 4
  br label %301

; <label>:301:                                    ; preds = %226, %293
  %302 = load i8*, i8** %14, align 8
  %303 = getelementptr inbounds i8, i8* %302, i64 1
  %304 = load i8, i8* %303, align 1
  %305 = zext i8 %304 to i32
  %306 = shl i32 %305, 8
  %307 = load i32, i32* %11, align 4
  %308 = add i32 %307, %306
  store i32 %308, i32* %11, align 4
  br label %309

; <label>:309:                                    ; preds = %226, %301
  %310 = load i8*, i8** %14, align 8
  %311 = getelementptr inbounds i8, i8* %310, i64 0
  %312 = load i8, i8* %311, align 1
  %313 = zext i8 %312 to i32
  %314 = load i32, i32* %11, align 4
  %315 = add i32 %314, %313
  store i32 %315, i32* %11, align 4
  br label %316

; <label>:316:                                    ; preds = %309, %226
  br label %317

; <label>:317:                                    ; preds = %316
  %318 = load i32, i32* %12, align 4
  %319 = load i32, i32* %11, align 4
  %320 = sub i32 %319, %318
  store i32 %320, i32* %11, align 4
  %321 = load i32, i32* %10, align 4
  %322 = load i32, i32* %11, align 4
  %323 = sub i32 %322, %321
  store i32 %323, i32* %11, align 4
  %324 = load i32, i32* %10, align 4
  %325 = lshr i32 %324, 13
  %326 = load i32, i32* %11, align 4
  %327 = xor i32 %326, %325
  store i32 %327, i32* %11, align 4
  %328 = load i32, i32* %10, align 4
  %329 = load i32, i32* %12, align 4
  %330 = sub i32 %329, %328
  store i32 %330, i32* %12, align 4
  %331 = load i32, i32* %11, align 4
  %332 = load i32, i32* %12, align 4
  %333 = sub i32 %332, %331
  store i32 %333, i32* %12, align 4
  %334 = load i32, i32* %11, align 4
  %335 = shl i32 %334, 8
  %336 = load i32, i32* %12, align 4
  %337 = xor i32 %336, %335
  store i32 %337, i32* %12, align 4
  %338 = load i32, i32* %11, align 4
  %339 = load i32, i32* %10, align 4
  %340 = sub i32 %339, %338
  store i32 %340, i32* %10, align 4
  %341 = load i32, i32* %12, align 4
  %342 = load i32, i32* %10, align 4
  %343 = sub i32 %342, %341
  store i32 %343, i32* %10, align 4
  %344 = load i32, i32* %12, align 4
  %345 = lshr i32 %344, 13
  %346 = load i32, i32* %10, align 4
  %347 = xor i32 %346, %345
  store i32 %347, i32* %10, align 4
  %348 = load i32, i32* %12, align 4
  %349 = load i32, i32* %11, align 4
  %350 = sub i32 %349, %348
  store i32 %350, i32* %11, align 4
  %351 = load i32, i32* %10, align 4
  %352 = load i32, i32* %11, align 4
  %353 = sub i32 %352, %351
  store i32 %353, i32* %11, align 4
  %354 = load i32, i32* %10, align 4
  %355 = lshr i32 %354, 12
  %356 = load i32, i32* %11, align 4
  %357 = xor i32 %356, %355
  store i32 %357, i32* %11, align 4
  %358 = load i32, i32* %10, align 4
  %359 = load i32, i32* %12, align 4
  %360 = sub i32 %359, %358
  store i32 %360, i32* %12, align 4
  %361 = load i32, i32* %11, align 4
  %362 = load i32, i32* %12, align 4
  %363 = sub i32 %362, %361
  store i32 %363, i32* %12, align 4
  %364 = load i32, i32* %11, align 4
  %365 = shl i32 %364, 16
  %366 = load i32, i32* %12, align 4
  %367 = xor i32 %366, %365
  store i32 %367, i32* %12, align 4
  %368 = load i32, i32* %11, align 4
  %369 = load i32, i32* %10, align 4
  %370 = sub i32 %369, %368
  store i32 %370, i32* %10, align 4
  %371 = load i32, i32* %12, align 4
  %372 = load i32, i32* %10, align 4
  %373 = sub i32 %372, %371
  store i32 %373, i32* %10, align 4
  %374 = load i32, i32* %12, align 4
  %375 = lshr i32 %374, 5
  %376 = load i32, i32* %10, align 4
  %377 = xor i32 %376, %375
  store i32 %377, i32* %10, align 4
  %378 = load i32, i32* %12, align 4
  %379 = load i32, i32* %11, align 4
  %380 = sub i32 %379, %378
  store i32 %380, i32* %11, align 4
  %381 = load i32, i32* %10, align 4
  %382 = load i32, i32* %11, align 4
  %383 = sub i32 %382, %381
  store i32 %383, i32* %11, align 4
  %384 = load i32, i32* %10, align 4
  %385 = lshr i32 %384, 3
  %386 = load i32, i32* %11, align 4
  %387 = xor i32 %386, %385
  store i32 %387, i32* %11, align 4
  %388 = load i32, i32* %10, align 4
  %389 = load i32, i32* %12, align 4
  %390 = sub i32 %389, %388
  store i32 %390, i32* %12, align 4
  %391 = load i32, i32* %11, align 4
  %392 = load i32, i32* %12, align 4
  %393 = sub i32 %392, %391
  store i32 %393, i32* %12, align 4
  %394 = load i32, i32* %11, align 4
  %395 = shl i32 %394, 10
  %396 = load i32, i32* %12, align 4
  %397 = xor i32 %396, %395
  store i32 %397, i32* %12, align 4
  %398 = load i32, i32* %11, align 4
  %399 = load i32, i32* %10, align 4
  %400 = sub i32 %399, %398
  store i32 %400, i32* %10, align 4
  %401 = load i32, i32* %12, align 4
  %402 = load i32, i32* %10, align 4
  %403 = sub i32 %402, %401
  store i32 %403, i32* %10, align 4
  %404 = load i32, i32* %12, align 4
  %405 = lshr i32 %404, 15
  %406 = load i32, i32* %10, align 4
  %407 = xor i32 %406, %405
  store i32 %407, i32* %10, align 4
  br label %408

; <label>:408:                                    ; preds = %317
  br label %409

; <label>:409:                                    ; preds = %408
  br label %410

; <label>:410:                                    ; preds = %409
  br label %411

; <label>:411:                                    ; preds = %410
  store %struct.my_hash* null, %struct.my_hash** %8, align 8
  %412 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %413 = icmp ne %struct.my_hash* %412, null
  br i1 %413, label %414, label %519

; <label>:414:                                    ; preds = %411
  br label %415

; <label>:415:                                    ; preds = %414
  %416 = load i32, i32* %10, align 4
  %417 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %418 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %417, i32 0, i32 2
  %419 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %418, i32 0, i32 0
  %420 = load %struct.UT_hash_table*, %struct.UT_hash_table** %419, align 8
  %421 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %420, i32 0, i32 1
  %422 = load i32, i32* %421, align 8
  %423 = sub i32 %422, 1
  %424 = and i32 %416, %423
  store i32 %424, i32* %15, align 4
  br label %425

; <label>:425:                                    ; preds = %415
  br label %426

; <label>:426:                                    ; preds = %425
  %427 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %428 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %427, i32 0, i32 2
  %429 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %428, i32 0, i32 0
  %430 = load %struct.UT_hash_table*, %struct.UT_hash_table** %429, align 8
  %431 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %430, i32 0, i32 0
  %432 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %431, align 8
  %433 = load i32, i32* %15, align 4
  %434 = zext i32 %433 to i64
  %435 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %432, i64 %434
  %436 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %435, i32 0, i32 0
  %437 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %436, align 8
  %438 = icmp ne %struct.UT_hash_handle* %437, null
  br i1 %438, label %439, label %463

; <label>:439:                                    ; preds = %426
  br label %440

; <label>:440:                                    ; preds = %439
  %441 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %442 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %441, i32 0, i32 2
  %443 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %442, i32 0, i32 0
  %444 = load %struct.UT_hash_table*, %struct.UT_hash_table** %443, align 8
  %445 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %444, i32 0, i32 0
  %446 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %445, align 8
  %447 = load i32, i32* %15, align 4
  %448 = zext i32 %447 to i64
  %449 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %446, i64 %448
  %450 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %449, i32 0, i32 0
  %451 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %450, align 8
  %452 = bitcast %struct.UT_hash_handle* %451 to i8*
  %453 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %454 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %453, i32 0, i32 2
  %455 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %454, i32 0, i32 0
  %456 = load %struct.UT_hash_table*, %struct.UT_hash_table** %455, align 8
  %457 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %456, i32 0, i32 5
  %458 = load i64, i64* %457, align 8
  %459 = sub i64 0, %458
  %460 = getelementptr inbounds i8, i8* %452, i64 %459
  %461 = bitcast i8* %460 to %struct.my_hash*
  store %struct.my_hash* %461, %struct.my_hash** %8, align 8
  br label %462

; <label>:462:                                    ; preds = %440
  br label %464

; <label>:463:                                    ; preds = %426
  store %struct.my_hash* null, %struct.my_hash** %8, align 8
  br label %464

; <label>:464:                                    ; preds = %463, %462
  br label %465

; <label>:465:                                    ; preds = %516, %464
  %466 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %467 = icmp ne %struct.my_hash* %466, null
  br i1 %467, label %468, label %517

; <label>:468:                                    ; preds = %465
  %469 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %470 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %469, i32 0, i32 2
  %471 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %470, i32 0, i32 7
  %472 = load i32, i32* %471, align 4
  %473 = load i32, i32* %10, align 4
  %474 = icmp eq i32 %472, %473
  br i1 %474, label %475, label %492

; <label>:475:                                    ; preds = %468
  %476 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %477 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %476, i32 0, i32 2
  %478 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %477, i32 0, i32 6
  %479 = load i32, i32* %478, align 8
  %480 = zext i32 %479 to i64
  %481 = icmp eq i64 %480, 8
  br i1 %481, label %482, label %492

; <label>:482:                                    ; preds = %475
  %483 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %484 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %483, i32 0, i32 2
  %485 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %484, i32 0, i32 5
  %486 = load i8*, i8** %485, align 8
  %487 = bitcast i64* %5 to i8*
  %488 = call i32 @memcmp(i8* %486, i8* %487, i64 8)
  %489 = icmp eq i32 %488, 0
  br i1 %489, label %490, label %491

; <label>:490:                                    ; preds = %482
  br label %517

; <label>:491:                                    ; preds = %482
  br label %492

; <label>:492:                                    ; preds = %491, %475, %468
  %493 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %494 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %493, i32 0, i32 2
  %495 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %494, i32 0, i32 4
  %496 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %495, align 8
  %497 = icmp ne %struct.UT_hash_handle* %496, null
  br i1 %497, label %498, label %515

; <label>:498:                                    ; preds = %492
  br label %499

; <label>:499:                                    ; preds = %498
  %500 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %501 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %500, i32 0, i32 2
  %502 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %501, i32 0, i32 4
  %503 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %502, align 8
  %504 = bitcast %struct.UT_hash_handle* %503 to i8*
  %505 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %506 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %505, i32 0, i32 2
  %507 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %506, i32 0, i32 0
  %508 = load %struct.UT_hash_table*, %struct.UT_hash_table** %507, align 8
  %509 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %508, i32 0, i32 5
  %510 = load i64, i64* %509, align 8
  %511 = sub i64 0, %510
  %512 = getelementptr inbounds i8, i8* %504, i64 %511
  %513 = bitcast i8* %512 to %struct.my_hash*
  store %struct.my_hash* %513, %struct.my_hash** %8, align 8
  br label %514

; <label>:514:                                    ; preds = %499
  br label %516

; <label>:515:                                    ; preds = %492
  store %struct.my_hash* null, %struct.my_hash** %8, align 8
  br label %516

; <label>:516:                                    ; preds = %515, %514
  br label %465

; <label>:517:                                    ; preds = %490, %465
  br label %518

; <label>:518:                                    ; preds = %517
  br label %519

; <label>:519:                                    ; preds = %518, %411
  br label %520

; <label>:520:                                    ; preds = %519
  br label %521

; <label>:521:                                    ; preds = %520
  %522 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %523 = icmp eq %struct.my_hash* %522, null
  br i1 %523, label %524, label %1376

; <label>:524:                                    ; preds = %521
  %525 = call i64* @alloc(i64 72)
  %526 = bitcast i64* %525 to %struct.my_hash*
  store %struct.my_hash* %526, %struct.my_hash** %8, align 8
  %527 = load i64, i64* %5, align 8
  %528 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %529 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %528, i32 0, i32 0
  store i64 %527, i64* %529, align 8
  %530 = load i64, i64* %6, align 8
  %531 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %532 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %531, i32 0, i32 1
  store i64 %530, i64* %532, align 8
  br label %533

; <label>:533:                                    ; preds = %524
  br label %534

; <label>:534:                                    ; preds = %533
  br label %535

; <label>:535:                                    ; preds = %534
  %536 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %537 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %536, i32 0, i32 0
  %538 = bitcast i64* %537 to i8*
  store i8* %538, i8** %20, align 8
  store i32 -17973521, i32* %16, align 4
  store i32 -1640531527, i32* %18, align 4
  store i32 -1640531527, i32* %17, align 4
  store i32 8, i32* %19, align 4
  br label %539

; <label>:539:                                    ; preds = %706, %535
  %540 = load i32, i32* %19, align 4
  %541 = icmp uge i32 %540, 12
  br i1 %541, label %542, label %711

; <label>:542:                                    ; preds = %539
  %543 = load i8*, i8** %20, align 8
  %544 = getelementptr inbounds i8, i8* %543, i64 0
  %545 = load i8, i8* %544, align 1
  %546 = zext i8 %545 to i32
  %547 = load i8*, i8** %20, align 8
  %548 = getelementptr inbounds i8, i8* %547, i64 1
  %549 = load i8, i8* %548, align 1
  %550 = zext i8 %549 to i32
  %551 = shl i32 %550, 8
  %552 = add i32 %546, %551
  %553 = load i8*, i8** %20, align 8
  %554 = getelementptr inbounds i8, i8* %553, i64 2
  %555 = load i8, i8* %554, align 1
  %556 = zext i8 %555 to i32
  %557 = shl i32 %556, 16
  %558 = add i32 %552, %557
  %559 = load i8*, i8** %20, align 8
  %560 = getelementptr inbounds i8, i8* %559, i64 3
  %561 = load i8, i8* %560, align 1
  %562 = zext i8 %561 to i32
  %563 = shl i32 %562, 24
  %564 = add i32 %558, %563
  %565 = load i32, i32* %17, align 4
  %566 = add i32 %565, %564
  store i32 %566, i32* %17, align 4
  %567 = load i8*, i8** %20, align 8
  %568 = getelementptr inbounds i8, i8* %567, i64 4
  %569 = load i8, i8* %568, align 1
  %570 = zext i8 %569 to i32
  %571 = load i8*, i8** %20, align 8
  %572 = getelementptr inbounds i8, i8* %571, i64 5
  %573 = load i8, i8* %572, align 1
  %574 = zext i8 %573 to i32
  %575 = shl i32 %574, 8
  %576 = add i32 %570, %575
  %577 = load i8*, i8** %20, align 8
  %578 = getelementptr inbounds i8, i8* %577, i64 6
  %579 = load i8, i8* %578, align 1
  %580 = zext i8 %579 to i32
  %581 = shl i32 %580, 16
  %582 = add i32 %576, %581
  %583 = load i8*, i8** %20, align 8
  %584 = getelementptr inbounds i8, i8* %583, i64 7
  %585 = load i8, i8* %584, align 1
  %586 = zext i8 %585 to i32
  %587 = shl i32 %586, 24
  %588 = add i32 %582, %587
  %589 = load i32, i32* %18, align 4
  %590 = add i32 %589, %588
  store i32 %590, i32* %18, align 4
  %591 = load i8*, i8** %20, align 8
  %592 = getelementptr inbounds i8, i8* %591, i64 8
  %593 = load i8, i8* %592, align 1
  %594 = zext i8 %593 to i32
  %595 = load i8*, i8** %20, align 8
  %596 = getelementptr inbounds i8, i8* %595, i64 9
  %597 = load i8, i8* %596, align 1
  %598 = zext i8 %597 to i32
  %599 = shl i32 %598, 8
  %600 = add i32 %594, %599
  %601 = load i8*, i8** %20, align 8
  %602 = getelementptr inbounds i8, i8* %601, i64 10
  %603 = load i8, i8* %602, align 1
  %604 = zext i8 %603 to i32
  %605 = shl i32 %604, 16
  %606 = add i32 %600, %605
  %607 = load i8*, i8** %20, align 8
  %608 = getelementptr inbounds i8, i8* %607, i64 11
  %609 = load i8, i8* %608, align 1
  %610 = zext i8 %609 to i32
  %611 = shl i32 %610, 24
  %612 = add i32 %606, %611
  %613 = load i32, i32* %16, align 4
  %614 = add i32 %613, %612
  store i32 %614, i32* %16, align 4
  br label %615

; <label>:615:                                    ; preds = %542
  %616 = load i32, i32* %18, align 4
  %617 = load i32, i32* %17, align 4
  %618 = sub i32 %617, %616
  store i32 %618, i32* %17, align 4
  %619 = load i32, i32* %16, align 4
  %620 = load i32, i32* %17, align 4
  %621 = sub i32 %620, %619
  store i32 %621, i32* %17, align 4
  %622 = load i32, i32* %16, align 4
  %623 = lshr i32 %622, 13
  %624 = load i32, i32* %17, align 4
  %625 = xor i32 %624, %623
  store i32 %625, i32* %17, align 4
  %626 = load i32, i32* %16, align 4
  %627 = load i32, i32* %18, align 4
  %628 = sub i32 %627, %626
  store i32 %628, i32* %18, align 4
  %629 = load i32, i32* %17, align 4
  %630 = load i32, i32* %18, align 4
  %631 = sub i32 %630, %629
  store i32 %631, i32* %18, align 4
  %632 = load i32, i32* %17, align 4
  %633 = shl i32 %632, 8
  %634 = load i32, i32* %18, align 4
  %635 = xor i32 %634, %633
  store i32 %635, i32* %18, align 4
  %636 = load i32, i32* %17, align 4
  %637 = load i32, i32* %16, align 4
  %638 = sub i32 %637, %636
  store i32 %638, i32* %16, align 4
  %639 = load i32, i32* %18, align 4
  %640 = load i32, i32* %16, align 4
  %641 = sub i32 %640, %639
  store i32 %641, i32* %16, align 4
  %642 = load i32, i32* %18, align 4
  %643 = lshr i32 %642, 13
  %644 = load i32, i32* %16, align 4
  %645 = xor i32 %644, %643
  store i32 %645, i32* %16, align 4
  %646 = load i32, i32* %18, align 4
  %647 = load i32, i32* %17, align 4
  %648 = sub i32 %647, %646
  store i32 %648, i32* %17, align 4
  %649 = load i32, i32* %16, align 4
  %650 = load i32, i32* %17, align 4
  %651 = sub i32 %650, %649
  store i32 %651, i32* %17, align 4
  %652 = load i32, i32* %16, align 4
  %653 = lshr i32 %652, 12
  %654 = load i32, i32* %17, align 4
  %655 = xor i32 %654, %653
  store i32 %655, i32* %17, align 4
  %656 = load i32, i32* %16, align 4
  %657 = load i32, i32* %18, align 4
  %658 = sub i32 %657, %656
  store i32 %658, i32* %18, align 4
  %659 = load i32, i32* %17, align 4
  %660 = load i32, i32* %18, align 4
  %661 = sub i32 %660, %659
  store i32 %661, i32* %18, align 4
  %662 = load i32, i32* %17, align 4
  %663 = shl i32 %662, 16
  %664 = load i32, i32* %18, align 4
  %665 = xor i32 %664, %663
  store i32 %665, i32* %18, align 4
  %666 = load i32, i32* %17, align 4
  %667 = load i32, i32* %16, align 4
  %668 = sub i32 %667, %666
  store i32 %668, i32* %16, align 4
  %669 = load i32, i32* %18, align 4
  %670 = load i32, i32* %16, align 4
  %671 = sub i32 %670, %669
  store i32 %671, i32* %16, align 4
  %672 = load i32, i32* %18, align 4
  %673 = lshr i32 %672, 5
  %674 = load i32, i32* %16, align 4
  %675 = xor i32 %674, %673
  store i32 %675, i32* %16, align 4
  %676 = load i32, i32* %18, align 4
  %677 = load i32, i32* %17, align 4
  %678 = sub i32 %677, %676
  store i32 %678, i32* %17, align 4
  %679 = load i32, i32* %16, align 4
  %680 = load i32, i32* %17, align 4
  %681 = sub i32 %680, %679
  store i32 %681, i32* %17, align 4
  %682 = load i32, i32* %16, align 4
  %683 = lshr i32 %682, 3
  %684 = load i32, i32* %17, align 4
  %685 = xor i32 %684, %683
  store i32 %685, i32* %17, align 4
  %686 = load i32, i32* %16, align 4
  %687 = load i32, i32* %18, align 4
  %688 = sub i32 %687, %686
  store i32 %688, i32* %18, align 4
  %689 = load i32, i32* %17, align 4
  %690 = load i32, i32* %18, align 4
  %691 = sub i32 %690, %689
  store i32 %691, i32* %18, align 4
  %692 = load i32, i32* %17, align 4
  %693 = shl i32 %692, 10
  %694 = load i32, i32* %18, align 4
  %695 = xor i32 %694, %693
  store i32 %695, i32* %18, align 4
  %696 = load i32, i32* %17, align 4
  %697 = load i32, i32* %16, align 4
  %698 = sub i32 %697, %696
  store i32 %698, i32* %16, align 4
  %699 = load i32, i32* %18, align 4
  %700 = load i32, i32* %16, align 4
  %701 = sub i32 %700, %699
  store i32 %701, i32* %16, align 4
  %702 = load i32, i32* %18, align 4
  %703 = lshr i32 %702, 15
  %704 = load i32, i32* %16, align 4
  %705 = xor i32 %704, %703
  store i32 %705, i32* %16, align 4
  br label %706

; <label>:706:                                    ; preds = %615
  %707 = load i8*, i8** %20, align 8
  %708 = getelementptr inbounds i8, i8* %707, i64 12
  store i8* %708, i8** %20, align 8
  %709 = load i32, i32* %19, align 4
  %710 = sub i32 %709, 12
  store i32 %710, i32* %19, align 4
  br label %539

; <label>:711:                                    ; preds = %539
  %712 = load i32, i32* %16, align 4
  %713 = add i32 %712, 8
  store i32 %713, i32* %16, align 4
  %714 = load i32, i32* %19, align 4
  switch i32 %714, label %801 [
    i32 11, label %715
    i32 10, label %723
    i32 9, label %731
    i32 8, label %739
    i32 7, label %747
    i32 6, label %755
    i32 5, label %763
    i32 4, label %770
    i32 3, label %778
    i32 2, label %786
    i32 1, label %794
  ]

; <label>:715:                                    ; preds = %711
  %716 = load i8*, i8** %20, align 8
  %717 = getelementptr inbounds i8, i8* %716, i64 10
  %718 = load i8, i8* %717, align 1
  %719 = zext i8 %718 to i32
  %720 = shl i32 %719, 24
  %721 = load i32, i32* %16, align 4
  %722 = add i32 %721, %720
  store i32 %722, i32* %16, align 4
  br label %723

; <label>:723:                                    ; preds = %711, %715
  %724 = load i8*, i8** %20, align 8
  %725 = getelementptr inbounds i8, i8* %724, i64 9
  %726 = load i8, i8* %725, align 1
  %727 = zext i8 %726 to i32
  %728 = shl i32 %727, 16
  %729 = load i32, i32* %16, align 4
  %730 = add i32 %729, %728
  store i32 %730, i32* %16, align 4
  br label %731

; <label>:731:                                    ; preds = %711, %723
  %732 = load i8*, i8** %20, align 8
  %733 = getelementptr inbounds i8, i8* %732, i64 8
  %734 = load i8, i8* %733, align 1
  %735 = zext i8 %734 to i32
  %736 = shl i32 %735, 8
  %737 = load i32, i32* %16, align 4
  %738 = add i32 %737, %736
  store i32 %738, i32* %16, align 4
  br label %739

; <label>:739:                                    ; preds = %711, %731
  %740 = load i8*, i8** %20, align 8
  %741 = getelementptr inbounds i8, i8* %740, i64 7
  %742 = load i8, i8* %741, align 1
  %743 = zext i8 %742 to i32
  %744 = shl i32 %743, 24
  %745 = load i32, i32* %18, align 4
  %746 = add i32 %745, %744
  store i32 %746, i32* %18, align 4
  br label %747

; <label>:747:                                    ; preds = %711, %739
  %748 = load i8*, i8** %20, align 8
  %749 = getelementptr inbounds i8, i8* %748, i64 6
  %750 = load i8, i8* %749, align 1
  %751 = zext i8 %750 to i32
  %752 = shl i32 %751, 16
  %753 = load i32, i32* %18, align 4
  %754 = add i32 %753, %752
  store i32 %754, i32* %18, align 4
  br label %755

; <label>:755:                                    ; preds = %711, %747
  %756 = load i8*, i8** %20, align 8
  %757 = getelementptr inbounds i8, i8* %756, i64 5
  %758 = load i8, i8* %757, align 1
  %759 = zext i8 %758 to i32
  %760 = shl i32 %759, 8
  %761 = load i32, i32* %18, align 4
  %762 = add i32 %761, %760
  store i32 %762, i32* %18, align 4
  br label %763

; <label>:763:                                    ; preds = %711, %755
  %764 = load i8*, i8** %20, align 8
  %765 = getelementptr inbounds i8, i8* %764, i64 4
  %766 = load i8, i8* %765, align 1
  %767 = zext i8 %766 to i32
  %768 = load i32, i32* %18, align 4
  %769 = add i32 %768, %767
  store i32 %769, i32* %18, align 4
  br label %770

; <label>:770:                                    ; preds = %711, %763
  %771 = load i8*, i8** %20, align 8
  %772 = getelementptr inbounds i8, i8* %771, i64 3
  %773 = load i8, i8* %772, align 1
  %774 = zext i8 %773 to i32
  %775 = shl i32 %774, 24
  %776 = load i32, i32* %17, align 4
  %777 = add i32 %776, %775
  store i32 %777, i32* %17, align 4
  br label %778

; <label>:778:                                    ; preds = %711, %770
  %779 = load i8*, i8** %20, align 8
  %780 = getelementptr inbounds i8, i8* %779, i64 2
  %781 = load i8, i8* %780, align 1
  %782 = zext i8 %781 to i32
  %783 = shl i32 %782, 16
  %784 = load i32, i32* %17, align 4
  %785 = add i32 %784, %783
  store i32 %785, i32* %17, align 4
  br label %786

; <label>:786:                                    ; preds = %711, %778
  %787 = load i8*, i8** %20, align 8
  %788 = getelementptr inbounds i8, i8* %787, i64 1
  %789 = load i8, i8* %788, align 1
  %790 = zext i8 %789 to i32
  %791 = shl i32 %790, 8
  %792 = load i32, i32* %17, align 4
  %793 = add i32 %792, %791
  store i32 %793, i32* %17, align 4
  br label %794

; <label>:794:                                    ; preds = %711, %786
  %795 = load i8*, i8** %20, align 8
  %796 = getelementptr inbounds i8, i8* %795, i64 0
  %797 = load i8, i8* %796, align 1
  %798 = zext i8 %797 to i32
  %799 = load i32, i32* %17, align 4
  %800 = add i32 %799, %798
  store i32 %800, i32* %17, align 4
  br label %801

; <label>:801:                                    ; preds = %794, %711
  br label %802

; <label>:802:                                    ; preds = %801
  %803 = load i32, i32* %18, align 4
  %804 = load i32, i32* %17, align 4
  %805 = sub i32 %804, %803
  store i32 %805, i32* %17, align 4
  %806 = load i32, i32* %16, align 4
  %807 = load i32, i32* %17, align 4
  %808 = sub i32 %807, %806
  store i32 %808, i32* %17, align 4
  %809 = load i32, i32* %16, align 4
  %810 = lshr i32 %809, 13
  %811 = load i32, i32* %17, align 4
  %812 = xor i32 %811, %810
  store i32 %812, i32* %17, align 4
  %813 = load i32, i32* %16, align 4
  %814 = load i32, i32* %18, align 4
  %815 = sub i32 %814, %813
  store i32 %815, i32* %18, align 4
  %816 = load i32, i32* %17, align 4
  %817 = load i32, i32* %18, align 4
  %818 = sub i32 %817, %816
  store i32 %818, i32* %18, align 4
  %819 = load i32, i32* %17, align 4
  %820 = shl i32 %819, 8
  %821 = load i32, i32* %18, align 4
  %822 = xor i32 %821, %820
  store i32 %822, i32* %18, align 4
  %823 = load i32, i32* %17, align 4
  %824 = load i32, i32* %16, align 4
  %825 = sub i32 %824, %823
  store i32 %825, i32* %16, align 4
  %826 = load i32, i32* %18, align 4
  %827 = load i32, i32* %16, align 4
  %828 = sub i32 %827, %826
  store i32 %828, i32* %16, align 4
  %829 = load i32, i32* %18, align 4
  %830 = lshr i32 %829, 13
  %831 = load i32, i32* %16, align 4
  %832 = xor i32 %831, %830
  store i32 %832, i32* %16, align 4
  %833 = load i32, i32* %18, align 4
  %834 = load i32, i32* %17, align 4
  %835 = sub i32 %834, %833
  store i32 %835, i32* %17, align 4
  %836 = load i32, i32* %16, align 4
  %837 = load i32, i32* %17, align 4
  %838 = sub i32 %837, %836
  store i32 %838, i32* %17, align 4
  %839 = load i32, i32* %16, align 4
  %840 = lshr i32 %839, 12
  %841 = load i32, i32* %17, align 4
  %842 = xor i32 %841, %840
  store i32 %842, i32* %17, align 4
  %843 = load i32, i32* %16, align 4
  %844 = load i32, i32* %18, align 4
  %845 = sub i32 %844, %843
  store i32 %845, i32* %18, align 4
  %846 = load i32, i32* %17, align 4
  %847 = load i32, i32* %18, align 4
  %848 = sub i32 %847, %846
  store i32 %848, i32* %18, align 4
  %849 = load i32, i32* %17, align 4
  %850 = shl i32 %849, 16
  %851 = load i32, i32* %18, align 4
  %852 = xor i32 %851, %850
  store i32 %852, i32* %18, align 4
  %853 = load i32, i32* %17, align 4
  %854 = load i32, i32* %16, align 4
  %855 = sub i32 %854, %853
  store i32 %855, i32* %16, align 4
  %856 = load i32, i32* %18, align 4
  %857 = load i32, i32* %16, align 4
  %858 = sub i32 %857, %856
  store i32 %858, i32* %16, align 4
  %859 = load i32, i32* %18, align 4
  %860 = lshr i32 %859, 5
  %861 = load i32, i32* %16, align 4
  %862 = xor i32 %861, %860
  store i32 %862, i32* %16, align 4
  %863 = load i32, i32* %18, align 4
  %864 = load i32, i32* %17, align 4
  %865 = sub i32 %864, %863
  store i32 %865, i32* %17, align 4
  %866 = load i32, i32* %16, align 4
  %867 = load i32, i32* %17, align 4
  %868 = sub i32 %867, %866
  store i32 %868, i32* %17, align 4
  %869 = load i32, i32* %16, align 4
  %870 = lshr i32 %869, 3
  %871 = load i32, i32* %17, align 4
  %872 = xor i32 %871, %870
  store i32 %872, i32* %17, align 4
  %873 = load i32, i32* %16, align 4
  %874 = load i32, i32* %18, align 4
  %875 = sub i32 %874, %873
  store i32 %875, i32* %18, align 4
  %876 = load i32, i32* %17, align 4
  %877 = load i32, i32* %18, align 4
  %878 = sub i32 %877, %876
  store i32 %878, i32* %18, align 4
  %879 = load i32, i32* %17, align 4
  %880 = shl i32 %879, 10
  %881 = load i32, i32* %18, align 4
  %882 = xor i32 %881, %880
  store i32 %882, i32* %18, align 4
  %883 = load i32, i32* %17, align 4
  %884 = load i32, i32* %16, align 4
  %885 = sub i32 %884, %883
  store i32 %885, i32* %16, align 4
  %886 = load i32, i32* %18, align 4
  %887 = load i32, i32* %16, align 4
  %888 = sub i32 %887, %886
  store i32 %888, i32* %16, align 4
  %889 = load i32, i32* %18, align 4
  %890 = lshr i32 %889, 15
  %891 = load i32, i32* %16, align 4
  %892 = xor i32 %891, %890
  store i32 %892, i32* %16, align 4
  br label %893

; <label>:893:                                    ; preds = %802
  br label %894

; <label>:894:                                    ; preds = %893
  br label %895

; <label>:895:                                    ; preds = %894
  br label %896

; <label>:896:                                    ; preds = %895
  %897 = load i32, i32* %16, align 4
  %898 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %899 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %898, i32 0, i32 2
  %900 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %899, i32 0, i32 7
  store i32 %897, i32* %900, align 4
  %901 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %902 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %901, i32 0, i32 0
  %903 = bitcast i64* %902 to i8*
  %904 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %905 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %904, i32 0, i32 2
  %906 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %905, i32 0, i32 5
  store i8* %903, i8** %906, align 8
  %907 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %908 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %907, i32 0, i32 2
  %909 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %908, i32 0, i32 6
  store i32 8, i32* %909, align 8
  %910 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %911 = icmp ne %struct.my_hash* %910, null
  br i1 %911, label %999, label %912

; <label>:912:                                    ; preds = %896
  %913 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %914 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %913, i32 0, i32 2
  %915 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %914, i32 0, i32 2
  store i8* null, i8** %915, align 8
  %916 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %917 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %916, i32 0, i32 2
  %918 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %917, i32 0, i32 1
  store i8* null, i8** %918, align 8
  br label %919

; <label>:919:                                    ; preds = %912
  %920 = call i8* @malloc(i64 64) #8
  %921 = bitcast i8* %920 to %struct.UT_hash_table*
  %922 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %923 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %922, i32 0, i32 2
  %924 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %923, i32 0, i32 0
  store %struct.UT_hash_table* %921, %struct.UT_hash_table** %924, align 8
  %925 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %926 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %925, i32 0, i32 2
  %927 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %926, i32 0, i32 0
  %928 = load %struct.UT_hash_table*, %struct.UT_hash_table** %927, align 8
  %929 = icmp ne %struct.UT_hash_table* %928, null
  br i1 %929, label %931, label %930

; <label>:930:                                    ; preds = %919
  call void @exit(i32 -1) #7
  unreachable

; <label>:931:                                    ; preds = %919
  %932 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %933 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %932, i32 0, i32 2
  %934 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %933, i32 0, i32 0
  %935 = load %struct.UT_hash_table*, %struct.UT_hash_table** %934, align 8
  %936 = bitcast %struct.UT_hash_table* %935 to i8*
  call void @llvm.memset.p0i8.i64(i8* %936, i8 0, i64 64, i32 8, i1 false)
  %937 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %938 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %937, i32 0, i32 2
  %939 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %940 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %939, i32 0, i32 2
  %941 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %940, i32 0, i32 0
  %942 = load %struct.UT_hash_table*, %struct.UT_hash_table** %941, align 8
  %943 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %942, i32 0, i32 4
  store %struct.UT_hash_handle* %938, %struct.UT_hash_handle** %943, align 8
  %944 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %945 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %944, i32 0, i32 2
  %946 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %945, i32 0, i32 0
  %947 = load %struct.UT_hash_table*, %struct.UT_hash_table** %946, align 8
  %948 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %947, i32 0, i32 1
  store i32 32, i32* %948, align 8
  %949 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %950 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %949, i32 0, i32 2
  %951 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %950, i32 0, i32 0
  %952 = load %struct.UT_hash_table*, %struct.UT_hash_table** %951, align 8
  %953 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %952, i32 0, i32 2
  store i32 5, i32* %953, align 4
  %954 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %955 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %954, i32 0, i32 2
  %956 = bitcast %struct.UT_hash_handle* %955 to i8*
  %957 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %958 = bitcast %struct.my_hash* %957 to i8*
  %959 = ptrtoint i8* %956 to i64
  %960 = ptrtoint i8* %958 to i64
  %961 = sub i64 %959, %960
  %962 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %963 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %962, i32 0, i32 2
  %964 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %963, i32 0, i32 0
  %965 = load %struct.UT_hash_table*, %struct.UT_hash_table** %964, align 8
  %966 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %965, i32 0, i32 5
  store i64 %961, i64* %966, align 8
  %967 = call i8* @malloc(i64 512) #8
  %968 = bitcast i8* %967 to %struct.UT_hash_bucket*
  %969 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %970 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %969, i32 0, i32 2
  %971 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %970, i32 0, i32 0
  %972 = load %struct.UT_hash_table*, %struct.UT_hash_table** %971, align 8
  %973 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %972, i32 0, i32 0
  store %struct.UT_hash_bucket* %968, %struct.UT_hash_bucket** %973, align 8
  %974 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %975 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %974, i32 0, i32 2
  %976 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %975, i32 0, i32 0
  %977 = load %struct.UT_hash_table*, %struct.UT_hash_table** %976, align 8
  %978 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %977, i32 0, i32 10
  store i32 -1609490463, i32* %978, align 8
  %979 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %980 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %979, i32 0, i32 2
  %981 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %980, i32 0, i32 0
  %982 = load %struct.UT_hash_table*, %struct.UT_hash_table** %981, align 8
  %983 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %982, i32 0, i32 0
  %984 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %983, align 8
  %985 = icmp ne %struct.UT_hash_bucket* %984, null
  br i1 %985, label %987, label %986

; <label>:986:                                    ; preds = %931
  call void @exit(i32 -1) #7
  unreachable

; <label>:987:                                    ; preds = %931
  %988 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %989 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %988, i32 0, i32 2
  %990 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %989, i32 0, i32 0
  %991 = load %struct.UT_hash_table*, %struct.UT_hash_table** %990, align 8
  %992 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %991, i32 0, i32 0
  %993 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %992, align 8
  %994 = bitcast %struct.UT_hash_bucket* %993 to i8*
  call void @llvm.memset.p0i8.i64(i8* %994, i8 0, i64 512, i32 8, i1 false)
  br label %995

; <label>:995:                                    ; preds = %987
  br label %996

; <label>:996:                                    ; preds = %995
  br label %997

; <label>:997:                                    ; preds = %996
  %998 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  store %struct.my_hash* %998, %struct.my_hash** %9, align 8
  br label %1046

; <label>:999:                                    ; preds = %896
  %1000 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1001 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1000, i32 0, i32 2
  %1002 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1001, i32 0, i32 0
  %1003 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1002, align 8
  %1004 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1005 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1004, i32 0, i32 2
  %1006 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1005, i32 0, i32 0
  store %struct.UT_hash_table* %1003, %struct.UT_hash_table** %1006, align 8
  br label %1007

; <label>:1007:                                   ; preds = %999
  %1008 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1009 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1008, i32 0, i32 2
  %1010 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1009, i32 0, i32 2
  store i8* null, i8** %1010, align 8
  %1011 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1012 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1011, i32 0, i32 2
  %1013 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1012, i32 0, i32 0
  %1014 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1013, align 8
  %1015 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1014, i32 0, i32 4
  %1016 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1015, align 8
  %1017 = bitcast %struct.UT_hash_handle* %1016 to i8*
  %1018 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1019 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1018, i32 0, i32 2
  %1020 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1019, i32 0, i32 0
  %1021 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1020, align 8
  %1022 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1021, i32 0, i32 5
  %1023 = load i64, i64* %1022, align 8
  %1024 = sub i64 0, %1023
  %1025 = getelementptr inbounds i8, i8* %1017, i64 %1024
  %1026 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1027 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1026, i32 0, i32 2
  %1028 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1027, i32 0, i32 1
  store i8* %1025, i8** %1028, align 8
  %1029 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1030 = bitcast %struct.my_hash* %1029 to i8*
  %1031 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1032 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1031, i32 0, i32 2
  %1033 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1032, i32 0, i32 0
  %1034 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1033, align 8
  %1035 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1034, i32 0, i32 4
  %1036 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1035, align 8
  %1037 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1036, i32 0, i32 2
  store i8* %1030, i8** %1037, align 8
  %1038 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1039 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1038, i32 0, i32 2
  %1040 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1041 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1040, i32 0, i32 2
  %1042 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1041, i32 0, i32 0
  %1043 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1042, align 8
  %1044 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1043, i32 0, i32 4
  store %struct.UT_hash_handle* %1039, %struct.UT_hash_handle** %1044, align 8
  br label %1045

; <label>:1045:                                   ; preds = %1007
  br label %1046

; <label>:1046:                                   ; preds = %1045, %997
  br label %1047

; <label>:1047:                                   ; preds = %1046
  %1048 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1049 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1048, i32 0, i32 2
  %1050 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1049, i32 0, i32 0
  %1051 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1050, align 8
  %1052 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1051, i32 0, i32 3
  %1053 = load i32, i32* %1052, align 8
  %1054 = add i32 %1053, 1
  store i32 %1054, i32* %1052, align 8
  br label %1055

; <label>:1055:                                   ; preds = %1047
  %1056 = load i32, i32* %16, align 4
  %1057 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1058 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1057, i32 0, i32 2
  %1059 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1058, i32 0, i32 0
  %1060 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1059, align 8
  %1061 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1060, i32 0, i32 1
  %1062 = load i32, i32* %1061, align 8
  %1063 = sub i32 %1062, 1
  %1064 = and i32 %1056, %1063
  store i32 %1064, i32* %21, align 4
  br label %1065

; <label>:1065:                                   ; preds = %1055
  br label %1066

; <label>:1066:                                   ; preds = %1065
  %1067 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1068 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1067, i32 0, i32 2
  %1069 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1068, i32 0, i32 0
  %1070 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1069, align 8
  %1071 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1070, i32 0, i32 0
  %1072 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1071, align 8
  %1073 = load i32, i32* %21, align 4
  %1074 = zext i32 %1073 to i64
  %1075 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1072, i64 %1074
  store %struct.UT_hash_bucket* %1075, %struct.UT_hash_bucket** %22, align 8
  %1076 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %22, align 8
  %1077 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1076, i32 0, i32 1
  %1078 = load i32, i32* %1077, align 8
  %1079 = add i32 %1078, 1
  store i32 %1079, i32* %1077, align 8
  %1080 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %22, align 8
  %1081 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1080, i32 0, i32 0
  %1082 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1081, align 8
  %1083 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1084 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1083, i32 0, i32 2
  %1085 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1084, i32 0, i32 4
  store %struct.UT_hash_handle* %1082, %struct.UT_hash_handle** %1085, align 8
  %1086 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1087 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1086, i32 0, i32 2
  %1088 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1087, i32 0, i32 3
  store %struct.UT_hash_handle* null, %struct.UT_hash_handle** %1088, align 8
  %1089 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %22, align 8
  %1090 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1089, i32 0, i32 0
  %1091 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1090, align 8
  %1092 = icmp ne %struct.UT_hash_handle* %1091, null
  br i1 %1092, label %1093, label %1100

; <label>:1093:                                   ; preds = %1066
  %1094 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1095 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1094, i32 0, i32 2
  %1096 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %22, align 8
  %1097 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1096, i32 0, i32 0
  %1098 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1097, align 8
  %1099 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1098, i32 0, i32 3
  store %struct.UT_hash_handle* %1095, %struct.UT_hash_handle** %1099, align 8
  br label %1100

; <label>:1100:                                   ; preds = %1093, %1066
  %1101 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1102 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1101, i32 0, i32 2
  %1103 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %22, align 8
  %1104 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1103, i32 0, i32 0
  store %struct.UT_hash_handle* %1102, %struct.UT_hash_handle** %1104, align 8
  %1105 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %22, align 8
  %1106 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1105, i32 0, i32 1
  %1107 = load i32, i32* %1106, align 8
  %1108 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %22, align 8
  %1109 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1108, i32 0, i32 2
  %1110 = load i32, i32* %1109, align 4
  %1111 = add i32 %1110, 1
  %1112 = mul i32 %1111, 10
  %1113 = icmp uge i32 %1107, %1112
  br i1 %1113, label %1114, label %1371

; <label>:1114:                                   ; preds = %1100
  %1115 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1116 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1115, i32 0, i32 2
  %1117 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1116, i32 0, i32 0
  %1118 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1117, align 8
  %1119 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1118, i32 0, i32 9
  %1120 = load i32, i32* %1119, align 4
  %1121 = icmp ne i32 %1120, 0
  br i1 %1121, label %1371, label %1122

; <label>:1122:                                   ; preds = %1114
  br label %1123

; <label>:1123:                                   ; preds = %1122
  %1124 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1125 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1124, i32 0, i32 2
  %1126 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1125, i32 0, i32 0
  %1127 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1126, align 8
  %1128 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1127, i32 0, i32 1
  %1129 = load i32, i32* %1128, align 8
  %1130 = zext i32 %1129 to i64
  %1131 = mul i64 2, %1130
  %1132 = mul i64 %1131, 16
  %1133 = call i8* @malloc(i64 %1132) #8
  %1134 = bitcast i8* %1133 to %struct.UT_hash_bucket*
  store %struct.UT_hash_bucket* %1134, %struct.UT_hash_bucket** %27, align 8
  %1135 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %27, align 8
  %1136 = icmp ne %struct.UT_hash_bucket* %1135, null
  br i1 %1136, label %1138, label %1137

; <label>:1137:                                   ; preds = %1123
  call void @exit(i32 -1) #7
  unreachable

; <label>:1138:                                   ; preds = %1123
  %1139 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %27, align 8
  %1140 = bitcast %struct.UT_hash_bucket* %1139 to i8*
  %1141 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1142 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1141, i32 0, i32 2
  %1143 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1142, i32 0, i32 0
  %1144 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1143, align 8
  %1145 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1144, i32 0, i32 1
  %1146 = load i32, i32* %1145, align 8
  %1147 = zext i32 %1146 to i64
  %1148 = mul i64 2, %1147
  %1149 = mul i64 %1148, 16
  call void @llvm.memset.p0i8.i64(i8* %1140, i8 0, i64 %1149, i32 8, i1 false)
  %1150 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1151 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1150, i32 0, i32 2
  %1152 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1151, i32 0, i32 0
  %1153 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1152, align 8
  %1154 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1153, i32 0, i32 3
  %1155 = load i32, i32* %1154, align 8
  %1156 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1157 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1156, i32 0, i32 2
  %1158 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1157, i32 0, i32 0
  %1159 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1158, align 8
  %1160 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1159, i32 0, i32 2
  %1161 = load i32, i32* %1160, align 4
  %1162 = add i32 %1161, 1
  %1163 = lshr i32 %1155, %1162
  %1164 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1165 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1164, i32 0, i32 2
  %1166 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1165, i32 0, i32 0
  %1167 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1166, align 8
  %1168 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1167, i32 0, i32 3
  %1169 = load i32, i32* %1168, align 8
  %1170 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1171 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1170, i32 0, i32 2
  %1172 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1171, i32 0, i32 0
  %1173 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1172, align 8
  %1174 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1173, i32 0, i32 1
  %1175 = load i32, i32* %1174, align 8
  %1176 = mul i32 %1175, 2
  %1177 = sub i32 %1176, 1
  %1178 = and i32 %1169, %1177
  %1179 = icmp ne i32 %1178, 0
  %1180 = zext i1 %1179 to i64
  %1181 = select i1 %1179, i32 1, i32 0
  %1182 = add i32 %1163, %1181
  %1183 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1184 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1183, i32 0, i32 2
  %1185 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1184, i32 0, i32 0
  %1186 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1185, align 8
  %1187 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1186, i32 0, i32 6
  store i32 %1182, i32* %1187, align 8
  %1188 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1189 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1188, i32 0, i32 2
  %1190 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1189, i32 0, i32 0
  %1191 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1190, align 8
  %1192 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1191, i32 0, i32 7
  store i32 0, i32* %1192, align 4
  store i32 0, i32* %24, align 4
  br label %1193

; <label>:1193:                                   ; preds = %1294, %1138
  %1194 = load i32, i32* %24, align 4
  %1195 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1196 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1195, i32 0, i32 2
  %1197 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1196, i32 0, i32 0
  %1198 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1197, align 8
  %1199 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1198, i32 0, i32 1
  %1200 = load i32, i32* %1199, align 8
  %1201 = icmp ult i32 %1194, %1200
  br i1 %1201, label %1202, label %1297

; <label>:1202:                                   ; preds = %1193
  %1203 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1204 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1203, i32 0, i32 2
  %1205 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1204, i32 0, i32 0
  %1206 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1205, align 8
  %1207 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1206, i32 0, i32 0
  %1208 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1207, align 8
  %1209 = load i32, i32* %24, align 4
  %1210 = zext i32 %1209 to i64
  %1211 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1208, i64 %1210
  %1212 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1211, i32 0, i32 0
  %1213 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1212, align 8
  store %struct.UT_hash_handle* %1213, %struct.UT_hash_handle** %25, align 8
  br label %1214

; <label>:1214:                                   ; preds = %1288, %1202
  %1215 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %25, align 8
  %1216 = icmp ne %struct.UT_hash_handle* %1215, null
  br i1 %1216, label %1217, label %1293

; <label>:1217:                                   ; preds = %1214
  %1218 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %25, align 8
  %1219 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1218, i32 0, i32 4
  %1220 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1219, align 8
  store %struct.UT_hash_handle* %1220, %struct.UT_hash_handle** %26, align 8
  br label %1221

; <label>:1221:                                   ; preds = %1217
  %1222 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %25, align 8
  %1223 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1222, i32 0, i32 7
  %1224 = load i32, i32* %1223, align 4
  %1225 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1226 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1225, i32 0, i32 2
  %1227 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1226, i32 0, i32 0
  %1228 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1227, align 8
  %1229 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1228, i32 0, i32 1
  %1230 = load i32, i32* %1229, align 8
  %1231 = mul i32 %1230, 2
  %1232 = sub i32 %1231, 1
  %1233 = and i32 %1224, %1232
  store i32 %1233, i32* %23, align 4
  br label %1234

; <label>:1234:                                   ; preds = %1221
  %1235 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %27, align 8
  %1236 = load i32, i32* %23, align 4
  %1237 = zext i32 %1236 to i64
  %1238 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1235, i64 %1237
  store %struct.UT_hash_bucket* %1238, %struct.UT_hash_bucket** %28, align 8
  %1239 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1240 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1239, i32 0, i32 1
  %1241 = load i32, i32* %1240, align 8
  %1242 = add i32 %1241, 1
  store i32 %1242, i32* %1240, align 8
  %1243 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1244 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1243, i32 0, i32 2
  %1245 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1244, i32 0, i32 0
  %1246 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1245, align 8
  %1247 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1246, i32 0, i32 6
  %1248 = load i32, i32* %1247, align 8
  %1249 = icmp ugt i32 %1242, %1248
  br i1 %1249, label %1250, label %1270

; <label>:1250:                                   ; preds = %1234
  %1251 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1252 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1251, i32 0, i32 2
  %1253 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1252, i32 0, i32 0
  %1254 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1253, align 8
  %1255 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1254, i32 0, i32 7
  %1256 = load i32, i32* %1255, align 4
  %1257 = add i32 %1256, 1
  store i32 %1257, i32* %1255, align 4
  %1258 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1259 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1258, i32 0, i32 1
  %1260 = load i32, i32* %1259, align 8
  %1261 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1262 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1261, i32 0, i32 2
  %1263 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1262, i32 0, i32 0
  %1264 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1263, align 8
  %1265 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1264, i32 0, i32 6
  %1266 = load i32, i32* %1265, align 8
  %1267 = udiv i32 %1260, %1266
  %1268 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1269 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1268, i32 0, i32 2
  store i32 %1267, i32* %1269, align 4
  br label %1270

; <label>:1270:                                   ; preds = %1250, %1234
  %1271 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %25, align 8
  %1272 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1271, i32 0, i32 3
  store %struct.UT_hash_handle* null, %struct.UT_hash_handle** %1272, align 8
  %1273 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1274 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1273, i32 0, i32 0
  %1275 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1274, align 8
  %1276 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %25, align 8
  %1277 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1276, i32 0, i32 4
  store %struct.UT_hash_handle* %1275, %struct.UT_hash_handle** %1277, align 8
  %1278 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1279 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1278, i32 0, i32 0
  %1280 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1279, align 8
  %1281 = icmp ne %struct.UT_hash_handle* %1280, null
  br i1 %1281, label %1282, label %1288

; <label>:1282:                                   ; preds = %1270
  %1283 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %25, align 8
  %1284 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1285 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1284, i32 0, i32 0
  %1286 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1285, align 8
  %1287 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1286, i32 0, i32 3
  store %struct.UT_hash_handle* %1283, %struct.UT_hash_handle** %1287, align 8
  br label %1288

; <label>:1288:                                   ; preds = %1282, %1270
  %1289 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %25, align 8
  %1290 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1291 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1290, i32 0, i32 0
  store %struct.UT_hash_handle* %1289, %struct.UT_hash_handle** %1291, align 8
  %1292 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  store %struct.UT_hash_handle* %1292, %struct.UT_hash_handle** %25, align 8
  br label %1214

; <label>:1293:                                   ; preds = %1214
  br label %1294

; <label>:1294:                                   ; preds = %1293
  %1295 = load i32, i32* %24, align 4
  %1296 = add i32 %1295, 1
  store i32 %1296, i32* %24, align 4
  br label %1193

; <label>:1297:                                   ; preds = %1193
  %1298 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1299 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1298, i32 0, i32 2
  %1300 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1299, i32 0, i32 0
  %1301 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1300, align 8
  %1302 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1301, i32 0, i32 0
  %1303 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1302, align 8
  %1304 = bitcast %struct.UT_hash_bucket* %1303 to i8*
  call void @free(i8* %1304)
  %1305 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1306 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1305, i32 0, i32 2
  %1307 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1306, i32 0, i32 0
  %1308 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1307, align 8
  %1309 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1308, i32 0, i32 1
  %1310 = load i32, i32* %1309, align 8
  %1311 = mul i32 %1310, 2
  store i32 %1311, i32* %1309, align 8
  %1312 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1313 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1312, i32 0, i32 2
  %1314 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1313, i32 0, i32 0
  %1315 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1314, align 8
  %1316 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1315, i32 0, i32 2
  %1317 = load i32, i32* %1316, align 4
  %1318 = add i32 %1317, 1
  store i32 %1318, i32* %1316, align 4
  %1319 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %27, align 8
  %1320 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1321 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1320, i32 0, i32 2
  %1322 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1321, i32 0, i32 0
  %1323 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1322, align 8
  %1324 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1323, i32 0, i32 0
  store %struct.UT_hash_bucket* %1319, %struct.UT_hash_bucket** %1324, align 8
  %1325 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1326 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1325, i32 0, i32 2
  %1327 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1326, i32 0, i32 0
  %1328 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1327, align 8
  %1329 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1328, i32 0, i32 7
  %1330 = load i32, i32* %1329, align 4
  %1331 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1332 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1331, i32 0, i32 2
  %1333 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1332, i32 0, i32 0
  %1334 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1333, align 8
  %1335 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1334, i32 0, i32 3
  %1336 = load i32, i32* %1335, align 8
  %1337 = lshr i32 %1336, 1
  %1338 = icmp ugt i32 %1330, %1337
  br i1 %1338, label %1339, label %1347

; <label>:1339:                                   ; preds = %1297
  %1340 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1341 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1340, i32 0, i32 2
  %1342 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1341, i32 0, i32 0
  %1343 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1342, align 8
  %1344 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1343, i32 0, i32 8
  %1345 = load i32, i32* %1344, align 8
  %1346 = add i32 %1345, 1
  br label %1348

; <label>:1347:                                   ; preds = %1297
  br label %1348

; <label>:1348:                                   ; preds = %1347, %1339
  %1349 = phi i32 [ %1346, %1339 ], [ 0, %1347 ]
  %1350 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1351 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1350, i32 0, i32 2
  %1352 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1351, i32 0, i32 0
  %1353 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1352, align 8
  %1354 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1353, i32 0, i32 8
  store i32 %1349, i32* %1354, align 8
  %1355 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1356 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1355, i32 0, i32 2
  %1357 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1356, i32 0, i32 0
  %1358 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1357, align 8
  %1359 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1358, i32 0, i32 8
  %1360 = load i32, i32* %1359, align 8
  %1361 = icmp ugt i32 %1360, 1
  br i1 %1361, label %1362, label %1368

; <label>:1362:                                   ; preds = %1348
  %1363 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1364 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1363, i32 0, i32 2
  %1365 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1364, i32 0, i32 0
  %1366 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1365, align 8
  %1367 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1366, i32 0, i32 9
  store i32 1, i32* %1367, align 4
  br label %1368

; <label>:1368:                                   ; preds = %1362, %1348
  br label %1369

; <label>:1369:                                   ; preds = %1368
  br label %1370

; <label>:1370:                                   ; preds = %1369
  br label %1371

; <label>:1371:                                   ; preds = %1370, %1114, %1100
  br label %1372

; <label>:1372:                                   ; preds = %1371
  br label %1373

; <label>:1373:                                   ; preds = %1372
  br label %1374

; <label>:1374:                                   ; preds = %1373
  br label %1375

; <label>:1375:                                   ; preds = %1374
  br label %1380

; <label>:1376:                                   ; preds = %521
  %1377 = load i64, i64* %6, align 8
  %1378 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1379 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1378, i32 0, i32 1
  store i64 %1377, i64* %1379, align 8
  br label %1380

; <label>:1380:                                   ; preds = %1376, %1375
  ret i64 39
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_hash_45set_33(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %7 = load i64, i64* %2, align 8
  %8 = call i64 @expect_cons(i64 %7, i64* %3)
  store i64 %8, i64* %4, align 8
  %9 = load i64, i64* %3, align 8
  %10 = call i64 @expect_cons(i64 %9, i64* %3)
  store i64 %10, i64* %5, align 8
  %11 = load i64, i64* %3, align 8
  %12 = call i64 @expect_cons(i64 %11, i64* %3)
  store i64 %12, i64* %6, align 8
  %13 = load i64, i64* %3, align 8
  %14 = icmp ne i64 %13, 0
  br i1 %14, label %15, label %16

; <label>:15:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %16

; <label>:16:                                     ; preds = %15, %1
  %17 = load i64, i64* %4, align 8
  %18 = load i64, i64* %5, align 8
  %19 = load i64, i64* %6, align 8
  %20 = call i64 @prim_hash_45set_33(i64 %17, i64 %18, i64 %19)
  ret i64 %20
}

attributes #0 = { noinline ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noinline nounwind ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind }
attributes #7 = { noreturn }
attributes #8 = { allocsize(0) }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"Apple LLVM version 9.0.0 (clang-900.0.39.2)"}


;;;;;;

define void @proc_main() {
  %cloptr174461 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174462 = getelementptr inbounds i64, i64* %cloptr174461, i64 0                ; &cloptr174461[0]
  %f174463 = ptrtoint void(i64,i64)* @lam174459 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174463, i64* %eptr174462                                               ; store fptr
  %arg171572 = ptrtoint i64* %cloptr174461 to i64                                    ; closure cast; i64* -> i64
  %cloptr174464 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174465 = getelementptr inbounds i64, i64* %cloptr174464, i64 0                ; &cloptr174464[0]
  %f174466 = ptrtoint void(i64,i64)* @lam174456 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174466, i64* %eptr174465                                               ; store fptr
  %arg171571 = ptrtoint i64* %cloptr174464 to i64                                    ; closure cast; i64* -> i64
  %cloptr174467 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174468 = getelementptr inbounds i64, i64* %cloptr174467, i64 0                ; &cloptr174467[0]
  %f174469 = ptrtoint void(i64,i64)* @lam173747 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174469, i64* %eptr174468                                               ; store fptr
  %arg171570 = ptrtoint i64* %cloptr174467 to i64                                    ; closure cast; i64* -> i64
  %rva173732 = add i64 0, 0                                                          ; quoted ()
  %rva173731 = call i64 @prim_cons(i64 %arg171570, i64 %rva173732)                   ; call prim_cons
  %rva173730 = call i64 @prim_cons(i64 %arg171571, i64 %rva173731)                   ; call prim_cons
  %cloptr174470 = inttoptr i64 %arg171572 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr174471 = getelementptr inbounds i64, i64* %cloptr174470, i64 0               ; &cloptr174470[0]
  %f174473 = load i64, i64* %i0ptr174471, align 8                                    ; load; *i0ptr174471
  %fptr174472 = inttoptr i64 %f174473 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174472(i64 %arg171572, i64 %rva173730)              ; tail call
  ret void
}


define i32 @main() {
  call fastcc void @proc_main()
  ret i32 0
}



define void @lam174459(i64 %env174460, i64 %rvp172555) {
  %cont171562 = call i64 @prim_car(i64 %rvp172555)                                   ; call prim_car
  %rvp172554 = call i64 @prim_cdr(i64 %rvp172555)                                    ; call prim_cdr
  %BHL$yu = call i64 @prim_car(i64 %rvp172554)                                       ; call prim_car
  %na172550 = call i64 @prim_cdr(i64 %rvp172554)                                     ; call prim_cdr
  %rva172553 = add i64 0, 0                                                          ; quoted ()
  %rva172552 = call i64 @prim_cons(i64 %BHL$yu, i64 %rva172553)                      ; call prim_cons
  %rva172551 = call i64 @prim_cons(i64 %cont171562, i64 %rva172552)                  ; call prim_cons
  %cloptr174474 = inttoptr i64 %BHL$yu to i64*                                       ; closure/env cast; i64 -> i64*
  %i0ptr174475 = getelementptr inbounds i64, i64* %cloptr174474, i64 0               ; &cloptr174474[0]
  %f174477 = load i64, i64* %i0ptr174475, align 8                                    ; load; *i0ptr174475
  %fptr174476 = inttoptr i64 %f174477 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174476(i64 %BHL$yu, i64 %rva172551)                 ; tail call
  ret void
}


define void @lam174456(i64 %env174457, i64 %rvp173701) {
  %_95171345 = call i64 @prim_car(i64 %rvp173701)                                    ; call prim_car
  %rvp173700 = call i64 @prim_cdr(i64 %rvp173701)                                    ; call prim_cdr
  %do0$Ycmb = call i64 @prim_car(i64 %rvp173700)                                     ; call prim_car
  %na172557 = call i64 @prim_cdr(i64 %rvp173700)                                     ; call prim_cdr
  %cloptr174478 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr174480 = getelementptr inbounds i64, i64* %cloptr174478, i64 1                ; &eptr174480[1]
  store i64 %do0$Ycmb, i64* %eptr174480                                              ; *eptr174480 = %do0$Ycmb
  %eptr174479 = getelementptr inbounds i64, i64* %cloptr174478, i64 0                ; &cloptr174478[0]
  %f174481 = ptrtoint void(i64,i64)* @lam174454 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174481, i64* %eptr174479                                               ; store fptr
  %arg171577 = ptrtoint i64* %cloptr174478 to i64                                    ; closure cast; i64* -> i64
  %cloptr174482 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174483 = getelementptr inbounds i64, i64* %cloptr174482, i64 0                ; &cloptr174482[0]
  %f174484 = ptrtoint void(i64,i64)* @lam173760 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174484, i64* %eptr174483                                               ; store fptr
  %arg171576 = ptrtoint i64* %cloptr174482 to i64                                    ; closure cast; i64* -> i64
  %rva173699 = add i64 0, 0                                                          ; quoted ()
  %rva173698 = call i64 @prim_cons(i64 %arg171576, i64 %rva173699)                   ; call prim_cons
  %rva173697 = call i64 @prim_cons(i64 %arg171577, i64 %rva173698)                   ; call prim_cons
  %cloptr174485 = inttoptr i64 %do0$Ycmb to i64*                                     ; closure/env cast; i64 -> i64*
  %i0ptr174486 = getelementptr inbounds i64, i64* %cloptr174485, i64 0               ; &cloptr174485[0]
  %f174488 = load i64, i64* %i0ptr174486, align 8                                    ; load; *i0ptr174486
  %fptr174487 = inttoptr i64 %f174488 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174487(i64 %do0$Ycmb, i64 %rva173697)               ; tail call
  ret void
}


define void @lam174454(i64 %env174455, i64 %rvp173667) {
  %envptr174489 = inttoptr i64 %env174455 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174490 = getelementptr inbounds i64, i64* %envptr174489, i64 1              ; &envptr174489[1]
  %do0$Ycmb = load i64, i64* %envptr174490, align 8                                  ; load; *envptr174490
  %_95171346 = call i64 @prim_car(i64 %rvp173667)                                    ; call prim_car
  %rvp173666 = call i64 @prim_cdr(i64 %rvp173667)                                    ; call prim_cdr
  %xK1$_37foldr1 = call i64 @prim_car(i64 %rvp173666)                                ; call prim_car
  %na172559 = call i64 @prim_cdr(i64 %rvp173666)                                     ; call prim_cdr
  %cloptr174491 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr174493 = getelementptr inbounds i64, i64* %cloptr174491, i64 1                ; &eptr174493[1]
  %eptr174494 = getelementptr inbounds i64, i64* %cloptr174491, i64 2                ; &eptr174494[2]
  store i64 %xK1$_37foldr1, i64* %eptr174493                                         ; *eptr174493 = %xK1$_37foldr1
  store i64 %do0$Ycmb, i64* %eptr174494                                              ; *eptr174494 = %do0$Ycmb
  %eptr174492 = getelementptr inbounds i64, i64* %cloptr174491, i64 0                ; &cloptr174491[0]
  %f174495 = ptrtoint void(i64,i64)* @lam174452 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174495, i64* %eptr174492                                               ; store fptr
  %arg171580 = ptrtoint i64* %cloptr174491 to i64                                    ; closure cast; i64* -> i64
  %cloptr174496 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174497 = getelementptr inbounds i64, i64* %cloptr174496, i64 0                ; &cloptr174496[0]
  %f174498 = ptrtoint void(i64,i64)* @lam173778 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174498, i64* %eptr174497                                               ; store fptr
  %arg171579 = ptrtoint i64* %cloptr174496 to i64                                    ; closure cast; i64* -> i64
  %rva173665 = add i64 0, 0                                                          ; quoted ()
  %rva173664 = call i64 @prim_cons(i64 %arg171579, i64 %rva173665)                   ; call prim_cons
  %rva173663 = call i64 @prim_cons(i64 %arg171580, i64 %rva173664)                   ; call prim_cons
  %cloptr174499 = inttoptr i64 %do0$Ycmb to i64*                                     ; closure/env cast; i64 -> i64*
  %i0ptr174500 = getelementptr inbounds i64, i64* %cloptr174499, i64 0               ; &cloptr174499[0]
  %f174502 = load i64, i64* %i0ptr174500, align 8                                    ; load; *i0ptr174500
  %fptr174501 = inttoptr i64 %f174502 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174501(i64 %do0$Ycmb, i64 %rva173663)               ; tail call
  ret void
}


define void @lam174452(i64 %env174453, i64 %rvp173629) {
  %envptr174503 = inttoptr i64 %env174453 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174504 = getelementptr inbounds i64, i64* %envptr174503, i64 2              ; &envptr174503[2]
  %do0$Ycmb = load i64, i64* %envptr174504, align 8                                  ; load; *envptr174504
  %envptr174505 = inttoptr i64 %env174453 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174506 = getelementptr inbounds i64, i64* %envptr174505, i64 1              ; &envptr174505[1]
  %xK1$_37foldr1 = load i64, i64* %envptr174506, align 8                             ; load; *envptr174506
  %_95171347 = call i64 @prim_car(i64 %rvp173629)                                    ; call prim_car
  %rvp173628 = call i64 @prim_cdr(i64 %rvp173629)                                    ; call prim_cdr
  %K1g$_37map1 = call i64 @prim_car(i64 %rvp173628)                                  ; call prim_car
  %na172561 = call i64 @prim_cdr(i64 %rvp173628)                                     ; call prim_cdr
  %cloptr174507 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr174509 = getelementptr inbounds i64, i64* %cloptr174507, i64 1                ; &eptr174509[1]
  %eptr174510 = getelementptr inbounds i64, i64* %cloptr174507, i64 2                ; &eptr174510[2]
  %eptr174511 = getelementptr inbounds i64, i64* %cloptr174507, i64 3                ; &eptr174511[3]
  store i64 %xK1$_37foldr1, i64* %eptr174509                                         ; *eptr174509 = %xK1$_37foldr1
  store i64 %do0$Ycmb, i64* %eptr174510                                              ; *eptr174510 = %do0$Ycmb
  store i64 %K1g$_37map1, i64* %eptr174511                                           ; *eptr174511 = %K1g$_37map1
  %eptr174508 = getelementptr inbounds i64, i64* %cloptr174507, i64 0                ; &cloptr174507[0]
  %f174512 = ptrtoint void(i64,i64)* @lam174450 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174512, i64* %eptr174508                                               ; store fptr
  %arg171583 = ptrtoint i64* %cloptr174507 to i64                                    ; closure cast; i64* -> i64
  %cloptr174513 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174514 = getelementptr inbounds i64, i64* %cloptr174513, i64 0                ; &cloptr174513[0]
  %f174515 = ptrtoint void(i64,i64)* @lam173798 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174515, i64* %eptr174514                                               ; store fptr
  %arg171582 = ptrtoint i64* %cloptr174513 to i64                                    ; closure cast; i64* -> i64
  %rva173627 = add i64 0, 0                                                          ; quoted ()
  %rva173626 = call i64 @prim_cons(i64 %arg171582, i64 %rva173627)                   ; call prim_cons
  %rva173625 = call i64 @prim_cons(i64 %arg171583, i64 %rva173626)                   ; call prim_cons
  %cloptr174516 = inttoptr i64 %do0$Ycmb to i64*                                     ; closure/env cast; i64 -> i64*
  %i0ptr174517 = getelementptr inbounds i64, i64* %cloptr174516, i64 0               ; &cloptr174516[0]
  %f174519 = load i64, i64* %i0ptr174517, align 8                                    ; load; *i0ptr174517
  %fptr174518 = inttoptr i64 %f174519 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174518(i64 %do0$Ycmb, i64 %rva173625)               ; tail call
  ret void
}


define void @lam174450(i64 %env174451, i64 %rvp173595) {
  %envptr174520 = inttoptr i64 %env174451 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174521 = getelementptr inbounds i64, i64* %envptr174520, i64 3              ; &envptr174520[3]
  %K1g$_37map1 = load i64, i64* %envptr174521, align 8                               ; load; *envptr174521
  %envptr174522 = inttoptr i64 %env174451 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174523 = getelementptr inbounds i64, i64* %envptr174522, i64 2              ; &envptr174522[2]
  %do0$Ycmb = load i64, i64* %envptr174523, align 8                                  ; load; *envptr174523
  %envptr174524 = inttoptr i64 %env174451 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174525 = getelementptr inbounds i64, i64* %envptr174524, i64 1              ; &envptr174524[1]
  %xK1$_37foldr1 = load i64, i64* %envptr174525, align 8                             ; load; *envptr174525
  %_95171348 = call i64 @prim_car(i64 %rvp173595)                                    ; call prim_car
  %rvp173594 = call i64 @prim_cdr(i64 %rvp173595)                                    ; call prim_cdr
  %l6L$_37take = call i64 @prim_car(i64 %rvp173594)                                  ; call prim_car
  %na172563 = call i64 @prim_cdr(i64 %rvp173594)                                     ; call prim_cdr
  %cloptr174526 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr174528 = getelementptr inbounds i64, i64* %cloptr174526, i64 1                ; &eptr174528[1]
  %eptr174529 = getelementptr inbounds i64, i64* %cloptr174526, i64 2                ; &eptr174529[2]
  %eptr174530 = getelementptr inbounds i64, i64* %cloptr174526, i64 3                ; &eptr174530[3]
  %eptr174531 = getelementptr inbounds i64, i64* %cloptr174526, i64 4                ; &eptr174531[4]
  store i64 %xK1$_37foldr1, i64* %eptr174528                                         ; *eptr174528 = %xK1$_37foldr1
  store i64 %do0$Ycmb, i64* %eptr174529                                              ; *eptr174529 = %do0$Ycmb
  store i64 %K1g$_37map1, i64* %eptr174530                                           ; *eptr174530 = %K1g$_37map1
  store i64 %l6L$_37take, i64* %eptr174531                                           ; *eptr174531 = %l6L$_37take
  %eptr174527 = getelementptr inbounds i64, i64* %cloptr174526, i64 0                ; &cloptr174526[0]
  %f174532 = ptrtoint void(i64,i64)* @lam174448 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174532, i64* %eptr174527                                               ; store fptr
  %arg171586 = ptrtoint i64* %cloptr174526 to i64                                    ; closure cast; i64* -> i64
  %cloptr174533 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174534 = getelementptr inbounds i64, i64* %cloptr174533, i64 0                ; &cloptr174533[0]
  %f174535 = ptrtoint void(i64,i64)* @lam173814 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174535, i64* %eptr174534                                               ; store fptr
  %arg171585 = ptrtoint i64* %cloptr174533 to i64                                    ; closure cast; i64* -> i64
  %rva173593 = add i64 0, 0                                                          ; quoted ()
  %rva173592 = call i64 @prim_cons(i64 %arg171585, i64 %rva173593)                   ; call prim_cons
  %rva173591 = call i64 @prim_cons(i64 %arg171586, i64 %rva173592)                   ; call prim_cons
  %cloptr174536 = inttoptr i64 %do0$Ycmb to i64*                                     ; closure/env cast; i64 -> i64*
  %i0ptr174537 = getelementptr inbounds i64, i64* %cloptr174536, i64 0               ; &cloptr174536[0]
  %f174539 = load i64, i64* %i0ptr174537, align 8                                    ; load; *i0ptr174537
  %fptr174538 = inttoptr i64 %f174539 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174538(i64 %do0$Ycmb, i64 %rva173591)               ; tail call
  ret void
}


define void @lam174448(i64 %env174449, i64 %rvp173566) {
  %envptr174540 = inttoptr i64 %env174449 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174541 = getelementptr inbounds i64, i64* %envptr174540, i64 4              ; &envptr174540[4]
  %l6L$_37take = load i64, i64* %envptr174541, align 8                               ; load; *envptr174541
  %envptr174542 = inttoptr i64 %env174449 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174543 = getelementptr inbounds i64, i64* %envptr174542, i64 3              ; &envptr174542[3]
  %K1g$_37map1 = load i64, i64* %envptr174543, align 8                               ; load; *envptr174543
  %envptr174544 = inttoptr i64 %env174449 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174545 = getelementptr inbounds i64, i64* %envptr174544, i64 2              ; &envptr174544[2]
  %do0$Ycmb = load i64, i64* %envptr174545, align 8                                  ; load; *envptr174545
  %envptr174546 = inttoptr i64 %env174449 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174547 = getelementptr inbounds i64, i64* %envptr174546, i64 1              ; &envptr174546[1]
  %xK1$_37foldr1 = load i64, i64* %envptr174547, align 8                             ; load; *envptr174547
  %_95171349 = call i64 @prim_car(i64 %rvp173566)                                    ; call prim_car
  %rvp173565 = call i64 @prim_cdr(i64 %rvp173566)                                    ; call prim_cdr
  %SU3$_37length = call i64 @prim_car(i64 %rvp173565)                                ; call prim_car
  %na172565 = call i64 @prim_cdr(i64 %rvp173565)                                     ; call prim_cdr
  %cloptr174548 = call i64* @alloc(i64 48)                                           ; malloc
  %eptr174550 = getelementptr inbounds i64, i64* %cloptr174548, i64 1                ; &eptr174550[1]
  %eptr174551 = getelementptr inbounds i64, i64* %cloptr174548, i64 2                ; &eptr174551[2]
  %eptr174552 = getelementptr inbounds i64, i64* %cloptr174548, i64 3                ; &eptr174552[3]
  %eptr174553 = getelementptr inbounds i64, i64* %cloptr174548, i64 4                ; &eptr174553[4]
  %eptr174554 = getelementptr inbounds i64, i64* %cloptr174548, i64 5                ; &eptr174554[5]
  store i64 %xK1$_37foldr1, i64* %eptr174550                                         ; *eptr174550 = %xK1$_37foldr1
  store i64 %SU3$_37length, i64* %eptr174551                                         ; *eptr174551 = %SU3$_37length
  store i64 %do0$Ycmb, i64* %eptr174552                                              ; *eptr174552 = %do0$Ycmb
  store i64 %K1g$_37map1, i64* %eptr174553                                           ; *eptr174553 = %K1g$_37map1
  store i64 %l6L$_37take, i64* %eptr174554                                           ; *eptr174554 = %l6L$_37take
  %eptr174549 = getelementptr inbounds i64, i64* %cloptr174548, i64 0                ; &cloptr174548[0]
  %f174555 = ptrtoint void(i64,i64)* @lam174446 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174555, i64* %eptr174549                                               ; store fptr
  %arg171589 = ptrtoint i64* %cloptr174548 to i64                                    ; closure cast; i64* -> i64
  %cloptr174556 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174557 = getelementptr inbounds i64, i64* %cloptr174556, i64 0                ; &cloptr174556[0]
  %f174558 = ptrtoint void(i64,i64)* @lam173827 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174558, i64* %eptr174557                                               ; store fptr
  %arg171588 = ptrtoint i64* %cloptr174556 to i64                                    ; closure cast; i64* -> i64
  %rva173564 = add i64 0, 0                                                          ; quoted ()
  %rva173563 = call i64 @prim_cons(i64 %arg171588, i64 %rva173564)                   ; call prim_cons
  %rva173562 = call i64 @prim_cons(i64 %arg171589, i64 %rva173563)                   ; call prim_cons
  %cloptr174559 = inttoptr i64 %do0$Ycmb to i64*                                     ; closure/env cast; i64 -> i64*
  %i0ptr174560 = getelementptr inbounds i64, i64* %cloptr174559, i64 0               ; &cloptr174559[0]
  %f174562 = load i64, i64* %i0ptr174560, align 8                                    ; load; *i0ptr174560
  %fptr174561 = inttoptr i64 %f174562 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174561(i64 %do0$Ycmb, i64 %rva173562)               ; tail call
  ret void
}


define void @lam174446(i64 %env174447, i64 %rvp173532) {
  %envptr174563 = inttoptr i64 %env174447 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174564 = getelementptr inbounds i64, i64* %envptr174563, i64 5              ; &envptr174563[5]
  %l6L$_37take = load i64, i64* %envptr174564, align 8                               ; load; *envptr174564
  %envptr174565 = inttoptr i64 %env174447 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174566 = getelementptr inbounds i64, i64* %envptr174565, i64 4              ; &envptr174565[4]
  %K1g$_37map1 = load i64, i64* %envptr174566, align 8                               ; load; *envptr174566
  %envptr174567 = inttoptr i64 %env174447 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174568 = getelementptr inbounds i64, i64* %envptr174567, i64 3              ; &envptr174567[3]
  %do0$Ycmb = load i64, i64* %envptr174568, align 8                                  ; load; *envptr174568
  %envptr174569 = inttoptr i64 %env174447 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174570 = getelementptr inbounds i64, i64* %envptr174569, i64 2              ; &envptr174569[2]
  %SU3$_37length = load i64, i64* %envptr174570, align 8                             ; load; *envptr174570
  %envptr174571 = inttoptr i64 %env174447 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174572 = getelementptr inbounds i64, i64* %envptr174571, i64 1              ; &envptr174571[1]
  %xK1$_37foldr1 = load i64, i64* %envptr174572, align 8                             ; load; *envptr174572
  %_95171350 = call i64 @prim_car(i64 %rvp173532)                                    ; call prim_car
  %rvp173531 = call i64 @prim_cdr(i64 %rvp173532)                                    ; call prim_cdr
  %aUW$_37foldl1 = call i64 @prim_car(i64 %rvp173531)                                ; call prim_car
  %na172567 = call i64 @prim_cdr(i64 %rvp173531)                                     ; call prim_cdr
  %cloptr174573 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr174575 = getelementptr inbounds i64, i64* %cloptr174573, i64 1                ; &eptr174575[1]
  store i64 %aUW$_37foldl1, i64* %eptr174575                                         ; *eptr174575 = %aUW$_37foldl1
  %eptr174574 = getelementptr inbounds i64, i64* %cloptr174573, i64 0                ; &cloptr174573[0]
  %f174576 = ptrtoint void(i64,i64)* @lam174444 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174576, i64* %eptr174574                                               ; store fptr
  %oNe$_37last = ptrtoint i64* %cloptr174573 to i64                                  ; closure cast; i64* -> i64
  %cloptr174577 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr174579 = getelementptr inbounds i64, i64* %cloptr174577, i64 1                ; &eptr174579[1]
  %eptr174580 = getelementptr inbounds i64, i64* %cloptr174577, i64 2                ; &eptr174580[2]
  store i64 %SU3$_37length, i64* %eptr174579                                         ; *eptr174579 = %SU3$_37length
  store i64 %l6L$_37take, i64* %eptr174580                                           ; *eptr174580 = %l6L$_37take
  %eptr174578 = getelementptr inbounds i64, i64* %cloptr174577, i64 0                ; &cloptr174577[0]
  %f174581 = ptrtoint void(i64,i64)* @lam174436 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174581, i64* %eptr174578                                               ; store fptr
  %kPg$_37drop_45right = ptrtoint i64* %cloptr174577 to i64                          ; closure cast; i64* -> i64
  %cloptr174582 = call i64* @alloc(i64 56)                                           ; malloc
  %eptr174584 = getelementptr inbounds i64, i64* %cloptr174582, i64 1                ; &eptr174584[1]
  %eptr174585 = getelementptr inbounds i64, i64* %cloptr174582, i64 2                ; &eptr174585[2]
  %eptr174586 = getelementptr inbounds i64, i64* %cloptr174582, i64 3                ; &eptr174586[3]
  %eptr174587 = getelementptr inbounds i64, i64* %cloptr174582, i64 4                ; &eptr174587[4]
  %eptr174588 = getelementptr inbounds i64, i64* %cloptr174582, i64 5                ; &eptr174588[5]
  %eptr174589 = getelementptr inbounds i64, i64* %cloptr174582, i64 6                ; &eptr174589[6]
  store i64 %aUW$_37foldl1, i64* %eptr174584                                         ; *eptr174584 = %aUW$_37foldl1
  store i64 %xK1$_37foldr1, i64* %eptr174585                                         ; *eptr174585 = %xK1$_37foldr1
  store i64 %kPg$_37drop_45right, i64* %eptr174586                                   ; *eptr174586 = %kPg$_37drop_45right
  store i64 %SU3$_37length, i64* %eptr174587                                         ; *eptr174587 = %SU3$_37length
  store i64 %do0$Ycmb, i64* %eptr174588                                              ; *eptr174588 = %do0$Ycmb
  store i64 %oNe$_37last, i64* %eptr174589                                           ; *eptr174589 = %oNe$_37last
  %eptr174583 = getelementptr inbounds i64, i64* %cloptr174582, i64 0                ; &cloptr174582[0]
  %f174590 = ptrtoint void(i64,i64)* @lam174430 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174590, i64* %eptr174583                                               ; store fptr
  %arg171609 = ptrtoint i64* %cloptr174582 to i64                                    ; closure cast; i64* -> i64
  %cloptr174591 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr174593 = getelementptr inbounds i64, i64* %cloptr174591, i64 1                ; &eptr174593[1]
  %eptr174594 = getelementptr inbounds i64, i64* %cloptr174591, i64 2                ; &eptr174594[2]
  store i64 %xK1$_37foldr1, i64* %eptr174593                                         ; *eptr174593 = %xK1$_37foldr1
  store i64 %K1g$_37map1, i64* %eptr174594                                           ; *eptr174594 = %K1g$_37map1
  %eptr174592 = getelementptr inbounds i64, i64* %cloptr174591, i64 0                ; &cloptr174591[0]
  %f174595 = ptrtoint void(i64,i64)* @lam173878 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174595, i64* %eptr174592                                               ; store fptr
  %arg171608 = ptrtoint i64* %cloptr174591 to i64                                    ; closure cast; i64* -> i64
  %rva173530 = add i64 0, 0                                                          ; quoted ()
  %rva173529 = call i64 @prim_cons(i64 %arg171608, i64 %rva173530)                   ; call prim_cons
  %rva173528 = call i64 @prim_cons(i64 %arg171609, i64 %rva173529)                   ; call prim_cons
  %cloptr174596 = inttoptr i64 %do0$Ycmb to i64*                                     ; closure/env cast; i64 -> i64*
  %i0ptr174597 = getelementptr inbounds i64, i64* %cloptr174596, i64 0               ; &cloptr174596[0]
  %f174599 = load i64, i64* %i0ptr174597, align 8                                    ; load; *i0ptr174597
  %fptr174598 = inttoptr i64 %f174599 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174598(i64 %do0$Ycmb, i64 %rva173528)               ; tail call
  ret void
}


define void @lam174444(i64 %env174445, i64 %rvp172584) {
  %envptr174600 = inttoptr i64 %env174445 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174601 = getelementptr inbounds i64, i64* %envptr174600, i64 1              ; &envptr174600[1]
  %aUW$_37foldl1 = load i64, i64* %envptr174601, align 8                             ; load; *envptr174601
  %cont171351 = call i64 @prim_car(i64 %rvp172584)                                   ; call prim_car
  %rvp172583 = call i64 @prim_cdr(i64 %rvp172584)                                    ; call prim_cdr
  %GmS$lst = call i64 @prim_car(i64 %rvp172583)                                      ; call prim_car
  %na172569 = call i64 @prim_cdr(i64 %rvp172583)                                     ; call prim_cdr
  %cloptr174602 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174603 = getelementptr inbounds i64, i64* %cloptr174602, i64 0                ; &cloptr174602[0]
  %f174604 = ptrtoint void(i64,i64)* @lam174442 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174604, i64* %eptr174603                                               ; store fptr
  %arg171593 = ptrtoint i64* %cloptr174602 to i64                                    ; closure cast; i64* -> i64
  %arg171592 = add i64 0, 0                                                          ; quoted ()
  %rva172582 = add i64 0, 0                                                          ; quoted ()
  %rva172581 = call i64 @prim_cons(i64 %GmS$lst, i64 %rva172582)                     ; call prim_cons
  %rva172580 = call i64 @prim_cons(i64 %arg171592, i64 %rva172581)                   ; call prim_cons
  %rva172579 = call i64 @prim_cons(i64 %arg171593, i64 %rva172580)                   ; call prim_cons
  %rva172578 = call i64 @prim_cons(i64 %cont171351, i64 %rva172579)                  ; call prim_cons
  %cloptr174605 = inttoptr i64 %aUW$_37foldl1 to i64*                                ; closure/env cast; i64 -> i64*
  %i0ptr174606 = getelementptr inbounds i64, i64* %cloptr174605, i64 0               ; &cloptr174605[0]
  %f174608 = load i64, i64* %i0ptr174606, align 8                                    ; load; *i0ptr174606
  %fptr174607 = inttoptr i64 %f174608 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174607(i64 %aUW$_37foldl1, i64 %rva172578)          ; tail call
  ret void
}


define void @lam174442(i64 %env174443, i64 %rvp172577) {
  %cont171352 = call i64 @prim_car(i64 %rvp172577)                                   ; call prim_car
  %rvp172576 = call i64 @prim_cdr(i64 %rvp172577)                                    ; call prim_cdr
  %zxk$x = call i64 @prim_car(i64 %rvp172576)                                        ; call prim_car
  %rvp172575 = call i64 @prim_cdr(i64 %rvp172576)                                    ; call prim_cdr
  %iMX$y = call i64 @prim_car(i64 %rvp172575)                                        ; call prim_car
  %na172571 = call i64 @prim_cdr(i64 %rvp172575)                                     ; call prim_cdr
  %arg171597 = add i64 0, 0                                                          ; quoted ()
  %rva172574 = add i64 0, 0                                                          ; quoted ()
  %rva172573 = call i64 @prim_cons(i64 %zxk$x, i64 %rva172574)                       ; call prim_cons
  %rva172572 = call i64 @prim_cons(i64 %arg171597, i64 %rva172573)                   ; call prim_cons
  %cloptr174609 = inttoptr i64 %cont171352 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr174610 = getelementptr inbounds i64, i64* %cloptr174609, i64 0               ; &cloptr174609[0]
  %f174612 = load i64, i64* %i0ptr174610, align 8                                    ; load; *i0ptr174610
  %fptr174611 = inttoptr i64 %f174612 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174611(i64 %cont171352, i64 %rva172572)             ; tail call
  ret void
}


define void @lam174436(i64 %env174437, i64 %rvp172600) {
  %envptr174613 = inttoptr i64 %env174437 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174614 = getelementptr inbounds i64, i64* %envptr174613, i64 2              ; &envptr174613[2]
  %l6L$_37take = load i64, i64* %envptr174614, align 8                               ; load; *envptr174614
  %envptr174615 = inttoptr i64 %env174437 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174616 = getelementptr inbounds i64, i64* %envptr174615, i64 1              ; &envptr174615[1]
  %SU3$_37length = load i64, i64* %envptr174616, align 8                             ; load; *envptr174616
  %cont171353 = call i64 @prim_car(i64 %rvp172600)                                   ; call prim_car
  %rvp172599 = call i64 @prim_cdr(i64 %rvp172600)                                    ; call prim_cdr
  %BjI$lst = call i64 @prim_car(i64 %rvp172599)                                      ; call prim_car
  %rvp172598 = call i64 @prim_cdr(i64 %rvp172599)                                    ; call prim_cdr
  %nUC$n = call i64 @prim_car(i64 %rvp172598)                                        ; call prim_car
  %na172586 = call i64 @prim_cdr(i64 %rvp172598)                                     ; call prim_cdr
  %cloptr174617 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr174619 = getelementptr inbounds i64, i64* %cloptr174617, i64 1                ; &eptr174619[1]
  %eptr174620 = getelementptr inbounds i64, i64* %cloptr174617, i64 2                ; &eptr174620[2]
  %eptr174621 = getelementptr inbounds i64, i64* %cloptr174617, i64 3                ; &eptr174621[3]
  %eptr174622 = getelementptr inbounds i64, i64* %cloptr174617, i64 4                ; &eptr174622[4]
  store i64 %BjI$lst, i64* %eptr174619                                               ; *eptr174619 = %BjI$lst
  store i64 %nUC$n, i64* %eptr174620                                                 ; *eptr174620 = %nUC$n
  store i64 %l6L$_37take, i64* %eptr174621                                           ; *eptr174621 = %l6L$_37take
  store i64 %cont171353, i64* %eptr174622                                            ; *eptr174622 = %cont171353
  %eptr174618 = getelementptr inbounds i64, i64* %cloptr174617, i64 0                ; &cloptr174617[0]
  %f174623 = ptrtoint void(i64,i64)* @lam174434 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174623, i64* %eptr174618                                               ; store fptr
  %arg171600 = ptrtoint i64* %cloptr174617 to i64                                    ; closure cast; i64* -> i64
  %rva172597 = add i64 0, 0                                                          ; quoted ()
  %rva172596 = call i64 @prim_cons(i64 %BjI$lst, i64 %rva172597)                     ; call prim_cons
  %rva172595 = call i64 @prim_cons(i64 %arg171600, i64 %rva172596)                   ; call prim_cons
  %cloptr174624 = inttoptr i64 %SU3$_37length to i64*                                ; closure/env cast; i64 -> i64*
  %i0ptr174625 = getelementptr inbounds i64, i64* %cloptr174624, i64 0               ; &cloptr174624[0]
  %f174627 = load i64, i64* %i0ptr174625, align 8                                    ; load; *i0ptr174625
  %fptr174626 = inttoptr i64 %f174627 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174626(i64 %SU3$_37length, i64 %rva172595)          ; tail call
  ret void
}


define void @lam174434(i64 %env174435, i64 %rvp172594) {
  %envptr174628 = inttoptr i64 %env174435 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174629 = getelementptr inbounds i64, i64* %envptr174628, i64 4              ; &envptr174628[4]
  %cont171353 = load i64, i64* %envptr174629, align 8                                ; load; *envptr174629
  %envptr174630 = inttoptr i64 %env174435 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174631 = getelementptr inbounds i64, i64* %envptr174630, i64 3              ; &envptr174630[3]
  %l6L$_37take = load i64, i64* %envptr174631, align 8                               ; load; *envptr174631
  %envptr174632 = inttoptr i64 %env174435 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174633 = getelementptr inbounds i64, i64* %envptr174632, i64 2              ; &envptr174632[2]
  %nUC$n = load i64, i64* %envptr174633, align 8                                     ; load; *envptr174633
  %envptr174634 = inttoptr i64 %env174435 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174635 = getelementptr inbounds i64, i64* %envptr174634, i64 1              ; &envptr174634[1]
  %BjI$lst = load i64, i64* %envptr174635, align 8                                   ; load; *envptr174635
  %_95171354 = call i64 @prim_car(i64 %rvp172594)                                    ; call prim_car
  %rvp172593 = call i64 @prim_cdr(i64 %rvp172594)                                    ; call prim_cdr
  %a171225 = call i64 @prim_car(i64 %rvp172593)                                      ; call prim_car
  %na172588 = call i64 @prim_cdr(i64 %rvp172593)                                     ; call prim_cdr
  %a171226 = call i64 @prim__45(i64 %a171225, i64 %nUC$n)                            ; call prim__45
  %rva172592 = add i64 0, 0                                                          ; quoted ()
  %rva172591 = call i64 @prim_cons(i64 %a171226, i64 %rva172592)                     ; call prim_cons
  %rva172590 = call i64 @prim_cons(i64 %BjI$lst, i64 %rva172591)                     ; call prim_cons
  %rva172589 = call i64 @prim_cons(i64 %cont171353, i64 %rva172590)                  ; call prim_cons
  %cloptr174636 = inttoptr i64 %l6L$_37take to i64*                                  ; closure/env cast; i64 -> i64*
  %i0ptr174637 = getelementptr inbounds i64, i64* %cloptr174636, i64 0               ; &cloptr174636[0]
  %f174639 = load i64, i64* %i0ptr174637, align 8                                    ; load; *i0ptr174637
  %fptr174638 = inttoptr i64 %f174639 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174638(i64 %l6L$_37take, i64 %rva172589)            ; tail call
  ret void
}


define void @lam174430(i64 %env174431, i64 %rvp173432) {
  %envptr174640 = inttoptr i64 %env174431 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174641 = getelementptr inbounds i64, i64* %envptr174640, i64 6              ; &envptr174640[6]
  %oNe$_37last = load i64, i64* %envptr174641, align 8                               ; load; *envptr174641
  %envptr174642 = inttoptr i64 %env174431 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174643 = getelementptr inbounds i64, i64* %envptr174642, i64 5              ; &envptr174642[5]
  %do0$Ycmb = load i64, i64* %envptr174643, align 8                                  ; load; *envptr174643
  %envptr174644 = inttoptr i64 %env174431 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174645 = getelementptr inbounds i64, i64* %envptr174644, i64 4              ; &envptr174644[4]
  %SU3$_37length = load i64, i64* %envptr174645, align 8                             ; load; *envptr174645
  %envptr174646 = inttoptr i64 %env174431 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174647 = getelementptr inbounds i64, i64* %envptr174646, i64 3              ; &envptr174646[3]
  %kPg$_37drop_45right = load i64, i64* %envptr174647, align 8                       ; load; *envptr174647
  %envptr174648 = inttoptr i64 %env174431 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174649 = getelementptr inbounds i64, i64* %envptr174648, i64 2              ; &envptr174648[2]
  %xK1$_37foldr1 = load i64, i64* %envptr174649, align 8                             ; load; *envptr174649
  %envptr174650 = inttoptr i64 %env174431 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174651 = getelementptr inbounds i64, i64* %envptr174650, i64 1              ; &envptr174650[1]
  %aUW$_37foldl1 = load i64, i64* %envptr174651, align 8                             ; load; *envptr174651
  %_95171355 = call i64 @prim_car(i64 %rvp173432)                                    ; call prim_car
  %rvp173431 = call i64 @prim_cdr(i64 %rvp173432)                                    ; call prim_cdr
  %zNF$_37foldr = call i64 @prim_car(i64 %rvp173431)                                 ; call prim_car
  %na172602 = call i64 @prim_cdr(i64 %rvp173431)                                     ; call prim_cdr
  %cloptr174652 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr174654 = getelementptr inbounds i64, i64* %cloptr174652, i64 1                ; &eptr174654[1]
  store i64 %xK1$_37foldr1, i64* %eptr174654                                         ; *eptr174654 = %xK1$_37foldr1
  %eptr174653 = getelementptr inbounds i64, i64* %cloptr174652, i64 0                ; &cloptr174652[0]
  %f174655 = ptrtoint void(i64,i64)* @lam174428 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174655, i64* %eptr174653                                               ; store fptr
  %slC$_37map1 = ptrtoint i64* %cloptr174652 to i64                                  ; closure cast; i64* -> i64
  %cloptr174656 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr174658 = getelementptr inbounds i64, i64* %cloptr174656, i64 1                ; &eptr174658[1]
  %eptr174659 = getelementptr inbounds i64, i64* %cloptr174656, i64 2                ; &eptr174659[2]
  %eptr174660 = getelementptr inbounds i64, i64* %cloptr174656, i64 3                ; &eptr174660[3]
  store i64 %kPg$_37drop_45right, i64* %eptr174658                                   ; *eptr174658 = %kPg$_37drop_45right
  store i64 %zNF$_37foldr, i64* %eptr174659                                          ; *eptr174659 = %zNF$_37foldr
  store i64 %oNe$_37last, i64* %eptr174660                                           ; *eptr174660 = %oNe$_37last
  %eptr174657 = getelementptr inbounds i64, i64* %cloptr174656, i64 0                ; &cloptr174656[0]
  %f174661 = ptrtoint void(i64,i64)* @lam174417 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174661, i64* %eptr174657                                               ; store fptr
  %RM7$_37map = ptrtoint i64* %cloptr174656 to i64                                   ; closure cast; i64* -> i64
  %cloptr174662 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr174664 = getelementptr inbounds i64, i64* %cloptr174662, i64 1                ; &eptr174664[1]
  %eptr174665 = getelementptr inbounds i64, i64* %cloptr174662, i64 2                ; &eptr174665[2]
  store i64 %aUW$_37foldl1, i64* %eptr174664                                         ; *eptr174664 = %aUW$_37foldl1
  store i64 %SU3$_37length, i64* %eptr174665                                         ; *eptr174665 = %SU3$_37length
  %eptr174663 = getelementptr inbounds i64, i64* %cloptr174662, i64 0                ; &cloptr174662[0]
  %f174666 = ptrtoint void(i64,i64)* @lam174401 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174666, i64* %eptr174663                                               ; store fptr
  %arg171651 = ptrtoint i64* %cloptr174662 to i64                                    ; closure cast; i64* -> i64
  %cloptr174667 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr174669 = getelementptr inbounds i64, i64* %cloptr174667, i64 1                ; &eptr174669[1]
  %eptr174670 = getelementptr inbounds i64, i64* %cloptr174667, i64 2                ; &eptr174670[2]
  %eptr174671 = getelementptr inbounds i64, i64* %cloptr174667, i64 3                ; &eptr174671[3]
  store i64 %xK1$_37foldr1, i64* %eptr174669                                         ; *eptr174669 = %xK1$_37foldr1
  store i64 %zNF$_37foldr, i64* %eptr174670                                          ; *eptr174670 = %zNF$_37foldr
  store i64 %slC$_37map1, i64* %eptr174671                                           ; *eptr174671 = %slC$_37map1
  %eptr174668 = getelementptr inbounds i64, i64* %cloptr174667, i64 0                ; &cloptr174667[0]
  %f174672 = ptrtoint void(i64,i64)* @lam173929 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174672, i64* %eptr174668                                               ; store fptr
  %arg171650 = ptrtoint i64* %cloptr174667 to i64                                    ; closure cast; i64* -> i64
  %rva173430 = add i64 0, 0                                                          ; quoted ()
  %rva173429 = call i64 @prim_cons(i64 %arg171650, i64 %rva173430)                   ; call prim_cons
  %rva173428 = call i64 @prim_cons(i64 %arg171651, i64 %rva173429)                   ; call prim_cons
  %cloptr174673 = inttoptr i64 %do0$Ycmb to i64*                                     ; closure/env cast; i64 -> i64*
  %i0ptr174674 = getelementptr inbounds i64, i64* %cloptr174673, i64 0               ; &cloptr174673[0]
  %f174676 = load i64, i64* %i0ptr174674, align 8                                    ; load; *i0ptr174674
  %fptr174675 = inttoptr i64 %f174676 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174675(i64 %do0$Ycmb, i64 %rva173428)               ; tail call
  ret void
}


define void @lam174428(i64 %env174429, i64 %rvp172627) {
  %envptr174677 = inttoptr i64 %env174429 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174678 = getelementptr inbounds i64, i64* %envptr174677, i64 1              ; &envptr174677[1]
  %xK1$_37foldr1 = load i64, i64* %envptr174678, align 8                             ; load; *envptr174678
  %cont171356 = call i64 @prim_car(i64 %rvp172627)                                   ; call prim_car
  %rvp172626 = call i64 @prim_cdr(i64 %rvp172627)                                    ; call prim_cdr
  %vY3$f = call i64 @prim_car(i64 %rvp172626)                                        ; call prim_car
  %rvp172625 = call i64 @prim_cdr(i64 %rvp172626)                                    ; call prim_cdr
  %rof$lst = call i64 @prim_car(i64 %rvp172625)                                      ; call prim_car
  %na172604 = call i64 @prim_cdr(i64 %rvp172625)                                     ; call prim_cdr
  %cloptr174679 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr174681 = getelementptr inbounds i64, i64* %cloptr174679, i64 1                ; &eptr174681[1]
  store i64 %vY3$f, i64* %eptr174681                                                 ; *eptr174681 = %vY3$f
  %eptr174680 = getelementptr inbounds i64, i64* %cloptr174679, i64 0                ; &cloptr174679[0]
  %f174682 = ptrtoint void(i64,i64)* @lam174426 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174682, i64* %eptr174680                                               ; store fptr
  %arg171613 = ptrtoint i64* %cloptr174679 to i64                                    ; closure cast; i64* -> i64
  %arg171612 = add i64 0, 0                                                          ; quoted ()
  %rva172624 = add i64 0, 0                                                          ; quoted ()
  %rva172623 = call i64 @prim_cons(i64 %rof$lst, i64 %rva172624)                     ; call prim_cons
  %rva172622 = call i64 @prim_cons(i64 %arg171612, i64 %rva172623)                   ; call prim_cons
  %rva172621 = call i64 @prim_cons(i64 %arg171613, i64 %rva172622)                   ; call prim_cons
  %rva172620 = call i64 @prim_cons(i64 %cont171356, i64 %rva172621)                  ; call prim_cons
  %cloptr174683 = inttoptr i64 %xK1$_37foldr1 to i64*                                ; closure/env cast; i64 -> i64*
  %i0ptr174684 = getelementptr inbounds i64, i64* %cloptr174683, i64 0               ; &cloptr174683[0]
  %f174686 = load i64, i64* %i0ptr174684, align 8                                    ; load; *i0ptr174684
  %fptr174685 = inttoptr i64 %f174686 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174685(i64 %xK1$_37foldr1, i64 %rva172620)          ; tail call
  ret void
}


define void @lam174426(i64 %env174427, i64 %rvp172619) {
  %envptr174687 = inttoptr i64 %env174427 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174688 = getelementptr inbounds i64, i64* %envptr174687, i64 1              ; &envptr174687[1]
  %vY3$f = load i64, i64* %envptr174688, align 8                                     ; load; *envptr174688
  %cont171357 = call i64 @prim_car(i64 %rvp172619)                                   ; call prim_car
  %rvp172618 = call i64 @prim_cdr(i64 %rvp172619)                                    ; call prim_cdr
  %WFk$v = call i64 @prim_car(i64 %rvp172618)                                        ; call prim_car
  %rvp172617 = call i64 @prim_cdr(i64 %rvp172618)                                    ; call prim_cdr
  %lJD$r = call i64 @prim_car(i64 %rvp172617)                                        ; call prim_car
  %na172606 = call i64 @prim_cdr(i64 %rvp172617)                                     ; call prim_cdr
  %cloptr174689 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr174691 = getelementptr inbounds i64, i64* %cloptr174689, i64 1                ; &eptr174691[1]
  %eptr174692 = getelementptr inbounds i64, i64* %cloptr174689, i64 2                ; &eptr174692[2]
  store i64 %lJD$r, i64* %eptr174691                                                 ; *eptr174691 = %lJD$r
  store i64 %cont171357, i64* %eptr174692                                            ; *eptr174692 = %cont171357
  %eptr174690 = getelementptr inbounds i64, i64* %cloptr174689, i64 0                ; &cloptr174689[0]
  %f174693 = ptrtoint void(i64,i64)* @lam174424 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174693, i64* %eptr174690                                               ; store fptr
  %arg171617 = ptrtoint i64* %cloptr174689 to i64                                    ; closure cast; i64* -> i64
  %rva172616 = add i64 0, 0                                                          ; quoted ()
  %rva172615 = call i64 @prim_cons(i64 %WFk$v, i64 %rva172616)                       ; call prim_cons
  %rva172614 = call i64 @prim_cons(i64 %arg171617, i64 %rva172615)                   ; call prim_cons
  %cloptr174694 = inttoptr i64 %vY3$f to i64*                                        ; closure/env cast; i64 -> i64*
  %i0ptr174695 = getelementptr inbounds i64, i64* %cloptr174694, i64 0               ; &cloptr174694[0]
  %f174697 = load i64, i64* %i0ptr174695, align 8                                    ; load; *i0ptr174695
  %fptr174696 = inttoptr i64 %f174697 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174696(i64 %vY3$f, i64 %rva172614)                  ; tail call
  ret void
}


define void @lam174424(i64 %env174425, i64 %rvp172613) {
  %envptr174698 = inttoptr i64 %env174425 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174699 = getelementptr inbounds i64, i64* %envptr174698, i64 2              ; &envptr174698[2]
  %cont171357 = load i64, i64* %envptr174699, align 8                                ; load; *envptr174699
  %envptr174700 = inttoptr i64 %env174425 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174701 = getelementptr inbounds i64, i64* %envptr174700, i64 1              ; &envptr174700[1]
  %lJD$r = load i64, i64* %envptr174701, align 8                                     ; load; *envptr174701
  %_95171358 = call i64 @prim_car(i64 %rvp172613)                                    ; call prim_car
  %rvp172612 = call i64 @prim_cdr(i64 %rvp172613)                                    ; call prim_cdr
  %a171235 = call i64 @prim_car(i64 %rvp172612)                                      ; call prim_car
  %na172608 = call i64 @prim_cdr(i64 %rvp172612)                                     ; call prim_cdr
  %retprim171359 = call i64 @prim_cons(i64 %a171235, i64 %lJD$r)                     ; call prim_cons
  %arg171622 = add i64 0, 0                                                          ; quoted ()
  %rva172611 = add i64 0, 0                                                          ; quoted ()
  %rva172610 = call i64 @prim_cons(i64 %retprim171359, i64 %rva172611)               ; call prim_cons
  %rva172609 = call i64 @prim_cons(i64 %arg171622, i64 %rva172610)                   ; call prim_cons
  %cloptr174702 = inttoptr i64 %cont171357 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr174703 = getelementptr inbounds i64, i64* %cloptr174702, i64 0               ; &cloptr174702[0]
  %f174705 = load i64, i64* %i0ptr174703, align 8                                    ; load; *i0ptr174703
  %fptr174704 = inttoptr i64 %f174705 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174704(i64 %cont171357, i64 %rva172609)             ; tail call
  ret void
}


define void @lam174417(i64 %env174418, i64 %q1m$args171361) {
  %envptr174706 = inttoptr i64 %env174418 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174707 = getelementptr inbounds i64, i64* %envptr174706, i64 3              ; &envptr174706[3]
  %oNe$_37last = load i64, i64* %envptr174707, align 8                               ; load; *envptr174707
  %envptr174708 = inttoptr i64 %env174418 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174709 = getelementptr inbounds i64, i64* %envptr174708, i64 2              ; &envptr174708[2]
  %zNF$_37foldr = load i64, i64* %envptr174709, align 8                              ; load; *envptr174709
  %envptr174710 = inttoptr i64 %env174418 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174711 = getelementptr inbounds i64, i64* %envptr174710, i64 1              ; &envptr174710[1]
  %kPg$_37drop_45right = load i64, i64* %envptr174711, align 8                       ; load; *envptr174711
  %cont171360 = call i64 @prim_car(i64 %q1m$args171361)                              ; call prim_car
  %q1m$args = call i64 @prim_cdr(i64 %q1m$args171361)                                ; call prim_cdr
  %AWF$f = call i64 @prim_car(i64 %q1m$args)                                         ; call prim_car
  %yi7$lsts = call i64 @prim_cdr(i64 %q1m$args)                                      ; call prim_cdr
  %arg171629 = add i64 0, 0                                                          ; quoted ()
  %a171239 = call i64 @prim_cons(i64 %arg171629, i64 %yi7$lsts)                      ; call prim_cons
  %cloptr174712 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr174714 = getelementptr inbounds i64, i64* %cloptr174712, i64 1                ; &eptr174714[1]
  %eptr174715 = getelementptr inbounds i64, i64* %cloptr174712, i64 2                ; &eptr174715[2]
  %eptr174716 = getelementptr inbounds i64, i64* %cloptr174712, i64 3                ; &eptr174716[3]
  store i64 %kPg$_37drop_45right, i64* %eptr174714                                   ; *eptr174714 = %kPg$_37drop_45right
  store i64 %AWF$f, i64* %eptr174715                                                 ; *eptr174715 = %AWF$f
  store i64 %oNe$_37last, i64* %eptr174716                                           ; *eptr174716 = %oNe$_37last
  %eptr174713 = getelementptr inbounds i64, i64* %cloptr174712, i64 0                ; &cloptr174712[0]
  %f174717 = ptrtoint void(i64,i64)* @lam174414 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174717, i64* %eptr174713                                               ; store fptr
  %arg171631 = ptrtoint i64* %cloptr174712 to i64                                    ; closure cast; i64* -> i64
  %a171240 = call i64 @prim_cons(i64 %arg171631, i64 %a171239)                       ; call prim_cons
  %cps_45lst171369 = call i64 @prim_cons(i64 %cont171360, i64 %a171240)              ; call prim_cons
  %cloptr174718 = inttoptr i64 %zNF$_37foldr to i64*                                 ; closure/env cast; i64 -> i64*
  %i0ptr174719 = getelementptr inbounds i64, i64* %cloptr174718, i64 0               ; &cloptr174718[0]
  %f174721 = load i64, i64* %i0ptr174719, align 8                                    ; load; *i0ptr174719
  %fptr174720 = inttoptr i64 %f174721 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174720(i64 %zNF$_37foldr, i64 %cps_45lst171369)     ; tail call
  ret void
}


define void @lam174414(i64 %env174415, i64 %kgF$fargs171363) {
  %envptr174722 = inttoptr i64 %env174415 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174723 = getelementptr inbounds i64, i64* %envptr174722, i64 3              ; &envptr174722[3]
  %oNe$_37last = load i64, i64* %envptr174723, align 8                               ; load; *envptr174723
  %envptr174724 = inttoptr i64 %env174415 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174725 = getelementptr inbounds i64, i64* %envptr174724, i64 2              ; &envptr174724[2]
  %AWF$f = load i64, i64* %envptr174725, align 8                                     ; load; *envptr174725
  %envptr174726 = inttoptr i64 %env174415 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174727 = getelementptr inbounds i64, i64* %envptr174726, i64 1              ; &envptr174726[1]
  %kPg$_37drop_45right = load i64, i64* %envptr174727, align 8                       ; load; *envptr174727
  %cont171362 = call i64 @prim_car(i64 %kgF$fargs171363)                             ; call prim_car
  %kgF$fargs = call i64 @prim_cdr(i64 %kgF$fargs171363)                              ; call prim_cdr
  %cloptr174728 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr174730 = getelementptr inbounds i64, i64* %cloptr174728, i64 1                ; &eptr174730[1]
  %eptr174731 = getelementptr inbounds i64, i64* %cloptr174728, i64 2                ; &eptr174731[2]
  %eptr174732 = getelementptr inbounds i64, i64* %cloptr174728, i64 3                ; &eptr174732[3]
  %eptr174733 = getelementptr inbounds i64, i64* %cloptr174728, i64 4                ; &eptr174733[4]
  store i64 %cont171362, i64* %eptr174730                                            ; *eptr174730 = %cont171362
  store i64 %AWF$f, i64* %eptr174731                                                 ; *eptr174731 = %AWF$f
  store i64 %oNe$_37last, i64* %eptr174732                                           ; *eptr174732 = %oNe$_37last
  store i64 %kgF$fargs, i64* %eptr174733                                             ; *eptr174733 = %kgF$fargs
  %eptr174729 = getelementptr inbounds i64, i64* %cloptr174728, i64 0                ; &cloptr174728[0]
  %f174734 = ptrtoint void(i64,i64)* @lam174412 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174734, i64* %eptr174729                                               ; store fptr
  %arg171636 = ptrtoint i64* %cloptr174728 to i64                                    ; closure cast; i64* -> i64
  %arg171634 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %rva172649 = add i64 0, 0                                                          ; quoted ()
  %rva172648 = call i64 @prim_cons(i64 %arg171634, i64 %rva172649)                   ; call prim_cons
  %rva172647 = call i64 @prim_cons(i64 %kgF$fargs, i64 %rva172648)                   ; call prim_cons
  %rva172646 = call i64 @prim_cons(i64 %arg171636, i64 %rva172647)                   ; call prim_cons
  %cloptr174735 = inttoptr i64 %kPg$_37drop_45right to i64*                          ; closure/env cast; i64 -> i64*
  %i0ptr174736 = getelementptr inbounds i64, i64* %cloptr174735, i64 0               ; &cloptr174735[0]
  %f174738 = load i64, i64* %i0ptr174736, align 8                                    ; load; *i0ptr174736
  %fptr174737 = inttoptr i64 %f174738 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174737(i64 %kPg$_37drop_45right, i64 %rva172646)    ; tail call
  ret void
}


define void @lam174412(i64 %env174413, i64 %rvp172645) {
  %envptr174739 = inttoptr i64 %env174413 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174740 = getelementptr inbounds i64, i64* %envptr174739, i64 4              ; &envptr174739[4]
  %kgF$fargs = load i64, i64* %envptr174740, align 8                                 ; load; *envptr174740
  %envptr174741 = inttoptr i64 %env174413 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174742 = getelementptr inbounds i64, i64* %envptr174741, i64 3              ; &envptr174741[3]
  %oNe$_37last = load i64, i64* %envptr174742, align 8                               ; load; *envptr174742
  %envptr174743 = inttoptr i64 %env174413 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174744 = getelementptr inbounds i64, i64* %envptr174743, i64 2              ; &envptr174743[2]
  %AWF$f = load i64, i64* %envptr174744, align 8                                     ; load; *envptr174744
  %envptr174745 = inttoptr i64 %env174413 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174746 = getelementptr inbounds i64, i64* %envptr174745, i64 1              ; &envptr174745[1]
  %cont171362 = load i64, i64* %envptr174746, align 8                                ; load; *envptr174746
  %_95171364 = call i64 @prim_car(i64 %rvp172645)                                    ; call prim_car
  %rvp172644 = call i64 @prim_cdr(i64 %rvp172645)                                    ; call prim_cdr
  %a171236 = call i64 @prim_car(i64 %rvp172644)                                      ; call prim_car
  %na172629 = call i64 @prim_cdr(i64 %rvp172644)                                     ; call prim_cdr
  %cloptr174747 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr174749 = getelementptr inbounds i64, i64* %cloptr174747, i64 1                ; &eptr174749[1]
  %eptr174750 = getelementptr inbounds i64, i64* %cloptr174747, i64 2                ; &eptr174750[2]
  %eptr174751 = getelementptr inbounds i64, i64* %cloptr174747, i64 3                ; &eptr174751[3]
  store i64 %cont171362, i64* %eptr174749                                            ; *eptr174749 = %cont171362
  store i64 %oNe$_37last, i64* %eptr174750                                           ; *eptr174750 = %oNe$_37last
  store i64 %kgF$fargs, i64* %eptr174751                                             ; *eptr174751 = %kgF$fargs
  %eptr174748 = getelementptr inbounds i64, i64* %cloptr174747, i64 0                ; &cloptr174747[0]
  %f174752 = ptrtoint void(i64,i64)* @lam174410 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174752, i64* %eptr174748                                               ; store fptr
  %arg171639 = ptrtoint i64* %cloptr174747 to i64                                    ; closure cast; i64* -> i64
  %cps_45lst171368 = call i64 @prim_cons(i64 %arg171639, i64 %a171236)               ; call prim_cons
  %cloptr174753 = inttoptr i64 %AWF$f to i64*                                        ; closure/env cast; i64 -> i64*
  %i0ptr174754 = getelementptr inbounds i64, i64* %cloptr174753, i64 0               ; &cloptr174753[0]
  %f174756 = load i64, i64* %i0ptr174754, align 8                                    ; load; *i0ptr174754
  %fptr174755 = inttoptr i64 %f174756 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174755(i64 %AWF$f, i64 %cps_45lst171368)            ; tail call
  ret void
}


define void @lam174410(i64 %env174411, i64 %rvp172643) {
  %envptr174757 = inttoptr i64 %env174411 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174758 = getelementptr inbounds i64, i64* %envptr174757, i64 3              ; &envptr174757[3]
  %kgF$fargs = load i64, i64* %envptr174758, align 8                                 ; load; *envptr174758
  %envptr174759 = inttoptr i64 %env174411 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174760 = getelementptr inbounds i64, i64* %envptr174759, i64 2              ; &envptr174759[2]
  %oNe$_37last = load i64, i64* %envptr174760, align 8                               ; load; *envptr174760
  %envptr174761 = inttoptr i64 %env174411 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174762 = getelementptr inbounds i64, i64* %envptr174761, i64 1              ; &envptr174761[1]
  %cont171362 = load i64, i64* %envptr174762, align 8                                ; load; *envptr174762
  %_95171365 = call i64 @prim_car(i64 %rvp172643)                                    ; call prim_car
  %rvp172642 = call i64 @prim_cdr(i64 %rvp172643)                                    ; call prim_cdr
  %a171237 = call i64 @prim_car(i64 %rvp172642)                                      ; call prim_car
  %na172631 = call i64 @prim_cdr(i64 %rvp172642)                                     ; call prim_cdr
  %cloptr174763 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr174765 = getelementptr inbounds i64, i64* %cloptr174763, i64 1                ; &eptr174765[1]
  %eptr174766 = getelementptr inbounds i64, i64* %cloptr174763, i64 2                ; &eptr174766[2]
  store i64 %cont171362, i64* %eptr174765                                            ; *eptr174765 = %cont171362
  store i64 %a171237, i64* %eptr174766                                               ; *eptr174766 = %a171237
  %eptr174764 = getelementptr inbounds i64, i64* %cloptr174763, i64 0                ; &cloptr174763[0]
  %f174767 = ptrtoint void(i64,i64)* @lam174408 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174767, i64* %eptr174764                                               ; store fptr
  %arg171641 = ptrtoint i64* %cloptr174763 to i64                                    ; closure cast; i64* -> i64
  %rva172641 = add i64 0, 0                                                          ; quoted ()
  %rva172640 = call i64 @prim_cons(i64 %kgF$fargs, i64 %rva172641)                   ; call prim_cons
  %rva172639 = call i64 @prim_cons(i64 %arg171641, i64 %rva172640)                   ; call prim_cons
  %cloptr174768 = inttoptr i64 %oNe$_37last to i64*                                  ; closure/env cast; i64 -> i64*
  %i0ptr174769 = getelementptr inbounds i64, i64* %cloptr174768, i64 0               ; &cloptr174768[0]
  %f174771 = load i64, i64* %i0ptr174769, align 8                                    ; load; *i0ptr174769
  %fptr174770 = inttoptr i64 %f174771 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174770(i64 %oNe$_37last, i64 %rva172639)            ; tail call
  ret void
}


define void @lam174408(i64 %env174409, i64 %rvp172638) {
  %envptr174772 = inttoptr i64 %env174409 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174773 = getelementptr inbounds i64, i64* %envptr174772, i64 2              ; &envptr174772[2]
  %a171237 = load i64, i64* %envptr174773, align 8                                   ; load; *envptr174773
  %envptr174774 = inttoptr i64 %env174409 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174775 = getelementptr inbounds i64, i64* %envptr174774, i64 1              ; &envptr174774[1]
  %cont171362 = load i64, i64* %envptr174775, align 8                                ; load; *envptr174775
  %_95171366 = call i64 @prim_car(i64 %rvp172638)                                    ; call prim_car
  %rvp172637 = call i64 @prim_cdr(i64 %rvp172638)                                    ; call prim_cdr
  %a171238 = call i64 @prim_car(i64 %rvp172637)                                      ; call prim_car
  %na172633 = call i64 @prim_cdr(i64 %rvp172637)                                     ; call prim_cdr
  %retprim171367 = call i64 @prim_cons(i64 %a171237, i64 %a171238)                   ; call prim_cons
  %arg171646 = add i64 0, 0                                                          ; quoted ()
  %rva172636 = add i64 0, 0                                                          ; quoted ()
  %rva172635 = call i64 @prim_cons(i64 %retprim171367, i64 %rva172636)               ; call prim_cons
  %rva172634 = call i64 @prim_cons(i64 %arg171646, i64 %rva172635)                   ; call prim_cons
  %cloptr174776 = inttoptr i64 %cont171362 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr174777 = getelementptr inbounds i64, i64* %cloptr174776, i64 0               ; &cloptr174776[0]
  %f174779 = load i64, i64* %i0ptr174777, align 8                                    ; load; *i0ptr174777
  %fptr174778 = inttoptr i64 %f174779 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174778(i64 %cont171362, i64 %rva172634)             ; tail call
  ret void
}


define void @lam174401(i64 %env174402, i64 %rvp173332) {
  %envptr174780 = inttoptr i64 %env174402 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174781 = getelementptr inbounds i64, i64* %envptr174780, i64 2              ; &envptr174780[2]
  %SU3$_37length = load i64, i64* %envptr174781, align 8                             ; load; *envptr174781
  %envptr174782 = inttoptr i64 %env174402 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174783 = getelementptr inbounds i64, i64* %envptr174782, i64 1              ; &envptr174782[1]
  %aUW$_37foldl1 = load i64, i64* %envptr174783, align 8                             ; load; *envptr174783
  %_95171370 = call i64 @prim_car(i64 %rvp173332)                                    ; call prim_car
  %rvp173331 = call i64 @prim_cdr(i64 %rvp173332)                                    ; call prim_cdr
  %Qgb$_37foldl = call i64 @prim_car(i64 %rvp173331)                                 ; call prim_car
  %na172651 = call i64 @prim_cdr(i64 %rvp173331)                                     ; call prim_cdr
  %cloptr174784 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174785 = getelementptr inbounds i64, i64* %cloptr174784, i64 0                ; &cloptr174784[0]
  %f174786 = ptrtoint void(i64,i64)* @lam174399 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174786, i64* %eptr174785                                               ; store fptr
  %G9j$_37_62 = ptrtoint i64* %cloptr174784 to i64                                   ; closure cast; i64* -> i64
  %cloptr174787 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174788 = getelementptr inbounds i64, i64* %cloptr174787, i64 0                ; &cloptr174787[0]
  %f174789 = ptrtoint void(i64,i64)* @lam174395 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174789, i64* %eptr174788                                               ; store fptr
  %You$_37_62_61 = ptrtoint i64* %cloptr174787 to i64                                ; closure cast; i64* -> i64
  %arg171666 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %arg171665 = add i64 0, 0                                                          ; quoted ()
  %GjE$_37append = call i64 @prim_make_45vector(i64 %arg171666, i64 %arg171665)      ; call prim_make_45vector
  %arg171668 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %cloptr174790 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr174792 = getelementptr inbounds i64, i64* %cloptr174790, i64 1                ; &eptr174792[1]
  store i64 %GjE$_37append, i64* %eptr174792                                         ; *eptr174792 = %GjE$_37append
  %eptr174791 = getelementptr inbounds i64, i64* %cloptr174790, i64 0                ; &cloptr174790[0]
  %f174793 = ptrtoint void(i64,i64)* @lam174388 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174793, i64* %eptr174791                                               ; store fptr
  %arg171667 = ptrtoint i64* %cloptr174790 to i64                                    ; closure cast; i64* -> i64
  %ww7$_950 = call i64 @prim_vector_45set_33(i64 %GjE$_37append, i64 %arg171668, i64 %arg171667); call prim_vector_45set_33
  %arg171687 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171498 = call i64 @prim_vector_45ref(i64 %GjE$_37append, i64 %arg171687)   ; call prim_vector_45ref
  %cloptr174794 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr174796 = getelementptr inbounds i64, i64* %cloptr174794, i64 1                ; &eptr174796[1]
  %eptr174797 = getelementptr inbounds i64, i64* %cloptr174794, i64 2                ; &eptr174797[2]
  %eptr174798 = getelementptr inbounds i64, i64* %cloptr174794, i64 3                ; &eptr174798[3]
  store i64 %aUW$_37foldl1, i64* %eptr174796                                         ; *eptr174796 = %aUW$_37foldl1
  store i64 %SU3$_37length, i64* %eptr174797                                         ; *eptr174797 = %SU3$_37length
  store i64 %G9j$_37_62, i64* %eptr174798                                            ; *eptr174798 = %G9j$_37_62
  %eptr174795 = getelementptr inbounds i64, i64* %cloptr174794, i64 0                ; &cloptr174794[0]
  %f174799 = ptrtoint void(i64,i64)* @lam174377 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174799, i64* %eptr174795                                               ; store fptr
  %arg171691 = ptrtoint i64* %cloptr174794 to i64                                    ; closure cast; i64* -> i64
  %arg171690 = add i64 0, 0                                                          ; quoted ()
  %rva173330 = add i64 0, 0                                                          ; quoted ()
  %rva173329 = call i64 @prim_cons(i64 %retprim171498, i64 %rva173330)               ; call prim_cons
  %rva173328 = call i64 @prim_cons(i64 %arg171690, i64 %rva173329)                   ; call prim_cons
  %cloptr174800 = inttoptr i64 %arg171691 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr174801 = getelementptr inbounds i64, i64* %cloptr174800, i64 0               ; &cloptr174800[0]
  %f174803 = load i64, i64* %i0ptr174801, align 8                                    ; load; *i0ptr174801
  %fptr174802 = inttoptr i64 %f174803 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174802(i64 %arg171691, i64 %rva173328)              ; tail call
  ret void
}


define void @lam174399(i64 %env174400, i64 %rvp172659) {
  %cont171371 = call i64 @prim_car(i64 %rvp172659)                                   ; call prim_car
  %rvp172658 = call i64 @prim_cdr(i64 %rvp172659)                                    ; call prim_cdr
  %u3a$a = call i64 @prim_car(i64 %rvp172658)                                        ; call prim_car
  %rvp172657 = call i64 @prim_cdr(i64 %rvp172658)                                    ; call prim_cdr
  %csl$b = call i64 @prim_car(i64 %rvp172657)                                        ; call prim_car
  %na172653 = call i64 @prim_cdr(i64 %rvp172657)                                     ; call prim_cdr
  %a171248 = call i64 @prim__60_61(i64 %u3a$a, i64 %csl$b)                           ; call prim__60_61
  %retprim171372 = call i64 @prim_not(i64 %a171248)                                  ; call prim_not
  %arg171657 = add i64 0, 0                                                          ; quoted ()
  %rva172656 = add i64 0, 0                                                          ; quoted ()
  %rva172655 = call i64 @prim_cons(i64 %retprim171372, i64 %rva172656)               ; call prim_cons
  %rva172654 = call i64 @prim_cons(i64 %arg171657, i64 %rva172655)                   ; call prim_cons
  %cloptr174804 = inttoptr i64 %cont171371 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr174805 = getelementptr inbounds i64, i64* %cloptr174804, i64 0               ; &cloptr174804[0]
  %f174807 = load i64, i64* %i0ptr174805, align 8                                    ; load; *i0ptr174805
  %fptr174806 = inttoptr i64 %f174807 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174806(i64 %cont171371, i64 %rva172654)             ; tail call
  ret void
}


define void @lam174395(i64 %env174396, i64 %rvp172667) {
  %cont171373 = call i64 @prim_car(i64 %rvp172667)                                   ; call prim_car
  %rvp172666 = call i64 @prim_cdr(i64 %rvp172667)                                    ; call prim_cdr
  %Ky1$a = call i64 @prim_car(i64 %rvp172666)                                        ; call prim_car
  %rvp172665 = call i64 @prim_cdr(i64 %rvp172666)                                    ; call prim_cdr
  %IRj$b = call i64 @prim_car(i64 %rvp172665)                                        ; call prim_car
  %na172661 = call i64 @prim_cdr(i64 %rvp172665)                                     ; call prim_cdr
  %a171249 = call i64 @prim__60(i64 %Ky1$a, i64 %IRj$b)                              ; call prim__60
  %retprim171374 = call i64 @prim_not(i64 %a171249)                                  ; call prim_not
  %arg171663 = add i64 0, 0                                                          ; quoted ()
  %rva172664 = add i64 0, 0                                                          ; quoted ()
  %rva172663 = call i64 @prim_cons(i64 %retprim171374, i64 %rva172664)               ; call prim_cons
  %rva172662 = call i64 @prim_cons(i64 %arg171663, i64 %rva172663)                   ; call prim_cons
  %cloptr174808 = inttoptr i64 %cont171373 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr174809 = getelementptr inbounds i64, i64* %cloptr174808, i64 0               ; &cloptr174808[0]
  %f174811 = load i64, i64* %i0ptr174809, align 8                                    ; load; *i0ptr174809
  %fptr174810 = inttoptr i64 %f174811 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174810(i64 %cont171373, i64 %rva172662)             ; tail call
  ret void
}


define void @lam174388(i64 %env174389, i64 %rvp172686) {
  %envptr174812 = inttoptr i64 %env174389 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174813 = getelementptr inbounds i64, i64* %envptr174812, i64 1              ; &envptr174812[1]
  %GjE$_37append = load i64, i64* %envptr174813, align 8                             ; load; *envptr174813
  %cont171495 = call i64 @prim_car(i64 %rvp172686)                                   ; call prim_car
  %rvp172685 = call i64 @prim_cdr(i64 %rvp172686)                                    ; call prim_cdr
  %YVn$ls0 = call i64 @prim_car(i64 %rvp172685)                                      ; call prim_car
  %rvp172684 = call i64 @prim_cdr(i64 %rvp172685)                                    ; call prim_cdr
  %hPK$ls1 = call i64 @prim_car(i64 %rvp172684)                                      ; call prim_car
  %na172669 = call i64 @prim_cdr(i64 %rvp172684)                                     ; call prim_cdr
  %a171250 = call i64 @prim_null_63(i64 %YVn$ls0)                                    ; call prim_null_63
  %cmp174814 = icmp eq i64 %a171250, 15                                              ; false?
  br i1 %cmp174814, label %else174816, label %then174815                             ; if

then174815:
  %arg171672 = add i64 0, 0                                                          ; quoted ()
  %rva172672 = add i64 0, 0                                                          ; quoted ()
  %rva172671 = call i64 @prim_cons(i64 %hPK$ls1, i64 %rva172672)                     ; call prim_cons
  %rva172670 = call i64 @prim_cons(i64 %arg171672, i64 %rva172671)                   ; call prim_cons
  %cloptr174817 = inttoptr i64 %cont171495 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr174818 = getelementptr inbounds i64, i64* %cloptr174817, i64 0               ; &cloptr174817[0]
  %f174820 = load i64, i64* %i0ptr174818, align 8                                    ; load; *i0ptr174818
  %fptr174819 = inttoptr i64 %f174820 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174819(i64 %cont171495, i64 %rva172670)             ; tail call
  ret void

else174816:
  %a171251 = call i64 @prim_car(i64 %YVn$ls0)                                        ; call prim_car
  %arg171675 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171252 = call i64 @prim_vector_45ref(i64 %GjE$_37append, i64 %arg171675)         ; call prim_vector_45ref
  %a171253 = call i64 @prim_cdr(i64 %YVn$ls0)                                        ; call prim_cdr
  %cloptr174821 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr174823 = getelementptr inbounds i64, i64* %cloptr174821, i64 1                ; &eptr174823[1]
  %eptr174824 = getelementptr inbounds i64, i64* %cloptr174821, i64 2                ; &eptr174824[2]
  store i64 %a171251, i64* %eptr174823                                               ; *eptr174823 = %a171251
  store i64 %cont171495, i64* %eptr174824                                            ; *eptr174824 = %cont171495
  %eptr174822 = getelementptr inbounds i64, i64* %cloptr174821, i64 0                ; &cloptr174821[0]
  %f174825 = ptrtoint void(i64,i64)* @lam174385 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174825, i64* %eptr174822                                               ; store fptr
  %arg171680 = ptrtoint i64* %cloptr174821 to i64                                    ; closure cast; i64* -> i64
  %rva172683 = add i64 0, 0                                                          ; quoted ()
  %rva172682 = call i64 @prim_cons(i64 %hPK$ls1, i64 %rva172683)                     ; call prim_cons
  %rva172681 = call i64 @prim_cons(i64 %a171253, i64 %rva172682)                     ; call prim_cons
  %rva172680 = call i64 @prim_cons(i64 %arg171680, i64 %rva172681)                   ; call prim_cons
  %cloptr174826 = inttoptr i64 %a171252 to i64*                                      ; closure/env cast; i64 -> i64*
  %i0ptr174827 = getelementptr inbounds i64, i64* %cloptr174826, i64 0               ; &cloptr174826[0]
  %f174829 = load i64, i64* %i0ptr174827, align 8                                    ; load; *i0ptr174827
  %fptr174828 = inttoptr i64 %f174829 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174828(i64 %a171252, i64 %rva172680)                ; tail call
  ret void
}


define void @lam174385(i64 %env174386, i64 %rvp172679) {
  %envptr174830 = inttoptr i64 %env174386 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174831 = getelementptr inbounds i64, i64* %envptr174830, i64 2              ; &envptr174830[2]
  %cont171495 = load i64, i64* %envptr174831, align 8                                ; load; *envptr174831
  %envptr174832 = inttoptr i64 %env174386 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174833 = getelementptr inbounds i64, i64* %envptr174832, i64 1              ; &envptr174832[1]
  %a171251 = load i64, i64* %envptr174833, align 8                                   ; load; *envptr174833
  %_95171496 = call i64 @prim_car(i64 %rvp172679)                                    ; call prim_car
  %rvp172678 = call i64 @prim_cdr(i64 %rvp172679)                                    ; call prim_cdr
  %a171254 = call i64 @prim_car(i64 %rvp172678)                                      ; call prim_car
  %na172674 = call i64 @prim_cdr(i64 %rvp172678)                                     ; call prim_cdr
  %retprim171497 = call i64 @prim_cons(i64 %a171251, i64 %a171254)                   ; call prim_cons
  %arg171685 = add i64 0, 0                                                          ; quoted ()
  %rva172677 = add i64 0, 0                                                          ; quoted ()
  %rva172676 = call i64 @prim_cons(i64 %retprim171497, i64 %rva172677)               ; call prim_cons
  %rva172675 = call i64 @prim_cons(i64 %arg171685, i64 %rva172676)                   ; call prim_cons
  %cloptr174834 = inttoptr i64 %cont171495 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr174835 = getelementptr inbounds i64, i64* %cloptr174834, i64 0               ; &cloptr174834[0]
  %f174837 = load i64, i64* %i0ptr174835, align 8                                    ; load; *i0ptr174835
  %fptr174836 = inttoptr i64 %f174837 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174836(i64 %cont171495, i64 %rva172675)             ; tail call
  ret void
}


define void @lam174377(i64 %env174378, i64 %rvp173327) {
  %envptr174838 = inttoptr i64 %env174378 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174839 = getelementptr inbounds i64, i64* %envptr174838, i64 3              ; &envptr174838[3]
  %G9j$_37_62 = load i64, i64* %envptr174839, align 8                                ; load; *envptr174839
  %envptr174840 = inttoptr i64 %env174378 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174841 = getelementptr inbounds i64, i64* %envptr174840, i64 2              ; &envptr174840[2]
  %SU3$_37length = load i64, i64* %envptr174841, align 8                             ; load; *envptr174841
  %envptr174842 = inttoptr i64 %env174378 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174843 = getelementptr inbounds i64, i64* %envptr174842, i64 1              ; &envptr174842[1]
  %aUW$_37foldl1 = load i64, i64* %envptr174843, align 8                             ; load; *envptr174843
  %_95171375 = call i64 @prim_car(i64 %rvp173327)                                    ; call prim_car
  %rvp173326 = call i64 @prim_cdr(i64 %rvp173327)                                    ; call prim_cdr
  %oRC$_37append = call i64 @prim_car(i64 %rvp173326)                                ; call prim_car
  %na172688 = call i64 @prim_cdr(i64 %rvp173326)                                     ; call prim_cdr
  %cloptr174844 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174845 = getelementptr inbounds i64, i64* %cloptr174844, i64 0                ; &cloptr174844[0]
  %f174846 = ptrtoint void(i64,i64)* @lam174375 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174846, i64* %eptr174845                                               ; store fptr
  %VT7$_37list_63 = ptrtoint i64* %cloptr174844 to i64                               ; closure cast; i64* -> i64
  %cloptr174847 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174848 = getelementptr inbounds i64, i64* %cloptr174847, i64 0                ; &cloptr174847[0]
  %f174849 = ptrtoint void(i64,i64)* @lam174323 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174849, i64* %eptr174848                                               ; store fptr
  %Re6$_37drop = ptrtoint i64* %cloptr174847 to i64                                  ; closure cast; i64* -> i64
  %cloptr174850 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174851 = getelementptr inbounds i64, i64* %cloptr174850, i64 0                ; &cloptr174850[0]
  %f174852 = ptrtoint void(i64,i64)* @lam174273 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174852, i64* %eptr174851                                               ; store fptr
  %bXN$_37memv = ptrtoint i64* %cloptr174850 to i64                                  ; closure cast; i64* -> i64
  %cloptr174853 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr174855 = getelementptr inbounds i64, i64* %cloptr174853, i64 1                ; &eptr174855[1]
  store i64 %aUW$_37foldl1, i64* %eptr174855                                         ; *eptr174855 = %aUW$_37foldl1
  %eptr174854 = getelementptr inbounds i64, i64* %cloptr174853, i64 0                ; &cloptr174853[0]
  %f174856 = ptrtoint void(i64,i64)* @lam174232 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174856, i64* %eptr174854                                               ; store fptr
  %jnh$_37_47 = ptrtoint i64* %cloptr174853 to i64                                   ; closure cast; i64* -> i64
  %cloptr174857 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174858 = getelementptr inbounds i64, i64* %cloptr174857, i64 0                ; &cloptr174857[0]
  %f174859 = ptrtoint void(i64,i64)* @lam174220 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174859, i64* %eptr174858                                               ; store fptr
  %IFD$_37first = ptrtoint i64* %cloptr174857 to i64                                 ; closure cast; i64* -> i64
  %cloptr174860 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174861 = getelementptr inbounds i64, i64* %cloptr174860, i64 0                ; &cloptr174860[0]
  %f174862 = ptrtoint void(i64,i64)* @lam174216 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174862, i64* %eptr174861                                               ; store fptr
  %pUG$_37second = ptrtoint i64* %cloptr174860 to i64                                ; closure cast; i64* -> i64
  %cloptr174863 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174864 = getelementptr inbounds i64, i64* %cloptr174863, i64 0                ; &cloptr174863[0]
  %f174865 = ptrtoint void(i64,i64)* @lam174212 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174865, i64* %eptr174864                                               ; store fptr
  %qI1$_37third = ptrtoint i64* %cloptr174863 to i64                                 ; closure cast; i64* -> i64
  %cloptr174866 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174867 = getelementptr inbounds i64, i64* %cloptr174866, i64 0                ; &cloptr174866[0]
  %f174868 = ptrtoint void(i64,i64)* @lam174208 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174868, i64* %eptr174867                                               ; store fptr
  %V7r$_37fourth = ptrtoint i64* %cloptr174866 to i64                                ; closure cast; i64* -> i64
  %cloptr174869 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174870 = getelementptr inbounds i64, i64* %cloptr174869, i64 0                ; &cloptr174869[0]
  %f174871 = ptrtoint void(i64,i64)* @lam174204 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174871, i64* %eptr174870                                               ; store fptr
  %xZq$promise_63 = ptrtoint i64* %cloptr174869 to i64                               ; closure cast; i64* -> i64
  %cloptr174872 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174873 = getelementptr inbounds i64, i64* %cloptr174872, i64 0                ; &cloptr174872[0]
  %f174874 = ptrtoint void(i64,i64)* @lam174196 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174874, i64* %eptr174873                                               ; store fptr
  %arg171953 = ptrtoint i64* %cloptr174872 to i64                                    ; closure cast; i64* -> i64
  %cloptr174875 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr174877 = getelementptr inbounds i64, i64* %cloptr174875, i64 1                ; &eptr174877[1]
  %eptr174878 = getelementptr inbounds i64, i64* %cloptr174875, i64 2                ; &eptr174878[2]
  %eptr174879 = getelementptr inbounds i64, i64* %cloptr174875, i64 3                ; &eptr174879[3]
  store i64 %SU3$_37length, i64* %eptr174877                                         ; *eptr174877 = %SU3$_37length
  store i64 %G9j$_37_62, i64* %eptr174878                                            ; *eptr174878 = %G9j$_37_62
  store i64 %Re6$_37drop, i64* %eptr174879                                           ; *eptr174879 = %Re6$_37drop
  %eptr174876 = getelementptr inbounds i64, i64* %cloptr174875, i64 0                ; &cloptr174875[0]
  %f174880 = ptrtoint void(i64,i64)* @lam174192 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174880, i64* %eptr174876                                               ; store fptr
  %arg171952 = ptrtoint i64* %cloptr174875 to i64                                    ; closure cast; i64* -> i64
  %rva173325 = add i64 0, 0                                                          ; quoted ()
  %rva173324 = call i64 @prim_cons(i64 %arg171952, i64 %rva173325)                   ; call prim_cons
  %cloptr174881 = inttoptr i64 %arg171953 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr174882 = getelementptr inbounds i64, i64* %cloptr174881, i64 0               ; &cloptr174881[0]
  %f174884 = load i64, i64* %i0ptr174882, align 8                                    ; load; *i0ptr174882
  %fptr174883 = inttoptr i64 %f174884 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174883(i64 %arg171953, i64 %rva173324)              ; tail call
  ret void
}


define void @lam174375(i64 %env174376, i64 %rvp172756) {
  %cont171376 = call i64 @prim_car(i64 %rvp172756)                                   ; call prim_car
  %rvp172755 = call i64 @prim_cdr(i64 %rvp172756)                                    ; call prim_cdr
  %jZH$a = call i64 @prim_car(i64 %rvp172755)                                        ; call prim_car
  %na172690 = call i64 @prim_cdr(i64 %rvp172755)                                     ; call prim_cdr
  %arg171693 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %Nob$a = call i64 @prim_make_45vector(i64 %arg171693, i64 %jZH$a)                  ; call prim_make_45vector
  %cloptr174885 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr174886 = getelementptr inbounds i64, i64* %cloptr174885, i64 0                ; &cloptr174885[0]
  %f174887 = ptrtoint void(i64,i64)* @lam174372 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174887, i64* %eptr174886                                               ; store fptr
  %arg171696 = ptrtoint i64* %cloptr174885 to i64                                    ; closure cast; i64* -> i64
  %cloptr174888 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr174890 = getelementptr inbounds i64, i64* %cloptr174888, i64 1                ; &eptr174890[1]
  %eptr174891 = getelementptr inbounds i64, i64* %cloptr174888, i64 2                ; &eptr174891[2]
  store i64 %Nob$a, i64* %eptr174890                                                 ; *eptr174890 = %Nob$a
  store i64 %cont171376, i64* %eptr174891                                            ; *eptr174891 = %cont171376
  %eptr174889 = getelementptr inbounds i64, i64* %cloptr174888, i64 0                ; &cloptr174888[0]
  %f174892 = ptrtoint void(i64,i64)* @lam174368 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174892, i64* %eptr174889                                               ; store fptr
  %arg171695 = ptrtoint i64* %cloptr174888 to i64                                    ; closure cast; i64* -> i64
  %cloptr174893 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr174895 = getelementptr inbounds i64, i64* %cloptr174893, i64 1                ; &eptr174895[1]
  %eptr174896 = getelementptr inbounds i64, i64* %cloptr174893, i64 2                ; &eptr174896[2]
  store i64 %Nob$a, i64* %eptr174895                                                 ; *eptr174895 = %Nob$a
  store i64 %cont171376, i64* %eptr174896                                            ; *eptr174896 = %cont171376
  %eptr174894 = getelementptr inbounds i64, i64* %cloptr174893, i64 0                ; &cloptr174893[0]
  %f174897 = ptrtoint void(i64,i64)* @lam174346 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174897, i64* %eptr174894                                               ; store fptr
  %arg171694 = ptrtoint i64* %cloptr174893 to i64                                    ; closure cast; i64* -> i64
  %rva172754 = add i64 0, 0                                                          ; quoted ()
  %rva172753 = call i64 @prim_cons(i64 %arg171694, i64 %rva172754)                   ; call prim_cons
  %rva172752 = call i64 @prim_cons(i64 %arg171695, i64 %rva172753)                   ; call prim_cons
  %cloptr174898 = inttoptr i64 %arg171696 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr174899 = getelementptr inbounds i64, i64* %cloptr174898, i64 0               ; &cloptr174898[0]
  %f174901 = load i64, i64* %i0ptr174899, align 8                                    ; load; *i0ptr174899
  %fptr174900 = inttoptr i64 %f174901 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174900(i64 %arg171696, i64 %rva172752)              ; tail call
  ret void
}


define void @lam174372(i64 %env174373, i64 %rvp172697) {
  %cont171382 = call i64 @prim_car(i64 %rvp172697)                                   ; call prim_car
  %rvp172696 = call i64 @prim_cdr(i64 %rvp172697)                                    ; call prim_cdr
  %SNL$k = call i64 @prim_car(i64 %rvp172696)                                        ; call prim_car
  %na172692 = call i64 @prim_cdr(i64 %rvp172696)                                     ; call prim_cdr
  %arg171698 = add i64 0, 0                                                          ; quoted ()
  %rva172695 = add i64 0, 0                                                          ; quoted ()
  %rva172694 = call i64 @prim_cons(i64 %SNL$k, i64 %rva172695)                       ; call prim_cons
  %rva172693 = call i64 @prim_cons(i64 %arg171698, i64 %rva172694)                   ; call prim_cons
  %cloptr174902 = inttoptr i64 %cont171382 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr174903 = getelementptr inbounds i64, i64* %cloptr174902, i64 0               ; &cloptr174902[0]
  %f174905 = load i64, i64* %i0ptr174903, align 8                                    ; load; *i0ptr174903
  %fptr174904 = inttoptr i64 %f174905 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174904(i64 %cont171382, i64 %rva172693)             ; tail call
  ret void
}


define void @lam174368(i64 %env174369, i64 %rvp172724) {
  %envptr174906 = inttoptr i64 %env174369 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174907 = getelementptr inbounds i64, i64* %envptr174906, i64 2              ; &envptr174906[2]
  %cont171376 = load i64, i64* %envptr174907, align 8                                ; load; *envptr174907
  %envptr174908 = inttoptr i64 %env174369 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174909 = getelementptr inbounds i64, i64* %envptr174908, i64 1              ; &envptr174908[1]
  %Nob$a = load i64, i64* %envptr174909, align 8                                     ; load; *envptr174909
  %_95171377 = call i64 @prim_car(i64 %rvp172724)                                    ; call prim_car
  %rvp172723 = call i64 @prim_cdr(i64 %rvp172724)                                    ; call prim_cdr
  %las$cc = call i64 @prim_car(i64 %rvp172723)                                       ; call prim_car
  %na172699 = call i64 @prim_cdr(i64 %rvp172723)                                     ; call prim_cdr
  %arg171700 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171255 = call i64 @prim_vector_45ref(i64 %Nob$a, i64 %arg171700)                 ; call prim_vector_45ref
  %a171256 = call i64 @prim_null_63(i64 %a171255)                                    ; call prim_null_63
  %cmp174910 = icmp eq i64 %a171256, 15                                              ; false?
  br i1 %cmp174910, label %else174912, label %then174911                             ; if

then174911:
  %arg171704 = add i64 0, 0                                                          ; quoted ()
  %arg171703 = call i64 @const_init_true()                                           ; quoted #t
  %rva172702 = add i64 0, 0                                                          ; quoted ()
  %rva172701 = call i64 @prim_cons(i64 %arg171703, i64 %rva172702)                   ; call prim_cons
  %rva172700 = call i64 @prim_cons(i64 %arg171704, i64 %rva172701)                   ; call prim_cons
  %cloptr174913 = inttoptr i64 %cont171376 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr174914 = getelementptr inbounds i64, i64* %cloptr174913, i64 0               ; &cloptr174913[0]
  %f174916 = load i64, i64* %i0ptr174914, align 8                                    ; load; *i0ptr174914
  %fptr174915 = inttoptr i64 %f174916 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174915(i64 %cont171376, i64 %rva172700)             ; tail call
  ret void

else174912:
  %arg171706 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171257 = call i64 @prim_vector_45ref(i64 %Nob$a, i64 %arg171706)                 ; call prim_vector_45ref
  %a171258 = call i64 @prim_cons_63(i64 %a171257)                                    ; call prim_cons_63
  %cmp174917 = icmp eq i64 %a171258, 15                                              ; false?
  br i1 %cmp174917, label %else174919, label %then174918                             ; if

then174918:
  %arg171709 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171259 = call i64 @prim_vector_45ref(i64 %Nob$a, i64 %arg171709)                 ; call prim_vector_45ref
  %retprim171381 = call i64 @prim_cdr(i64 %a171259)                                  ; call prim_cdr
  %cloptr174920 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr174922 = getelementptr inbounds i64, i64* %cloptr174920, i64 1                ; &eptr174922[1]
  %eptr174923 = getelementptr inbounds i64, i64* %cloptr174920, i64 2                ; &eptr174923[2]
  %eptr174924 = getelementptr inbounds i64, i64* %cloptr174920, i64 3                ; &eptr174924[3]
  store i64 %las$cc, i64* %eptr174922                                                ; *eptr174922 = %las$cc
  store i64 %Nob$a, i64* %eptr174923                                                 ; *eptr174923 = %Nob$a
  store i64 %cont171376, i64* %eptr174924                                            ; *eptr174924 = %cont171376
  %eptr174921 = getelementptr inbounds i64, i64* %cloptr174920, i64 0                ; &cloptr174920[0]
  %f174925 = ptrtoint void(i64,i64)* @lam174360 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174925, i64* %eptr174921                                               ; store fptr
  %arg171714 = ptrtoint i64* %cloptr174920 to i64                                    ; closure cast; i64* -> i64
  %arg171713 = add i64 0, 0                                                          ; quoted ()
  %rva172719 = add i64 0, 0                                                          ; quoted ()
  %rva172718 = call i64 @prim_cons(i64 %retprim171381, i64 %rva172719)               ; call prim_cons
  %rva172717 = call i64 @prim_cons(i64 %arg171713, i64 %rva172718)                   ; call prim_cons
  %cloptr174926 = inttoptr i64 %arg171714 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr174927 = getelementptr inbounds i64, i64* %cloptr174926, i64 0               ; &cloptr174926[0]
  %f174929 = load i64, i64* %i0ptr174927, align 8                                    ; load; *i0ptr174927
  %fptr174928 = inttoptr i64 %f174929 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174928(i64 %arg171714, i64 %rva172717)              ; tail call
  ret void

else174919:
  %arg171728 = add i64 0, 0                                                          ; quoted ()
  %arg171727 = call i64 @const_init_false()                                          ; quoted #f
  %rva172722 = add i64 0, 0                                                          ; quoted ()
  %rva172721 = call i64 @prim_cons(i64 %arg171727, i64 %rva172722)                   ; call prim_cons
  %rva172720 = call i64 @prim_cons(i64 %arg171728, i64 %rva172721)                   ; call prim_cons
  %cloptr174930 = inttoptr i64 %cont171376 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr174931 = getelementptr inbounds i64, i64* %cloptr174930, i64 0               ; &cloptr174930[0]
  %f174933 = load i64, i64* %i0ptr174931, align 8                                    ; load; *i0ptr174931
  %fptr174932 = inttoptr i64 %f174933 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174932(i64 %cont171376, i64 %rva172720)             ; tail call
  ret void
}


define void @lam174360(i64 %env174361, i64 %rvp172716) {
  %envptr174934 = inttoptr i64 %env174361 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174935 = getelementptr inbounds i64, i64* %envptr174934, i64 3              ; &envptr174934[3]
  %cont171376 = load i64, i64* %envptr174935, align 8                                ; load; *envptr174935
  %envptr174936 = inttoptr i64 %env174361 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174937 = getelementptr inbounds i64, i64* %envptr174936, i64 2              ; &envptr174936[2]
  %Nob$a = load i64, i64* %envptr174937, align 8                                     ; load; *envptr174937
  %envptr174938 = inttoptr i64 %env174361 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174939 = getelementptr inbounds i64, i64* %envptr174938, i64 1              ; &envptr174938[1]
  %las$cc = load i64, i64* %envptr174939, align 8                                    ; load; *envptr174939
  %_95171378 = call i64 @prim_car(i64 %rvp172716)                                    ; call prim_car
  %rvp172715 = call i64 @prim_cdr(i64 %rvp172716)                                    ; call prim_cdr
  %pF6$b = call i64 @prim_car(i64 %rvp172715)                                        ; call prim_car
  %na172704 = call i64 @prim_cdr(i64 %rvp172715)                                     ; call prim_cdr
  %arg171715 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171260 = call i64 @prim_vector_45ref(i64 %Nob$a, i64 %arg171715)                 ; call prim_vector_45ref
  %a171261 = call i64 @prim_cdr(i64 %a171260)                                        ; call prim_cdr
  %arg171719 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171380 = call i64 @prim_vector_45set_33(i64 %Nob$a, i64 %arg171719, i64 %a171261); call prim_vector_45set_33
  %cloptr174940 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr174942 = getelementptr inbounds i64, i64* %cloptr174940, i64 1                ; &eptr174942[1]
  %eptr174943 = getelementptr inbounds i64, i64* %cloptr174940, i64 2                ; &eptr174943[2]
  store i64 %las$cc, i64* %eptr174942                                                ; *eptr174942 = %las$cc
  store i64 %cont171376, i64* %eptr174943                                            ; *eptr174943 = %cont171376
  %eptr174941 = getelementptr inbounds i64, i64* %cloptr174940, i64 0                ; &cloptr174940[0]
  %f174944 = ptrtoint void(i64,i64)* @lam174356 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174944, i64* %eptr174941                                               ; store fptr
  %arg171723 = ptrtoint i64* %cloptr174940 to i64                                    ; closure cast; i64* -> i64
  %arg171722 = add i64 0, 0                                                          ; quoted ()
  %rva172714 = add i64 0, 0                                                          ; quoted ()
  %rva172713 = call i64 @prim_cons(i64 %retprim171380, i64 %rva172714)               ; call prim_cons
  %rva172712 = call i64 @prim_cons(i64 %arg171722, i64 %rva172713)                   ; call prim_cons
  %cloptr174945 = inttoptr i64 %arg171723 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr174946 = getelementptr inbounds i64, i64* %cloptr174945, i64 0               ; &cloptr174945[0]
  %f174948 = load i64, i64* %i0ptr174946, align 8                                    ; load; *i0ptr174946
  %fptr174947 = inttoptr i64 %f174948 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174947(i64 %arg171723, i64 %rva172712)              ; tail call
  ret void
}


define void @lam174356(i64 %env174357, i64 %rvp172711) {
  %envptr174949 = inttoptr i64 %env174357 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174950 = getelementptr inbounds i64, i64* %envptr174949, i64 2              ; &envptr174949[2]
  %cont171376 = load i64, i64* %envptr174950, align 8                                ; load; *envptr174950
  %envptr174951 = inttoptr i64 %env174357 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174952 = getelementptr inbounds i64, i64* %envptr174951, i64 1              ; &envptr174951[1]
  %las$cc = load i64, i64* %envptr174952, align 8                                    ; load; *envptr174952
  %_95171379 = call i64 @prim_car(i64 %rvp172711)                                    ; call prim_car
  %rvp172710 = call i64 @prim_cdr(i64 %rvp172711)                                    ; call prim_cdr
  %bJL$_950 = call i64 @prim_car(i64 %rvp172710)                                     ; call prim_car
  %na172706 = call i64 @prim_cdr(i64 %rvp172710)                                     ; call prim_cdr
  %rva172709 = add i64 0, 0                                                          ; quoted ()
  %rva172708 = call i64 @prim_cons(i64 %las$cc, i64 %rva172709)                      ; call prim_cons
  %rva172707 = call i64 @prim_cons(i64 %cont171376, i64 %rva172708)                  ; call prim_cons
  %cloptr174953 = inttoptr i64 %las$cc to i64*                                       ; closure/env cast; i64 -> i64*
  %i0ptr174954 = getelementptr inbounds i64, i64* %cloptr174953, i64 0               ; &cloptr174953[0]
  %f174956 = load i64, i64* %i0ptr174954, align 8                                    ; load; *i0ptr174954
  %fptr174955 = inttoptr i64 %f174956 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174955(i64 %las$cc, i64 %rva172707)                 ; tail call
  ret void
}


define void @lam174346(i64 %env174347, i64 %rvp172751) {
  %envptr174957 = inttoptr i64 %env174347 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174958 = getelementptr inbounds i64, i64* %envptr174957, i64 2              ; &envptr174957[2]
  %cont171376 = load i64, i64* %envptr174958, align 8                                ; load; *envptr174958
  %envptr174959 = inttoptr i64 %env174347 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174960 = getelementptr inbounds i64, i64* %envptr174959, i64 1              ; &envptr174959[1]
  %Nob$a = load i64, i64* %envptr174960, align 8                                     ; load; *envptr174960
  %_95171377 = call i64 @prim_car(i64 %rvp172751)                                    ; call prim_car
  %rvp172750 = call i64 @prim_cdr(i64 %rvp172751)                                    ; call prim_cdr
  %las$cc = call i64 @prim_car(i64 %rvp172750)                                       ; call prim_car
  %na172726 = call i64 @prim_cdr(i64 %rvp172750)                                     ; call prim_cdr
  %arg171730 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171255 = call i64 @prim_vector_45ref(i64 %Nob$a, i64 %arg171730)                 ; call prim_vector_45ref
  %a171256 = call i64 @prim_null_63(i64 %a171255)                                    ; call prim_null_63
  %cmp174961 = icmp eq i64 %a171256, 15                                              ; false?
  br i1 %cmp174961, label %else174963, label %then174962                             ; if

then174962:
  %arg171734 = add i64 0, 0                                                          ; quoted ()
  %arg171733 = call i64 @const_init_true()                                           ; quoted #t
  %rva172729 = add i64 0, 0                                                          ; quoted ()
  %rva172728 = call i64 @prim_cons(i64 %arg171733, i64 %rva172729)                   ; call prim_cons
  %rva172727 = call i64 @prim_cons(i64 %arg171734, i64 %rva172728)                   ; call prim_cons
  %cloptr174964 = inttoptr i64 %cont171376 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr174965 = getelementptr inbounds i64, i64* %cloptr174964, i64 0               ; &cloptr174964[0]
  %f174967 = load i64, i64* %i0ptr174965, align 8                                    ; load; *i0ptr174965
  %fptr174966 = inttoptr i64 %f174967 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174966(i64 %cont171376, i64 %rva172727)             ; tail call
  ret void

else174963:
  %arg171736 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171257 = call i64 @prim_vector_45ref(i64 %Nob$a, i64 %arg171736)                 ; call prim_vector_45ref
  %a171258 = call i64 @prim_cons_63(i64 %a171257)                                    ; call prim_cons_63
  %cmp174968 = icmp eq i64 %a171258, 15                                              ; false?
  br i1 %cmp174968, label %else174970, label %then174969                             ; if

then174969:
  %arg171739 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171259 = call i64 @prim_vector_45ref(i64 %Nob$a, i64 %arg171739)                 ; call prim_vector_45ref
  %retprim171381 = call i64 @prim_cdr(i64 %a171259)                                  ; call prim_cdr
  %cloptr174971 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr174973 = getelementptr inbounds i64, i64* %cloptr174971, i64 1                ; &eptr174973[1]
  %eptr174974 = getelementptr inbounds i64, i64* %cloptr174971, i64 2                ; &eptr174974[2]
  %eptr174975 = getelementptr inbounds i64, i64* %cloptr174971, i64 3                ; &eptr174975[3]
  store i64 %las$cc, i64* %eptr174973                                                ; *eptr174973 = %las$cc
  store i64 %Nob$a, i64* %eptr174974                                                 ; *eptr174974 = %Nob$a
  store i64 %cont171376, i64* %eptr174975                                            ; *eptr174975 = %cont171376
  %eptr174972 = getelementptr inbounds i64, i64* %cloptr174971, i64 0                ; &cloptr174971[0]
  %f174976 = ptrtoint void(i64,i64)* @lam174338 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174976, i64* %eptr174972                                               ; store fptr
  %arg171744 = ptrtoint i64* %cloptr174971 to i64                                    ; closure cast; i64* -> i64
  %arg171743 = add i64 0, 0                                                          ; quoted ()
  %rva172746 = add i64 0, 0                                                          ; quoted ()
  %rva172745 = call i64 @prim_cons(i64 %retprim171381, i64 %rva172746)               ; call prim_cons
  %rva172744 = call i64 @prim_cons(i64 %arg171743, i64 %rva172745)                   ; call prim_cons
  %cloptr174977 = inttoptr i64 %arg171744 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr174978 = getelementptr inbounds i64, i64* %cloptr174977, i64 0               ; &cloptr174977[0]
  %f174980 = load i64, i64* %i0ptr174978, align 8                                    ; load; *i0ptr174978
  %fptr174979 = inttoptr i64 %f174980 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174979(i64 %arg171744, i64 %rva172744)              ; tail call
  ret void

else174970:
  %arg171758 = add i64 0, 0                                                          ; quoted ()
  %arg171757 = call i64 @const_init_false()                                          ; quoted #f
  %rva172749 = add i64 0, 0                                                          ; quoted ()
  %rva172748 = call i64 @prim_cons(i64 %arg171757, i64 %rva172749)                   ; call prim_cons
  %rva172747 = call i64 @prim_cons(i64 %arg171758, i64 %rva172748)                   ; call prim_cons
  %cloptr174981 = inttoptr i64 %cont171376 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr174982 = getelementptr inbounds i64, i64* %cloptr174981, i64 0               ; &cloptr174981[0]
  %f174984 = load i64, i64* %i0ptr174982, align 8                                    ; load; *i0ptr174982
  %fptr174983 = inttoptr i64 %f174984 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174983(i64 %cont171376, i64 %rva172747)             ; tail call
  ret void
}


define void @lam174338(i64 %env174339, i64 %rvp172743) {
  %envptr174985 = inttoptr i64 %env174339 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174986 = getelementptr inbounds i64, i64* %envptr174985, i64 3              ; &envptr174985[3]
  %cont171376 = load i64, i64* %envptr174986, align 8                                ; load; *envptr174986
  %envptr174987 = inttoptr i64 %env174339 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174988 = getelementptr inbounds i64, i64* %envptr174987, i64 2              ; &envptr174987[2]
  %Nob$a = load i64, i64* %envptr174988, align 8                                     ; load; *envptr174988
  %envptr174989 = inttoptr i64 %env174339 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr174990 = getelementptr inbounds i64, i64* %envptr174989, i64 1              ; &envptr174989[1]
  %las$cc = load i64, i64* %envptr174990, align 8                                    ; load; *envptr174990
  %_95171378 = call i64 @prim_car(i64 %rvp172743)                                    ; call prim_car
  %rvp172742 = call i64 @prim_cdr(i64 %rvp172743)                                    ; call prim_cdr
  %pF6$b = call i64 @prim_car(i64 %rvp172742)                                        ; call prim_car
  %na172731 = call i64 @prim_cdr(i64 %rvp172742)                                     ; call prim_cdr
  %arg171745 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171260 = call i64 @prim_vector_45ref(i64 %Nob$a, i64 %arg171745)                 ; call prim_vector_45ref
  %a171261 = call i64 @prim_cdr(i64 %a171260)                                        ; call prim_cdr
  %arg171749 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171380 = call i64 @prim_vector_45set_33(i64 %Nob$a, i64 %arg171749, i64 %a171261); call prim_vector_45set_33
  %cloptr174991 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr174993 = getelementptr inbounds i64, i64* %cloptr174991, i64 1                ; &eptr174993[1]
  %eptr174994 = getelementptr inbounds i64, i64* %cloptr174991, i64 2                ; &eptr174994[2]
  store i64 %las$cc, i64* %eptr174993                                                ; *eptr174993 = %las$cc
  store i64 %cont171376, i64* %eptr174994                                            ; *eptr174994 = %cont171376
  %eptr174992 = getelementptr inbounds i64, i64* %cloptr174991, i64 0                ; &cloptr174991[0]
  %f174995 = ptrtoint void(i64,i64)* @lam174334 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f174995, i64* %eptr174992                                               ; store fptr
  %arg171753 = ptrtoint i64* %cloptr174991 to i64                                    ; closure cast; i64* -> i64
  %arg171752 = add i64 0, 0                                                          ; quoted ()
  %rva172741 = add i64 0, 0                                                          ; quoted ()
  %rva172740 = call i64 @prim_cons(i64 %retprim171380, i64 %rva172741)               ; call prim_cons
  %rva172739 = call i64 @prim_cons(i64 %arg171752, i64 %rva172740)                   ; call prim_cons
  %cloptr174996 = inttoptr i64 %arg171753 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr174997 = getelementptr inbounds i64, i64* %cloptr174996, i64 0               ; &cloptr174996[0]
  %f174999 = load i64, i64* %i0ptr174997, align 8                                    ; load; *i0ptr174997
  %fptr174998 = inttoptr i64 %f174999 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr174998(i64 %arg171753, i64 %rva172739)              ; tail call
  ret void
}


define void @lam174334(i64 %env174335, i64 %rvp172738) {
  %envptr175000 = inttoptr i64 %env174335 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175001 = getelementptr inbounds i64, i64* %envptr175000, i64 2              ; &envptr175000[2]
  %cont171376 = load i64, i64* %envptr175001, align 8                                ; load; *envptr175001
  %envptr175002 = inttoptr i64 %env174335 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175003 = getelementptr inbounds i64, i64* %envptr175002, i64 1              ; &envptr175002[1]
  %las$cc = load i64, i64* %envptr175003, align 8                                    ; load; *envptr175003
  %_95171379 = call i64 @prim_car(i64 %rvp172738)                                    ; call prim_car
  %rvp172737 = call i64 @prim_cdr(i64 %rvp172738)                                    ; call prim_cdr
  %bJL$_950 = call i64 @prim_car(i64 %rvp172737)                                     ; call prim_car
  %na172733 = call i64 @prim_cdr(i64 %rvp172737)                                     ; call prim_cdr
  %rva172736 = add i64 0, 0                                                          ; quoted ()
  %rva172735 = call i64 @prim_cons(i64 %las$cc, i64 %rva172736)                      ; call prim_cons
  %rva172734 = call i64 @prim_cons(i64 %cont171376, i64 %rva172735)                  ; call prim_cons
  %cloptr175004 = inttoptr i64 %las$cc to i64*                                       ; closure/env cast; i64 -> i64*
  %i0ptr175005 = getelementptr inbounds i64, i64* %cloptr175004, i64 0               ; &cloptr175004[0]
  %f175007 = load i64, i64* %i0ptr175005, align 8                                    ; load; *i0ptr175005
  %fptr175006 = inttoptr i64 %f175007 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175006(i64 %las$cc, i64 %rva172734)                 ; tail call
  ret void
}


define void @lam174323(i64 %env174324, i64 %rvp172819) {
  %cont171383 = call i64 @prim_car(i64 %rvp172819)                                   ; call prim_car
  %rvp172818 = call i64 @prim_cdr(i64 %rvp172819)                                    ; call prim_cdr
  %y95$lst = call i64 @prim_car(i64 %rvp172818)                                      ; call prim_car
  %rvp172817 = call i64 @prim_cdr(i64 %rvp172818)                                    ; call prim_cdr
  %lVM$n = call i64 @prim_car(i64 %rvp172817)                                        ; call prim_car
  %na172758 = call i64 @prim_cdr(i64 %rvp172817)                                     ; call prim_cdr
  %arg171761 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %WbF$lst = call i64 @prim_make_45vector(i64 %arg171761, i64 %y95$lst)              ; call prim_make_45vector
  %arg171763 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %B1g$n = call i64 @prim_make_45vector(i64 %arg171763, i64 %lVM$n)                  ; call prim_make_45vector
  %cloptr175008 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr175009 = getelementptr inbounds i64, i64* %cloptr175008, i64 0                ; &cloptr175008[0]
  %f175010 = ptrtoint void(i64,i64)* @lam174319 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175010, i64* %eptr175009                                               ; store fptr
  %arg171766 = ptrtoint i64* %cloptr175008 to i64                                    ; closure cast; i64* -> i64
  %cloptr175011 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175013 = getelementptr inbounds i64, i64* %cloptr175011, i64 1                ; &eptr175013[1]
  %eptr175014 = getelementptr inbounds i64, i64* %cloptr175011, i64 2                ; &eptr175014[2]
  %eptr175015 = getelementptr inbounds i64, i64* %cloptr175011, i64 3                ; &eptr175015[3]
  store i64 %cont171383, i64* %eptr175013                                            ; *eptr175013 = %cont171383
  store i64 %WbF$lst, i64* %eptr175014                                               ; *eptr175014 = %WbF$lst
  store i64 %B1g$n, i64* %eptr175015                                                 ; *eptr175015 = %B1g$n
  %eptr175012 = getelementptr inbounds i64, i64* %cloptr175011, i64 0                ; &cloptr175011[0]
  %f175016 = ptrtoint void(i64,i64)* @lam174316 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175016, i64* %eptr175012                                               ; store fptr
  %arg171765 = ptrtoint i64* %cloptr175011 to i64                                    ; closure cast; i64* -> i64
  %cloptr175017 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175019 = getelementptr inbounds i64, i64* %cloptr175017, i64 1                ; &eptr175019[1]
  %eptr175020 = getelementptr inbounds i64, i64* %cloptr175017, i64 2                ; &eptr175020[2]
  %eptr175021 = getelementptr inbounds i64, i64* %cloptr175017, i64 3                ; &eptr175021[3]
  store i64 %cont171383, i64* %eptr175019                                            ; *eptr175019 = %cont171383
  store i64 %WbF$lst, i64* %eptr175020                                               ; *eptr175020 = %WbF$lst
  store i64 %B1g$n, i64* %eptr175021                                                 ; *eptr175021 = %B1g$n
  %eptr175018 = getelementptr inbounds i64, i64* %cloptr175017, i64 0                ; &cloptr175017[0]
  %f175022 = ptrtoint void(i64,i64)* @lam174295 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175022, i64* %eptr175018                                               ; store fptr
  %arg171764 = ptrtoint i64* %cloptr175017 to i64                                    ; closure cast; i64* -> i64
  %rva172816 = add i64 0, 0                                                          ; quoted ()
  %rva172815 = call i64 @prim_cons(i64 %arg171764, i64 %rva172816)                   ; call prim_cons
  %rva172814 = call i64 @prim_cons(i64 %arg171765, i64 %rva172815)                   ; call prim_cons
  %cloptr175023 = inttoptr i64 %arg171766 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175024 = getelementptr inbounds i64, i64* %cloptr175023, i64 0               ; &cloptr175023[0]
  %f175026 = load i64, i64* %i0ptr175024, align 8                                    ; load; *i0ptr175024
  %fptr175025 = inttoptr i64 %f175026 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175025(i64 %arg171766, i64 %rva172814)              ; tail call
  ret void
}


define void @lam174319(i64 %env174320, i64 %rvp172765) {
  %cont171390 = call i64 @prim_car(i64 %rvp172765)                                   ; call prim_car
  %rvp172764 = call i64 @prim_cdr(i64 %rvp172765)                                    ; call prim_cdr
  %hh1$u = call i64 @prim_car(i64 %rvp172764)                                        ; call prim_car
  %na172760 = call i64 @prim_cdr(i64 %rvp172764)                                     ; call prim_cdr
  %rva172763 = add i64 0, 0                                                          ; quoted ()
  %rva172762 = call i64 @prim_cons(i64 %hh1$u, i64 %rva172763)                       ; call prim_cons
  %rva172761 = call i64 @prim_cons(i64 %cont171390, i64 %rva172762)                  ; call prim_cons
  %cloptr175027 = inttoptr i64 %hh1$u to i64*                                        ; closure/env cast; i64 -> i64*
  %i0ptr175028 = getelementptr inbounds i64, i64* %cloptr175027, i64 0               ; &cloptr175027[0]
  %f175030 = load i64, i64* %i0ptr175028, align 8                                    ; load; *i0ptr175028
  %fptr175029 = inttoptr i64 %f175030 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175029(i64 %hh1$u, i64 %rva172761)                  ; tail call
  ret void
}


define void @lam174316(i64 %env174317, i64 %rvp172789) {
  %envptr175031 = inttoptr i64 %env174317 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175032 = getelementptr inbounds i64, i64* %envptr175031, i64 3              ; &envptr175031[3]
  %B1g$n = load i64, i64* %envptr175032, align 8                                     ; load; *envptr175032
  %envptr175033 = inttoptr i64 %env174317 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175034 = getelementptr inbounds i64, i64* %envptr175033, i64 2              ; &envptr175033[2]
  %WbF$lst = load i64, i64* %envptr175034, align 8                                   ; load; *envptr175034
  %envptr175035 = inttoptr i64 %env174317 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175036 = getelementptr inbounds i64, i64* %envptr175035, i64 1              ; &envptr175035[1]
  %cont171383 = load i64, i64* %envptr175036, align 8                                ; load; *envptr175036
  %_95171384 = call i64 @prim_car(i64 %rvp172789)                                    ; call prim_car
  %rvp172788 = call i64 @prim_cdr(i64 %rvp172789)                                    ; call prim_cdr
  %sqe$cc = call i64 @prim_car(i64 %rvp172788)                                       ; call prim_car
  %na172767 = call i64 @prim_cdr(i64 %rvp172788)                                     ; call prim_cdr
  %arg171770 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171262 = call i64 @prim_vector_45ref(i64 %B1g$n, i64 %arg171770)                 ; call prim_vector_45ref
  %arg171773 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171263 = call i64 @prim__61(i64 %arg171773, i64 %a171262)                        ; call prim__61
  %cmp175037 = icmp eq i64 %a171263, 15                                              ; false?
  br i1 %cmp175037, label %else175039, label %then175038                             ; if

then175038:
  %arg171774 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171385 = call i64 @prim_vector_45ref(i64 %WbF$lst, i64 %arg171774)         ; call prim_vector_45ref
  %arg171777 = add i64 0, 0                                                          ; quoted ()
  %rva172770 = add i64 0, 0                                                          ; quoted ()
  %rva172769 = call i64 @prim_cons(i64 %retprim171385, i64 %rva172770)               ; call prim_cons
  %rva172768 = call i64 @prim_cons(i64 %arg171777, i64 %rva172769)                   ; call prim_cons
  %cloptr175040 = inttoptr i64 %cont171383 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175041 = getelementptr inbounds i64, i64* %cloptr175040, i64 0               ; &cloptr175040[0]
  %f175043 = load i64, i64* %i0ptr175041, align 8                                    ; load; *i0ptr175041
  %fptr175042 = inttoptr i64 %f175043 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175042(i64 %cont171383, i64 %rva172768)             ; tail call
  ret void

else175039:
  %arg171779 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171264 = call i64 @prim_vector_45ref(i64 %WbF$lst, i64 %arg171779)               ; call prim_vector_45ref
  %a171265 = call i64 @prim_cdr(i64 %a171264)                                        ; call prim_cdr
  %arg171783 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171389 = call i64 @prim_vector_45set_33(i64 %WbF$lst, i64 %arg171783, i64 %a171265); call prim_vector_45set_33
  %cloptr175044 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175046 = getelementptr inbounds i64, i64* %cloptr175044, i64 1                ; &eptr175046[1]
  %eptr175047 = getelementptr inbounds i64, i64* %cloptr175044, i64 2                ; &eptr175047[2]
  %eptr175048 = getelementptr inbounds i64, i64* %cloptr175044, i64 3                ; &eptr175048[3]
  store i64 %cont171383, i64* %eptr175046                                            ; *eptr175046 = %cont171383
  store i64 %sqe$cc, i64* %eptr175047                                                ; *eptr175047 = %sqe$cc
  store i64 %B1g$n, i64* %eptr175048                                                 ; *eptr175048 = %B1g$n
  %eptr175045 = getelementptr inbounds i64, i64* %cloptr175044, i64 0                ; &cloptr175044[0]
  %f175049 = ptrtoint void(i64,i64)* @lam174310 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175049, i64* %eptr175045                                               ; store fptr
  %arg171787 = ptrtoint i64* %cloptr175044 to i64                                    ; closure cast; i64* -> i64
  %arg171786 = add i64 0, 0                                                          ; quoted ()
  %rva172787 = add i64 0, 0                                                          ; quoted ()
  %rva172786 = call i64 @prim_cons(i64 %retprim171389, i64 %rva172787)               ; call prim_cons
  %rva172785 = call i64 @prim_cons(i64 %arg171786, i64 %rva172786)                   ; call prim_cons
  %cloptr175050 = inttoptr i64 %arg171787 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175051 = getelementptr inbounds i64, i64* %cloptr175050, i64 0               ; &cloptr175050[0]
  %f175053 = load i64, i64* %i0ptr175051, align 8                                    ; load; *i0ptr175051
  %fptr175052 = inttoptr i64 %f175053 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175052(i64 %arg171787, i64 %rva172785)              ; tail call
  ret void
}


define void @lam174310(i64 %env174311, i64 %rvp172784) {
  %envptr175054 = inttoptr i64 %env174311 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175055 = getelementptr inbounds i64, i64* %envptr175054, i64 3              ; &envptr175054[3]
  %B1g$n = load i64, i64* %envptr175055, align 8                                     ; load; *envptr175055
  %envptr175056 = inttoptr i64 %env174311 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175057 = getelementptr inbounds i64, i64* %envptr175056, i64 2              ; &envptr175056[2]
  %sqe$cc = load i64, i64* %envptr175057, align 8                                    ; load; *envptr175057
  %envptr175058 = inttoptr i64 %env174311 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175059 = getelementptr inbounds i64, i64* %envptr175058, i64 1              ; &envptr175058[1]
  %cont171383 = load i64, i64* %envptr175059, align 8                                ; load; *envptr175059
  %_95171386 = call i64 @prim_car(i64 %rvp172784)                                    ; call prim_car
  %rvp172783 = call i64 @prim_cdr(i64 %rvp172784)                                    ; call prim_cdr
  %SPF$_950 = call i64 @prim_car(i64 %rvp172783)                                     ; call prim_car
  %na172772 = call i64 @prim_cdr(i64 %rvp172783)                                     ; call prim_cdr
  %arg171788 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171266 = call i64 @prim_vector_45ref(i64 %B1g$n, i64 %arg171788)                 ; call prim_vector_45ref
  %arg171790 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %a171267 = call i64 @prim__45(i64 %a171266, i64 %arg171790)                        ; call prim__45
  %arg171793 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171388 = call i64 @prim_vector_45set_33(i64 %B1g$n, i64 %arg171793, i64 %a171267); call prim_vector_45set_33
  %cloptr175060 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr175062 = getelementptr inbounds i64, i64* %cloptr175060, i64 1                ; &eptr175062[1]
  %eptr175063 = getelementptr inbounds i64, i64* %cloptr175060, i64 2                ; &eptr175063[2]
  store i64 %cont171383, i64* %eptr175062                                            ; *eptr175062 = %cont171383
  store i64 %sqe$cc, i64* %eptr175063                                                ; *eptr175063 = %sqe$cc
  %eptr175061 = getelementptr inbounds i64, i64* %cloptr175060, i64 0                ; &cloptr175060[0]
  %f175064 = ptrtoint void(i64,i64)* @lam174305 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175064, i64* %eptr175061                                               ; store fptr
  %arg171797 = ptrtoint i64* %cloptr175060 to i64                                    ; closure cast; i64* -> i64
  %arg171796 = add i64 0, 0                                                          ; quoted ()
  %rva172782 = add i64 0, 0                                                          ; quoted ()
  %rva172781 = call i64 @prim_cons(i64 %retprim171388, i64 %rva172782)               ; call prim_cons
  %rva172780 = call i64 @prim_cons(i64 %arg171796, i64 %rva172781)                   ; call prim_cons
  %cloptr175065 = inttoptr i64 %arg171797 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175066 = getelementptr inbounds i64, i64* %cloptr175065, i64 0               ; &cloptr175065[0]
  %f175068 = load i64, i64* %i0ptr175066, align 8                                    ; load; *i0ptr175066
  %fptr175067 = inttoptr i64 %f175068 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175067(i64 %arg171797, i64 %rva172780)              ; tail call
  ret void
}


define void @lam174305(i64 %env174306, i64 %rvp172779) {
  %envptr175069 = inttoptr i64 %env174306 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175070 = getelementptr inbounds i64, i64* %envptr175069, i64 2              ; &envptr175069[2]
  %sqe$cc = load i64, i64* %envptr175070, align 8                                    ; load; *envptr175070
  %envptr175071 = inttoptr i64 %env174306 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175072 = getelementptr inbounds i64, i64* %envptr175071, i64 1              ; &envptr175071[1]
  %cont171383 = load i64, i64* %envptr175072, align 8                                ; load; *envptr175072
  %_95171387 = call i64 @prim_car(i64 %rvp172779)                                    ; call prim_car
  %rvp172778 = call i64 @prim_cdr(i64 %rvp172779)                                    ; call prim_cdr
  %ojF$_951 = call i64 @prim_car(i64 %rvp172778)                                     ; call prim_car
  %na172774 = call i64 @prim_cdr(i64 %rvp172778)                                     ; call prim_cdr
  %rva172777 = add i64 0, 0                                                          ; quoted ()
  %rva172776 = call i64 @prim_cons(i64 %sqe$cc, i64 %rva172777)                      ; call prim_cons
  %rva172775 = call i64 @prim_cons(i64 %cont171383, i64 %rva172776)                  ; call prim_cons
  %cloptr175073 = inttoptr i64 %sqe$cc to i64*                                       ; closure/env cast; i64 -> i64*
  %i0ptr175074 = getelementptr inbounds i64, i64* %cloptr175073, i64 0               ; &cloptr175073[0]
  %f175076 = load i64, i64* %i0ptr175074, align 8                                    ; load; *i0ptr175074
  %fptr175075 = inttoptr i64 %f175076 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175075(i64 %sqe$cc, i64 %rva172775)                 ; tail call
  ret void
}


define void @lam174295(i64 %env174296, i64 %rvp172813) {
  %envptr175077 = inttoptr i64 %env174296 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175078 = getelementptr inbounds i64, i64* %envptr175077, i64 3              ; &envptr175077[3]
  %B1g$n = load i64, i64* %envptr175078, align 8                                     ; load; *envptr175078
  %envptr175079 = inttoptr i64 %env174296 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175080 = getelementptr inbounds i64, i64* %envptr175079, i64 2              ; &envptr175079[2]
  %WbF$lst = load i64, i64* %envptr175080, align 8                                   ; load; *envptr175080
  %envptr175081 = inttoptr i64 %env174296 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175082 = getelementptr inbounds i64, i64* %envptr175081, i64 1              ; &envptr175081[1]
  %cont171383 = load i64, i64* %envptr175082, align 8                                ; load; *envptr175082
  %_95171384 = call i64 @prim_car(i64 %rvp172813)                                    ; call prim_car
  %rvp172812 = call i64 @prim_cdr(i64 %rvp172813)                                    ; call prim_cdr
  %sqe$cc = call i64 @prim_car(i64 %rvp172812)                                       ; call prim_car
  %na172791 = call i64 @prim_cdr(i64 %rvp172812)                                     ; call prim_cdr
  %arg171801 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171262 = call i64 @prim_vector_45ref(i64 %B1g$n, i64 %arg171801)                 ; call prim_vector_45ref
  %arg171804 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171263 = call i64 @prim__61(i64 %arg171804, i64 %a171262)                        ; call prim__61
  %cmp175083 = icmp eq i64 %a171263, 15                                              ; false?
  br i1 %cmp175083, label %else175085, label %then175084                             ; if

then175084:
  %arg171805 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171385 = call i64 @prim_vector_45ref(i64 %WbF$lst, i64 %arg171805)         ; call prim_vector_45ref
  %arg171808 = add i64 0, 0                                                          ; quoted ()
  %rva172794 = add i64 0, 0                                                          ; quoted ()
  %rva172793 = call i64 @prim_cons(i64 %retprim171385, i64 %rva172794)               ; call prim_cons
  %rva172792 = call i64 @prim_cons(i64 %arg171808, i64 %rva172793)                   ; call prim_cons
  %cloptr175086 = inttoptr i64 %cont171383 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175087 = getelementptr inbounds i64, i64* %cloptr175086, i64 0               ; &cloptr175086[0]
  %f175089 = load i64, i64* %i0ptr175087, align 8                                    ; load; *i0ptr175087
  %fptr175088 = inttoptr i64 %f175089 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175088(i64 %cont171383, i64 %rva172792)             ; tail call
  ret void

else175085:
  %arg171810 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171264 = call i64 @prim_vector_45ref(i64 %WbF$lst, i64 %arg171810)               ; call prim_vector_45ref
  %a171265 = call i64 @prim_cdr(i64 %a171264)                                        ; call prim_cdr
  %arg171814 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171389 = call i64 @prim_vector_45set_33(i64 %WbF$lst, i64 %arg171814, i64 %a171265); call prim_vector_45set_33
  %cloptr175090 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175092 = getelementptr inbounds i64, i64* %cloptr175090, i64 1                ; &eptr175092[1]
  %eptr175093 = getelementptr inbounds i64, i64* %cloptr175090, i64 2                ; &eptr175093[2]
  %eptr175094 = getelementptr inbounds i64, i64* %cloptr175090, i64 3                ; &eptr175094[3]
  store i64 %cont171383, i64* %eptr175092                                            ; *eptr175092 = %cont171383
  store i64 %sqe$cc, i64* %eptr175093                                                ; *eptr175093 = %sqe$cc
  store i64 %B1g$n, i64* %eptr175094                                                 ; *eptr175094 = %B1g$n
  %eptr175091 = getelementptr inbounds i64, i64* %cloptr175090, i64 0                ; &cloptr175090[0]
  %f175095 = ptrtoint void(i64,i64)* @lam174289 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175095, i64* %eptr175091                                               ; store fptr
  %arg171818 = ptrtoint i64* %cloptr175090 to i64                                    ; closure cast; i64* -> i64
  %arg171817 = add i64 0, 0                                                          ; quoted ()
  %rva172811 = add i64 0, 0                                                          ; quoted ()
  %rva172810 = call i64 @prim_cons(i64 %retprim171389, i64 %rva172811)               ; call prim_cons
  %rva172809 = call i64 @prim_cons(i64 %arg171817, i64 %rva172810)                   ; call prim_cons
  %cloptr175096 = inttoptr i64 %arg171818 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175097 = getelementptr inbounds i64, i64* %cloptr175096, i64 0               ; &cloptr175096[0]
  %f175099 = load i64, i64* %i0ptr175097, align 8                                    ; load; *i0ptr175097
  %fptr175098 = inttoptr i64 %f175099 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175098(i64 %arg171818, i64 %rva172809)              ; tail call
  ret void
}


define void @lam174289(i64 %env174290, i64 %rvp172808) {
  %envptr175100 = inttoptr i64 %env174290 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175101 = getelementptr inbounds i64, i64* %envptr175100, i64 3              ; &envptr175100[3]
  %B1g$n = load i64, i64* %envptr175101, align 8                                     ; load; *envptr175101
  %envptr175102 = inttoptr i64 %env174290 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175103 = getelementptr inbounds i64, i64* %envptr175102, i64 2              ; &envptr175102[2]
  %sqe$cc = load i64, i64* %envptr175103, align 8                                    ; load; *envptr175103
  %envptr175104 = inttoptr i64 %env174290 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175105 = getelementptr inbounds i64, i64* %envptr175104, i64 1              ; &envptr175104[1]
  %cont171383 = load i64, i64* %envptr175105, align 8                                ; load; *envptr175105
  %_95171386 = call i64 @prim_car(i64 %rvp172808)                                    ; call prim_car
  %rvp172807 = call i64 @prim_cdr(i64 %rvp172808)                                    ; call prim_cdr
  %SPF$_950 = call i64 @prim_car(i64 %rvp172807)                                     ; call prim_car
  %na172796 = call i64 @prim_cdr(i64 %rvp172807)                                     ; call prim_cdr
  %arg171819 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171266 = call i64 @prim_vector_45ref(i64 %B1g$n, i64 %arg171819)                 ; call prim_vector_45ref
  %arg171821 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %a171267 = call i64 @prim__45(i64 %a171266, i64 %arg171821)                        ; call prim__45
  %arg171824 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171388 = call i64 @prim_vector_45set_33(i64 %B1g$n, i64 %arg171824, i64 %a171267); call prim_vector_45set_33
  %cloptr175106 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr175108 = getelementptr inbounds i64, i64* %cloptr175106, i64 1                ; &eptr175108[1]
  %eptr175109 = getelementptr inbounds i64, i64* %cloptr175106, i64 2                ; &eptr175109[2]
  store i64 %cont171383, i64* %eptr175108                                            ; *eptr175108 = %cont171383
  store i64 %sqe$cc, i64* %eptr175109                                                ; *eptr175109 = %sqe$cc
  %eptr175107 = getelementptr inbounds i64, i64* %cloptr175106, i64 0                ; &cloptr175106[0]
  %f175110 = ptrtoint void(i64,i64)* @lam174284 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175110, i64* %eptr175107                                               ; store fptr
  %arg171828 = ptrtoint i64* %cloptr175106 to i64                                    ; closure cast; i64* -> i64
  %arg171827 = add i64 0, 0                                                          ; quoted ()
  %rva172806 = add i64 0, 0                                                          ; quoted ()
  %rva172805 = call i64 @prim_cons(i64 %retprim171388, i64 %rva172806)               ; call prim_cons
  %rva172804 = call i64 @prim_cons(i64 %arg171827, i64 %rva172805)                   ; call prim_cons
  %cloptr175111 = inttoptr i64 %arg171828 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175112 = getelementptr inbounds i64, i64* %cloptr175111, i64 0               ; &cloptr175111[0]
  %f175114 = load i64, i64* %i0ptr175112, align 8                                    ; load; *i0ptr175112
  %fptr175113 = inttoptr i64 %f175114 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175113(i64 %arg171828, i64 %rva172804)              ; tail call
  ret void
}


define void @lam174284(i64 %env174285, i64 %rvp172803) {
  %envptr175115 = inttoptr i64 %env174285 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175116 = getelementptr inbounds i64, i64* %envptr175115, i64 2              ; &envptr175115[2]
  %sqe$cc = load i64, i64* %envptr175116, align 8                                    ; load; *envptr175116
  %envptr175117 = inttoptr i64 %env174285 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175118 = getelementptr inbounds i64, i64* %envptr175117, i64 1              ; &envptr175117[1]
  %cont171383 = load i64, i64* %envptr175118, align 8                                ; load; *envptr175118
  %_95171387 = call i64 @prim_car(i64 %rvp172803)                                    ; call prim_car
  %rvp172802 = call i64 @prim_cdr(i64 %rvp172803)                                    ; call prim_cdr
  %ojF$_951 = call i64 @prim_car(i64 %rvp172802)                                     ; call prim_car
  %na172798 = call i64 @prim_cdr(i64 %rvp172802)                                     ; call prim_cdr
  %rva172801 = add i64 0, 0                                                          ; quoted ()
  %rva172800 = call i64 @prim_cons(i64 %sqe$cc, i64 %rva172801)                      ; call prim_cons
  %rva172799 = call i64 @prim_cons(i64 %cont171383, i64 %rva172800)                  ; call prim_cons
  %cloptr175119 = inttoptr i64 %sqe$cc to i64*                                       ; closure/env cast; i64 -> i64*
  %i0ptr175120 = getelementptr inbounds i64, i64* %cloptr175119, i64 0               ; &cloptr175119[0]
  %f175122 = load i64, i64* %i0ptr175120, align 8                                    ; load; *i0ptr175120
  %fptr175121 = inttoptr i64 %f175122 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175121(i64 %sqe$cc, i64 %rva172799)                 ; tail call
  ret void
}


define void @lam174273(i64 %env174274, i64 %rvp172874) {
  %cont171391 = call i64 @prim_car(i64 %rvp172874)                                   ; call prim_car
  %rvp172873 = call i64 @prim_cdr(i64 %rvp172874)                                    ; call prim_cdr
  %tcC$v = call i64 @prim_car(i64 %rvp172873)                                        ; call prim_car
  %rvp172872 = call i64 @prim_cdr(i64 %rvp172873)                                    ; call prim_cdr
  %oqq$lst = call i64 @prim_car(i64 %rvp172872)                                      ; call prim_car
  %na172821 = call i64 @prim_cdr(i64 %rvp172872)                                     ; call prim_cdr
  %arg171833 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %yZv$lst = call i64 @prim_make_45vector(i64 %arg171833, i64 %oqq$lst)              ; call prim_make_45vector
  %cloptr175123 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr175124 = getelementptr inbounds i64, i64* %cloptr175123, i64 0                ; &cloptr175123[0]
  %f175125 = ptrtoint void(i64,i64)* @lam174270 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175125, i64* %eptr175124                                               ; store fptr
  %arg171836 = ptrtoint i64* %cloptr175123 to i64                                    ; closure cast; i64* -> i64
  %cloptr175126 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175128 = getelementptr inbounds i64, i64* %cloptr175126, i64 1                ; &eptr175128[1]
  %eptr175129 = getelementptr inbounds i64, i64* %cloptr175126, i64 2                ; &eptr175129[2]
  %eptr175130 = getelementptr inbounds i64, i64* %cloptr175126, i64 3                ; &eptr175130[3]
  store i64 %cont171391, i64* %eptr175128                                            ; *eptr175128 = %cont171391
  store i64 %tcC$v, i64* %eptr175129                                                 ; *eptr175129 = %tcC$v
  store i64 %yZv$lst, i64* %eptr175130                                               ; *eptr175130 = %yZv$lst
  %eptr175127 = getelementptr inbounds i64, i64* %cloptr175126, i64 0                ; &cloptr175126[0]
  %f175131 = ptrtoint void(i64,i64)* @lam174267 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175131, i64* %eptr175127                                               ; store fptr
  %arg171835 = ptrtoint i64* %cloptr175126 to i64                                    ; closure cast; i64* -> i64
  %cloptr175132 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175134 = getelementptr inbounds i64, i64* %cloptr175132, i64 1                ; &eptr175134[1]
  %eptr175135 = getelementptr inbounds i64, i64* %cloptr175132, i64 2                ; &eptr175135[2]
  %eptr175136 = getelementptr inbounds i64, i64* %cloptr175132, i64 3                ; &eptr175136[3]
  store i64 %cont171391, i64* %eptr175134                                            ; *eptr175134 = %cont171391
  store i64 %tcC$v, i64* %eptr175135                                                 ; *eptr175135 = %tcC$v
  store i64 %yZv$lst, i64* %eptr175136                                               ; *eptr175136 = %yZv$lst
  %eptr175133 = getelementptr inbounds i64, i64* %cloptr175132, i64 0                ; &cloptr175132[0]
  %f175137 = ptrtoint void(i64,i64)* @lam174250 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175137, i64* %eptr175133                                               ; store fptr
  %arg171834 = ptrtoint i64* %cloptr175132 to i64                                    ; closure cast; i64* -> i64
  %rva172871 = add i64 0, 0                                                          ; quoted ()
  %rva172870 = call i64 @prim_cons(i64 %arg171834, i64 %rva172871)                   ; call prim_cons
  %rva172869 = call i64 @prim_cons(i64 %arg171835, i64 %rva172870)                   ; call prim_cons
  %cloptr175138 = inttoptr i64 %arg171836 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175139 = getelementptr inbounds i64, i64* %cloptr175138, i64 0               ; &cloptr175138[0]
  %f175141 = load i64, i64* %i0ptr175139, align 8                                    ; load; *i0ptr175139
  %fptr175140 = inttoptr i64 %f175141 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175140(i64 %arg171836, i64 %rva172869)              ; tail call
  ret void
}


define void @lam174270(i64 %env174271, i64 %rvp172828) {
  %cont171396 = call i64 @prim_car(i64 %rvp172828)                                   ; call prim_car
  %rvp172827 = call i64 @prim_cdr(i64 %rvp172828)                                    ; call prim_cdr
  %HVu$u = call i64 @prim_car(i64 %rvp172827)                                        ; call prim_car
  %na172823 = call i64 @prim_cdr(i64 %rvp172827)                                     ; call prim_cdr
  %rva172826 = add i64 0, 0                                                          ; quoted ()
  %rva172825 = call i64 @prim_cons(i64 %HVu$u, i64 %rva172826)                       ; call prim_cons
  %rva172824 = call i64 @prim_cons(i64 %cont171396, i64 %rva172825)                  ; call prim_cons
  %cloptr175142 = inttoptr i64 %HVu$u to i64*                                        ; closure/env cast; i64 -> i64*
  %i0ptr175143 = getelementptr inbounds i64, i64* %cloptr175142, i64 0               ; &cloptr175142[0]
  %f175145 = load i64, i64* %i0ptr175143, align 8                                    ; load; *i0ptr175143
  %fptr175144 = inttoptr i64 %f175145 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175144(i64 %HVu$u, i64 %rva172824)                  ; tail call
  ret void
}


define void @lam174267(i64 %env174268, i64 %rvp172848) {
  %envptr175146 = inttoptr i64 %env174268 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175147 = getelementptr inbounds i64, i64* %envptr175146, i64 3              ; &envptr175146[3]
  %yZv$lst = load i64, i64* %envptr175147, align 8                                   ; load; *envptr175147
  %envptr175148 = inttoptr i64 %env174268 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175149 = getelementptr inbounds i64, i64* %envptr175148, i64 2              ; &envptr175148[2]
  %tcC$v = load i64, i64* %envptr175149, align 8                                     ; load; *envptr175149
  %envptr175150 = inttoptr i64 %env174268 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175151 = getelementptr inbounds i64, i64* %envptr175150, i64 1              ; &envptr175150[1]
  %cont171391 = load i64, i64* %envptr175151, align 8                                ; load; *envptr175151
  %_95171392 = call i64 @prim_car(i64 %rvp172848)                                    ; call prim_car
  %rvp172847 = call i64 @prim_cdr(i64 %rvp172848)                                    ; call prim_cdr
  %fm1$cc = call i64 @prim_car(i64 %rvp172847)                                       ; call prim_car
  %na172830 = call i64 @prim_cdr(i64 %rvp172847)                                     ; call prim_cdr
  %arg171840 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171268 = call i64 @prim_vector_45ref(i64 %yZv$lst, i64 %arg171840)               ; call prim_vector_45ref
  %a171269 = call i64 @prim_null_63(i64 %a171268)                                    ; call prim_null_63
  %cmp175152 = icmp eq i64 %a171269, 15                                              ; false?
  br i1 %cmp175152, label %else175154, label %then175153                             ; if

then175153:
  %arg171844 = add i64 0, 0                                                          ; quoted ()
  %arg171843 = call i64 @const_init_false()                                          ; quoted #f
  %rva172833 = add i64 0, 0                                                          ; quoted ()
  %rva172832 = call i64 @prim_cons(i64 %arg171843, i64 %rva172833)                   ; call prim_cons
  %rva172831 = call i64 @prim_cons(i64 %arg171844, i64 %rva172832)                   ; call prim_cons
  %cloptr175155 = inttoptr i64 %cont171391 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175156 = getelementptr inbounds i64, i64* %cloptr175155, i64 0               ; &cloptr175155[0]
  %f175158 = load i64, i64* %i0ptr175156, align 8                                    ; load; *i0ptr175156
  %fptr175157 = inttoptr i64 %f175158 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175157(i64 %cont171391, i64 %rva172831)             ; tail call
  ret void

else175154:
  %arg171846 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171270 = call i64 @prim_vector_45ref(i64 %yZv$lst, i64 %arg171846)               ; call prim_vector_45ref
  %a171271 = call i64 @prim_car(i64 %a171270)                                        ; call prim_car
  %a171272 = call i64 @prim_eqv_63(i64 %a171271, i64 %tcC$v)                         ; call prim_eqv_63
  %cmp175159 = icmp eq i64 %a171272, 15                                              ; false?
  br i1 %cmp175159, label %else175161, label %then175160                             ; if

then175160:
  %arg171851 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171393 = call i64 @prim_vector_45ref(i64 %yZv$lst, i64 %arg171851)         ; call prim_vector_45ref
  %arg171854 = add i64 0, 0                                                          ; quoted ()
  %rva172836 = add i64 0, 0                                                          ; quoted ()
  %rva172835 = call i64 @prim_cons(i64 %retprim171393, i64 %rva172836)               ; call prim_cons
  %rva172834 = call i64 @prim_cons(i64 %arg171854, i64 %rva172835)                   ; call prim_cons
  %cloptr175162 = inttoptr i64 %cont171391 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175163 = getelementptr inbounds i64, i64* %cloptr175162, i64 0               ; &cloptr175162[0]
  %f175165 = load i64, i64* %i0ptr175163, align 8                                    ; load; *i0ptr175163
  %fptr175164 = inttoptr i64 %f175165 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175164(i64 %cont171391, i64 %rva172834)             ; tail call
  ret void

else175161:
  %arg171856 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171273 = call i64 @prim_vector_45ref(i64 %yZv$lst, i64 %arg171856)               ; call prim_vector_45ref
  %a171274 = call i64 @prim_cdr(i64 %a171273)                                        ; call prim_cdr
  %arg171860 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171395 = call i64 @prim_vector_45set_33(i64 %yZv$lst, i64 %arg171860, i64 %a171274); call prim_vector_45set_33
  %cloptr175166 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr175168 = getelementptr inbounds i64, i64* %cloptr175166, i64 1                ; &eptr175168[1]
  %eptr175169 = getelementptr inbounds i64, i64* %cloptr175166, i64 2                ; &eptr175169[2]
  store i64 %cont171391, i64* %eptr175168                                            ; *eptr175168 = %cont171391
  store i64 %fm1$cc, i64* %eptr175169                                                ; *eptr175169 = %fm1$cc
  %eptr175167 = getelementptr inbounds i64, i64* %cloptr175166, i64 0                ; &cloptr175166[0]
  %f175170 = ptrtoint void(i64,i64)* @lam174261 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175170, i64* %eptr175167                                               ; store fptr
  %arg171864 = ptrtoint i64* %cloptr175166 to i64                                    ; closure cast; i64* -> i64
  %arg171863 = add i64 0, 0                                                          ; quoted ()
  %rva172846 = add i64 0, 0                                                          ; quoted ()
  %rva172845 = call i64 @prim_cons(i64 %retprim171395, i64 %rva172846)               ; call prim_cons
  %rva172844 = call i64 @prim_cons(i64 %arg171863, i64 %rva172845)                   ; call prim_cons
  %cloptr175171 = inttoptr i64 %arg171864 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175172 = getelementptr inbounds i64, i64* %cloptr175171, i64 0               ; &cloptr175171[0]
  %f175174 = load i64, i64* %i0ptr175172, align 8                                    ; load; *i0ptr175172
  %fptr175173 = inttoptr i64 %f175174 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175173(i64 %arg171864, i64 %rva172844)              ; tail call
  ret void
}


define void @lam174261(i64 %env174262, i64 %rvp172843) {
  %envptr175175 = inttoptr i64 %env174262 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175176 = getelementptr inbounds i64, i64* %envptr175175, i64 2              ; &envptr175175[2]
  %fm1$cc = load i64, i64* %envptr175176, align 8                                    ; load; *envptr175176
  %envptr175177 = inttoptr i64 %env174262 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175178 = getelementptr inbounds i64, i64* %envptr175177, i64 1              ; &envptr175177[1]
  %cont171391 = load i64, i64* %envptr175178, align 8                                ; load; *envptr175178
  %_95171394 = call i64 @prim_car(i64 %rvp172843)                                    ; call prim_car
  %rvp172842 = call i64 @prim_cdr(i64 %rvp172843)                                    ; call prim_cdr
  %h8N$_950 = call i64 @prim_car(i64 %rvp172842)                                     ; call prim_car
  %na172838 = call i64 @prim_cdr(i64 %rvp172842)                                     ; call prim_cdr
  %rva172841 = add i64 0, 0                                                          ; quoted ()
  %rva172840 = call i64 @prim_cons(i64 %fm1$cc, i64 %rva172841)                      ; call prim_cons
  %rva172839 = call i64 @prim_cons(i64 %cont171391, i64 %rva172840)                  ; call prim_cons
  %cloptr175179 = inttoptr i64 %fm1$cc to i64*                                       ; closure/env cast; i64 -> i64*
  %i0ptr175180 = getelementptr inbounds i64, i64* %cloptr175179, i64 0               ; &cloptr175179[0]
  %f175182 = load i64, i64* %i0ptr175180, align 8                                    ; load; *i0ptr175180
  %fptr175181 = inttoptr i64 %f175182 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175181(i64 %fm1$cc, i64 %rva172839)                 ; tail call
  ret void
}


define void @lam174250(i64 %env174251, i64 %rvp172868) {
  %envptr175183 = inttoptr i64 %env174251 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175184 = getelementptr inbounds i64, i64* %envptr175183, i64 3              ; &envptr175183[3]
  %yZv$lst = load i64, i64* %envptr175184, align 8                                   ; load; *envptr175184
  %envptr175185 = inttoptr i64 %env174251 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175186 = getelementptr inbounds i64, i64* %envptr175185, i64 2              ; &envptr175185[2]
  %tcC$v = load i64, i64* %envptr175186, align 8                                     ; load; *envptr175186
  %envptr175187 = inttoptr i64 %env174251 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175188 = getelementptr inbounds i64, i64* %envptr175187, i64 1              ; &envptr175187[1]
  %cont171391 = load i64, i64* %envptr175188, align 8                                ; load; *envptr175188
  %_95171392 = call i64 @prim_car(i64 %rvp172868)                                    ; call prim_car
  %rvp172867 = call i64 @prim_cdr(i64 %rvp172868)                                    ; call prim_cdr
  %fm1$cc = call i64 @prim_car(i64 %rvp172867)                                       ; call prim_car
  %na172850 = call i64 @prim_cdr(i64 %rvp172867)                                     ; call prim_cdr
  %arg171868 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171268 = call i64 @prim_vector_45ref(i64 %yZv$lst, i64 %arg171868)               ; call prim_vector_45ref
  %a171269 = call i64 @prim_null_63(i64 %a171268)                                    ; call prim_null_63
  %cmp175189 = icmp eq i64 %a171269, 15                                              ; false?
  br i1 %cmp175189, label %else175191, label %then175190                             ; if

then175190:
  %arg171872 = add i64 0, 0                                                          ; quoted ()
  %arg171871 = call i64 @const_init_false()                                          ; quoted #f
  %rva172853 = add i64 0, 0                                                          ; quoted ()
  %rva172852 = call i64 @prim_cons(i64 %arg171871, i64 %rva172853)                   ; call prim_cons
  %rva172851 = call i64 @prim_cons(i64 %arg171872, i64 %rva172852)                   ; call prim_cons
  %cloptr175192 = inttoptr i64 %cont171391 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175193 = getelementptr inbounds i64, i64* %cloptr175192, i64 0               ; &cloptr175192[0]
  %f175195 = load i64, i64* %i0ptr175193, align 8                                    ; load; *i0ptr175193
  %fptr175194 = inttoptr i64 %f175195 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175194(i64 %cont171391, i64 %rva172851)             ; tail call
  ret void

else175191:
  %arg171874 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171270 = call i64 @prim_vector_45ref(i64 %yZv$lst, i64 %arg171874)               ; call prim_vector_45ref
  %a171271 = call i64 @prim_car(i64 %a171270)                                        ; call prim_car
  %a171272 = call i64 @prim_eqv_63(i64 %a171271, i64 %tcC$v)                         ; call prim_eqv_63
  %cmp175196 = icmp eq i64 %a171272, 15                                              ; false?
  br i1 %cmp175196, label %else175198, label %then175197                             ; if

then175197:
  %arg171879 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171393 = call i64 @prim_vector_45ref(i64 %yZv$lst, i64 %arg171879)         ; call prim_vector_45ref
  %arg171882 = add i64 0, 0                                                          ; quoted ()
  %rva172856 = add i64 0, 0                                                          ; quoted ()
  %rva172855 = call i64 @prim_cons(i64 %retprim171393, i64 %rva172856)               ; call prim_cons
  %rva172854 = call i64 @prim_cons(i64 %arg171882, i64 %rva172855)                   ; call prim_cons
  %cloptr175199 = inttoptr i64 %cont171391 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175200 = getelementptr inbounds i64, i64* %cloptr175199, i64 0               ; &cloptr175199[0]
  %f175202 = load i64, i64* %i0ptr175200, align 8                                    ; load; *i0ptr175200
  %fptr175201 = inttoptr i64 %f175202 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175201(i64 %cont171391, i64 %rva172854)             ; tail call
  ret void

else175198:
  %arg171884 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171273 = call i64 @prim_vector_45ref(i64 %yZv$lst, i64 %arg171884)               ; call prim_vector_45ref
  %a171274 = call i64 @prim_cdr(i64 %a171273)                                        ; call prim_cdr
  %arg171888 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171395 = call i64 @prim_vector_45set_33(i64 %yZv$lst, i64 %arg171888, i64 %a171274); call prim_vector_45set_33
  %cloptr175203 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr175205 = getelementptr inbounds i64, i64* %cloptr175203, i64 1                ; &eptr175205[1]
  %eptr175206 = getelementptr inbounds i64, i64* %cloptr175203, i64 2                ; &eptr175206[2]
  store i64 %cont171391, i64* %eptr175205                                            ; *eptr175205 = %cont171391
  store i64 %fm1$cc, i64* %eptr175206                                                ; *eptr175206 = %fm1$cc
  %eptr175204 = getelementptr inbounds i64, i64* %cloptr175203, i64 0                ; &cloptr175203[0]
  %f175207 = ptrtoint void(i64,i64)* @lam174244 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175207, i64* %eptr175204                                               ; store fptr
  %arg171892 = ptrtoint i64* %cloptr175203 to i64                                    ; closure cast; i64* -> i64
  %arg171891 = add i64 0, 0                                                          ; quoted ()
  %rva172866 = add i64 0, 0                                                          ; quoted ()
  %rva172865 = call i64 @prim_cons(i64 %retprim171395, i64 %rva172866)               ; call prim_cons
  %rva172864 = call i64 @prim_cons(i64 %arg171891, i64 %rva172865)                   ; call prim_cons
  %cloptr175208 = inttoptr i64 %arg171892 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175209 = getelementptr inbounds i64, i64* %cloptr175208, i64 0               ; &cloptr175208[0]
  %f175211 = load i64, i64* %i0ptr175209, align 8                                    ; load; *i0ptr175209
  %fptr175210 = inttoptr i64 %f175211 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175210(i64 %arg171892, i64 %rva172864)              ; tail call
  ret void
}


define void @lam174244(i64 %env174245, i64 %rvp172863) {
  %envptr175212 = inttoptr i64 %env174245 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175213 = getelementptr inbounds i64, i64* %envptr175212, i64 2              ; &envptr175212[2]
  %fm1$cc = load i64, i64* %envptr175213, align 8                                    ; load; *envptr175213
  %envptr175214 = inttoptr i64 %env174245 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175215 = getelementptr inbounds i64, i64* %envptr175214, i64 1              ; &envptr175214[1]
  %cont171391 = load i64, i64* %envptr175215, align 8                                ; load; *envptr175215
  %_95171394 = call i64 @prim_car(i64 %rvp172863)                                    ; call prim_car
  %rvp172862 = call i64 @prim_cdr(i64 %rvp172863)                                    ; call prim_cdr
  %h8N$_950 = call i64 @prim_car(i64 %rvp172862)                                     ; call prim_car
  %na172858 = call i64 @prim_cdr(i64 %rvp172862)                                     ; call prim_cdr
  %rva172861 = add i64 0, 0                                                          ; quoted ()
  %rva172860 = call i64 @prim_cons(i64 %fm1$cc, i64 %rva172861)                      ; call prim_cons
  %rva172859 = call i64 @prim_cons(i64 %cont171391, i64 %rva172860)                  ; call prim_cons
  %cloptr175216 = inttoptr i64 %fm1$cc to i64*                                       ; closure/env cast; i64 -> i64*
  %i0ptr175217 = getelementptr inbounds i64, i64* %cloptr175216, i64 0               ; &cloptr175216[0]
  %f175219 = load i64, i64* %i0ptr175217, align 8                                    ; load; *i0ptr175217
  %fptr175218 = inttoptr i64 %f175219 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175218(i64 %fm1$cc, i64 %rva172859)                 ; tail call
  ret void
}


define void @lam174232(i64 %env174233, i64 %Fot$args171398) {
  %envptr175220 = inttoptr i64 %env174233 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175221 = getelementptr inbounds i64, i64* %envptr175220, i64 1              ; &envptr175220[1]
  %aUW$_37foldl1 = load i64, i64* %envptr175221, align 8                             ; load; *envptr175221
  %cont171397 = call i64 @prim_car(i64 %Fot$args171398)                              ; call prim_car
  %Fot$args = call i64 @prim_cdr(i64 %Fot$args171398)                                ; call prim_cdr
  %a171275 = call i64 @prim_null_63(i64 %Fot$args)                                   ; call prim_null_63
  %cmp175222 = icmp eq i64 %a171275, 15                                              ; false?
  br i1 %cmp175222, label %else175224, label %then175223                             ; if

then175223:
  %arg171900 = add i64 0, 0                                                          ; quoted ()
  %arg171899 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %rva172877 = add i64 0, 0                                                          ; quoted ()
  %rva172876 = call i64 @prim_cons(i64 %arg171899, i64 %rva172877)                   ; call prim_cons
  %rva172875 = call i64 @prim_cons(i64 %arg171900, i64 %rva172876)                   ; call prim_cons
  %cloptr175225 = inttoptr i64 %cont171397 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175226 = getelementptr inbounds i64, i64* %cloptr175225, i64 0               ; &cloptr175225[0]
  %f175228 = load i64, i64* %i0ptr175226, align 8                                    ; load; *i0ptr175226
  %fptr175227 = inttoptr i64 %f175228 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175227(i64 %cont171397, i64 %rva172875)             ; tail call
  ret void

else175224:
  %a171276 = call i64 @prim_cdr(i64 %Fot$args)                                       ; call prim_cdr
  %a171277 = call i64 @prim_null_63(i64 %a171276)                                    ; call prim_null_63
  %cmp175229 = icmp eq i64 %a171277, 15                                              ; false?
  br i1 %cmp175229, label %else175231, label %then175230                             ; if

then175230:
  %retprim171399 = call i64 @prim_car(i64 %Fot$args)                                 ; call prim_car
  %arg171906 = add i64 0, 0                                                          ; quoted ()
  %rva172880 = add i64 0, 0                                                          ; quoted ()
  %rva172879 = call i64 @prim_cons(i64 %retprim171399, i64 %rva172880)               ; call prim_cons
  %rva172878 = call i64 @prim_cons(i64 %arg171906, i64 %rva172879)                   ; call prim_cons
  %cloptr175232 = inttoptr i64 %cont171397 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175233 = getelementptr inbounds i64, i64* %cloptr175232, i64 0               ; &cloptr175232[0]
  %f175235 = load i64, i64* %i0ptr175233, align 8                                    ; load; *i0ptr175233
  %fptr175234 = inttoptr i64 %f175235 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175234(i64 %cont171397, i64 %rva172878)             ; tail call
  ret void

else175231:
  %a171278 = call i64 @prim_car(i64 %Fot$args)                                       ; call prim_car
  %a171279 = call i64 @prim_cdr(i64 %Fot$args)                                       ; call prim_cdr
  %cloptr175236 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr175237 = getelementptr inbounds i64, i64* %cloptr175236, i64 0                ; &cloptr175236[0]
  %f175238 = ptrtoint void(i64,i64)* @lam174230 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175238, i64* %eptr175237                                               ; store fptr
  %arg171912 = ptrtoint i64* %cloptr175236 to i64                                    ; closure cast; i64* -> i64
  %rva172893 = add i64 0, 0                                                          ; quoted ()
  %rva172892 = call i64 @prim_cons(i64 %a171279, i64 %rva172893)                     ; call prim_cons
  %rva172891 = call i64 @prim_cons(i64 %a171278, i64 %rva172892)                     ; call prim_cons
  %rva172890 = call i64 @prim_cons(i64 %arg171912, i64 %rva172891)                   ; call prim_cons
  %rva172889 = call i64 @prim_cons(i64 %cont171397, i64 %rva172890)                  ; call prim_cons
  %cloptr175239 = inttoptr i64 %aUW$_37foldl1 to i64*                                ; closure/env cast; i64 -> i64*
  %i0ptr175240 = getelementptr inbounds i64, i64* %cloptr175239, i64 0               ; &cloptr175239[0]
  %f175242 = load i64, i64* %i0ptr175240, align 8                                    ; load; *i0ptr175240
  %fptr175241 = inttoptr i64 %f175242 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175241(i64 %aUW$_37foldl1, i64 %rva172889)          ; tail call
  ret void
}


define void @lam174230(i64 %env174231, i64 %rvp172888) {
  %cont171400 = call i64 @prim_car(i64 %rvp172888)                                   ; call prim_car
  %rvp172887 = call i64 @prim_cdr(i64 %rvp172888)                                    ; call prim_cdr
  %Z5h$n = call i64 @prim_car(i64 %rvp172887)                                        ; call prim_car
  %rvp172886 = call i64 @prim_cdr(i64 %rvp172887)                                    ; call prim_cdr
  %dKn$v = call i64 @prim_car(i64 %rvp172886)                                        ; call prim_car
  %na172882 = call i64 @prim_cdr(i64 %rvp172886)                                     ; call prim_cdr
  %retprim171401 = call i64 @prim__47(i64 %dKn$v, i64 %Z5h$n)                        ; call prim__47
  %arg171918 = add i64 0, 0                                                          ; quoted ()
  %rva172885 = add i64 0, 0                                                          ; quoted ()
  %rva172884 = call i64 @prim_cons(i64 %retprim171401, i64 %rva172885)               ; call prim_cons
  %rva172883 = call i64 @prim_cons(i64 %arg171918, i64 %rva172884)                   ; call prim_cons
  %cloptr175243 = inttoptr i64 %cont171400 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175244 = getelementptr inbounds i64, i64* %cloptr175243, i64 0               ; &cloptr175243[0]
  %f175246 = load i64, i64* %i0ptr175244, align 8                                    ; load; *i0ptr175244
  %fptr175245 = inttoptr i64 %f175246 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175245(i64 %cont171400, i64 %rva172883)             ; tail call
  ret void
}


define void @lam174220(i64 %env174221, i64 %rvp172900) {
  %cont171402 = call i64 @prim_car(i64 %rvp172900)                                   ; call prim_car
  %rvp172899 = call i64 @prim_cdr(i64 %rvp172900)                                    ; call prim_cdr
  %Gnx$x = call i64 @prim_car(i64 %rvp172899)                                        ; call prim_car
  %na172895 = call i64 @prim_cdr(i64 %rvp172899)                                     ; call prim_cdr
  %retprim171403 = call i64 @prim_car(i64 %Gnx$x)                                    ; call prim_car
  %arg171922 = add i64 0, 0                                                          ; quoted ()
  %rva172898 = add i64 0, 0                                                          ; quoted ()
  %rva172897 = call i64 @prim_cons(i64 %retprim171403, i64 %rva172898)               ; call prim_cons
  %rva172896 = call i64 @prim_cons(i64 %arg171922, i64 %rva172897)                   ; call prim_cons
  %cloptr175247 = inttoptr i64 %cont171402 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175248 = getelementptr inbounds i64, i64* %cloptr175247, i64 0               ; &cloptr175247[0]
  %f175250 = load i64, i64* %i0ptr175248, align 8                                    ; load; *i0ptr175248
  %fptr175249 = inttoptr i64 %f175250 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175249(i64 %cont171402, i64 %rva172896)             ; tail call
  ret void
}


define void @lam174216(i64 %env174217, i64 %rvp172907) {
  %cont171404 = call i64 @prim_car(i64 %rvp172907)                                   ; call prim_car
  %rvp172906 = call i64 @prim_cdr(i64 %rvp172907)                                    ; call prim_cdr
  %Isf$x = call i64 @prim_car(i64 %rvp172906)                                        ; call prim_car
  %na172902 = call i64 @prim_cdr(i64 %rvp172906)                                     ; call prim_cdr
  %a171280 = call i64 @prim_cdr(i64 %Isf$x)                                          ; call prim_cdr
  %retprim171405 = call i64 @prim_car(i64 %a171280)                                  ; call prim_car
  %arg171927 = add i64 0, 0                                                          ; quoted ()
  %rva172905 = add i64 0, 0                                                          ; quoted ()
  %rva172904 = call i64 @prim_cons(i64 %retprim171405, i64 %rva172905)               ; call prim_cons
  %rva172903 = call i64 @prim_cons(i64 %arg171927, i64 %rva172904)                   ; call prim_cons
  %cloptr175251 = inttoptr i64 %cont171404 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175252 = getelementptr inbounds i64, i64* %cloptr175251, i64 0               ; &cloptr175251[0]
  %f175254 = load i64, i64* %i0ptr175252, align 8                                    ; load; *i0ptr175252
  %fptr175253 = inttoptr i64 %f175254 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175253(i64 %cont171404, i64 %rva172903)             ; tail call
  ret void
}


define void @lam174212(i64 %env174213, i64 %rvp172914) {
  %cont171406 = call i64 @prim_car(i64 %rvp172914)                                   ; call prim_car
  %rvp172913 = call i64 @prim_cdr(i64 %rvp172914)                                    ; call prim_cdr
  %r0d$x = call i64 @prim_car(i64 %rvp172913)                                        ; call prim_car
  %na172909 = call i64 @prim_cdr(i64 %rvp172913)                                     ; call prim_cdr
  %a171281 = call i64 @prim_cdr(i64 %r0d$x)                                          ; call prim_cdr
  %a171282 = call i64 @prim_cdr(i64 %a171281)                                        ; call prim_cdr
  %retprim171407 = call i64 @prim_car(i64 %a171282)                                  ; call prim_car
  %arg171933 = add i64 0, 0                                                          ; quoted ()
  %rva172912 = add i64 0, 0                                                          ; quoted ()
  %rva172911 = call i64 @prim_cons(i64 %retprim171407, i64 %rva172912)               ; call prim_cons
  %rva172910 = call i64 @prim_cons(i64 %arg171933, i64 %rva172911)                   ; call prim_cons
  %cloptr175255 = inttoptr i64 %cont171406 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175256 = getelementptr inbounds i64, i64* %cloptr175255, i64 0               ; &cloptr175255[0]
  %f175258 = load i64, i64* %i0ptr175256, align 8                                    ; load; *i0ptr175256
  %fptr175257 = inttoptr i64 %f175258 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175257(i64 %cont171406, i64 %rva172910)             ; tail call
  ret void
}


define void @lam174208(i64 %env174209, i64 %rvp172921) {
  %cont171408 = call i64 @prim_car(i64 %rvp172921)                                   ; call prim_car
  %rvp172920 = call i64 @prim_cdr(i64 %rvp172921)                                    ; call prim_cdr
  %U9d$x = call i64 @prim_car(i64 %rvp172920)                                        ; call prim_car
  %na172916 = call i64 @prim_cdr(i64 %rvp172920)                                     ; call prim_cdr
  %a171283 = call i64 @prim_cdr(i64 %U9d$x)                                          ; call prim_cdr
  %a171284 = call i64 @prim_cdr(i64 %a171283)                                        ; call prim_cdr
  %a171285 = call i64 @prim_cdr(i64 %a171284)                                        ; call prim_cdr
  %retprim171409 = call i64 @prim_car(i64 %a171285)                                  ; call prim_car
  %arg171940 = add i64 0, 0                                                          ; quoted ()
  %rva172919 = add i64 0, 0                                                          ; quoted ()
  %rva172918 = call i64 @prim_cons(i64 %retprim171409, i64 %rva172919)               ; call prim_cons
  %rva172917 = call i64 @prim_cons(i64 %arg171940, i64 %rva172918)                   ; call prim_cons
  %cloptr175259 = inttoptr i64 %cont171408 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175260 = getelementptr inbounds i64, i64* %cloptr175259, i64 0               ; &cloptr175259[0]
  %f175262 = load i64, i64* %i0ptr175260, align 8                                    ; load; *i0ptr175260
  %fptr175261 = inttoptr i64 %f175262 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175261(i64 %cont171408, i64 %rva172917)             ; tail call
  ret void
}


define void @lam174204(i64 %env174205, i64 %rvp172931) {
  %cont171410 = call i64 @prim_car(i64 %rvp172931)                                   ; call prim_car
  %rvp172930 = call i64 @prim_cdr(i64 %rvp172931)                                    ; call prim_cdr
  %Tck$p = call i64 @prim_car(i64 %rvp172930)                                        ; call prim_car
  %na172923 = call i64 @prim_cdr(i64 %rvp172930)                                     ; call prim_cdr
  %a171286 = call i64 @prim_cons_63(i64 %Tck$p)                                      ; call prim_cons_63
  %cmp175263 = icmp eq i64 %a171286, 15                                              ; false?
  br i1 %cmp175263, label %else175265, label %then175264                             ; if

then175264:
  %a171287 = call i64 @prim_car(i64 %Tck$p)                                          ; call prim_car
  %arg171944 = call i64 @const_init_symbol(i8* getelementptr inbounds ([10 x i8], [10 x i8]* @sym175266, i32 0, i32 0)); quoted string
  %retprim171411 = call i64 @prim_eq_63(i64 %a171287, i64 %arg171944)                ; call prim_eq_63
  %arg171947 = add i64 0, 0                                                          ; quoted ()
  %rva172926 = add i64 0, 0                                                          ; quoted ()
  %rva172925 = call i64 @prim_cons(i64 %retprim171411, i64 %rva172926)               ; call prim_cons
  %rva172924 = call i64 @prim_cons(i64 %arg171947, i64 %rva172925)                   ; call prim_cons
  %cloptr175267 = inttoptr i64 %cont171410 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175268 = getelementptr inbounds i64, i64* %cloptr175267, i64 0               ; &cloptr175267[0]
  %f175270 = load i64, i64* %i0ptr175268, align 8                                    ; load; *i0ptr175268
  %fptr175269 = inttoptr i64 %f175270 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175269(i64 %cont171410, i64 %rva172924)             ; tail call
  ret void

else175265:
  %arg171950 = add i64 0, 0                                                          ; quoted ()
  %arg171949 = call i64 @const_init_false()                                          ; quoted #f
  %rva172929 = add i64 0, 0                                                          ; quoted ()
  %rva172928 = call i64 @prim_cons(i64 %arg171949, i64 %rva172929)                   ; call prim_cons
  %rva172927 = call i64 @prim_cons(i64 %arg171950, i64 %rva172928)                   ; call prim_cons
  %cloptr175271 = inttoptr i64 %cont171410 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175272 = getelementptr inbounds i64, i64* %cloptr175271, i64 0               ; &cloptr175271[0]
  %f175274 = load i64, i64* %i0ptr175272, align 8                                    ; load; *i0ptr175272
  %fptr175273 = inttoptr i64 %f175274 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175273(i64 %cont171410, i64 %rva172927)             ; tail call
  ret void
}


define void @lam174196(i64 %env174197, i64 %ReG$lst171494) {
  %cont171493 = call i64 @prim_car(i64 %ReG$lst171494)                               ; call prim_car
  %ReG$lst = call i64 @prim_cdr(i64 %ReG$lst171494)                                  ; call prim_cdr
  %arg171957 = add i64 0, 0                                                          ; quoted ()
  %rva172934 = add i64 0, 0                                                          ; quoted ()
  %rva172933 = call i64 @prim_cons(i64 %ReG$lst, i64 %rva172934)                     ; call prim_cons
  %rva172932 = call i64 @prim_cons(i64 %arg171957, i64 %rva172933)                   ; call prim_cons
  %cloptr175275 = inttoptr i64 %cont171493 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175276 = getelementptr inbounds i64, i64* %cloptr175275, i64 0               ; &cloptr175275[0]
  %f175278 = load i64, i64* %i0ptr175276, align 8                                    ; load; *i0ptr175276
  %fptr175277 = inttoptr i64 %f175278 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175277(i64 %cont171493, i64 %rva172932)             ; tail call
  ret void
}


define void @lam174192(i64 %env174193, i64 %rvp173323) {
  %envptr175279 = inttoptr i64 %env174193 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175280 = getelementptr inbounds i64, i64* %envptr175279, i64 3              ; &envptr175279[3]
  %Re6$_37drop = load i64, i64* %envptr175280, align 8                               ; load; *envptr175280
  %envptr175281 = inttoptr i64 %env174193 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175282 = getelementptr inbounds i64, i64* %envptr175281, i64 2              ; &envptr175281[2]
  %G9j$_37_62 = load i64, i64* %envptr175282, align 8                                ; load; *envptr175282
  %envptr175283 = inttoptr i64 %env174193 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175284 = getelementptr inbounds i64, i64* %envptr175283, i64 1              ; &envptr175283[1]
  %SU3$_37length = load i64, i64* %envptr175284, align 8                             ; load; *envptr175284
  %_95171412 = call i64 @prim_car(i64 %rvp173323)                                    ; call prim_car
  %rvp173322 = call i64 @prim_cdr(i64 %rvp173323)                                    ; call prim_cdr
  %zNC$_37raise_45handler = call i64 @prim_car(i64 %rvp173322)                       ; call prim_car
  %na172936 = call i64 @prim_cdr(i64 %rvp173322)                                     ; call prim_cdr
  %cloptr175285 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr175286 = getelementptr inbounds i64, i64* %cloptr175285, i64 0                ; &cloptr175285[0]
  %f175287 = ptrtoint void(i64,i64)* @lam174190 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175287, i64* %eptr175286                                               ; store fptr
  %arg171960 = ptrtoint i64* %cloptr175285 to i64                                    ; closure cast; i64* -> i64
  %cloptr175288 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175290 = getelementptr inbounds i64, i64* %cloptr175288, i64 1                ; &eptr175290[1]
  %eptr175291 = getelementptr inbounds i64, i64* %cloptr175288, i64 2                ; &eptr175291[2]
  %eptr175292 = getelementptr inbounds i64, i64* %cloptr175288, i64 3                ; &eptr175292[3]
  store i64 %SU3$_37length, i64* %eptr175290                                         ; *eptr175290 = %SU3$_37length
  store i64 %G9j$_37_62, i64* %eptr175291                                            ; *eptr175291 = %G9j$_37_62
  store i64 %Re6$_37drop, i64* %eptr175292                                           ; *eptr175292 = %Re6$_37drop
  %eptr175289 = getelementptr inbounds i64, i64* %cloptr175288, i64 0                ; &cloptr175288[0]
  %f175293 = ptrtoint void(i64,i64)* @lam174186 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175293, i64* %eptr175289                                               ; store fptr
  %arg171959 = ptrtoint i64* %cloptr175288 to i64                                    ; closure cast; i64* -> i64
  %rva173321 = add i64 0, 0                                                          ; quoted ()
  %rva173320 = call i64 @prim_cons(i64 %arg171959, i64 %rva173321)                   ; call prim_cons
  %cloptr175294 = inttoptr i64 %arg171960 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175295 = getelementptr inbounds i64, i64* %cloptr175294, i64 0               ; &cloptr175294[0]
  %f175297 = load i64, i64* %i0ptr175295, align 8                                    ; load; *i0ptr175295
  %fptr175296 = inttoptr i64 %f175297 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175296(i64 %arg171960, i64 %rva173320)              ; tail call
  ret void
}


define void @lam174190(i64 %env174191, i64 %oJD$lst171492) {
  %cont171491 = call i64 @prim_car(i64 %oJD$lst171492)                               ; call prim_car
  %oJD$lst = call i64 @prim_cdr(i64 %oJD$lst171492)                                  ; call prim_cdr
  %arg171964 = add i64 0, 0                                                          ; quoted ()
  %rva172939 = add i64 0, 0                                                          ; quoted ()
  %rva172938 = call i64 @prim_cons(i64 %oJD$lst, i64 %rva172939)                     ; call prim_cons
  %rva172937 = call i64 @prim_cons(i64 %arg171964, i64 %rva172938)                   ; call prim_cons
  %cloptr175298 = inttoptr i64 %cont171491 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175299 = getelementptr inbounds i64, i64* %cloptr175298, i64 0               ; &cloptr175298[0]
  %f175301 = load i64, i64* %i0ptr175299, align 8                                    ; load; *i0ptr175299
  %fptr175300 = inttoptr i64 %f175301 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175300(i64 %cont171491, i64 %rva172937)             ; tail call
  ret void
}


define void @lam174186(i64 %env174187, i64 %rvp173319) {
  %envptr175302 = inttoptr i64 %env174187 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175303 = getelementptr inbounds i64, i64* %envptr175302, i64 3              ; &envptr175302[3]
  %Re6$_37drop = load i64, i64* %envptr175303, align 8                               ; load; *envptr175303
  %envptr175304 = inttoptr i64 %env174187 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175305 = getelementptr inbounds i64, i64* %envptr175304, i64 2              ; &envptr175304[2]
  %G9j$_37_62 = load i64, i64* %envptr175305, align 8                                ; load; *envptr175305
  %envptr175306 = inttoptr i64 %env174187 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175307 = getelementptr inbounds i64, i64* %envptr175306, i64 1              ; &envptr175306[1]
  %SU3$_37length = load i64, i64* %envptr175307, align 8                             ; load; *envptr175307
  %_95171489 = call i64 @prim_car(i64 %rvp173319)                                    ; call prim_car
  %rvp173318 = call i64 @prim_cdr(i64 %rvp173319)                                    ; call prim_cdr
  %a171288 = call i64 @prim_car(i64 %rvp173318)                                      ; call prim_car
  %na172941 = call i64 @prim_cdr(i64 %rvp173318)                                     ; call prim_cdr
  %arg171967 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %retprim171490 = call i64 @prim_make_45vector(i64 %arg171967, i64 %a171288)        ; call prim_make_45vector
  %cloptr175308 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175310 = getelementptr inbounds i64, i64* %cloptr175308, i64 1                ; &eptr175310[1]
  %eptr175311 = getelementptr inbounds i64, i64* %cloptr175308, i64 2                ; &eptr175311[2]
  %eptr175312 = getelementptr inbounds i64, i64* %cloptr175308, i64 3                ; &eptr175312[3]
  store i64 %SU3$_37length, i64* %eptr175310                                         ; *eptr175310 = %SU3$_37length
  store i64 %G9j$_37_62, i64* %eptr175311                                            ; *eptr175311 = %G9j$_37_62
  store i64 %Re6$_37drop, i64* %eptr175312                                           ; *eptr175312 = %Re6$_37drop
  %eptr175309 = getelementptr inbounds i64, i64* %cloptr175308, i64 0                ; &cloptr175308[0]
  %f175313 = ptrtoint void(i64,i64)* @lam174183 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175313, i64* %eptr175309                                               ; store fptr
  %arg171970 = ptrtoint i64* %cloptr175308 to i64                                    ; closure cast; i64* -> i64
  %arg171969 = add i64 0, 0                                                          ; quoted ()
  %rva173317 = add i64 0, 0                                                          ; quoted ()
  %rva173316 = call i64 @prim_cons(i64 %retprim171490, i64 %rva173317)               ; call prim_cons
  %rva173315 = call i64 @prim_cons(i64 %arg171969, i64 %rva173316)                   ; call prim_cons
  %cloptr175314 = inttoptr i64 %arg171970 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175315 = getelementptr inbounds i64, i64* %cloptr175314, i64 0               ; &cloptr175314[0]
  %f175317 = load i64, i64* %i0ptr175315, align 8                                    ; load; *i0ptr175315
  %fptr175316 = inttoptr i64 %f175317 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175316(i64 %arg171970, i64 %rva173315)              ; tail call
  ret void
}


define void @lam174183(i64 %env174184, i64 %rvp173314) {
  %envptr175318 = inttoptr i64 %env174184 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175319 = getelementptr inbounds i64, i64* %envptr175318, i64 3              ; &envptr175318[3]
  %Re6$_37drop = load i64, i64* %envptr175319, align 8                               ; load; *envptr175319
  %envptr175320 = inttoptr i64 %env174184 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175321 = getelementptr inbounds i64, i64* %envptr175320, i64 2              ; &envptr175320[2]
  %G9j$_37_62 = load i64, i64* %envptr175321, align 8                                ; load; *envptr175321
  %envptr175322 = inttoptr i64 %env174184 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175323 = getelementptr inbounds i64, i64* %envptr175322, i64 1              ; &envptr175322[1]
  %SU3$_37length = load i64, i64* %envptr175323, align 8                             ; load; *envptr175323
  %_95171413 = call i64 @prim_car(i64 %rvp173314)                                    ; call prim_car
  %rvp173313 = call i64 @prim_cdr(i64 %rvp173314)                                    ; call prim_cdr
  %c3d$_37wind_45stack = call i64 @prim_car(i64 %rvp173313)                          ; call prim_car
  %na172943 = call i64 @prim_cdr(i64 %rvp173313)                                     ; call prim_cdr
  %cloptr175324 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175326 = getelementptr inbounds i64, i64* %cloptr175324, i64 1                ; &eptr175326[1]
  %eptr175327 = getelementptr inbounds i64, i64* %cloptr175324, i64 2                ; &eptr175327[2]
  %eptr175328 = getelementptr inbounds i64, i64* %cloptr175324, i64 3                ; &eptr175328[3]
  store i64 %SU3$_37length, i64* %eptr175326                                         ; *eptr175326 = %SU3$_37length
  store i64 %G9j$_37_62, i64* %eptr175327                                            ; *eptr175327 = %G9j$_37_62
  store i64 %Re6$_37drop, i64* %eptr175328                                           ; *eptr175328 = %Re6$_37drop
  %eptr175325 = getelementptr inbounds i64, i64* %cloptr175324, i64 0                ; &cloptr175324[0]
  %f175329 = ptrtoint void(i64,i64)* @lam174181 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175329, i64* %eptr175325                                               ; store fptr
  %Hbp$_37common_45tail = ptrtoint i64* %cloptr175324 to i64                         ; closure cast; i64* -> i64
  %cloptr175330 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr175332 = getelementptr inbounds i64, i64* %cloptr175330, i64 1                ; &eptr175332[1]
  %eptr175333 = getelementptr inbounds i64, i64* %cloptr175330, i64 2                ; &eptr175333[2]
  store i64 %c3d$_37wind_45stack, i64* %eptr175332                                   ; *eptr175332 = %c3d$_37wind_45stack
  store i64 %Hbp$_37common_45tail, i64* %eptr175333                                  ; *eptr175333 = %Hbp$_37common_45tail
  %eptr175331 = getelementptr inbounds i64, i64* %cloptr175330, i64 0                ; &cloptr175330[0]
  %f175334 = ptrtoint void(i64,i64)* @lam174119 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175334, i64* %eptr175331                                               ; store fptr
  %snX$_37do_45wind = ptrtoint i64* %cloptr175330 to i64                             ; closure cast; i64* -> i64
  %cloptr175335 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr175336 = getelementptr inbounds i64, i64* %cloptr175335, i64 0                ; &cloptr175335[0]
  %f175337 = ptrtoint void(i64,i64)* @lam174052 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175337, i64* %eptr175336                                               ; store fptr
  %arg172156 = ptrtoint i64* %cloptr175335 to i64                                    ; closure cast; i64* -> i64
  %cloptr175338 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr175339 = getelementptr inbounds i64, i64* %cloptr175338, i64 0                ; &cloptr175338[0]
  %f175340 = ptrtoint void(i64,i64)* @lam174048 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175340, i64* %eptr175339                                               ; store fptr
  %arg172155 = ptrtoint i64* %cloptr175338 to i64                                    ; closure cast; i64* -> i64
  %rva173312 = add i64 0, 0                                                          ; quoted ()
  %rva173311 = call i64 @prim_cons(i64 %arg172155, i64 %rva173312)                   ; call prim_cons
  %cloptr175341 = inttoptr i64 %arg172156 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175342 = getelementptr inbounds i64, i64* %cloptr175341, i64 0               ; &cloptr175341[0]
  %f175344 = load i64, i64* %i0ptr175342, align 8                                    ; load; *i0ptr175342
  %fptr175343 = inttoptr i64 %f175344 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175343(i64 %arg172156, i64 %rva173311)              ; tail call
  ret void
}


define void @lam174181(i64 %env174182, i64 %rvp173075) {
  %envptr175345 = inttoptr i64 %env174182 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175346 = getelementptr inbounds i64, i64* %envptr175345, i64 3              ; &envptr175345[3]
  %Re6$_37drop = load i64, i64* %envptr175346, align 8                               ; load; *envptr175346
  %envptr175347 = inttoptr i64 %env174182 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175348 = getelementptr inbounds i64, i64* %envptr175347, i64 2              ; &envptr175347[2]
  %G9j$_37_62 = load i64, i64* %envptr175348, align 8                                ; load; *envptr175348
  %envptr175349 = inttoptr i64 %env174182 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175350 = getelementptr inbounds i64, i64* %envptr175349, i64 1              ; &envptr175349[1]
  %SU3$_37length = load i64, i64* %envptr175350, align 8                             ; load; *envptr175350
  %cont171414 = call i64 @prim_car(i64 %rvp173075)                                   ; call prim_car
  %rvp173074 = call i64 @prim_cdr(i64 %rvp173075)                                    ; call prim_cdr
  %Le8$x = call i64 @prim_car(i64 %rvp173074)                                        ; call prim_car
  %rvp173073 = call i64 @prim_cdr(i64 %rvp173074)                                    ; call prim_cdr
  %Pzs$y = call i64 @prim_car(i64 %rvp173073)                                        ; call prim_car
  %na172945 = call i64 @prim_cdr(i64 %rvp173073)                                     ; call prim_cdr
  %cloptr175351 = call i64* @alloc(i64 56)                                           ; malloc
  %eptr175353 = getelementptr inbounds i64, i64* %cloptr175351, i64 1                ; &eptr175353[1]
  %eptr175354 = getelementptr inbounds i64, i64* %cloptr175351, i64 2                ; &eptr175354[2]
  %eptr175355 = getelementptr inbounds i64, i64* %cloptr175351, i64 3                ; &eptr175355[3]
  %eptr175356 = getelementptr inbounds i64, i64* %cloptr175351, i64 4                ; &eptr175356[4]
  %eptr175357 = getelementptr inbounds i64, i64* %cloptr175351, i64 5                ; &eptr175357[5]
  %eptr175358 = getelementptr inbounds i64, i64* %cloptr175351, i64 6                ; &eptr175358[6]
  store i64 %Pzs$y, i64* %eptr175353                                                 ; *eptr175353 = %Pzs$y
  store i64 %SU3$_37length, i64* %eptr175354                                         ; *eptr175354 = %SU3$_37length
  store i64 %cont171414, i64* %eptr175355                                            ; *eptr175355 = %cont171414
  store i64 %Le8$x, i64* %eptr175356                                                 ; *eptr175356 = %Le8$x
  store i64 %G9j$_37_62, i64* %eptr175357                                            ; *eptr175357 = %G9j$_37_62
  store i64 %Re6$_37drop, i64* %eptr175358                                           ; *eptr175358 = %Re6$_37drop
  %eptr175352 = getelementptr inbounds i64, i64* %cloptr175351, i64 0                ; &cloptr175351[0]
  %f175359 = ptrtoint void(i64,i64)* @lam174179 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175359, i64* %eptr175352                                               ; store fptr
  %arg171972 = ptrtoint i64* %cloptr175351 to i64                                    ; closure cast; i64* -> i64
  %rva173072 = add i64 0, 0                                                          ; quoted ()
  %rva173071 = call i64 @prim_cons(i64 %Le8$x, i64 %rva173072)                       ; call prim_cons
  %rva173070 = call i64 @prim_cons(i64 %arg171972, i64 %rva173071)                   ; call prim_cons
  %cloptr175360 = inttoptr i64 %SU3$_37length to i64*                                ; closure/env cast; i64 -> i64*
  %i0ptr175361 = getelementptr inbounds i64, i64* %cloptr175360, i64 0               ; &cloptr175360[0]
  %f175363 = load i64, i64* %i0ptr175361, align 8                                    ; load; *i0ptr175361
  %fptr175362 = inttoptr i64 %f175363 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175362(i64 %SU3$_37length, i64 %rva173070)          ; tail call
  ret void
}


define void @lam174179(i64 %env174180, i64 %rvp173069) {
  %envptr175364 = inttoptr i64 %env174180 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175365 = getelementptr inbounds i64, i64* %envptr175364, i64 6              ; &envptr175364[6]
  %Re6$_37drop = load i64, i64* %envptr175365, align 8                               ; load; *envptr175365
  %envptr175366 = inttoptr i64 %env174180 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175367 = getelementptr inbounds i64, i64* %envptr175366, i64 5              ; &envptr175366[5]
  %G9j$_37_62 = load i64, i64* %envptr175367, align 8                                ; load; *envptr175367
  %envptr175368 = inttoptr i64 %env174180 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175369 = getelementptr inbounds i64, i64* %envptr175368, i64 4              ; &envptr175368[4]
  %Le8$x = load i64, i64* %envptr175369, align 8                                     ; load; *envptr175369
  %envptr175370 = inttoptr i64 %env174180 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175371 = getelementptr inbounds i64, i64* %envptr175370, i64 3              ; &envptr175370[3]
  %cont171414 = load i64, i64* %envptr175371, align 8                                ; load; *envptr175371
  %envptr175372 = inttoptr i64 %env174180 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175373 = getelementptr inbounds i64, i64* %envptr175372, i64 2              ; &envptr175372[2]
  %SU3$_37length = load i64, i64* %envptr175373, align 8                             ; load; *envptr175373
  %envptr175374 = inttoptr i64 %env174180 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175375 = getelementptr inbounds i64, i64* %envptr175374, i64 1              ; &envptr175374[1]
  %Pzs$y = load i64, i64* %envptr175375, align 8                                     ; load; *envptr175375
  %_95171415 = call i64 @prim_car(i64 %rvp173069)                                    ; call prim_car
  %rvp173068 = call i64 @prim_cdr(i64 %rvp173069)                                    ; call prim_cdr
  %OGB$lx = call i64 @prim_car(i64 %rvp173068)                                       ; call prim_car
  %na172947 = call i64 @prim_cdr(i64 %rvp173068)                                     ; call prim_cdr
  %cloptr175376 = call i64* @alloc(i64 56)                                           ; malloc
  %eptr175378 = getelementptr inbounds i64, i64* %cloptr175376, i64 1                ; &eptr175378[1]
  %eptr175379 = getelementptr inbounds i64, i64* %cloptr175376, i64 2                ; &eptr175379[2]
  %eptr175380 = getelementptr inbounds i64, i64* %cloptr175376, i64 3                ; &eptr175380[3]
  %eptr175381 = getelementptr inbounds i64, i64* %cloptr175376, i64 4                ; &eptr175381[4]
  %eptr175382 = getelementptr inbounds i64, i64* %cloptr175376, i64 5                ; &eptr175382[5]
  %eptr175383 = getelementptr inbounds i64, i64* %cloptr175376, i64 6                ; &eptr175383[6]
  store i64 %Pzs$y, i64* %eptr175378                                                 ; *eptr175378 = %Pzs$y
  store i64 %cont171414, i64* %eptr175379                                            ; *eptr175379 = %cont171414
  store i64 %OGB$lx, i64* %eptr175380                                                ; *eptr175380 = %OGB$lx
  store i64 %Le8$x, i64* %eptr175381                                                 ; *eptr175381 = %Le8$x
  store i64 %G9j$_37_62, i64* %eptr175382                                            ; *eptr175382 = %G9j$_37_62
  store i64 %Re6$_37drop, i64* %eptr175383                                           ; *eptr175383 = %Re6$_37drop
  %eptr175377 = getelementptr inbounds i64, i64* %cloptr175376, i64 0                ; &cloptr175376[0]
  %f175384 = ptrtoint void(i64,i64)* @lam174177 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175384, i64* %eptr175377                                               ; store fptr
  %arg171975 = ptrtoint i64* %cloptr175376 to i64                                    ; closure cast; i64* -> i64
  %rva173067 = add i64 0, 0                                                          ; quoted ()
  %rva173066 = call i64 @prim_cons(i64 %Pzs$y, i64 %rva173067)                       ; call prim_cons
  %rva173065 = call i64 @prim_cons(i64 %arg171975, i64 %rva173066)                   ; call prim_cons
  %cloptr175385 = inttoptr i64 %SU3$_37length to i64*                                ; closure/env cast; i64 -> i64*
  %i0ptr175386 = getelementptr inbounds i64, i64* %cloptr175385, i64 0               ; &cloptr175385[0]
  %f175388 = load i64, i64* %i0ptr175386, align 8                                    ; load; *i0ptr175386
  %fptr175387 = inttoptr i64 %f175388 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175387(i64 %SU3$_37length, i64 %rva173065)          ; tail call
  ret void
}


define void @lam174177(i64 %env174178, i64 %rvp173064) {
  %envptr175389 = inttoptr i64 %env174178 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175390 = getelementptr inbounds i64, i64* %envptr175389, i64 6              ; &envptr175389[6]
  %Re6$_37drop = load i64, i64* %envptr175390, align 8                               ; load; *envptr175390
  %envptr175391 = inttoptr i64 %env174178 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175392 = getelementptr inbounds i64, i64* %envptr175391, i64 5              ; &envptr175391[5]
  %G9j$_37_62 = load i64, i64* %envptr175392, align 8                                ; load; *envptr175392
  %envptr175393 = inttoptr i64 %env174178 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175394 = getelementptr inbounds i64, i64* %envptr175393, i64 4              ; &envptr175393[4]
  %Le8$x = load i64, i64* %envptr175394, align 8                                     ; load; *envptr175394
  %envptr175395 = inttoptr i64 %env174178 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175396 = getelementptr inbounds i64, i64* %envptr175395, i64 3              ; &envptr175395[3]
  %OGB$lx = load i64, i64* %envptr175396, align 8                                    ; load; *envptr175396
  %envptr175397 = inttoptr i64 %env174178 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175398 = getelementptr inbounds i64, i64* %envptr175397, i64 2              ; &envptr175397[2]
  %cont171414 = load i64, i64* %envptr175398, align 8                                ; load; *envptr175398
  %envptr175399 = inttoptr i64 %env174178 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175400 = getelementptr inbounds i64, i64* %envptr175399, i64 1              ; &envptr175399[1]
  %Pzs$y = load i64, i64* %envptr175400, align 8                                     ; load; *envptr175400
  %_95171416 = call i64 @prim_car(i64 %rvp173064)                                    ; call prim_car
  %rvp173063 = call i64 @prim_cdr(i64 %rvp173064)                                    ; call prim_cdr
  %ap2$ly = call i64 @prim_car(i64 %rvp173063)                                       ; call prim_car
  %na172949 = call i64 @prim_cdr(i64 %rvp173063)                                     ; call prim_cdr
  %cloptr175401 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr175402 = getelementptr inbounds i64, i64* %cloptr175401, i64 0                ; &cloptr175401[0]
  %f175403 = ptrtoint void(i64,i64)* @lam174175 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175403, i64* %eptr175402                                               ; store fptr
  %arg171978 = ptrtoint i64* %cloptr175401 to i64                                    ; closure cast; i64* -> i64
  %cloptr175404 = call i64* @alloc(i64 64)                                           ; malloc
  %eptr175406 = getelementptr inbounds i64, i64* %cloptr175404, i64 1                ; &eptr175406[1]
  %eptr175407 = getelementptr inbounds i64, i64* %cloptr175404, i64 2                ; &eptr175407[2]
  %eptr175408 = getelementptr inbounds i64, i64* %cloptr175404, i64 3                ; &eptr175408[3]
  %eptr175409 = getelementptr inbounds i64, i64* %cloptr175404, i64 4                ; &eptr175409[4]
  %eptr175410 = getelementptr inbounds i64, i64* %cloptr175404, i64 5                ; &eptr175410[5]
  %eptr175411 = getelementptr inbounds i64, i64* %cloptr175404, i64 6                ; &eptr175411[6]
  %eptr175412 = getelementptr inbounds i64, i64* %cloptr175404, i64 7                ; &eptr175412[7]
  store i64 %Pzs$y, i64* %eptr175406                                                 ; *eptr175406 = %Pzs$y
  store i64 %ap2$ly, i64* %eptr175407                                                ; *eptr175407 = %ap2$ly
  store i64 %cont171414, i64* %eptr175408                                            ; *eptr175408 = %cont171414
  store i64 %OGB$lx, i64* %eptr175409                                                ; *eptr175409 = %OGB$lx
  store i64 %Le8$x, i64* %eptr175410                                                 ; *eptr175410 = %Le8$x
  store i64 %G9j$_37_62, i64* %eptr175411                                            ; *eptr175411 = %G9j$_37_62
  store i64 %Re6$_37drop, i64* %eptr175412                                           ; *eptr175412 = %Re6$_37drop
  %eptr175405 = getelementptr inbounds i64, i64* %cloptr175404, i64 0                ; &cloptr175404[0]
  %f175413 = ptrtoint void(i64,i64)* @lam174171 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175413, i64* %eptr175405                                               ; store fptr
  %arg171977 = ptrtoint i64* %cloptr175404 to i64                                    ; closure cast; i64* -> i64
  %rva173062 = add i64 0, 0                                                          ; quoted ()
  %rva173061 = call i64 @prim_cons(i64 %arg171977, i64 %rva173062)                   ; call prim_cons
  %cloptr175414 = inttoptr i64 %arg171978 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175415 = getelementptr inbounds i64, i64* %cloptr175414, i64 0               ; &cloptr175414[0]
  %f175417 = load i64, i64* %i0ptr175415, align 8                                    ; load; *i0ptr175415
  %fptr175416 = inttoptr i64 %f175417 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175416(i64 %arg171978, i64 %rva173061)              ; tail call
  ret void
}


define void @lam174175(i64 %env174176, i64 %BoR$lst171426) {
  %cont171425 = call i64 @prim_car(i64 %BoR$lst171426)                               ; call prim_car
  %BoR$lst = call i64 @prim_cdr(i64 %BoR$lst171426)                                  ; call prim_cdr
  %arg171982 = add i64 0, 0                                                          ; quoted ()
  %rva172952 = add i64 0, 0                                                          ; quoted ()
  %rva172951 = call i64 @prim_cons(i64 %BoR$lst, i64 %rva172952)                     ; call prim_cons
  %rva172950 = call i64 @prim_cons(i64 %arg171982, i64 %rva172951)                   ; call prim_cons
  %cloptr175418 = inttoptr i64 %cont171425 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175419 = getelementptr inbounds i64, i64* %cloptr175418, i64 0               ; &cloptr175418[0]
  %f175421 = load i64, i64* %i0ptr175419, align 8                                    ; load; *i0ptr175419
  %fptr175420 = inttoptr i64 %f175421 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175420(i64 %cont171425, i64 %rva172950)             ; tail call
  ret void
}


define void @lam174171(i64 %env174172, i64 %rvp173060) {
  %envptr175422 = inttoptr i64 %env174172 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175423 = getelementptr inbounds i64, i64* %envptr175422, i64 7              ; &envptr175422[7]
  %Re6$_37drop = load i64, i64* %envptr175423, align 8                               ; load; *envptr175423
  %envptr175424 = inttoptr i64 %env174172 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175425 = getelementptr inbounds i64, i64* %envptr175424, i64 6              ; &envptr175424[6]
  %G9j$_37_62 = load i64, i64* %envptr175425, align 8                                ; load; *envptr175425
  %envptr175426 = inttoptr i64 %env174172 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175427 = getelementptr inbounds i64, i64* %envptr175426, i64 5              ; &envptr175426[5]
  %Le8$x = load i64, i64* %envptr175427, align 8                                     ; load; *envptr175427
  %envptr175428 = inttoptr i64 %env174172 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175429 = getelementptr inbounds i64, i64* %envptr175428, i64 4              ; &envptr175428[4]
  %OGB$lx = load i64, i64* %envptr175429, align 8                                    ; load; *envptr175429
  %envptr175430 = inttoptr i64 %env174172 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175431 = getelementptr inbounds i64, i64* %envptr175430, i64 3              ; &envptr175430[3]
  %cont171414 = load i64, i64* %envptr175431, align 8                                ; load; *envptr175431
  %envptr175432 = inttoptr i64 %env174172 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175433 = getelementptr inbounds i64, i64* %envptr175432, i64 2              ; &envptr175432[2]
  %ap2$ly = load i64, i64* %envptr175433, align 8                                    ; load; *envptr175433
  %envptr175434 = inttoptr i64 %env174172 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175435 = getelementptr inbounds i64, i64* %envptr175434, i64 1              ; &envptr175434[1]
  %Pzs$y = load i64, i64* %envptr175435, align 8                                     ; load; *envptr175435
  %_95171423 = call i64 @prim_car(i64 %rvp173060)                                    ; call prim_car
  %rvp173059 = call i64 @prim_cdr(i64 %rvp173060)                                    ; call prim_cdr
  %a171289 = call i64 @prim_car(i64 %rvp173059)                                      ; call prim_car
  %na172954 = call i64 @prim_cdr(i64 %rvp173059)                                     ; call prim_cdr
  %arg171985 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %retprim171424 = call i64 @prim_make_45vector(i64 %arg171985, i64 %a171289)        ; call prim_make_45vector
  %cloptr175436 = call i64* @alloc(i64 64)                                           ; malloc
  %eptr175438 = getelementptr inbounds i64, i64* %cloptr175436, i64 1                ; &eptr175438[1]
  %eptr175439 = getelementptr inbounds i64, i64* %cloptr175436, i64 2                ; &eptr175439[2]
  %eptr175440 = getelementptr inbounds i64, i64* %cloptr175436, i64 3                ; &eptr175440[3]
  %eptr175441 = getelementptr inbounds i64, i64* %cloptr175436, i64 4                ; &eptr175441[4]
  %eptr175442 = getelementptr inbounds i64, i64* %cloptr175436, i64 5                ; &eptr175442[5]
  %eptr175443 = getelementptr inbounds i64, i64* %cloptr175436, i64 6                ; &eptr175443[6]
  %eptr175444 = getelementptr inbounds i64, i64* %cloptr175436, i64 7                ; &eptr175444[7]
  store i64 %Pzs$y, i64* %eptr175438                                                 ; *eptr175438 = %Pzs$y
  store i64 %ap2$ly, i64* %eptr175439                                                ; *eptr175439 = %ap2$ly
  store i64 %cont171414, i64* %eptr175440                                            ; *eptr175440 = %cont171414
  store i64 %OGB$lx, i64* %eptr175441                                                ; *eptr175441 = %OGB$lx
  store i64 %Le8$x, i64* %eptr175442                                                 ; *eptr175442 = %Le8$x
  store i64 %G9j$_37_62, i64* %eptr175443                                            ; *eptr175443 = %G9j$_37_62
  store i64 %Re6$_37drop, i64* %eptr175444                                           ; *eptr175444 = %Re6$_37drop
  %eptr175437 = getelementptr inbounds i64, i64* %cloptr175436, i64 0                ; &cloptr175436[0]
  %f175445 = ptrtoint void(i64,i64)* @lam174168 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175445, i64* %eptr175437                                               ; store fptr
  %arg171988 = ptrtoint i64* %cloptr175436 to i64                                    ; closure cast; i64* -> i64
  %arg171987 = add i64 0, 0                                                          ; quoted ()
  %rva173058 = add i64 0, 0                                                          ; quoted ()
  %rva173057 = call i64 @prim_cons(i64 %retprim171424, i64 %rva173058)               ; call prim_cons
  %rva173056 = call i64 @prim_cons(i64 %arg171987, i64 %rva173057)                   ; call prim_cons
  %cloptr175446 = inttoptr i64 %arg171988 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175447 = getelementptr inbounds i64, i64* %cloptr175446, i64 0               ; &cloptr175446[0]
  %f175449 = load i64, i64* %i0ptr175447, align 8                                    ; load; *i0ptr175447
  %fptr175448 = inttoptr i64 %f175449 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175448(i64 %arg171988, i64 %rva173056)              ; tail call
  ret void
}


define void @lam174168(i64 %env174169, i64 %rvp173055) {
  %envptr175450 = inttoptr i64 %env174169 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175451 = getelementptr inbounds i64, i64* %envptr175450, i64 7              ; &envptr175450[7]
  %Re6$_37drop = load i64, i64* %envptr175451, align 8                               ; load; *envptr175451
  %envptr175452 = inttoptr i64 %env174169 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175453 = getelementptr inbounds i64, i64* %envptr175452, i64 6              ; &envptr175452[6]
  %G9j$_37_62 = load i64, i64* %envptr175453, align 8                                ; load; *envptr175453
  %envptr175454 = inttoptr i64 %env174169 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175455 = getelementptr inbounds i64, i64* %envptr175454, i64 5              ; &envptr175454[5]
  %Le8$x = load i64, i64* %envptr175455, align 8                                     ; load; *envptr175455
  %envptr175456 = inttoptr i64 %env174169 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175457 = getelementptr inbounds i64, i64* %envptr175456, i64 4              ; &envptr175456[4]
  %OGB$lx = load i64, i64* %envptr175457, align 8                                    ; load; *envptr175457
  %envptr175458 = inttoptr i64 %env174169 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175459 = getelementptr inbounds i64, i64* %envptr175458, i64 3              ; &envptr175458[3]
  %cont171414 = load i64, i64* %envptr175459, align 8                                ; load; *envptr175459
  %envptr175460 = inttoptr i64 %env174169 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175461 = getelementptr inbounds i64, i64* %envptr175460, i64 2              ; &envptr175460[2]
  %ap2$ly = load i64, i64* %envptr175461, align 8                                    ; load; *envptr175461
  %envptr175462 = inttoptr i64 %env174169 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175463 = getelementptr inbounds i64, i64* %envptr175462, i64 1              ; &envptr175462[1]
  %Pzs$y = load i64, i64* %envptr175463, align 8                                     ; load; *envptr175463
  %_95171417 = call i64 @prim_car(i64 %rvp173055)                                    ; call prim_car
  %rvp173054 = call i64 @prim_cdr(i64 %rvp173055)                                    ; call prim_cdr
  %fYg$loop = call i64 @prim_car(i64 %rvp173054)                                     ; call prim_car
  %na172956 = call i64 @prim_cdr(i64 %rvp173054)                                     ; call prim_cdr
  %arg171990 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %cloptr175464 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr175466 = getelementptr inbounds i64, i64* %cloptr175464, i64 1                ; &eptr175466[1]
  store i64 %fYg$loop, i64* %eptr175466                                              ; *eptr175466 = %fYg$loop
  %eptr175465 = getelementptr inbounds i64, i64* %cloptr175464, i64 0                ; &cloptr175464[0]
  %f175467 = ptrtoint void(i64,i64)* @lam174165 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175467, i64* %eptr175465                                               ; store fptr
  %arg171989 = ptrtoint i64* %cloptr175464 to i64                                    ; closure cast; i64* -> i64
  %m1b$_95171188 = call i64 @prim_vector_45set_33(i64 %fYg$loop, i64 %arg171990, i64 %arg171989); call prim_vector_45set_33
  %arg172005 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171294 = call i64 @prim_vector_45ref(i64 %fYg$loop, i64 %arg172005)              ; call prim_vector_45ref
  %cloptr175468 = call i64* @alloc(i64 72)                                           ; malloc
  %eptr175470 = getelementptr inbounds i64, i64* %cloptr175468, i64 1                ; &eptr175470[1]
  %eptr175471 = getelementptr inbounds i64, i64* %cloptr175468, i64 2                ; &eptr175471[2]
  %eptr175472 = getelementptr inbounds i64, i64* %cloptr175468, i64 3                ; &eptr175472[3]
  %eptr175473 = getelementptr inbounds i64, i64* %cloptr175468, i64 4                ; &eptr175473[4]
  %eptr175474 = getelementptr inbounds i64, i64* %cloptr175468, i64 5                ; &eptr175474[5]
  %eptr175475 = getelementptr inbounds i64, i64* %cloptr175468, i64 6                ; &eptr175475[6]
  %eptr175476 = getelementptr inbounds i64, i64* %cloptr175468, i64 7                ; &eptr175476[7]
  %eptr175477 = getelementptr inbounds i64, i64* %cloptr175468, i64 8                ; &eptr175477[8]
  store i64 %Pzs$y, i64* %eptr175470                                                 ; *eptr175470 = %Pzs$y
  store i64 %ap2$ly, i64* %eptr175471                                                ; *eptr175471 = %ap2$ly
  store i64 %a171294, i64* %eptr175472                                               ; *eptr175472 = %a171294
  store i64 %cont171414, i64* %eptr175473                                            ; *eptr175473 = %cont171414
  store i64 %OGB$lx, i64* %eptr175474                                                ; *eptr175474 = %OGB$lx
  store i64 %Le8$x, i64* %eptr175475                                                 ; *eptr175475 = %Le8$x
  store i64 %G9j$_37_62, i64* %eptr175476                                            ; *eptr175476 = %G9j$_37_62
  store i64 %Re6$_37drop, i64* %eptr175477                                           ; *eptr175477 = %Re6$_37drop
  %eptr175469 = getelementptr inbounds i64, i64* %cloptr175468, i64 0                ; &cloptr175468[0]
  %f175478 = ptrtoint void(i64,i64)* @lam174158 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175478, i64* %eptr175469                                               ; store fptr
  %arg172009 = ptrtoint i64* %cloptr175468 to i64                                    ; closure cast; i64* -> i64
  %rva173053 = add i64 0, 0                                                          ; quoted ()
  %rva173052 = call i64 @prim_cons(i64 %ap2$ly, i64 %rva173053)                      ; call prim_cons
  %rva173051 = call i64 @prim_cons(i64 %OGB$lx, i64 %rva173052)                      ; call prim_cons
  %rva173050 = call i64 @prim_cons(i64 %arg172009, i64 %rva173051)                   ; call prim_cons
  %cloptr175479 = inttoptr i64 %G9j$_37_62 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175480 = getelementptr inbounds i64, i64* %cloptr175479, i64 0               ; &cloptr175479[0]
  %f175482 = load i64, i64* %i0ptr175480, align 8                                    ; load; *i0ptr175480
  %fptr175481 = inttoptr i64 %f175482 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175481(i64 %G9j$_37_62, i64 %rva173050)             ; tail call
  ret void
}


define void @lam174165(i64 %env174166, i64 %rvp172968) {
  %envptr175483 = inttoptr i64 %env174166 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175484 = getelementptr inbounds i64, i64* %envptr175483, i64 1              ; &envptr175483[1]
  %fYg$loop = load i64, i64* %envptr175484, align 8                                  ; load; *envptr175484
  %cont171418 = call i64 @prim_car(i64 %rvp172968)                                   ; call prim_car
  %rvp172967 = call i64 @prim_cdr(i64 %rvp172968)                                    ; call prim_cdr
  %Nqd$x = call i64 @prim_car(i64 %rvp172967)                                        ; call prim_car
  %rvp172966 = call i64 @prim_cdr(i64 %rvp172967)                                    ; call prim_cdr
  %e2g$y = call i64 @prim_car(i64 %rvp172966)                                        ; call prim_car
  %na172958 = call i64 @prim_cdr(i64 %rvp172966)                                     ; call prim_cdr
  %a171290 = call i64 @prim_eq_63(i64 %Nqd$x, i64 %e2g$y)                            ; call prim_eq_63
  %cmp175485 = icmp eq i64 %a171290, 15                                              ; false?
  br i1 %cmp175485, label %else175487, label %then175486                             ; if

then175486:
  %arg171995 = add i64 0, 0                                                          ; quoted ()
  %rva172961 = add i64 0, 0                                                          ; quoted ()
  %rva172960 = call i64 @prim_cons(i64 %Nqd$x, i64 %rva172961)                       ; call prim_cons
  %rva172959 = call i64 @prim_cons(i64 %arg171995, i64 %rva172960)                   ; call prim_cons
  %cloptr175488 = inttoptr i64 %cont171418 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175489 = getelementptr inbounds i64, i64* %cloptr175488, i64 0               ; &cloptr175488[0]
  %f175491 = load i64, i64* %i0ptr175489, align 8                                    ; load; *i0ptr175489
  %fptr175490 = inttoptr i64 %f175491 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175490(i64 %cont171418, i64 %rva172959)             ; tail call
  ret void

else175487:
  %arg171997 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171291 = call i64 @prim_vector_45ref(i64 %fYg$loop, i64 %arg171997)              ; call prim_vector_45ref
  %a171292 = call i64 @prim_cdr(i64 %Nqd$x)                                          ; call prim_cdr
  %a171293 = call i64 @prim_cdr(i64 %e2g$y)                                          ; call prim_cdr
  %rva172965 = add i64 0, 0                                                          ; quoted ()
  %rva172964 = call i64 @prim_cons(i64 %a171293, i64 %rva172965)                     ; call prim_cons
  %rva172963 = call i64 @prim_cons(i64 %a171292, i64 %rva172964)                     ; call prim_cons
  %rva172962 = call i64 @prim_cons(i64 %cont171418, i64 %rva172963)                  ; call prim_cons
  %cloptr175492 = inttoptr i64 %a171291 to i64*                                      ; closure/env cast; i64 -> i64*
  %i0ptr175493 = getelementptr inbounds i64, i64* %cloptr175492, i64 0               ; &cloptr175492[0]
  %f175495 = load i64, i64* %i0ptr175493, align 8                                    ; load; *i0ptr175493
  %fptr175494 = inttoptr i64 %f175495 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175494(i64 %a171291, i64 %rva172962)                ; tail call
  ret void
}


define void @lam174158(i64 %env174159, i64 %rvp173049) {
  %envptr175496 = inttoptr i64 %env174159 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175497 = getelementptr inbounds i64, i64* %envptr175496, i64 8              ; &envptr175496[8]
  %Re6$_37drop = load i64, i64* %envptr175497, align 8                               ; load; *envptr175497
  %envptr175498 = inttoptr i64 %env174159 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175499 = getelementptr inbounds i64, i64* %envptr175498, i64 7              ; &envptr175498[7]
  %G9j$_37_62 = load i64, i64* %envptr175499, align 8                                ; load; *envptr175499
  %envptr175500 = inttoptr i64 %env174159 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175501 = getelementptr inbounds i64, i64* %envptr175500, i64 6              ; &envptr175500[6]
  %Le8$x = load i64, i64* %envptr175501, align 8                                     ; load; *envptr175501
  %envptr175502 = inttoptr i64 %env174159 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175503 = getelementptr inbounds i64, i64* %envptr175502, i64 5              ; &envptr175502[5]
  %OGB$lx = load i64, i64* %envptr175503, align 8                                    ; load; *envptr175503
  %envptr175504 = inttoptr i64 %env174159 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175505 = getelementptr inbounds i64, i64* %envptr175504, i64 4              ; &envptr175504[4]
  %cont171414 = load i64, i64* %envptr175505, align 8                                ; load; *envptr175505
  %envptr175506 = inttoptr i64 %env174159 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175507 = getelementptr inbounds i64, i64* %envptr175506, i64 3              ; &envptr175506[3]
  %a171294 = load i64, i64* %envptr175507, align 8                                   ; load; *envptr175507
  %envptr175508 = inttoptr i64 %env174159 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175509 = getelementptr inbounds i64, i64* %envptr175508, i64 2              ; &envptr175508[2]
  %ap2$ly = load i64, i64* %envptr175509, align 8                                    ; load; *envptr175509
  %envptr175510 = inttoptr i64 %env174159 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175511 = getelementptr inbounds i64, i64* %envptr175510, i64 1              ; &envptr175510[1]
  %Pzs$y = load i64, i64* %envptr175511, align 8                                     ; load; *envptr175511
  %_95171419 = call i64 @prim_car(i64 %rvp173049)                                    ; call prim_car
  %rvp173048 = call i64 @prim_cdr(i64 %rvp173049)                                    ; call prim_cdr
  %a171295 = call i64 @prim_car(i64 %rvp173048)                                      ; call prim_car
  %na172970 = call i64 @prim_cdr(i64 %rvp173048)                                     ; call prim_cdr
  %cmp175512 = icmp eq i64 %a171295, 15                                              ; false?
  br i1 %cmp175512, label %else175514, label %then175513                             ; if

then175513:
  %a171296 = call i64 @prim__45(i64 %OGB$lx, i64 %ap2$ly)                            ; call prim__45
  %cloptr175515 = call i64* @alloc(i64 64)                                           ; malloc
  %eptr175517 = getelementptr inbounds i64, i64* %cloptr175515, i64 1                ; &eptr175517[1]
  %eptr175518 = getelementptr inbounds i64, i64* %cloptr175515, i64 2                ; &eptr175518[2]
  %eptr175519 = getelementptr inbounds i64, i64* %cloptr175515, i64 3                ; &eptr175519[3]
  %eptr175520 = getelementptr inbounds i64, i64* %cloptr175515, i64 4                ; &eptr175520[4]
  %eptr175521 = getelementptr inbounds i64, i64* %cloptr175515, i64 5                ; &eptr175521[5]
  %eptr175522 = getelementptr inbounds i64, i64* %cloptr175515, i64 6                ; &eptr175522[6]
  %eptr175523 = getelementptr inbounds i64, i64* %cloptr175515, i64 7                ; &eptr175523[7]
  store i64 %Pzs$y, i64* %eptr175517                                                 ; *eptr175517 = %Pzs$y
  store i64 %ap2$ly, i64* %eptr175518                                                ; *eptr175518 = %ap2$ly
  store i64 %a171294, i64* %eptr175519                                               ; *eptr175519 = %a171294
  store i64 %cont171414, i64* %eptr175520                                            ; *eptr175520 = %cont171414
  store i64 %OGB$lx, i64* %eptr175521                                                ; *eptr175521 = %OGB$lx
  store i64 %G9j$_37_62, i64* %eptr175522                                            ; *eptr175522 = %G9j$_37_62
  store i64 %Re6$_37drop, i64* %eptr175523                                           ; *eptr175523 = %Re6$_37drop
  %eptr175516 = getelementptr inbounds i64, i64* %cloptr175515, i64 0                ; &cloptr175515[0]
  %f175524 = ptrtoint void(i64,i64)* @lam174140 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175524, i64* %eptr175516                                               ; store fptr
  %arg172015 = ptrtoint i64* %cloptr175515 to i64                                    ; closure cast; i64* -> i64
  %rva173009 = add i64 0, 0                                                          ; quoted ()
  %rva173008 = call i64 @prim_cons(i64 %a171296, i64 %rva173009)                     ; call prim_cons
  %rva173007 = call i64 @prim_cons(i64 %Le8$x, i64 %rva173008)                       ; call prim_cons
  %rva173006 = call i64 @prim_cons(i64 %arg172015, i64 %rva173007)                   ; call prim_cons
  %cloptr175525 = inttoptr i64 %Re6$_37drop to i64*                                  ; closure/env cast; i64 -> i64*
  %i0ptr175526 = getelementptr inbounds i64, i64* %cloptr175525, i64 0               ; &cloptr175525[0]
  %f175528 = load i64, i64* %i0ptr175526, align 8                                    ; load; *i0ptr175526
  %fptr175527 = inttoptr i64 %f175528 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175527(i64 %Re6$_37drop, i64 %rva173006)            ; tail call
  ret void

else175514:
  %cloptr175529 = call i64* @alloc(i64 64)                                           ; malloc
  %eptr175531 = getelementptr inbounds i64, i64* %cloptr175529, i64 1                ; &eptr175531[1]
  %eptr175532 = getelementptr inbounds i64, i64* %cloptr175529, i64 2                ; &eptr175532[2]
  %eptr175533 = getelementptr inbounds i64, i64* %cloptr175529, i64 3                ; &eptr175533[3]
  %eptr175534 = getelementptr inbounds i64, i64* %cloptr175529, i64 4                ; &eptr175534[4]
  %eptr175535 = getelementptr inbounds i64, i64* %cloptr175529, i64 5                ; &eptr175535[5]
  %eptr175536 = getelementptr inbounds i64, i64* %cloptr175529, i64 6                ; &eptr175536[6]
  %eptr175537 = getelementptr inbounds i64, i64* %cloptr175529, i64 7                ; &eptr175537[7]
  store i64 %Pzs$y, i64* %eptr175531                                                 ; *eptr175531 = %Pzs$y
  store i64 %ap2$ly, i64* %eptr175532                                                ; *eptr175532 = %ap2$ly
  store i64 %a171294, i64* %eptr175533                                               ; *eptr175533 = %a171294
  store i64 %cont171414, i64* %eptr175534                                            ; *eptr175534 = %cont171414
  store i64 %OGB$lx, i64* %eptr175535                                                ; *eptr175535 = %OGB$lx
  store i64 %G9j$_37_62, i64* %eptr175536                                            ; *eptr175536 = %G9j$_37_62
  store i64 %Re6$_37drop, i64* %eptr175537                                           ; *eptr175537 = %Re6$_37drop
  %eptr175530 = getelementptr inbounds i64, i64* %cloptr175529, i64 0                ; &cloptr175529[0]
  %f175538 = ptrtoint void(i64,i64)* @lam174156 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175538, i64* %eptr175530                                               ; store fptr
  %arg172040 = ptrtoint i64* %cloptr175529 to i64                                    ; closure cast; i64* -> i64
  %arg172039 = add i64 0, 0                                                          ; quoted ()
  %rva173047 = add i64 0, 0                                                          ; quoted ()
  %rva173046 = call i64 @prim_cons(i64 %Le8$x, i64 %rva173047)                       ; call prim_cons
  %rva173045 = call i64 @prim_cons(i64 %arg172039, i64 %rva173046)                   ; call prim_cons
  %cloptr175539 = inttoptr i64 %arg172040 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175540 = getelementptr inbounds i64, i64* %cloptr175539, i64 0               ; &cloptr175539[0]
  %f175542 = load i64, i64* %i0ptr175540, align 8                                    ; load; *i0ptr175540
  %fptr175541 = inttoptr i64 %f175542 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175541(i64 %arg172040, i64 %rva173045)              ; tail call
  ret void
}


define void @lam174156(i64 %env174157, i64 %rvp173044) {
  %envptr175543 = inttoptr i64 %env174157 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175544 = getelementptr inbounds i64, i64* %envptr175543, i64 7              ; &envptr175543[7]
  %Re6$_37drop = load i64, i64* %envptr175544, align 8                               ; load; *envptr175544
  %envptr175545 = inttoptr i64 %env174157 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175546 = getelementptr inbounds i64, i64* %envptr175545, i64 6              ; &envptr175545[6]
  %G9j$_37_62 = load i64, i64* %envptr175546, align 8                                ; load; *envptr175546
  %envptr175547 = inttoptr i64 %env174157 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175548 = getelementptr inbounds i64, i64* %envptr175547, i64 5              ; &envptr175547[5]
  %OGB$lx = load i64, i64* %envptr175548, align 8                                    ; load; *envptr175548
  %envptr175549 = inttoptr i64 %env174157 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175550 = getelementptr inbounds i64, i64* %envptr175549, i64 4              ; &envptr175549[4]
  %cont171414 = load i64, i64* %envptr175550, align 8                                ; load; *envptr175550
  %envptr175551 = inttoptr i64 %env174157 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175552 = getelementptr inbounds i64, i64* %envptr175551, i64 3              ; &envptr175551[3]
  %a171294 = load i64, i64* %envptr175552, align 8                                   ; load; *envptr175552
  %envptr175553 = inttoptr i64 %env174157 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175554 = getelementptr inbounds i64, i64* %envptr175553, i64 2              ; &envptr175553[2]
  %ap2$ly = load i64, i64* %envptr175554, align 8                                    ; load; *envptr175554
  %envptr175555 = inttoptr i64 %env174157 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175556 = getelementptr inbounds i64, i64* %envptr175555, i64 1              ; &envptr175555[1]
  %Pzs$y = load i64, i64* %envptr175556, align 8                                     ; load; *envptr175556
  %_95171420 = call i64 @prim_car(i64 %rvp173044)                                    ; call prim_car
  %rvp173043 = call i64 @prim_cdr(i64 %rvp173044)                                    ; call prim_cdr
  %a171297 = call i64 @prim_car(i64 %rvp173043)                                      ; call prim_car
  %na173011 = call i64 @prim_cdr(i64 %rvp173043)                                     ; call prim_cdr
  %cloptr175557 = call i64* @alloc(i64 64)                                           ; malloc
  %eptr175559 = getelementptr inbounds i64, i64* %cloptr175557, i64 1                ; &eptr175559[1]
  %eptr175560 = getelementptr inbounds i64, i64* %cloptr175557, i64 2                ; &eptr175560[2]
  %eptr175561 = getelementptr inbounds i64, i64* %cloptr175557, i64 3                ; &eptr175561[3]
  %eptr175562 = getelementptr inbounds i64, i64* %cloptr175557, i64 4                ; &eptr175562[4]
  %eptr175563 = getelementptr inbounds i64, i64* %cloptr175557, i64 5                ; &eptr175563[5]
  %eptr175564 = getelementptr inbounds i64, i64* %cloptr175557, i64 6                ; &eptr175564[6]
  %eptr175565 = getelementptr inbounds i64, i64* %cloptr175557, i64 7                ; &eptr175565[7]
  store i64 %Pzs$y, i64* %eptr175559                                                 ; *eptr175559 = %Pzs$y
  store i64 %ap2$ly, i64* %eptr175560                                                ; *eptr175560 = %ap2$ly
  store i64 %a171294, i64* %eptr175561                                               ; *eptr175561 = %a171294
  store i64 %a171297, i64* %eptr175562                                               ; *eptr175562 = %a171297
  store i64 %cont171414, i64* %eptr175563                                            ; *eptr175563 = %cont171414
  store i64 %OGB$lx, i64* %eptr175564                                                ; *eptr175564 = %OGB$lx
  store i64 %Re6$_37drop, i64* %eptr175565                                           ; *eptr175565 = %Re6$_37drop
  %eptr175558 = getelementptr inbounds i64, i64* %cloptr175557, i64 0                ; &cloptr175557[0]
  %f175566 = ptrtoint void(i64,i64)* @lam174154 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175566, i64* %eptr175558                                               ; store fptr
  %arg172043 = ptrtoint i64* %cloptr175557 to i64                                    ; closure cast; i64* -> i64
  %rva173042 = add i64 0, 0                                                          ; quoted ()
  %rva173041 = call i64 @prim_cons(i64 %OGB$lx, i64 %rva173042)                      ; call prim_cons
  %rva173040 = call i64 @prim_cons(i64 %ap2$ly, i64 %rva173041)                      ; call prim_cons
  %rva173039 = call i64 @prim_cons(i64 %arg172043, i64 %rva173040)                   ; call prim_cons
  %cloptr175567 = inttoptr i64 %G9j$_37_62 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175568 = getelementptr inbounds i64, i64* %cloptr175567, i64 0               ; &cloptr175567[0]
  %f175570 = load i64, i64* %i0ptr175568, align 8                                    ; load; *i0ptr175568
  %fptr175569 = inttoptr i64 %f175570 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175569(i64 %G9j$_37_62, i64 %rva173039)             ; tail call
  ret void
}


define void @lam174154(i64 %env174155, i64 %rvp173038) {
  %envptr175571 = inttoptr i64 %env174155 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175572 = getelementptr inbounds i64, i64* %envptr175571, i64 7              ; &envptr175571[7]
  %Re6$_37drop = load i64, i64* %envptr175572, align 8                               ; load; *envptr175572
  %envptr175573 = inttoptr i64 %env174155 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175574 = getelementptr inbounds i64, i64* %envptr175573, i64 6              ; &envptr175573[6]
  %OGB$lx = load i64, i64* %envptr175574, align 8                                    ; load; *envptr175574
  %envptr175575 = inttoptr i64 %env174155 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175576 = getelementptr inbounds i64, i64* %envptr175575, i64 5              ; &envptr175575[5]
  %cont171414 = load i64, i64* %envptr175576, align 8                                ; load; *envptr175576
  %envptr175577 = inttoptr i64 %env174155 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175578 = getelementptr inbounds i64, i64* %envptr175577, i64 4              ; &envptr175577[4]
  %a171297 = load i64, i64* %envptr175578, align 8                                   ; load; *envptr175578
  %envptr175579 = inttoptr i64 %env174155 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175580 = getelementptr inbounds i64, i64* %envptr175579, i64 3              ; &envptr175579[3]
  %a171294 = load i64, i64* %envptr175580, align 8                                   ; load; *envptr175580
  %envptr175581 = inttoptr i64 %env174155 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175582 = getelementptr inbounds i64, i64* %envptr175581, i64 2              ; &envptr175581[2]
  %ap2$ly = load i64, i64* %envptr175582, align 8                                    ; load; *envptr175582
  %envptr175583 = inttoptr i64 %env174155 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175584 = getelementptr inbounds i64, i64* %envptr175583, i64 1              ; &envptr175583[1]
  %Pzs$y = load i64, i64* %envptr175584, align 8                                     ; load; *envptr175584
  %_95171421 = call i64 @prim_car(i64 %rvp173038)                                    ; call prim_car
  %rvp173037 = call i64 @prim_cdr(i64 %rvp173038)                                    ; call prim_cdr
  %a171298 = call i64 @prim_car(i64 %rvp173037)                                      ; call prim_car
  %na173013 = call i64 @prim_cdr(i64 %rvp173037)                                     ; call prim_cdr
  %cmp175585 = icmp eq i64 %a171298, 15                                              ; false?
  br i1 %cmp175585, label %else175587, label %then175586                             ; if

then175586:
  %a171299 = call i64 @prim__45(i64 %ap2$ly, i64 %OGB$lx)                            ; call prim__45
  %cloptr175588 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175590 = getelementptr inbounds i64, i64* %cloptr175588, i64 1                ; &eptr175590[1]
  %eptr175591 = getelementptr inbounds i64, i64* %cloptr175588, i64 2                ; &eptr175591[2]
  %eptr175592 = getelementptr inbounds i64, i64* %cloptr175588, i64 3                ; &eptr175592[3]
  store i64 %a171294, i64* %eptr175590                                               ; *eptr175590 = %a171294
  store i64 %a171297, i64* %eptr175591                                               ; *eptr175591 = %a171297
  store i64 %cont171414, i64* %eptr175592                                            ; *eptr175592 = %cont171414
  %eptr175589 = getelementptr inbounds i64, i64* %cloptr175588, i64 0                ; &cloptr175588[0]
  %f175593 = ptrtoint void(i64,i64)* @lam174147 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175593, i64* %eptr175589                                               ; store fptr
  %arg172049 = ptrtoint i64* %cloptr175588 to i64                                    ; closure cast; i64* -> i64
  %rva173025 = add i64 0, 0                                                          ; quoted ()
  %rva173024 = call i64 @prim_cons(i64 %a171299, i64 %rva173025)                     ; call prim_cons
  %rva173023 = call i64 @prim_cons(i64 %Pzs$y, i64 %rva173024)                       ; call prim_cons
  %rva173022 = call i64 @prim_cons(i64 %arg172049, i64 %rva173023)                   ; call prim_cons
  %cloptr175594 = inttoptr i64 %Re6$_37drop to i64*                                  ; closure/env cast; i64 -> i64*
  %i0ptr175595 = getelementptr inbounds i64, i64* %cloptr175594, i64 0               ; &cloptr175594[0]
  %f175597 = load i64, i64* %i0ptr175595, align 8                                    ; load; *i0ptr175595
  %fptr175596 = inttoptr i64 %f175597 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175596(i64 %Re6$_37drop, i64 %rva173022)            ; tail call
  ret void

else175587:
  %cloptr175598 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175600 = getelementptr inbounds i64, i64* %cloptr175598, i64 1                ; &eptr175600[1]
  %eptr175601 = getelementptr inbounds i64, i64* %cloptr175598, i64 2                ; &eptr175601[2]
  %eptr175602 = getelementptr inbounds i64, i64* %cloptr175598, i64 3                ; &eptr175602[3]
  store i64 %a171294, i64* %eptr175600                                               ; *eptr175600 = %a171294
  store i64 %a171297, i64* %eptr175601                                               ; *eptr175601 = %a171297
  store i64 %cont171414, i64* %eptr175602                                            ; *eptr175602 = %cont171414
  %eptr175599 = getelementptr inbounds i64, i64* %cloptr175598, i64 0                ; &cloptr175598[0]
  %f175603 = ptrtoint void(i64,i64)* @lam174152 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175603, i64* %eptr175599                                               ; store fptr
  %arg172057 = ptrtoint i64* %cloptr175598 to i64                                    ; closure cast; i64* -> i64
  %arg172056 = add i64 0, 0                                                          ; quoted ()
  %rva173036 = add i64 0, 0                                                          ; quoted ()
  %rva173035 = call i64 @prim_cons(i64 %Pzs$y, i64 %rva173036)                       ; call prim_cons
  %rva173034 = call i64 @prim_cons(i64 %arg172056, i64 %rva173035)                   ; call prim_cons
  %cloptr175604 = inttoptr i64 %arg172057 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175605 = getelementptr inbounds i64, i64* %cloptr175604, i64 0               ; &cloptr175604[0]
  %f175607 = load i64, i64* %i0ptr175605, align 8                                    ; load; *i0ptr175605
  %fptr175606 = inttoptr i64 %f175607 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175606(i64 %arg172057, i64 %rva173034)              ; tail call
  ret void
}


define void @lam174152(i64 %env174153, i64 %rvp173033) {
  %envptr175608 = inttoptr i64 %env174153 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175609 = getelementptr inbounds i64, i64* %envptr175608, i64 3              ; &envptr175608[3]
  %cont171414 = load i64, i64* %envptr175609, align 8                                ; load; *envptr175609
  %envptr175610 = inttoptr i64 %env174153 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175611 = getelementptr inbounds i64, i64* %envptr175610, i64 2              ; &envptr175610[2]
  %a171297 = load i64, i64* %envptr175611, align 8                                   ; load; *envptr175611
  %envptr175612 = inttoptr i64 %env174153 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175613 = getelementptr inbounds i64, i64* %envptr175612, i64 1              ; &envptr175612[1]
  %a171294 = load i64, i64* %envptr175613, align 8                                   ; load; *envptr175613
  %_95171422 = call i64 @prim_car(i64 %rvp173033)                                    ; call prim_car
  %rvp173032 = call i64 @prim_cdr(i64 %rvp173033)                                    ; call prim_cdr
  %a171300 = call i64 @prim_car(i64 %rvp173032)                                      ; call prim_car
  %na173027 = call i64 @prim_cdr(i64 %rvp173032)                                     ; call prim_cdr
  %rva173031 = add i64 0, 0                                                          ; quoted ()
  %rva173030 = call i64 @prim_cons(i64 %a171300, i64 %rva173031)                     ; call prim_cons
  %rva173029 = call i64 @prim_cons(i64 %a171297, i64 %rva173030)                     ; call prim_cons
  %rva173028 = call i64 @prim_cons(i64 %cont171414, i64 %rva173029)                  ; call prim_cons
  %cloptr175614 = inttoptr i64 %a171294 to i64*                                      ; closure/env cast; i64 -> i64*
  %i0ptr175615 = getelementptr inbounds i64, i64* %cloptr175614, i64 0               ; &cloptr175614[0]
  %f175617 = load i64, i64* %i0ptr175615, align 8                                    ; load; *i0ptr175615
  %fptr175616 = inttoptr i64 %f175617 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175616(i64 %a171294, i64 %rva173028)                ; tail call
  ret void
}


define void @lam174147(i64 %env174148, i64 %rvp173021) {
  %envptr175618 = inttoptr i64 %env174148 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175619 = getelementptr inbounds i64, i64* %envptr175618, i64 3              ; &envptr175618[3]
  %cont171414 = load i64, i64* %envptr175619, align 8                                ; load; *envptr175619
  %envptr175620 = inttoptr i64 %env174148 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175621 = getelementptr inbounds i64, i64* %envptr175620, i64 2              ; &envptr175620[2]
  %a171297 = load i64, i64* %envptr175621, align 8                                   ; load; *envptr175621
  %envptr175622 = inttoptr i64 %env174148 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175623 = getelementptr inbounds i64, i64* %envptr175622, i64 1              ; &envptr175622[1]
  %a171294 = load i64, i64* %envptr175623, align 8                                   ; load; *envptr175623
  %_95171422 = call i64 @prim_car(i64 %rvp173021)                                    ; call prim_car
  %rvp173020 = call i64 @prim_cdr(i64 %rvp173021)                                    ; call prim_cdr
  %a171300 = call i64 @prim_car(i64 %rvp173020)                                      ; call prim_car
  %na173015 = call i64 @prim_cdr(i64 %rvp173020)                                     ; call prim_cdr
  %rva173019 = add i64 0, 0                                                          ; quoted ()
  %rva173018 = call i64 @prim_cons(i64 %a171300, i64 %rva173019)                     ; call prim_cons
  %rva173017 = call i64 @prim_cons(i64 %a171297, i64 %rva173018)                     ; call prim_cons
  %rva173016 = call i64 @prim_cons(i64 %cont171414, i64 %rva173017)                  ; call prim_cons
  %cloptr175624 = inttoptr i64 %a171294 to i64*                                      ; closure/env cast; i64 -> i64*
  %i0ptr175625 = getelementptr inbounds i64, i64* %cloptr175624, i64 0               ; &cloptr175624[0]
  %f175627 = load i64, i64* %i0ptr175625, align 8                                    ; load; *i0ptr175625
  %fptr175626 = inttoptr i64 %f175627 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175626(i64 %a171294, i64 %rva173016)                ; tail call
  ret void
}


define void @lam174140(i64 %env174141, i64 %rvp173005) {
  %envptr175628 = inttoptr i64 %env174141 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175629 = getelementptr inbounds i64, i64* %envptr175628, i64 7              ; &envptr175628[7]
  %Re6$_37drop = load i64, i64* %envptr175629, align 8                               ; load; *envptr175629
  %envptr175630 = inttoptr i64 %env174141 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175631 = getelementptr inbounds i64, i64* %envptr175630, i64 6              ; &envptr175630[6]
  %G9j$_37_62 = load i64, i64* %envptr175631, align 8                                ; load; *envptr175631
  %envptr175632 = inttoptr i64 %env174141 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175633 = getelementptr inbounds i64, i64* %envptr175632, i64 5              ; &envptr175632[5]
  %OGB$lx = load i64, i64* %envptr175633, align 8                                    ; load; *envptr175633
  %envptr175634 = inttoptr i64 %env174141 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175635 = getelementptr inbounds i64, i64* %envptr175634, i64 4              ; &envptr175634[4]
  %cont171414 = load i64, i64* %envptr175635, align 8                                ; load; *envptr175635
  %envptr175636 = inttoptr i64 %env174141 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175637 = getelementptr inbounds i64, i64* %envptr175636, i64 3              ; &envptr175636[3]
  %a171294 = load i64, i64* %envptr175637, align 8                                   ; load; *envptr175637
  %envptr175638 = inttoptr i64 %env174141 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175639 = getelementptr inbounds i64, i64* %envptr175638, i64 2              ; &envptr175638[2]
  %ap2$ly = load i64, i64* %envptr175639, align 8                                    ; load; *envptr175639
  %envptr175640 = inttoptr i64 %env174141 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175641 = getelementptr inbounds i64, i64* %envptr175640, i64 1              ; &envptr175640[1]
  %Pzs$y = load i64, i64* %envptr175641, align 8                                     ; load; *envptr175641
  %_95171420 = call i64 @prim_car(i64 %rvp173005)                                    ; call prim_car
  %rvp173004 = call i64 @prim_cdr(i64 %rvp173005)                                    ; call prim_cdr
  %a171297 = call i64 @prim_car(i64 %rvp173004)                                      ; call prim_car
  %na172972 = call i64 @prim_cdr(i64 %rvp173004)                                     ; call prim_cdr
  %cloptr175642 = call i64* @alloc(i64 64)                                           ; malloc
  %eptr175644 = getelementptr inbounds i64, i64* %cloptr175642, i64 1                ; &eptr175644[1]
  %eptr175645 = getelementptr inbounds i64, i64* %cloptr175642, i64 2                ; &eptr175645[2]
  %eptr175646 = getelementptr inbounds i64, i64* %cloptr175642, i64 3                ; &eptr175646[3]
  %eptr175647 = getelementptr inbounds i64, i64* %cloptr175642, i64 4                ; &eptr175647[4]
  %eptr175648 = getelementptr inbounds i64, i64* %cloptr175642, i64 5                ; &eptr175648[5]
  %eptr175649 = getelementptr inbounds i64, i64* %cloptr175642, i64 6                ; &eptr175649[6]
  %eptr175650 = getelementptr inbounds i64, i64* %cloptr175642, i64 7                ; &eptr175650[7]
  store i64 %Pzs$y, i64* %eptr175644                                                 ; *eptr175644 = %Pzs$y
  store i64 %ap2$ly, i64* %eptr175645                                                ; *eptr175645 = %ap2$ly
  store i64 %a171294, i64* %eptr175646                                               ; *eptr175646 = %a171294
  store i64 %a171297, i64* %eptr175647                                               ; *eptr175647 = %a171297
  store i64 %cont171414, i64* %eptr175648                                            ; *eptr175648 = %cont171414
  store i64 %OGB$lx, i64* %eptr175649                                                ; *eptr175649 = %OGB$lx
  store i64 %Re6$_37drop, i64* %eptr175650                                           ; *eptr175650 = %Re6$_37drop
  %eptr175643 = getelementptr inbounds i64, i64* %cloptr175642, i64 0                ; &cloptr175642[0]
  %f175651 = ptrtoint void(i64,i64)* @lam174138 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175651, i64* %eptr175643                                               ; store fptr
  %arg172019 = ptrtoint i64* %cloptr175642 to i64                                    ; closure cast; i64* -> i64
  %rva173003 = add i64 0, 0                                                          ; quoted ()
  %rva173002 = call i64 @prim_cons(i64 %OGB$lx, i64 %rva173003)                      ; call prim_cons
  %rva173001 = call i64 @prim_cons(i64 %ap2$ly, i64 %rva173002)                      ; call prim_cons
  %rva173000 = call i64 @prim_cons(i64 %arg172019, i64 %rva173001)                   ; call prim_cons
  %cloptr175652 = inttoptr i64 %G9j$_37_62 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175653 = getelementptr inbounds i64, i64* %cloptr175652, i64 0               ; &cloptr175652[0]
  %f175655 = load i64, i64* %i0ptr175653, align 8                                    ; load; *i0ptr175653
  %fptr175654 = inttoptr i64 %f175655 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175654(i64 %G9j$_37_62, i64 %rva173000)             ; tail call
  ret void
}


define void @lam174138(i64 %env174139, i64 %rvp172999) {
  %envptr175656 = inttoptr i64 %env174139 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175657 = getelementptr inbounds i64, i64* %envptr175656, i64 7              ; &envptr175656[7]
  %Re6$_37drop = load i64, i64* %envptr175657, align 8                               ; load; *envptr175657
  %envptr175658 = inttoptr i64 %env174139 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175659 = getelementptr inbounds i64, i64* %envptr175658, i64 6              ; &envptr175658[6]
  %OGB$lx = load i64, i64* %envptr175659, align 8                                    ; load; *envptr175659
  %envptr175660 = inttoptr i64 %env174139 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175661 = getelementptr inbounds i64, i64* %envptr175660, i64 5              ; &envptr175660[5]
  %cont171414 = load i64, i64* %envptr175661, align 8                                ; load; *envptr175661
  %envptr175662 = inttoptr i64 %env174139 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175663 = getelementptr inbounds i64, i64* %envptr175662, i64 4              ; &envptr175662[4]
  %a171297 = load i64, i64* %envptr175663, align 8                                   ; load; *envptr175663
  %envptr175664 = inttoptr i64 %env174139 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175665 = getelementptr inbounds i64, i64* %envptr175664, i64 3              ; &envptr175664[3]
  %a171294 = load i64, i64* %envptr175665, align 8                                   ; load; *envptr175665
  %envptr175666 = inttoptr i64 %env174139 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175667 = getelementptr inbounds i64, i64* %envptr175666, i64 2              ; &envptr175666[2]
  %ap2$ly = load i64, i64* %envptr175667, align 8                                    ; load; *envptr175667
  %envptr175668 = inttoptr i64 %env174139 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175669 = getelementptr inbounds i64, i64* %envptr175668, i64 1              ; &envptr175668[1]
  %Pzs$y = load i64, i64* %envptr175669, align 8                                     ; load; *envptr175669
  %_95171421 = call i64 @prim_car(i64 %rvp172999)                                    ; call prim_car
  %rvp172998 = call i64 @prim_cdr(i64 %rvp172999)                                    ; call prim_cdr
  %a171298 = call i64 @prim_car(i64 %rvp172998)                                      ; call prim_car
  %na172974 = call i64 @prim_cdr(i64 %rvp172998)                                     ; call prim_cdr
  %cmp175670 = icmp eq i64 %a171298, 15                                              ; false?
  br i1 %cmp175670, label %else175672, label %then175671                             ; if

then175671:
  %a171299 = call i64 @prim__45(i64 %ap2$ly, i64 %OGB$lx)                            ; call prim__45
  %cloptr175673 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175675 = getelementptr inbounds i64, i64* %cloptr175673, i64 1                ; &eptr175675[1]
  %eptr175676 = getelementptr inbounds i64, i64* %cloptr175673, i64 2                ; &eptr175676[2]
  %eptr175677 = getelementptr inbounds i64, i64* %cloptr175673, i64 3                ; &eptr175677[3]
  store i64 %a171294, i64* %eptr175675                                               ; *eptr175675 = %a171294
  store i64 %a171297, i64* %eptr175676                                               ; *eptr175676 = %a171297
  store i64 %cont171414, i64* %eptr175677                                            ; *eptr175677 = %cont171414
  %eptr175674 = getelementptr inbounds i64, i64* %cloptr175673, i64 0                ; &cloptr175673[0]
  %f175678 = ptrtoint void(i64,i64)* @lam174131 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175678, i64* %eptr175674                                               ; store fptr
  %arg172025 = ptrtoint i64* %cloptr175673 to i64                                    ; closure cast; i64* -> i64
  %rva172986 = add i64 0, 0                                                          ; quoted ()
  %rva172985 = call i64 @prim_cons(i64 %a171299, i64 %rva172986)                     ; call prim_cons
  %rva172984 = call i64 @prim_cons(i64 %Pzs$y, i64 %rva172985)                       ; call prim_cons
  %rva172983 = call i64 @prim_cons(i64 %arg172025, i64 %rva172984)                   ; call prim_cons
  %cloptr175679 = inttoptr i64 %Re6$_37drop to i64*                                  ; closure/env cast; i64 -> i64*
  %i0ptr175680 = getelementptr inbounds i64, i64* %cloptr175679, i64 0               ; &cloptr175679[0]
  %f175682 = load i64, i64* %i0ptr175680, align 8                                    ; load; *i0ptr175680
  %fptr175681 = inttoptr i64 %f175682 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175681(i64 %Re6$_37drop, i64 %rva172983)            ; tail call
  ret void

else175672:
  %cloptr175683 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175685 = getelementptr inbounds i64, i64* %cloptr175683, i64 1                ; &eptr175685[1]
  %eptr175686 = getelementptr inbounds i64, i64* %cloptr175683, i64 2                ; &eptr175686[2]
  %eptr175687 = getelementptr inbounds i64, i64* %cloptr175683, i64 3                ; &eptr175687[3]
  store i64 %a171294, i64* %eptr175685                                               ; *eptr175685 = %a171294
  store i64 %a171297, i64* %eptr175686                                               ; *eptr175686 = %a171297
  store i64 %cont171414, i64* %eptr175687                                            ; *eptr175687 = %cont171414
  %eptr175684 = getelementptr inbounds i64, i64* %cloptr175683, i64 0                ; &cloptr175683[0]
  %f175688 = ptrtoint void(i64,i64)* @lam174136 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175688, i64* %eptr175684                                               ; store fptr
  %arg172033 = ptrtoint i64* %cloptr175683 to i64                                    ; closure cast; i64* -> i64
  %arg172032 = add i64 0, 0                                                          ; quoted ()
  %rva172997 = add i64 0, 0                                                          ; quoted ()
  %rva172996 = call i64 @prim_cons(i64 %Pzs$y, i64 %rva172997)                       ; call prim_cons
  %rva172995 = call i64 @prim_cons(i64 %arg172032, i64 %rva172996)                   ; call prim_cons
  %cloptr175689 = inttoptr i64 %arg172033 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175690 = getelementptr inbounds i64, i64* %cloptr175689, i64 0               ; &cloptr175689[0]
  %f175692 = load i64, i64* %i0ptr175690, align 8                                    ; load; *i0ptr175690
  %fptr175691 = inttoptr i64 %f175692 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175691(i64 %arg172033, i64 %rva172995)              ; tail call
  ret void
}


define void @lam174136(i64 %env174137, i64 %rvp172994) {
  %envptr175693 = inttoptr i64 %env174137 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175694 = getelementptr inbounds i64, i64* %envptr175693, i64 3              ; &envptr175693[3]
  %cont171414 = load i64, i64* %envptr175694, align 8                                ; load; *envptr175694
  %envptr175695 = inttoptr i64 %env174137 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175696 = getelementptr inbounds i64, i64* %envptr175695, i64 2              ; &envptr175695[2]
  %a171297 = load i64, i64* %envptr175696, align 8                                   ; load; *envptr175696
  %envptr175697 = inttoptr i64 %env174137 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175698 = getelementptr inbounds i64, i64* %envptr175697, i64 1              ; &envptr175697[1]
  %a171294 = load i64, i64* %envptr175698, align 8                                   ; load; *envptr175698
  %_95171422 = call i64 @prim_car(i64 %rvp172994)                                    ; call prim_car
  %rvp172993 = call i64 @prim_cdr(i64 %rvp172994)                                    ; call prim_cdr
  %a171300 = call i64 @prim_car(i64 %rvp172993)                                      ; call prim_car
  %na172988 = call i64 @prim_cdr(i64 %rvp172993)                                     ; call prim_cdr
  %rva172992 = add i64 0, 0                                                          ; quoted ()
  %rva172991 = call i64 @prim_cons(i64 %a171300, i64 %rva172992)                     ; call prim_cons
  %rva172990 = call i64 @prim_cons(i64 %a171297, i64 %rva172991)                     ; call prim_cons
  %rva172989 = call i64 @prim_cons(i64 %cont171414, i64 %rva172990)                  ; call prim_cons
  %cloptr175699 = inttoptr i64 %a171294 to i64*                                      ; closure/env cast; i64 -> i64*
  %i0ptr175700 = getelementptr inbounds i64, i64* %cloptr175699, i64 0               ; &cloptr175699[0]
  %f175702 = load i64, i64* %i0ptr175700, align 8                                    ; load; *i0ptr175700
  %fptr175701 = inttoptr i64 %f175702 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175701(i64 %a171294, i64 %rva172989)                ; tail call
  ret void
}


define void @lam174131(i64 %env174132, i64 %rvp172982) {
  %envptr175703 = inttoptr i64 %env174132 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175704 = getelementptr inbounds i64, i64* %envptr175703, i64 3              ; &envptr175703[3]
  %cont171414 = load i64, i64* %envptr175704, align 8                                ; load; *envptr175704
  %envptr175705 = inttoptr i64 %env174132 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175706 = getelementptr inbounds i64, i64* %envptr175705, i64 2              ; &envptr175705[2]
  %a171297 = load i64, i64* %envptr175706, align 8                                   ; load; *envptr175706
  %envptr175707 = inttoptr i64 %env174132 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175708 = getelementptr inbounds i64, i64* %envptr175707, i64 1              ; &envptr175707[1]
  %a171294 = load i64, i64* %envptr175708, align 8                                   ; load; *envptr175708
  %_95171422 = call i64 @prim_car(i64 %rvp172982)                                    ; call prim_car
  %rvp172981 = call i64 @prim_cdr(i64 %rvp172982)                                    ; call prim_cdr
  %a171300 = call i64 @prim_car(i64 %rvp172981)                                      ; call prim_car
  %na172976 = call i64 @prim_cdr(i64 %rvp172981)                                     ; call prim_cdr
  %rva172980 = add i64 0, 0                                                          ; quoted ()
  %rva172979 = call i64 @prim_cons(i64 %a171300, i64 %rva172980)                     ; call prim_cons
  %rva172978 = call i64 @prim_cons(i64 %a171297, i64 %rva172979)                     ; call prim_cons
  %rva172977 = call i64 @prim_cons(i64 %cont171414, i64 %rva172978)                  ; call prim_cons
  %cloptr175709 = inttoptr i64 %a171294 to i64*                                      ; closure/env cast; i64 -> i64*
  %i0ptr175710 = getelementptr inbounds i64, i64* %cloptr175709, i64 0               ; &cloptr175709[0]
  %f175712 = load i64, i64* %i0ptr175710, align 8                                    ; load; *i0ptr175710
  %fptr175711 = inttoptr i64 %f175712 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175711(i64 %a171294, i64 %rva172977)                ; tail call
  ret void
}


define void @lam174119(i64 %env174120, i64 %rvp173175) {
  %envptr175713 = inttoptr i64 %env174120 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175714 = getelementptr inbounds i64, i64* %envptr175713, i64 2              ; &envptr175713[2]
  %Hbp$_37common_45tail = load i64, i64* %envptr175714, align 8                      ; load; *envptr175714
  %envptr175715 = inttoptr i64 %env174120 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175716 = getelementptr inbounds i64, i64* %envptr175715, i64 1              ; &envptr175715[1]
  %c3d$_37wind_45stack = load i64, i64* %envptr175716, align 8                       ; load; *envptr175716
  %cont171427 = call i64 @prim_car(i64 %rvp173175)                                   ; call prim_car
  %rvp173174 = call i64 @prim_cdr(i64 %rvp173175)                                    ; call prim_cdr
  %TMl$new = call i64 @prim_car(i64 %rvp173174)                                      ; call prim_car
  %na173077 = call i64 @prim_cdr(i64 %rvp173174)                                     ; call prim_cdr
  %arg172062 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171301 = call i64 @prim_vector_45ref(i64 %c3d$_37wind_45stack, i64 %arg172062)   ; call prim_vector_45ref
  %cloptr175717 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175719 = getelementptr inbounds i64, i64* %cloptr175717, i64 1                ; &eptr175719[1]
  %eptr175720 = getelementptr inbounds i64, i64* %cloptr175717, i64 2                ; &eptr175720[2]
  %eptr175721 = getelementptr inbounds i64, i64* %cloptr175717, i64 3                ; &eptr175721[3]
  store i64 %c3d$_37wind_45stack, i64* %eptr175719                                   ; *eptr175719 = %c3d$_37wind_45stack
  store i64 %TMl$new, i64* %eptr175720                                               ; *eptr175720 = %TMl$new
  store i64 %cont171427, i64* %eptr175721                                            ; *eptr175721 = %cont171427
  %eptr175718 = getelementptr inbounds i64, i64* %cloptr175717, i64 0                ; &cloptr175717[0]
  %f175722 = ptrtoint void(i64,i64)* @lam174116 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175722, i64* %eptr175718                                               ; store fptr
  %arg172066 = ptrtoint i64* %cloptr175717 to i64                                    ; closure cast; i64* -> i64
  %rva173173 = add i64 0, 0                                                          ; quoted ()
  %rva173172 = call i64 @prim_cons(i64 %a171301, i64 %rva173173)                     ; call prim_cons
  %rva173171 = call i64 @prim_cons(i64 %TMl$new, i64 %rva173172)                     ; call prim_cons
  %rva173170 = call i64 @prim_cons(i64 %arg172066, i64 %rva173171)                   ; call prim_cons
  %cloptr175723 = inttoptr i64 %Hbp$_37common_45tail to i64*                         ; closure/env cast; i64 -> i64*
  %i0ptr175724 = getelementptr inbounds i64, i64* %cloptr175723, i64 0               ; &cloptr175723[0]
  %f175726 = load i64, i64* %i0ptr175724, align 8                                    ; load; *i0ptr175724
  %fptr175725 = inttoptr i64 %f175726 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175725(i64 %Hbp$_37common_45tail, i64 %rva173170)   ; tail call
  ret void
}


define void @lam174116(i64 %env174117, i64 %rvp173169) {
  %envptr175727 = inttoptr i64 %env174117 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175728 = getelementptr inbounds i64, i64* %envptr175727, i64 3              ; &envptr175727[3]
  %cont171427 = load i64, i64* %envptr175728, align 8                                ; load; *envptr175728
  %envptr175729 = inttoptr i64 %env174117 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175730 = getelementptr inbounds i64, i64* %envptr175729, i64 2              ; &envptr175729[2]
  %TMl$new = load i64, i64* %envptr175730, align 8                                   ; load; *envptr175730
  %envptr175731 = inttoptr i64 %env174117 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175732 = getelementptr inbounds i64, i64* %envptr175731, i64 1              ; &envptr175731[1]
  %c3d$_37wind_45stack = load i64, i64* %envptr175732, align 8                       ; load; *envptr175732
  %_95171428 = call i64 @prim_car(i64 %rvp173169)                                    ; call prim_car
  %rvp173168 = call i64 @prim_cdr(i64 %rvp173169)                                    ; call prim_cdr
  %eKt$tail = call i64 @prim_car(i64 %rvp173168)                                     ; call prim_car
  %na173079 = call i64 @prim_cdr(i64 %rvp173168)                                     ; call prim_cdr
  %cloptr175733 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr175734 = getelementptr inbounds i64, i64* %cloptr175733, i64 0                ; &cloptr175733[0]
  %f175735 = ptrtoint void(i64,i64)* @lam174114 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175735, i64* %eptr175734                                               ; store fptr
  %arg172069 = ptrtoint i64* %cloptr175733 to i64                                    ; closure cast; i64* -> i64
  %cloptr175736 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr175738 = getelementptr inbounds i64, i64* %cloptr175736, i64 1                ; &eptr175738[1]
  %eptr175739 = getelementptr inbounds i64, i64* %cloptr175736, i64 2                ; &eptr175739[2]
  %eptr175740 = getelementptr inbounds i64, i64* %cloptr175736, i64 3                ; &eptr175740[3]
  %eptr175741 = getelementptr inbounds i64, i64* %cloptr175736, i64 4                ; &eptr175741[4]
  store i64 %c3d$_37wind_45stack, i64* %eptr175738                                   ; *eptr175738 = %c3d$_37wind_45stack
  store i64 %eKt$tail, i64* %eptr175739                                              ; *eptr175739 = %eKt$tail
  store i64 %TMl$new, i64* %eptr175740                                               ; *eptr175740 = %TMl$new
  store i64 %cont171427, i64* %eptr175741                                            ; *eptr175741 = %cont171427
  %eptr175737 = getelementptr inbounds i64, i64* %cloptr175736, i64 0                ; &cloptr175736[0]
  %f175742 = ptrtoint void(i64,i64)* @lam174110 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175742, i64* %eptr175737                                               ; store fptr
  %arg172068 = ptrtoint i64* %cloptr175736 to i64                                    ; closure cast; i64* -> i64
  %rva173167 = add i64 0, 0                                                          ; quoted ()
  %rva173166 = call i64 @prim_cons(i64 %arg172068, i64 %rva173167)                   ; call prim_cons
  %cloptr175743 = inttoptr i64 %arg172069 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175744 = getelementptr inbounds i64, i64* %cloptr175743, i64 0               ; &cloptr175743[0]
  %f175746 = load i64, i64* %i0ptr175744, align 8                                    ; load; *i0ptr175744
  %fptr175745 = inttoptr i64 %f175746 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175745(i64 %arg172069, i64 %rva173166)              ; tail call
  ret void
}


define void @lam174114(i64 %env174115, i64 %FPy$lst171449) {
  %cont171448 = call i64 @prim_car(i64 %FPy$lst171449)                               ; call prim_car
  %FPy$lst = call i64 @prim_cdr(i64 %FPy$lst171449)                                  ; call prim_cdr
  %arg172073 = add i64 0, 0                                                          ; quoted ()
  %rva173082 = add i64 0, 0                                                          ; quoted ()
  %rva173081 = call i64 @prim_cons(i64 %FPy$lst, i64 %rva173082)                     ; call prim_cons
  %rva173080 = call i64 @prim_cons(i64 %arg172073, i64 %rva173081)                   ; call prim_cons
  %cloptr175747 = inttoptr i64 %cont171448 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175748 = getelementptr inbounds i64, i64* %cloptr175747, i64 0               ; &cloptr175747[0]
  %f175750 = load i64, i64* %i0ptr175748, align 8                                    ; load; *i0ptr175748
  %fptr175749 = inttoptr i64 %f175750 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175749(i64 %cont171448, i64 %rva173080)             ; tail call
  ret void
}


define void @lam174110(i64 %env174111, i64 %rvp173165) {
  %envptr175751 = inttoptr i64 %env174111 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175752 = getelementptr inbounds i64, i64* %envptr175751, i64 4              ; &envptr175751[4]
  %cont171427 = load i64, i64* %envptr175752, align 8                                ; load; *envptr175752
  %envptr175753 = inttoptr i64 %env174111 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175754 = getelementptr inbounds i64, i64* %envptr175753, i64 3              ; &envptr175753[3]
  %TMl$new = load i64, i64* %envptr175754, align 8                                   ; load; *envptr175754
  %envptr175755 = inttoptr i64 %env174111 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175756 = getelementptr inbounds i64, i64* %envptr175755, i64 2              ; &envptr175755[2]
  %eKt$tail = load i64, i64* %envptr175756, align 8                                  ; load; *envptr175756
  %envptr175757 = inttoptr i64 %env174111 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175758 = getelementptr inbounds i64, i64* %envptr175757, i64 1              ; &envptr175757[1]
  %c3d$_37wind_45stack = load i64, i64* %envptr175758, align 8                       ; load; *envptr175758
  %_95171446 = call i64 @prim_car(i64 %rvp173165)                                    ; call prim_car
  %rvp173164 = call i64 @prim_cdr(i64 %rvp173165)                                    ; call prim_cdr
  %a171302 = call i64 @prim_car(i64 %rvp173164)                                      ; call prim_car
  %na173084 = call i64 @prim_cdr(i64 %rvp173164)                                     ; call prim_cdr
  %arg172076 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %retprim171447 = call i64 @prim_make_45vector(i64 %arg172076, i64 %a171302)        ; call prim_make_45vector
  %cloptr175759 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr175761 = getelementptr inbounds i64, i64* %cloptr175759, i64 1                ; &eptr175761[1]
  %eptr175762 = getelementptr inbounds i64, i64* %cloptr175759, i64 2                ; &eptr175762[2]
  %eptr175763 = getelementptr inbounds i64, i64* %cloptr175759, i64 3                ; &eptr175763[3]
  %eptr175764 = getelementptr inbounds i64, i64* %cloptr175759, i64 4                ; &eptr175764[4]
  store i64 %c3d$_37wind_45stack, i64* %eptr175761                                   ; *eptr175761 = %c3d$_37wind_45stack
  store i64 %eKt$tail, i64* %eptr175762                                              ; *eptr175762 = %eKt$tail
  store i64 %TMl$new, i64* %eptr175763                                               ; *eptr175763 = %TMl$new
  store i64 %cont171427, i64* %eptr175764                                            ; *eptr175764 = %cont171427
  %eptr175760 = getelementptr inbounds i64, i64* %cloptr175759, i64 0                ; &cloptr175759[0]
  %f175765 = ptrtoint void(i64,i64)* @lam174107 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175765, i64* %eptr175760                                               ; store fptr
  %arg172079 = ptrtoint i64* %cloptr175759 to i64                                    ; closure cast; i64* -> i64
  %arg172078 = add i64 0, 0                                                          ; quoted ()
  %rva173163 = add i64 0, 0                                                          ; quoted ()
  %rva173162 = call i64 @prim_cons(i64 %retprim171447, i64 %rva173163)               ; call prim_cons
  %rva173161 = call i64 @prim_cons(i64 %arg172078, i64 %rva173162)                   ; call prim_cons
  %cloptr175766 = inttoptr i64 %arg172079 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175767 = getelementptr inbounds i64, i64* %cloptr175766, i64 0               ; &cloptr175766[0]
  %f175769 = load i64, i64* %i0ptr175767, align 8                                    ; load; *i0ptr175767
  %fptr175768 = inttoptr i64 %f175769 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175768(i64 %arg172079, i64 %rva173161)              ; tail call
  ret void
}


define void @lam174107(i64 %env174108, i64 %rvp173160) {
  %envptr175770 = inttoptr i64 %env174108 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175771 = getelementptr inbounds i64, i64* %envptr175770, i64 4              ; &envptr175770[4]
  %cont171427 = load i64, i64* %envptr175771, align 8                                ; load; *envptr175771
  %envptr175772 = inttoptr i64 %env174108 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175773 = getelementptr inbounds i64, i64* %envptr175772, i64 3              ; &envptr175772[3]
  %TMl$new = load i64, i64* %envptr175773, align 8                                   ; load; *envptr175773
  %envptr175774 = inttoptr i64 %env174108 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175775 = getelementptr inbounds i64, i64* %envptr175774, i64 2              ; &envptr175774[2]
  %eKt$tail = load i64, i64* %envptr175775, align 8                                  ; load; *envptr175775
  %envptr175776 = inttoptr i64 %env174108 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175777 = getelementptr inbounds i64, i64* %envptr175776, i64 1              ; &envptr175776[1]
  %c3d$_37wind_45stack = load i64, i64* %envptr175777, align 8                       ; load; *envptr175777
  %_95171440 = call i64 @prim_car(i64 %rvp173160)                                    ; call prim_car
  %rvp173159 = call i64 @prim_cdr(i64 %rvp173160)                                    ; call prim_cdr
  %ROj$f = call i64 @prim_car(i64 %rvp173159)                                        ; call prim_car
  %na173086 = call i64 @prim_cdr(i64 %rvp173159)                                     ; call prim_cdr
  %arg172081 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %cloptr175778 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175780 = getelementptr inbounds i64, i64* %cloptr175778, i64 1                ; &eptr175780[1]
  %eptr175781 = getelementptr inbounds i64, i64* %cloptr175778, i64 2                ; &eptr175781[2]
  %eptr175782 = getelementptr inbounds i64, i64* %cloptr175778, i64 3                ; &eptr175782[3]
  store i64 %c3d$_37wind_45stack, i64* %eptr175780                                   ; *eptr175780 = %c3d$_37wind_45stack
  store i64 %eKt$tail, i64* %eptr175781                                              ; *eptr175781 = %eKt$tail
  store i64 %ROj$f, i64* %eptr175782                                                 ; *eptr175782 = %ROj$f
  %eptr175779 = getelementptr inbounds i64, i64* %cloptr175778, i64 0                ; &cloptr175778[0]
  %f175783 = ptrtoint void(i64,i64)* @lam174104 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175783, i64* %eptr175779                                               ; store fptr
  %arg172080 = ptrtoint i64* %cloptr175778 to i64                                    ; closure cast; i64* -> i64
  %O1L$_95171190 = call i64 @prim_vector_45set_33(i64 %ROj$f, i64 %arg172081, i64 %arg172080); call prim_vector_45set_33
  %arg172106 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171310 = call i64 @prim_vector_45ref(i64 %ROj$f, i64 %arg172106)                 ; call prim_vector_45ref
  %arg172108 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171311 = call i64 @prim_vector_45ref(i64 %c3d$_37wind_45stack, i64 %arg172108)   ; call prim_vector_45ref
  %cloptr175784 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr175786 = getelementptr inbounds i64, i64* %cloptr175784, i64 1                ; &eptr175786[1]
  %eptr175787 = getelementptr inbounds i64, i64* %cloptr175784, i64 2                ; &eptr175787[2]
  %eptr175788 = getelementptr inbounds i64, i64* %cloptr175784, i64 3                ; &eptr175788[3]
  %eptr175789 = getelementptr inbounds i64, i64* %cloptr175784, i64 4                ; &eptr175789[4]
  store i64 %c3d$_37wind_45stack, i64* %eptr175786                                   ; *eptr175786 = %c3d$_37wind_45stack
  store i64 %eKt$tail, i64* %eptr175787                                              ; *eptr175787 = %eKt$tail
  store i64 %TMl$new, i64* %eptr175788                                               ; *eptr175788 = %TMl$new
  store i64 %cont171427, i64* %eptr175789                                            ; *eptr175789 = %cont171427
  %eptr175785 = getelementptr inbounds i64, i64* %cloptr175784, i64 0                ; &cloptr175784[0]
  %f175790 = ptrtoint void(i64,i64)* @lam174088 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175790, i64* %eptr175785                                               ; store fptr
  %arg172111 = ptrtoint i64* %cloptr175784 to i64                                    ; closure cast; i64* -> i64
  %rva173158 = add i64 0, 0                                                          ; quoted ()
  %rva173157 = call i64 @prim_cons(i64 %a171311, i64 %rva173158)                     ; call prim_cons
  %rva173156 = call i64 @prim_cons(i64 %arg172111, i64 %rva173157)                   ; call prim_cons
  %cloptr175791 = inttoptr i64 %a171310 to i64*                                      ; closure/env cast; i64 -> i64*
  %i0ptr175792 = getelementptr inbounds i64, i64* %cloptr175791, i64 0               ; &cloptr175791[0]
  %f175794 = load i64, i64* %i0ptr175792, align 8                                    ; load; *i0ptr175792
  %fptr175793 = inttoptr i64 %f175794 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175793(i64 %a171310, i64 %rva173156)                ; tail call
  ret void
}


define void @lam174104(i64 %env174105, i64 %rvp173109) {
  %envptr175795 = inttoptr i64 %env174105 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175796 = getelementptr inbounds i64, i64* %envptr175795, i64 3              ; &envptr175795[3]
  %ROj$f = load i64, i64* %envptr175796, align 8                                     ; load; *envptr175796
  %envptr175797 = inttoptr i64 %env174105 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175798 = getelementptr inbounds i64, i64* %envptr175797, i64 2              ; &envptr175797[2]
  %eKt$tail = load i64, i64* %envptr175798, align 8                                  ; load; *envptr175798
  %envptr175799 = inttoptr i64 %env174105 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175800 = getelementptr inbounds i64, i64* %envptr175799, i64 1              ; &envptr175799[1]
  %c3d$_37wind_45stack = load i64, i64* %envptr175800, align 8                       ; load; *envptr175800
  %cont171441 = call i64 @prim_car(i64 %rvp173109)                                   ; call prim_car
  %rvp173108 = call i64 @prim_cdr(i64 %rvp173109)                                    ; call prim_cdr
  %mDB$l = call i64 @prim_car(i64 %rvp173108)                                        ; call prim_car
  %na173088 = call i64 @prim_cdr(i64 %rvp173108)                                     ; call prim_cdr
  %a171303 = call i64 @prim_eq_63(i64 %mDB$l, i64 %eKt$tail)                         ; call prim_eq_63
  %a171304 = call i64 @prim_not(i64 %a171303)                                        ; call prim_not
  %cmp175801 = icmp eq i64 %a171304, 15                                              ; false?
  br i1 %cmp175801, label %else175803, label %then175802                             ; if

then175802:
  %a171305 = call i64 @prim_cdr(i64 %mDB$l)                                          ; call prim_cdr
  %arg172088 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171444 = call i64 @prim_vector_45set_33(i64 %c3d$_37wind_45stack, i64 %arg172088, i64 %a171305); call prim_vector_45set_33
  %cloptr175804 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175806 = getelementptr inbounds i64, i64* %cloptr175804, i64 1                ; &eptr175806[1]
  %eptr175807 = getelementptr inbounds i64, i64* %cloptr175804, i64 2                ; &eptr175807[2]
  %eptr175808 = getelementptr inbounds i64, i64* %cloptr175804, i64 3                ; &eptr175808[3]
  store i64 %mDB$l, i64* %eptr175806                                                 ; *eptr175806 = %mDB$l
  store i64 %ROj$f, i64* %eptr175807                                                 ; *eptr175807 = %ROj$f
  store i64 %cont171441, i64* %eptr175808                                            ; *eptr175808 = %cont171441
  %eptr175805 = getelementptr inbounds i64, i64* %cloptr175804, i64 0                ; &cloptr175804[0]
  %f175809 = ptrtoint void(i64,i64)* @lam174099 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175809, i64* %eptr175805                                               ; store fptr
  %arg172092 = ptrtoint i64* %cloptr175804 to i64                                    ; closure cast; i64* -> i64
  %arg172091 = add i64 0, 0                                                          ; quoted ()
  %rva173104 = add i64 0, 0                                                          ; quoted ()
  %rva173103 = call i64 @prim_cons(i64 %retprim171444, i64 %rva173104)               ; call prim_cons
  %rva173102 = call i64 @prim_cons(i64 %arg172091, i64 %rva173103)                   ; call prim_cons
  %cloptr175810 = inttoptr i64 %arg172092 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175811 = getelementptr inbounds i64, i64* %cloptr175810, i64 0               ; &cloptr175810[0]
  %f175813 = load i64, i64* %i0ptr175811, align 8                                    ; load; *i0ptr175811
  %fptr175812 = inttoptr i64 %f175813 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175812(i64 %arg172092, i64 %rva173102)              ; tail call
  ret void

else175803:
  %retprim171445 = call i64 @prim_void()                                             ; call prim_void
  %arg172104 = add i64 0, 0                                                          ; quoted ()
  %rva173107 = add i64 0, 0                                                          ; quoted ()
  %rva173106 = call i64 @prim_cons(i64 %retprim171445, i64 %rva173107)               ; call prim_cons
  %rva173105 = call i64 @prim_cons(i64 %arg172104, i64 %rva173106)                   ; call prim_cons
  %cloptr175814 = inttoptr i64 %cont171441 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175815 = getelementptr inbounds i64, i64* %cloptr175814, i64 0               ; &cloptr175814[0]
  %f175817 = load i64, i64* %i0ptr175815, align 8                                    ; load; *i0ptr175815
  %fptr175816 = inttoptr i64 %f175817 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175816(i64 %cont171441, i64 %rva173105)             ; tail call
  ret void
}


define void @lam174099(i64 %env174100, i64 %rvp173101) {
  %envptr175818 = inttoptr i64 %env174100 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175819 = getelementptr inbounds i64, i64* %envptr175818, i64 3              ; &envptr175818[3]
  %cont171441 = load i64, i64* %envptr175819, align 8                                ; load; *envptr175819
  %envptr175820 = inttoptr i64 %env174100 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175821 = getelementptr inbounds i64, i64* %envptr175820, i64 2              ; &envptr175820[2]
  %ROj$f = load i64, i64* %envptr175821, align 8                                     ; load; *envptr175821
  %envptr175822 = inttoptr i64 %env174100 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175823 = getelementptr inbounds i64, i64* %envptr175822, i64 1              ; &envptr175822[1]
  %mDB$l = load i64, i64* %envptr175823, align 8                                     ; load; *envptr175823
  %_95171442 = call i64 @prim_car(i64 %rvp173101)                                    ; call prim_car
  %rvp173100 = call i64 @prim_cdr(i64 %rvp173101)                                    ; call prim_cdr
  %kqg$_95171191 = call i64 @prim_car(i64 %rvp173100)                                ; call prim_car
  %na173090 = call i64 @prim_cdr(i64 %rvp173100)                                     ; call prim_cdr
  %a171306 = call i64 @prim_car(i64 %mDB$l)                                          ; call prim_car
  %a171307 = call i64 @prim_cdr(i64 %a171306)                                        ; call prim_cdr
  %cloptr175824 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175826 = getelementptr inbounds i64, i64* %cloptr175824, i64 1                ; &eptr175826[1]
  %eptr175827 = getelementptr inbounds i64, i64* %cloptr175824, i64 2                ; &eptr175827[2]
  %eptr175828 = getelementptr inbounds i64, i64* %cloptr175824, i64 3                ; &eptr175828[3]
  store i64 %mDB$l, i64* %eptr175826                                                 ; *eptr175826 = %mDB$l
  store i64 %ROj$f, i64* %eptr175827                                                 ; *eptr175827 = %ROj$f
  store i64 %cont171441, i64* %eptr175828                                            ; *eptr175828 = %cont171441
  %eptr175825 = getelementptr inbounds i64, i64* %cloptr175824, i64 0                ; &cloptr175824[0]
  %f175829 = ptrtoint void(i64,i64)* @lam174097 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175829, i64* %eptr175825                                               ; store fptr
  %arg172095 = ptrtoint i64* %cloptr175824 to i64                                    ; closure cast; i64* -> i64
  %rva173099 = add i64 0, 0                                                          ; quoted ()
  %rva173098 = call i64 @prim_cons(i64 %arg172095, i64 %rva173099)                   ; call prim_cons
  %cloptr175830 = inttoptr i64 %a171307 to i64*                                      ; closure/env cast; i64 -> i64*
  %i0ptr175831 = getelementptr inbounds i64, i64* %cloptr175830, i64 0               ; &cloptr175830[0]
  %f175833 = load i64, i64* %i0ptr175831, align 8                                    ; load; *i0ptr175831
  %fptr175832 = inttoptr i64 %f175833 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175832(i64 %a171307, i64 %rva173098)                ; tail call
  ret void
}


define void @lam174097(i64 %env174098, i64 %rvp173097) {
  %envptr175834 = inttoptr i64 %env174098 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175835 = getelementptr inbounds i64, i64* %envptr175834, i64 3              ; &envptr175834[3]
  %cont171441 = load i64, i64* %envptr175835, align 8                                ; load; *envptr175835
  %envptr175836 = inttoptr i64 %env174098 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175837 = getelementptr inbounds i64, i64* %envptr175836, i64 2              ; &envptr175836[2]
  %ROj$f = load i64, i64* %envptr175837, align 8                                     ; load; *envptr175837
  %envptr175838 = inttoptr i64 %env174098 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175839 = getelementptr inbounds i64, i64* %envptr175838, i64 1              ; &envptr175838[1]
  %mDB$l = load i64, i64* %envptr175839, align 8                                     ; load; *envptr175839
  %_95171443 = call i64 @prim_car(i64 %rvp173097)                                    ; call prim_car
  %rvp173096 = call i64 @prim_cdr(i64 %rvp173097)                                    ; call prim_cdr
  %oUc$_95171192 = call i64 @prim_car(i64 %rvp173096)                                ; call prim_car
  %na173092 = call i64 @prim_cdr(i64 %rvp173096)                                     ; call prim_cdr
  %arg172097 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171308 = call i64 @prim_vector_45ref(i64 %ROj$f, i64 %arg172097)                 ; call prim_vector_45ref
  %a171309 = call i64 @prim_cdr(i64 %mDB$l)                                          ; call prim_cdr
  %rva173095 = add i64 0, 0                                                          ; quoted ()
  %rva173094 = call i64 @prim_cons(i64 %a171309, i64 %rva173095)                     ; call prim_cons
  %rva173093 = call i64 @prim_cons(i64 %cont171441, i64 %rva173094)                  ; call prim_cons
  %cloptr175840 = inttoptr i64 %a171308 to i64*                                      ; closure/env cast; i64 -> i64*
  %i0ptr175841 = getelementptr inbounds i64, i64* %cloptr175840, i64 0               ; &cloptr175840[0]
  %f175843 = load i64, i64* %i0ptr175841, align 8                                    ; load; *i0ptr175841
  %fptr175842 = inttoptr i64 %f175843 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175842(i64 %a171308, i64 %rva173093)                ; tail call
  ret void
}


define void @lam174088(i64 %env174089, i64 %rvp173155) {
  %envptr175844 = inttoptr i64 %env174089 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175845 = getelementptr inbounds i64, i64* %envptr175844, i64 4              ; &envptr175844[4]
  %cont171427 = load i64, i64* %envptr175845, align 8                                ; load; *envptr175845
  %envptr175846 = inttoptr i64 %env174089 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175847 = getelementptr inbounds i64, i64* %envptr175846, i64 3              ; &envptr175846[3]
  %TMl$new = load i64, i64* %envptr175847, align 8                                   ; load; *envptr175847
  %envptr175848 = inttoptr i64 %env174089 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175849 = getelementptr inbounds i64, i64* %envptr175848, i64 2              ; &envptr175848[2]
  %eKt$tail = load i64, i64* %envptr175849, align 8                                  ; load; *envptr175849
  %envptr175850 = inttoptr i64 %env174089 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175851 = getelementptr inbounds i64, i64* %envptr175850, i64 1              ; &envptr175850[1]
  %c3d$_37wind_45stack = load i64, i64* %envptr175851, align 8                       ; load; *envptr175851
  %_95171429 = call i64 @prim_car(i64 %rvp173155)                                    ; call prim_car
  %rvp173154 = call i64 @prim_cdr(i64 %rvp173155)                                    ; call prim_cdr
  %BC6$_95171189 = call i64 @prim_car(i64 %rvp173154)                                ; call prim_car
  %na173111 = call i64 @prim_cdr(i64 %rvp173154)                                     ; call prim_cdr
  %cloptr175852 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr175853 = getelementptr inbounds i64, i64* %cloptr175852, i64 0                ; &cloptr175852[0]
  %f175854 = ptrtoint void(i64,i64)* @lam174086 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175854, i64* %eptr175853                                               ; store fptr
  %arg172114 = ptrtoint i64* %cloptr175852 to i64                                    ; closure cast; i64* -> i64
  %cloptr175855 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr175857 = getelementptr inbounds i64, i64* %cloptr175855, i64 1                ; &eptr175857[1]
  %eptr175858 = getelementptr inbounds i64, i64* %cloptr175855, i64 2                ; &eptr175858[2]
  %eptr175859 = getelementptr inbounds i64, i64* %cloptr175855, i64 3                ; &eptr175859[3]
  %eptr175860 = getelementptr inbounds i64, i64* %cloptr175855, i64 4                ; &eptr175860[4]
  store i64 %c3d$_37wind_45stack, i64* %eptr175857                                   ; *eptr175857 = %c3d$_37wind_45stack
  store i64 %eKt$tail, i64* %eptr175858                                              ; *eptr175858 = %eKt$tail
  store i64 %TMl$new, i64* %eptr175859                                               ; *eptr175859 = %TMl$new
  store i64 %cont171427, i64* %eptr175860                                            ; *eptr175860 = %cont171427
  %eptr175856 = getelementptr inbounds i64, i64* %cloptr175855, i64 0                ; &cloptr175855[0]
  %f175861 = ptrtoint void(i64,i64)* @lam174082 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175861, i64* %eptr175856                                               ; store fptr
  %arg172113 = ptrtoint i64* %cloptr175855 to i64                                    ; closure cast; i64* -> i64
  %rva173153 = add i64 0, 0                                                          ; quoted ()
  %rva173152 = call i64 @prim_cons(i64 %arg172113, i64 %rva173153)                   ; call prim_cons
  %cloptr175862 = inttoptr i64 %arg172114 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175863 = getelementptr inbounds i64, i64* %cloptr175862, i64 0               ; &cloptr175862[0]
  %f175865 = load i64, i64* %i0ptr175863, align 8                                    ; load; *i0ptr175863
  %fptr175864 = inttoptr i64 %f175865 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175864(i64 %arg172114, i64 %rva173152)              ; tail call
  ret void
}


define void @lam174086(i64 %env174087, i64 %lMS$lst171439) {
  %cont171438 = call i64 @prim_car(i64 %lMS$lst171439)                               ; call prim_car
  %lMS$lst = call i64 @prim_cdr(i64 %lMS$lst171439)                                  ; call prim_cdr
  %arg172118 = add i64 0, 0                                                          ; quoted ()
  %rva173114 = add i64 0, 0                                                          ; quoted ()
  %rva173113 = call i64 @prim_cons(i64 %lMS$lst, i64 %rva173114)                     ; call prim_cons
  %rva173112 = call i64 @prim_cons(i64 %arg172118, i64 %rva173113)                   ; call prim_cons
  %cloptr175866 = inttoptr i64 %cont171438 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175867 = getelementptr inbounds i64, i64* %cloptr175866, i64 0               ; &cloptr175866[0]
  %f175869 = load i64, i64* %i0ptr175867, align 8                                    ; load; *i0ptr175867
  %fptr175868 = inttoptr i64 %f175869 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175868(i64 %cont171438, i64 %rva173112)             ; tail call
  ret void
}


define void @lam174082(i64 %env174083, i64 %rvp173151) {
  %envptr175870 = inttoptr i64 %env174083 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175871 = getelementptr inbounds i64, i64* %envptr175870, i64 4              ; &envptr175870[4]
  %cont171427 = load i64, i64* %envptr175871, align 8                                ; load; *envptr175871
  %envptr175872 = inttoptr i64 %env174083 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175873 = getelementptr inbounds i64, i64* %envptr175872, i64 3              ; &envptr175872[3]
  %TMl$new = load i64, i64* %envptr175873, align 8                                   ; load; *envptr175873
  %envptr175874 = inttoptr i64 %env174083 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175875 = getelementptr inbounds i64, i64* %envptr175874, i64 2              ; &envptr175874[2]
  %eKt$tail = load i64, i64* %envptr175875, align 8                                  ; load; *envptr175875
  %envptr175876 = inttoptr i64 %env174083 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175877 = getelementptr inbounds i64, i64* %envptr175876, i64 1              ; &envptr175876[1]
  %c3d$_37wind_45stack = load i64, i64* %envptr175877, align 8                       ; load; *envptr175877
  %_95171436 = call i64 @prim_car(i64 %rvp173151)                                    ; call prim_car
  %rvp173150 = call i64 @prim_cdr(i64 %rvp173151)                                    ; call prim_cdr
  %a171312 = call i64 @prim_car(i64 %rvp173150)                                      ; call prim_car
  %na173116 = call i64 @prim_cdr(i64 %rvp173150)                                     ; call prim_cdr
  %arg172121 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %retprim171437 = call i64 @prim_make_45vector(i64 %arg172121, i64 %a171312)        ; call prim_make_45vector
  %cloptr175878 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr175880 = getelementptr inbounds i64, i64* %cloptr175878, i64 1                ; &eptr175880[1]
  %eptr175881 = getelementptr inbounds i64, i64* %cloptr175878, i64 2                ; &eptr175881[2]
  %eptr175882 = getelementptr inbounds i64, i64* %cloptr175878, i64 3                ; &eptr175882[3]
  %eptr175883 = getelementptr inbounds i64, i64* %cloptr175878, i64 4                ; &eptr175883[4]
  store i64 %c3d$_37wind_45stack, i64* %eptr175880                                   ; *eptr175880 = %c3d$_37wind_45stack
  store i64 %eKt$tail, i64* %eptr175881                                              ; *eptr175881 = %eKt$tail
  store i64 %TMl$new, i64* %eptr175882                                               ; *eptr175882 = %TMl$new
  store i64 %cont171427, i64* %eptr175883                                            ; *eptr175883 = %cont171427
  %eptr175879 = getelementptr inbounds i64, i64* %cloptr175878, i64 0                ; &cloptr175878[0]
  %f175884 = ptrtoint void(i64,i64)* @lam174079 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175884, i64* %eptr175879                                               ; store fptr
  %arg172124 = ptrtoint i64* %cloptr175878 to i64                                    ; closure cast; i64* -> i64
  %arg172123 = add i64 0, 0                                                          ; quoted ()
  %rva173149 = add i64 0, 0                                                          ; quoted ()
  %rva173148 = call i64 @prim_cons(i64 %retprim171437, i64 %rva173149)               ; call prim_cons
  %rva173147 = call i64 @prim_cons(i64 %arg172123, i64 %rva173148)                   ; call prim_cons
  %cloptr175885 = inttoptr i64 %arg172124 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175886 = getelementptr inbounds i64, i64* %cloptr175885, i64 0               ; &cloptr175885[0]
  %f175888 = load i64, i64* %i0ptr175886, align 8                                    ; load; *i0ptr175886
  %fptr175887 = inttoptr i64 %f175888 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175887(i64 %arg172124, i64 %rva173147)              ; tail call
  ret void
}


define void @lam174079(i64 %env174080, i64 %rvp173146) {
  %envptr175889 = inttoptr i64 %env174080 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175890 = getelementptr inbounds i64, i64* %envptr175889, i64 4              ; &envptr175889[4]
  %cont171427 = load i64, i64* %envptr175890, align 8                                ; load; *envptr175890
  %envptr175891 = inttoptr i64 %env174080 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175892 = getelementptr inbounds i64, i64* %envptr175891, i64 3              ; &envptr175891[3]
  %TMl$new = load i64, i64* %envptr175892, align 8                                   ; load; *envptr175892
  %envptr175893 = inttoptr i64 %env174080 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175894 = getelementptr inbounds i64, i64* %envptr175893, i64 2              ; &envptr175893[2]
  %eKt$tail = load i64, i64* %envptr175894, align 8                                  ; load; *envptr175894
  %envptr175895 = inttoptr i64 %env174080 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175896 = getelementptr inbounds i64, i64* %envptr175895, i64 1              ; &envptr175895[1]
  %c3d$_37wind_45stack = load i64, i64* %envptr175896, align 8                       ; load; *envptr175896
  %_95171430 = call i64 @prim_car(i64 %rvp173146)                                    ; call prim_car
  %rvp173145 = call i64 @prim_cdr(i64 %rvp173146)                                    ; call prim_cdr
  %R3A$f = call i64 @prim_car(i64 %rvp173145)                                        ; call prim_car
  %na173118 = call i64 @prim_cdr(i64 %rvp173145)                                     ; call prim_cdr
  %arg172126 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %cloptr175897 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175899 = getelementptr inbounds i64, i64* %cloptr175897, i64 1                ; &eptr175899[1]
  %eptr175900 = getelementptr inbounds i64, i64* %cloptr175897, i64 2                ; &eptr175900[2]
  %eptr175901 = getelementptr inbounds i64, i64* %cloptr175897, i64 3                ; &eptr175901[3]
  store i64 %c3d$_37wind_45stack, i64* %eptr175899                                   ; *eptr175899 = %c3d$_37wind_45stack
  store i64 %eKt$tail, i64* %eptr175900                                              ; *eptr175900 = %eKt$tail
  store i64 %R3A$f, i64* %eptr175901                                                 ; *eptr175901 = %R3A$f
  %eptr175898 = getelementptr inbounds i64, i64* %cloptr175897, i64 0                ; &cloptr175897[0]
  %f175902 = ptrtoint void(i64,i64)* @lam174076 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175902, i64* %eptr175898                                               ; store fptr
  %arg172125 = ptrtoint i64* %cloptr175897 to i64                                    ; closure cast; i64* -> i64
  %bUY$_95171193 = call i64 @prim_vector_45set_33(i64 %R3A$f, i64 %arg172126, i64 %arg172125); call prim_vector_45set_33
  %arg172150 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171319 = call i64 @prim_vector_45ref(i64 %R3A$f, i64 %arg172150)                 ; call prim_vector_45ref
  %rva173144 = add i64 0, 0                                                          ; quoted ()
  %rva173143 = call i64 @prim_cons(i64 %TMl$new, i64 %rva173144)                     ; call prim_cons
  %rva173142 = call i64 @prim_cons(i64 %cont171427, i64 %rva173143)                  ; call prim_cons
  %cloptr175903 = inttoptr i64 %a171319 to i64*                                      ; closure/env cast; i64 -> i64*
  %i0ptr175904 = getelementptr inbounds i64, i64* %cloptr175903, i64 0               ; &cloptr175903[0]
  %f175906 = load i64, i64* %i0ptr175904, align 8                                    ; load; *i0ptr175904
  %fptr175905 = inttoptr i64 %f175906 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175905(i64 %a171319, i64 %rva173142)                ; tail call
  ret void
}


define void @lam174076(i64 %env174077, i64 %rvp173141) {
  %envptr175907 = inttoptr i64 %env174077 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175908 = getelementptr inbounds i64, i64* %envptr175907, i64 3              ; &envptr175907[3]
  %R3A$f = load i64, i64* %envptr175908, align 8                                     ; load; *envptr175908
  %envptr175909 = inttoptr i64 %env174077 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175910 = getelementptr inbounds i64, i64* %envptr175909, i64 2              ; &envptr175909[2]
  %eKt$tail = load i64, i64* %envptr175910, align 8                                  ; load; *envptr175910
  %envptr175911 = inttoptr i64 %env174077 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175912 = getelementptr inbounds i64, i64* %envptr175911, i64 1              ; &envptr175911[1]
  %c3d$_37wind_45stack = load i64, i64* %envptr175912, align 8                       ; load; *envptr175912
  %cont171431 = call i64 @prim_car(i64 %rvp173141)                                   ; call prim_car
  %rvp173140 = call i64 @prim_cdr(i64 %rvp173141)                                    ; call prim_cdr
  %SWJ$l = call i64 @prim_car(i64 %rvp173140)                                        ; call prim_car
  %na173120 = call i64 @prim_cdr(i64 %rvp173140)                                     ; call prim_cdr
  %a171313 = call i64 @prim_eq_63(i64 %SWJ$l, i64 %eKt$tail)                         ; call prim_eq_63
  %a171314 = call i64 @prim_not(i64 %a171313)                                        ; call prim_not
  %cmp175913 = icmp eq i64 %a171314, 15                                              ; false?
  br i1 %cmp175913, label %else175915, label %then175914                             ; if

then175914:
  %arg172131 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171315 = call i64 @prim_vector_45ref(i64 %R3A$f, i64 %arg172131)                 ; call prim_vector_45ref
  %a171316 = call i64 @prim_cdr(i64 %SWJ$l)                                          ; call prim_cdr
  %cloptr175916 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175918 = getelementptr inbounds i64, i64* %cloptr175916, i64 1                ; &eptr175918[1]
  %eptr175919 = getelementptr inbounds i64, i64* %cloptr175916, i64 2                ; &eptr175919[2]
  %eptr175920 = getelementptr inbounds i64, i64* %cloptr175916, i64 3                ; &eptr175920[3]
  store i64 %SWJ$l, i64* %eptr175918                                                 ; *eptr175918 = %SWJ$l
  store i64 %c3d$_37wind_45stack, i64* %eptr175919                                   ; *eptr175919 = %c3d$_37wind_45stack
  store i64 %cont171431, i64* %eptr175920                                            ; *eptr175920 = %cont171431
  %eptr175917 = getelementptr inbounds i64, i64* %cloptr175916, i64 0                ; &cloptr175916[0]
  %f175921 = ptrtoint void(i64,i64)* @lam174071 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175921, i64* %eptr175917                                               ; store fptr
  %arg172135 = ptrtoint i64* %cloptr175916 to i64                                    ; closure cast; i64* -> i64
  %rva173136 = add i64 0, 0                                                          ; quoted ()
  %rva173135 = call i64 @prim_cons(i64 %a171316, i64 %rva173136)                     ; call prim_cons
  %rva173134 = call i64 @prim_cons(i64 %arg172135, i64 %rva173135)                   ; call prim_cons
  %cloptr175922 = inttoptr i64 %a171315 to i64*                                      ; closure/env cast; i64 -> i64*
  %i0ptr175923 = getelementptr inbounds i64, i64* %cloptr175922, i64 0               ; &cloptr175922[0]
  %f175925 = load i64, i64* %i0ptr175923, align 8                                    ; load; *i0ptr175923
  %fptr175924 = inttoptr i64 %f175925 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175924(i64 %a171315, i64 %rva173134)                ; tail call
  ret void

else175915:
  %retprim171435 = call i64 @prim_void()                                             ; call prim_void
  %arg172148 = add i64 0, 0                                                          ; quoted ()
  %rva173139 = add i64 0, 0                                                          ; quoted ()
  %rva173138 = call i64 @prim_cons(i64 %retprim171435, i64 %rva173139)               ; call prim_cons
  %rva173137 = call i64 @prim_cons(i64 %arg172148, i64 %rva173138)                   ; call prim_cons
  %cloptr175926 = inttoptr i64 %cont171431 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175927 = getelementptr inbounds i64, i64* %cloptr175926, i64 0               ; &cloptr175926[0]
  %f175929 = load i64, i64* %i0ptr175927, align 8                                    ; load; *i0ptr175927
  %fptr175928 = inttoptr i64 %f175929 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175928(i64 %cont171431, i64 %rva173137)             ; tail call
  ret void
}


define void @lam174071(i64 %env174072, i64 %rvp173133) {
  %envptr175930 = inttoptr i64 %env174072 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175931 = getelementptr inbounds i64, i64* %envptr175930, i64 3              ; &envptr175930[3]
  %cont171431 = load i64, i64* %envptr175931, align 8                                ; load; *envptr175931
  %envptr175932 = inttoptr i64 %env174072 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175933 = getelementptr inbounds i64, i64* %envptr175932, i64 2              ; &envptr175932[2]
  %c3d$_37wind_45stack = load i64, i64* %envptr175933, align 8                       ; load; *envptr175933
  %envptr175934 = inttoptr i64 %env174072 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175935 = getelementptr inbounds i64, i64* %envptr175934, i64 1              ; &envptr175934[1]
  %SWJ$l = load i64, i64* %envptr175935, align 8                                     ; load; *envptr175935
  %_95171432 = call i64 @prim_car(i64 %rvp173133)                                    ; call prim_car
  %rvp173132 = call i64 @prim_cdr(i64 %rvp173133)                                    ; call prim_cdr
  %T8G$_95171194 = call i64 @prim_car(i64 %rvp173132)                                ; call prim_car
  %na173122 = call i64 @prim_cdr(i64 %rvp173132)                                     ; call prim_cdr
  %a171317 = call i64 @prim_car(i64 %SWJ$l)                                          ; call prim_car
  %a171318 = call i64 @prim_car(i64 %a171317)                                        ; call prim_car
  %cloptr175936 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr175938 = getelementptr inbounds i64, i64* %cloptr175936, i64 1                ; &eptr175938[1]
  %eptr175939 = getelementptr inbounds i64, i64* %cloptr175936, i64 2                ; &eptr175939[2]
  %eptr175940 = getelementptr inbounds i64, i64* %cloptr175936, i64 3                ; &eptr175940[3]
  store i64 %SWJ$l, i64* %eptr175938                                                 ; *eptr175938 = %SWJ$l
  store i64 %c3d$_37wind_45stack, i64* %eptr175939                                   ; *eptr175939 = %c3d$_37wind_45stack
  store i64 %cont171431, i64* %eptr175940                                            ; *eptr175940 = %cont171431
  %eptr175937 = getelementptr inbounds i64, i64* %cloptr175936, i64 0                ; &cloptr175936[0]
  %f175941 = ptrtoint void(i64,i64)* @lam174069 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175941, i64* %eptr175937                                               ; store fptr
  %arg172139 = ptrtoint i64* %cloptr175936 to i64                                    ; closure cast; i64* -> i64
  %rva173131 = add i64 0, 0                                                          ; quoted ()
  %rva173130 = call i64 @prim_cons(i64 %arg172139, i64 %rva173131)                   ; call prim_cons
  %cloptr175942 = inttoptr i64 %a171318 to i64*                                      ; closure/env cast; i64 -> i64*
  %i0ptr175943 = getelementptr inbounds i64, i64* %cloptr175942, i64 0               ; &cloptr175942[0]
  %f175945 = load i64, i64* %i0ptr175943, align 8                                    ; load; *i0ptr175943
  %fptr175944 = inttoptr i64 %f175945 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175944(i64 %a171318, i64 %rva173130)                ; tail call
  ret void
}


define void @lam174069(i64 %env174070, i64 %rvp173129) {
  %envptr175946 = inttoptr i64 %env174070 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175947 = getelementptr inbounds i64, i64* %envptr175946, i64 3              ; &envptr175946[3]
  %cont171431 = load i64, i64* %envptr175947, align 8                                ; load; *envptr175947
  %envptr175948 = inttoptr i64 %env174070 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175949 = getelementptr inbounds i64, i64* %envptr175948, i64 2              ; &envptr175948[2]
  %c3d$_37wind_45stack = load i64, i64* %envptr175949, align 8                       ; load; *envptr175949
  %envptr175950 = inttoptr i64 %env174070 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175951 = getelementptr inbounds i64, i64* %envptr175950, i64 1              ; &envptr175950[1]
  %SWJ$l = load i64, i64* %envptr175951, align 8                                     ; load; *envptr175951
  %_95171433 = call i64 @prim_car(i64 %rvp173129)                                    ; call prim_car
  %rvp173128 = call i64 @prim_cdr(i64 %rvp173129)                                    ; call prim_cdr
  %Ghq$_95171195 = call i64 @prim_car(i64 %rvp173128)                                ; call prim_car
  %na173124 = call i64 @prim_cdr(i64 %rvp173128)                                     ; call prim_cdr
  %arg172142 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171434 = call i64 @prim_vector_45set_33(i64 %c3d$_37wind_45stack, i64 %arg172142, i64 %SWJ$l); call prim_vector_45set_33
  %arg172145 = add i64 0, 0                                                          ; quoted ()
  %rva173127 = add i64 0, 0                                                          ; quoted ()
  %rva173126 = call i64 @prim_cons(i64 %retprim171434, i64 %rva173127)               ; call prim_cons
  %rva173125 = call i64 @prim_cons(i64 %arg172145, i64 %rva173126)                   ; call prim_cons
  %cloptr175952 = inttoptr i64 %cont171431 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175953 = getelementptr inbounds i64, i64* %cloptr175952, i64 0               ; &cloptr175952[0]
  %f175955 = load i64, i64* %i0ptr175953, align 8                                    ; load; *i0ptr175953
  %fptr175954 = inttoptr i64 %f175955 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175954(i64 %cont171431, i64 %rva173125)             ; tail call
  ret void
}


define void @lam174052(i64 %env174053, i64 %MUv$lst171488) {
  %cont171487 = call i64 @prim_car(i64 %MUv$lst171488)                               ; call prim_car
  %MUv$lst = call i64 @prim_cdr(i64 %MUv$lst171488)                                  ; call prim_cdr
  %arg172160 = add i64 0, 0                                                          ; quoted ()
  %rva173178 = add i64 0, 0                                                          ; quoted ()
  %rva173177 = call i64 @prim_cons(i64 %MUv$lst, i64 %rva173178)                     ; call prim_cons
  %rva173176 = call i64 @prim_cons(i64 %arg172160, i64 %rva173177)                   ; call prim_cons
  %cloptr175956 = inttoptr i64 %cont171487 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175957 = getelementptr inbounds i64, i64* %cloptr175956, i64 0               ; &cloptr175956[0]
  %f175959 = load i64, i64* %i0ptr175957, align 8                                    ; load; *i0ptr175957
  %fptr175958 = inttoptr i64 %f175959 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175958(i64 %cont171487, i64 %rva173176)             ; tail call
  ret void
}


define void @lam174048(i64 %env174049, i64 %rvp173310) {
  %_95171485 = call i64 @prim_car(i64 %rvp173310)                                    ; call prim_car
  %rvp173309 = call i64 @prim_cdr(i64 %rvp173310)                                    ; call prim_cdr
  %a171320 = call i64 @prim_car(i64 %rvp173309)                                      ; call prim_car
  %na173180 = call i64 @prim_cdr(i64 %rvp173309)                                     ; call prim_cdr
  %arg172163 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %retprim171486 = call i64 @prim_make_45vector(i64 %arg172163, i64 %a171320)        ; call prim_make_45vector
  %cloptr175960 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr175961 = getelementptr inbounds i64, i64* %cloptr175960, i64 0                ; &cloptr175960[0]
  %f175962 = ptrtoint void(i64,i64)* @lam174045 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175962, i64* %eptr175961                                               ; store fptr
  %arg172166 = ptrtoint i64* %cloptr175960 to i64                                    ; closure cast; i64* -> i64
  %arg172165 = add i64 0, 0                                                          ; quoted ()
  %rva173308 = add i64 0, 0                                                          ; quoted ()
  %rva173307 = call i64 @prim_cons(i64 %retprim171486, i64 %rva173308)               ; call prim_cons
  %rva173306 = call i64 @prim_cons(i64 %arg172165, i64 %rva173307)                   ; call prim_cons
  %cloptr175963 = inttoptr i64 %arg172166 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175964 = getelementptr inbounds i64, i64* %cloptr175963, i64 0               ; &cloptr175963[0]
  %f175966 = load i64, i64* %i0ptr175964, align 8                                    ; load; *i0ptr175964
  %fptr175965 = inttoptr i64 %f175966 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175965(i64 %arg172166, i64 %rva173306)              ; tail call
  ret void
}


define void @lam174045(i64 %env174046, i64 %rvp173305) {
  %_95171450 = call i64 @prim_car(i64 %rvp173305)                                    ; call prim_car
  %rvp173304 = call i64 @prim_cdr(i64 %rvp173305)                                    ; call prim_cdr
  %Lyz$x = call i64 @prim_car(i64 %rvp173304)                                        ; call prim_car
  %na173182 = call i64 @prim_cdr(i64 %rvp173304)                                     ; call prim_cdr
  %cloptr175967 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr175968 = getelementptr inbounds i64, i64* %cloptr175967, i64 0                ; &cloptr175967[0]
  %f175969 = ptrtoint void(i64,i64)* @lam174043 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175969, i64* %eptr175968                                               ; store fptr
  %arg172168 = ptrtoint i64* %cloptr175967 to i64                                    ; closure cast; i64* -> i64
  %cloptr175970 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr175972 = getelementptr inbounds i64, i64* %cloptr175970, i64 1                ; &eptr175972[1]
  store i64 %Lyz$x, i64* %eptr175972                                                 ; *eptr175972 = %Lyz$x
  %eptr175971 = getelementptr inbounds i64, i64* %cloptr175970, i64 0                ; &cloptr175970[0]
  %f175973 = ptrtoint void(i64,i64)* @lam174039 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175973, i64* %eptr175971                                               ; store fptr
  %arg172167 = ptrtoint i64* %cloptr175970 to i64                                    ; closure cast; i64* -> i64
  %rva173303 = add i64 0, 0                                                          ; quoted ()
  %rva173302 = call i64 @prim_cons(i64 %arg172167, i64 %rva173303)                   ; call prim_cons
  %cloptr175974 = inttoptr i64 %arg172168 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175975 = getelementptr inbounds i64, i64* %cloptr175974, i64 0               ; &cloptr175974[0]
  %f175977 = load i64, i64* %i0ptr175975, align 8                                    ; load; *i0ptr175975
  %fptr175976 = inttoptr i64 %f175977 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175976(i64 %arg172168, i64 %rva173302)              ; tail call
  ret void
}


define void @lam174043(i64 %env174044, i64 %Pwg$lst171484) {
  %cont171483 = call i64 @prim_car(i64 %Pwg$lst171484)                               ; call prim_car
  %Pwg$lst = call i64 @prim_cdr(i64 %Pwg$lst171484)                                  ; call prim_cdr
  %arg172172 = add i64 0, 0                                                          ; quoted ()
  %rva173185 = add i64 0, 0                                                          ; quoted ()
  %rva173184 = call i64 @prim_cons(i64 %Pwg$lst, i64 %rva173185)                     ; call prim_cons
  %rva173183 = call i64 @prim_cons(i64 %arg172172, i64 %rva173184)                   ; call prim_cons
  %cloptr175978 = inttoptr i64 %cont171483 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr175979 = getelementptr inbounds i64, i64* %cloptr175978, i64 0               ; &cloptr175978[0]
  %f175981 = load i64, i64* %i0ptr175979, align 8                                    ; load; *i0ptr175979
  %fptr175980 = inttoptr i64 %f175981 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175980(i64 %cont171483, i64 %rva173183)             ; tail call
  ret void
}


define void @lam174039(i64 %env174040, i64 %rvp173301) {
  %envptr175982 = inttoptr i64 %env174040 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175983 = getelementptr inbounds i64, i64* %envptr175982, i64 1              ; &envptr175982[1]
  %Lyz$x = load i64, i64* %envptr175983, align 8                                     ; load; *envptr175983
  %_95171481 = call i64 @prim_car(i64 %rvp173301)                                    ; call prim_car
  %rvp173300 = call i64 @prim_cdr(i64 %rvp173301)                                    ; call prim_cdr
  %a171321 = call i64 @prim_car(i64 %rvp173300)                                      ; call prim_car
  %na173187 = call i64 @prim_cdr(i64 %rvp173300)                                     ; call prim_cdr
  %arg172175 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %retprim171482 = call i64 @prim_make_45vector(i64 %arg172175, i64 %a171321)        ; call prim_make_45vector
  %cloptr175984 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr175986 = getelementptr inbounds i64, i64* %cloptr175984, i64 1                ; &eptr175986[1]
  store i64 %Lyz$x, i64* %eptr175986                                                 ; *eptr175986 = %Lyz$x
  %eptr175985 = getelementptr inbounds i64, i64* %cloptr175984, i64 0                ; &cloptr175984[0]
  %f175987 = ptrtoint void(i64,i64)* @lam174036 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175987, i64* %eptr175985                                               ; store fptr
  %arg172178 = ptrtoint i64* %cloptr175984 to i64                                    ; closure cast; i64* -> i64
  %arg172177 = add i64 0, 0                                                          ; quoted ()
  %rva173299 = add i64 0, 0                                                          ; quoted ()
  %rva173298 = call i64 @prim_cons(i64 %retprim171482, i64 %rva173299)               ; call prim_cons
  %rva173297 = call i64 @prim_cons(i64 %arg172177, i64 %rva173298)                   ; call prim_cons
  %cloptr175988 = inttoptr i64 %arg172178 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr175989 = getelementptr inbounds i64, i64* %cloptr175988, i64 0               ; &cloptr175988[0]
  %f175991 = load i64, i64* %i0ptr175989, align 8                                    ; load; *i0ptr175989
  %fptr175990 = inttoptr i64 %f175991 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr175990(i64 %arg172178, i64 %rva173297)              ; tail call
  ret void
}


define void @lam174036(i64 %env174037, i64 %rvp173296) {
  %envptr175992 = inttoptr i64 %env174037 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr175993 = getelementptr inbounds i64, i64* %envptr175992, i64 1              ; &envptr175992[1]
  %Lyz$x = load i64, i64* %envptr175993, align 8                                     ; load; *envptr175993
  %_95171451 = call i64 @prim_car(i64 %rvp173296)                                    ; call prim_car
  %rvp173295 = call i64 @prim_cdr(i64 %rvp173296)                                    ; call prim_cdr
  %Nvg$a = call i64 @prim_car(i64 %rvp173295)                                        ; call prim_car
  %na173189 = call i64 @prim_cdr(i64 %rvp173295)                                     ; call prim_cdr
  %cloptr175994 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr175995 = getelementptr inbounds i64, i64* %cloptr175994, i64 0                ; &cloptr175994[0]
  %f175996 = ptrtoint void(i64,i64)* @lam174034 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f175996, i64* %eptr175995                                               ; store fptr
  %arg172180 = ptrtoint i64* %cloptr175994 to i64                                    ; closure cast; i64* -> i64
  %cloptr175997 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr175999 = getelementptr inbounds i64, i64* %cloptr175997, i64 1                ; &eptr175999[1]
  %eptr176000 = getelementptr inbounds i64, i64* %cloptr175997, i64 2                ; &eptr176000[2]
  store i64 %Lyz$x, i64* %eptr175999                                                 ; *eptr175999 = %Lyz$x
  store i64 %Nvg$a, i64* %eptr176000                                                 ; *eptr176000 = %Nvg$a
  %eptr175998 = getelementptr inbounds i64, i64* %cloptr175997, i64 0                ; &cloptr175997[0]
  %f176001 = ptrtoint void(i64,i64)* @lam174030 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176001, i64* %eptr175998                                               ; store fptr
  %arg172179 = ptrtoint i64* %cloptr175997 to i64                                    ; closure cast; i64* -> i64
  %rva173294 = add i64 0, 0                                                          ; quoted ()
  %rva173293 = call i64 @prim_cons(i64 %arg172179, i64 %rva173294)                   ; call prim_cons
  %cloptr176002 = inttoptr i64 %arg172180 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176003 = getelementptr inbounds i64, i64* %cloptr176002, i64 0               ; &cloptr176002[0]
  %f176005 = load i64, i64* %i0ptr176003, align 8                                    ; load; *i0ptr176003
  %fptr176004 = inttoptr i64 %f176005 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176004(i64 %arg172180, i64 %rva173293)              ; tail call
  ret void
}


define void @lam174034(i64 %env174035, i64 %jFA$lst171480) {
  %cont171479 = call i64 @prim_car(i64 %jFA$lst171480)                               ; call prim_car
  %jFA$lst = call i64 @prim_cdr(i64 %jFA$lst171480)                                  ; call prim_cdr
  %arg172184 = add i64 0, 0                                                          ; quoted ()
  %rva173192 = add i64 0, 0                                                          ; quoted ()
  %rva173191 = call i64 @prim_cons(i64 %jFA$lst, i64 %rva173192)                     ; call prim_cons
  %rva173190 = call i64 @prim_cons(i64 %arg172184, i64 %rva173191)                   ; call prim_cons
  %cloptr176006 = inttoptr i64 %cont171479 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176007 = getelementptr inbounds i64, i64* %cloptr176006, i64 0               ; &cloptr176006[0]
  %f176009 = load i64, i64* %i0ptr176007, align 8                                    ; load; *i0ptr176007
  %fptr176008 = inttoptr i64 %f176009 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176008(i64 %cont171479, i64 %rva173190)             ; tail call
  ret void
}


define void @lam174030(i64 %env174031, i64 %rvp173292) {
  %envptr176010 = inttoptr i64 %env174031 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176011 = getelementptr inbounds i64, i64* %envptr176010, i64 2              ; &envptr176010[2]
  %Nvg$a = load i64, i64* %envptr176011, align 8                                     ; load; *envptr176011
  %envptr176012 = inttoptr i64 %env174031 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176013 = getelementptr inbounds i64, i64* %envptr176012, i64 1              ; &envptr176012[1]
  %Lyz$x = load i64, i64* %envptr176013, align 8                                     ; load; *envptr176013
  %_95171477 = call i64 @prim_car(i64 %rvp173292)                                    ; call prim_car
  %rvp173291 = call i64 @prim_cdr(i64 %rvp173292)                                    ; call prim_cdr
  %a171322 = call i64 @prim_car(i64 %rvp173291)                                      ; call prim_car
  %na173194 = call i64 @prim_cdr(i64 %rvp173291)                                     ; call prim_cdr
  %arg172187 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %retprim171478 = call i64 @prim_make_45vector(i64 %arg172187, i64 %a171322)        ; call prim_make_45vector
  %cloptr176014 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr176016 = getelementptr inbounds i64, i64* %cloptr176014, i64 1                ; &eptr176016[1]
  %eptr176017 = getelementptr inbounds i64, i64* %cloptr176014, i64 2                ; &eptr176017[2]
  store i64 %Lyz$x, i64* %eptr176016                                                 ; *eptr176016 = %Lyz$x
  store i64 %Nvg$a, i64* %eptr176017                                                 ; *eptr176017 = %Nvg$a
  %eptr176015 = getelementptr inbounds i64, i64* %cloptr176014, i64 0                ; &cloptr176014[0]
  %f176018 = ptrtoint void(i64,i64)* @lam174027 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176018, i64* %eptr176015                                               ; store fptr
  %arg172190 = ptrtoint i64* %cloptr176014 to i64                                    ; closure cast; i64* -> i64
  %arg172189 = add i64 0, 0                                                          ; quoted ()
  %rva173290 = add i64 0, 0                                                          ; quoted ()
  %rva173289 = call i64 @prim_cons(i64 %retprim171478, i64 %rva173290)               ; call prim_cons
  %rva173288 = call i64 @prim_cons(i64 %arg172189, i64 %rva173289)                   ; call prim_cons
  %cloptr176019 = inttoptr i64 %arg172190 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176020 = getelementptr inbounds i64, i64* %cloptr176019, i64 0               ; &cloptr176019[0]
  %f176022 = load i64, i64* %i0ptr176020, align 8                                    ; load; *i0ptr176020
  %fptr176021 = inttoptr i64 %f176022 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176021(i64 %arg172190, i64 %rva173288)              ; tail call
  ret void
}


define void @lam174027(i64 %env174028, i64 %rvp173287) {
  %envptr176023 = inttoptr i64 %env174028 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176024 = getelementptr inbounds i64, i64* %envptr176023, i64 2              ; &envptr176023[2]
  %Nvg$a = load i64, i64* %envptr176024, align 8                                     ; load; *envptr176024
  %envptr176025 = inttoptr i64 %env174028 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176026 = getelementptr inbounds i64, i64* %envptr176025, i64 1              ; &envptr176025[1]
  %Lyz$x = load i64, i64* %envptr176026, align 8                                     ; load; *envptr176026
  %_95171452 = call i64 @prim_car(i64 %rvp173287)                                    ; call prim_car
  %rvp173286 = call i64 @prim_cdr(i64 %rvp173287)                                    ; call prim_cdr
  %M9T$b = call i64 @prim_car(i64 %rvp173286)                                        ; call prim_car
  %na173196 = call i64 @prim_cdr(i64 %rvp173286)                                     ; call prim_cdr
  %cloptr176027 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr176028 = getelementptr inbounds i64, i64* %cloptr176027, i64 0                ; &cloptr176027[0]
  %f176029 = ptrtoint void(i64,i64)* @lam174025 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176029, i64* %eptr176028                                               ; store fptr
  %arg172192 = ptrtoint i64* %cloptr176027 to i64                                    ; closure cast; i64* -> i64
  %cloptr176030 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr176032 = getelementptr inbounds i64, i64* %cloptr176030, i64 1                ; &eptr176032[1]
  %eptr176033 = getelementptr inbounds i64, i64* %cloptr176030, i64 2                ; &eptr176033[2]
  %eptr176034 = getelementptr inbounds i64, i64* %cloptr176030, i64 3                ; &eptr176034[3]
  store i64 %Lyz$x, i64* %eptr176032                                                 ; *eptr176032 = %Lyz$x
  store i64 %Nvg$a, i64* %eptr176033                                                 ; *eptr176033 = %Nvg$a
  store i64 %M9T$b, i64* %eptr176034                                                 ; *eptr176034 = %M9T$b
  %eptr176031 = getelementptr inbounds i64, i64* %cloptr176030, i64 0                ; &cloptr176030[0]
  %f176035 = ptrtoint void(i64,i64)* @lam174021 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176035, i64* %eptr176031                                               ; store fptr
  %arg172191 = ptrtoint i64* %cloptr176030 to i64                                    ; closure cast; i64* -> i64
  %rva173285 = add i64 0, 0                                                          ; quoted ()
  %rva173284 = call i64 @prim_cons(i64 %arg172191, i64 %rva173285)                   ; call prim_cons
  %cloptr176036 = inttoptr i64 %arg172192 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176037 = getelementptr inbounds i64, i64* %cloptr176036, i64 0               ; &cloptr176036[0]
  %f176039 = load i64, i64* %i0ptr176037, align 8                                    ; load; *i0ptr176037
  %fptr176038 = inttoptr i64 %f176039 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176038(i64 %arg172192, i64 %rva173284)              ; tail call
  ret void
}


define void @lam174025(i64 %env174026, i64 %rOg$lst171476) {
  %cont171475 = call i64 @prim_car(i64 %rOg$lst171476)                               ; call prim_car
  %rOg$lst = call i64 @prim_cdr(i64 %rOg$lst171476)                                  ; call prim_cdr
  %arg172196 = add i64 0, 0                                                          ; quoted ()
  %rva173199 = add i64 0, 0                                                          ; quoted ()
  %rva173198 = call i64 @prim_cons(i64 %rOg$lst, i64 %rva173199)                     ; call prim_cons
  %rva173197 = call i64 @prim_cons(i64 %arg172196, i64 %rva173198)                   ; call prim_cons
  %cloptr176040 = inttoptr i64 %cont171475 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176041 = getelementptr inbounds i64, i64* %cloptr176040, i64 0               ; &cloptr176040[0]
  %f176043 = load i64, i64* %i0ptr176041, align 8                                    ; load; *i0ptr176041
  %fptr176042 = inttoptr i64 %f176043 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176042(i64 %cont171475, i64 %rva173197)             ; tail call
  ret void
}


define void @lam174021(i64 %env174022, i64 %rvp173283) {
  %envptr176044 = inttoptr i64 %env174022 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176045 = getelementptr inbounds i64, i64* %envptr176044, i64 3              ; &envptr176044[3]
  %M9T$b = load i64, i64* %envptr176045, align 8                                     ; load; *envptr176045
  %envptr176046 = inttoptr i64 %env174022 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176047 = getelementptr inbounds i64, i64* %envptr176046, i64 2              ; &envptr176046[2]
  %Nvg$a = load i64, i64* %envptr176047, align 8                                     ; load; *envptr176047
  %envptr176048 = inttoptr i64 %env174022 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176049 = getelementptr inbounds i64, i64* %envptr176048, i64 1              ; &envptr176048[1]
  %Lyz$x = load i64, i64* %envptr176049, align 8                                     ; load; *envptr176049
  %_95171473 = call i64 @prim_car(i64 %rvp173283)                                    ; call prim_car
  %rvp173282 = call i64 @prim_cdr(i64 %rvp173283)                                    ; call prim_cdr
  %a171323 = call i64 @prim_car(i64 %rvp173282)                                      ; call prim_car
  %na173201 = call i64 @prim_cdr(i64 %rvp173282)                                     ; call prim_cdr
  %arg172199 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %retprim171474 = call i64 @prim_make_45vector(i64 %arg172199, i64 %a171323)        ; call prim_make_45vector
  %cloptr176050 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr176052 = getelementptr inbounds i64, i64* %cloptr176050, i64 1                ; &eptr176052[1]
  %eptr176053 = getelementptr inbounds i64, i64* %cloptr176050, i64 2                ; &eptr176053[2]
  %eptr176054 = getelementptr inbounds i64, i64* %cloptr176050, i64 3                ; &eptr176054[3]
  store i64 %Lyz$x, i64* %eptr176052                                                 ; *eptr176052 = %Lyz$x
  store i64 %Nvg$a, i64* %eptr176053                                                 ; *eptr176053 = %Nvg$a
  store i64 %M9T$b, i64* %eptr176054                                                 ; *eptr176054 = %M9T$b
  %eptr176051 = getelementptr inbounds i64, i64* %cloptr176050, i64 0                ; &cloptr176050[0]
  %f176055 = ptrtoint void(i64,i64)* @lam174018 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176055, i64* %eptr176051                                               ; store fptr
  %arg172202 = ptrtoint i64* %cloptr176050 to i64                                    ; closure cast; i64* -> i64
  %arg172201 = add i64 0, 0                                                          ; quoted ()
  %rva173281 = add i64 0, 0                                                          ; quoted ()
  %rva173280 = call i64 @prim_cons(i64 %retprim171474, i64 %rva173281)               ; call prim_cons
  %rva173279 = call i64 @prim_cons(i64 %arg172201, i64 %rva173280)                   ; call prim_cons
  %cloptr176056 = inttoptr i64 %arg172202 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176057 = getelementptr inbounds i64, i64* %cloptr176056, i64 0               ; &cloptr176056[0]
  %f176059 = load i64, i64* %i0ptr176057, align 8                                    ; load; *i0ptr176057
  %fptr176058 = inttoptr i64 %f176059 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176058(i64 %arg172202, i64 %rva173279)              ; tail call
  ret void
}


define void @lam174018(i64 %env174019, i64 %rvp173278) {
  %envptr176060 = inttoptr i64 %env174019 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176061 = getelementptr inbounds i64, i64* %envptr176060, i64 3              ; &envptr176060[3]
  %M9T$b = load i64, i64* %envptr176061, align 8                                     ; load; *envptr176061
  %envptr176062 = inttoptr i64 %env174019 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176063 = getelementptr inbounds i64, i64* %envptr176062, i64 2              ; &envptr176062[2]
  %Nvg$a = load i64, i64* %envptr176063, align 8                                     ; load; *envptr176063
  %envptr176064 = inttoptr i64 %env174019 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176065 = getelementptr inbounds i64, i64* %envptr176064, i64 1              ; &envptr176064[1]
  %Lyz$x = load i64, i64* %envptr176065, align 8                                     ; load; *envptr176065
  %_95171453 = call i64 @prim_car(i64 %rvp173278)                                    ; call prim_car
  %rvp173277 = call i64 @prim_cdr(i64 %rvp173278)                                    ; call prim_cdr
  %tfw$c = call i64 @prim_car(i64 %rvp173277)                                        ; call prim_car
  %na173203 = call i64 @prim_cdr(i64 %rvp173277)                                     ; call prim_cdr
  %cloptr176066 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr176067 = getelementptr inbounds i64, i64* %cloptr176066, i64 0                ; &cloptr176066[0]
  %f176068 = ptrtoint void(i64,i64)* @lam174016 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176068, i64* %eptr176067                                               ; store fptr
  %arg172204 = ptrtoint i64* %cloptr176066 to i64                                    ; closure cast; i64* -> i64
  %cloptr176069 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr176071 = getelementptr inbounds i64, i64* %cloptr176069, i64 1                ; &eptr176071[1]
  %eptr176072 = getelementptr inbounds i64, i64* %cloptr176069, i64 2                ; &eptr176072[2]
  %eptr176073 = getelementptr inbounds i64, i64* %cloptr176069, i64 3                ; &eptr176073[3]
  %eptr176074 = getelementptr inbounds i64, i64* %cloptr176069, i64 4                ; &eptr176074[4]
  store i64 %Lyz$x, i64* %eptr176071                                                 ; *eptr176071 = %Lyz$x
  store i64 %Nvg$a, i64* %eptr176072                                                 ; *eptr176072 = %Nvg$a
  store i64 %M9T$b, i64* %eptr176073                                                 ; *eptr176073 = %M9T$b
  store i64 %tfw$c, i64* %eptr176074                                                 ; *eptr176074 = %tfw$c
  %eptr176070 = getelementptr inbounds i64, i64* %cloptr176069, i64 0                ; &cloptr176069[0]
  %f176075 = ptrtoint void(i64,i64)* @lam174012 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176075, i64* %eptr176070                                               ; store fptr
  %arg172203 = ptrtoint i64* %cloptr176069 to i64                                    ; closure cast; i64* -> i64
  %rva173276 = add i64 0, 0                                                          ; quoted ()
  %rva173275 = call i64 @prim_cons(i64 %arg172203, i64 %rva173276)                   ; call prim_cons
  %cloptr176076 = inttoptr i64 %arg172204 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176077 = getelementptr inbounds i64, i64* %cloptr176076, i64 0               ; &cloptr176076[0]
  %f176079 = load i64, i64* %i0ptr176077, align 8                                    ; load; *i0ptr176077
  %fptr176078 = inttoptr i64 %f176079 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176078(i64 %arg172204, i64 %rva173275)              ; tail call
  ret void
}


define void @lam174016(i64 %env174017, i64 %RqU$lst171472) {
  %cont171471 = call i64 @prim_car(i64 %RqU$lst171472)                               ; call prim_car
  %RqU$lst = call i64 @prim_cdr(i64 %RqU$lst171472)                                  ; call prim_cdr
  %arg172208 = add i64 0, 0                                                          ; quoted ()
  %rva173206 = add i64 0, 0                                                          ; quoted ()
  %rva173205 = call i64 @prim_cons(i64 %RqU$lst, i64 %rva173206)                     ; call prim_cons
  %rva173204 = call i64 @prim_cons(i64 %arg172208, i64 %rva173205)                   ; call prim_cons
  %cloptr176080 = inttoptr i64 %cont171471 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176081 = getelementptr inbounds i64, i64* %cloptr176080, i64 0               ; &cloptr176080[0]
  %f176083 = load i64, i64* %i0ptr176081, align 8                                    ; load; *i0ptr176081
  %fptr176082 = inttoptr i64 %f176083 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176082(i64 %cont171471, i64 %rva173204)             ; tail call
  ret void
}


define void @lam174012(i64 %env174013, i64 %rvp173274) {
  %envptr176084 = inttoptr i64 %env174013 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176085 = getelementptr inbounds i64, i64* %envptr176084, i64 4              ; &envptr176084[4]
  %tfw$c = load i64, i64* %envptr176085, align 8                                     ; load; *envptr176085
  %envptr176086 = inttoptr i64 %env174013 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176087 = getelementptr inbounds i64, i64* %envptr176086, i64 3              ; &envptr176086[3]
  %M9T$b = load i64, i64* %envptr176087, align 8                                     ; load; *envptr176087
  %envptr176088 = inttoptr i64 %env174013 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176089 = getelementptr inbounds i64, i64* %envptr176088, i64 2              ; &envptr176088[2]
  %Nvg$a = load i64, i64* %envptr176089, align 8                                     ; load; *envptr176089
  %envptr176090 = inttoptr i64 %env174013 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176091 = getelementptr inbounds i64, i64* %envptr176090, i64 1              ; &envptr176090[1]
  %Lyz$x = load i64, i64* %envptr176091, align 8                                     ; load; *envptr176091
  %_95171469 = call i64 @prim_car(i64 %rvp173274)                                    ; call prim_car
  %rvp173273 = call i64 @prim_cdr(i64 %rvp173274)                                    ; call prim_cdr
  %a171324 = call i64 @prim_car(i64 %rvp173273)                                      ; call prim_car
  %na173208 = call i64 @prim_cdr(i64 %rvp173273)                                     ; call prim_cdr
  %arg172211 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %retprim171470 = call i64 @prim_make_45vector(i64 %arg172211, i64 %a171324)        ; call prim_make_45vector
  %cloptr176092 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr176094 = getelementptr inbounds i64, i64* %cloptr176092, i64 1                ; &eptr176094[1]
  %eptr176095 = getelementptr inbounds i64, i64* %cloptr176092, i64 2                ; &eptr176095[2]
  %eptr176096 = getelementptr inbounds i64, i64* %cloptr176092, i64 3                ; &eptr176096[3]
  %eptr176097 = getelementptr inbounds i64, i64* %cloptr176092, i64 4                ; &eptr176097[4]
  store i64 %Lyz$x, i64* %eptr176094                                                 ; *eptr176094 = %Lyz$x
  store i64 %Nvg$a, i64* %eptr176095                                                 ; *eptr176095 = %Nvg$a
  store i64 %M9T$b, i64* %eptr176096                                                 ; *eptr176096 = %M9T$b
  store i64 %tfw$c, i64* %eptr176097                                                 ; *eptr176097 = %tfw$c
  %eptr176093 = getelementptr inbounds i64, i64* %cloptr176092, i64 0                ; &cloptr176092[0]
  %f176098 = ptrtoint void(i64,i64)* @lam174009 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176098, i64* %eptr176093                                               ; store fptr
  %arg172214 = ptrtoint i64* %cloptr176092 to i64                                    ; closure cast; i64* -> i64
  %arg172213 = add i64 0, 0                                                          ; quoted ()
  %rva173272 = add i64 0, 0                                                          ; quoted ()
  %rva173271 = call i64 @prim_cons(i64 %retprim171470, i64 %rva173272)               ; call prim_cons
  %rva173270 = call i64 @prim_cons(i64 %arg172213, i64 %rva173271)                   ; call prim_cons
  %cloptr176099 = inttoptr i64 %arg172214 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176100 = getelementptr inbounds i64, i64* %cloptr176099, i64 0               ; &cloptr176099[0]
  %f176102 = load i64, i64* %i0ptr176100, align 8                                    ; load; *i0ptr176100
  %fptr176101 = inttoptr i64 %f176102 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176101(i64 %arg172214, i64 %rva173270)              ; tail call
  ret void
}


define void @lam174009(i64 %env174010, i64 %rvp173269) {
  %envptr176103 = inttoptr i64 %env174010 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176104 = getelementptr inbounds i64, i64* %envptr176103, i64 4              ; &envptr176103[4]
  %tfw$c = load i64, i64* %envptr176104, align 8                                     ; load; *envptr176104
  %envptr176105 = inttoptr i64 %env174010 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176106 = getelementptr inbounds i64, i64* %envptr176105, i64 3              ; &envptr176105[3]
  %M9T$b = load i64, i64* %envptr176106, align 8                                     ; load; *envptr176106
  %envptr176107 = inttoptr i64 %env174010 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176108 = getelementptr inbounds i64, i64* %envptr176107, i64 2              ; &envptr176107[2]
  %Nvg$a = load i64, i64* %envptr176108, align 8                                     ; load; *envptr176108
  %envptr176109 = inttoptr i64 %env174010 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176110 = getelementptr inbounds i64, i64* %envptr176109, i64 1              ; &envptr176109[1]
  %Lyz$x = load i64, i64* %envptr176110, align 8                                     ; load; *envptr176110
  %_95171454 = call i64 @prim_car(i64 %rvp173269)                                    ; call prim_car
  %rvp173268 = call i64 @prim_cdr(i64 %rvp173269)                                    ; call prim_cdr
  %fU0$d = call i64 @prim_car(i64 %rvp173268)                                        ; call prim_car
  %na173210 = call i64 @prim_cdr(i64 %rvp173268)                                     ; call prim_cdr
  %arg172216 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %arg172215 = call i64 @const_init_int(i64 2)                                       ; quoted int
  %a171325 = call i64 @prim_cons(i64 %arg172216, i64 %arg172215)                     ; call prim_cons
  %arg172218 = call i64 @const_init_int(i64 3)                                       ; quoted int
  %arg172217 = call i64 @const_init_int(i64 4)                                       ; quoted int
  %a171326 = call i64 @prim_cons(i64 %arg172218, i64 %arg172217)                     ; call prim_cons
  %arg172220 = call i64 @const_init_int(i64 152)                                     ; quoted int
  %arg172219 = call i64 @const_init_int(i64 6)                                       ; quoted int
  %a171327 = call i64 @prim_cons(i64 %arg172220, i64 %arg172219)                     ; call prim_cons
  %arg172222 = call i64 @const_init_int(i64 7)                                       ; quoted int
  %arg172221 = call i64 @const_init_int(i64 1024)                                    ; quoted int
  %a171328 = call i64 @prim_cons(i64 %arg172222, i64 %arg172221)                     ; call prim_cons
  %cloptr176111 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr176112 = getelementptr inbounds i64, i64* %cloptr176111, i64 0                ; &cloptr176111[0]
  %f176113 = ptrtoint void(i64,i64)* @lam173999 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176113, i64* %eptr176112                                               ; store fptr
  %arg172228 = ptrtoint i64* %cloptr176111 to i64                                    ; closure cast; i64* -> i64
  %cloptr176114 = call i64* @alloc(i64 48)                                           ; malloc
  %eptr176116 = getelementptr inbounds i64, i64* %cloptr176114, i64 1                ; &eptr176116[1]
  %eptr176117 = getelementptr inbounds i64, i64* %cloptr176114, i64 2                ; &eptr176117[2]
  %eptr176118 = getelementptr inbounds i64, i64* %cloptr176114, i64 3                ; &eptr176118[3]
  %eptr176119 = getelementptr inbounds i64, i64* %cloptr176114, i64 4                ; &eptr176119[4]
  %eptr176120 = getelementptr inbounds i64, i64* %cloptr176114, i64 5                ; &eptr176120[5]
  store i64 %fU0$d, i64* %eptr176116                                                 ; *eptr176116 = %fU0$d
  store i64 %Lyz$x, i64* %eptr176117                                                 ; *eptr176117 = %Lyz$x
  store i64 %Nvg$a, i64* %eptr176118                                                 ; *eptr176118 = %Nvg$a
  store i64 %M9T$b, i64* %eptr176119                                                 ; *eptr176119 = %M9T$b
  store i64 %tfw$c, i64* %eptr176120                                                 ; *eptr176120 = %tfw$c
  %eptr176115 = getelementptr inbounds i64, i64* %cloptr176114, i64 0                ; &cloptr176114[0]
  %f176121 = ptrtoint void(i64,i64)* @lam173995 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176121, i64* %eptr176115                                               ; store fptr
  %arg172227 = ptrtoint i64* %cloptr176114 to i64                                    ; closure cast; i64* -> i64
  %rva173267 = add i64 0, 0                                                          ; quoted ()
  %rva173266 = call i64 @prim_cons(i64 %a171328, i64 %rva173267)                     ; call prim_cons
  %rva173265 = call i64 @prim_cons(i64 %a171327, i64 %rva173266)                     ; call prim_cons
  %rva173264 = call i64 @prim_cons(i64 %a171326, i64 %rva173265)                     ; call prim_cons
  %rva173263 = call i64 @prim_cons(i64 %a171325, i64 %rva173264)                     ; call prim_cons
  %rva173262 = call i64 @prim_cons(i64 %arg172227, i64 %rva173263)                   ; call prim_cons
  %cloptr176122 = inttoptr i64 %arg172228 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176123 = getelementptr inbounds i64, i64* %cloptr176122, i64 0               ; &cloptr176122[0]
  %f176125 = load i64, i64* %i0ptr176123, align 8                                    ; load; *i0ptr176123
  %fptr176124 = inttoptr i64 %f176125 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176124(i64 %arg172228, i64 %rva173262)              ; tail call
  ret void
}


define void @lam173999(i64 %env174000, i64 %m8a$lst171468) {
  %cont171467 = call i64 @prim_car(i64 %m8a$lst171468)                               ; call prim_car
  %m8a$lst = call i64 @prim_cdr(i64 %m8a$lst171468)                                  ; call prim_cdr
  %arg172232 = add i64 0, 0                                                          ; quoted ()
  %rva173213 = add i64 0, 0                                                          ; quoted ()
  %rva173212 = call i64 @prim_cons(i64 %m8a$lst, i64 %rva173213)                     ; call prim_cons
  %rva173211 = call i64 @prim_cons(i64 %arg172232, i64 %rva173212)                   ; call prim_cons
  %cloptr176126 = inttoptr i64 %cont171467 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176127 = getelementptr inbounds i64, i64* %cloptr176126, i64 0               ; &cloptr176126[0]
  %f176129 = load i64, i64* %i0ptr176127, align 8                                    ; load; *i0ptr176127
  %fptr176128 = inttoptr i64 %f176129 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176128(i64 %cont171467, i64 %rva173211)             ; tail call
  ret void
}


define void @lam173995(i64 %env173996, i64 %rvp173261) {
  %envptr176130 = inttoptr i64 %env173996 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176131 = getelementptr inbounds i64, i64* %envptr176130, i64 5              ; &envptr176130[5]
  %tfw$c = load i64, i64* %envptr176131, align 8                                     ; load; *envptr176131
  %envptr176132 = inttoptr i64 %env173996 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176133 = getelementptr inbounds i64, i64* %envptr176132, i64 4              ; &envptr176132[4]
  %M9T$b = load i64, i64* %envptr176133, align 8                                     ; load; *envptr176133
  %envptr176134 = inttoptr i64 %env173996 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176135 = getelementptr inbounds i64, i64* %envptr176134, i64 3              ; &envptr176134[3]
  %Nvg$a = load i64, i64* %envptr176135, align 8                                     ; load; *envptr176135
  %envptr176136 = inttoptr i64 %env173996 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176137 = getelementptr inbounds i64, i64* %envptr176136, i64 2              ; &envptr176136[2]
  %Lyz$x = load i64, i64* %envptr176137, align 8                                     ; load; *envptr176137
  %envptr176138 = inttoptr i64 %env173996 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176139 = getelementptr inbounds i64, i64* %envptr176138, i64 1              ; &envptr176138[1]
  %fU0$d = load i64, i64* %envptr176139, align 8                                     ; load; *envptr176139
  %_95171465 = call i64 @prim_car(i64 %rvp173261)                                    ; call prim_car
  %rvp173260 = call i64 @prim_cdr(i64 %rvp173261)                                    ; call prim_cdr
  %a171329 = call i64 @prim_car(i64 %rvp173260)                                      ; call prim_car
  %na173215 = call i64 @prim_cdr(i64 %rvp173260)                                     ; call prim_cdr
  %a171330 = call i64 @prim_make_45hash(i64 %a171329)                                ; call prim_make_45hash
  %arg172236 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171466 = call i64 @prim_vector_45set_33(i64 %Lyz$x, i64 %arg172236, i64 %a171330); call prim_vector_45set_33
  %cloptr176140 = call i64* @alloc(i64 48)                                           ; malloc
  %eptr176142 = getelementptr inbounds i64, i64* %cloptr176140, i64 1                ; &eptr176142[1]
  %eptr176143 = getelementptr inbounds i64, i64* %cloptr176140, i64 2                ; &eptr176143[2]
  %eptr176144 = getelementptr inbounds i64, i64* %cloptr176140, i64 3                ; &eptr176144[3]
  %eptr176145 = getelementptr inbounds i64, i64* %cloptr176140, i64 4                ; &eptr176145[4]
  %eptr176146 = getelementptr inbounds i64, i64* %cloptr176140, i64 5                ; &eptr176146[5]
  store i64 %fU0$d, i64* %eptr176142                                                 ; *eptr176142 = %fU0$d
  store i64 %Lyz$x, i64* %eptr176143                                                 ; *eptr176143 = %Lyz$x
  store i64 %Nvg$a, i64* %eptr176144                                                 ; *eptr176144 = %Nvg$a
  store i64 %M9T$b, i64* %eptr176145                                                 ; *eptr176145 = %M9T$b
  store i64 %tfw$c, i64* %eptr176146                                                 ; *eptr176146 = %tfw$c
  %eptr176141 = getelementptr inbounds i64, i64* %cloptr176140, i64 0                ; &cloptr176140[0]
  %f176147 = ptrtoint void(i64,i64)* @lam173992 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176147, i64* %eptr176141                                               ; store fptr
  %arg172240 = ptrtoint i64* %cloptr176140 to i64                                    ; closure cast; i64* -> i64
  %arg172239 = add i64 0, 0                                                          ; quoted ()
  %rva173259 = add i64 0, 0                                                          ; quoted ()
  %rva173258 = call i64 @prim_cons(i64 %retprim171466, i64 %rva173259)               ; call prim_cons
  %rva173257 = call i64 @prim_cons(i64 %arg172239, i64 %rva173258)                   ; call prim_cons
  %cloptr176148 = inttoptr i64 %arg172240 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176149 = getelementptr inbounds i64, i64* %cloptr176148, i64 0               ; &cloptr176148[0]
  %f176151 = load i64, i64* %i0ptr176149, align 8                                    ; load; *i0ptr176149
  %fptr176150 = inttoptr i64 %f176151 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176150(i64 %arg172240, i64 %rva173257)              ; tail call
  ret void
}


define void @lam173992(i64 %env173993, i64 %rvp173256) {
  %envptr176152 = inttoptr i64 %env173993 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176153 = getelementptr inbounds i64, i64* %envptr176152, i64 5              ; &envptr176152[5]
  %tfw$c = load i64, i64* %envptr176153, align 8                                     ; load; *envptr176153
  %envptr176154 = inttoptr i64 %env173993 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176155 = getelementptr inbounds i64, i64* %envptr176154, i64 4              ; &envptr176154[4]
  %M9T$b = load i64, i64* %envptr176155, align 8                                     ; load; *envptr176155
  %envptr176156 = inttoptr i64 %env173993 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176157 = getelementptr inbounds i64, i64* %envptr176156, i64 3              ; &envptr176156[3]
  %Nvg$a = load i64, i64* %envptr176157, align 8                                     ; load; *envptr176157
  %envptr176158 = inttoptr i64 %env173993 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176159 = getelementptr inbounds i64, i64* %envptr176158, i64 2              ; &envptr176158[2]
  %Lyz$x = load i64, i64* %envptr176159, align 8                                     ; load; *envptr176159
  %envptr176160 = inttoptr i64 %env173993 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176161 = getelementptr inbounds i64, i64* %envptr176160, i64 1              ; &envptr176160[1]
  %fU0$d = load i64, i64* %envptr176161, align 8                                     ; load; *envptr176161
  %_95171455 = call i64 @prim_car(i64 %rvp173256)                                    ; call prim_car
  %rvp173255 = call i64 @prim_cdr(i64 %rvp173256)                                    ; call prim_cdr
  %k93$_95171196 = call i64 @prim_car(i64 %rvp173255)                                ; call prim_car
  %na173217 = call i64 @prim_cdr(i64 %rvp173255)                                     ; call prim_cdr
  %arg172241 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171331 = call i64 @prim_vector_45ref(i64 %Lyz$x, i64 %arg172241)                 ; call prim_vector_45ref
  %arg172243 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %a171332 = call i64 @prim_hash_45ref(i64 %a171331, i64 %arg172243)                 ; call prim_hash_45ref
  %arg172246 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171464 = call i64 @prim_vector_45set_33(i64 %Nvg$a, i64 %arg172246, i64 %a171332); call prim_vector_45set_33
  %cloptr176162 = call i64* @alloc(i64 48)                                           ; malloc
  %eptr176164 = getelementptr inbounds i64, i64* %cloptr176162, i64 1                ; &eptr176164[1]
  %eptr176165 = getelementptr inbounds i64, i64* %cloptr176162, i64 2                ; &eptr176165[2]
  %eptr176166 = getelementptr inbounds i64, i64* %cloptr176162, i64 3                ; &eptr176166[3]
  %eptr176167 = getelementptr inbounds i64, i64* %cloptr176162, i64 4                ; &eptr176167[4]
  %eptr176168 = getelementptr inbounds i64, i64* %cloptr176162, i64 5                ; &eptr176168[5]
  store i64 %fU0$d, i64* %eptr176164                                                 ; *eptr176164 = %fU0$d
  store i64 %Lyz$x, i64* %eptr176165                                                 ; *eptr176165 = %Lyz$x
  store i64 %Nvg$a, i64* %eptr176166                                                 ; *eptr176166 = %Nvg$a
  store i64 %M9T$b, i64* %eptr176167                                                 ; *eptr176167 = %M9T$b
  store i64 %tfw$c, i64* %eptr176168                                                 ; *eptr176168 = %tfw$c
  %eptr176163 = getelementptr inbounds i64, i64* %cloptr176162, i64 0                ; &cloptr176162[0]
  %f176169 = ptrtoint void(i64,i64)* @lam173987 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176169, i64* %eptr176163                                               ; store fptr
  %arg172250 = ptrtoint i64* %cloptr176162 to i64                                    ; closure cast; i64* -> i64
  %arg172249 = add i64 0, 0                                                          ; quoted ()
  %rva173254 = add i64 0, 0                                                          ; quoted ()
  %rva173253 = call i64 @prim_cons(i64 %retprim171464, i64 %rva173254)               ; call prim_cons
  %rva173252 = call i64 @prim_cons(i64 %arg172249, i64 %rva173253)                   ; call prim_cons
  %cloptr176170 = inttoptr i64 %arg172250 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176171 = getelementptr inbounds i64, i64* %cloptr176170, i64 0               ; &cloptr176170[0]
  %f176173 = load i64, i64* %i0ptr176171, align 8                                    ; load; *i0ptr176171
  %fptr176172 = inttoptr i64 %f176173 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176172(i64 %arg172250, i64 %rva173252)              ; tail call
  ret void
}


define void @lam173987(i64 %env173988, i64 %rvp173251) {
  %envptr176174 = inttoptr i64 %env173988 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176175 = getelementptr inbounds i64, i64* %envptr176174, i64 5              ; &envptr176174[5]
  %tfw$c = load i64, i64* %envptr176175, align 8                                     ; load; *envptr176175
  %envptr176176 = inttoptr i64 %env173988 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176177 = getelementptr inbounds i64, i64* %envptr176176, i64 4              ; &envptr176176[4]
  %M9T$b = load i64, i64* %envptr176177, align 8                                     ; load; *envptr176177
  %envptr176178 = inttoptr i64 %env173988 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176179 = getelementptr inbounds i64, i64* %envptr176178, i64 3              ; &envptr176178[3]
  %Nvg$a = load i64, i64* %envptr176179, align 8                                     ; load; *envptr176179
  %envptr176180 = inttoptr i64 %env173988 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176181 = getelementptr inbounds i64, i64* %envptr176180, i64 2              ; &envptr176180[2]
  %Lyz$x = load i64, i64* %envptr176181, align 8                                     ; load; *envptr176181
  %envptr176182 = inttoptr i64 %env173988 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176183 = getelementptr inbounds i64, i64* %envptr176182, i64 1              ; &envptr176182[1]
  %fU0$d = load i64, i64* %envptr176183, align 8                                     ; load; *envptr176183
  %_95171456 = call i64 @prim_car(i64 %rvp173251)                                    ; call prim_car
  %rvp173250 = call i64 @prim_cdr(i64 %rvp173251)                                    ; call prim_cdr
  %zhd$_95171197 = call i64 @prim_car(i64 %rvp173250)                                ; call prim_car
  %na173219 = call i64 @prim_cdr(i64 %rvp173250)                                     ; call prim_cdr
  %arg172251 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171333 = call i64 @prim_vector_45ref(i64 %Lyz$x, i64 %arg172251)                 ; call prim_vector_45ref
  %arg172253 = call i64 @const_init_int(i64 3)                                       ; quoted int
  %a171334 = call i64 @prim_hash_45ref(i64 %a171333, i64 %arg172253)                 ; call prim_hash_45ref
  %arg172256 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171463 = call i64 @prim_vector_45set_33(i64 %M9T$b, i64 %arg172256, i64 %a171334); call prim_vector_45set_33
  %cloptr176184 = call i64* @alloc(i64 48)                                           ; malloc
  %eptr176186 = getelementptr inbounds i64, i64* %cloptr176184, i64 1                ; &eptr176186[1]
  %eptr176187 = getelementptr inbounds i64, i64* %cloptr176184, i64 2                ; &eptr176187[2]
  %eptr176188 = getelementptr inbounds i64, i64* %cloptr176184, i64 3                ; &eptr176188[3]
  %eptr176189 = getelementptr inbounds i64, i64* %cloptr176184, i64 4                ; &eptr176189[4]
  %eptr176190 = getelementptr inbounds i64, i64* %cloptr176184, i64 5                ; &eptr176190[5]
  store i64 %fU0$d, i64* %eptr176186                                                 ; *eptr176186 = %fU0$d
  store i64 %Lyz$x, i64* %eptr176187                                                 ; *eptr176187 = %Lyz$x
  store i64 %Nvg$a, i64* %eptr176188                                                 ; *eptr176188 = %Nvg$a
  store i64 %M9T$b, i64* %eptr176189                                                 ; *eptr176189 = %M9T$b
  store i64 %tfw$c, i64* %eptr176190                                                 ; *eptr176190 = %tfw$c
  %eptr176185 = getelementptr inbounds i64, i64* %cloptr176184, i64 0                ; &cloptr176184[0]
  %f176191 = ptrtoint void(i64,i64)* @lam173982 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176191, i64* %eptr176185                                               ; store fptr
  %arg172260 = ptrtoint i64* %cloptr176184 to i64                                    ; closure cast; i64* -> i64
  %arg172259 = add i64 0, 0                                                          ; quoted ()
  %rva173249 = add i64 0, 0                                                          ; quoted ()
  %rva173248 = call i64 @prim_cons(i64 %retprim171463, i64 %rva173249)               ; call prim_cons
  %rva173247 = call i64 @prim_cons(i64 %arg172259, i64 %rva173248)                   ; call prim_cons
  %cloptr176192 = inttoptr i64 %arg172260 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176193 = getelementptr inbounds i64, i64* %cloptr176192, i64 0               ; &cloptr176192[0]
  %f176195 = load i64, i64* %i0ptr176193, align 8                                    ; load; *i0ptr176193
  %fptr176194 = inttoptr i64 %f176195 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176194(i64 %arg172260, i64 %rva173247)              ; tail call
  ret void
}


define void @lam173982(i64 %env173983, i64 %rvp173246) {
  %envptr176196 = inttoptr i64 %env173983 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176197 = getelementptr inbounds i64, i64* %envptr176196, i64 5              ; &envptr176196[5]
  %tfw$c = load i64, i64* %envptr176197, align 8                                     ; load; *envptr176197
  %envptr176198 = inttoptr i64 %env173983 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176199 = getelementptr inbounds i64, i64* %envptr176198, i64 4              ; &envptr176198[4]
  %M9T$b = load i64, i64* %envptr176199, align 8                                     ; load; *envptr176199
  %envptr176200 = inttoptr i64 %env173983 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176201 = getelementptr inbounds i64, i64* %envptr176200, i64 3              ; &envptr176200[3]
  %Nvg$a = load i64, i64* %envptr176201, align 8                                     ; load; *envptr176201
  %envptr176202 = inttoptr i64 %env173983 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176203 = getelementptr inbounds i64, i64* %envptr176202, i64 2              ; &envptr176202[2]
  %Lyz$x = load i64, i64* %envptr176203, align 8                                     ; load; *envptr176203
  %envptr176204 = inttoptr i64 %env173983 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176205 = getelementptr inbounds i64, i64* %envptr176204, i64 1              ; &envptr176204[1]
  %fU0$d = load i64, i64* %envptr176205, align 8                                     ; load; *envptr176205
  %_95171457 = call i64 @prim_car(i64 %rvp173246)                                    ; call prim_car
  %rvp173245 = call i64 @prim_cdr(i64 %rvp173246)                                    ; call prim_cdr
  %oqH$_95171198 = call i64 @prim_car(i64 %rvp173245)                                ; call prim_car
  %na173221 = call i64 @prim_cdr(i64 %rvp173245)                                     ; call prim_cdr
  %arg172261 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171335 = call i64 @prim_vector_45ref(i64 %Lyz$x, i64 %arg172261)                 ; call prim_vector_45ref
  %arg172263 = call i64 @const_init_int(i64 7)                                       ; quoted int
  %a171336 = call i64 @prim_hash_45ref(i64 %a171335, i64 %arg172263)                 ; call prim_hash_45ref
  %arg172266 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171462 = call i64 @prim_vector_45set_33(i64 %tfw$c, i64 %arg172266, i64 %a171336); call prim_vector_45set_33
  %cloptr176206 = call i64* @alloc(i64 48)                                           ; malloc
  %eptr176208 = getelementptr inbounds i64, i64* %cloptr176206, i64 1                ; &eptr176208[1]
  %eptr176209 = getelementptr inbounds i64, i64* %cloptr176206, i64 2                ; &eptr176209[2]
  %eptr176210 = getelementptr inbounds i64, i64* %cloptr176206, i64 3                ; &eptr176210[3]
  %eptr176211 = getelementptr inbounds i64, i64* %cloptr176206, i64 4                ; &eptr176211[4]
  %eptr176212 = getelementptr inbounds i64, i64* %cloptr176206, i64 5                ; &eptr176212[5]
  store i64 %fU0$d, i64* %eptr176208                                                 ; *eptr176208 = %fU0$d
  store i64 %Lyz$x, i64* %eptr176209                                                 ; *eptr176209 = %Lyz$x
  store i64 %Nvg$a, i64* %eptr176210                                                 ; *eptr176210 = %Nvg$a
  store i64 %M9T$b, i64* %eptr176211                                                 ; *eptr176211 = %M9T$b
  store i64 %tfw$c, i64* %eptr176212                                                 ; *eptr176212 = %tfw$c
  %eptr176207 = getelementptr inbounds i64, i64* %cloptr176206, i64 0                ; &cloptr176206[0]
  %f176213 = ptrtoint void(i64,i64)* @lam173977 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176213, i64* %eptr176207                                               ; store fptr
  %arg172270 = ptrtoint i64* %cloptr176206 to i64                                    ; closure cast; i64* -> i64
  %arg172269 = add i64 0, 0                                                          ; quoted ()
  %rva173244 = add i64 0, 0                                                          ; quoted ()
  %rva173243 = call i64 @prim_cons(i64 %retprim171462, i64 %rva173244)               ; call prim_cons
  %rva173242 = call i64 @prim_cons(i64 %arg172269, i64 %rva173243)                   ; call prim_cons
  %cloptr176214 = inttoptr i64 %arg172270 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176215 = getelementptr inbounds i64, i64* %cloptr176214, i64 0               ; &cloptr176214[0]
  %f176217 = load i64, i64* %i0ptr176215, align 8                                    ; load; *i0ptr176215
  %fptr176216 = inttoptr i64 %f176217 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176216(i64 %arg172270, i64 %rva173242)              ; tail call
  ret void
}


define void @lam173977(i64 %env173978, i64 %rvp173241) {
  %envptr176218 = inttoptr i64 %env173978 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176219 = getelementptr inbounds i64, i64* %envptr176218, i64 5              ; &envptr176218[5]
  %tfw$c = load i64, i64* %envptr176219, align 8                                     ; load; *envptr176219
  %envptr176220 = inttoptr i64 %env173978 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176221 = getelementptr inbounds i64, i64* %envptr176220, i64 4              ; &envptr176220[4]
  %M9T$b = load i64, i64* %envptr176221, align 8                                     ; load; *envptr176221
  %envptr176222 = inttoptr i64 %env173978 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176223 = getelementptr inbounds i64, i64* %envptr176222, i64 3              ; &envptr176222[3]
  %Nvg$a = load i64, i64* %envptr176223, align 8                                     ; load; *envptr176223
  %envptr176224 = inttoptr i64 %env173978 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176225 = getelementptr inbounds i64, i64* %envptr176224, i64 2              ; &envptr176224[2]
  %Lyz$x = load i64, i64* %envptr176225, align 8                                     ; load; *envptr176225
  %envptr176226 = inttoptr i64 %env173978 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176227 = getelementptr inbounds i64, i64* %envptr176226, i64 1              ; &envptr176226[1]
  %fU0$d = load i64, i64* %envptr176227, align 8                                     ; load; *envptr176227
  %_95171458 = call i64 @prim_car(i64 %rvp173241)                                    ; call prim_car
  %rvp173240 = call i64 @prim_cdr(i64 %rvp173241)                                    ; call prim_cdr
  %dmQ$_95171199 = call i64 @prim_car(i64 %rvp173240)                                ; call prim_car
  %na173223 = call i64 @prim_cdr(i64 %rvp173240)                                     ; call prim_cdr
  %arg172271 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171337 = call i64 @prim_vector_45ref(i64 %Lyz$x, i64 %arg172271)                 ; call prim_vector_45ref
  %arg172273 = call i64 @const_init_int(i64 152)                                     ; quoted int
  %a171338 = call i64 @prim_hash_45ref(i64 %a171337, i64 %arg172273)                 ; call prim_hash_45ref
  %arg172276 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %retprim171461 = call i64 @prim_vector_45set_33(i64 %fU0$d, i64 %arg172276, i64 %a171338); call prim_vector_45set_33
  %cloptr176228 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr176230 = getelementptr inbounds i64, i64* %cloptr176228, i64 1                ; &eptr176230[1]
  %eptr176231 = getelementptr inbounds i64, i64* %cloptr176228, i64 2                ; &eptr176231[2]
  %eptr176232 = getelementptr inbounds i64, i64* %cloptr176228, i64 3                ; &eptr176232[3]
  %eptr176233 = getelementptr inbounds i64, i64* %cloptr176228, i64 4                ; &eptr176233[4]
  store i64 %fU0$d, i64* %eptr176230                                                 ; *eptr176230 = %fU0$d
  store i64 %Nvg$a, i64* %eptr176231                                                 ; *eptr176231 = %Nvg$a
  store i64 %M9T$b, i64* %eptr176232                                                 ; *eptr176232 = %M9T$b
  store i64 %tfw$c, i64* %eptr176233                                                 ; *eptr176233 = %tfw$c
  %eptr176229 = getelementptr inbounds i64, i64* %cloptr176228, i64 0                ; &cloptr176228[0]
  %f176234 = ptrtoint void(i64,i64)* @lam173972 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176234, i64* %eptr176229                                               ; store fptr
  %arg172280 = ptrtoint i64* %cloptr176228 to i64                                    ; closure cast; i64* -> i64
  %arg172279 = add i64 0, 0                                                          ; quoted ()
  %rva173239 = add i64 0, 0                                                          ; quoted ()
  %rva173238 = call i64 @prim_cons(i64 %retprim171461, i64 %rva173239)               ; call prim_cons
  %rva173237 = call i64 @prim_cons(i64 %arg172279, i64 %rva173238)                   ; call prim_cons
  %cloptr176235 = inttoptr i64 %arg172280 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176236 = getelementptr inbounds i64, i64* %cloptr176235, i64 0               ; &cloptr176235[0]
  %f176238 = load i64, i64* %i0ptr176236, align 8                                    ; load; *i0ptr176236
  %fptr176237 = inttoptr i64 %f176238 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176237(i64 %arg172280, i64 %rva173237)              ; tail call
  ret void
}


define void @lam173972(i64 %env173973, i64 %rvp173236) {
  %envptr176239 = inttoptr i64 %env173973 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176240 = getelementptr inbounds i64, i64* %envptr176239, i64 4              ; &envptr176239[4]
  %tfw$c = load i64, i64* %envptr176240, align 8                                     ; load; *envptr176240
  %envptr176241 = inttoptr i64 %env173973 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176242 = getelementptr inbounds i64, i64* %envptr176241, i64 3              ; &envptr176241[3]
  %M9T$b = load i64, i64* %envptr176242, align 8                                     ; load; *envptr176242
  %envptr176243 = inttoptr i64 %env173973 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176244 = getelementptr inbounds i64, i64* %envptr176243, i64 2              ; &envptr176243[2]
  %Nvg$a = load i64, i64* %envptr176244, align 8                                     ; load; *envptr176244
  %envptr176245 = inttoptr i64 %env173973 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176246 = getelementptr inbounds i64, i64* %envptr176245, i64 1              ; &envptr176245[1]
  %fU0$d = load i64, i64* %envptr176246, align 8                                     ; load; *envptr176246
  %_95171459 = call i64 @prim_car(i64 %rvp173236)                                    ; call prim_car
  %rvp173235 = call i64 @prim_cdr(i64 %rvp173236)                                    ; call prim_cdr
  %H2T$_95171200 = call i64 @prim_car(i64 %rvp173235)                                ; call prim_car
  %na173225 = call i64 @prim_cdr(i64 %rvp173235)                                     ; call prim_cdr
  %arg172281 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171339 = call i64 @prim_vector_45ref(i64 %Nvg$a, i64 %arg172281)                 ; call prim_vector_45ref
  %arg172283 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171340 = call i64 @prim_vector_45ref(i64 %M9T$b, i64 %arg172283)                 ; call prim_vector_45ref
  %arg172285 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171341 = call i64 @prim_vector_45ref(i64 %tfw$c, i64 %arg172285)                 ; call prim_vector_45ref
  %arg172287 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171342 = call i64 @prim_vector_45ref(i64 %fU0$d, i64 %arg172287)                 ; call prim_vector_45ref
  %a171343 = call i64 @prim__43(i64 %a171341, i64 %a171342)                          ; call prim__43
  %a171344 = call i64 @prim__43(i64 %a171340, i64 %a171343)                          ; call prim__43
  %retprim171460 = call i64 @prim__43(i64 %a171339, i64 %a171344)                    ; call prim__43
  %cloptr176247 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr176248 = getelementptr inbounds i64, i64* %cloptr176247, i64 0                ; &cloptr176247[0]
  %f176249 = ptrtoint void(i64,i64)* @lam173966 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176249, i64* %eptr176248                                               ; store fptr
  %arg172297 = ptrtoint i64* %cloptr176247 to i64                                    ; closure cast; i64* -> i64
  %arg172296 = add i64 0, 0                                                          ; quoted ()
  %rva173234 = add i64 0, 0                                                          ; quoted ()
  %rva173233 = call i64 @prim_cons(i64 %retprim171460, i64 %rva173234)               ; call prim_cons
  %rva173232 = call i64 @prim_cons(i64 %arg172296, i64 %rva173233)                   ; call prim_cons
  %cloptr176250 = inttoptr i64 %arg172297 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176251 = getelementptr inbounds i64, i64* %cloptr176250, i64 0               ; &cloptr176250[0]
  %f176253 = load i64, i64* %i0ptr176251, align 8                                    ; load; *i0ptr176251
  %fptr176252 = inttoptr i64 %f176253 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176252(i64 %arg172297, i64 %rva173232)              ; tail call
  ret void
}


define void @lam173966(i64 %env173967, i64 %rvp173231) {
  %_950 = call i64 @prim_car(i64 %rvp173231)                                         ; call prim_car
  %rvp173230 = call i64 @prim_cdr(i64 %rvp173231)                                    ; call prim_cdr
  %x = call i64 @prim_car(i64 %rvp173230)                                            ; call prim_car
  %na173227 = call i64 @prim_cdr(i64 %rvp173230)                                     ; call prim_cdr
  %_951 = call i64 @prim_halt(i64 %x)                                                ; call prim_halt
  %rva173229 = add i64 0, 0                                                          ; quoted ()
  %rva173228 = call i64 @prim_cons(i64 %_951, i64 %rva173229)                        ; call prim_cons
  %cloptr176254 = inttoptr i64 %_951 to i64*                                         ; closure/env cast; i64 -> i64*
  %i0ptr176255 = getelementptr inbounds i64, i64* %cloptr176254, i64 0               ; &cloptr176254[0]
  %f176257 = load i64, i64* %i0ptr176255, align 8                                    ; load; *i0ptr176255
  %fptr176256 = inttoptr i64 %f176257 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176256(i64 %_951, i64 %rva173228)                   ; tail call
  ret void
}


define void @lam173929(i64 %env173930, i64 %rvp173427) {
  %envptr176258 = inttoptr i64 %env173930 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176259 = getelementptr inbounds i64, i64* %envptr176258, i64 3              ; &envptr176258[3]
  %slC$_37map1 = load i64, i64* %envptr176259, align 8                               ; load; *envptr176259
  %envptr176260 = inttoptr i64 %env173930 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176261 = getelementptr inbounds i64, i64* %envptr176260, i64 2              ; &envptr176260[2]
  %zNF$_37foldr = load i64, i64* %envptr176261, align 8                              ; load; *envptr176261
  %envptr176262 = inttoptr i64 %env173930 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176263 = getelementptr inbounds i64, i64* %envptr176262, i64 1              ; &envptr176262[1]
  %xK1$_37foldr1 = load i64, i64* %envptr176263, align 8                             ; load; *envptr176263
  %cont171499 = call i64 @prim_car(i64 %rvp173427)                                   ; call prim_car
  %rvp173426 = call i64 @prim_cdr(i64 %rvp173427)                                    ; call prim_cdr
  %CZn$_37foldl = call i64 @prim_car(i64 %rvp173426)                                 ; call prim_car
  %na173334 = call i64 @prim_cdr(i64 %rvp173426)                                     ; call prim_cdr
  %arg172302 = add i64 0, 0                                                          ; quoted ()
  %cloptr176264 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr176266 = getelementptr inbounds i64, i64* %cloptr176264, i64 1                ; &eptr176266[1]
  %eptr176267 = getelementptr inbounds i64, i64* %cloptr176264, i64 2                ; &eptr176267[2]
  %eptr176268 = getelementptr inbounds i64, i64* %cloptr176264, i64 3                ; &eptr176268[3]
  %eptr176269 = getelementptr inbounds i64, i64* %cloptr176264, i64 4                ; &eptr176269[4]
  store i64 %xK1$_37foldr1, i64* %eptr176266                                         ; *eptr176266 = %xK1$_37foldr1
  store i64 %CZn$_37foldl, i64* %eptr176267                                          ; *eptr176267 = %CZn$_37foldl
  store i64 %zNF$_37foldr, i64* %eptr176268                                          ; *eptr176268 = %zNF$_37foldr
  store i64 %slC$_37map1, i64* %eptr176269                                           ; *eptr176269 = %slC$_37map1
  %eptr176265 = getelementptr inbounds i64, i64* %cloptr176264, i64 0                ; &cloptr176264[0]
  %f176270 = ptrtoint void(i64,i64)* @lam173926 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176270, i64* %eptr176265                                               ; store fptr
  %arg172301 = ptrtoint i64* %cloptr176264 to i64                                    ; closure cast; i64* -> i64
  %rva173425 = add i64 0, 0                                                          ; quoted ()
  %rva173424 = call i64 @prim_cons(i64 %arg172301, i64 %rva173425)                   ; call prim_cons
  %rva173423 = call i64 @prim_cons(i64 %arg172302, i64 %rva173424)                   ; call prim_cons
  %cloptr176271 = inttoptr i64 %cont171499 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176272 = getelementptr inbounds i64, i64* %cloptr176271, i64 0               ; &cloptr176271[0]
  %f176274 = load i64, i64* %i0ptr176272, align 8                                    ; load; *i0ptr176272
  %fptr176273 = inttoptr i64 %f176274 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176273(i64 %cont171499, i64 %rva173423)             ; tail call
  ret void
}


define void @lam173926(i64 %env173927, i64 %gP1$args171501) {
  %envptr176275 = inttoptr i64 %env173927 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176276 = getelementptr inbounds i64, i64* %envptr176275, i64 4              ; &envptr176275[4]
  %slC$_37map1 = load i64, i64* %envptr176276, align 8                               ; load; *envptr176276
  %envptr176277 = inttoptr i64 %env173927 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176278 = getelementptr inbounds i64, i64* %envptr176277, i64 3              ; &envptr176277[3]
  %zNF$_37foldr = load i64, i64* %envptr176278, align 8                              ; load; *envptr176278
  %envptr176279 = inttoptr i64 %env173927 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176280 = getelementptr inbounds i64, i64* %envptr176279, i64 2              ; &envptr176279[2]
  %CZn$_37foldl = load i64, i64* %envptr176280, align 8                              ; load; *envptr176280
  %envptr176281 = inttoptr i64 %env173927 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176282 = getelementptr inbounds i64, i64* %envptr176281, i64 1              ; &envptr176281[1]
  %xK1$_37foldr1 = load i64, i64* %envptr176282, align 8                             ; load; *envptr176282
  %cont171500 = call i64 @prim_car(i64 %gP1$args171501)                              ; call prim_car
  %gP1$args = call i64 @prim_cdr(i64 %gP1$args171501)                                ; call prim_cdr
  %Wco$f = call i64 @prim_car(i64 %gP1$args)                                         ; call prim_car
  %a171241 = call i64 @prim_cdr(i64 %gP1$args)                                       ; call prim_cdr
  %retprim171520 = call i64 @prim_car(i64 %a171241)                                  ; call prim_car
  %cloptr176283 = call i64* @alloc(i64 64)                                           ; malloc
  %eptr176285 = getelementptr inbounds i64, i64* %cloptr176283, i64 1                ; &eptr176285[1]
  %eptr176286 = getelementptr inbounds i64, i64* %cloptr176283, i64 2                ; &eptr176286[2]
  %eptr176287 = getelementptr inbounds i64, i64* %cloptr176283, i64 3                ; &eptr176287[3]
  %eptr176288 = getelementptr inbounds i64, i64* %cloptr176283, i64 4                ; &eptr176288[4]
  %eptr176289 = getelementptr inbounds i64, i64* %cloptr176283, i64 5                ; &eptr176289[5]
  %eptr176290 = getelementptr inbounds i64, i64* %cloptr176283, i64 6                ; &eptr176290[6]
  %eptr176291 = getelementptr inbounds i64, i64* %cloptr176283, i64 7                ; &eptr176291[7]
  store i64 %Wco$f, i64* %eptr176285                                                 ; *eptr176285 = %Wco$f
  store i64 %xK1$_37foldr1, i64* %eptr176286                                         ; *eptr176286 = %xK1$_37foldr1
  store i64 %CZn$_37foldl, i64* %eptr176287                                          ; *eptr176287 = %CZn$_37foldl
  store i64 %zNF$_37foldr, i64* %eptr176288                                          ; *eptr176288 = %zNF$_37foldr
  store i64 %slC$_37map1, i64* %eptr176289                                           ; *eptr176289 = %slC$_37map1
  store i64 %gP1$args, i64* %eptr176290                                              ; *eptr176290 = %gP1$args
  store i64 %cont171500, i64* %eptr176291                                            ; *eptr176291 = %cont171500
  %eptr176284 = getelementptr inbounds i64, i64* %cloptr176283, i64 0                ; &cloptr176283[0]
  %f176292 = ptrtoint void(i64,i64)* @lam173924 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176292, i64* %eptr176284                                               ; store fptr
  %arg172311 = ptrtoint i64* %cloptr176283 to i64                                    ; closure cast; i64* -> i64
  %arg172310 = add i64 0, 0                                                          ; quoted ()
  %rva173422 = add i64 0, 0                                                          ; quoted ()
  %rva173421 = call i64 @prim_cons(i64 %retprim171520, i64 %rva173422)               ; call prim_cons
  %rva173420 = call i64 @prim_cons(i64 %arg172310, i64 %rva173421)                   ; call prim_cons
  %cloptr176293 = inttoptr i64 %arg172311 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176294 = getelementptr inbounds i64, i64* %cloptr176293, i64 0               ; &cloptr176293[0]
  %f176296 = load i64, i64* %i0ptr176294, align 8                                    ; load; *i0ptr176294
  %fptr176295 = inttoptr i64 %f176296 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176295(i64 %arg172311, i64 %rva173420)              ; tail call
  ret void
}


define void @lam173924(i64 %env173925, i64 %rvp173419) {
  %envptr176297 = inttoptr i64 %env173925 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176298 = getelementptr inbounds i64, i64* %envptr176297, i64 7              ; &envptr176297[7]
  %cont171500 = load i64, i64* %envptr176298, align 8                                ; load; *envptr176298
  %envptr176299 = inttoptr i64 %env173925 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176300 = getelementptr inbounds i64, i64* %envptr176299, i64 6              ; &envptr176299[6]
  %gP1$args = load i64, i64* %envptr176300, align 8                                  ; load; *envptr176300
  %envptr176301 = inttoptr i64 %env173925 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176302 = getelementptr inbounds i64, i64* %envptr176301, i64 5              ; &envptr176301[5]
  %slC$_37map1 = load i64, i64* %envptr176302, align 8                               ; load; *envptr176302
  %envptr176303 = inttoptr i64 %env173925 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176304 = getelementptr inbounds i64, i64* %envptr176303, i64 4              ; &envptr176303[4]
  %zNF$_37foldr = load i64, i64* %envptr176304, align 8                              ; load; *envptr176304
  %envptr176305 = inttoptr i64 %env173925 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176306 = getelementptr inbounds i64, i64* %envptr176305, i64 3              ; &envptr176305[3]
  %CZn$_37foldl = load i64, i64* %envptr176306, align 8                              ; load; *envptr176306
  %envptr176307 = inttoptr i64 %env173925 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176308 = getelementptr inbounds i64, i64* %envptr176307, i64 2              ; &envptr176307[2]
  %xK1$_37foldr1 = load i64, i64* %envptr176308, align 8                             ; load; *envptr176308
  %envptr176309 = inttoptr i64 %env173925 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176310 = getelementptr inbounds i64, i64* %envptr176309, i64 1              ; &envptr176309[1]
  %Wco$f = load i64, i64* %envptr176310, align 8                                     ; load; *envptr176310
  %_95171502 = call i64 @prim_car(i64 %rvp173419)                                    ; call prim_car
  %rvp173418 = call i64 @prim_cdr(i64 %rvp173419)                                    ; call prim_cdr
  %D75$acc = call i64 @prim_car(i64 %rvp173418)                                      ; call prim_car
  %na173336 = call i64 @prim_cdr(i64 %rvp173418)                                     ; call prim_cdr
  %a171242 = call i64 @prim_cdr(i64 %gP1$args)                                       ; call prim_cdr
  %retprim171519 = call i64 @prim_cdr(i64 %a171242)                                  ; call prim_cdr
  %cloptr176311 = call i64* @alloc(i64 64)                                           ; malloc
  %eptr176313 = getelementptr inbounds i64, i64* %cloptr176311, i64 1                ; &eptr176313[1]
  %eptr176314 = getelementptr inbounds i64, i64* %cloptr176311, i64 2                ; &eptr176314[2]
  %eptr176315 = getelementptr inbounds i64, i64* %cloptr176311, i64 3                ; &eptr176315[3]
  %eptr176316 = getelementptr inbounds i64, i64* %cloptr176311, i64 4                ; &eptr176316[4]
  %eptr176317 = getelementptr inbounds i64, i64* %cloptr176311, i64 5                ; &eptr176317[5]
  %eptr176318 = getelementptr inbounds i64, i64* %cloptr176311, i64 6                ; &eptr176318[6]
  %eptr176319 = getelementptr inbounds i64, i64* %cloptr176311, i64 7                ; &eptr176319[7]
  store i64 %Wco$f, i64* %eptr176313                                                 ; *eptr176313 = %Wco$f
  store i64 %xK1$_37foldr1, i64* %eptr176314                                         ; *eptr176314 = %xK1$_37foldr1
  store i64 %D75$acc, i64* %eptr176315                                               ; *eptr176315 = %D75$acc
  store i64 %CZn$_37foldl, i64* %eptr176316                                          ; *eptr176316 = %CZn$_37foldl
  store i64 %zNF$_37foldr, i64* %eptr176317                                          ; *eptr176317 = %zNF$_37foldr
  store i64 %slC$_37map1, i64* %eptr176318                                           ; *eptr176318 = %slC$_37map1
  store i64 %cont171500, i64* %eptr176319                                            ; *eptr176319 = %cont171500
  %eptr176312 = getelementptr inbounds i64, i64* %cloptr176311, i64 0                ; &cloptr176311[0]
  %f176320 = ptrtoint void(i64,i64)* @lam173922 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176320, i64* %eptr176312                                               ; store fptr
  %arg172316 = ptrtoint i64* %cloptr176311 to i64                                    ; closure cast; i64* -> i64
  %arg172315 = add i64 0, 0                                                          ; quoted ()
  %rva173417 = add i64 0, 0                                                          ; quoted ()
  %rva173416 = call i64 @prim_cons(i64 %retprim171519, i64 %rva173417)               ; call prim_cons
  %rva173415 = call i64 @prim_cons(i64 %arg172315, i64 %rva173416)                   ; call prim_cons
  %cloptr176321 = inttoptr i64 %arg172316 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176322 = getelementptr inbounds i64, i64* %cloptr176321, i64 0               ; &cloptr176321[0]
  %f176324 = load i64, i64* %i0ptr176322, align 8                                    ; load; *i0ptr176322
  %fptr176323 = inttoptr i64 %f176324 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176323(i64 %arg172316, i64 %rva173415)              ; tail call
  ret void
}


define void @lam173922(i64 %env173923, i64 %rvp173414) {
  %envptr176325 = inttoptr i64 %env173923 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176326 = getelementptr inbounds i64, i64* %envptr176325, i64 7              ; &envptr176325[7]
  %cont171500 = load i64, i64* %envptr176326, align 8                                ; load; *envptr176326
  %envptr176327 = inttoptr i64 %env173923 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176328 = getelementptr inbounds i64, i64* %envptr176327, i64 6              ; &envptr176327[6]
  %slC$_37map1 = load i64, i64* %envptr176328, align 8                               ; load; *envptr176328
  %envptr176329 = inttoptr i64 %env173923 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176330 = getelementptr inbounds i64, i64* %envptr176329, i64 5              ; &envptr176329[5]
  %zNF$_37foldr = load i64, i64* %envptr176330, align 8                              ; load; *envptr176330
  %envptr176331 = inttoptr i64 %env173923 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176332 = getelementptr inbounds i64, i64* %envptr176331, i64 4              ; &envptr176331[4]
  %CZn$_37foldl = load i64, i64* %envptr176332, align 8                              ; load; *envptr176332
  %envptr176333 = inttoptr i64 %env173923 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176334 = getelementptr inbounds i64, i64* %envptr176333, i64 3              ; &envptr176333[3]
  %D75$acc = load i64, i64* %envptr176334, align 8                                   ; load; *envptr176334
  %envptr176335 = inttoptr i64 %env173923 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176336 = getelementptr inbounds i64, i64* %envptr176335, i64 2              ; &envptr176335[2]
  %xK1$_37foldr1 = load i64, i64* %envptr176336, align 8                             ; load; *envptr176336
  %envptr176337 = inttoptr i64 %env173923 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176338 = getelementptr inbounds i64, i64* %envptr176337, i64 1              ; &envptr176337[1]
  %Wco$f = load i64, i64* %envptr176338, align 8                                     ; load; *envptr176338
  %_95171503 = call i64 @prim_car(i64 %rvp173414)                                    ; call prim_car
  %rvp173413 = call i64 @prim_cdr(i64 %rvp173414)                                    ; call prim_cdr
  %gXS$lsts = call i64 @prim_car(i64 %rvp173413)                                     ; call prim_car
  %na173338 = call i64 @prim_cdr(i64 %rvp173413)                                     ; call prim_cdr
  %cloptr176339 = call i64* @alloc(i64 64)                                           ; malloc
  %eptr176341 = getelementptr inbounds i64, i64* %cloptr176339, i64 1                ; &eptr176341[1]
  %eptr176342 = getelementptr inbounds i64, i64* %cloptr176339, i64 2                ; &eptr176342[2]
  %eptr176343 = getelementptr inbounds i64, i64* %cloptr176339, i64 3                ; &eptr176343[3]
  %eptr176344 = getelementptr inbounds i64, i64* %cloptr176339, i64 4                ; &eptr176344[4]
  %eptr176345 = getelementptr inbounds i64, i64* %cloptr176339, i64 5                ; &eptr176345[5]
  %eptr176346 = getelementptr inbounds i64, i64* %cloptr176339, i64 6                ; &eptr176346[6]
  %eptr176347 = getelementptr inbounds i64, i64* %cloptr176339, i64 7                ; &eptr176347[7]
  store i64 %Wco$f, i64* %eptr176341                                                 ; *eptr176341 = %Wco$f
  store i64 %D75$acc, i64* %eptr176342                                               ; *eptr176342 = %D75$acc
  store i64 %CZn$_37foldl, i64* %eptr176343                                          ; *eptr176343 = %CZn$_37foldl
  store i64 %zNF$_37foldr, i64* %eptr176344                                          ; *eptr176344 = %zNF$_37foldr
  store i64 %gXS$lsts, i64* %eptr176345                                              ; *eptr176345 = %gXS$lsts
  store i64 %slC$_37map1, i64* %eptr176346                                           ; *eptr176346 = %slC$_37map1
  store i64 %cont171500, i64* %eptr176347                                            ; *eptr176347 = %cont171500
  %eptr176340 = getelementptr inbounds i64, i64* %cloptr176339, i64 0                ; &cloptr176339[0]
  %f176348 = ptrtoint void(i64,i64)* @lam173920 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176348, i64* %eptr176340                                               ; store fptr
  %arg172320 = ptrtoint i64* %cloptr176339 to i64                                    ; closure cast; i64* -> i64
  %cloptr176349 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr176350 = getelementptr inbounds i64, i64* %cloptr176349, i64 0                ; &cloptr176349[0]
  %f176351 = ptrtoint void(i64,i64)* @lam173892 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176351, i64* %eptr176350                                               ; store fptr
  %arg172319 = ptrtoint i64* %cloptr176349 to i64                                    ; closure cast; i64* -> i64
  %arg172318 = call i64 @const_init_false()                                          ; quoted #f
  %rva173412 = add i64 0, 0                                                          ; quoted ()
  %rva173411 = call i64 @prim_cons(i64 %gXS$lsts, i64 %rva173412)                    ; call prim_cons
  %rva173410 = call i64 @prim_cons(i64 %arg172318, i64 %rva173411)                   ; call prim_cons
  %rva173409 = call i64 @prim_cons(i64 %arg172319, i64 %rva173410)                   ; call prim_cons
  %rva173408 = call i64 @prim_cons(i64 %arg172320, i64 %rva173409)                   ; call prim_cons
  %cloptr176352 = inttoptr i64 %xK1$_37foldr1 to i64*                                ; closure/env cast; i64 -> i64*
  %i0ptr176353 = getelementptr inbounds i64, i64* %cloptr176352, i64 0               ; &cloptr176352[0]
  %f176355 = load i64, i64* %i0ptr176353, align 8                                    ; load; *i0ptr176353
  %fptr176354 = inttoptr i64 %f176355 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176354(i64 %xK1$_37foldr1, i64 %rva173408)          ; tail call
  ret void
}


define void @lam173920(i64 %env173921, i64 %rvp173396) {
  %envptr176356 = inttoptr i64 %env173921 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176357 = getelementptr inbounds i64, i64* %envptr176356, i64 7              ; &envptr176356[7]
  %cont171500 = load i64, i64* %envptr176357, align 8                                ; load; *envptr176357
  %envptr176358 = inttoptr i64 %env173921 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176359 = getelementptr inbounds i64, i64* %envptr176358, i64 6              ; &envptr176358[6]
  %slC$_37map1 = load i64, i64* %envptr176359, align 8                               ; load; *envptr176359
  %envptr176360 = inttoptr i64 %env173921 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176361 = getelementptr inbounds i64, i64* %envptr176360, i64 5              ; &envptr176360[5]
  %gXS$lsts = load i64, i64* %envptr176361, align 8                                  ; load; *envptr176361
  %envptr176362 = inttoptr i64 %env173921 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176363 = getelementptr inbounds i64, i64* %envptr176362, i64 4              ; &envptr176362[4]
  %zNF$_37foldr = load i64, i64* %envptr176363, align 8                              ; load; *envptr176363
  %envptr176364 = inttoptr i64 %env173921 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176365 = getelementptr inbounds i64, i64* %envptr176364, i64 3              ; &envptr176364[3]
  %CZn$_37foldl = load i64, i64* %envptr176365, align 8                              ; load; *envptr176365
  %envptr176366 = inttoptr i64 %env173921 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176367 = getelementptr inbounds i64, i64* %envptr176366, i64 2              ; &envptr176366[2]
  %D75$acc = load i64, i64* %envptr176367, align 8                                   ; load; *envptr176367
  %envptr176368 = inttoptr i64 %env173921 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176369 = getelementptr inbounds i64, i64* %envptr176368, i64 1              ; &envptr176368[1]
  %Wco$f = load i64, i64* %envptr176369, align 8                                     ; load; *envptr176369
  %_95171504 = call i64 @prim_car(i64 %rvp173396)                                    ; call prim_car
  %rvp173395 = call i64 @prim_cdr(i64 %rvp173396)                                    ; call prim_cdr
  %a171243 = call i64 @prim_car(i64 %rvp173395)                                      ; call prim_car
  %na173340 = call i64 @prim_cdr(i64 %rvp173395)                                     ; call prim_cdr
  %cmp176370 = icmp eq i64 %a171243, 15                                              ; false?
  br i1 %cmp176370, label %else176372, label %then176371                             ; if

then176371:
  %arg172323 = add i64 0, 0                                                          ; quoted ()
  %rva173343 = add i64 0, 0                                                          ; quoted ()
  %rva173342 = call i64 @prim_cons(i64 %D75$acc, i64 %rva173343)                     ; call prim_cons
  %rva173341 = call i64 @prim_cons(i64 %arg172323, i64 %rva173342)                   ; call prim_cons
  %cloptr176373 = inttoptr i64 %cont171500 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176374 = getelementptr inbounds i64, i64* %cloptr176373, i64 0               ; &cloptr176373[0]
  %f176376 = load i64, i64* %i0ptr176374, align 8                                    ; load; *i0ptr176374
  %fptr176375 = inttoptr i64 %f176376 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176375(i64 %cont171500, i64 %rva173341)             ; tail call
  ret void

else176372:
  %cloptr176377 = call i64* @alloc(i64 64)                                           ; malloc
  %eptr176379 = getelementptr inbounds i64, i64* %cloptr176377, i64 1                ; &eptr176379[1]
  %eptr176380 = getelementptr inbounds i64, i64* %cloptr176377, i64 2                ; &eptr176380[2]
  %eptr176381 = getelementptr inbounds i64, i64* %cloptr176377, i64 3                ; &eptr176381[3]
  %eptr176382 = getelementptr inbounds i64, i64* %cloptr176377, i64 4                ; &eptr176382[4]
  %eptr176383 = getelementptr inbounds i64, i64* %cloptr176377, i64 5                ; &eptr176383[5]
  %eptr176384 = getelementptr inbounds i64, i64* %cloptr176377, i64 6                ; &eptr176384[6]
  %eptr176385 = getelementptr inbounds i64, i64* %cloptr176377, i64 7                ; &eptr176385[7]
  store i64 %Wco$f, i64* %eptr176379                                                 ; *eptr176379 = %Wco$f
  store i64 %D75$acc, i64* %eptr176380                                               ; *eptr176380 = %D75$acc
  store i64 %CZn$_37foldl, i64* %eptr176381                                          ; *eptr176381 = %CZn$_37foldl
  store i64 %zNF$_37foldr, i64* %eptr176382                                          ; *eptr176382 = %zNF$_37foldr
  store i64 %gXS$lsts, i64* %eptr176383                                              ; *eptr176383 = %gXS$lsts
  store i64 %slC$_37map1, i64* %eptr176384                                           ; *eptr176384 = %slC$_37map1
  store i64 %cont171500, i64* %eptr176385                                            ; *eptr176385 = %cont171500
  %eptr176378 = getelementptr inbounds i64, i64* %cloptr176377, i64 0                ; &cloptr176377[0]
  %f176386 = ptrtoint void(i64,i64)* @lam173918 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176386, i64* %eptr176378                                               ; store fptr
  %arg172327 = ptrtoint i64* %cloptr176377 to i64                                    ; closure cast; i64* -> i64
  %cloptr176387 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr176388 = getelementptr inbounds i64, i64* %cloptr176387, i64 0                ; &cloptr176387[0]
  %f176389 = ptrtoint void(i64,i64)* @lam173899 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176389, i64* %eptr176388                                               ; store fptr
  %arg172326 = ptrtoint i64* %cloptr176387 to i64                                    ; closure cast; i64* -> i64
  %rva173394 = add i64 0, 0                                                          ; quoted ()
  %rva173393 = call i64 @prim_cons(i64 %gXS$lsts, i64 %rva173394)                    ; call prim_cons
  %rva173392 = call i64 @prim_cons(i64 %arg172326, i64 %rva173393)                   ; call prim_cons
  %rva173391 = call i64 @prim_cons(i64 %arg172327, i64 %rva173392)                   ; call prim_cons
  %cloptr176390 = inttoptr i64 %slC$_37map1 to i64*                                  ; closure/env cast; i64 -> i64*
  %i0ptr176391 = getelementptr inbounds i64, i64* %cloptr176390, i64 0               ; &cloptr176390[0]
  %f176393 = load i64, i64* %i0ptr176391, align 8                                    ; load; *i0ptr176391
  %fptr176392 = inttoptr i64 %f176393 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176392(i64 %slC$_37map1, i64 %rva173391)            ; tail call
  ret void
}


define void @lam173918(i64 %env173919, i64 %rvp173383) {
  %envptr176394 = inttoptr i64 %env173919 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176395 = getelementptr inbounds i64, i64* %envptr176394, i64 7              ; &envptr176394[7]
  %cont171500 = load i64, i64* %envptr176395, align 8                                ; load; *envptr176395
  %envptr176396 = inttoptr i64 %env173919 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176397 = getelementptr inbounds i64, i64* %envptr176396, i64 6              ; &envptr176396[6]
  %slC$_37map1 = load i64, i64* %envptr176397, align 8                               ; load; *envptr176397
  %envptr176398 = inttoptr i64 %env173919 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176399 = getelementptr inbounds i64, i64* %envptr176398, i64 5              ; &envptr176398[5]
  %gXS$lsts = load i64, i64* %envptr176399, align 8                                  ; load; *envptr176399
  %envptr176400 = inttoptr i64 %env173919 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176401 = getelementptr inbounds i64, i64* %envptr176400, i64 4              ; &envptr176400[4]
  %zNF$_37foldr = load i64, i64* %envptr176401, align 8                              ; load; *envptr176401
  %envptr176402 = inttoptr i64 %env173919 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176403 = getelementptr inbounds i64, i64* %envptr176402, i64 3              ; &envptr176402[3]
  %CZn$_37foldl = load i64, i64* %envptr176403, align 8                              ; load; *envptr176403
  %envptr176404 = inttoptr i64 %env173919 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176405 = getelementptr inbounds i64, i64* %envptr176404, i64 2              ; &envptr176404[2]
  %D75$acc = load i64, i64* %envptr176405, align 8                                   ; load; *envptr176405
  %envptr176406 = inttoptr i64 %env173919 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176407 = getelementptr inbounds i64, i64* %envptr176406, i64 1              ; &envptr176406[1]
  %Wco$f = load i64, i64* %envptr176407, align 8                                     ; load; *envptr176407
  %_95171505 = call i64 @prim_car(i64 %rvp173383)                                    ; call prim_car
  %rvp173382 = call i64 @prim_cdr(i64 %rvp173383)                                    ; call prim_cdr
  %XGa$lsts_43 = call i64 @prim_car(i64 %rvp173382)                                  ; call prim_car
  %na173345 = call i64 @prim_cdr(i64 %rvp173382)                                     ; call prim_cdr
  %cloptr176408 = call i64* @alloc(i64 56)                                           ; malloc
  %eptr176410 = getelementptr inbounds i64, i64* %cloptr176408, i64 1                ; &eptr176410[1]
  %eptr176411 = getelementptr inbounds i64, i64* %cloptr176408, i64 2                ; &eptr176411[2]
  %eptr176412 = getelementptr inbounds i64, i64* %cloptr176408, i64 3                ; &eptr176412[3]
  %eptr176413 = getelementptr inbounds i64, i64* %cloptr176408, i64 4                ; &eptr176413[4]
  %eptr176414 = getelementptr inbounds i64, i64* %cloptr176408, i64 5                ; &eptr176414[5]
  %eptr176415 = getelementptr inbounds i64, i64* %cloptr176408, i64 6                ; &eptr176415[6]
  store i64 %Wco$f, i64* %eptr176410                                                 ; *eptr176410 = %Wco$f
  store i64 %XGa$lsts_43, i64* %eptr176411                                           ; *eptr176411 = %XGa$lsts_43
  store i64 %D75$acc, i64* %eptr176412                                               ; *eptr176412 = %D75$acc
  store i64 %CZn$_37foldl, i64* %eptr176413                                          ; *eptr176413 = %CZn$_37foldl
  store i64 %zNF$_37foldr, i64* %eptr176414                                          ; *eptr176414 = %zNF$_37foldr
  store i64 %cont171500, i64* %eptr176415                                            ; *eptr176415 = %cont171500
  %eptr176409 = getelementptr inbounds i64, i64* %cloptr176408, i64 0                ; &cloptr176408[0]
  %f176416 = ptrtoint void(i64,i64)* @lam173916 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176416, i64* %eptr176409                                               ; store fptr
  %arg172331 = ptrtoint i64* %cloptr176408 to i64                                    ; closure cast; i64* -> i64
  %cloptr176417 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr176418 = getelementptr inbounds i64, i64* %cloptr176417, i64 0                ; &cloptr176417[0]
  %f176419 = ptrtoint void(i64,i64)* @lam173904 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176419, i64* %eptr176418                                               ; store fptr
  %arg172330 = ptrtoint i64* %cloptr176417 to i64                                    ; closure cast; i64* -> i64
  %rva173381 = add i64 0, 0                                                          ; quoted ()
  %rva173380 = call i64 @prim_cons(i64 %gXS$lsts, i64 %rva173381)                    ; call prim_cons
  %rva173379 = call i64 @prim_cons(i64 %arg172330, i64 %rva173380)                   ; call prim_cons
  %rva173378 = call i64 @prim_cons(i64 %arg172331, i64 %rva173379)                   ; call prim_cons
  %cloptr176420 = inttoptr i64 %slC$_37map1 to i64*                                  ; closure/env cast; i64 -> i64*
  %i0ptr176421 = getelementptr inbounds i64, i64* %cloptr176420, i64 0               ; &cloptr176420[0]
  %f176423 = load i64, i64* %i0ptr176421, align 8                                    ; load; *i0ptr176421
  %fptr176422 = inttoptr i64 %f176423 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176422(i64 %slC$_37map1, i64 %rva173378)            ; tail call
  ret void
}


define void @lam173916(i64 %env173917, i64 %rvp173370) {
  %envptr176424 = inttoptr i64 %env173917 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176425 = getelementptr inbounds i64, i64* %envptr176424, i64 6              ; &envptr176424[6]
  %cont171500 = load i64, i64* %envptr176425, align 8                                ; load; *envptr176425
  %envptr176426 = inttoptr i64 %env173917 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176427 = getelementptr inbounds i64, i64* %envptr176426, i64 5              ; &envptr176426[5]
  %zNF$_37foldr = load i64, i64* %envptr176427, align 8                              ; load; *envptr176427
  %envptr176428 = inttoptr i64 %env173917 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176429 = getelementptr inbounds i64, i64* %envptr176428, i64 4              ; &envptr176428[4]
  %CZn$_37foldl = load i64, i64* %envptr176429, align 8                              ; load; *envptr176429
  %envptr176430 = inttoptr i64 %env173917 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176431 = getelementptr inbounds i64, i64* %envptr176430, i64 3              ; &envptr176430[3]
  %D75$acc = load i64, i64* %envptr176431, align 8                                   ; load; *envptr176431
  %envptr176432 = inttoptr i64 %env173917 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176433 = getelementptr inbounds i64, i64* %envptr176432, i64 2              ; &envptr176432[2]
  %XGa$lsts_43 = load i64, i64* %envptr176433, align 8                               ; load; *envptr176433
  %envptr176434 = inttoptr i64 %env173917 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176435 = getelementptr inbounds i64, i64* %envptr176434, i64 1              ; &envptr176434[1]
  %Wco$f = load i64, i64* %envptr176435, align 8                                     ; load; *envptr176435
  %_95171506 = call i64 @prim_car(i64 %rvp173370)                                    ; call prim_car
  %rvp173369 = call i64 @prim_cdr(i64 %rvp173370)                                    ; call prim_cdr
  %mrp$vs = call i64 @prim_car(i64 %rvp173369)                                       ; call prim_car
  %na173347 = call i64 @prim_cdr(i64 %rvp173369)                                     ; call prim_cdr
  %arg172333 = add i64 0, 0                                                          ; quoted ()
  %a171244 = call i64 @prim_cons(i64 %D75$acc, i64 %arg172333)                       ; call prim_cons
  %cloptr176436 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr176438 = getelementptr inbounds i64, i64* %cloptr176436, i64 1                ; &eptr176438[1]
  %eptr176439 = getelementptr inbounds i64, i64* %cloptr176436, i64 2                ; &eptr176439[2]
  %eptr176440 = getelementptr inbounds i64, i64* %cloptr176436, i64 3                ; &eptr176440[3]
  %eptr176441 = getelementptr inbounds i64, i64* %cloptr176436, i64 4                ; &eptr176441[4]
  store i64 %Wco$f, i64* %eptr176438                                                 ; *eptr176438 = %Wco$f
  store i64 %XGa$lsts_43, i64* %eptr176439                                           ; *eptr176439 = %XGa$lsts_43
  store i64 %CZn$_37foldl, i64* %eptr176440                                          ; *eptr176440 = %CZn$_37foldl
  store i64 %cont171500, i64* %eptr176441                                            ; *eptr176441 = %cont171500
  %eptr176437 = getelementptr inbounds i64, i64* %cloptr176436, i64 0                ; &cloptr176436[0]
  %f176442 = ptrtoint void(i64,i64)* @lam173913 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176442, i64* %eptr176437                                               ; store fptr
  %arg172338 = ptrtoint i64* %cloptr176436 to i64                                    ; closure cast; i64* -> i64
  %cloptr176443 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr176444 = getelementptr inbounds i64, i64* %cloptr176443, i64 0                ; &cloptr176443[0]
  %f176445 = ptrtoint void(i64,i64)* @lam173909 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176445, i64* %eptr176444                                               ; store fptr
  %arg172337 = ptrtoint i64* %cloptr176443 to i64                                    ; closure cast; i64* -> i64
  %rva173368 = add i64 0, 0                                                          ; quoted ()
  %rva173367 = call i64 @prim_cons(i64 %mrp$vs, i64 %rva173368)                      ; call prim_cons
  %rva173366 = call i64 @prim_cons(i64 %a171244, i64 %rva173367)                     ; call prim_cons
  %rva173365 = call i64 @prim_cons(i64 %arg172337, i64 %rva173366)                   ; call prim_cons
  %rva173364 = call i64 @prim_cons(i64 %arg172338, i64 %rva173365)                   ; call prim_cons
  %cloptr176446 = inttoptr i64 %zNF$_37foldr to i64*                                 ; closure/env cast; i64 -> i64*
  %i0ptr176447 = getelementptr inbounds i64, i64* %cloptr176446, i64 0               ; &cloptr176446[0]
  %f176449 = load i64, i64* %i0ptr176447, align 8                                    ; load; *i0ptr176447
  %fptr176448 = inttoptr i64 %f176449 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176448(i64 %zNF$_37foldr, i64 %rva173364)           ; tail call
  ret void
}


define void @lam173913(i64 %env173914, i64 %rvp173355) {
  %envptr176450 = inttoptr i64 %env173914 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176451 = getelementptr inbounds i64, i64* %envptr176450, i64 4              ; &envptr176450[4]
  %cont171500 = load i64, i64* %envptr176451, align 8                                ; load; *envptr176451
  %envptr176452 = inttoptr i64 %env173914 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176453 = getelementptr inbounds i64, i64* %envptr176452, i64 3              ; &envptr176452[3]
  %CZn$_37foldl = load i64, i64* %envptr176453, align 8                              ; load; *envptr176453
  %envptr176454 = inttoptr i64 %env173914 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176455 = getelementptr inbounds i64, i64* %envptr176454, i64 2              ; &envptr176454[2]
  %XGa$lsts_43 = load i64, i64* %envptr176455, align 8                               ; load; *envptr176455
  %envptr176456 = inttoptr i64 %env173914 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176457 = getelementptr inbounds i64, i64* %envptr176456, i64 1              ; &envptr176456[1]
  %Wco$f = load i64, i64* %envptr176457, align 8                                     ; load; *envptr176457
  %_95171509 = call i64 @prim_car(i64 %rvp173355)                                    ; call prim_car
  %rvp173354 = call i64 @prim_cdr(i64 %rvp173355)                                    ; call prim_cdr
  %a171245 = call i64 @prim_car(i64 %rvp173354)                                      ; call prim_car
  %na173349 = call i64 @prim_cdr(i64 %rvp173354)                                     ; call prim_cdr
  %cloptr176458 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr176460 = getelementptr inbounds i64, i64* %cloptr176458, i64 1                ; &eptr176460[1]
  %eptr176461 = getelementptr inbounds i64, i64* %cloptr176458, i64 2                ; &eptr176461[2]
  %eptr176462 = getelementptr inbounds i64, i64* %cloptr176458, i64 3                ; &eptr176462[3]
  %eptr176463 = getelementptr inbounds i64, i64* %cloptr176458, i64 4                ; &eptr176463[4]
  store i64 %Wco$f, i64* %eptr176460                                                 ; *eptr176460 = %Wco$f
  store i64 %XGa$lsts_43, i64* %eptr176461                                           ; *eptr176461 = %XGa$lsts_43
  store i64 %CZn$_37foldl, i64* %eptr176462                                          ; *eptr176462 = %CZn$_37foldl
  store i64 %cont171500, i64* %eptr176463                                            ; *eptr176463 = %cont171500
  %eptr176459 = getelementptr inbounds i64, i64* %cloptr176458, i64 0                ; &cloptr176458[0]
  %f176464 = ptrtoint void(i64,i64)* @lam173911 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176464, i64* %eptr176459                                               ; store fptr
  %arg172341 = ptrtoint i64* %cloptr176458 to i64                                    ; closure cast; i64* -> i64
  %cps_45lst171510 = call i64 @prim_cons(i64 %arg172341, i64 %a171245)               ; call prim_cons
  %cloptr176465 = inttoptr i64 %Wco$f to i64*                                        ; closure/env cast; i64 -> i64*
  %i0ptr176466 = getelementptr inbounds i64, i64* %cloptr176465, i64 0               ; &cloptr176465[0]
  %f176468 = load i64, i64* %i0ptr176466, align 8                                    ; load; *i0ptr176466
  %fptr176467 = inttoptr i64 %f176468 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176467(i64 %Wco$f, i64 %cps_45lst171510)            ; tail call
  ret void
}


define void @lam173911(i64 %env173912, i64 %rvp173353) {
  %envptr176469 = inttoptr i64 %env173912 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176470 = getelementptr inbounds i64, i64* %envptr176469, i64 4              ; &envptr176469[4]
  %cont171500 = load i64, i64* %envptr176470, align 8                                ; load; *envptr176470
  %envptr176471 = inttoptr i64 %env173912 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176472 = getelementptr inbounds i64, i64* %envptr176471, i64 3              ; &envptr176471[3]
  %CZn$_37foldl = load i64, i64* %envptr176472, align 8                              ; load; *envptr176472
  %envptr176473 = inttoptr i64 %env173912 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176474 = getelementptr inbounds i64, i64* %envptr176473, i64 2              ; &envptr176473[2]
  %XGa$lsts_43 = load i64, i64* %envptr176474, align 8                               ; load; *envptr176474
  %envptr176475 = inttoptr i64 %env173912 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176476 = getelementptr inbounds i64, i64* %envptr176475, i64 1              ; &envptr176475[1]
  %Wco$f = load i64, i64* %envptr176476, align 8                                     ; load; *envptr176476
  %_95171507 = call i64 @prim_car(i64 %rvp173353)                                    ; call prim_car
  %rvp173352 = call i64 @prim_cdr(i64 %rvp173353)                                    ; call prim_cdr
  %AQk$acc_43 = call i64 @prim_car(i64 %rvp173352)                                   ; call prim_car
  %na173351 = call i64 @prim_cdr(i64 %rvp173352)                                     ; call prim_cdr
  %a171246 = call i64 @prim_cons(i64 %AQk$acc_43, i64 %XGa$lsts_43)                  ; call prim_cons
  %a171247 = call i64 @prim_cons(i64 %Wco$f, i64 %a171246)                           ; call prim_cons
  %cps_45lst171508 = call i64 @prim_cons(i64 %cont171500, i64 %a171247)              ; call prim_cons
  %cloptr176477 = inttoptr i64 %CZn$_37foldl to i64*                                 ; closure/env cast; i64 -> i64*
  %i0ptr176478 = getelementptr inbounds i64, i64* %cloptr176477, i64 0               ; &cloptr176477[0]
  %f176480 = load i64, i64* %i0ptr176478, align 8                                    ; load; *i0ptr176478
  %fptr176479 = inttoptr i64 %f176480 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176479(i64 %CZn$_37foldl, i64 %cps_45lst171508)     ; tail call
  ret void
}


define void @lam173909(i64 %env173910, i64 %rvp173363) {
  %cont171511 = call i64 @prim_car(i64 %rvp173363)                                   ; call prim_car
  %rvp173362 = call i64 @prim_cdr(i64 %rvp173363)                                    ; call prim_cdr
  %Vmh$a = call i64 @prim_car(i64 %rvp173362)                                        ; call prim_car
  %rvp173361 = call i64 @prim_cdr(i64 %rvp173362)                                    ; call prim_cdr
  %x44$b = call i64 @prim_car(i64 %rvp173361)                                        ; call prim_car
  %na173357 = call i64 @prim_cdr(i64 %rvp173361)                                     ; call prim_cdr
  %retprim171512 = call i64 @prim_cons(i64 %Vmh$a, i64 %x44$b)                       ; call prim_cons
  %arg172351 = add i64 0, 0                                                          ; quoted ()
  %rva173360 = add i64 0, 0                                                          ; quoted ()
  %rva173359 = call i64 @prim_cons(i64 %retprim171512, i64 %rva173360)               ; call prim_cons
  %rva173358 = call i64 @prim_cons(i64 %arg172351, i64 %rva173359)                   ; call prim_cons
  %cloptr176481 = inttoptr i64 %cont171511 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176482 = getelementptr inbounds i64, i64* %cloptr176481, i64 0               ; &cloptr176481[0]
  %f176484 = load i64, i64* %i0ptr176482, align 8                                    ; load; *i0ptr176482
  %fptr176483 = inttoptr i64 %f176484 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176483(i64 %cont171511, i64 %rva173358)             ; tail call
  ret void
}


define void @lam173904(i64 %env173905, i64 %rvp173377) {
  %cont171513 = call i64 @prim_car(i64 %rvp173377)                                   ; call prim_car
  %rvp173376 = call i64 @prim_cdr(i64 %rvp173377)                                    ; call prim_cdr
  %Joe$x = call i64 @prim_car(i64 %rvp173376)                                        ; call prim_car
  %na173372 = call i64 @prim_cdr(i64 %rvp173376)                                     ; call prim_cdr
  %retprim171514 = call i64 @prim_car(i64 %Joe$x)                                    ; call prim_car
  %arg172355 = add i64 0, 0                                                          ; quoted ()
  %rva173375 = add i64 0, 0                                                          ; quoted ()
  %rva173374 = call i64 @prim_cons(i64 %retprim171514, i64 %rva173375)               ; call prim_cons
  %rva173373 = call i64 @prim_cons(i64 %arg172355, i64 %rva173374)                   ; call prim_cons
  %cloptr176485 = inttoptr i64 %cont171513 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176486 = getelementptr inbounds i64, i64* %cloptr176485, i64 0               ; &cloptr176485[0]
  %f176488 = load i64, i64* %i0ptr176486, align 8                                    ; load; *i0ptr176486
  %fptr176487 = inttoptr i64 %f176488 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176487(i64 %cont171513, i64 %rva173373)             ; tail call
  ret void
}


define void @lam173899(i64 %env173900, i64 %rvp173390) {
  %cont171515 = call i64 @prim_car(i64 %rvp173390)                                   ; call prim_car
  %rvp173389 = call i64 @prim_cdr(i64 %rvp173390)                                    ; call prim_cdr
  %Hd8$x = call i64 @prim_car(i64 %rvp173389)                                        ; call prim_car
  %na173385 = call i64 @prim_cdr(i64 %rvp173389)                                     ; call prim_cdr
  %retprim171516 = call i64 @prim_cdr(i64 %Hd8$x)                                    ; call prim_cdr
  %arg172359 = add i64 0, 0                                                          ; quoted ()
  %rva173388 = add i64 0, 0                                                          ; quoted ()
  %rva173387 = call i64 @prim_cons(i64 %retprim171516, i64 %rva173388)               ; call prim_cons
  %rva173386 = call i64 @prim_cons(i64 %arg172359, i64 %rva173387)                   ; call prim_cons
  %cloptr176489 = inttoptr i64 %cont171515 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176490 = getelementptr inbounds i64, i64* %cloptr176489, i64 0               ; &cloptr176489[0]
  %f176492 = load i64, i64* %i0ptr176490, align 8                                    ; load; *i0ptr176490
  %fptr176491 = inttoptr i64 %f176492 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176491(i64 %cont171515, i64 %rva173386)             ; tail call
  ret void
}


define void @lam173892(i64 %env173893, i64 %rvp173407) {
  %cont171517 = call i64 @prim_car(i64 %rvp173407)                                   ; call prim_car
  %rvp173406 = call i64 @prim_cdr(i64 %rvp173407)                                    ; call prim_cdr
  %x0i$lst = call i64 @prim_car(i64 %rvp173406)                                      ; call prim_car
  %rvp173405 = call i64 @prim_cdr(i64 %rvp173406)                                    ; call prim_cdr
  %lJ5$b = call i64 @prim_car(i64 %rvp173405)                                        ; call prim_car
  %na173398 = call i64 @prim_cdr(i64 %rvp173405)                                     ; call prim_cdr
  %cmp176493 = icmp eq i64 %lJ5$b, 15                                                ; false?
  br i1 %cmp176493, label %else176495, label %then176494                             ; if

then176494:
  %arg172362 = add i64 0, 0                                                          ; quoted ()
  %rva173401 = add i64 0, 0                                                          ; quoted ()
  %rva173400 = call i64 @prim_cons(i64 %lJ5$b, i64 %rva173401)                       ; call prim_cons
  %rva173399 = call i64 @prim_cons(i64 %arg172362, i64 %rva173400)                   ; call prim_cons
  %cloptr176496 = inttoptr i64 %cont171517 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176497 = getelementptr inbounds i64, i64* %cloptr176496, i64 0               ; &cloptr176496[0]
  %f176499 = load i64, i64* %i0ptr176497, align 8                                    ; load; *i0ptr176497
  %fptr176498 = inttoptr i64 %f176499 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176498(i64 %cont171517, i64 %rva173399)             ; tail call
  ret void

else176495:
  %retprim171518 = call i64 @prim_null_63(i64 %x0i$lst)                              ; call prim_null_63
  %arg172366 = add i64 0, 0                                                          ; quoted ()
  %rva173404 = add i64 0, 0                                                          ; quoted ()
  %rva173403 = call i64 @prim_cons(i64 %retprim171518, i64 %rva173404)               ; call prim_cons
  %rva173402 = call i64 @prim_cons(i64 %arg172366, i64 %rva173403)                   ; call prim_cons
  %cloptr176500 = inttoptr i64 %cont171517 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176501 = getelementptr inbounds i64, i64* %cloptr176500, i64 0               ; &cloptr176500[0]
  %f176503 = load i64, i64* %i0ptr176501, align 8                                    ; load; *i0ptr176501
  %fptr176502 = inttoptr i64 %f176503 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176502(i64 %cont171517, i64 %rva173402)             ; tail call
  ret void
}


define void @lam173878(i64 %env173879, i64 %rvp173527) {
  %envptr176504 = inttoptr i64 %env173879 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176505 = getelementptr inbounds i64, i64* %envptr176504, i64 2              ; &envptr176504[2]
  %K1g$_37map1 = load i64, i64* %envptr176505, align 8                               ; load; *envptr176505
  %envptr176506 = inttoptr i64 %env173879 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176507 = getelementptr inbounds i64, i64* %envptr176506, i64 1              ; &envptr176506[1]
  %xK1$_37foldr1 = load i64, i64* %envptr176507, align 8                             ; load; *envptr176507
  %cont171521 = call i64 @prim_car(i64 %rvp173527)                                   ; call prim_car
  %rvp173526 = call i64 @prim_cdr(i64 %rvp173527)                                    ; call prim_cdr
  %w0A$_37foldr = call i64 @prim_car(i64 %rvp173526)                                 ; call prim_car
  %na173434 = call i64 @prim_cdr(i64 %rvp173526)                                     ; call prim_cdr
  %arg172369 = add i64 0, 0                                                          ; quoted ()
  %cloptr176508 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr176510 = getelementptr inbounds i64, i64* %cloptr176508, i64 1                ; &eptr176510[1]
  %eptr176511 = getelementptr inbounds i64, i64* %cloptr176508, i64 2                ; &eptr176511[2]
  %eptr176512 = getelementptr inbounds i64, i64* %cloptr176508, i64 3                ; &eptr176512[3]
  store i64 %xK1$_37foldr1, i64* %eptr176510                                         ; *eptr176510 = %xK1$_37foldr1
  store i64 %K1g$_37map1, i64* %eptr176511                                           ; *eptr176511 = %K1g$_37map1
  store i64 %w0A$_37foldr, i64* %eptr176512                                          ; *eptr176512 = %w0A$_37foldr
  %eptr176509 = getelementptr inbounds i64, i64* %cloptr176508, i64 0                ; &cloptr176508[0]
  %f176513 = ptrtoint void(i64,i64)* @lam173875 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176513, i64* %eptr176509                                               ; store fptr
  %arg172368 = ptrtoint i64* %cloptr176508 to i64                                    ; closure cast; i64* -> i64
  %rva173525 = add i64 0, 0                                                          ; quoted ()
  %rva173524 = call i64 @prim_cons(i64 %arg172368, i64 %rva173525)                   ; call prim_cons
  %rva173523 = call i64 @prim_cons(i64 %arg172369, i64 %rva173524)                   ; call prim_cons
  %cloptr176514 = inttoptr i64 %cont171521 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176515 = getelementptr inbounds i64, i64* %cloptr176514, i64 0               ; &cloptr176514[0]
  %f176517 = load i64, i64* %i0ptr176515, align 8                                    ; load; *i0ptr176515
  %fptr176516 = inttoptr i64 %f176517 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176516(i64 %cont171521, i64 %rva173523)             ; tail call
  ret void
}


define void @lam173875(i64 %env173876, i64 %J6X$args171523) {
  %envptr176518 = inttoptr i64 %env173876 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176519 = getelementptr inbounds i64, i64* %envptr176518, i64 3              ; &envptr176518[3]
  %w0A$_37foldr = load i64, i64* %envptr176519, align 8                              ; load; *envptr176519
  %envptr176520 = inttoptr i64 %env173876 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176521 = getelementptr inbounds i64, i64* %envptr176520, i64 2              ; &envptr176520[2]
  %K1g$_37map1 = load i64, i64* %envptr176521, align 8                               ; load; *envptr176521
  %envptr176522 = inttoptr i64 %env173876 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176523 = getelementptr inbounds i64, i64* %envptr176522, i64 1              ; &envptr176522[1]
  %xK1$_37foldr1 = load i64, i64* %envptr176523, align 8                             ; load; *envptr176523
  %cont171522 = call i64 @prim_car(i64 %J6X$args171523)                              ; call prim_car
  %J6X$args = call i64 @prim_cdr(i64 %J6X$args171523)                                ; call prim_cdr
  %Miu$f = call i64 @prim_car(i64 %J6X$args)                                         ; call prim_car
  %a171227 = call i64 @prim_cdr(i64 %J6X$args)                                       ; call prim_cdr
  %retprim171542 = call i64 @prim_car(i64 %a171227)                                  ; call prim_car
  %cloptr176524 = call i64* @alloc(i64 56)                                           ; malloc
  %eptr176526 = getelementptr inbounds i64, i64* %cloptr176524, i64 1                ; &eptr176526[1]
  %eptr176527 = getelementptr inbounds i64, i64* %cloptr176524, i64 2                ; &eptr176527[2]
  %eptr176528 = getelementptr inbounds i64, i64* %cloptr176524, i64 3                ; &eptr176528[3]
  %eptr176529 = getelementptr inbounds i64, i64* %cloptr176524, i64 4                ; &eptr176529[4]
  %eptr176530 = getelementptr inbounds i64, i64* %cloptr176524, i64 5                ; &eptr176530[5]
  %eptr176531 = getelementptr inbounds i64, i64* %cloptr176524, i64 6                ; &eptr176531[6]
  store i64 %xK1$_37foldr1, i64* %eptr176526                                         ; *eptr176526 = %xK1$_37foldr1
  store i64 %cont171522, i64* %eptr176527                                            ; *eptr176527 = %cont171522
  store i64 %J6X$args, i64* %eptr176528                                              ; *eptr176528 = %J6X$args
  store i64 %K1g$_37map1, i64* %eptr176529                                           ; *eptr176529 = %K1g$_37map1
  store i64 %Miu$f, i64* %eptr176530                                                 ; *eptr176530 = %Miu$f
  store i64 %w0A$_37foldr, i64* %eptr176531                                          ; *eptr176531 = %w0A$_37foldr
  %eptr176525 = getelementptr inbounds i64, i64* %cloptr176524, i64 0                ; &cloptr176524[0]
  %f176532 = ptrtoint void(i64,i64)* @lam173873 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176532, i64* %eptr176525                                               ; store fptr
  %arg172378 = ptrtoint i64* %cloptr176524 to i64                                    ; closure cast; i64* -> i64
  %arg172377 = add i64 0, 0                                                          ; quoted ()
  %rva173522 = add i64 0, 0                                                          ; quoted ()
  %rva173521 = call i64 @prim_cons(i64 %retprim171542, i64 %rva173522)               ; call prim_cons
  %rva173520 = call i64 @prim_cons(i64 %arg172377, i64 %rva173521)                   ; call prim_cons
  %cloptr176533 = inttoptr i64 %arg172378 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176534 = getelementptr inbounds i64, i64* %cloptr176533, i64 0               ; &cloptr176533[0]
  %f176536 = load i64, i64* %i0ptr176534, align 8                                    ; load; *i0ptr176534
  %fptr176535 = inttoptr i64 %f176536 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176535(i64 %arg172378, i64 %rva173520)              ; tail call
  ret void
}


define void @lam173873(i64 %env173874, i64 %rvp173519) {
  %envptr176537 = inttoptr i64 %env173874 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176538 = getelementptr inbounds i64, i64* %envptr176537, i64 6              ; &envptr176537[6]
  %w0A$_37foldr = load i64, i64* %envptr176538, align 8                              ; load; *envptr176538
  %envptr176539 = inttoptr i64 %env173874 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176540 = getelementptr inbounds i64, i64* %envptr176539, i64 5              ; &envptr176539[5]
  %Miu$f = load i64, i64* %envptr176540, align 8                                     ; load; *envptr176540
  %envptr176541 = inttoptr i64 %env173874 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176542 = getelementptr inbounds i64, i64* %envptr176541, i64 4              ; &envptr176541[4]
  %K1g$_37map1 = load i64, i64* %envptr176542, align 8                               ; load; *envptr176542
  %envptr176543 = inttoptr i64 %env173874 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176544 = getelementptr inbounds i64, i64* %envptr176543, i64 3              ; &envptr176543[3]
  %J6X$args = load i64, i64* %envptr176544, align 8                                  ; load; *envptr176544
  %envptr176545 = inttoptr i64 %env173874 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176546 = getelementptr inbounds i64, i64* %envptr176545, i64 2              ; &envptr176545[2]
  %cont171522 = load i64, i64* %envptr176546, align 8                                ; load; *envptr176546
  %envptr176547 = inttoptr i64 %env173874 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176548 = getelementptr inbounds i64, i64* %envptr176547, i64 1              ; &envptr176547[1]
  %xK1$_37foldr1 = load i64, i64* %envptr176548, align 8                             ; load; *envptr176548
  %_95171524 = call i64 @prim_car(i64 %rvp173519)                                    ; call prim_car
  %rvp173518 = call i64 @prim_cdr(i64 %rvp173519)                                    ; call prim_cdr
  %Cu7$acc = call i64 @prim_car(i64 %rvp173518)                                      ; call prim_car
  %na173436 = call i64 @prim_cdr(i64 %rvp173518)                                     ; call prim_cdr
  %a171228 = call i64 @prim_cdr(i64 %J6X$args)                                       ; call prim_cdr
  %retprim171541 = call i64 @prim_cdr(i64 %a171228)                                  ; call prim_cdr
  %cloptr176549 = call i64* @alloc(i64 56)                                           ; malloc
  %eptr176551 = getelementptr inbounds i64, i64* %cloptr176549, i64 1                ; &eptr176551[1]
  %eptr176552 = getelementptr inbounds i64, i64* %cloptr176549, i64 2                ; &eptr176552[2]
  %eptr176553 = getelementptr inbounds i64, i64* %cloptr176549, i64 3                ; &eptr176553[3]
  %eptr176554 = getelementptr inbounds i64, i64* %cloptr176549, i64 4                ; &eptr176554[4]
  %eptr176555 = getelementptr inbounds i64, i64* %cloptr176549, i64 5                ; &eptr176555[5]
  %eptr176556 = getelementptr inbounds i64, i64* %cloptr176549, i64 6                ; &eptr176556[6]
  store i64 %xK1$_37foldr1, i64* %eptr176551                                         ; *eptr176551 = %xK1$_37foldr1
  store i64 %cont171522, i64* %eptr176552                                            ; *eptr176552 = %cont171522
  store i64 %K1g$_37map1, i64* %eptr176553                                           ; *eptr176553 = %K1g$_37map1
  store i64 %Cu7$acc, i64* %eptr176554                                               ; *eptr176554 = %Cu7$acc
  store i64 %Miu$f, i64* %eptr176555                                                 ; *eptr176555 = %Miu$f
  store i64 %w0A$_37foldr, i64* %eptr176556                                          ; *eptr176556 = %w0A$_37foldr
  %eptr176550 = getelementptr inbounds i64, i64* %cloptr176549, i64 0                ; &cloptr176549[0]
  %f176557 = ptrtoint void(i64,i64)* @lam173871 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176557, i64* %eptr176550                                               ; store fptr
  %arg172383 = ptrtoint i64* %cloptr176549 to i64                                    ; closure cast; i64* -> i64
  %arg172382 = add i64 0, 0                                                          ; quoted ()
  %rva173517 = add i64 0, 0                                                          ; quoted ()
  %rva173516 = call i64 @prim_cons(i64 %retprim171541, i64 %rva173517)               ; call prim_cons
  %rva173515 = call i64 @prim_cons(i64 %arg172382, i64 %rva173516)                   ; call prim_cons
  %cloptr176558 = inttoptr i64 %arg172383 to i64*                                    ; closure/env cast; i64 -> i64*
  %i0ptr176559 = getelementptr inbounds i64, i64* %cloptr176558, i64 0               ; &cloptr176558[0]
  %f176561 = load i64, i64* %i0ptr176559, align 8                                    ; load; *i0ptr176559
  %fptr176560 = inttoptr i64 %f176561 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176560(i64 %arg172383, i64 %rva173515)              ; tail call
  ret void
}


define void @lam173871(i64 %env173872, i64 %rvp173514) {
  %envptr176562 = inttoptr i64 %env173872 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176563 = getelementptr inbounds i64, i64* %envptr176562, i64 6              ; &envptr176562[6]
  %w0A$_37foldr = load i64, i64* %envptr176563, align 8                              ; load; *envptr176563
  %envptr176564 = inttoptr i64 %env173872 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176565 = getelementptr inbounds i64, i64* %envptr176564, i64 5              ; &envptr176564[5]
  %Miu$f = load i64, i64* %envptr176565, align 8                                     ; load; *envptr176565
  %envptr176566 = inttoptr i64 %env173872 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176567 = getelementptr inbounds i64, i64* %envptr176566, i64 4              ; &envptr176566[4]
  %Cu7$acc = load i64, i64* %envptr176567, align 8                                   ; load; *envptr176567
  %envptr176568 = inttoptr i64 %env173872 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176569 = getelementptr inbounds i64, i64* %envptr176568, i64 3              ; &envptr176568[3]
  %K1g$_37map1 = load i64, i64* %envptr176569, align 8                               ; load; *envptr176569
  %envptr176570 = inttoptr i64 %env173872 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176571 = getelementptr inbounds i64, i64* %envptr176570, i64 2              ; &envptr176570[2]
  %cont171522 = load i64, i64* %envptr176571, align 8                                ; load; *envptr176571
  %envptr176572 = inttoptr i64 %env173872 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176573 = getelementptr inbounds i64, i64* %envptr176572, i64 1              ; &envptr176572[1]
  %xK1$_37foldr1 = load i64, i64* %envptr176573, align 8                             ; load; *envptr176573
  %_95171525 = call i64 @prim_car(i64 %rvp173514)                                    ; call prim_car
  %rvp173513 = call i64 @prim_cdr(i64 %rvp173514)                                    ; call prim_cdr
  %DBJ$lsts = call i64 @prim_car(i64 %rvp173513)                                     ; call prim_car
  %na173438 = call i64 @prim_cdr(i64 %rvp173513)                                     ; call prim_cdr
  %cloptr176574 = call i64* @alloc(i64 64)                                           ; malloc
  %eptr176576 = getelementptr inbounds i64, i64* %cloptr176574, i64 1                ; &eptr176576[1]
  %eptr176577 = getelementptr inbounds i64, i64* %cloptr176574, i64 2                ; &eptr176577[2]
  %eptr176578 = getelementptr inbounds i64, i64* %cloptr176574, i64 3                ; &eptr176578[3]
  %eptr176579 = getelementptr inbounds i64, i64* %cloptr176574, i64 4                ; &eptr176579[4]
  %eptr176580 = getelementptr inbounds i64, i64* %cloptr176574, i64 5                ; &eptr176580[5]
  %eptr176581 = getelementptr inbounds i64, i64* %cloptr176574, i64 6                ; &eptr176581[6]
  %eptr176582 = getelementptr inbounds i64, i64* %cloptr176574, i64 7                ; &eptr176582[7]
  store i64 %xK1$_37foldr1, i64* %eptr176576                                         ; *eptr176576 = %xK1$_37foldr1
  store i64 %cont171522, i64* %eptr176577                                            ; *eptr176577 = %cont171522
  store i64 %K1g$_37map1, i64* %eptr176578                                           ; *eptr176578 = %K1g$_37map1
  store i64 %DBJ$lsts, i64* %eptr176579                                              ; *eptr176579 = %DBJ$lsts
  store i64 %Cu7$acc, i64* %eptr176580                                               ; *eptr176580 = %Cu7$acc
  store i64 %Miu$f, i64* %eptr176581                                                 ; *eptr176581 = %Miu$f
  store i64 %w0A$_37foldr, i64* %eptr176582                                          ; *eptr176582 = %w0A$_37foldr
  %eptr176575 = getelementptr inbounds i64, i64* %cloptr176574, i64 0                ; &cloptr176574[0]
  %f176583 = ptrtoint void(i64,i64)* @lam173869 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176583, i64* %eptr176575                                               ; store fptr
  %arg172387 = ptrtoint i64* %cloptr176574 to i64                                    ; closure cast; i64* -> i64
  %cloptr176584 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr176585 = getelementptr inbounds i64, i64* %cloptr176584, i64 0                ; &cloptr176584[0]
  %f176586 = ptrtoint void(i64,i64)* @lam173841 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176586, i64* %eptr176585                                               ; store fptr
  %arg172386 = ptrtoint i64* %cloptr176584 to i64                                    ; closure cast; i64* -> i64
  %arg172385 = call i64 @const_init_false()                                          ; quoted #f
  %rva173512 = add i64 0, 0                                                          ; quoted ()
  %rva173511 = call i64 @prim_cons(i64 %DBJ$lsts, i64 %rva173512)                    ; call prim_cons
  %rva173510 = call i64 @prim_cons(i64 %arg172385, i64 %rva173511)                   ; call prim_cons
  %rva173509 = call i64 @prim_cons(i64 %arg172386, i64 %rva173510)                   ; call prim_cons
  %rva173508 = call i64 @prim_cons(i64 %arg172387, i64 %rva173509)                   ; call prim_cons
  %cloptr176587 = inttoptr i64 %xK1$_37foldr1 to i64*                                ; closure/env cast; i64 -> i64*
  %i0ptr176588 = getelementptr inbounds i64, i64* %cloptr176587, i64 0               ; &cloptr176587[0]
  %f176590 = load i64, i64* %i0ptr176588, align 8                                    ; load; *i0ptr176588
  %fptr176589 = inttoptr i64 %f176590 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176589(i64 %xK1$_37foldr1, i64 %rva173508)          ; tail call
  ret void
}


define void @lam173869(i64 %env173870, i64 %rvp173496) {
  %envptr176591 = inttoptr i64 %env173870 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176592 = getelementptr inbounds i64, i64* %envptr176591, i64 7              ; &envptr176591[7]
  %w0A$_37foldr = load i64, i64* %envptr176592, align 8                              ; load; *envptr176592
  %envptr176593 = inttoptr i64 %env173870 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176594 = getelementptr inbounds i64, i64* %envptr176593, i64 6              ; &envptr176593[6]
  %Miu$f = load i64, i64* %envptr176594, align 8                                     ; load; *envptr176594
  %envptr176595 = inttoptr i64 %env173870 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176596 = getelementptr inbounds i64, i64* %envptr176595, i64 5              ; &envptr176595[5]
  %Cu7$acc = load i64, i64* %envptr176596, align 8                                   ; load; *envptr176596
  %envptr176597 = inttoptr i64 %env173870 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176598 = getelementptr inbounds i64, i64* %envptr176597, i64 4              ; &envptr176597[4]
  %DBJ$lsts = load i64, i64* %envptr176598, align 8                                  ; load; *envptr176598
  %envptr176599 = inttoptr i64 %env173870 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176600 = getelementptr inbounds i64, i64* %envptr176599, i64 3              ; &envptr176599[3]
  %K1g$_37map1 = load i64, i64* %envptr176600, align 8                               ; load; *envptr176600
  %envptr176601 = inttoptr i64 %env173870 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176602 = getelementptr inbounds i64, i64* %envptr176601, i64 2              ; &envptr176601[2]
  %cont171522 = load i64, i64* %envptr176602, align 8                                ; load; *envptr176602
  %envptr176603 = inttoptr i64 %env173870 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176604 = getelementptr inbounds i64, i64* %envptr176603, i64 1              ; &envptr176603[1]
  %xK1$_37foldr1 = load i64, i64* %envptr176604, align 8                             ; load; *envptr176604
  %_95171526 = call i64 @prim_car(i64 %rvp173496)                                    ; call prim_car
  %rvp173495 = call i64 @prim_cdr(i64 %rvp173496)                                    ; call prim_cdr
  %a171229 = call i64 @prim_car(i64 %rvp173495)                                      ; call prim_car
  %na173440 = call i64 @prim_cdr(i64 %rvp173495)                                     ; call prim_cdr
  %cmp176605 = icmp eq i64 %a171229, 15                                              ; false?
  br i1 %cmp176605, label %else176607, label %then176606                             ; if

then176606:
  %arg172390 = add i64 0, 0                                                          ; quoted ()
  %rva173443 = add i64 0, 0                                                          ; quoted ()
  %rva173442 = call i64 @prim_cons(i64 %Cu7$acc, i64 %rva173443)                     ; call prim_cons
  %rva173441 = call i64 @prim_cons(i64 %arg172390, i64 %rva173442)                   ; call prim_cons
  %cloptr176608 = inttoptr i64 %cont171522 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176609 = getelementptr inbounds i64, i64* %cloptr176608, i64 0               ; &cloptr176608[0]
  %f176611 = load i64, i64* %i0ptr176609, align 8                                    ; load; *i0ptr176609
  %fptr176610 = inttoptr i64 %f176611 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176610(i64 %cont171522, i64 %rva173441)             ; tail call
  ret void

else176607:
  %cloptr176612 = call i64* @alloc(i64 64)                                           ; malloc
  %eptr176614 = getelementptr inbounds i64, i64* %cloptr176612, i64 1                ; &eptr176614[1]
  %eptr176615 = getelementptr inbounds i64, i64* %cloptr176612, i64 2                ; &eptr176615[2]
  %eptr176616 = getelementptr inbounds i64, i64* %cloptr176612, i64 3                ; &eptr176616[3]
  %eptr176617 = getelementptr inbounds i64, i64* %cloptr176612, i64 4                ; &eptr176617[4]
  %eptr176618 = getelementptr inbounds i64, i64* %cloptr176612, i64 5                ; &eptr176618[5]
  %eptr176619 = getelementptr inbounds i64, i64* %cloptr176612, i64 6                ; &eptr176619[6]
  %eptr176620 = getelementptr inbounds i64, i64* %cloptr176612, i64 7                ; &eptr176620[7]
  store i64 %xK1$_37foldr1, i64* %eptr176614                                         ; *eptr176614 = %xK1$_37foldr1
  store i64 %cont171522, i64* %eptr176615                                            ; *eptr176615 = %cont171522
  store i64 %K1g$_37map1, i64* %eptr176616                                           ; *eptr176616 = %K1g$_37map1
  store i64 %DBJ$lsts, i64* %eptr176617                                              ; *eptr176617 = %DBJ$lsts
  store i64 %Cu7$acc, i64* %eptr176618                                               ; *eptr176618 = %Cu7$acc
  store i64 %Miu$f, i64* %eptr176619                                                 ; *eptr176619 = %Miu$f
  store i64 %w0A$_37foldr, i64* %eptr176620                                          ; *eptr176620 = %w0A$_37foldr
  %eptr176613 = getelementptr inbounds i64, i64* %cloptr176612, i64 0                ; &cloptr176612[0]
  %f176621 = ptrtoint void(i64,i64)* @lam173867 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176621, i64* %eptr176613                                               ; store fptr
  %arg172394 = ptrtoint i64* %cloptr176612 to i64                                    ; closure cast; i64* -> i64
  %cloptr176622 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr176623 = getelementptr inbounds i64, i64* %cloptr176622, i64 0                ; &cloptr176622[0]
  %f176624 = ptrtoint void(i64,i64)* @lam173848 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176624, i64* %eptr176623                                               ; store fptr
  %arg172393 = ptrtoint i64* %cloptr176622 to i64                                    ; closure cast; i64* -> i64
  %rva173494 = add i64 0, 0                                                          ; quoted ()
  %rva173493 = call i64 @prim_cons(i64 %DBJ$lsts, i64 %rva173494)                    ; call prim_cons
  %rva173492 = call i64 @prim_cons(i64 %arg172393, i64 %rva173493)                   ; call prim_cons
  %rva173491 = call i64 @prim_cons(i64 %arg172394, i64 %rva173492)                   ; call prim_cons
  %cloptr176625 = inttoptr i64 %K1g$_37map1 to i64*                                  ; closure/env cast; i64 -> i64*
  %i0ptr176626 = getelementptr inbounds i64, i64* %cloptr176625, i64 0               ; &cloptr176625[0]
  %f176628 = load i64, i64* %i0ptr176626, align 8                                    ; load; *i0ptr176626
  %fptr176627 = inttoptr i64 %f176628 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176627(i64 %K1g$_37map1, i64 %rva173491)            ; tail call
  ret void
}


define void @lam173867(i64 %env173868, i64 %rvp173483) {
  %envptr176629 = inttoptr i64 %env173868 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176630 = getelementptr inbounds i64, i64* %envptr176629, i64 7              ; &envptr176629[7]
  %w0A$_37foldr = load i64, i64* %envptr176630, align 8                              ; load; *envptr176630
  %envptr176631 = inttoptr i64 %env173868 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176632 = getelementptr inbounds i64, i64* %envptr176631, i64 6              ; &envptr176631[6]
  %Miu$f = load i64, i64* %envptr176632, align 8                                     ; load; *envptr176632
  %envptr176633 = inttoptr i64 %env173868 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176634 = getelementptr inbounds i64, i64* %envptr176633, i64 5              ; &envptr176633[5]
  %Cu7$acc = load i64, i64* %envptr176634, align 8                                   ; load; *envptr176634
  %envptr176635 = inttoptr i64 %env173868 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176636 = getelementptr inbounds i64, i64* %envptr176635, i64 4              ; &envptr176635[4]
  %DBJ$lsts = load i64, i64* %envptr176636, align 8                                  ; load; *envptr176636
  %envptr176637 = inttoptr i64 %env173868 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176638 = getelementptr inbounds i64, i64* %envptr176637, i64 3              ; &envptr176637[3]
  %K1g$_37map1 = load i64, i64* %envptr176638, align 8                               ; load; *envptr176638
  %envptr176639 = inttoptr i64 %env173868 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176640 = getelementptr inbounds i64, i64* %envptr176639, i64 2              ; &envptr176639[2]
  %cont171522 = load i64, i64* %envptr176640, align 8                                ; load; *envptr176640
  %envptr176641 = inttoptr i64 %env173868 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176642 = getelementptr inbounds i64, i64* %envptr176641, i64 1              ; &envptr176641[1]
  %xK1$_37foldr1 = load i64, i64* %envptr176642, align 8                             ; load; *envptr176642
  %_95171527 = call i64 @prim_car(i64 %rvp173483)                                    ; call prim_car
  %rvp173482 = call i64 @prim_cdr(i64 %rvp173483)                                    ; call prim_cdr
  %re2$lsts_43 = call i64 @prim_car(i64 %rvp173482)                                  ; call prim_car
  %na173445 = call i64 @prim_cdr(i64 %rvp173482)                                     ; call prim_cdr
  %cloptr176643 = call i64* @alloc(i64 56)                                           ; malloc
  %eptr176645 = getelementptr inbounds i64, i64* %cloptr176643, i64 1                ; &eptr176645[1]
  %eptr176646 = getelementptr inbounds i64, i64* %cloptr176643, i64 2                ; &eptr176646[2]
  %eptr176647 = getelementptr inbounds i64, i64* %cloptr176643, i64 3                ; &eptr176647[3]
  %eptr176648 = getelementptr inbounds i64, i64* %cloptr176643, i64 4                ; &eptr176648[4]
  %eptr176649 = getelementptr inbounds i64, i64* %cloptr176643, i64 5                ; &eptr176649[5]
  %eptr176650 = getelementptr inbounds i64, i64* %cloptr176643, i64 6                ; &eptr176650[6]
  store i64 %xK1$_37foldr1, i64* %eptr176645                                         ; *eptr176645 = %xK1$_37foldr1
  store i64 %cont171522, i64* %eptr176646                                            ; *eptr176646 = %cont171522
  store i64 %Cu7$acc, i64* %eptr176647                                               ; *eptr176647 = %Cu7$acc
  store i64 %Miu$f, i64* %eptr176648                                                 ; *eptr176648 = %Miu$f
  store i64 %w0A$_37foldr, i64* %eptr176649                                          ; *eptr176649 = %w0A$_37foldr
  store i64 %re2$lsts_43, i64* %eptr176650                                           ; *eptr176650 = %re2$lsts_43
  %eptr176644 = getelementptr inbounds i64, i64* %cloptr176643, i64 0                ; &cloptr176643[0]
  %f176651 = ptrtoint void(i64,i64)* @lam173865 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176651, i64* %eptr176644                                               ; store fptr
  %arg172398 = ptrtoint i64* %cloptr176643 to i64                                    ; closure cast; i64* -> i64
  %cloptr176652 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr176653 = getelementptr inbounds i64, i64* %cloptr176652, i64 0                ; &cloptr176652[0]
  %f176654 = ptrtoint void(i64,i64)* @lam173853 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176654, i64* %eptr176653                                               ; store fptr
  %arg172397 = ptrtoint i64* %cloptr176652 to i64                                    ; closure cast; i64* -> i64
  %rva173481 = add i64 0, 0                                                          ; quoted ()
  %rva173480 = call i64 @prim_cons(i64 %DBJ$lsts, i64 %rva173481)                    ; call prim_cons
  %rva173479 = call i64 @prim_cons(i64 %arg172397, i64 %rva173480)                   ; call prim_cons
  %rva173478 = call i64 @prim_cons(i64 %arg172398, i64 %rva173479)                   ; call prim_cons
  %cloptr176655 = inttoptr i64 %K1g$_37map1 to i64*                                  ; closure/env cast; i64 -> i64*
  %i0ptr176656 = getelementptr inbounds i64, i64* %cloptr176655, i64 0               ; &cloptr176655[0]
  %f176658 = load i64, i64* %i0ptr176656, align 8                                    ; load; *i0ptr176656
  %fptr176657 = inttoptr i64 %f176658 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176657(i64 %K1g$_37map1, i64 %rva173478)            ; tail call
  ret void
}


define void @lam173865(i64 %env173866, i64 %rvp173470) {
  %envptr176659 = inttoptr i64 %env173866 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176660 = getelementptr inbounds i64, i64* %envptr176659, i64 6              ; &envptr176659[6]
  %re2$lsts_43 = load i64, i64* %envptr176660, align 8                               ; load; *envptr176660
  %envptr176661 = inttoptr i64 %env173866 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176662 = getelementptr inbounds i64, i64* %envptr176661, i64 5              ; &envptr176661[5]
  %w0A$_37foldr = load i64, i64* %envptr176662, align 8                              ; load; *envptr176662
  %envptr176663 = inttoptr i64 %env173866 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176664 = getelementptr inbounds i64, i64* %envptr176663, i64 4              ; &envptr176663[4]
  %Miu$f = load i64, i64* %envptr176664, align 8                                     ; load; *envptr176664
  %envptr176665 = inttoptr i64 %env173866 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176666 = getelementptr inbounds i64, i64* %envptr176665, i64 3              ; &envptr176665[3]
  %Cu7$acc = load i64, i64* %envptr176666, align 8                                   ; load; *envptr176666
  %envptr176667 = inttoptr i64 %env173866 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176668 = getelementptr inbounds i64, i64* %envptr176667, i64 2              ; &envptr176667[2]
  %cont171522 = load i64, i64* %envptr176668, align 8                                ; load; *envptr176668
  %envptr176669 = inttoptr i64 %env173866 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176670 = getelementptr inbounds i64, i64* %envptr176669, i64 1              ; &envptr176669[1]
  %xK1$_37foldr1 = load i64, i64* %envptr176670, align 8                             ; load; *envptr176670
  %_95171528 = call i64 @prim_car(i64 %rvp173470)                                    ; call prim_car
  %rvp173469 = call i64 @prim_cdr(i64 %rvp173470)                                    ; call prim_cdr
  %Y4O$vs = call i64 @prim_car(i64 %rvp173469)                                       ; call prim_car
  %na173447 = call i64 @prim_cdr(i64 %rvp173469)                                     ; call prim_cdr
  %a171230 = call i64 @prim_cons(i64 %Cu7$acc, i64 %re2$lsts_43)                     ; call prim_cons
  %a171231 = call i64 @prim_cons(i64 %Miu$f, i64 %a171230)                           ; call prim_cons
  %cloptr176671 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr176673 = getelementptr inbounds i64, i64* %cloptr176671, i64 1                ; &eptr176673[1]
  %eptr176674 = getelementptr inbounds i64, i64* %cloptr176671, i64 2                ; &eptr176674[2]
  %eptr176675 = getelementptr inbounds i64, i64* %cloptr176671, i64 3                ; &eptr176675[3]
  %eptr176676 = getelementptr inbounds i64, i64* %cloptr176671, i64 4                ; &eptr176676[4]
  store i64 %xK1$_37foldr1, i64* %eptr176673                                         ; *eptr176673 = %xK1$_37foldr1
  store i64 %cont171522, i64* %eptr176674                                            ; *eptr176674 = %cont171522
  store i64 %Y4O$vs, i64* %eptr176675                                                ; *eptr176675 = %Y4O$vs
  store i64 %Miu$f, i64* %eptr176676                                                 ; *eptr176676 = %Miu$f
  %eptr176672 = getelementptr inbounds i64, i64* %cloptr176671, i64 0                ; &cloptr176671[0]
  %f176677 = ptrtoint void(i64,i64)* @lam173863 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176677, i64* %eptr176672                                               ; store fptr
  %arg172405 = ptrtoint i64* %cloptr176671 to i64                                    ; closure cast; i64* -> i64
  %cps_45lst171534 = call i64 @prim_cons(i64 %arg172405, i64 %a171231)               ; call prim_cons
  %cloptr176678 = inttoptr i64 %w0A$_37foldr to i64*                                 ; closure/env cast; i64 -> i64*
  %i0ptr176679 = getelementptr inbounds i64, i64* %cloptr176678, i64 0               ; &cloptr176678[0]
  %f176681 = load i64, i64* %i0ptr176679, align 8                                    ; load; *i0ptr176679
  %fptr176680 = inttoptr i64 %f176681 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176680(i64 %w0A$_37foldr, i64 %cps_45lst171534)     ; tail call
  ret void
}


define void @lam173863(i64 %env173864, i64 %rvp173468) {
  %envptr176682 = inttoptr i64 %env173864 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176683 = getelementptr inbounds i64, i64* %envptr176682, i64 4              ; &envptr176682[4]
  %Miu$f = load i64, i64* %envptr176683, align 8                                     ; load; *envptr176683
  %envptr176684 = inttoptr i64 %env173864 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176685 = getelementptr inbounds i64, i64* %envptr176684, i64 3              ; &envptr176684[3]
  %Y4O$vs = load i64, i64* %envptr176685, align 8                                    ; load; *envptr176685
  %envptr176686 = inttoptr i64 %env173864 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176687 = getelementptr inbounds i64, i64* %envptr176686, i64 2              ; &envptr176686[2]
  %cont171522 = load i64, i64* %envptr176687, align 8                                ; load; *envptr176687
  %envptr176688 = inttoptr i64 %env173864 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176689 = getelementptr inbounds i64, i64* %envptr176688, i64 1              ; &envptr176688[1]
  %xK1$_37foldr1 = load i64, i64* %envptr176689, align 8                             ; load; *envptr176689
  %_95171529 = call i64 @prim_car(i64 %rvp173468)                                    ; call prim_car
  %rvp173467 = call i64 @prim_cdr(i64 %rvp173468)                                    ; call prim_cdr
  %a171232 = call i64 @prim_car(i64 %rvp173467)                                      ; call prim_car
  %na173449 = call i64 @prim_cdr(i64 %rvp173467)                                     ; call prim_cdr
  %arg172406 = add i64 0, 0                                                          ; quoted ()
  %a171233 = call i64 @prim_cons(i64 %a171232, i64 %arg172406)                       ; call prim_cons
  %cloptr176690 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr176692 = getelementptr inbounds i64, i64* %cloptr176690, i64 1                ; &eptr176692[1]
  %eptr176693 = getelementptr inbounds i64, i64* %cloptr176690, i64 2                ; &eptr176693[2]
  store i64 %cont171522, i64* %eptr176692                                            ; *eptr176692 = %cont171522
  store i64 %Miu$f, i64* %eptr176693                                                 ; *eptr176693 = %Miu$f
  %eptr176691 = getelementptr inbounds i64, i64* %cloptr176690, i64 0                ; &cloptr176690[0]
  %f176694 = ptrtoint void(i64,i64)* @lam173860 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176694, i64* %eptr176691                                               ; store fptr
  %arg172411 = ptrtoint i64* %cloptr176690 to i64                                    ; closure cast; i64* -> i64
  %cloptr176695 = call i64* @alloc(i64 8)                                            ; malloc
  %eptr176696 = getelementptr inbounds i64, i64* %cloptr176695, i64 0                ; &cloptr176695[0]
  %f176697 = ptrtoint void(i64,i64)* @lam173858 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176697, i64* %eptr176696                                               ; store fptr
  %arg172410 = ptrtoint i64* %cloptr176695 to i64                                    ; closure cast; i64* -> i64
  %rva173466 = add i64 0, 0                                                          ; quoted ()
  %rva173465 = call i64 @prim_cons(i64 %Y4O$vs, i64 %rva173466)                      ; call prim_cons
  %rva173464 = call i64 @prim_cons(i64 %a171233, i64 %rva173465)                     ; call prim_cons
  %rva173463 = call i64 @prim_cons(i64 %arg172410, i64 %rva173464)                   ; call prim_cons
  %rva173462 = call i64 @prim_cons(i64 %arg172411, i64 %rva173463)                   ; call prim_cons
  %cloptr176698 = inttoptr i64 %xK1$_37foldr1 to i64*                                ; closure/env cast; i64 -> i64*
  %i0ptr176699 = getelementptr inbounds i64, i64* %cloptr176698, i64 0               ; &cloptr176698[0]
  %f176701 = load i64, i64* %i0ptr176699, align 8                                    ; load; *i0ptr176699
  %fptr176700 = inttoptr i64 %f176701 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176700(i64 %xK1$_37foldr1, i64 %rva173462)          ; tail call
  ret void
}


define void @lam173860(i64 %env173861, i64 %rvp173453) {
  %envptr176702 = inttoptr i64 %env173861 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176703 = getelementptr inbounds i64, i64* %envptr176702, i64 2              ; &envptr176702[2]
  %Miu$f = load i64, i64* %envptr176703, align 8                                     ; load; *envptr176703
  %envptr176704 = inttoptr i64 %env173861 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176705 = getelementptr inbounds i64, i64* %envptr176704, i64 1              ; &envptr176704[1]
  %cont171522 = load i64, i64* %envptr176705, align 8                                ; load; *envptr176705
  %_95171530 = call i64 @prim_car(i64 %rvp173453)                                    ; call prim_car
  %rvp173452 = call i64 @prim_cdr(i64 %rvp173453)                                    ; call prim_cdr
  %a171234 = call i64 @prim_car(i64 %rvp173452)                                      ; call prim_car
  %na173451 = call i64 @prim_cdr(i64 %rvp173452)                                     ; call prim_cdr
  %cps_45lst171531 = call i64 @prim_cons(i64 %cont171522, i64 %a171234)              ; call prim_cons
  %cloptr176706 = inttoptr i64 %Miu$f to i64*                                        ; closure/env cast; i64 -> i64*
  %i0ptr176707 = getelementptr inbounds i64, i64* %cloptr176706, i64 0               ; &cloptr176706[0]
  %f176709 = load i64, i64* %i0ptr176707, align 8                                    ; load; *i0ptr176707
  %fptr176708 = inttoptr i64 %f176709 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176708(i64 %Miu$f, i64 %cps_45lst171531)            ; tail call
  ret void
}


define void @lam173858(i64 %env173859, i64 %rvp173461) {
  %cont171532 = call i64 @prim_car(i64 %rvp173461)                                   ; call prim_car
  %rvp173460 = call i64 @prim_cdr(i64 %rvp173461)                                    ; call prim_cdr
  %WRO$a = call i64 @prim_car(i64 %rvp173460)                                        ; call prim_car
  %rvp173459 = call i64 @prim_cdr(i64 %rvp173460)                                    ; call prim_cdr
  %Sle$b = call i64 @prim_car(i64 %rvp173459)                                        ; call prim_car
  %na173455 = call i64 @prim_cdr(i64 %rvp173459)                                     ; call prim_cdr
  %retprim171533 = call i64 @prim_cons(i64 %WRO$a, i64 %Sle$b)                       ; call prim_cons
  %arg172418 = add i64 0, 0                                                          ; quoted ()
  %rva173458 = add i64 0, 0                                                          ; quoted ()
  %rva173457 = call i64 @prim_cons(i64 %retprim171533, i64 %rva173458)               ; call prim_cons
  %rva173456 = call i64 @prim_cons(i64 %arg172418, i64 %rva173457)                   ; call prim_cons
  %cloptr176710 = inttoptr i64 %cont171532 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176711 = getelementptr inbounds i64, i64* %cloptr176710, i64 0               ; &cloptr176710[0]
  %f176713 = load i64, i64* %i0ptr176711, align 8                                    ; load; *i0ptr176711
  %fptr176712 = inttoptr i64 %f176713 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176712(i64 %cont171532, i64 %rva173456)             ; tail call
  ret void
}


define void @lam173853(i64 %env173854, i64 %rvp173477) {
  %cont171535 = call i64 @prim_car(i64 %rvp173477)                                   ; call prim_car
  %rvp173476 = call i64 @prim_cdr(i64 %rvp173477)                                    ; call prim_cdr
  %DYr$x = call i64 @prim_car(i64 %rvp173476)                                        ; call prim_car
  %na173472 = call i64 @prim_cdr(i64 %rvp173476)                                     ; call prim_cdr
  %retprim171536 = call i64 @prim_car(i64 %DYr$x)                                    ; call prim_car
  %arg172422 = add i64 0, 0                                                          ; quoted ()
  %rva173475 = add i64 0, 0                                                          ; quoted ()
  %rva173474 = call i64 @prim_cons(i64 %retprim171536, i64 %rva173475)               ; call prim_cons
  %rva173473 = call i64 @prim_cons(i64 %arg172422, i64 %rva173474)                   ; call prim_cons
  %cloptr176714 = inttoptr i64 %cont171535 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176715 = getelementptr inbounds i64, i64* %cloptr176714, i64 0               ; &cloptr176714[0]
  %f176717 = load i64, i64* %i0ptr176715, align 8                                    ; load; *i0ptr176715
  %fptr176716 = inttoptr i64 %f176717 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176716(i64 %cont171535, i64 %rva173473)             ; tail call
  ret void
}


define void @lam173848(i64 %env173849, i64 %rvp173490) {
  %cont171537 = call i64 @prim_car(i64 %rvp173490)                                   ; call prim_car
  %rvp173489 = call i64 @prim_cdr(i64 %rvp173490)                                    ; call prim_cdr
  %f5r$x = call i64 @prim_car(i64 %rvp173489)                                        ; call prim_car
  %na173485 = call i64 @prim_cdr(i64 %rvp173489)                                     ; call prim_cdr
  %retprim171538 = call i64 @prim_cdr(i64 %f5r$x)                                    ; call prim_cdr
  %arg172426 = add i64 0, 0                                                          ; quoted ()
  %rva173488 = add i64 0, 0                                                          ; quoted ()
  %rva173487 = call i64 @prim_cons(i64 %retprim171538, i64 %rva173488)               ; call prim_cons
  %rva173486 = call i64 @prim_cons(i64 %arg172426, i64 %rva173487)                   ; call prim_cons
  %cloptr176718 = inttoptr i64 %cont171537 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176719 = getelementptr inbounds i64, i64* %cloptr176718, i64 0               ; &cloptr176718[0]
  %f176721 = load i64, i64* %i0ptr176719, align 8                                    ; load; *i0ptr176719
  %fptr176720 = inttoptr i64 %f176721 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176720(i64 %cont171537, i64 %rva173486)             ; tail call
  ret void
}


define void @lam173841(i64 %env173842, i64 %rvp173507) {
  %cont171539 = call i64 @prim_car(i64 %rvp173507)                                   ; call prim_car
  %rvp173506 = call i64 @prim_cdr(i64 %rvp173507)                                    ; call prim_cdr
  %fPk$lst = call i64 @prim_car(i64 %rvp173506)                                      ; call prim_car
  %rvp173505 = call i64 @prim_cdr(i64 %rvp173506)                                    ; call prim_cdr
  %ZkD$b = call i64 @prim_car(i64 %rvp173505)                                        ; call prim_car
  %na173498 = call i64 @prim_cdr(i64 %rvp173505)                                     ; call prim_cdr
  %cmp176722 = icmp eq i64 %ZkD$b, 15                                                ; false?
  br i1 %cmp176722, label %else176724, label %then176723                             ; if

then176723:
  %arg172429 = add i64 0, 0                                                          ; quoted ()
  %rva173501 = add i64 0, 0                                                          ; quoted ()
  %rva173500 = call i64 @prim_cons(i64 %ZkD$b, i64 %rva173501)                       ; call prim_cons
  %rva173499 = call i64 @prim_cons(i64 %arg172429, i64 %rva173500)                   ; call prim_cons
  %cloptr176725 = inttoptr i64 %cont171539 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176726 = getelementptr inbounds i64, i64* %cloptr176725, i64 0               ; &cloptr176725[0]
  %f176728 = load i64, i64* %i0ptr176726, align 8                                    ; load; *i0ptr176726
  %fptr176727 = inttoptr i64 %f176728 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176727(i64 %cont171539, i64 %rva173499)             ; tail call
  ret void

else176724:
  %retprim171540 = call i64 @prim_null_63(i64 %fPk$lst)                              ; call prim_null_63
  %arg172433 = add i64 0, 0                                                          ; quoted ()
  %rva173504 = add i64 0, 0                                                          ; quoted ()
  %rva173503 = call i64 @prim_cons(i64 %retprim171540, i64 %rva173504)               ; call prim_cons
  %rva173502 = call i64 @prim_cons(i64 %arg172433, i64 %rva173503)                   ; call prim_cons
  %cloptr176729 = inttoptr i64 %cont171539 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176730 = getelementptr inbounds i64, i64* %cloptr176729, i64 0               ; &cloptr176729[0]
  %f176732 = load i64, i64* %i0ptr176730, align 8                                    ; load; *i0ptr176730
  %fptr176731 = inttoptr i64 %f176732 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176731(i64 %cont171539, i64 %rva173502)             ; tail call
  ret void
}


define void @lam173827(i64 %env173828, i64 %rvp173561) {
  %cont171543 = call i64 @prim_car(i64 %rvp173561)                                   ; call prim_car
  %rvp173560 = call i64 @prim_cdr(i64 %rvp173561)                                    ; call prim_cdr
  %zzG$_37foldl1 = call i64 @prim_car(i64 %rvp173560)                                ; call prim_car
  %na173534 = call i64 @prim_cdr(i64 %rvp173560)                                     ; call prim_cdr
  %arg172436 = add i64 0, 0                                                          ; quoted ()
  %cloptr176733 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr176735 = getelementptr inbounds i64, i64* %cloptr176733, i64 1                ; &eptr176735[1]
  store i64 %zzG$_37foldl1, i64* %eptr176735                                         ; *eptr176735 = %zzG$_37foldl1
  %eptr176734 = getelementptr inbounds i64, i64* %cloptr176733, i64 0                ; &cloptr176733[0]
  %f176736 = ptrtoint void(i64,i64)* @lam173824 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176736, i64* %eptr176734                                               ; store fptr
  %arg172435 = ptrtoint i64* %cloptr176733 to i64                                    ; closure cast; i64* -> i64
  %rva173559 = add i64 0, 0                                                          ; quoted ()
  %rva173558 = call i64 @prim_cons(i64 %arg172435, i64 %rva173559)                   ; call prim_cons
  %rva173557 = call i64 @prim_cons(i64 %arg172436, i64 %rva173558)                   ; call prim_cons
  %cloptr176737 = inttoptr i64 %cont171543 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176738 = getelementptr inbounds i64, i64* %cloptr176737, i64 0               ; &cloptr176737[0]
  %f176740 = load i64, i64* %i0ptr176738, align 8                                    ; load; *i0ptr176738
  %fptr176739 = inttoptr i64 %f176740 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176739(i64 %cont171543, i64 %rva173557)             ; tail call
  ret void
}


define void @lam173824(i64 %env173825, i64 %rvp173556) {
  %envptr176741 = inttoptr i64 %env173825 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176742 = getelementptr inbounds i64, i64* %envptr176741, i64 1              ; &envptr176741[1]
  %zzG$_37foldl1 = load i64, i64* %envptr176742, align 8                             ; load; *envptr176742
  %cont171544 = call i64 @prim_car(i64 %rvp173556)                                   ; call prim_car
  %rvp173555 = call i64 @prim_cdr(i64 %rvp173556)                                    ; call prim_cdr
  %vmb$f = call i64 @prim_car(i64 %rvp173555)                                        ; call prim_car
  %rvp173554 = call i64 @prim_cdr(i64 %rvp173555)                                    ; call prim_cdr
  %e37$acc = call i64 @prim_car(i64 %rvp173554)                                      ; call prim_car
  %rvp173553 = call i64 @prim_cdr(i64 %rvp173554)                                    ; call prim_cdr
  %wn3$lst = call i64 @prim_car(i64 %rvp173553)                                      ; call prim_car
  %na173536 = call i64 @prim_cdr(i64 %rvp173553)                                     ; call prim_cdr
  %a171221 = call i64 @prim_null_63(i64 %wn3$lst)                                    ; call prim_null_63
  %cmp176743 = icmp eq i64 %a171221, 15                                              ; false?
  br i1 %cmp176743, label %else176745, label %then176744                             ; if

then176744:
  %arg172440 = add i64 0, 0                                                          ; quoted ()
  %rva173539 = add i64 0, 0                                                          ; quoted ()
  %rva173538 = call i64 @prim_cons(i64 %e37$acc, i64 %rva173539)                     ; call prim_cons
  %rva173537 = call i64 @prim_cons(i64 %arg172440, i64 %rva173538)                   ; call prim_cons
  %cloptr176746 = inttoptr i64 %cont171544 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176747 = getelementptr inbounds i64, i64* %cloptr176746, i64 0               ; &cloptr176746[0]
  %f176749 = load i64, i64* %i0ptr176747, align 8                                    ; load; *i0ptr176747
  %fptr176748 = inttoptr i64 %f176749 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176748(i64 %cont171544, i64 %rva173537)             ; tail call
  ret void

else176745:
  %a171222 = call i64 @prim_car(i64 %wn3$lst)                                        ; call prim_car
  %cloptr176750 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr176752 = getelementptr inbounds i64, i64* %cloptr176750, i64 1                ; &eptr176752[1]
  %eptr176753 = getelementptr inbounds i64, i64* %cloptr176750, i64 2                ; &eptr176753[2]
  %eptr176754 = getelementptr inbounds i64, i64* %cloptr176750, i64 3                ; &eptr176754[3]
  %eptr176755 = getelementptr inbounds i64, i64* %cloptr176750, i64 4                ; &eptr176755[4]
  store i64 %wn3$lst, i64* %eptr176752                                               ; *eptr176752 = %wn3$lst
  store i64 %vmb$f, i64* %eptr176753                                                 ; *eptr176753 = %vmb$f
  store i64 %cont171544, i64* %eptr176754                                            ; *eptr176754 = %cont171544
  store i64 %zzG$_37foldl1, i64* %eptr176755                                         ; *eptr176755 = %zzG$_37foldl1
  %eptr176751 = getelementptr inbounds i64, i64* %cloptr176750, i64 0                ; &cloptr176750[0]
  %f176756 = ptrtoint void(i64,i64)* @lam173822 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176756, i64* %eptr176751                                               ; store fptr
  %arg172445 = ptrtoint i64* %cloptr176750 to i64                                    ; closure cast; i64* -> i64
  %rva173552 = add i64 0, 0                                                          ; quoted ()
  %rva173551 = call i64 @prim_cons(i64 %e37$acc, i64 %rva173552)                     ; call prim_cons
  %rva173550 = call i64 @prim_cons(i64 %a171222, i64 %rva173551)                     ; call prim_cons
  %rva173549 = call i64 @prim_cons(i64 %arg172445, i64 %rva173550)                   ; call prim_cons
  %cloptr176757 = inttoptr i64 %vmb$f to i64*                                        ; closure/env cast; i64 -> i64*
  %i0ptr176758 = getelementptr inbounds i64, i64* %cloptr176757, i64 0               ; &cloptr176757[0]
  %f176760 = load i64, i64* %i0ptr176758, align 8                                    ; load; *i0ptr176758
  %fptr176759 = inttoptr i64 %f176760 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176759(i64 %vmb$f, i64 %rva173549)                  ; tail call
  ret void
}


define void @lam173822(i64 %env173823, i64 %rvp173548) {
  %envptr176761 = inttoptr i64 %env173823 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176762 = getelementptr inbounds i64, i64* %envptr176761, i64 4              ; &envptr176761[4]
  %zzG$_37foldl1 = load i64, i64* %envptr176762, align 8                             ; load; *envptr176762
  %envptr176763 = inttoptr i64 %env173823 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176764 = getelementptr inbounds i64, i64* %envptr176763, i64 3              ; &envptr176763[3]
  %cont171544 = load i64, i64* %envptr176764, align 8                                ; load; *envptr176764
  %envptr176765 = inttoptr i64 %env173823 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176766 = getelementptr inbounds i64, i64* %envptr176765, i64 2              ; &envptr176765[2]
  %vmb$f = load i64, i64* %envptr176766, align 8                                     ; load; *envptr176766
  %envptr176767 = inttoptr i64 %env173823 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176768 = getelementptr inbounds i64, i64* %envptr176767, i64 1              ; &envptr176767[1]
  %wn3$lst = load i64, i64* %envptr176768, align 8                                   ; load; *envptr176768
  %_95171545 = call i64 @prim_car(i64 %rvp173548)                                    ; call prim_car
  %rvp173547 = call i64 @prim_cdr(i64 %rvp173548)                                    ; call prim_cdr
  %a171223 = call i64 @prim_car(i64 %rvp173547)                                      ; call prim_car
  %na173541 = call i64 @prim_cdr(i64 %rvp173547)                                     ; call prim_cdr
  %a171224 = call i64 @prim_cdr(i64 %wn3$lst)                                        ; call prim_cdr
  %rva173546 = add i64 0, 0                                                          ; quoted ()
  %rva173545 = call i64 @prim_cons(i64 %a171224, i64 %rva173546)                     ; call prim_cons
  %rva173544 = call i64 @prim_cons(i64 %a171223, i64 %rva173545)                     ; call prim_cons
  %rva173543 = call i64 @prim_cons(i64 %vmb$f, i64 %rva173544)                       ; call prim_cons
  %rva173542 = call i64 @prim_cons(i64 %cont171544, i64 %rva173543)                  ; call prim_cons
  %cloptr176769 = inttoptr i64 %zzG$_37foldl1 to i64*                                ; closure/env cast; i64 -> i64*
  %i0ptr176770 = getelementptr inbounds i64, i64* %cloptr176769, i64 0               ; &cloptr176769[0]
  %f176772 = load i64, i64* %i0ptr176770, align 8                                    ; load; *i0ptr176770
  %fptr176771 = inttoptr i64 %f176772 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176771(i64 %zzG$_37foldl1, i64 %rva173542)          ; tail call
  ret void
}


define void @lam173814(i64 %env173815, i64 %rvp173590) {
  %cont171546 = call i64 @prim_car(i64 %rvp173590)                                   ; call prim_car
  %rvp173589 = call i64 @prim_cdr(i64 %rvp173590)                                    ; call prim_cdr
  %hBm$_37length = call i64 @prim_car(i64 %rvp173589)                                ; call prim_car
  %na173568 = call i64 @prim_cdr(i64 %rvp173589)                                     ; call prim_cdr
  %arg172454 = add i64 0, 0                                                          ; quoted ()
  %cloptr176773 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr176775 = getelementptr inbounds i64, i64* %cloptr176773, i64 1                ; &eptr176775[1]
  store i64 %hBm$_37length, i64* %eptr176775                                         ; *eptr176775 = %hBm$_37length
  %eptr176774 = getelementptr inbounds i64, i64* %cloptr176773, i64 0                ; &cloptr176773[0]
  %f176776 = ptrtoint void(i64,i64)* @lam173811 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176776, i64* %eptr176774                                               ; store fptr
  %arg172453 = ptrtoint i64* %cloptr176773 to i64                                    ; closure cast; i64* -> i64
  %rva173588 = add i64 0, 0                                                          ; quoted ()
  %rva173587 = call i64 @prim_cons(i64 %arg172453, i64 %rva173588)                   ; call prim_cons
  %rva173586 = call i64 @prim_cons(i64 %arg172454, i64 %rva173587)                   ; call prim_cons
  %cloptr176777 = inttoptr i64 %cont171546 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176778 = getelementptr inbounds i64, i64* %cloptr176777, i64 0               ; &cloptr176777[0]
  %f176780 = load i64, i64* %i0ptr176778, align 8                                    ; load; *i0ptr176778
  %fptr176779 = inttoptr i64 %f176780 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176779(i64 %cont171546, i64 %rva173586)             ; tail call
  ret void
}


define void @lam173811(i64 %env173812, i64 %rvp173585) {
  %envptr176781 = inttoptr i64 %env173812 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176782 = getelementptr inbounds i64, i64* %envptr176781, i64 1              ; &envptr176781[1]
  %hBm$_37length = load i64, i64* %envptr176782, align 8                             ; load; *envptr176782
  %cont171547 = call i64 @prim_car(i64 %rvp173585)                                   ; call prim_car
  %rvp173584 = call i64 @prim_cdr(i64 %rvp173585)                                    ; call prim_cdr
  %An7$lst = call i64 @prim_car(i64 %rvp173584)                                      ; call prim_car
  %na173570 = call i64 @prim_cdr(i64 %rvp173584)                                     ; call prim_cdr
  %a171218 = call i64 @prim_null_63(i64 %An7$lst)                                    ; call prim_null_63
  %cmp176783 = icmp eq i64 %a171218, 15                                              ; false?
  br i1 %cmp176783, label %else176785, label %then176784                             ; if

then176784:
  %arg172458 = add i64 0, 0                                                          ; quoted ()
  %arg172457 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %rva173573 = add i64 0, 0                                                          ; quoted ()
  %rva173572 = call i64 @prim_cons(i64 %arg172457, i64 %rva173573)                   ; call prim_cons
  %rva173571 = call i64 @prim_cons(i64 %arg172458, i64 %rva173572)                   ; call prim_cons
  %cloptr176786 = inttoptr i64 %cont171547 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176787 = getelementptr inbounds i64, i64* %cloptr176786, i64 0               ; &cloptr176786[0]
  %f176789 = load i64, i64* %i0ptr176787, align 8                                    ; load; *i0ptr176787
  %fptr176788 = inttoptr i64 %f176789 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176788(i64 %cont171547, i64 %rva173571)             ; tail call
  ret void

else176785:
  %a171219 = call i64 @prim_cdr(i64 %An7$lst)                                        ; call prim_cdr
  %cloptr176790 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr176792 = getelementptr inbounds i64, i64* %cloptr176790, i64 1                ; &eptr176792[1]
  store i64 %cont171547, i64* %eptr176792                                            ; *eptr176792 = %cont171547
  %eptr176791 = getelementptr inbounds i64, i64* %cloptr176790, i64 0                ; &cloptr176790[0]
  %f176793 = ptrtoint void(i64,i64)* @lam173809 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176793, i64* %eptr176791                                               ; store fptr
  %arg172462 = ptrtoint i64* %cloptr176790 to i64                                    ; closure cast; i64* -> i64
  %rva173583 = add i64 0, 0                                                          ; quoted ()
  %rva173582 = call i64 @prim_cons(i64 %a171219, i64 %rva173583)                     ; call prim_cons
  %rva173581 = call i64 @prim_cons(i64 %arg172462, i64 %rva173582)                   ; call prim_cons
  %cloptr176794 = inttoptr i64 %hBm$_37length to i64*                                ; closure/env cast; i64 -> i64*
  %i0ptr176795 = getelementptr inbounds i64, i64* %cloptr176794, i64 0               ; &cloptr176794[0]
  %f176797 = load i64, i64* %i0ptr176795, align 8                                    ; load; *i0ptr176795
  %fptr176796 = inttoptr i64 %f176797 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176796(i64 %hBm$_37length, i64 %rva173581)          ; tail call
  ret void
}


define void @lam173809(i64 %env173810, i64 %rvp173580) {
  %envptr176798 = inttoptr i64 %env173810 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176799 = getelementptr inbounds i64, i64* %envptr176798, i64 1              ; &envptr176798[1]
  %cont171547 = load i64, i64* %envptr176799, align 8                                ; load; *envptr176799
  %_95171548 = call i64 @prim_car(i64 %rvp173580)                                    ; call prim_car
  %rvp173579 = call i64 @prim_cdr(i64 %rvp173580)                                    ; call prim_cdr
  %a171220 = call i64 @prim_car(i64 %rvp173579)                                      ; call prim_car
  %na173575 = call i64 @prim_cdr(i64 %rvp173579)                                     ; call prim_cdr
  %arg172465 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %retprim171549 = call i64 @prim__43(i64 %arg172465, i64 %a171220)                  ; call prim__43
  %arg172467 = add i64 0, 0                                                          ; quoted ()
  %rva173578 = add i64 0, 0                                                          ; quoted ()
  %rva173577 = call i64 @prim_cons(i64 %retprim171549, i64 %rva173578)               ; call prim_cons
  %rva173576 = call i64 @prim_cons(i64 %arg172467, i64 %rva173577)                   ; call prim_cons
  %cloptr176800 = inttoptr i64 %cont171547 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176801 = getelementptr inbounds i64, i64* %cloptr176800, i64 0               ; &cloptr176800[0]
  %f176803 = load i64, i64* %i0ptr176801, align 8                                    ; load; *i0ptr176801
  %fptr176802 = inttoptr i64 %f176803 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176802(i64 %cont171547, i64 %rva173576)             ; tail call
  ret void
}


define void @lam173798(i64 %env173799, i64 %rvp173624) {
  %cont171550 = call i64 @prim_car(i64 %rvp173624)                                   ; call prim_car
  %rvp173623 = call i64 @prim_cdr(i64 %rvp173624)                                    ; call prim_cdr
  %Zju$_37take = call i64 @prim_car(i64 %rvp173623)                                  ; call prim_car
  %na173597 = call i64 @prim_cdr(i64 %rvp173623)                                     ; call prim_cdr
  %arg172470 = add i64 0, 0                                                          ; quoted ()
  %cloptr176804 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr176806 = getelementptr inbounds i64, i64* %cloptr176804, i64 1                ; &eptr176806[1]
  store i64 %Zju$_37take, i64* %eptr176806                                           ; *eptr176806 = %Zju$_37take
  %eptr176805 = getelementptr inbounds i64, i64* %cloptr176804, i64 0                ; &cloptr176804[0]
  %f176807 = ptrtoint void(i64,i64)* @lam173795 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176807, i64* %eptr176805                                               ; store fptr
  %arg172469 = ptrtoint i64* %cloptr176804 to i64                                    ; closure cast; i64* -> i64
  %rva173622 = add i64 0, 0                                                          ; quoted ()
  %rva173621 = call i64 @prim_cons(i64 %arg172469, i64 %rva173622)                   ; call prim_cons
  %rva173620 = call i64 @prim_cons(i64 %arg172470, i64 %rva173621)                   ; call prim_cons
  %cloptr176808 = inttoptr i64 %cont171550 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176809 = getelementptr inbounds i64, i64* %cloptr176808, i64 0               ; &cloptr176808[0]
  %f176811 = load i64, i64* %i0ptr176809, align 8                                    ; load; *i0ptr176809
  %fptr176810 = inttoptr i64 %f176811 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176810(i64 %cont171550, i64 %rva173620)             ; tail call
  ret void
}


define void @lam173795(i64 %env173796, i64 %rvp173619) {
  %envptr176812 = inttoptr i64 %env173796 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176813 = getelementptr inbounds i64, i64* %envptr176812, i64 1              ; &envptr176812[1]
  %Zju$_37take = load i64, i64* %envptr176813, align 8                               ; load; *envptr176813
  %cont171551 = call i64 @prim_car(i64 %rvp173619)                                   ; call prim_car
  %rvp173618 = call i64 @prim_cdr(i64 %rvp173619)                                    ; call prim_cdr
  %njg$lst = call i64 @prim_car(i64 %rvp173618)                                      ; call prim_car
  %rvp173617 = call i64 @prim_cdr(i64 %rvp173618)                                    ; call prim_cdr
  %rDL$n = call i64 @prim_car(i64 %rvp173617)                                        ; call prim_car
  %na173599 = call i64 @prim_cdr(i64 %rvp173617)                                     ; call prim_cdr
  %arg172472 = call i64 @const_init_int(i64 0)                                       ; quoted int
  %a171212 = call i64 @prim__61(i64 %rDL$n, i64 %arg172472)                          ; call prim__61
  %cmp176814 = icmp eq i64 %a171212, 15                                              ; false?
  br i1 %cmp176814, label %else176816, label %then176815                             ; if

then176815:
  %arg172475 = add i64 0, 0                                                          ; quoted ()
  %arg172474 = add i64 0, 0                                                          ; quoted ()
  %rva173602 = add i64 0, 0                                                          ; quoted ()
  %rva173601 = call i64 @prim_cons(i64 %arg172474, i64 %rva173602)                   ; call prim_cons
  %rva173600 = call i64 @prim_cons(i64 %arg172475, i64 %rva173601)                   ; call prim_cons
  %cloptr176817 = inttoptr i64 %cont171551 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176818 = getelementptr inbounds i64, i64* %cloptr176817, i64 0               ; &cloptr176817[0]
  %f176820 = load i64, i64* %i0ptr176818, align 8                                    ; load; *i0ptr176818
  %fptr176819 = inttoptr i64 %f176820 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176819(i64 %cont171551, i64 %rva173600)             ; tail call
  ret void

else176816:
  %a171213 = call i64 @prim_null_63(i64 %njg$lst)                                    ; call prim_null_63
  %cmp176821 = icmp eq i64 %a171213, 15                                              ; false?
  br i1 %cmp176821, label %else176823, label %then176822                             ; if

then176822:
  %arg172479 = add i64 0, 0                                                          ; quoted ()
  %arg172478 = add i64 0, 0                                                          ; quoted ()
  %rva173605 = add i64 0, 0                                                          ; quoted ()
  %rva173604 = call i64 @prim_cons(i64 %arg172478, i64 %rva173605)                   ; call prim_cons
  %rva173603 = call i64 @prim_cons(i64 %arg172479, i64 %rva173604)                   ; call prim_cons
  %cloptr176824 = inttoptr i64 %cont171551 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176825 = getelementptr inbounds i64, i64* %cloptr176824, i64 0               ; &cloptr176824[0]
  %f176827 = load i64, i64* %i0ptr176825, align 8                                    ; load; *i0ptr176825
  %fptr176826 = inttoptr i64 %f176827 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176826(i64 %cont171551, i64 %rva173603)             ; tail call
  ret void

else176823:
  %a171214 = call i64 @prim_car(i64 %njg$lst)                                        ; call prim_car
  %a171215 = call i64 @prim_cdr(i64 %njg$lst)                                        ; call prim_cdr
  %arg172483 = call i64 @const_init_int(i64 1)                                       ; quoted int
  %a171216 = call i64 @prim__45(i64 %rDL$n, i64 %arg172483)                          ; call prim__45
  %cloptr176828 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr176830 = getelementptr inbounds i64, i64* %cloptr176828, i64 1                ; &eptr176830[1]
  %eptr176831 = getelementptr inbounds i64, i64* %cloptr176828, i64 2                ; &eptr176831[2]
  store i64 %a171214, i64* %eptr176830                                               ; *eptr176830 = %a171214
  store i64 %cont171551, i64* %eptr176831                                            ; *eptr176831 = %cont171551
  %eptr176829 = getelementptr inbounds i64, i64* %cloptr176828, i64 0                ; &cloptr176828[0]
  %f176832 = ptrtoint void(i64,i64)* @lam173791 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176832, i64* %eptr176829                                               ; store fptr
  %arg172487 = ptrtoint i64* %cloptr176828 to i64                                    ; closure cast; i64* -> i64
  %rva173616 = add i64 0, 0                                                          ; quoted ()
  %rva173615 = call i64 @prim_cons(i64 %a171216, i64 %rva173616)                     ; call prim_cons
  %rva173614 = call i64 @prim_cons(i64 %a171215, i64 %rva173615)                     ; call prim_cons
  %rva173613 = call i64 @prim_cons(i64 %arg172487, i64 %rva173614)                   ; call prim_cons
  %cloptr176833 = inttoptr i64 %Zju$_37take to i64*                                  ; closure/env cast; i64 -> i64*
  %i0ptr176834 = getelementptr inbounds i64, i64* %cloptr176833, i64 0               ; &cloptr176833[0]
  %f176836 = load i64, i64* %i0ptr176834, align 8                                    ; load; *i0ptr176834
  %fptr176835 = inttoptr i64 %f176836 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176835(i64 %Zju$_37take, i64 %rva173613)            ; tail call
  ret void
}


define void @lam173791(i64 %env173792, i64 %rvp173612) {
  %envptr176837 = inttoptr i64 %env173792 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176838 = getelementptr inbounds i64, i64* %envptr176837, i64 2              ; &envptr176837[2]
  %cont171551 = load i64, i64* %envptr176838, align 8                                ; load; *envptr176838
  %envptr176839 = inttoptr i64 %env173792 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176840 = getelementptr inbounds i64, i64* %envptr176839, i64 1              ; &envptr176839[1]
  %a171214 = load i64, i64* %envptr176840, align 8                                   ; load; *envptr176840
  %_95171552 = call i64 @prim_car(i64 %rvp173612)                                    ; call prim_car
  %rvp173611 = call i64 @prim_cdr(i64 %rvp173612)                                    ; call prim_cdr
  %a171217 = call i64 @prim_car(i64 %rvp173611)                                      ; call prim_car
  %na173607 = call i64 @prim_cdr(i64 %rvp173611)                                     ; call prim_cdr
  %retprim171553 = call i64 @prim_cons(i64 %a171214, i64 %a171217)                   ; call prim_cons
  %arg172492 = add i64 0, 0                                                          ; quoted ()
  %rva173610 = add i64 0, 0                                                          ; quoted ()
  %rva173609 = call i64 @prim_cons(i64 %retprim171553, i64 %rva173610)               ; call prim_cons
  %rva173608 = call i64 @prim_cons(i64 %arg172492, i64 %rva173609)                   ; call prim_cons
  %cloptr176841 = inttoptr i64 %cont171551 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176842 = getelementptr inbounds i64, i64* %cloptr176841, i64 0               ; &cloptr176841[0]
  %f176844 = load i64, i64* %i0ptr176842, align 8                                    ; load; *i0ptr176842
  %fptr176843 = inttoptr i64 %f176844 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176843(i64 %cont171551, i64 %rva173608)             ; tail call
  ret void
}


define void @lam173778(i64 %env173779, i64 %rvp173662) {
  %cont171554 = call i64 @prim_car(i64 %rvp173662)                                   ; call prim_car
  %rvp173661 = call i64 @prim_cdr(i64 %rvp173662)                                    ; call prim_cdr
  %bPh$_37map = call i64 @prim_car(i64 %rvp173661)                                   ; call prim_car
  %na173631 = call i64 @prim_cdr(i64 %rvp173661)                                     ; call prim_cdr
  %arg172495 = add i64 0, 0                                                          ; quoted ()
  %cloptr176845 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr176847 = getelementptr inbounds i64, i64* %cloptr176845, i64 1                ; &eptr176847[1]
  store i64 %bPh$_37map, i64* %eptr176847                                            ; *eptr176847 = %bPh$_37map
  %eptr176846 = getelementptr inbounds i64, i64* %cloptr176845, i64 0                ; &cloptr176845[0]
  %f176848 = ptrtoint void(i64,i64)* @lam173775 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176848, i64* %eptr176846                                               ; store fptr
  %arg172494 = ptrtoint i64* %cloptr176845 to i64                                    ; closure cast; i64* -> i64
  %rva173660 = add i64 0, 0                                                          ; quoted ()
  %rva173659 = call i64 @prim_cons(i64 %arg172494, i64 %rva173660)                   ; call prim_cons
  %rva173658 = call i64 @prim_cons(i64 %arg172495, i64 %rva173659)                   ; call prim_cons
  %cloptr176849 = inttoptr i64 %cont171554 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176850 = getelementptr inbounds i64, i64* %cloptr176849, i64 0               ; &cloptr176849[0]
  %f176852 = load i64, i64* %i0ptr176850, align 8                                    ; load; *i0ptr176850
  %fptr176851 = inttoptr i64 %f176852 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176851(i64 %cont171554, i64 %rva173658)             ; tail call
  ret void
}


define void @lam173775(i64 %env173776, i64 %rvp173657) {
  %envptr176853 = inttoptr i64 %env173776 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176854 = getelementptr inbounds i64, i64* %envptr176853, i64 1              ; &envptr176853[1]
  %bPh$_37map = load i64, i64* %envptr176854, align 8                                ; load; *envptr176854
  %cont171555 = call i64 @prim_car(i64 %rvp173657)                                   ; call prim_car
  %rvp173656 = call i64 @prim_cdr(i64 %rvp173657)                                    ; call prim_cdr
  %gIK$f = call i64 @prim_car(i64 %rvp173656)                                        ; call prim_car
  %rvp173655 = call i64 @prim_cdr(i64 %rvp173656)                                    ; call prim_cdr
  %ESl$lst = call i64 @prim_car(i64 %rvp173655)                                      ; call prim_car
  %na173633 = call i64 @prim_cdr(i64 %rvp173655)                                     ; call prim_cdr
  %a171207 = call i64 @prim_null_63(i64 %ESl$lst)                                    ; call prim_null_63
  %cmp176855 = icmp eq i64 %a171207, 15                                              ; false?
  br i1 %cmp176855, label %else176857, label %then176856                             ; if

then176856:
  %arg172499 = add i64 0, 0                                                          ; quoted ()
  %arg172498 = add i64 0, 0                                                          ; quoted ()
  %rva173636 = add i64 0, 0                                                          ; quoted ()
  %rva173635 = call i64 @prim_cons(i64 %arg172498, i64 %rva173636)                   ; call prim_cons
  %rva173634 = call i64 @prim_cons(i64 %arg172499, i64 %rva173635)                   ; call prim_cons
  %cloptr176858 = inttoptr i64 %cont171555 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176859 = getelementptr inbounds i64, i64* %cloptr176858, i64 0               ; &cloptr176858[0]
  %f176861 = load i64, i64* %i0ptr176859, align 8                                    ; load; *i0ptr176859
  %fptr176860 = inttoptr i64 %f176861 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176860(i64 %cont171555, i64 %rva173634)             ; tail call
  ret void

else176857:
  %a171208 = call i64 @prim_car(i64 %ESl$lst)                                        ; call prim_car
  %cloptr176862 = call i64* @alloc(i64 40)                                           ; malloc
  %eptr176864 = getelementptr inbounds i64, i64* %cloptr176862, i64 1                ; &eptr176864[1]
  %eptr176865 = getelementptr inbounds i64, i64* %cloptr176862, i64 2                ; &eptr176865[2]
  %eptr176866 = getelementptr inbounds i64, i64* %cloptr176862, i64 3                ; &eptr176866[3]
  %eptr176867 = getelementptr inbounds i64, i64* %cloptr176862, i64 4                ; &eptr176867[4]
  store i64 %ESl$lst, i64* %eptr176864                                               ; *eptr176864 = %ESl$lst
  store i64 %bPh$_37map, i64* %eptr176865                                            ; *eptr176865 = %bPh$_37map
  store i64 %gIK$f, i64* %eptr176866                                                 ; *eptr176866 = %gIK$f
  store i64 %cont171555, i64* %eptr176867                                            ; *eptr176867 = %cont171555
  %eptr176863 = getelementptr inbounds i64, i64* %cloptr176862, i64 0                ; &cloptr176862[0]
  %f176868 = ptrtoint void(i64,i64)* @lam173773 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176868, i64* %eptr176863                                               ; store fptr
  %arg172503 = ptrtoint i64* %cloptr176862 to i64                                    ; closure cast; i64* -> i64
  %rva173654 = add i64 0, 0                                                          ; quoted ()
  %rva173653 = call i64 @prim_cons(i64 %a171208, i64 %rva173654)                     ; call prim_cons
  %rva173652 = call i64 @prim_cons(i64 %arg172503, i64 %rva173653)                   ; call prim_cons
  %cloptr176869 = inttoptr i64 %gIK$f to i64*                                        ; closure/env cast; i64 -> i64*
  %i0ptr176870 = getelementptr inbounds i64, i64* %cloptr176869, i64 0               ; &cloptr176869[0]
  %f176872 = load i64, i64* %i0ptr176870, align 8                                    ; load; *i0ptr176870
  %fptr176871 = inttoptr i64 %f176872 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176871(i64 %gIK$f, i64 %rva173652)                  ; tail call
  ret void
}


define void @lam173773(i64 %env173774, i64 %rvp173651) {
  %envptr176873 = inttoptr i64 %env173774 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176874 = getelementptr inbounds i64, i64* %envptr176873, i64 4              ; &envptr176873[4]
  %cont171555 = load i64, i64* %envptr176874, align 8                                ; load; *envptr176874
  %envptr176875 = inttoptr i64 %env173774 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176876 = getelementptr inbounds i64, i64* %envptr176875, i64 3              ; &envptr176875[3]
  %gIK$f = load i64, i64* %envptr176876, align 8                                     ; load; *envptr176876
  %envptr176877 = inttoptr i64 %env173774 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176878 = getelementptr inbounds i64, i64* %envptr176877, i64 2              ; &envptr176877[2]
  %bPh$_37map = load i64, i64* %envptr176878, align 8                                ; load; *envptr176878
  %envptr176879 = inttoptr i64 %env173774 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176880 = getelementptr inbounds i64, i64* %envptr176879, i64 1              ; &envptr176879[1]
  %ESl$lst = load i64, i64* %envptr176880, align 8                                   ; load; *envptr176880
  %_95171556 = call i64 @prim_car(i64 %rvp173651)                                    ; call prim_car
  %rvp173650 = call i64 @prim_cdr(i64 %rvp173651)                                    ; call prim_cdr
  %a171209 = call i64 @prim_car(i64 %rvp173650)                                      ; call prim_car
  %na173638 = call i64 @prim_cdr(i64 %rvp173650)                                     ; call prim_cdr
  %a171210 = call i64 @prim_cdr(i64 %ESl$lst)                                        ; call prim_cdr
  %cloptr176881 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr176883 = getelementptr inbounds i64, i64* %cloptr176881, i64 1                ; &eptr176883[1]
  %eptr176884 = getelementptr inbounds i64, i64* %cloptr176881, i64 2                ; &eptr176884[2]
  store i64 %cont171555, i64* %eptr176883                                            ; *eptr176883 = %cont171555
  store i64 %a171209, i64* %eptr176884                                               ; *eptr176884 = %a171209
  %eptr176882 = getelementptr inbounds i64, i64* %cloptr176881, i64 0                ; &cloptr176881[0]
  %f176885 = ptrtoint void(i64,i64)* @lam173771 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176885, i64* %eptr176882                                               ; store fptr
  %arg172508 = ptrtoint i64* %cloptr176881 to i64                                    ; closure cast; i64* -> i64
  %rva173649 = add i64 0, 0                                                          ; quoted ()
  %rva173648 = call i64 @prim_cons(i64 %a171210, i64 %rva173649)                     ; call prim_cons
  %rva173647 = call i64 @prim_cons(i64 %gIK$f, i64 %rva173648)                       ; call prim_cons
  %rva173646 = call i64 @prim_cons(i64 %arg172508, i64 %rva173647)                   ; call prim_cons
  %cloptr176886 = inttoptr i64 %bPh$_37map to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176887 = getelementptr inbounds i64, i64* %cloptr176886, i64 0               ; &cloptr176886[0]
  %f176889 = load i64, i64* %i0ptr176887, align 8                                    ; load; *i0ptr176887
  %fptr176888 = inttoptr i64 %f176889 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176888(i64 %bPh$_37map, i64 %rva173646)             ; tail call
  ret void
}


define void @lam173771(i64 %env173772, i64 %rvp173645) {
  %envptr176890 = inttoptr i64 %env173772 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176891 = getelementptr inbounds i64, i64* %envptr176890, i64 2              ; &envptr176890[2]
  %a171209 = load i64, i64* %envptr176891, align 8                                   ; load; *envptr176891
  %envptr176892 = inttoptr i64 %env173772 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176893 = getelementptr inbounds i64, i64* %envptr176892, i64 1              ; &envptr176892[1]
  %cont171555 = load i64, i64* %envptr176893, align 8                                ; load; *envptr176893
  %_95171557 = call i64 @prim_car(i64 %rvp173645)                                    ; call prim_car
  %rvp173644 = call i64 @prim_cdr(i64 %rvp173645)                                    ; call prim_cdr
  %a171211 = call i64 @prim_car(i64 %rvp173644)                                      ; call prim_car
  %na173640 = call i64 @prim_cdr(i64 %rvp173644)                                     ; call prim_cdr
  %retprim171558 = call i64 @prim_cons(i64 %a171209, i64 %a171211)                   ; call prim_cons
  %arg172513 = add i64 0, 0                                                          ; quoted ()
  %rva173643 = add i64 0, 0                                                          ; quoted ()
  %rva173642 = call i64 @prim_cons(i64 %retprim171558, i64 %rva173643)               ; call prim_cons
  %rva173641 = call i64 @prim_cons(i64 %arg172513, i64 %rva173642)                   ; call prim_cons
  %cloptr176894 = inttoptr i64 %cont171555 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176895 = getelementptr inbounds i64, i64* %cloptr176894, i64 0               ; &cloptr176894[0]
  %f176897 = load i64, i64* %i0ptr176895, align 8                                    ; load; *i0ptr176895
  %fptr176896 = inttoptr i64 %f176897 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176896(i64 %cont171555, i64 %rva173641)             ; tail call
  ret void
}


define void @lam173760(i64 %env173761, i64 %rvp173696) {
  %cont171559 = call i64 @prim_car(i64 %rvp173696)                                   ; call prim_car
  %rvp173695 = call i64 @prim_cdr(i64 %rvp173696)                                    ; call prim_cdr
  %Xwk$_37foldr1 = call i64 @prim_car(i64 %rvp173695)                                ; call prim_car
  %na173669 = call i64 @prim_cdr(i64 %rvp173695)                                     ; call prim_cdr
  %arg172516 = add i64 0, 0                                                          ; quoted ()
  %cloptr176898 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr176900 = getelementptr inbounds i64, i64* %cloptr176898, i64 1                ; &eptr176900[1]
  store i64 %Xwk$_37foldr1, i64* %eptr176900                                         ; *eptr176900 = %Xwk$_37foldr1
  %eptr176899 = getelementptr inbounds i64, i64* %cloptr176898, i64 0                ; &cloptr176898[0]
  %f176901 = ptrtoint void(i64,i64)* @lam173757 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176901, i64* %eptr176899                                               ; store fptr
  %arg172515 = ptrtoint i64* %cloptr176898 to i64                                    ; closure cast; i64* -> i64
  %rva173694 = add i64 0, 0                                                          ; quoted ()
  %rva173693 = call i64 @prim_cons(i64 %arg172515, i64 %rva173694)                   ; call prim_cons
  %rva173692 = call i64 @prim_cons(i64 %arg172516, i64 %rva173693)                   ; call prim_cons
  %cloptr176902 = inttoptr i64 %cont171559 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176903 = getelementptr inbounds i64, i64* %cloptr176902, i64 0               ; &cloptr176902[0]
  %f176905 = load i64, i64* %i0ptr176903, align 8                                    ; load; *i0ptr176903
  %fptr176904 = inttoptr i64 %f176905 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176904(i64 %cont171559, i64 %rva173692)             ; tail call
  ret void
}


define void @lam173757(i64 %env173758, i64 %rvp173691) {
  %envptr176906 = inttoptr i64 %env173758 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176907 = getelementptr inbounds i64, i64* %envptr176906, i64 1              ; &envptr176906[1]
  %Xwk$_37foldr1 = load i64, i64* %envptr176907, align 8                             ; load; *envptr176907
  %cont171560 = call i64 @prim_car(i64 %rvp173691)                                   ; call prim_car
  %rvp173690 = call i64 @prim_cdr(i64 %rvp173691)                                    ; call prim_cdr
  %WiP$f = call i64 @prim_car(i64 %rvp173690)                                        ; call prim_car
  %rvp173689 = call i64 @prim_cdr(i64 %rvp173690)                                    ; call prim_cdr
  %h94$acc = call i64 @prim_car(i64 %rvp173689)                                      ; call prim_car
  %rvp173688 = call i64 @prim_cdr(i64 %rvp173689)                                    ; call prim_cdr
  %IZW$lst = call i64 @prim_car(i64 %rvp173688)                                      ; call prim_car
  %na173671 = call i64 @prim_cdr(i64 %rvp173688)                                     ; call prim_cdr
  %a171203 = call i64 @prim_null_63(i64 %IZW$lst)                                    ; call prim_null_63
  %cmp176908 = icmp eq i64 %a171203, 15                                              ; false?
  br i1 %cmp176908, label %else176910, label %then176909                             ; if

then176909:
  %arg172520 = add i64 0, 0                                                          ; quoted ()
  %rva173674 = add i64 0, 0                                                          ; quoted ()
  %rva173673 = call i64 @prim_cons(i64 %h94$acc, i64 %rva173674)                     ; call prim_cons
  %rva173672 = call i64 @prim_cons(i64 %arg172520, i64 %rva173673)                   ; call prim_cons
  %cloptr176911 = inttoptr i64 %cont171560 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176912 = getelementptr inbounds i64, i64* %cloptr176911, i64 0               ; &cloptr176911[0]
  %f176914 = load i64, i64* %i0ptr176912, align 8                                    ; load; *i0ptr176912
  %fptr176913 = inttoptr i64 %f176914 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176913(i64 %cont171560, i64 %rva173672)             ; tail call
  ret void

else176910:
  %a171204 = call i64 @prim_car(i64 %IZW$lst)                                        ; call prim_car
  %a171205 = call i64 @prim_cdr(i64 %IZW$lst)                                        ; call prim_cdr
  %cloptr176915 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr176917 = getelementptr inbounds i64, i64* %cloptr176915, i64 1                ; &eptr176917[1]
  %eptr176918 = getelementptr inbounds i64, i64* %cloptr176915, i64 2                ; &eptr176918[2]
  %eptr176919 = getelementptr inbounds i64, i64* %cloptr176915, i64 3                ; &eptr176919[3]
  store i64 %cont171560, i64* %eptr176917                                            ; *eptr176917 = %cont171560
  store i64 %a171204, i64* %eptr176918                                               ; *eptr176918 = %a171204
  store i64 %WiP$f, i64* %eptr176919                                                 ; *eptr176919 = %WiP$f
  %eptr176916 = getelementptr inbounds i64, i64* %cloptr176915, i64 0                ; &cloptr176915[0]
  %f176920 = ptrtoint void(i64,i64)* @lam173755 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176920, i64* %eptr176916                                               ; store fptr
  %arg172527 = ptrtoint i64* %cloptr176915 to i64                                    ; closure cast; i64* -> i64
  %rva173687 = add i64 0, 0                                                          ; quoted ()
  %rva173686 = call i64 @prim_cons(i64 %a171205, i64 %rva173687)                     ; call prim_cons
  %rva173685 = call i64 @prim_cons(i64 %h94$acc, i64 %rva173686)                     ; call prim_cons
  %rva173684 = call i64 @prim_cons(i64 %WiP$f, i64 %rva173685)                       ; call prim_cons
  %rva173683 = call i64 @prim_cons(i64 %arg172527, i64 %rva173684)                   ; call prim_cons
  %cloptr176921 = inttoptr i64 %Xwk$_37foldr1 to i64*                                ; closure/env cast; i64 -> i64*
  %i0ptr176922 = getelementptr inbounds i64, i64* %cloptr176921, i64 0               ; &cloptr176921[0]
  %f176924 = load i64, i64* %i0ptr176922, align 8                                    ; load; *i0ptr176922
  %fptr176923 = inttoptr i64 %f176924 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176923(i64 %Xwk$_37foldr1, i64 %rva173683)          ; tail call
  ret void
}


define void @lam173755(i64 %env173756, i64 %rvp173682) {
  %envptr176925 = inttoptr i64 %env173756 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176926 = getelementptr inbounds i64, i64* %envptr176925, i64 3              ; &envptr176925[3]
  %WiP$f = load i64, i64* %envptr176926, align 8                                     ; load; *envptr176926
  %envptr176927 = inttoptr i64 %env173756 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176928 = getelementptr inbounds i64, i64* %envptr176927, i64 2              ; &envptr176927[2]
  %a171204 = load i64, i64* %envptr176928, align 8                                   ; load; *envptr176928
  %envptr176929 = inttoptr i64 %env173756 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176930 = getelementptr inbounds i64, i64* %envptr176929, i64 1              ; &envptr176929[1]
  %cont171560 = load i64, i64* %envptr176930, align 8                                ; load; *envptr176930
  %_95171561 = call i64 @prim_car(i64 %rvp173682)                                    ; call prim_car
  %rvp173681 = call i64 @prim_cdr(i64 %rvp173682)                                    ; call prim_cdr
  %a171206 = call i64 @prim_car(i64 %rvp173681)                                      ; call prim_car
  %na173676 = call i64 @prim_cdr(i64 %rvp173681)                                     ; call prim_cdr
  %rva173680 = add i64 0, 0                                                          ; quoted ()
  %rva173679 = call i64 @prim_cons(i64 %a171206, i64 %rva173680)                     ; call prim_cons
  %rva173678 = call i64 @prim_cons(i64 %a171204, i64 %rva173679)                     ; call prim_cons
  %rva173677 = call i64 @prim_cons(i64 %cont171560, i64 %rva173678)                  ; call prim_cons
  %cloptr176931 = inttoptr i64 %WiP$f to i64*                                        ; closure/env cast; i64 -> i64*
  %i0ptr176932 = getelementptr inbounds i64, i64* %cloptr176931, i64 0               ; &cloptr176931[0]
  %f176934 = load i64, i64* %i0ptr176932, align 8                                    ; load; *i0ptr176932
  %fptr176933 = inttoptr i64 %f176934 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176933(i64 %WiP$f, i64 %rva173677)                  ; tail call
  ret void
}


define void @lam173747(i64 %env173748, i64 %rvp173729) {
  %cont171563 = call i64 @prim_car(i64 %rvp173729)                                   ; call prim_car
  %rvp173728 = call i64 @prim_cdr(i64 %rvp173729)                                    ; call prim_cdr
  %FZG$y = call i64 @prim_car(i64 %rvp173728)                                        ; call prim_car
  %na173703 = call i64 @prim_cdr(i64 %rvp173728)                                     ; call prim_cdr
  %arg172534 = add i64 0, 0                                                          ; quoted ()
  %cloptr176935 = call i64* @alloc(i64 16)                                           ; malloc
  %eptr176937 = getelementptr inbounds i64, i64* %cloptr176935, i64 1                ; &eptr176937[1]
  store i64 %FZG$y, i64* %eptr176937                                                 ; *eptr176937 = %FZG$y
  %eptr176936 = getelementptr inbounds i64, i64* %cloptr176935, i64 0                ; &cloptr176935[0]
  %f176938 = ptrtoint void(i64,i64)* @lam173744 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176938, i64* %eptr176936                                               ; store fptr
  %arg172533 = ptrtoint i64* %cloptr176935 to i64                                    ; closure cast; i64* -> i64
  %rva173727 = add i64 0, 0                                                          ; quoted ()
  %rva173726 = call i64 @prim_cons(i64 %arg172533, i64 %rva173727)                   ; call prim_cons
  %rva173725 = call i64 @prim_cons(i64 %arg172534, i64 %rva173726)                   ; call prim_cons
  %cloptr176939 = inttoptr i64 %cont171563 to i64*                                   ; closure/env cast; i64 -> i64*
  %i0ptr176940 = getelementptr inbounds i64, i64* %cloptr176939, i64 0               ; &cloptr176939[0]
  %f176942 = load i64, i64* %i0ptr176940, align 8                                    ; load; *i0ptr176940
  %fptr176941 = inttoptr i64 %f176942 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176941(i64 %cont171563, i64 %rva173725)             ; tail call
  ret void
}


define void @lam173744(i64 %env173745, i64 %rvp173724) {
  %envptr176943 = inttoptr i64 %env173745 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176944 = getelementptr inbounds i64, i64* %envptr176943, i64 1              ; &envptr176943[1]
  %FZG$y = load i64, i64* %envptr176944, align 8                                     ; load; *envptr176944
  %cont171564 = call i64 @prim_car(i64 %rvp173724)                                   ; call prim_car
  %rvp173723 = call i64 @prim_cdr(i64 %rvp173724)                                    ; call prim_cdr
  %eqf$f = call i64 @prim_car(i64 %rvp173723)                                        ; call prim_car
  %na173705 = call i64 @prim_cdr(i64 %rvp173723)                                     ; call prim_cdr
  %cloptr176945 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr176947 = getelementptr inbounds i64, i64* %cloptr176945, i64 1                ; &eptr176947[1]
  %eptr176948 = getelementptr inbounds i64, i64* %cloptr176945, i64 2                ; &eptr176948[2]
  store i64 %FZG$y, i64* %eptr176947                                                 ; *eptr176947 = %FZG$y
  store i64 %eqf$f, i64* %eptr176948                                                 ; *eptr176948 = %eqf$f
  %eptr176946 = getelementptr inbounds i64, i64* %cloptr176945, i64 0                ; &cloptr176945[0]
  %f176949 = ptrtoint void(i64,i64)* @lam173742 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176949, i64* %eptr176946                                               ; store fptr
  %arg172536 = ptrtoint i64* %cloptr176945 to i64                                    ; closure cast; i64* -> i64
  %rva173722 = add i64 0, 0                                                          ; quoted ()
  %rva173721 = call i64 @prim_cons(i64 %arg172536, i64 %rva173722)                   ; call prim_cons
  %rva173720 = call i64 @prim_cons(i64 %cont171564, i64 %rva173721)                  ; call prim_cons
  %cloptr176950 = inttoptr i64 %eqf$f to i64*                                        ; closure/env cast; i64 -> i64*
  %i0ptr176951 = getelementptr inbounds i64, i64* %cloptr176950, i64 0               ; &cloptr176950[0]
  %f176953 = load i64, i64* %i0ptr176951, align 8                                    ; load; *i0ptr176951
  %fptr176952 = inttoptr i64 %f176953 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176952(i64 %eqf$f, i64 %rva173720)                  ; tail call
  ret void
}


define void @lam173742(i64 %env173743, i64 %h9x$args171566) {
  %envptr176954 = inttoptr i64 %env173743 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176955 = getelementptr inbounds i64, i64* %envptr176954, i64 2              ; &envptr176954[2]
  %eqf$f = load i64, i64* %envptr176955, align 8                                     ; load; *envptr176955
  %envptr176956 = inttoptr i64 %env173743 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176957 = getelementptr inbounds i64, i64* %envptr176956, i64 1              ; &envptr176956[1]
  %FZG$y = load i64, i64* %envptr176957, align 8                                     ; load; *envptr176957
  %cont171565 = call i64 @prim_car(i64 %h9x$args171566)                              ; call prim_car
  %h9x$args = call i64 @prim_cdr(i64 %h9x$args171566)                                ; call prim_cdr
  %cloptr176958 = call i64* @alloc(i64 32)                                           ; malloc
  %eptr176960 = getelementptr inbounds i64, i64* %cloptr176958, i64 1                ; &eptr176960[1]
  %eptr176961 = getelementptr inbounds i64, i64* %cloptr176958, i64 2                ; &eptr176961[2]
  %eptr176962 = getelementptr inbounds i64, i64* %cloptr176958, i64 3                ; &eptr176962[3]
  store i64 %h9x$args, i64* %eptr176960                                              ; *eptr176960 = %h9x$args
  store i64 %eqf$f, i64* %eptr176961                                                 ; *eptr176961 = %eqf$f
  store i64 %cont171565, i64* %eptr176962                                            ; *eptr176962 = %cont171565
  %eptr176959 = getelementptr inbounds i64, i64* %cloptr176958, i64 0                ; &cloptr176958[0]
  %f176963 = ptrtoint void(i64,i64)* @lam173740 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176963, i64* %eptr176959                                               ; store fptr
  %arg172542 = ptrtoint i64* %cloptr176958 to i64                                    ; closure cast; i64* -> i64
  %rva173719 = add i64 0, 0                                                          ; quoted ()
  %rva173718 = call i64 @prim_cons(i64 %FZG$y, i64 %rva173719)                       ; call prim_cons
  %rva173717 = call i64 @prim_cons(i64 %arg172542, i64 %rva173718)                   ; call prim_cons
  %cloptr176964 = inttoptr i64 %FZG$y to i64*                                        ; closure/env cast; i64 -> i64*
  %i0ptr176965 = getelementptr inbounds i64, i64* %cloptr176964, i64 0               ; &cloptr176964[0]
  %f176967 = load i64, i64* %i0ptr176965, align 8                                    ; load; *i0ptr176965
  %fptr176966 = inttoptr i64 %f176967 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176966(i64 %FZG$y, i64 %rva173717)                  ; tail call
  ret void
}


define void @lam173740(i64 %env173741, i64 %rvp173716) {
  %envptr176968 = inttoptr i64 %env173741 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176969 = getelementptr inbounds i64, i64* %envptr176968, i64 3              ; &envptr176968[3]
  %cont171565 = load i64, i64* %envptr176969, align 8                                ; load; *envptr176969
  %envptr176970 = inttoptr i64 %env173741 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176971 = getelementptr inbounds i64, i64* %envptr176970, i64 2              ; &envptr176970[2]
  %eqf$f = load i64, i64* %envptr176971, align 8                                     ; load; *envptr176971
  %envptr176972 = inttoptr i64 %env173741 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176973 = getelementptr inbounds i64, i64* %envptr176972, i64 1              ; &envptr176972[1]
  %h9x$args = load i64, i64* %envptr176973, align 8                                  ; load; *envptr176973
  %_95171567 = call i64 @prim_car(i64 %rvp173716)                                    ; call prim_car
  %rvp173715 = call i64 @prim_cdr(i64 %rvp173716)                                    ; call prim_cdr
  %a171201 = call i64 @prim_car(i64 %rvp173715)                                      ; call prim_car
  %na173707 = call i64 @prim_cdr(i64 %rvp173715)                                     ; call prim_cdr
  %cloptr176974 = call i64* @alloc(i64 24)                                           ; malloc
  %eptr176976 = getelementptr inbounds i64, i64* %cloptr176974, i64 1                ; &eptr176976[1]
  %eptr176977 = getelementptr inbounds i64, i64* %cloptr176974, i64 2                ; &eptr176977[2]
  store i64 %h9x$args, i64* %eptr176976                                              ; *eptr176976 = %h9x$args
  store i64 %cont171565, i64* %eptr176977                                            ; *eptr176977 = %cont171565
  %eptr176975 = getelementptr inbounds i64, i64* %cloptr176974, i64 0                ; &cloptr176974[0]
  %f176978 = ptrtoint void(i64,i64)* @lam173738 to i64                               ; fptr cast; i64(...)* -> i64
  store i64 %f176978, i64* %eptr176975                                               ; store fptr
  %arg172545 = ptrtoint i64* %cloptr176974 to i64                                    ; closure cast; i64* -> i64
  %rva173714 = add i64 0, 0                                                          ; quoted ()
  %rva173713 = call i64 @prim_cons(i64 %eqf$f, i64 %rva173714)                       ; call prim_cons
  %rva173712 = call i64 @prim_cons(i64 %arg172545, i64 %rva173713)                   ; call prim_cons
  %cloptr176979 = inttoptr i64 %a171201 to i64*                                      ; closure/env cast; i64 -> i64*
  %i0ptr176980 = getelementptr inbounds i64, i64* %cloptr176979, i64 0               ; &cloptr176979[0]
  %f176982 = load i64, i64* %i0ptr176980, align 8                                    ; load; *i0ptr176980
  %fptr176981 = inttoptr i64 %f176982 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176981(i64 %a171201, i64 %rva173712)                ; tail call
  ret void
}


define void @lam173738(i64 %env173739, i64 %rvp173711) {
  %envptr176983 = inttoptr i64 %env173739 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176984 = getelementptr inbounds i64, i64* %envptr176983, i64 2              ; &envptr176983[2]
  %cont171565 = load i64, i64* %envptr176984, align 8                                ; load; *envptr176984
  %envptr176985 = inttoptr i64 %env173739 to i64*                                    ; closure/env cast; i64 -> i64*
  %envptr176986 = getelementptr inbounds i64, i64* %envptr176985, i64 1              ; &envptr176985[1]
  %h9x$args = load i64, i64* %envptr176986, align 8                                  ; load; *envptr176986
  %_95171568 = call i64 @prim_car(i64 %rvp173711)                                    ; call prim_car
  %rvp173710 = call i64 @prim_cdr(i64 %rvp173711)                                    ; call prim_cdr
  %a171202 = call i64 @prim_car(i64 %rvp173710)                                      ; call prim_car
  %na173709 = call i64 @prim_cdr(i64 %rvp173710)                                     ; call prim_cdr
  %cps_45lst171569 = call i64 @prim_cons(i64 %cont171565, i64 %h9x$args)             ; call prim_cons
  %cloptr176987 = inttoptr i64 %a171202 to i64*                                      ; closure/env cast; i64 -> i64*
  %i0ptr176988 = getelementptr inbounds i64, i64* %cloptr176987, i64 0               ; &cloptr176987[0]
  %f176990 = load i64, i64* %i0ptr176988, align 8                                    ; load; *i0ptr176988
  %fptr176989 = inttoptr i64 %f176990 to void (i64,i64)*                             ; cast fptr; i64 -> void(...)*
  musttail call fastcc void %fptr176989(i64 %a171202, i64 %cps_45lst171569)          ; tail call
  ret void
}





@sym175266 = private unnamed_addr constant [10 x i8] c"%%promise\00", align 8
