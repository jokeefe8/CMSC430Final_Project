(define x (make-hash (list (cons 5 46) (cons 6 4) (cons 2 8) (cons 1 9) (cons 102 293))))
(+ (hash-ref x 5) (hash-ref x 6) (hash-ref x 102))
