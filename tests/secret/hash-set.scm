(define x (make-hash (list (cons 1 2) (cons 3 4))))
(define y (hash-ref x 3))
(hash-set! x 3 7)
(- y (hash-ref x 3))
