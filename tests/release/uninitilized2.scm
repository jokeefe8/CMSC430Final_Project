(define r 4)
(define (g a b) (+ a b))
(define (f a b) (a b b))
(begin
  (f g r)
  (f g s))
