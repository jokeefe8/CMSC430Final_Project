(define a 2147483647)
(define b -2147483648)

(+ a b)
(+ a -5)
(+ b 5)
(+ b -1)
