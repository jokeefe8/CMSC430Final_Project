; ModuleID = 'header.cpp'
source_filename = "header.cpp"
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.13.0"

%struct.my_hash = type { i64, i64, %struct.UT_hash_handle }
%struct.UT_hash_handle = type { %struct.UT_hash_table*, i8*, i8*, %struct.UT_hash_handle*, %struct.UT_hash_handle*, i8*, i32, i32 }
%struct.UT_hash_table = type { %struct.UT_hash_bucket*, i32, i32, i32, %struct.UT_hash_handle*, i64, i32, i32, i32, i32, i32 }
%struct.UT_hash_bucket = type { %struct.UT_hash_handle*, i32, i32 }

@MEMLIMIT = global i64 268435456, align 8
@MEMCOUNT = global i64 0, align 8
@.str = private unnamed_addr constant [25 x i8] c"library run-time error: \00", align 1
@.str.1 = private unnamed_addr constant [3 x i8] c"%s\00", align 1
@.str.2 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.3 = private unnamed_addr constant [46 x i8] c"Error: Program exceeded memory cap of 256 MB!\00", align 1
@.str.4 = private unnamed_addr constant [6 x i8] c"%llu\0A\00", align 1
@.str.5 = private unnamed_addr constant [68 x i8] c"Expected value: null (in expect_args0). Prim cannot take arguments.\00", align 1
@.str.6 = private unnamed_addr constant [79 x i8] c"Expected cons value (in expect_args1). Prim applied on an empty argument list.\00", align 1
@.str.7 = private unnamed_addr constant [70 x i8] c"Expected null value (in expect_args1). Prim can only take 1 argument.\00", align 1
@.str.8 = private unnamed_addr constant [37 x i8] c"Expected a cons value. (expect_cons)\00", align 1
@.str.9 = private unnamed_addr constant [51 x i8] c"Expected a vector or special value. (expect_other)\00", align 1
@.str.10 = private unnamed_addr constant [3 x i8] c"()\00", align 1
@.str.11 = private unnamed_addr constant [13 x i8] c"#<procedure>\00", align 1
@.str.12 = private unnamed_addr constant [2 x i8] c"(\00", align 1
@.str.13 = private unnamed_addr constant [4 x i8] c" . \00", align 1
@.str.14 = private unnamed_addr constant [2 x i8] c")\00", align 1
@.str.15 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.16 = private unnamed_addr constant [5 x i8] c"\22%s\22\00", align 1
@.str.17 = private unnamed_addr constant [3 x i8] c"#(\00", align 1
@.str.18 = private unnamed_addr constant [2 x i8] c",\00", align 1
@.str.19 = private unnamed_addr constant [7 x i8] c"#hash(\00", align 1
@.str.20 = private unnamed_addr constant [10 x i8] c"(%d . %d)\00", align 1
@.str.21 = private unnamed_addr constant [11 x i8] c" (%d . %d)\00", align 1
@.str.22 = private unnamed_addr constant [37 x i8] c"(print.. v); unrecognized value %llu\00", align 1
@.str.23 = private unnamed_addr constant [4 x i8] c"'()\00", align 1
@.str.24 = private unnamed_addr constant [3 x i8] c"'(\00", align 1
@.str.25 = private unnamed_addr constant [4 x i8] c"'%s\00", align 1
@.str.26 = private unnamed_addr constant [35 x i8] c"(print v); unrecognized value %llu\00", align 1
@.str.27 = private unnamed_addr constant [49 x i8] c"first argument to make-vector must be an integer\00", align 1
@.str.28 = private unnamed_addr constant [39 x i8] c"prim applied on more than 2 arguments.\00", align 1
@.str.29 = private unnamed_addr constant [49 x i8] c"second argument to vector-ref must be an integer\00", align 1
@.str.30 = private unnamed_addr constant [46 x i8] c"first argument to vector-ref must be a vector\00", align 1
@.str.31 = private unnamed_addr constant [46 x i8] c"vector-ref not given a properly formed vector\00", align 1
@.str.32 = private unnamed_addr constant [72 x i8] c"Error: vector-ref attempted to access element outside of vector bounds.\00", align 1
@.str.33 = private unnamed_addr constant [49 x i8] c"second argument to vector-set must be an integer\00", align 1
@.str.34 = private unnamed_addr constant [48 x i8] c"first argument to vector-set must be an integer\00", align 1
@.str.35 = private unnamed_addr constant [46 x i8] c"vector-set not given a properly formed vector\00", align 1
@.str.36 = private unnamed_addr constant [72 x i8] c"Error: vector-set attempted to access element outside of vector bounds.\00", align 1
@.str.37 = private unnamed_addr constant [34 x i8] c"(prim + a b); a is not an integer\00", align 1
@.str.38 = private unnamed_addr constant [34 x i8] c"(prim + a b); b is not an integer\00", align 1
@.str.39 = private unnamed_addr constant [71 x i8] c"Error: potential integer overflow of C integer type through addition. \00", align 1
@.str.40 = private unnamed_addr constant [36 x i8] c"Tried to apply + on non list value.\00", align 1
@.str.41 = private unnamed_addr constant [34 x i8] c"(prim - a b); a is not an integer\00", align 1
@.str.42 = private unnamed_addr constant [34 x i8] c"(prim - a b); b is not an integer\00", align 1
@.str.43 = private unnamed_addr constant [74 x i8] c"Error: potential integer overflow of C integer type through subtraction. \00", align 1
@.str.44 = private unnamed_addr constant [36 x i8] c"Tried to apply - on non list value.\00", align 1
@.str.45 = private unnamed_addr constant [34 x i8] c"(prim * a b); a is not an integer\00", align 1
@.str.46 = private unnamed_addr constant [34 x i8] c"(prim * a b); b is not an integer\00", align 1
@.str.47 = private unnamed_addr constant [77 x i8] c"Error: potential integer overflow of C integer type through multiplication. \00", align 1
@.str.48 = private unnamed_addr constant [36 x i8] c"Tried to apply * on non list value.\00", align 1
@.str.49 = private unnamed_addr constant [34 x i8] c"(prim / a b); a is not an integer\00", align 1
@.str.50 = private unnamed_addr constant [34 x i8] c"(prim / a b); b is not an integer\00", align 1
@.str.51 = private unnamed_addr constant [49 x i8] c"Tried to divide by zero, division by zero error!\00", align 1
@.str.52 = private unnamed_addr constant [71 x i8] c"Error: potential integer overflow of C integer type through division. \00", align 1
@.str.53 = private unnamed_addr constant [34 x i8] c"(prim = a b); a is not an integer\00", align 1
@.str.54 = private unnamed_addr constant [34 x i8] c"(prim = a b); b is not an integer\00", align 1
@.str.55 = private unnamed_addr constant [34 x i8] c"(prim < a b); a is not an integer\00", align 1
@.str.56 = private unnamed_addr constant [34 x i8] c"(prim < a b); b is not an integer\00", align 1
@.str.57 = private unnamed_addr constant [35 x i8] c"(prim <= a b); a is not an integer\00", align 1
@.str.58 = private unnamed_addr constant [35 x i8] c"(prim <= a b); b is not an integer\00", align 1
@.str.59 = private unnamed_addr constant [47 x i8] c"Optional argument to make-hash must be a list.\00", align 1
@.str.60 = private unnamed_addr constant [59 x i8] c"Error: hash-ref not given a valid hash as first parameter!\00", align 1
@.str.61 = private unnamed_addr constant [60 x i8] c"Error: hash-set! not given a valid hash as first parameter!\00", align 1

; Function Attrs: noinline ssp uwtable
define void @fatal_err(i8*) #0 {
  %2 = alloca i8*, align 8
  store i8* %0, i8** %2, align 8
  %3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i32 0, i32 0))
  %4 = load i8*, i8** %2, align 8
  %5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i8* %4)
  %6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.2, i32 0, i32 0))
  call void @exit(i32 1) #7
  unreachable
                                                  ; No predecessors!
  ret void
}

declare i32 @printf(i8*, ...) #1

; Function Attrs: noreturn
declare void @exit(i32) #2

; Function Attrs: noinline ssp uwtable
define i64* @alloc(i64) #0 {
  %2 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %3 = load i64, i64* @MEMCOUNT, align 8
  %4 = load i64, i64* %2, align 8
  %5 = add i64 %3, %4
  store i64 %5, i64* @MEMCOUNT, align 8
  %6 = load i64, i64* @MEMCOUNT, align 8
  %7 = load i64, i64* @MEMLIMIT, align 8
  %8 = icmp uge i64 %6, %7
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.3, i32 0, i32 0))
  br label %10

; <label>:10:                                     ; preds = %9, %1
  %11 = load i64, i64* %2, align 8
  %12 = call i8* @malloc(i64 %11) #8
  %13 = bitcast i8* %12 to i64*
  ret i64* %13
}

; Function Attrs: allocsize(0)
declare i8* @malloc(i64) #3

; Function Attrs: noinline ssp uwtable
define void @print_u64(i64) #0 {
  %2 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %3 = load i64, i64* %2, align 8
  %4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.4, i32 0, i32 0), i64 %3)
  ret void
}

; Function Attrs: noinline ssp uwtable
define i64 @expect_args0(i64) #0 {
  %2 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %3 = load i64, i64* %2, align 8
  %4 = icmp ne i64 %3, 0
  br i1 %4, label %5, label %6

; <label>:5:                                      ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.5, i32 0, i32 0))
  br label %6

; <label>:6:                                      ; preds = %5, %1
  ret i64 0
}

; Function Attrs: noinline ssp uwtable
define i64 @expect_args1(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64*, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = and i64 %4, 7
  %6 = icmp ne i64 %5, 1
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([79 x i8], [79 x i8]* @.str.6, i32 0, i32 0))
  br label %8

; <label>:8:                                      ; preds = %7, %1
  %9 = load i64, i64* %2, align 8
  %10 = and i64 %9, -8
  %11 = inttoptr i64 %10 to i64*
  store i64* %11, i64** %3, align 8
  %12 = load i64*, i64** %3, align 8
  %13 = getelementptr inbounds i64, i64* %12, i64 1
  %14 = load i64, i64* %13, align 8
  %15 = icmp ne i64 %14, 0
  br i1 %15, label %16, label %17

; <label>:16:                                     ; preds = %8
  call void @fatal_err(i8* getelementptr inbounds ([70 x i8], [70 x i8]* @.str.7, i32 0, i32 0))
  br label %17

; <label>:17:                                     ; preds = %16, %8
  %18 = load i64*, i64** %3, align 8
  %19 = getelementptr inbounds i64, i64* %18, i64 0
  %20 = load i64, i64* %19, align 8
  ret i64 %20
}

; Function Attrs: noinline ssp uwtable
define i64 @expect_cons(i64, i64*) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64*, align 8
  %5 = alloca i64*, align 8
  store i64 %0, i64* %3, align 8
  store i64* %1, i64** %4, align 8
  %6 = load i64, i64* %3, align 8
  %7 = and i64 %6, 7
  %8 = icmp ne i64 %7, 1
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.8, i32 0, i32 0))
  br label %10

; <label>:10:                                     ; preds = %9, %2
  %11 = load i64, i64* %3, align 8
  %12 = and i64 %11, -8
  %13 = inttoptr i64 %12 to i64*
  store i64* %13, i64** %5, align 8
  %14 = load i64*, i64** %5, align 8
  %15 = getelementptr inbounds i64, i64* %14, i64 1
  %16 = load i64, i64* %15, align 8
  %17 = load i64*, i64** %4, align 8
  store i64 %16, i64* %17, align 8
  %18 = load i64*, i64** %5, align 8
  %19 = getelementptr inbounds i64, i64* %18, i64 0
  %20 = load i64, i64* %19, align 8
  ret i64 %20
}

; Function Attrs: noinline ssp uwtable
define i64 @expect_other(i64, i64*) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64*, align 8
  %5 = alloca i64*, align 8
  store i64 %0, i64* %3, align 8
  store i64* %1, i64** %4, align 8
  %6 = load i64, i64* %3, align 8
  %7 = and i64 %6, 7
  %8 = icmp ne i64 %7, 6
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.9, i32 0, i32 0))
  br label %10

; <label>:10:                                     ; preds = %9, %2
  %11 = load i64, i64* %3, align 8
  %12 = and i64 %11, -8
  %13 = inttoptr i64 %12 to i64*
  store i64* %13, i64** %5, align 8
  %14 = load i64*, i64** %5, align 8
  %15 = getelementptr inbounds i64, i64* %14, i64 1
  %16 = load i64, i64* %15, align 8
  %17 = load i64*, i64** %4, align 8
  store i64 %16, i64* %17, align 8
  %18 = load i64*, i64** %5, align 8
  %19 = getelementptr inbounds i64, i64* %18, i64 0
  %20 = load i64, i64* %19, align 8
  ret i64 %20
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @const_init_int(i64) #4 {
  %2 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %3 = load i64, i64* %2, align 8
  %4 = trunc i64 %3 to i32
  %5 = zext i32 %4 to i64
  %6 = shl i64 %5, 32
  %7 = or i64 %6, 2
  ret i64 %7
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @const_init_void() #4 {
  ret i64 39
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @const_init_null() #4 {
  ret i64 0
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @const_init_true() #4 {
  ret i64 31
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @const_init_false() #4 {
  ret i64 15
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @const_init_string(i8*) #4 {
  %2 = alloca i8*, align 8
  store i8* %0, i8** %2, align 8
  %3 = load i8*, i8** %2, align 8
  %4 = ptrtoint i8* %3 to i64
  %5 = or i64 %4, 3
  ret i64 %5
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @const_init_symbol(i8*) #4 {
  %2 = alloca i8*, align 8
  store i8* %0, i8** %2, align 8
  %3 = load i8*, i8** %2, align 8
  %4 = ptrtoint i8* %3 to i64
  %5 = or i64 %4, 4
  ret i64 %5
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_print_aux(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64*, align 8
  %4 = alloca i64*, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64, align 8
  %7 = alloca i64*, align 8
  %8 = alloca %struct.my_hash*, align 8
  %9 = alloca %struct.my_hash*, align 8
  store i64 %0, i64* %2, align 8
  %10 = load i64, i64* %2, align 8
  %11 = icmp eq i64 %10, 0
  br i1 %11, label %12, label %14

; <label>:12:                                     ; preds = %1
  %13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.10, i32 0, i32 0))
  br label %187

; <label>:14:                                     ; preds = %1
  %15 = load i64, i64* %2, align 8
  %16 = and i64 %15, 7
  %17 = icmp eq i64 %16, 0
  br i1 %17, label %18, label %20

; <label>:18:                                     ; preds = %14
  %19 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.11, i32 0, i32 0))
  br label %186

; <label>:20:                                     ; preds = %14
  %21 = load i64, i64* %2, align 8
  %22 = and i64 %21, 7
  %23 = icmp eq i64 %22, 1
  br i1 %23, label %24, label %39

; <label>:24:                                     ; preds = %20
  %25 = load i64, i64* %2, align 8
  %26 = and i64 %25, -8
  %27 = inttoptr i64 %26 to i64*
  store i64* %27, i64** %3, align 8
  %28 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.12, i32 0, i32 0))
  %29 = load i64*, i64** %3, align 8
  %30 = getelementptr inbounds i64, i64* %29, i64 0
  %31 = load i64, i64* %30, align 8
  %32 = call i64 @prim_print_aux(i64 %31)
  %33 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.13, i32 0, i32 0))
  %34 = load i64*, i64** %3, align 8
  %35 = getelementptr inbounds i64, i64* %34, i64 1
  %36 = load i64, i64* %35, align 8
  %37 = call i64 @prim_print_aux(i64 %36)
  %38 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.14, i32 0, i32 0))
  br label %185

; <label>:39:                                     ; preds = %20
  %40 = load i64, i64* %2, align 8
  %41 = and i64 %40, 7
  %42 = icmp eq i64 %41, 2
  br i1 %42, label %43, label %48

; <label>:43:                                     ; preds = %39
  %44 = load i64, i64* %2, align 8
  %45 = lshr i64 %44, 32
  %46 = trunc i64 %45 to i32
  %47 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.15, i32 0, i32 0), i32 %46)
  br label %184

; <label>:48:                                     ; preds = %39
  %49 = load i64, i64* %2, align 8
  %50 = and i64 %49, 7
  %51 = icmp eq i64 %50, 3
  br i1 %51, label %52, label %57

; <label>:52:                                     ; preds = %48
  %53 = load i64, i64* %2, align 8
  %54 = and i64 %53, -8
  %55 = inttoptr i64 %54 to i8*
  %56 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.16, i32 0, i32 0), i8* %55)
  br label %183

; <label>:57:                                     ; preds = %48
  %58 = load i64, i64* %2, align 8
  %59 = and i64 %58, 7
  %60 = icmp eq i64 %59, 4
  br i1 %60, label %61, label %66

; <label>:61:                                     ; preds = %57
  %62 = load i64, i64* %2, align 8
  %63 = and i64 %62, -8
  %64 = inttoptr i64 %63 to i8*
  %65 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0), i8* %64)
  br label %182

; <label>:66:                                     ; preds = %57
  %67 = load i64, i64* %2, align 8
  %68 = and i64 %67, 7
  %69 = icmp eq i64 %68, 6
  br i1 %69, label %70, label %107

; <label>:70:                                     ; preds = %66
  %71 = load i64, i64* %2, align 8
  %72 = and i64 %71, -8
  %73 = inttoptr i64 %72 to i64*
  %74 = getelementptr inbounds i64, i64* %73, i64 0
  %75 = load i64, i64* %74, align 8
  %76 = and i64 %75, 7
  %77 = icmp eq i64 1, %76
  br i1 %77, label %78, label %107

; <label>:78:                                     ; preds = %70
  %79 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.17, i32 0, i32 0))
  %80 = load i64, i64* %2, align 8
  %81 = and i64 %80, -8
  %82 = inttoptr i64 %81 to i64*
  store i64* %82, i64** %4, align 8
  %83 = load i64*, i64** %4, align 8
  %84 = getelementptr inbounds i64, i64* %83, i64 0
  %85 = load i64, i64* %84, align 8
  %86 = lshr i64 %85, 3
  store i64 %86, i64* %5, align 8
  %87 = load i64*, i64** %4, align 8
  %88 = getelementptr inbounds i64, i64* %87, i64 1
  %89 = load i64, i64* %88, align 8
  %90 = call i64 @prim_print_aux(i64 %89)
  store i64 2, i64* %6, align 8
  br label %91

; <label>:91:                                     ; preds = %102, %78
  %92 = load i64, i64* %6, align 8
  %93 = load i64, i64* %5, align 8
  %94 = icmp ule i64 %92, %93
  br i1 %94, label %95, label %105

; <label>:95:                                     ; preds = %91
  %96 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.18, i32 0, i32 0))
  %97 = load i64*, i64** %4, align 8
  %98 = load i64, i64* %6, align 8
  %99 = getelementptr inbounds i64, i64* %97, i64 %98
  %100 = load i64, i64* %99, align 8
  %101 = call i64 @prim_print_aux(i64 %100)
  br label %102

; <label>:102:                                    ; preds = %95
  %103 = load i64, i64* %6, align 8
  %104 = add i64 %103, 1
  store i64 %104, i64* %6, align 8
  br label %91

; <label>:105:                                    ; preds = %91
  %106 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.14, i32 0, i32 0))
  br label %181

; <label>:107:                                    ; preds = %70, %66
  %108 = load i64, i64* %2, align 8
  %109 = and i64 %108, 7
  %110 = icmp eq i64 %109, 6
  br i1 %110, label %111, label %177

; <label>:111:                                    ; preds = %107
  %112 = load i64, i64* %2, align 8
  %113 = and i64 %112, -8
  %114 = inttoptr i64 %113 to i64*
  %115 = getelementptr inbounds i64, i64* %114, i64 0
  %116 = load i64, i64* %115, align 8
  %117 = icmp eq i64 2, %116
  br i1 %117, label %118, label %177

; <label>:118:                                    ; preds = %111
  %119 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.19, i32 0, i32 0))
  %120 = load i64, i64* %2, align 8
  %121 = and i64 %120, -8
  %122 = inttoptr i64 %121 to i64*
  store i64* %122, i64** %7, align 8
  %123 = load i64*, i64** %7, align 8
  %124 = getelementptr inbounds i64, i64* %123, i64 1
  %125 = load i64, i64* %124, align 8
  %126 = and i64 %125, -8
  %127 = inttoptr i64 %126 to i64*
  %128 = bitcast i64* %127 to %struct.my_hash*
  store %struct.my_hash* %128, %struct.my_hash** %8, align 8
  %129 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  store %struct.my_hash* %129, %struct.my_hash** %9, align 8
  %130 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %131 = icmp ne %struct.my_hash* %130, null
  br i1 %131, label %132, label %151

; <label>:132:                                    ; preds = %118
  %133 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %134 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %133, i32 0, i32 0
  %135 = load i64, i64* %134, align 8
  %136 = and i64 %135, -8
  %137 = lshr i64 %136, 32
  %138 = trunc i64 %137 to i32
  %139 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %140 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %139, i32 0, i32 1
  %141 = load i64, i64* %140, align 8
  %142 = and i64 %141, -8
  %143 = lshr i64 %142, 32
  %144 = trunc i64 %143 to i32
  %145 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.20, i32 0, i32 0), i32 %138, i32 %144)
  %146 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %147 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %146, i32 0, i32 2
  %148 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %147, i32 0, i32 2
  %149 = load i8*, i8** %148, align 8
  %150 = bitcast i8* %149 to %struct.my_hash*
  store %struct.my_hash* %150, %struct.my_hash** %9, align 8
  br label %151

; <label>:151:                                    ; preds = %132, %118
  br label %152

; <label>:152:                                    ; preds = %169, %151
  %153 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %154 = icmp ne %struct.my_hash* %153, null
  br i1 %154, label %155, label %175

; <label>:155:                                    ; preds = %152
  %156 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %157 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %156, i32 0, i32 0
  %158 = load i64, i64* %157, align 8
  %159 = and i64 %158, -8
  %160 = lshr i64 %159, 32
  %161 = trunc i64 %160 to i32
  %162 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %163 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %162, i32 0, i32 1
  %164 = load i64, i64* %163, align 8
  %165 = and i64 %164, -8
  %166 = lshr i64 %165, 32
  %167 = trunc i64 %166 to i32
  %168 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.21, i32 0, i32 0), i32 %161, i32 %167)
  br label %169

; <label>:169:                                    ; preds = %155
  %170 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %171 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %170, i32 0, i32 2
  %172 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %171, i32 0, i32 2
  %173 = load i8*, i8** %172, align 8
  %174 = bitcast i8* %173 to %struct.my_hash*
  store %struct.my_hash* %174, %struct.my_hash** %9, align 8
  br label %152

; <label>:175:                                    ; preds = %152
  %176 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.14, i32 0, i32 0))
  br label %180

; <label>:177:                                    ; preds = %111, %107
  %178 = load i64, i64* %2, align 8
  %179 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.22, i32 0, i32 0), i64 %178)
  br label %180

; <label>:180:                                    ; preds = %177, %175
  br label %181

; <label>:181:                                    ; preds = %180, %105
  br label %182

; <label>:182:                                    ; preds = %181, %61
  br label %183

; <label>:183:                                    ; preds = %182, %52
  br label %184

; <label>:184:                                    ; preds = %183, %43
  br label %185

; <label>:185:                                    ; preds = %184, %24
  br label %186

; <label>:186:                                    ; preds = %185, %18
  br label %187

; <label>:187:                                    ; preds = %186, %12
  ret i64 39
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_print(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64*, align 8
  %4 = alloca i64*, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64, align 8
  %7 = alloca i64*, align 8
  %8 = alloca %struct.my_hash*, align 8
  %9 = alloca %struct.my_hash*, align 8
  store i64 %0, i64* %2, align 8
  %10 = load i64, i64* %2, align 8
  %11 = icmp eq i64 %10, 0
  br i1 %11, label %12, label %14

; <label>:12:                                     ; preds = %1
  %13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.23, i32 0, i32 0))
  br label %187

; <label>:14:                                     ; preds = %1
  %15 = load i64, i64* %2, align 8
  %16 = and i64 %15, 7
  %17 = icmp eq i64 %16, 0
  br i1 %17, label %18, label %20

; <label>:18:                                     ; preds = %14
  %19 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.11, i32 0, i32 0))
  br label %186

; <label>:20:                                     ; preds = %14
  %21 = load i64, i64* %2, align 8
  %22 = and i64 %21, 7
  %23 = icmp eq i64 %22, 1
  br i1 %23, label %24, label %39

; <label>:24:                                     ; preds = %20
  %25 = load i64, i64* %2, align 8
  %26 = and i64 %25, -8
  %27 = inttoptr i64 %26 to i64*
  store i64* %27, i64** %3, align 8
  %28 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.24, i32 0, i32 0))
  %29 = load i64*, i64** %3, align 8
  %30 = getelementptr inbounds i64, i64* %29, i64 0
  %31 = load i64, i64* %30, align 8
  %32 = call i64 @prim_print_aux(i64 %31)
  %33 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.13, i32 0, i32 0))
  %34 = load i64*, i64** %3, align 8
  %35 = getelementptr inbounds i64, i64* %34, i64 1
  %36 = load i64, i64* %35, align 8
  %37 = call i64 @prim_print_aux(i64 %36)
  %38 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.14, i32 0, i32 0))
  br label %185

; <label>:39:                                     ; preds = %20
  %40 = load i64, i64* %2, align 8
  %41 = and i64 %40, 7
  %42 = icmp eq i64 %41, 2
  br i1 %42, label %43, label %48

; <label>:43:                                     ; preds = %39
  %44 = load i64, i64* %2, align 8
  %45 = lshr i64 %44, 32
  %46 = trunc i64 %45 to i32
  %47 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.15, i32 0, i32 0), i32 %46)
  br label %184

; <label>:48:                                     ; preds = %39
  %49 = load i64, i64* %2, align 8
  %50 = and i64 %49, 7
  %51 = icmp eq i64 %50, 3
  br i1 %51, label %52, label %57

; <label>:52:                                     ; preds = %48
  %53 = load i64, i64* %2, align 8
  %54 = and i64 %53, -8
  %55 = inttoptr i64 %54 to i8*
  %56 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.16, i32 0, i32 0), i8* %55)
  br label %183

; <label>:57:                                     ; preds = %48
  %58 = load i64, i64* %2, align 8
  %59 = and i64 %58, 7
  %60 = icmp eq i64 %59, 4
  br i1 %60, label %61, label %66

; <label>:61:                                     ; preds = %57
  %62 = load i64, i64* %2, align 8
  %63 = and i64 %62, -8
  %64 = inttoptr i64 %63 to i8*
  %65 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.25, i32 0, i32 0), i8* %64)
  br label %182

; <label>:66:                                     ; preds = %57
  %67 = load i64, i64* %2, align 8
  %68 = and i64 %67, 7
  %69 = icmp eq i64 %68, 6
  br i1 %69, label %70, label %107

; <label>:70:                                     ; preds = %66
  %71 = load i64, i64* %2, align 8
  %72 = and i64 %71, -8
  %73 = inttoptr i64 %72 to i64*
  %74 = getelementptr inbounds i64, i64* %73, i64 0
  %75 = load i64, i64* %74, align 8
  %76 = and i64 %75, 7
  %77 = icmp eq i64 1, %76
  br i1 %77, label %78, label %107

; <label>:78:                                     ; preds = %70
  %79 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.17, i32 0, i32 0))
  %80 = load i64, i64* %2, align 8
  %81 = and i64 %80, -8
  %82 = inttoptr i64 %81 to i64*
  store i64* %82, i64** %4, align 8
  %83 = load i64*, i64** %4, align 8
  %84 = getelementptr inbounds i64, i64* %83, i64 0
  %85 = load i64, i64* %84, align 8
  %86 = lshr i64 %85, 3
  store i64 %86, i64* %5, align 8
  %87 = load i64*, i64** %4, align 8
  %88 = getelementptr inbounds i64, i64* %87, i64 1
  %89 = load i64, i64* %88, align 8
  %90 = call i64 @prim_print(i64 %89)
  store i64 2, i64* %6, align 8
  br label %91

; <label>:91:                                     ; preds = %102, %78
  %92 = load i64, i64* %6, align 8
  %93 = load i64, i64* %5, align 8
  %94 = icmp ule i64 %92, %93
  br i1 %94, label %95, label %105

; <label>:95:                                     ; preds = %91
  %96 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.18, i32 0, i32 0))
  %97 = load i64*, i64** %4, align 8
  %98 = load i64, i64* %6, align 8
  %99 = getelementptr inbounds i64, i64* %97, i64 %98
  %100 = load i64, i64* %99, align 8
  %101 = call i64 @prim_print(i64 %100)
  br label %102

; <label>:102:                                    ; preds = %95
  %103 = load i64, i64* %6, align 8
  %104 = add i64 %103, 1
  store i64 %104, i64* %6, align 8
  br label %91

; <label>:105:                                    ; preds = %91
  %106 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.14, i32 0, i32 0))
  br label %181

; <label>:107:                                    ; preds = %70, %66
  %108 = load i64, i64* %2, align 8
  %109 = and i64 %108, 7
  %110 = icmp eq i64 %109, 6
  br i1 %110, label %111, label %177

; <label>:111:                                    ; preds = %107
  %112 = load i64, i64* %2, align 8
  %113 = and i64 %112, -8
  %114 = inttoptr i64 %113 to i64*
  %115 = getelementptr inbounds i64, i64* %114, i64 0
  %116 = load i64, i64* %115, align 8
  %117 = icmp eq i64 2, %116
  br i1 %117, label %118, label %177

; <label>:118:                                    ; preds = %111
  %119 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.19, i32 0, i32 0))
  %120 = load i64, i64* %2, align 8
  %121 = and i64 %120, -8
  %122 = inttoptr i64 %121 to i64*
  store i64* %122, i64** %7, align 8
  %123 = load i64*, i64** %7, align 8
  %124 = getelementptr inbounds i64, i64* %123, i64 1
  %125 = load i64, i64* %124, align 8
  %126 = and i64 %125, -8
  %127 = inttoptr i64 %126 to i64*
  %128 = bitcast i64* %127 to %struct.my_hash*
  store %struct.my_hash* %128, %struct.my_hash** %8, align 8
  %129 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  store %struct.my_hash* %129, %struct.my_hash** %9, align 8
  %130 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %131 = icmp ne %struct.my_hash* %130, null
  br i1 %131, label %132, label %151

; <label>:132:                                    ; preds = %118
  %133 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %134 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %133, i32 0, i32 0
  %135 = load i64, i64* %134, align 8
  %136 = and i64 %135, -8
  %137 = lshr i64 %136, 32
  %138 = trunc i64 %137 to i32
  %139 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %140 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %139, i32 0, i32 1
  %141 = load i64, i64* %140, align 8
  %142 = and i64 %141, -8
  %143 = lshr i64 %142, 32
  %144 = trunc i64 %143 to i32
  %145 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.20, i32 0, i32 0), i32 %138, i32 %144)
  %146 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %147 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %146, i32 0, i32 2
  %148 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %147, i32 0, i32 2
  %149 = load i8*, i8** %148, align 8
  %150 = bitcast i8* %149 to %struct.my_hash*
  store %struct.my_hash* %150, %struct.my_hash** %9, align 8
  br label %151

; <label>:151:                                    ; preds = %132, %118
  br label %152

; <label>:152:                                    ; preds = %169, %151
  %153 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %154 = icmp ne %struct.my_hash* %153, null
  br i1 %154, label %155, label %175

; <label>:155:                                    ; preds = %152
  %156 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %157 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %156, i32 0, i32 0
  %158 = load i64, i64* %157, align 8
  %159 = and i64 %158, -8
  %160 = lshr i64 %159, 32
  %161 = trunc i64 %160 to i32
  %162 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %163 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %162, i32 0, i32 1
  %164 = load i64, i64* %163, align 8
  %165 = and i64 %164, -8
  %166 = lshr i64 %165, 32
  %167 = trunc i64 %166 to i32
  %168 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.21, i32 0, i32 0), i32 %161, i32 %167)
  br label %169

; <label>:169:                                    ; preds = %155
  %170 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %171 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %170, i32 0, i32 2
  %172 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %171, i32 0, i32 2
  %173 = load i8*, i8** %172, align 8
  %174 = bitcast i8* %173 to %struct.my_hash*
  store %struct.my_hash* %174, %struct.my_hash** %9, align 8
  br label %152

; <label>:175:                                    ; preds = %152
  %176 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.14, i32 0, i32 0))
  br label %180

; <label>:177:                                    ; preds = %111, %107
  %178 = load i64, i64* %2, align 8
  %179 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.26, i32 0, i32 0), i64 %178)
  br label %180

; <label>:180:                                    ; preds = %177, %175
  br label %181

; <label>:181:                                    ; preds = %180, %105
  br label %182

; <label>:182:                                    ; preds = %181, %61
  br label %183

; <label>:183:                                    ; preds = %182, %52
  br label %184

; <label>:184:                                    ; preds = %183, %43
  br label %185

; <label>:185:                                    ; preds = %184, %24
  br label %186

; <label>:186:                                    ; preds = %185, %18
  br label %187

; <label>:187:                                    ; preds = %186, %12
  ret i64 39
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_print(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_print(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_halt(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = call i64 @prim_print(i64 %4)
  %6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.2, i32 0, i32 0))
  call void @exit(i32 0) #7
  unreachable
                                                  ; No predecessors!
  %8 = load i64, i64* %2, align 8
  ret i64 %8
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_vector(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64*, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64*, align 8
  %6 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %7 = load i64, i64* @MEMCOUNT, align 8
  %8 = add i64 %7, 4096
  store i64 %8, i64* @MEMCOUNT, align 8
  %9 = load i64, i64* @MEMCOUNT, align 8
  %10 = load i64, i64* @MEMLIMIT, align 8
  %11 = icmp uge i64 %9, %10
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.3, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %1
  %14 = call i8* @malloc(i64 4096) #8
  %15 = bitcast i8* %14 to i64*
  store i64* %15, i64** %3, align 8
  store i64 0, i64* %4, align 8
  br label %16

; <label>:16:                                     ; preds = %25, %13
  %17 = load i64, i64* %2, align 8
  %18 = and i64 %17, 7
  %19 = icmp eq i64 %18, 1
  br i1 %19, label %20, label %23

; <label>:20:                                     ; preds = %16
  %21 = load i64, i64* %4, align 8
  %22 = icmp ult i64 %21, 512
  br label %23

; <label>:23:                                     ; preds = %20, %16
  %24 = phi i1 [ false, %16 ], [ %22, %20 ]
  br i1 %24, label %25, label %32

; <label>:25:                                     ; preds = %23
  %26 = load i64, i64* %2, align 8
  %27 = call i64 @expect_cons(i64 %26, i64* %2)
  %28 = load i64*, i64** %3, align 8
  %29 = load i64, i64* %4, align 8
  %30 = add i64 %29, 1
  store i64 %30, i64* %4, align 8
  %31 = getelementptr inbounds i64, i64* %28, i64 %29
  store i64 %27, i64* %31, align 8
  br label %16

; <label>:32:                                     ; preds = %23
  %33 = load i64, i64* %4, align 8
  %34 = add i64 %33, 1
  %35 = mul i64 %34, 8
  %36 = call i64* @alloc(i64 %35)
  store i64* %36, i64** %5, align 8
  %37 = load i64, i64* %4, align 8
  %38 = shl i64 %37, 3
  %39 = or i64 %38, 1
  %40 = load i64*, i64** %5, align 8
  %41 = getelementptr inbounds i64, i64* %40, i64 0
  store i64 %39, i64* %41, align 8
  store i64 0, i64* %6, align 8
  br label %42

; <label>:42:                                     ; preds = %55, %32
  %43 = load i64, i64* %6, align 8
  %44 = load i64, i64* %4, align 8
  %45 = icmp ult i64 %43, %44
  br i1 %45, label %46, label %58

; <label>:46:                                     ; preds = %42
  %47 = load i64*, i64** %3, align 8
  %48 = load i64, i64* %6, align 8
  %49 = getelementptr inbounds i64, i64* %47, i64 %48
  %50 = load i64, i64* %49, align 8
  %51 = load i64*, i64** %5, align 8
  %52 = load i64, i64* %6, align 8
  %53 = add i64 %52, 1
  %54 = getelementptr inbounds i64, i64* %51, i64 %53
  store i64 %50, i64* %54, align 8
  br label %55

; <label>:55:                                     ; preds = %46
  %56 = load i64, i64* %6, align 8
  %57 = add i64 %56, 1
  store i64 %57, i64* %6, align 8
  br label %42

; <label>:58:                                     ; preds = %42
  %59 = load i64*, i64** %3, align 8
  %60 = icmp eq i64* %59, null
  br i1 %60, label %63, label %61

; <label>:61:                                     ; preds = %58
  %62 = bitcast i64* %59 to i8*
  call void @_ZdaPv(i8* %62) #9
  br label %63

; <label>:63:                                     ; preds = %61, %58
  %64 = load i64, i64* @MEMCOUNT, align 8
  %65 = sub i64 %64, 4096
  store i64 %65, i64* @MEMCOUNT, align 8
  %66 = load i64*, i64** %5, align 8
  %67 = ptrtoint i64* %66 to i64
  %68 = or i64 %67, 6
  ret i64 %68
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdaPv(i8*) #5

; Function Attrs: noinline ssp uwtable
define i64 @prim_make_45vector(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64*, align 8
  %7 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  store i64 %1, i64* %4, align 8
  %8 = load i64, i64* %3, align 8
  %9 = and i64 %8, 7
  %10 = icmp ne i64 %9, 2
  br i1 %10, label %11, label %12

; <label>:11:                                     ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.27, i32 0, i32 0))
  br label %12

; <label>:12:                                     ; preds = %11, %2
  %13 = load i64, i64* %3, align 8
  %14 = and i64 %13, -8
  %15 = lshr i64 %14, 32
  %16 = trunc i64 %15 to i32
  %17 = sext i32 %16 to i64
  store i64 %17, i64* %5, align 8
  %18 = load i64, i64* %5, align 8
  %19 = add i64 %18, 1
  %20 = mul i64 %19, 8
  %21 = call i64* @alloc(i64 %20)
  store i64* %21, i64** %6, align 8
  %22 = load i64, i64* %5, align 8
  %23 = shl i64 %22, 3
  %24 = or i64 %23, 1
  %25 = load i64*, i64** %6, align 8
  %26 = getelementptr inbounds i64, i64* %25, i64 0
  store i64 %24, i64* %26, align 8
  store i64 1, i64* %7, align 8
  br label %27

; <label>:27:                                     ; preds = %36, %12
  %28 = load i64, i64* %7, align 8
  %29 = load i64, i64* %5, align 8
  %30 = icmp ule i64 %28, %29
  br i1 %30, label %31, label %39

; <label>:31:                                     ; preds = %27
  %32 = load i64, i64* %4, align 8
  %33 = load i64*, i64** %6, align 8
  %34 = load i64, i64* %7, align 8
  %35 = getelementptr inbounds i64, i64* %33, i64 %34
  store i64 %32, i64* %35, align 8
  br label %36

; <label>:36:                                     ; preds = %31
  %37 = load i64, i64* %7, align 8
  %38 = add i64 %37, 1
  store i64 %38, i64* %7, align 8
  br label %27

; <label>:39:                                     ; preds = %27
  %40 = load i64*, i64** %6, align 8
  %41 = ptrtoint i64* %40 to i64
  %42 = or i64 %41, 6
  ret i64 %42
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_make_45vector(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %6 = load i64, i64* %2, align 8
  %7 = call i64 @expect_cons(i64 %6, i64* %3)
  store i64 %7, i64* %4, align 8
  %8 = load i64, i64* %3, align 8
  %9 = call i64 @expect_cons(i64 %8, i64* %3)
  store i64 %9, i64* %5, align 8
  %10 = load i64, i64* %3, align 8
  %11 = icmp ne i64 %10, 0
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %1
  %14 = load i64, i64* %4, align 8
  %15 = load i64, i64* %5, align 8
  %16 = call i64 @prim_make_45vector(i64 %14, i64 %15)
  ret i64 %16
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_vector_45ref(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  store i64 %1, i64* %4, align 8
  %5 = load i64, i64* %4, align 8
  %6 = and i64 %5, 7
  %7 = icmp ne i64 %6, 2
  br i1 %7, label %8, label %9

; <label>:8:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.29, i32 0, i32 0))
  br label %9

; <label>:9:                                      ; preds = %8, %2
  %10 = load i64, i64* %3, align 8
  %11 = and i64 %10, 7
  %12 = icmp ne i64 %11, 6
  br i1 %12, label %13, label %14

; <label>:13:                                     ; preds = %9
  call void @fatal_err(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.30, i32 0, i32 0))
  br label %14

; <label>:14:                                     ; preds = %13, %9
  %15 = load i64, i64* %3, align 8
  %16 = and i64 %15, -8
  %17 = inttoptr i64 %16 to i64*
  %18 = getelementptr inbounds i64, i64* %17, i64 0
  %19 = load i64, i64* %18, align 8
  %20 = and i64 %19, 7
  %21 = icmp ne i64 %20, 1
  br i1 %21, label %22, label %23

; <label>:22:                                     ; preds = %14
  call void @fatal_err(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.31, i32 0, i32 0))
  br label %23

; <label>:23:                                     ; preds = %22, %14
  %24 = load i64, i64* %3, align 8
  %25 = and i64 %24, -8
  %26 = inttoptr i64 %25 to i64*
  %27 = getelementptr inbounds i64, i64* %26, i64 0
  %28 = load i64, i64* %27, align 8
  %29 = lshr i64 %28, 3
  %30 = load i64, i64* %4, align 8
  %31 = and i64 %30, -8
  %32 = lshr i64 %31, 32
  %33 = trunc i64 %32 to i32
  %34 = sext i32 %33 to i64
  %35 = icmp ult i64 %29, %34
  br i1 %35, label %36, label %45

; <label>:36:                                     ; preds = %23
  %37 = load i64, i64* %3, align 8
  %38 = and i64 %37, -8
  %39 = inttoptr i64 %38 to i64*
  %40 = getelementptr inbounds i64, i64* %39, i64 0
  %41 = load i64, i64* %40, align 8
  %42 = lshr i64 %41, 3
  %43 = icmp ugt i64 %42, 0
  br i1 %43, label %44, label %45

; <label>:44:                                     ; preds = %36
  call void @fatal_err(i8* getelementptr inbounds ([72 x i8], [72 x i8]* @.str.32, i32 0, i32 0))
  br label %45

; <label>:45:                                     ; preds = %44, %36, %23
  %46 = load i64, i64* %3, align 8
  %47 = and i64 %46, -8
  %48 = inttoptr i64 %47 to i64*
  %49 = load i64, i64* %4, align 8
  %50 = and i64 %49, -8
  %51 = lshr i64 %50, 32
  %52 = trunc i64 %51 to i32
  %53 = add nsw i32 1, %52
  %54 = sext i32 %53 to i64
  %55 = getelementptr inbounds i64, i64* %48, i64 %54
  %56 = load i64, i64* %55, align 8
  ret i64 %56
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_vector_45ref(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %6 = load i64, i64* %2, align 8
  %7 = call i64 @expect_cons(i64 %6, i64* %3)
  store i64 %7, i64* %4, align 8
  %8 = load i64, i64* %3, align 8
  %9 = call i64 @expect_cons(i64 %8, i64* %3)
  store i64 %9, i64* %5, align 8
  %10 = load i64, i64* %3, align 8
  %11 = icmp ne i64 %10, 0
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %1
  %14 = load i64, i64* %4, align 8
  %15 = load i64, i64* %5, align 8
  %16 = call i64 @prim_vector_45ref(i64 %14, i64 %15)
  ret i64 %16
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_vector_45set_33(i64, i64, i64) #0 {
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64, align 8
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  store i64 %2, i64* %6, align 8
  %7 = load i64, i64* %5, align 8
  %8 = and i64 %7, 7
  %9 = icmp ne i64 %8, 2
  br i1 %9, label %10, label %11

; <label>:10:                                     ; preds = %3
  call void @fatal_err(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.33, i32 0, i32 0))
  br label %11

; <label>:11:                                     ; preds = %10, %3
  %12 = load i64, i64* %4, align 8
  %13 = and i64 %12, 7
  %14 = icmp ne i64 %13, 6
  br i1 %14, label %15, label %16

; <label>:15:                                     ; preds = %11
  call void @fatal_err(i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.34, i32 0, i32 0))
  br label %16

; <label>:16:                                     ; preds = %15, %11
  %17 = load i64, i64* %4, align 8
  %18 = and i64 %17, -8
  %19 = inttoptr i64 %18 to i64*
  %20 = getelementptr inbounds i64, i64* %19, i64 0
  %21 = load i64, i64* %20, align 8
  %22 = and i64 %21, 7
  %23 = icmp ne i64 %22, 1
  br i1 %23, label %24, label %25

; <label>:24:                                     ; preds = %16
  call void @fatal_err(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.35, i32 0, i32 0))
  br label %25

; <label>:25:                                     ; preds = %24, %16
  %26 = load i64, i64* %4, align 8
  %27 = and i64 %26, -8
  %28 = inttoptr i64 %27 to i64*
  %29 = getelementptr inbounds i64, i64* %28, i64 0
  %30 = load i64, i64* %29, align 8
  %31 = lshr i64 %30, 3
  %32 = load i64, i64* %5, align 8
  %33 = and i64 %32, -8
  %34 = lshr i64 %33, 32
  %35 = trunc i64 %34 to i32
  %36 = sext i32 %35 to i64
  %37 = icmp ult i64 %31, %36
  br i1 %37, label %38, label %47

; <label>:38:                                     ; preds = %25
  %39 = load i64, i64* %6, align 8
  %40 = and i64 %39, -8
  %41 = inttoptr i64 %40 to i64*
  %42 = getelementptr inbounds i64, i64* %41, i64 0
  %43 = load i64, i64* %42, align 8
  %44 = lshr i64 %43, 3
  %45 = icmp ugt i64 %44, 0
  br i1 %45, label %46, label %47

; <label>:46:                                     ; preds = %38
  call void @fatal_err(i8* getelementptr inbounds ([72 x i8], [72 x i8]* @.str.36, i32 0, i32 0))
  br label %47

; <label>:47:                                     ; preds = %46, %38, %25
  %48 = load i64, i64* %6, align 8
  %49 = load i64, i64* %4, align 8
  %50 = and i64 %49, -8
  %51 = inttoptr i64 %50 to i64*
  %52 = load i64, i64* %5, align 8
  %53 = and i64 %52, -8
  %54 = lshr i64 %53, 32
  %55 = trunc i64 %54 to i32
  %56 = add nsw i32 1, %55
  %57 = sext i32 %56 to i64
  %58 = getelementptr inbounds i64, i64* %51, i64 %57
  store i64 %48, i64* %58, align 8
  ret i64 39
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_vector_45set_33(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %7 = load i64, i64* %2, align 8
  %8 = call i64 @expect_cons(i64 %7, i64* %3)
  store i64 %8, i64* %4, align 8
  %9 = load i64, i64* %3, align 8
  %10 = call i64 @expect_cons(i64 %9, i64* %3)
  store i64 %10, i64* %5, align 8
  %11 = load i64, i64* %3, align 8
  %12 = call i64 @expect_cons(i64 %11, i64* %3)
  store i64 %12, i64* %6, align 8
  %13 = load i64, i64* %3, align 8
  %14 = icmp ne i64 %13, 0
  br i1 %14, label %15, label %16

; <label>:15:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %16

; <label>:16:                                     ; preds = %15, %1
  %17 = load i64, i64* %4, align 8
  %18 = load i64, i64* %5, align 8
  %19 = load i64, i64* %6, align 8
  %20 = call i64 @prim_vector_45set_33(i64 %17, i64 %18, i64 %19)
  ret i64 %20
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_void() #4 {
  ret i64 39
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_eq_63(i64, i64) #4 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  %6 = load i64, i64* %4, align 8
  %7 = load i64, i64* %5, align 8
  %8 = icmp eq i64 %6, %7
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %2
  store i64 31, i64* %3, align 8
  br label %11

; <label>:10:                                     ; preds = %2
  store i64 15, i64* %3, align 8
  br label %11

; <label>:11:                                     ; preds = %10, %9
  %12 = load i64, i64* %3, align 8
  ret i64 %12
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_eq_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %6 = load i64, i64* %2, align 8
  %7 = call i64 @expect_cons(i64 %6, i64* %3)
  store i64 %7, i64* %4, align 8
  %8 = load i64, i64* %3, align 8
  %9 = call i64 @expect_cons(i64 %8, i64* %3)
  store i64 %9, i64* %5, align 8
  %10 = load i64, i64* %3, align 8
  %11 = icmp ne i64 %10, 0
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %1
  %14 = load i64, i64* %4, align 8
  %15 = load i64, i64* %5, align 8
  %16 = call i64 @prim_eq_63(i64 %14, i64 %15)
  ret i64 %16
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_eqv_63(i64, i64) #4 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  %6 = load i64, i64* %4, align 8
  %7 = load i64, i64* %5, align 8
  %8 = icmp eq i64 %6, %7
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %2
  store i64 31, i64* %3, align 8
  br label %11

; <label>:10:                                     ; preds = %2
  store i64 15, i64* %3, align 8
  br label %11

; <label>:11:                                     ; preds = %10, %9
  %12 = load i64, i64* %3, align 8
  ret i64 %12
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_eqv_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %6 = load i64, i64* %2, align 8
  %7 = call i64 @expect_cons(i64 %6, i64* %3)
  store i64 %7, i64* %4, align 8
  %8 = load i64, i64* %3, align 8
  %9 = call i64 @expect_cons(i64 %8, i64* %3)
  store i64 %9, i64* %5, align 8
  %10 = load i64, i64* %3, align 8
  %11 = icmp ne i64 %10, 0
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %1
  %14 = load i64, i64* %4, align 8
  %15 = load i64, i64* %5, align 8
  %16 = call i64 @prim_eqv_63(i64 %14, i64 %15)
  ret i64 %16
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_number_63(i64) #4 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = and i64 %4, 7
  %6 = icmp eq i64 %5, 2
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  store i64 31, i64* %2, align 8
  br label %9

; <label>:8:                                      ; preds = %1
  store i64 15, i64* %2, align 8
  br label %9

; <label>:9:                                      ; preds = %8, %7
  %10 = load i64, i64* %2, align 8
  ret i64 %10
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_number_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_number_63(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_integer_63(i64) #4 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = and i64 %4, 7
  %6 = icmp eq i64 %5, 2
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  store i64 31, i64* %2, align 8
  br label %9

; <label>:8:                                      ; preds = %1
  store i64 15, i64* %2, align 8
  br label %9

; <label>:9:                                      ; preds = %8, %7
  %10 = load i64, i64* %2, align 8
  ret i64 %10
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_integer_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_integer_63(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_void_63(i64) #4 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = icmp eq i64 %4, 39
  br i1 %5, label %6, label %7

; <label>:6:                                      ; preds = %1
  store i64 31, i64* %2, align 8
  br label %8

; <label>:7:                                      ; preds = %1
  store i64 15, i64* %2, align 8
  br label %8

; <label>:8:                                      ; preds = %7, %6
  %9 = load i64, i64* %2, align 8
  ret i64 %9
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_void_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_void_63(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_procedure_63(i64) #4 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = and i64 %4, 7
  %6 = icmp eq i64 %5, 0
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  store i64 31, i64* %2, align 8
  br label %9

; <label>:8:                                      ; preds = %1
  store i64 15, i64* %2, align 8
  br label %9

; <label>:9:                                      ; preds = %8, %7
  %10 = load i64, i64* %2, align 8
  ret i64 %10
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_procedure_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_procedure_63(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_null_63(i64) #4 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = icmp eq i64 %4, 0
  br i1 %5, label %6, label %7

; <label>:6:                                      ; preds = %1
  store i64 31, i64* %2, align 8
  br label %8

; <label>:7:                                      ; preds = %1
  store i64 15, i64* %2, align 8
  br label %8

; <label>:8:                                      ; preds = %7, %6
  %9 = load i64, i64* %2, align 8
  ret i64 %9
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_null_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_null_63(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_cons_63(i64) #4 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = and i64 %4, 7
  %6 = icmp eq i64 %5, 1
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  store i64 31, i64* %2, align 8
  br label %9

; <label>:8:                                      ; preds = %1
  store i64 15, i64* %2, align 8
  br label %9

; <label>:9:                                      ; preds = %8, %7
  %10 = load i64, i64* %2, align 8
  ret i64 %10
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_cons_63(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_cons_63(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_cons(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64*, align 8
  store i64 %0, i64* %3, align 8
  store i64 %1, i64* %4, align 8
  %6 = call i64* @alloc(i64 16)
  store i64* %6, i64** %5, align 8
  %7 = load i64, i64* %3, align 8
  %8 = load i64*, i64** %5, align 8
  %9 = getelementptr inbounds i64, i64* %8, i64 0
  store i64 %7, i64* %9, align 8
  %10 = load i64, i64* %4, align 8
  %11 = load i64*, i64** %5, align 8
  %12 = getelementptr inbounds i64, i64* %11, i64 1
  store i64 %10, i64* %12, align 8
  %13 = load i64*, i64** %5, align 8
  %14 = ptrtoint i64* %13 to i64
  %15 = or i64 %14, 1
  ret i64 %15
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_cons(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %6 = load i64, i64* %2, align 8
  %7 = call i64 @expect_cons(i64 %6, i64* %3)
  store i64 %7, i64* %4, align 8
  %8 = load i64, i64* %3, align 8
  %9 = call i64 @expect_cons(i64 %8, i64* %3)
  store i64 %9, i64* %5, align 8
  %10 = load i64, i64* %3, align 8
  %11 = icmp ne i64 %10, 0
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %1
  %14 = load i64, i64* %4, align 8
  %15 = load i64, i64* %5, align 8
  %16 = call i64 @prim_cons(i64 %14, i64 %15)
  ret i64 %16
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_car(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %5 = load i64, i64* %2, align 8
  %6 = call i64 @expect_cons(i64 %5, i64* %3)
  store i64 %6, i64* %4, align 8
  %7 = load i64, i64* %4, align 8
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_car(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_car(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_cdr(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %5 = load i64, i64* %2, align 8
  %6 = call i64 @expect_cons(i64 %5, i64* %3)
  store i64 %6, i64* %4, align 8
  %7 = load i64, i64* %3, align 8
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_cdr(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_cdr(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @prim__43(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  store i64 %1, i64* %4, align 8
  %5 = load i64, i64* %3, align 8
  %6 = and i64 %5, 7
  %7 = icmp ne i64 %6, 2
  br i1 %7, label %8, label %9

; <label>:8:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.37, i32 0, i32 0))
  br label %9

; <label>:9:                                      ; preds = %8, %2
  %10 = load i64, i64* %4, align 8
  %11 = and i64 %10, 7
  %12 = icmp ne i64 %11, 2
  br i1 %12, label %13, label %14

; <label>:13:                                     ; preds = %9
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.38, i32 0, i32 0))
  br label %14

; <label>:14:                                     ; preds = %13, %9
  %15 = load i64, i64* %3, align 8
  %16 = and i64 %15, -8
  %17 = lshr i64 %16, 32
  %18 = trunc i64 %17 to i32
  %19 = icmp sgt i32 %18, 0
  br i1 %19, label %20, label %31

; <label>:20:                                     ; preds = %14
  %21 = load i64, i64* %4, align 8
  %22 = and i64 %21, -8
  %23 = lshr i64 %22, 32
  %24 = trunc i64 %23 to i32
  %25 = load i64, i64* %3, align 8
  %26 = and i64 %25, -8
  %27 = lshr i64 %26, 32
  %28 = trunc i64 %27 to i32
  %29 = sub nsw i32 2147483647, %28
  %30 = icmp sgt i32 %24, %29
  br i1 %30, label %50, label %31

; <label>:31:                                     ; preds = %20, %14
  %32 = load i64, i64* %3, align 8
  %33 = and i64 %32, -8
  %34 = lshr i64 %33, 32
  %35 = trunc i64 %34 to i32
  %36 = icmp slt i32 %35, 0
  br i1 %36, label %37, label %51

; <label>:37:                                     ; preds = %31
  %38 = load i64, i64* %4, align 8
  %39 = and i64 %38, -8
  %40 = lshr i64 %39, 32
  %41 = trunc i64 %40 to i32
  %42 = sext i32 %41 to i64
  %43 = load i64, i64* %3, align 8
  %44 = and i64 %43, -8
  %45 = lshr i64 %44, 32
  %46 = trunc i64 %45 to i32
  %47 = sext i32 %46 to i64
  %48 = sub nsw i64 -2147483648, %47
  %49 = icmp slt i64 %42, %48
  br i1 %49, label %50, label %51

; <label>:50:                                     ; preds = %37, %20
  call void @fatal_err(i8* getelementptr inbounds ([71 x i8], [71 x i8]* @.str.39, i32 0, i32 0))
  br label %51

; <label>:51:                                     ; preds = %50, %37, %31
  %52 = load i64, i64* %3, align 8
  %53 = and i64 %52, -8
  %54 = lshr i64 %53, 32
  %55 = trunc i64 %54 to i32
  %56 = load i64, i64* %4, align 8
  %57 = and i64 %56, -8
  %58 = lshr i64 %57, 32
  %59 = trunc i64 %58 to i32
  %60 = add nsw i32 %55, %59
  %61 = zext i32 %60 to i64
  %62 = shl i64 %61, 32
  %63 = or i64 %62, 2
  ret i64 %63
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim__43(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64*, align 8
  store i64 %0, i64* %3, align 8
  %5 = load i64, i64* %3, align 8
  %6 = icmp eq i64 %5, 0
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  store i64 2, i64* %2, align 8
  br label %85

; <label>:8:                                      ; preds = %1
  %9 = load i64, i64* %3, align 8
  %10 = and i64 %9, 7
  %11 = icmp ne i64 %10, 1
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %8
  call void @fatal_err(i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.40, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %8
  %14 = load i64, i64* %3, align 8
  %15 = and i64 %14, -8
  %16 = inttoptr i64 %15 to i64*
  store i64* %16, i64** %4, align 8
  %17 = load i64*, i64** %4, align 8
  %18 = getelementptr inbounds i64, i64* %17, i64 0
  %19 = load i64, i64* %18, align 8
  %20 = and i64 %19, -8
  %21 = lshr i64 %20, 32
  %22 = trunc i64 %21 to i32
  %23 = icmp sgt i32 %22, 0
  br i1 %23, label %24, label %40

; <label>:24:                                     ; preds = %13
  %25 = load i64*, i64** %4, align 8
  %26 = getelementptr inbounds i64, i64* %25, i64 1
  %27 = load i64, i64* %26, align 8
  %28 = call i64 @applyprim__43(i64 %27)
  %29 = and i64 %28, -8
  %30 = lshr i64 %29, 32
  %31 = trunc i64 %30 to i32
  %32 = load i64*, i64** %4, align 8
  %33 = getelementptr inbounds i64, i64* %32, i64 0
  %34 = load i64, i64* %33, align 8
  %35 = and i64 %34, -8
  %36 = lshr i64 %35, 32
  %37 = trunc i64 %36 to i32
  %38 = sub nsw i32 2147483647, %37
  %39 = icmp sgt i32 %31, %38
  br i1 %39, label %66, label %40

; <label>:40:                                     ; preds = %24, %13
  %41 = load i64*, i64** %4, align 8
  %42 = getelementptr inbounds i64, i64* %41, i64 0
  %43 = load i64, i64* %42, align 8
  %44 = and i64 %43, -8
  %45 = lshr i64 %44, 32
  %46 = trunc i64 %45 to i32
  %47 = icmp slt i32 %46, 0
  br i1 %47, label %48, label %67

; <label>:48:                                     ; preds = %40
  %49 = load i64*, i64** %4, align 8
  %50 = getelementptr inbounds i64, i64* %49, i64 1
  %51 = load i64, i64* %50, align 8
  %52 = call i64 @applyprim__43(i64 %51)
  %53 = and i64 %52, -8
  %54 = lshr i64 %53, 32
  %55 = trunc i64 %54 to i32
  %56 = sext i32 %55 to i64
  %57 = load i64*, i64** %4, align 8
  %58 = getelementptr inbounds i64, i64* %57, i64 0
  %59 = load i64, i64* %58, align 8
  %60 = and i64 %59, -8
  %61 = lshr i64 %60, 32
  %62 = trunc i64 %61 to i32
  %63 = sext i32 %62 to i64
  %64 = sub nsw i64 -2147483648, %63
  %65 = icmp slt i64 %56, %64
  br i1 %65, label %66, label %67

; <label>:66:                                     ; preds = %48, %24
  call void @fatal_err(i8* getelementptr inbounds ([71 x i8], [71 x i8]* @.str.39, i32 0, i32 0))
  br label %67

; <label>:67:                                     ; preds = %66, %48, %40
  %68 = load i64*, i64** %4, align 8
  %69 = getelementptr inbounds i64, i64* %68, i64 0
  %70 = load i64, i64* %69, align 8
  %71 = and i64 %70, -8
  %72 = lshr i64 %71, 32
  %73 = trunc i64 %72 to i32
  %74 = load i64*, i64** %4, align 8
  %75 = getelementptr inbounds i64, i64* %74, i64 1
  %76 = load i64, i64* %75, align 8
  %77 = call i64 @applyprim__43(i64 %76)
  %78 = and i64 %77, -8
  %79 = lshr i64 %78, 32
  %80 = trunc i64 %79 to i32
  %81 = add nsw i32 %73, %80
  %82 = zext i32 %81 to i64
  %83 = shl i64 %82, 32
  %84 = or i64 %83, 2
  store i64 %84, i64* %2, align 8
  br label %85

; <label>:85:                                     ; preds = %67, %7
  %86 = load i64, i64* %2, align 8
  ret i64 %86
}

; Function Attrs: noinline ssp uwtable
define i64 @prim__45(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  store i64 %1, i64* %4, align 8
  %5 = load i64, i64* %3, align 8
  %6 = and i64 %5, 7
  %7 = icmp ne i64 %6, 2
  br i1 %7, label %8, label %9

; <label>:8:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.41, i32 0, i32 0))
  br label %9

; <label>:9:                                      ; preds = %8, %2
  %10 = load i64, i64* %4, align 8
  %11 = and i64 %10, 7
  %12 = icmp ne i64 %11, 2
  br i1 %12, label %13, label %14

; <label>:13:                                     ; preds = %9
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.42, i32 0, i32 0))
  br label %14

; <label>:14:                                     ; preds = %13, %9
  %15 = load i64, i64* %3, align 8
  %16 = and i64 %15, -8
  %17 = lshr i64 %16, 32
  %18 = trunc i64 %17 to i32
  %19 = icmp sgt i32 %18, 0
  br i1 %19, label %20, label %33

; <label>:20:                                     ; preds = %14
  %21 = load i64, i64* %4, align 8
  %22 = and i64 %21, -8
  %23 = lshr i64 %22, 32
  %24 = trunc i64 %23 to i32
  %25 = sext i32 %24 to i64
  %26 = load i64, i64* %3, align 8
  %27 = and i64 %26, -8
  %28 = lshr i64 %27, 32
  %29 = trunc i64 %28 to i32
  %30 = sext i32 %29 to i64
  %31 = add nsw i64 -2147483648, %30
  %32 = icmp slt i64 %25, %31
  br i1 %32, label %50, label %33

; <label>:33:                                     ; preds = %20, %14
  %34 = load i64, i64* %3, align 8
  %35 = and i64 %34, -8
  %36 = lshr i64 %35, 32
  %37 = trunc i64 %36 to i32
  %38 = icmp slt i32 %37, 0
  br i1 %38, label %39, label %51

; <label>:39:                                     ; preds = %33
  %40 = load i64, i64* %4, align 8
  %41 = and i64 %40, -8
  %42 = lshr i64 %41, 32
  %43 = trunc i64 %42 to i32
  %44 = load i64, i64* %3, align 8
  %45 = and i64 %44, -8
  %46 = lshr i64 %45, 32
  %47 = trunc i64 %46 to i32
  %48 = add nsw i32 2147483647, %47
  %49 = icmp sgt i32 %43, %48
  br i1 %49, label %50, label %51

; <label>:50:                                     ; preds = %39, %20
  call void @fatal_err(i8* getelementptr inbounds ([74 x i8], [74 x i8]* @.str.43, i32 0, i32 0))
  br label %51

; <label>:51:                                     ; preds = %50, %39, %33
  %52 = load i64, i64* %3, align 8
  %53 = and i64 %52, -8
  %54 = lshr i64 %53, 32
  %55 = trunc i64 %54 to i32
  %56 = load i64, i64* %4, align 8
  %57 = and i64 %56, -8
  %58 = lshr i64 %57, 32
  %59 = trunc i64 %58 to i32
  %60 = sub nsw i32 %55, %59
  %61 = zext i32 %60 to i64
  %62 = shl i64 %61, 32
  %63 = or i64 %62, 2
  ret i64 %63
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim__45(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64*, align 8
  store i64 %0, i64* %3, align 8
  %5 = load i64, i64* %3, align 8
  %6 = icmp eq i64 %5, 0
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  store i64 2, i64* %2, align 8
  br label %111

; <label>:8:                                      ; preds = %1
  %9 = load i64, i64* %3, align 8
  %10 = and i64 %9, 7
  %11 = icmp ne i64 %10, 1
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %8
  call void @fatal_err(i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.44, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %8
  %14 = load i64, i64* %3, align 8
  %15 = and i64 %14, -8
  %16 = inttoptr i64 %15 to i64*
  store i64* %16, i64** %4, align 8
  %17 = load i64*, i64** %4, align 8
  %18 = getelementptr inbounds i64, i64* %17, i64 1
  %19 = load i64, i64* %18, align 8
  %20 = icmp eq i64 %19, 0
  br i1 %20, label %21, label %42

; <label>:21:                                     ; preds = %13
  %22 = load i64*, i64** %4, align 8
  %23 = getelementptr inbounds i64, i64* %22, i64 0
  %24 = load i64, i64* %23, align 8
  %25 = and i64 %24, -8
  %26 = lshr i64 %25, 32
  %27 = trunc i64 %26 to i32
  %28 = sext i32 %27 to i64
  %29 = icmp eq i64 %28, -2147483648
  br i1 %29, label %30, label %31

; <label>:30:                                     ; preds = %21
  call void @fatal_err(i8* getelementptr inbounds ([74 x i8], [74 x i8]* @.str.43, i32 0, i32 0))
  br label %31

; <label>:31:                                     ; preds = %30, %21
  %32 = load i64*, i64** %4, align 8
  %33 = getelementptr inbounds i64, i64* %32, i64 0
  %34 = load i64, i64* %33, align 8
  %35 = and i64 %34, -8
  %36 = lshr i64 %35, 32
  %37 = trunc i64 %36 to i32
  %38 = sub nsw i32 0, %37
  %39 = zext i32 %38 to i64
  %40 = shl i64 %39, 32
  %41 = or i64 %40, 2
  store i64 %41, i64* %2, align 8
  br label %111

; <label>:42:                                     ; preds = %13
  %43 = load i64*, i64** %4, align 8
  %44 = getelementptr inbounds i64, i64* %43, i64 0
  %45 = load i64, i64* %44, align 8
  %46 = and i64 %45, -8
  %47 = lshr i64 %46, 32
  %48 = trunc i64 %47 to i32
  %49 = icmp sgt i32 %48, 0
  br i1 %49, label %50, label %68

; <label>:50:                                     ; preds = %42
  %51 = load i64*, i64** %4, align 8
  %52 = getelementptr inbounds i64, i64* %51, i64 1
  %53 = load i64, i64* %52, align 8
  %54 = call i64 @applyprim__43(i64 %53)
  %55 = and i64 %54, -8
  %56 = lshr i64 %55, 32
  %57 = trunc i64 %56 to i32
  %58 = sext i32 %57 to i64
  %59 = load i64*, i64** %4, align 8
  %60 = getelementptr inbounds i64, i64* %59, i64 0
  %61 = load i64, i64* %60, align 8
  %62 = and i64 %61, -8
  %63 = lshr i64 %62, 32
  %64 = trunc i64 %63 to i32
  %65 = sext i32 %64 to i64
  %66 = add nsw i64 -2147483648, %65
  %67 = icmp slt i64 %58, %66
  br i1 %67, label %92, label %68

; <label>:68:                                     ; preds = %50, %42
  %69 = load i64*, i64** %4, align 8
  %70 = getelementptr inbounds i64, i64* %69, i64 0
  %71 = load i64, i64* %70, align 8
  %72 = and i64 %71, -8
  %73 = lshr i64 %72, 32
  %74 = trunc i64 %73 to i32
  %75 = icmp slt i32 %74, 0
  br i1 %75, label %76, label %93

; <label>:76:                                     ; preds = %68
  %77 = load i64*, i64** %4, align 8
  %78 = getelementptr inbounds i64, i64* %77, i64 1
  %79 = load i64, i64* %78, align 8
  %80 = call i64 @applyprim__43(i64 %79)
  %81 = and i64 %80, -8
  %82 = lshr i64 %81, 32
  %83 = trunc i64 %82 to i32
  %84 = load i64*, i64** %4, align 8
  %85 = getelementptr inbounds i64, i64* %84, i64 0
  %86 = load i64, i64* %85, align 8
  %87 = and i64 %86, -8
  %88 = lshr i64 %87, 32
  %89 = trunc i64 %88 to i32
  %90 = add nsw i32 2147483647, %89
  %91 = icmp sgt i32 %83, %90
  br i1 %91, label %92, label %93

; <label>:92:                                     ; preds = %76, %50
  call void @fatal_err(i8* getelementptr inbounds ([74 x i8], [74 x i8]* @.str.43, i32 0, i32 0))
  br label %93

; <label>:93:                                     ; preds = %92, %76, %68
  %94 = load i64*, i64** %4, align 8
  %95 = getelementptr inbounds i64, i64* %94, i64 0
  %96 = load i64, i64* %95, align 8
  %97 = and i64 %96, -8
  %98 = lshr i64 %97, 32
  %99 = trunc i64 %98 to i32
  %100 = load i64*, i64** %4, align 8
  %101 = getelementptr inbounds i64, i64* %100, i64 1
  %102 = load i64, i64* %101, align 8
  %103 = call i64 @applyprim__43(i64 %102)
  %104 = and i64 %103, -8
  %105 = lshr i64 %104, 32
  %106 = trunc i64 %105 to i32
  %107 = sub nsw i32 %99, %106
  %108 = zext i32 %107 to i64
  %109 = shl i64 %108, 32
  %110 = or i64 %109, 2
  store i64 %110, i64* %2, align 8
  br label %111

; <label>:111:                                    ; preds = %93, %31, %7
  %112 = load i64, i64* %2, align 8
  ret i64 %112
}

; Function Attrs: noinline ssp uwtable
define i64 @prim__42(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  store i64 %1, i64* %4, align 8
  %5 = load i64, i64* %3, align 8
  %6 = and i64 %5, 7
  %7 = icmp ne i64 %6, 2
  br i1 %7, label %8, label %9

; <label>:8:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.45, i32 0, i32 0))
  br label %9

; <label>:9:                                      ; preds = %8, %2
  %10 = load i64, i64* %4, align 8
  %11 = and i64 %10, 7
  %12 = icmp ne i64 %11, 2
  br i1 %12, label %13, label %14

; <label>:13:                                     ; preds = %9
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.46, i32 0, i32 0))
  br label %14

; <label>:14:                                     ; preds = %13, %9
  %15 = load i64, i64* %4, align 8
  %16 = and i64 %15, -8
  %17 = lshr i64 %16, 32
  %18 = trunc i64 %17 to i32
  %19 = icmp ne i32 %18, 0
  br i1 %19, label %20, label %70

; <label>:20:                                     ; preds = %14
  %21 = load i64, i64* %3, align 8
  %22 = and i64 %21, -8
  %23 = lshr i64 %22, 32
  %24 = trunc i64 %23 to i32
  %25 = icmp sgt i32 %24, 0
  br i1 %25, label %26, label %37

; <label>:26:                                     ; preds = %20
  %27 = load i64, i64* %4, align 8
  %28 = and i64 %27, -8
  %29 = lshr i64 %28, 32
  %30 = trunc i64 %29 to i32
  %31 = load i64, i64* %3, align 8
  %32 = and i64 %31, -8
  %33 = lshr i64 %32, 32
  %34 = trunc i64 %33 to i32
  %35 = sdiv i32 2147483647, %34
  %36 = icmp sgt i32 %30, %35
  br i1 %36, label %69, label %37

; <label>:37:                                     ; preds = %26, %20
  %38 = load i64, i64* %3, align 8
  %39 = and i64 %38, -8
  %40 = lshr i64 %39, 32
  %41 = trunc i64 %40 to i32
  %42 = icmp slt i32 %41, -1
  br i1 %42, label %43, label %56

; <label>:43:                                     ; preds = %37
  %44 = load i64, i64* %4, align 8
  %45 = and i64 %44, -8
  %46 = lshr i64 %45, 32
  %47 = trunc i64 %46 to i32
  %48 = sext i32 %47 to i64
  %49 = load i64, i64* %3, align 8
  %50 = and i64 %49, -8
  %51 = lshr i64 %50, 32
  %52 = trunc i64 %51 to i32
  %53 = sext i32 %52 to i64
  %54 = sdiv i64 -2147483648, %53
  %55 = icmp slt i64 %48, %54
  br i1 %55, label %69, label %56

; <label>:56:                                     ; preds = %43, %37
  %57 = load i64, i64* %3, align 8
  %58 = and i64 %57, -8
  %59 = lshr i64 %58, 32
  %60 = trunc i64 %59 to i32
  %61 = icmp eq i32 %60, -1
  br i1 %61, label %62, label %70

; <label>:62:                                     ; preds = %56
  %63 = load i64, i64* %4, align 8
  %64 = and i64 %63, -8
  %65 = lshr i64 %64, 32
  %66 = trunc i64 %65 to i32
  %67 = sext i32 %66 to i64
  %68 = icmp eq i64 %67, -2147483648
  br i1 %68, label %69, label %70

; <label>:69:                                     ; preds = %62, %43, %26
  call void @fatal_err(i8* getelementptr inbounds ([77 x i8], [77 x i8]* @.str.47, i32 0, i32 0))
  br label %70

; <label>:70:                                     ; preds = %69, %62, %56, %14
  %71 = load i64, i64* %3, align 8
  %72 = and i64 %71, -8
  %73 = lshr i64 %72, 32
  %74 = trunc i64 %73 to i32
  %75 = load i64, i64* %4, align 8
  %76 = and i64 %75, -8
  %77 = lshr i64 %76, 32
  %78 = trunc i64 %77 to i32
  %79 = mul nsw i32 %74, %78
  %80 = zext i32 %79 to i64
  %81 = shl i64 %80, 32
  %82 = or i64 %81, 2
  ret i64 %82
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim__42(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64*, align 8
  store i64 %0, i64* %3, align 8
  %5 = load i64, i64* %3, align 8
  %6 = icmp eq i64 %5, 0
  br i1 %6, label %7, label %8

; <label>:7:                                      ; preds = %1
  store i64 4294967298, i64* %2, align 8
  br label %112

; <label>:8:                                      ; preds = %1
  %9 = load i64, i64* %3, align 8
  %10 = and i64 %9, 7
  %11 = icmp ne i64 %10, 1
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %8
  call void @fatal_err(i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.48, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %8
  %14 = load i64, i64* %3, align 8
  %15 = and i64 %14, -8
  %16 = inttoptr i64 %15 to i64*
  store i64* %16, i64** %4, align 8
  %17 = load i64*, i64** %4, align 8
  %18 = getelementptr inbounds i64, i64* %17, i64 1
  %19 = load i64, i64* %18, align 8
  %20 = call i64 @applyprim__42(i64 %19)
  %21 = and i64 %20, -8
  %22 = lshr i64 %21, 32
  %23 = trunc i64 %22 to i32
  %24 = icmp ne i32 %23, 0
  br i1 %24, label %25, label %94

; <label>:25:                                     ; preds = %13
  %26 = load i64*, i64** %4, align 8
  %27 = getelementptr inbounds i64, i64* %26, i64 0
  %28 = load i64, i64* %27, align 8
  %29 = and i64 %28, -8
  %30 = lshr i64 %29, 32
  %31 = trunc i64 %30 to i32
  %32 = icmp sgt i32 %31, 0
  br i1 %32, label %33, label %49

; <label>:33:                                     ; preds = %25
  %34 = load i64*, i64** %4, align 8
  %35 = getelementptr inbounds i64, i64* %34, i64 1
  %36 = load i64, i64* %35, align 8
  %37 = call i64 @applyprim__42(i64 %36)
  %38 = and i64 %37, -8
  %39 = lshr i64 %38, 32
  %40 = trunc i64 %39 to i32
  %41 = load i64*, i64** %4, align 8
  %42 = getelementptr inbounds i64, i64* %41, i64 0
  %43 = load i64, i64* %42, align 8
  %44 = and i64 %43, -8
  %45 = lshr i64 %44, 32
  %46 = trunc i64 %45 to i32
  %47 = sdiv i32 2147483647, %46
  %48 = icmp sgt i32 %40, %47
  br i1 %48, label %93, label %49

; <label>:49:                                     ; preds = %33, %25
  %50 = load i64*, i64** %4, align 8
  %51 = getelementptr inbounds i64, i64* %50, i64 0
  %52 = load i64, i64* %51, align 8
  %53 = and i64 %52, -8
  %54 = lshr i64 %53, 32
  %55 = trunc i64 %54 to i32
  %56 = icmp slt i32 %55, -1
  br i1 %56, label %57, label %75

; <label>:57:                                     ; preds = %49
  %58 = load i64*, i64** %4, align 8
  %59 = getelementptr inbounds i64, i64* %58, i64 1
  %60 = load i64, i64* %59, align 8
  %61 = call i64 @applyprim__42(i64 %60)
  %62 = and i64 %61, -8
  %63 = lshr i64 %62, 32
  %64 = trunc i64 %63 to i32
  %65 = sext i32 %64 to i64
  %66 = load i64*, i64** %4, align 8
  %67 = getelementptr inbounds i64, i64* %66, i64 0
  %68 = load i64, i64* %67, align 8
  %69 = and i64 %68, -8
  %70 = lshr i64 %69, 32
  %71 = trunc i64 %70 to i32
  %72 = sext i32 %71 to i64
  %73 = sdiv i64 -2147483648, %72
  %74 = icmp slt i64 %65, %73
  br i1 %74, label %93, label %75

; <label>:75:                                     ; preds = %57, %49
  %76 = load i64*, i64** %4, align 8
  %77 = getelementptr inbounds i64, i64* %76, i64 0
  %78 = load i64, i64* %77, align 8
  %79 = and i64 %78, -8
  %80 = lshr i64 %79, 32
  %81 = trunc i64 %80 to i32
  %82 = icmp eq i32 %81, -1
  br i1 %82, label %83, label %94

; <label>:83:                                     ; preds = %75
  %84 = load i64*, i64** %4, align 8
  %85 = getelementptr inbounds i64, i64* %84, i64 1
  %86 = load i64, i64* %85, align 8
  %87 = call i64 @applyprim__42(i64 %86)
  %88 = and i64 %87, -8
  %89 = lshr i64 %88, 32
  %90 = trunc i64 %89 to i32
  %91 = sext i32 %90 to i64
  %92 = icmp eq i64 %91, -2147483648
  br i1 %92, label %93, label %94

; <label>:93:                                     ; preds = %83, %57, %33
  call void @fatal_err(i8* getelementptr inbounds ([77 x i8], [77 x i8]* @.str.47, i32 0, i32 0))
  br label %94

; <label>:94:                                     ; preds = %93, %83, %75, %13
  %95 = load i64*, i64** %4, align 8
  %96 = getelementptr inbounds i64, i64* %95, i64 0
  %97 = load i64, i64* %96, align 8
  %98 = and i64 %97, -8
  %99 = lshr i64 %98, 32
  %100 = trunc i64 %99 to i32
  %101 = load i64*, i64** %4, align 8
  %102 = getelementptr inbounds i64, i64* %101, i64 1
  %103 = load i64, i64* %102, align 8
  %104 = call i64 @applyprim__42(i64 %103)
  %105 = and i64 %104, -8
  %106 = lshr i64 %105, 32
  %107 = trunc i64 %106 to i32
  %108 = mul nsw i32 %100, %107
  %109 = zext i32 %108 to i64
  %110 = shl i64 %109, 32
  %111 = or i64 %110, 2
  store i64 %111, i64* %2, align 8
  br label %112

; <label>:112:                                    ; preds = %94, %7
  %113 = load i64, i64* %2, align 8
  ret i64 %113
}

; Function Attrs: noinline ssp uwtable
define i64 @prim__47(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  store i64 %1, i64* %4, align 8
  %5 = load i64, i64* %3, align 8
  %6 = and i64 %5, 7
  %7 = icmp ne i64 %6, 2
  br i1 %7, label %8, label %9

; <label>:8:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.49, i32 0, i32 0))
  br label %9

; <label>:9:                                      ; preds = %8, %2
  %10 = load i64, i64* %4, align 8
  %11 = and i64 %10, 7
  %12 = icmp ne i64 %11, 2
  br i1 %12, label %13, label %14

; <label>:13:                                     ; preds = %9
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.50, i32 0, i32 0))
  br label %14

; <label>:14:                                     ; preds = %13, %9
  %15 = load i64, i64* %4, align 8
  %16 = and i64 %15, -8
  %17 = lshr i64 %16, 32
  %18 = trunc i64 %17 to i32
  %19 = icmp eq i32 %18, 0
  br i1 %19, label %20, label %21

; <label>:20:                                     ; preds = %14
  call void @fatal_err(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.51, i32 0, i32 0))
  br label %21

; <label>:21:                                     ; preds = %20, %14
  %22 = load i64, i64* %3, align 8
  %23 = and i64 %22, -8
  %24 = lshr i64 %23, 32
  %25 = trunc i64 %24 to i32
  %26 = sext i32 %25 to i64
  %27 = icmp eq i64 %26, -2147483648
  br i1 %27, label %28, label %34

; <label>:28:                                     ; preds = %21
  %29 = load i64, i64* %4, align 8
  %30 = and i64 %29, -8
  %31 = lshr i64 %30, 32
  %32 = trunc i64 %31 to i32
  %33 = icmp eq i32 %32, -1
  br i1 %33, label %47, label %34

; <label>:34:                                     ; preds = %28, %21
  %35 = load i64, i64* %3, align 8
  %36 = and i64 %35, -8
  %37 = lshr i64 %36, 32
  %38 = trunc i64 %37 to i32
  %39 = icmp eq i32 %38, -1
  br i1 %39, label %40, label %48

; <label>:40:                                     ; preds = %34
  %41 = load i64, i64* %4, align 8
  %42 = and i64 %41, -8
  %43 = lshr i64 %42, 32
  %44 = trunc i64 %43 to i32
  %45 = sext i32 %44 to i64
  %46 = icmp eq i64 %45, -2147483648
  br i1 %46, label %47, label %48

; <label>:47:                                     ; preds = %40, %28
  call void @fatal_err(i8* getelementptr inbounds ([71 x i8], [71 x i8]* @.str.52, i32 0, i32 0))
  br label %48

; <label>:48:                                     ; preds = %47, %40, %34
  %49 = load i64, i64* %3, align 8
  %50 = and i64 %49, -8
  %51 = lshr i64 %50, 32
  %52 = trunc i64 %51 to i32
  %53 = load i64, i64* %4, align 8
  %54 = and i64 %53, -8
  %55 = lshr i64 %54, 32
  %56 = trunc i64 %55 to i32
  %57 = sdiv i32 %52, %56
  %58 = zext i32 %57 to i64
  %59 = shl i64 %58, 32
  %60 = or i64 %59, 2
  ret i64 %60
}

; Function Attrs: noinline ssp uwtable
define i64 @prim__61(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  %6 = load i64, i64* %4, align 8
  %7 = and i64 %6, 7
  %8 = icmp ne i64 %7, 2
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.53, i32 0, i32 0))
  br label %10

; <label>:10:                                     ; preds = %9, %2
  %11 = load i64, i64* %5, align 8
  %12 = and i64 %11, 7
  %13 = icmp ne i64 %12, 2
  br i1 %13, label %14, label %15

; <label>:14:                                     ; preds = %10
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.54, i32 0, i32 0))
  br label %15

; <label>:15:                                     ; preds = %14, %10
  %16 = load i64, i64* %4, align 8
  %17 = and i64 %16, -8
  %18 = lshr i64 %17, 32
  %19 = trunc i64 %18 to i32
  %20 = load i64, i64* %5, align 8
  %21 = and i64 %20, -8
  %22 = lshr i64 %21, 32
  %23 = trunc i64 %22 to i32
  %24 = icmp eq i32 %19, %23
  br i1 %24, label %25, label %26

; <label>:25:                                     ; preds = %15
  store i64 31, i64* %3, align 8
  br label %27

; <label>:26:                                     ; preds = %15
  store i64 15, i64* %3, align 8
  br label %27

; <label>:27:                                     ; preds = %26, %25
  %28 = load i64, i64* %3, align 8
  ret i64 %28
}

; Function Attrs: noinline ssp uwtable
define i64 @prim__60(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  %6 = load i64, i64* %4, align 8
  %7 = and i64 %6, 7
  %8 = icmp ne i64 %7, 2
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.55, i32 0, i32 0))
  br label %10

; <label>:10:                                     ; preds = %9, %2
  %11 = load i64, i64* %5, align 8
  %12 = and i64 %11, 7
  %13 = icmp ne i64 %12, 2
  br i1 %13, label %14, label %15

; <label>:14:                                     ; preds = %10
  call void @fatal_err(i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.56, i32 0, i32 0))
  br label %15

; <label>:15:                                     ; preds = %14, %10
  %16 = load i64, i64* %4, align 8
  %17 = and i64 %16, -8
  %18 = lshr i64 %17, 32
  %19 = trunc i64 %18 to i32
  %20 = load i64, i64* %5, align 8
  %21 = and i64 %20, -8
  %22 = lshr i64 %21, 32
  %23 = trunc i64 %22 to i32
  %24 = icmp slt i32 %19, %23
  br i1 %24, label %25, label %26

; <label>:25:                                     ; preds = %15
  store i64 31, i64* %3, align 8
  br label %27

; <label>:26:                                     ; preds = %15
  store i64 15, i64* %3, align 8
  br label %27

; <label>:27:                                     ; preds = %26, %25
  %28 = load i64, i64* %3, align 8
  ret i64 %28
}

; Function Attrs: noinline ssp uwtable
define i64 @prim__60_61(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  %6 = load i64, i64* %4, align 8
  %7 = and i64 %6, 7
  %8 = icmp ne i64 %7, 2
  br i1 %8, label %9, label %10

; <label>:9:                                      ; preds = %2
  call void @fatal_err(i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.57, i32 0, i32 0))
  br label %10

; <label>:10:                                     ; preds = %9, %2
  %11 = load i64, i64* %5, align 8
  %12 = and i64 %11, 7
  %13 = icmp ne i64 %12, 2
  br i1 %13, label %14, label %15

; <label>:14:                                     ; preds = %10
  call void @fatal_err(i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.58, i32 0, i32 0))
  br label %15

; <label>:15:                                     ; preds = %14, %10
  %16 = load i64, i64* %4, align 8
  %17 = and i64 %16, -8
  %18 = lshr i64 %17, 32
  %19 = trunc i64 %18 to i32
  %20 = load i64, i64* %5, align 8
  %21 = and i64 %20, -8
  %22 = lshr i64 %21, 32
  %23 = trunc i64 %22 to i32
  %24 = icmp sle i32 %19, %23
  br i1 %24, label %25, label %26

; <label>:25:                                     ; preds = %15
  store i64 31, i64* %3, align 8
  br label %27

; <label>:26:                                     ; preds = %15
  store i64 15, i64* %3, align 8
  br label %27

; <label>:27:                                     ; preds = %26, %25
  %28 = load i64, i64* %3, align 8
  ret i64 %28
}

; Function Attrs: noinline nounwind ssp uwtable
define i64 @prim_not(i64) #4 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %3, align 8
  %4 = load i64, i64* %3, align 8
  %5 = icmp eq i64 %4, 15
  br i1 %5, label %6, label %7

; <label>:6:                                      ; preds = %1
  store i64 31, i64* %2, align 8
  br label %8

; <label>:7:                                      ; preds = %1
  store i64 15, i64* %2, align 8
  br label %8

; <label>:8:                                      ; preds = %7, %6
  %9 = load i64, i64* %2, align 8
  ret i64 %9
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_not(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_not(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_make_45hash(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca %struct.my_hash*, align 8
  %4 = alloca %struct.my_hash*, align 8
  %5 = alloca i64*, align 8
  %6 = alloca i64, align 8
  %7 = alloca i64*, align 8
  %8 = alloca i64, align 8
  %9 = alloca i64, align 8
  %10 = alloca i64, align 8
  %11 = alloca i32, align 4
  %12 = alloca i32, align 4
  %13 = alloca i32, align 4
  %14 = alloca i32, align 4
  %15 = alloca i8*, align 8
  %16 = alloca i32, align 4
  %17 = alloca i32, align 4
  %18 = alloca i32, align 4
  %19 = alloca i32, align 4
  %20 = alloca i32, align 4
  %21 = alloca i8*, align 8
  %22 = alloca i32, align 4
  %23 = alloca %struct.UT_hash_bucket*, align 8
  %24 = alloca i32, align 4
  %25 = alloca i32, align 4
  %26 = alloca %struct.UT_hash_handle*, align 8
  %27 = alloca %struct.UT_hash_handle*, align 8
  %28 = alloca %struct.UT_hash_bucket*, align 8
  %29 = alloca %struct.UT_hash_bucket*, align 8
  store i64 %0, i64* %2, align 8
  %30 = load i64, i64* %2, align 8
  %31 = and i64 %30, 7
  %32 = icmp ne i64 %31, 1
  br i1 %32, label %33, label %34

; <label>:33:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([47 x i8], [47 x i8]* @.str.59, i32 0, i32 0))
  br label %34

; <label>:34:                                     ; preds = %33, %1
  store %struct.my_hash* null, %struct.my_hash** %3, align 8
  store %struct.my_hash* null, %struct.my_hash** %4, align 8
  %35 = load i64, i64* @MEMCOUNT, align 8
  %36 = add i64 %35, 4112
  store i64 %36, i64* @MEMCOUNT, align 8
  %37 = load i64, i64* @MEMCOUNT, align 8
  %38 = load i64, i64* @MEMLIMIT, align 8
  %39 = icmp uge i64 %37, %38
  br i1 %39, label %40, label %41

; <label>:40:                                     ; preds = %34
  call void @fatal_err(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.3, i32 0, i32 0))
  br label %41

; <label>:41:                                     ; preds = %40, %34
  %42 = call i8* @malloc(i64 4096) #8
  %43 = bitcast i8* %42 to i64*
  store i64* %43, i64** %5, align 8
  store i64 0, i64* %6, align 8
  %44 = call i8* @malloc(i64 16) #8
  %45 = bitcast i8* %44 to i64*
  store i64* %45, i64** %7, align 8
  %46 = load i64*, i64** %7, align 8
  %47 = getelementptr inbounds i64, i64* %46, i64 0
  store i64 2, i64* %47, align 8
  br label %48

; <label>:48:                                     ; preds = %57, %41
  %49 = load i64, i64* %2, align 8
  %50 = and i64 %49, 7
  %51 = icmp eq i64 %50, 1
  br i1 %51, label %52, label %55

; <label>:52:                                     ; preds = %48
  %53 = load i64, i64* %6, align 8
  %54 = icmp ult i64 %53, 512
  br label %55

; <label>:55:                                     ; preds = %52, %48
  %56 = phi i1 [ false, %48 ], [ %54, %52 ]
  br i1 %56, label %57, label %64

; <label>:57:                                     ; preds = %55
  %58 = load i64, i64* %2, align 8
  %59 = call i64 @expect_cons(i64 %58, i64* %2)
  %60 = load i64*, i64** %5, align 8
  %61 = load i64, i64* %6, align 8
  %62 = add i64 %61, 1
  store i64 %62, i64* %6, align 8
  %63 = getelementptr inbounds i64, i64* %60, i64 %61
  store i64 %59, i64* %63, align 8
  br label %48

; <label>:64:                                     ; preds = %55
  store i64 0, i64* %8, align 8
  br label %65

; <label>:65:                                     ; preds = %1414, %64
  %66 = load i64, i64* %8, align 8
  %67 = load i64, i64* %6, align 8
  %68 = icmp ult i64 %66, %67
  br i1 %68, label %69, label %1417

; <label>:69:                                     ; preds = %65
  %70 = load i64*, i64** %5, align 8
  %71 = load i64, i64* %8, align 8
  %72 = getelementptr inbounds i64, i64* %70, i64 %71
  %73 = load i64, i64* %72, align 8
  store i64 %73, i64* %9, align 8
  %74 = load i64, i64* %9, align 8
  %75 = call i64 @expect_cons(i64 %74, i64* %9)
  store i64 %75, i64* %10, align 8
  br label %76

; <label>:76:                                     ; preds = %69
  br label %77

; <label>:77:                                     ; preds = %76
  br label %78

; <label>:78:                                     ; preds = %77
  %79 = bitcast i64* %10 to i8*
  store i8* %79, i8** %15, align 8
  store i32 -17973521, i32* %11, align 4
  store i32 -1640531527, i32* %13, align 4
  store i32 -1640531527, i32* %12, align 4
  store i32 8, i32* %14, align 4
  br label %80

; <label>:80:                                     ; preds = %247, %78
  %81 = load i32, i32* %14, align 4
  %82 = icmp uge i32 %81, 12
  br i1 %82, label %83, label %252

; <label>:83:                                     ; preds = %80
  %84 = load i8*, i8** %15, align 8
  %85 = getelementptr inbounds i8, i8* %84, i64 0
  %86 = load i8, i8* %85, align 1
  %87 = zext i8 %86 to i32
  %88 = load i8*, i8** %15, align 8
  %89 = getelementptr inbounds i8, i8* %88, i64 1
  %90 = load i8, i8* %89, align 1
  %91 = zext i8 %90 to i32
  %92 = shl i32 %91, 8
  %93 = add i32 %87, %92
  %94 = load i8*, i8** %15, align 8
  %95 = getelementptr inbounds i8, i8* %94, i64 2
  %96 = load i8, i8* %95, align 1
  %97 = zext i8 %96 to i32
  %98 = shl i32 %97, 16
  %99 = add i32 %93, %98
  %100 = load i8*, i8** %15, align 8
  %101 = getelementptr inbounds i8, i8* %100, i64 3
  %102 = load i8, i8* %101, align 1
  %103 = zext i8 %102 to i32
  %104 = shl i32 %103, 24
  %105 = add i32 %99, %104
  %106 = load i32, i32* %12, align 4
  %107 = add i32 %106, %105
  store i32 %107, i32* %12, align 4
  %108 = load i8*, i8** %15, align 8
  %109 = getelementptr inbounds i8, i8* %108, i64 4
  %110 = load i8, i8* %109, align 1
  %111 = zext i8 %110 to i32
  %112 = load i8*, i8** %15, align 8
  %113 = getelementptr inbounds i8, i8* %112, i64 5
  %114 = load i8, i8* %113, align 1
  %115 = zext i8 %114 to i32
  %116 = shl i32 %115, 8
  %117 = add i32 %111, %116
  %118 = load i8*, i8** %15, align 8
  %119 = getelementptr inbounds i8, i8* %118, i64 6
  %120 = load i8, i8* %119, align 1
  %121 = zext i8 %120 to i32
  %122 = shl i32 %121, 16
  %123 = add i32 %117, %122
  %124 = load i8*, i8** %15, align 8
  %125 = getelementptr inbounds i8, i8* %124, i64 7
  %126 = load i8, i8* %125, align 1
  %127 = zext i8 %126 to i32
  %128 = shl i32 %127, 24
  %129 = add i32 %123, %128
  %130 = load i32, i32* %13, align 4
  %131 = add i32 %130, %129
  store i32 %131, i32* %13, align 4
  %132 = load i8*, i8** %15, align 8
  %133 = getelementptr inbounds i8, i8* %132, i64 8
  %134 = load i8, i8* %133, align 1
  %135 = zext i8 %134 to i32
  %136 = load i8*, i8** %15, align 8
  %137 = getelementptr inbounds i8, i8* %136, i64 9
  %138 = load i8, i8* %137, align 1
  %139 = zext i8 %138 to i32
  %140 = shl i32 %139, 8
  %141 = add i32 %135, %140
  %142 = load i8*, i8** %15, align 8
  %143 = getelementptr inbounds i8, i8* %142, i64 10
  %144 = load i8, i8* %143, align 1
  %145 = zext i8 %144 to i32
  %146 = shl i32 %145, 16
  %147 = add i32 %141, %146
  %148 = load i8*, i8** %15, align 8
  %149 = getelementptr inbounds i8, i8* %148, i64 11
  %150 = load i8, i8* %149, align 1
  %151 = zext i8 %150 to i32
  %152 = shl i32 %151, 24
  %153 = add i32 %147, %152
  %154 = load i32, i32* %11, align 4
  %155 = add i32 %154, %153
  store i32 %155, i32* %11, align 4
  br label %156

; <label>:156:                                    ; preds = %83
  %157 = load i32, i32* %13, align 4
  %158 = load i32, i32* %12, align 4
  %159 = sub i32 %158, %157
  store i32 %159, i32* %12, align 4
  %160 = load i32, i32* %11, align 4
  %161 = load i32, i32* %12, align 4
  %162 = sub i32 %161, %160
  store i32 %162, i32* %12, align 4
  %163 = load i32, i32* %11, align 4
  %164 = lshr i32 %163, 13
  %165 = load i32, i32* %12, align 4
  %166 = xor i32 %165, %164
  store i32 %166, i32* %12, align 4
  %167 = load i32, i32* %11, align 4
  %168 = load i32, i32* %13, align 4
  %169 = sub i32 %168, %167
  store i32 %169, i32* %13, align 4
  %170 = load i32, i32* %12, align 4
  %171 = load i32, i32* %13, align 4
  %172 = sub i32 %171, %170
  store i32 %172, i32* %13, align 4
  %173 = load i32, i32* %12, align 4
  %174 = shl i32 %173, 8
  %175 = load i32, i32* %13, align 4
  %176 = xor i32 %175, %174
  store i32 %176, i32* %13, align 4
  %177 = load i32, i32* %12, align 4
  %178 = load i32, i32* %11, align 4
  %179 = sub i32 %178, %177
  store i32 %179, i32* %11, align 4
  %180 = load i32, i32* %13, align 4
  %181 = load i32, i32* %11, align 4
  %182 = sub i32 %181, %180
  store i32 %182, i32* %11, align 4
  %183 = load i32, i32* %13, align 4
  %184 = lshr i32 %183, 13
  %185 = load i32, i32* %11, align 4
  %186 = xor i32 %185, %184
  store i32 %186, i32* %11, align 4
  %187 = load i32, i32* %13, align 4
  %188 = load i32, i32* %12, align 4
  %189 = sub i32 %188, %187
  store i32 %189, i32* %12, align 4
  %190 = load i32, i32* %11, align 4
  %191 = load i32, i32* %12, align 4
  %192 = sub i32 %191, %190
  store i32 %192, i32* %12, align 4
  %193 = load i32, i32* %11, align 4
  %194 = lshr i32 %193, 12
  %195 = load i32, i32* %12, align 4
  %196 = xor i32 %195, %194
  store i32 %196, i32* %12, align 4
  %197 = load i32, i32* %11, align 4
  %198 = load i32, i32* %13, align 4
  %199 = sub i32 %198, %197
  store i32 %199, i32* %13, align 4
  %200 = load i32, i32* %12, align 4
  %201 = load i32, i32* %13, align 4
  %202 = sub i32 %201, %200
  store i32 %202, i32* %13, align 4
  %203 = load i32, i32* %12, align 4
  %204 = shl i32 %203, 16
  %205 = load i32, i32* %13, align 4
  %206 = xor i32 %205, %204
  store i32 %206, i32* %13, align 4
  %207 = load i32, i32* %12, align 4
  %208 = load i32, i32* %11, align 4
  %209 = sub i32 %208, %207
  store i32 %209, i32* %11, align 4
  %210 = load i32, i32* %13, align 4
  %211 = load i32, i32* %11, align 4
  %212 = sub i32 %211, %210
  store i32 %212, i32* %11, align 4
  %213 = load i32, i32* %13, align 4
  %214 = lshr i32 %213, 5
  %215 = load i32, i32* %11, align 4
  %216 = xor i32 %215, %214
  store i32 %216, i32* %11, align 4
  %217 = load i32, i32* %13, align 4
  %218 = load i32, i32* %12, align 4
  %219 = sub i32 %218, %217
  store i32 %219, i32* %12, align 4
  %220 = load i32, i32* %11, align 4
  %221 = load i32, i32* %12, align 4
  %222 = sub i32 %221, %220
  store i32 %222, i32* %12, align 4
  %223 = load i32, i32* %11, align 4
  %224 = lshr i32 %223, 3
  %225 = load i32, i32* %12, align 4
  %226 = xor i32 %225, %224
  store i32 %226, i32* %12, align 4
  %227 = load i32, i32* %11, align 4
  %228 = load i32, i32* %13, align 4
  %229 = sub i32 %228, %227
  store i32 %229, i32* %13, align 4
  %230 = load i32, i32* %12, align 4
  %231 = load i32, i32* %13, align 4
  %232 = sub i32 %231, %230
  store i32 %232, i32* %13, align 4
  %233 = load i32, i32* %12, align 4
  %234 = shl i32 %233, 10
  %235 = load i32, i32* %13, align 4
  %236 = xor i32 %235, %234
  store i32 %236, i32* %13, align 4
  %237 = load i32, i32* %12, align 4
  %238 = load i32, i32* %11, align 4
  %239 = sub i32 %238, %237
  store i32 %239, i32* %11, align 4
  %240 = load i32, i32* %13, align 4
  %241 = load i32, i32* %11, align 4
  %242 = sub i32 %241, %240
  store i32 %242, i32* %11, align 4
  %243 = load i32, i32* %13, align 4
  %244 = lshr i32 %243, 15
  %245 = load i32, i32* %11, align 4
  %246 = xor i32 %245, %244
  store i32 %246, i32* %11, align 4
  br label %247

; <label>:247:                                    ; preds = %156
  %248 = load i8*, i8** %15, align 8
  %249 = getelementptr inbounds i8, i8* %248, i64 12
  store i8* %249, i8** %15, align 8
  %250 = load i32, i32* %14, align 4
  %251 = sub i32 %250, 12
  store i32 %251, i32* %14, align 4
  br label %80

; <label>:252:                                    ; preds = %80
  %253 = load i32, i32* %11, align 4
  %254 = add i32 %253, 8
  store i32 %254, i32* %11, align 4
  %255 = load i32, i32* %14, align 4
  switch i32 %255, label %342 [
    i32 11, label %256
    i32 10, label %264
    i32 9, label %272
    i32 8, label %280
    i32 7, label %288
    i32 6, label %296
    i32 5, label %304
    i32 4, label %311
    i32 3, label %319
    i32 2, label %327
    i32 1, label %335
  ]

; <label>:256:                                    ; preds = %252
  %257 = load i8*, i8** %15, align 8
  %258 = getelementptr inbounds i8, i8* %257, i64 10
  %259 = load i8, i8* %258, align 1
  %260 = zext i8 %259 to i32
  %261 = shl i32 %260, 24
  %262 = load i32, i32* %11, align 4
  %263 = add i32 %262, %261
  store i32 %263, i32* %11, align 4
  br label %264

; <label>:264:                                    ; preds = %252, %256
  %265 = load i8*, i8** %15, align 8
  %266 = getelementptr inbounds i8, i8* %265, i64 9
  %267 = load i8, i8* %266, align 1
  %268 = zext i8 %267 to i32
  %269 = shl i32 %268, 16
  %270 = load i32, i32* %11, align 4
  %271 = add i32 %270, %269
  store i32 %271, i32* %11, align 4
  br label %272

; <label>:272:                                    ; preds = %252, %264
  %273 = load i8*, i8** %15, align 8
  %274 = getelementptr inbounds i8, i8* %273, i64 8
  %275 = load i8, i8* %274, align 1
  %276 = zext i8 %275 to i32
  %277 = shl i32 %276, 8
  %278 = load i32, i32* %11, align 4
  %279 = add i32 %278, %277
  store i32 %279, i32* %11, align 4
  br label %280

; <label>:280:                                    ; preds = %252, %272
  %281 = load i8*, i8** %15, align 8
  %282 = getelementptr inbounds i8, i8* %281, i64 7
  %283 = load i8, i8* %282, align 1
  %284 = zext i8 %283 to i32
  %285 = shl i32 %284, 24
  %286 = load i32, i32* %13, align 4
  %287 = add i32 %286, %285
  store i32 %287, i32* %13, align 4
  br label %288

; <label>:288:                                    ; preds = %252, %280
  %289 = load i8*, i8** %15, align 8
  %290 = getelementptr inbounds i8, i8* %289, i64 6
  %291 = load i8, i8* %290, align 1
  %292 = zext i8 %291 to i32
  %293 = shl i32 %292, 16
  %294 = load i32, i32* %13, align 4
  %295 = add i32 %294, %293
  store i32 %295, i32* %13, align 4
  br label %296

; <label>:296:                                    ; preds = %252, %288
  %297 = load i8*, i8** %15, align 8
  %298 = getelementptr inbounds i8, i8* %297, i64 5
  %299 = load i8, i8* %298, align 1
  %300 = zext i8 %299 to i32
  %301 = shl i32 %300, 8
  %302 = load i32, i32* %13, align 4
  %303 = add i32 %302, %301
  store i32 %303, i32* %13, align 4
  br label %304

; <label>:304:                                    ; preds = %252, %296
  %305 = load i8*, i8** %15, align 8
  %306 = getelementptr inbounds i8, i8* %305, i64 4
  %307 = load i8, i8* %306, align 1
  %308 = zext i8 %307 to i32
  %309 = load i32, i32* %13, align 4
  %310 = add i32 %309, %308
  store i32 %310, i32* %13, align 4
  br label %311

; <label>:311:                                    ; preds = %252, %304
  %312 = load i8*, i8** %15, align 8
  %313 = getelementptr inbounds i8, i8* %312, i64 3
  %314 = load i8, i8* %313, align 1
  %315 = zext i8 %314 to i32
  %316 = shl i32 %315, 24
  %317 = load i32, i32* %12, align 4
  %318 = add i32 %317, %316
  store i32 %318, i32* %12, align 4
  br label %319

; <label>:319:                                    ; preds = %252, %311
  %320 = load i8*, i8** %15, align 8
  %321 = getelementptr inbounds i8, i8* %320, i64 2
  %322 = load i8, i8* %321, align 1
  %323 = zext i8 %322 to i32
  %324 = shl i32 %323, 16
  %325 = load i32, i32* %12, align 4
  %326 = add i32 %325, %324
  store i32 %326, i32* %12, align 4
  br label %327

; <label>:327:                                    ; preds = %252, %319
  %328 = load i8*, i8** %15, align 8
  %329 = getelementptr inbounds i8, i8* %328, i64 1
  %330 = load i8, i8* %329, align 1
  %331 = zext i8 %330 to i32
  %332 = shl i32 %331, 8
  %333 = load i32, i32* %12, align 4
  %334 = add i32 %333, %332
  store i32 %334, i32* %12, align 4
  br label %335

; <label>:335:                                    ; preds = %252, %327
  %336 = load i8*, i8** %15, align 8
  %337 = getelementptr inbounds i8, i8* %336, i64 0
  %338 = load i8, i8* %337, align 1
  %339 = zext i8 %338 to i32
  %340 = load i32, i32* %12, align 4
  %341 = add i32 %340, %339
  store i32 %341, i32* %12, align 4
  br label %342

; <label>:342:                                    ; preds = %335, %252
  br label %343

; <label>:343:                                    ; preds = %342
  %344 = load i32, i32* %13, align 4
  %345 = load i32, i32* %12, align 4
  %346 = sub i32 %345, %344
  store i32 %346, i32* %12, align 4
  %347 = load i32, i32* %11, align 4
  %348 = load i32, i32* %12, align 4
  %349 = sub i32 %348, %347
  store i32 %349, i32* %12, align 4
  %350 = load i32, i32* %11, align 4
  %351 = lshr i32 %350, 13
  %352 = load i32, i32* %12, align 4
  %353 = xor i32 %352, %351
  store i32 %353, i32* %12, align 4
  %354 = load i32, i32* %11, align 4
  %355 = load i32, i32* %13, align 4
  %356 = sub i32 %355, %354
  store i32 %356, i32* %13, align 4
  %357 = load i32, i32* %12, align 4
  %358 = load i32, i32* %13, align 4
  %359 = sub i32 %358, %357
  store i32 %359, i32* %13, align 4
  %360 = load i32, i32* %12, align 4
  %361 = shl i32 %360, 8
  %362 = load i32, i32* %13, align 4
  %363 = xor i32 %362, %361
  store i32 %363, i32* %13, align 4
  %364 = load i32, i32* %12, align 4
  %365 = load i32, i32* %11, align 4
  %366 = sub i32 %365, %364
  store i32 %366, i32* %11, align 4
  %367 = load i32, i32* %13, align 4
  %368 = load i32, i32* %11, align 4
  %369 = sub i32 %368, %367
  store i32 %369, i32* %11, align 4
  %370 = load i32, i32* %13, align 4
  %371 = lshr i32 %370, 13
  %372 = load i32, i32* %11, align 4
  %373 = xor i32 %372, %371
  store i32 %373, i32* %11, align 4
  %374 = load i32, i32* %13, align 4
  %375 = load i32, i32* %12, align 4
  %376 = sub i32 %375, %374
  store i32 %376, i32* %12, align 4
  %377 = load i32, i32* %11, align 4
  %378 = load i32, i32* %12, align 4
  %379 = sub i32 %378, %377
  store i32 %379, i32* %12, align 4
  %380 = load i32, i32* %11, align 4
  %381 = lshr i32 %380, 12
  %382 = load i32, i32* %12, align 4
  %383 = xor i32 %382, %381
  store i32 %383, i32* %12, align 4
  %384 = load i32, i32* %11, align 4
  %385 = load i32, i32* %13, align 4
  %386 = sub i32 %385, %384
  store i32 %386, i32* %13, align 4
  %387 = load i32, i32* %12, align 4
  %388 = load i32, i32* %13, align 4
  %389 = sub i32 %388, %387
  store i32 %389, i32* %13, align 4
  %390 = load i32, i32* %12, align 4
  %391 = shl i32 %390, 16
  %392 = load i32, i32* %13, align 4
  %393 = xor i32 %392, %391
  store i32 %393, i32* %13, align 4
  %394 = load i32, i32* %12, align 4
  %395 = load i32, i32* %11, align 4
  %396 = sub i32 %395, %394
  store i32 %396, i32* %11, align 4
  %397 = load i32, i32* %13, align 4
  %398 = load i32, i32* %11, align 4
  %399 = sub i32 %398, %397
  store i32 %399, i32* %11, align 4
  %400 = load i32, i32* %13, align 4
  %401 = lshr i32 %400, 5
  %402 = load i32, i32* %11, align 4
  %403 = xor i32 %402, %401
  store i32 %403, i32* %11, align 4
  %404 = load i32, i32* %13, align 4
  %405 = load i32, i32* %12, align 4
  %406 = sub i32 %405, %404
  store i32 %406, i32* %12, align 4
  %407 = load i32, i32* %11, align 4
  %408 = load i32, i32* %12, align 4
  %409 = sub i32 %408, %407
  store i32 %409, i32* %12, align 4
  %410 = load i32, i32* %11, align 4
  %411 = lshr i32 %410, 3
  %412 = load i32, i32* %12, align 4
  %413 = xor i32 %412, %411
  store i32 %413, i32* %12, align 4
  %414 = load i32, i32* %11, align 4
  %415 = load i32, i32* %13, align 4
  %416 = sub i32 %415, %414
  store i32 %416, i32* %13, align 4
  %417 = load i32, i32* %12, align 4
  %418 = load i32, i32* %13, align 4
  %419 = sub i32 %418, %417
  store i32 %419, i32* %13, align 4
  %420 = load i32, i32* %12, align 4
  %421 = shl i32 %420, 10
  %422 = load i32, i32* %13, align 4
  %423 = xor i32 %422, %421
  store i32 %423, i32* %13, align 4
  %424 = load i32, i32* %12, align 4
  %425 = load i32, i32* %11, align 4
  %426 = sub i32 %425, %424
  store i32 %426, i32* %11, align 4
  %427 = load i32, i32* %13, align 4
  %428 = load i32, i32* %11, align 4
  %429 = sub i32 %428, %427
  store i32 %429, i32* %11, align 4
  %430 = load i32, i32* %13, align 4
  %431 = lshr i32 %430, 15
  %432 = load i32, i32* %11, align 4
  %433 = xor i32 %432, %431
  store i32 %433, i32* %11, align 4
  br label %434

; <label>:434:                                    ; preds = %343
  br label %435

; <label>:435:                                    ; preds = %434
  br label %436

; <label>:436:                                    ; preds = %435
  br label %437

; <label>:437:                                    ; preds = %436
  store %struct.my_hash* null, %struct.my_hash** %4, align 8
  %438 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %439 = icmp ne %struct.my_hash* %438, null
  br i1 %439, label %440, label %545

; <label>:440:                                    ; preds = %437
  br label %441

; <label>:441:                                    ; preds = %440
  %442 = load i32, i32* %11, align 4
  %443 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %444 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %443, i32 0, i32 2
  %445 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %444, i32 0, i32 0
  %446 = load %struct.UT_hash_table*, %struct.UT_hash_table** %445, align 8
  %447 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %446, i32 0, i32 1
  %448 = load i32, i32* %447, align 8
  %449 = sub i32 %448, 1
  %450 = and i32 %442, %449
  store i32 %450, i32* %16, align 4
  br label %451

; <label>:451:                                    ; preds = %441
  br label %452

; <label>:452:                                    ; preds = %451
  %453 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %454 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %453, i32 0, i32 2
  %455 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %454, i32 0, i32 0
  %456 = load %struct.UT_hash_table*, %struct.UT_hash_table** %455, align 8
  %457 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %456, i32 0, i32 0
  %458 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %457, align 8
  %459 = load i32, i32* %16, align 4
  %460 = zext i32 %459 to i64
  %461 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %458, i64 %460
  %462 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %461, i32 0, i32 0
  %463 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %462, align 8
  %464 = icmp ne %struct.UT_hash_handle* %463, null
  br i1 %464, label %465, label %489

; <label>:465:                                    ; preds = %452
  br label %466

; <label>:466:                                    ; preds = %465
  %467 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %468 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %467, i32 0, i32 2
  %469 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %468, i32 0, i32 0
  %470 = load %struct.UT_hash_table*, %struct.UT_hash_table** %469, align 8
  %471 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %470, i32 0, i32 0
  %472 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %471, align 8
  %473 = load i32, i32* %16, align 4
  %474 = zext i32 %473 to i64
  %475 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %472, i64 %474
  %476 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %475, i32 0, i32 0
  %477 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %476, align 8
  %478 = bitcast %struct.UT_hash_handle* %477 to i8*
  %479 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %480 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %479, i32 0, i32 2
  %481 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %480, i32 0, i32 0
  %482 = load %struct.UT_hash_table*, %struct.UT_hash_table** %481, align 8
  %483 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %482, i32 0, i32 5
  %484 = load i64, i64* %483, align 8
  %485 = sub i64 0, %484
  %486 = getelementptr inbounds i8, i8* %478, i64 %485
  %487 = bitcast i8* %486 to %struct.my_hash*
  store %struct.my_hash* %487, %struct.my_hash** %4, align 8
  br label %488

; <label>:488:                                    ; preds = %466
  br label %490

; <label>:489:                                    ; preds = %452
  store %struct.my_hash* null, %struct.my_hash** %4, align 8
  br label %490

; <label>:490:                                    ; preds = %489, %488
  br label %491

; <label>:491:                                    ; preds = %542, %490
  %492 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %493 = icmp ne %struct.my_hash* %492, null
  br i1 %493, label %494, label %543

; <label>:494:                                    ; preds = %491
  %495 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %496 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %495, i32 0, i32 2
  %497 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %496, i32 0, i32 7
  %498 = load i32, i32* %497, align 4
  %499 = load i32, i32* %11, align 4
  %500 = icmp eq i32 %498, %499
  br i1 %500, label %501, label %518

; <label>:501:                                    ; preds = %494
  %502 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %503 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %502, i32 0, i32 2
  %504 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %503, i32 0, i32 6
  %505 = load i32, i32* %504, align 8
  %506 = zext i32 %505 to i64
  %507 = icmp eq i64 %506, 8
  br i1 %507, label %508, label %518

; <label>:508:                                    ; preds = %501
  %509 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %510 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %509, i32 0, i32 2
  %511 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %510, i32 0, i32 5
  %512 = load i8*, i8** %511, align 8
  %513 = bitcast i64* %10 to i8*
  %514 = call i32 @memcmp(i8* %512, i8* %513, i64 8)
  %515 = icmp eq i32 %514, 0
  br i1 %515, label %516, label %517

; <label>:516:                                    ; preds = %508
  br label %543

; <label>:517:                                    ; preds = %508
  br label %518

; <label>:518:                                    ; preds = %517, %501, %494
  %519 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %520 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %519, i32 0, i32 2
  %521 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %520, i32 0, i32 4
  %522 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %521, align 8
  %523 = icmp ne %struct.UT_hash_handle* %522, null
  br i1 %523, label %524, label %541

; <label>:524:                                    ; preds = %518
  br label %525

; <label>:525:                                    ; preds = %524
  %526 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %527 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %526, i32 0, i32 2
  %528 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %527, i32 0, i32 4
  %529 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %528, align 8
  %530 = bitcast %struct.UT_hash_handle* %529 to i8*
  %531 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %532 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %531, i32 0, i32 2
  %533 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %532, i32 0, i32 0
  %534 = load %struct.UT_hash_table*, %struct.UT_hash_table** %533, align 8
  %535 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %534, i32 0, i32 5
  %536 = load i64, i64* %535, align 8
  %537 = sub i64 0, %536
  %538 = getelementptr inbounds i8, i8* %530, i64 %537
  %539 = bitcast i8* %538 to %struct.my_hash*
  store %struct.my_hash* %539, %struct.my_hash** %4, align 8
  br label %540

; <label>:540:                                    ; preds = %525
  br label %542

; <label>:541:                                    ; preds = %518
  store %struct.my_hash* null, %struct.my_hash** %4, align 8
  br label %542

; <label>:542:                                    ; preds = %541, %540
  br label %491

; <label>:543:                                    ; preds = %516, %491
  br label %544

; <label>:544:                                    ; preds = %543
  br label %545

; <label>:545:                                    ; preds = %544, %437
  br label %546

; <label>:546:                                    ; preds = %545
  br label %547

; <label>:547:                                    ; preds = %546
  %548 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %549 = icmp eq %struct.my_hash* %548, null
  br i1 %549, label %550, label %1409

; <label>:550:                                    ; preds = %547
  %551 = load i64, i64* @MEMCOUNT, align 8
  %552 = add i64 %551, 72
  store i64 %552, i64* @MEMCOUNT, align 8
  %553 = load i64, i64* @MEMCOUNT, align 8
  %554 = load i64, i64* @MEMLIMIT, align 8
  %555 = icmp uge i64 %553, %554
  br i1 %555, label %556, label %557

; <label>:556:                                    ; preds = %550
  call void @fatal_err(i8* getelementptr inbounds ([46 x i8], [46 x i8]* @.str.3, i32 0, i32 0))
  br label %557

; <label>:557:                                    ; preds = %556, %550
  %558 = call i8* @malloc(i64 72) #8
  %559 = bitcast i8* %558 to %struct.my_hash*
  store %struct.my_hash* %559, %struct.my_hash** %4, align 8
  %560 = load i64, i64* %10, align 8
  %561 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %562 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %561, i32 0, i32 0
  store i64 %560, i64* %562, align 8
  %563 = load i64, i64* %9, align 8
  %564 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %565 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %564, i32 0, i32 1
  store i64 %563, i64* %565, align 8
  br label %566

; <label>:566:                                    ; preds = %557
  br label %567

; <label>:567:                                    ; preds = %566
  br label %568

; <label>:568:                                    ; preds = %567
  %569 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %570 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %569, i32 0, i32 0
  %571 = bitcast i64* %570 to i8*
  store i8* %571, i8** %21, align 8
  store i32 -17973521, i32* %17, align 4
  store i32 -1640531527, i32* %19, align 4
  store i32 -1640531527, i32* %18, align 4
  store i32 8, i32* %20, align 4
  br label %572

; <label>:572:                                    ; preds = %739, %568
  %573 = load i32, i32* %20, align 4
  %574 = icmp uge i32 %573, 12
  br i1 %574, label %575, label %744

; <label>:575:                                    ; preds = %572
  %576 = load i8*, i8** %21, align 8
  %577 = getelementptr inbounds i8, i8* %576, i64 0
  %578 = load i8, i8* %577, align 1
  %579 = zext i8 %578 to i32
  %580 = load i8*, i8** %21, align 8
  %581 = getelementptr inbounds i8, i8* %580, i64 1
  %582 = load i8, i8* %581, align 1
  %583 = zext i8 %582 to i32
  %584 = shl i32 %583, 8
  %585 = add i32 %579, %584
  %586 = load i8*, i8** %21, align 8
  %587 = getelementptr inbounds i8, i8* %586, i64 2
  %588 = load i8, i8* %587, align 1
  %589 = zext i8 %588 to i32
  %590 = shl i32 %589, 16
  %591 = add i32 %585, %590
  %592 = load i8*, i8** %21, align 8
  %593 = getelementptr inbounds i8, i8* %592, i64 3
  %594 = load i8, i8* %593, align 1
  %595 = zext i8 %594 to i32
  %596 = shl i32 %595, 24
  %597 = add i32 %591, %596
  %598 = load i32, i32* %18, align 4
  %599 = add i32 %598, %597
  store i32 %599, i32* %18, align 4
  %600 = load i8*, i8** %21, align 8
  %601 = getelementptr inbounds i8, i8* %600, i64 4
  %602 = load i8, i8* %601, align 1
  %603 = zext i8 %602 to i32
  %604 = load i8*, i8** %21, align 8
  %605 = getelementptr inbounds i8, i8* %604, i64 5
  %606 = load i8, i8* %605, align 1
  %607 = zext i8 %606 to i32
  %608 = shl i32 %607, 8
  %609 = add i32 %603, %608
  %610 = load i8*, i8** %21, align 8
  %611 = getelementptr inbounds i8, i8* %610, i64 6
  %612 = load i8, i8* %611, align 1
  %613 = zext i8 %612 to i32
  %614 = shl i32 %613, 16
  %615 = add i32 %609, %614
  %616 = load i8*, i8** %21, align 8
  %617 = getelementptr inbounds i8, i8* %616, i64 7
  %618 = load i8, i8* %617, align 1
  %619 = zext i8 %618 to i32
  %620 = shl i32 %619, 24
  %621 = add i32 %615, %620
  %622 = load i32, i32* %19, align 4
  %623 = add i32 %622, %621
  store i32 %623, i32* %19, align 4
  %624 = load i8*, i8** %21, align 8
  %625 = getelementptr inbounds i8, i8* %624, i64 8
  %626 = load i8, i8* %625, align 1
  %627 = zext i8 %626 to i32
  %628 = load i8*, i8** %21, align 8
  %629 = getelementptr inbounds i8, i8* %628, i64 9
  %630 = load i8, i8* %629, align 1
  %631 = zext i8 %630 to i32
  %632 = shl i32 %631, 8
  %633 = add i32 %627, %632
  %634 = load i8*, i8** %21, align 8
  %635 = getelementptr inbounds i8, i8* %634, i64 10
  %636 = load i8, i8* %635, align 1
  %637 = zext i8 %636 to i32
  %638 = shl i32 %637, 16
  %639 = add i32 %633, %638
  %640 = load i8*, i8** %21, align 8
  %641 = getelementptr inbounds i8, i8* %640, i64 11
  %642 = load i8, i8* %641, align 1
  %643 = zext i8 %642 to i32
  %644 = shl i32 %643, 24
  %645 = add i32 %639, %644
  %646 = load i32, i32* %17, align 4
  %647 = add i32 %646, %645
  store i32 %647, i32* %17, align 4
  br label %648

; <label>:648:                                    ; preds = %575
  %649 = load i32, i32* %19, align 4
  %650 = load i32, i32* %18, align 4
  %651 = sub i32 %650, %649
  store i32 %651, i32* %18, align 4
  %652 = load i32, i32* %17, align 4
  %653 = load i32, i32* %18, align 4
  %654 = sub i32 %653, %652
  store i32 %654, i32* %18, align 4
  %655 = load i32, i32* %17, align 4
  %656 = lshr i32 %655, 13
  %657 = load i32, i32* %18, align 4
  %658 = xor i32 %657, %656
  store i32 %658, i32* %18, align 4
  %659 = load i32, i32* %17, align 4
  %660 = load i32, i32* %19, align 4
  %661 = sub i32 %660, %659
  store i32 %661, i32* %19, align 4
  %662 = load i32, i32* %18, align 4
  %663 = load i32, i32* %19, align 4
  %664 = sub i32 %663, %662
  store i32 %664, i32* %19, align 4
  %665 = load i32, i32* %18, align 4
  %666 = shl i32 %665, 8
  %667 = load i32, i32* %19, align 4
  %668 = xor i32 %667, %666
  store i32 %668, i32* %19, align 4
  %669 = load i32, i32* %18, align 4
  %670 = load i32, i32* %17, align 4
  %671 = sub i32 %670, %669
  store i32 %671, i32* %17, align 4
  %672 = load i32, i32* %19, align 4
  %673 = load i32, i32* %17, align 4
  %674 = sub i32 %673, %672
  store i32 %674, i32* %17, align 4
  %675 = load i32, i32* %19, align 4
  %676 = lshr i32 %675, 13
  %677 = load i32, i32* %17, align 4
  %678 = xor i32 %677, %676
  store i32 %678, i32* %17, align 4
  %679 = load i32, i32* %19, align 4
  %680 = load i32, i32* %18, align 4
  %681 = sub i32 %680, %679
  store i32 %681, i32* %18, align 4
  %682 = load i32, i32* %17, align 4
  %683 = load i32, i32* %18, align 4
  %684 = sub i32 %683, %682
  store i32 %684, i32* %18, align 4
  %685 = load i32, i32* %17, align 4
  %686 = lshr i32 %685, 12
  %687 = load i32, i32* %18, align 4
  %688 = xor i32 %687, %686
  store i32 %688, i32* %18, align 4
  %689 = load i32, i32* %17, align 4
  %690 = load i32, i32* %19, align 4
  %691 = sub i32 %690, %689
  store i32 %691, i32* %19, align 4
  %692 = load i32, i32* %18, align 4
  %693 = load i32, i32* %19, align 4
  %694 = sub i32 %693, %692
  store i32 %694, i32* %19, align 4
  %695 = load i32, i32* %18, align 4
  %696 = shl i32 %695, 16
  %697 = load i32, i32* %19, align 4
  %698 = xor i32 %697, %696
  store i32 %698, i32* %19, align 4
  %699 = load i32, i32* %18, align 4
  %700 = load i32, i32* %17, align 4
  %701 = sub i32 %700, %699
  store i32 %701, i32* %17, align 4
  %702 = load i32, i32* %19, align 4
  %703 = load i32, i32* %17, align 4
  %704 = sub i32 %703, %702
  store i32 %704, i32* %17, align 4
  %705 = load i32, i32* %19, align 4
  %706 = lshr i32 %705, 5
  %707 = load i32, i32* %17, align 4
  %708 = xor i32 %707, %706
  store i32 %708, i32* %17, align 4
  %709 = load i32, i32* %19, align 4
  %710 = load i32, i32* %18, align 4
  %711 = sub i32 %710, %709
  store i32 %711, i32* %18, align 4
  %712 = load i32, i32* %17, align 4
  %713 = load i32, i32* %18, align 4
  %714 = sub i32 %713, %712
  store i32 %714, i32* %18, align 4
  %715 = load i32, i32* %17, align 4
  %716 = lshr i32 %715, 3
  %717 = load i32, i32* %18, align 4
  %718 = xor i32 %717, %716
  store i32 %718, i32* %18, align 4
  %719 = load i32, i32* %17, align 4
  %720 = load i32, i32* %19, align 4
  %721 = sub i32 %720, %719
  store i32 %721, i32* %19, align 4
  %722 = load i32, i32* %18, align 4
  %723 = load i32, i32* %19, align 4
  %724 = sub i32 %723, %722
  store i32 %724, i32* %19, align 4
  %725 = load i32, i32* %18, align 4
  %726 = shl i32 %725, 10
  %727 = load i32, i32* %19, align 4
  %728 = xor i32 %727, %726
  store i32 %728, i32* %19, align 4
  %729 = load i32, i32* %18, align 4
  %730 = load i32, i32* %17, align 4
  %731 = sub i32 %730, %729
  store i32 %731, i32* %17, align 4
  %732 = load i32, i32* %19, align 4
  %733 = load i32, i32* %17, align 4
  %734 = sub i32 %733, %732
  store i32 %734, i32* %17, align 4
  %735 = load i32, i32* %19, align 4
  %736 = lshr i32 %735, 15
  %737 = load i32, i32* %17, align 4
  %738 = xor i32 %737, %736
  store i32 %738, i32* %17, align 4
  br label %739

; <label>:739:                                    ; preds = %648
  %740 = load i8*, i8** %21, align 8
  %741 = getelementptr inbounds i8, i8* %740, i64 12
  store i8* %741, i8** %21, align 8
  %742 = load i32, i32* %20, align 4
  %743 = sub i32 %742, 12
  store i32 %743, i32* %20, align 4
  br label %572

; <label>:744:                                    ; preds = %572
  %745 = load i32, i32* %17, align 4
  %746 = add i32 %745, 8
  store i32 %746, i32* %17, align 4
  %747 = load i32, i32* %20, align 4
  switch i32 %747, label %834 [
    i32 11, label %748
    i32 10, label %756
    i32 9, label %764
    i32 8, label %772
    i32 7, label %780
    i32 6, label %788
    i32 5, label %796
    i32 4, label %803
    i32 3, label %811
    i32 2, label %819
    i32 1, label %827
  ]

; <label>:748:                                    ; preds = %744
  %749 = load i8*, i8** %21, align 8
  %750 = getelementptr inbounds i8, i8* %749, i64 10
  %751 = load i8, i8* %750, align 1
  %752 = zext i8 %751 to i32
  %753 = shl i32 %752, 24
  %754 = load i32, i32* %17, align 4
  %755 = add i32 %754, %753
  store i32 %755, i32* %17, align 4
  br label %756

; <label>:756:                                    ; preds = %744, %748
  %757 = load i8*, i8** %21, align 8
  %758 = getelementptr inbounds i8, i8* %757, i64 9
  %759 = load i8, i8* %758, align 1
  %760 = zext i8 %759 to i32
  %761 = shl i32 %760, 16
  %762 = load i32, i32* %17, align 4
  %763 = add i32 %762, %761
  store i32 %763, i32* %17, align 4
  br label %764

; <label>:764:                                    ; preds = %744, %756
  %765 = load i8*, i8** %21, align 8
  %766 = getelementptr inbounds i8, i8* %765, i64 8
  %767 = load i8, i8* %766, align 1
  %768 = zext i8 %767 to i32
  %769 = shl i32 %768, 8
  %770 = load i32, i32* %17, align 4
  %771 = add i32 %770, %769
  store i32 %771, i32* %17, align 4
  br label %772

; <label>:772:                                    ; preds = %744, %764
  %773 = load i8*, i8** %21, align 8
  %774 = getelementptr inbounds i8, i8* %773, i64 7
  %775 = load i8, i8* %774, align 1
  %776 = zext i8 %775 to i32
  %777 = shl i32 %776, 24
  %778 = load i32, i32* %19, align 4
  %779 = add i32 %778, %777
  store i32 %779, i32* %19, align 4
  br label %780

; <label>:780:                                    ; preds = %744, %772
  %781 = load i8*, i8** %21, align 8
  %782 = getelementptr inbounds i8, i8* %781, i64 6
  %783 = load i8, i8* %782, align 1
  %784 = zext i8 %783 to i32
  %785 = shl i32 %784, 16
  %786 = load i32, i32* %19, align 4
  %787 = add i32 %786, %785
  store i32 %787, i32* %19, align 4
  br label %788

; <label>:788:                                    ; preds = %744, %780
  %789 = load i8*, i8** %21, align 8
  %790 = getelementptr inbounds i8, i8* %789, i64 5
  %791 = load i8, i8* %790, align 1
  %792 = zext i8 %791 to i32
  %793 = shl i32 %792, 8
  %794 = load i32, i32* %19, align 4
  %795 = add i32 %794, %793
  store i32 %795, i32* %19, align 4
  br label %796

; <label>:796:                                    ; preds = %744, %788
  %797 = load i8*, i8** %21, align 8
  %798 = getelementptr inbounds i8, i8* %797, i64 4
  %799 = load i8, i8* %798, align 1
  %800 = zext i8 %799 to i32
  %801 = load i32, i32* %19, align 4
  %802 = add i32 %801, %800
  store i32 %802, i32* %19, align 4
  br label %803

; <label>:803:                                    ; preds = %744, %796
  %804 = load i8*, i8** %21, align 8
  %805 = getelementptr inbounds i8, i8* %804, i64 3
  %806 = load i8, i8* %805, align 1
  %807 = zext i8 %806 to i32
  %808 = shl i32 %807, 24
  %809 = load i32, i32* %18, align 4
  %810 = add i32 %809, %808
  store i32 %810, i32* %18, align 4
  br label %811

; <label>:811:                                    ; preds = %744, %803
  %812 = load i8*, i8** %21, align 8
  %813 = getelementptr inbounds i8, i8* %812, i64 2
  %814 = load i8, i8* %813, align 1
  %815 = zext i8 %814 to i32
  %816 = shl i32 %815, 16
  %817 = load i32, i32* %18, align 4
  %818 = add i32 %817, %816
  store i32 %818, i32* %18, align 4
  br label %819

; <label>:819:                                    ; preds = %744, %811
  %820 = load i8*, i8** %21, align 8
  %821 = getelementptr inbounds i8, i8* %820, i64 1
  %822 = load i8, i8* %821, align 1
  %823 = zext i8 %822 to i32
  %824 = shl i32 %823, 8
  %825 = load i32, i32* %18, align 4
  %826 = add i32 %825, %824
  store i32 %826, i32* %18, align 4
  br label %827

; <label>:827:                                    ; preds = %744, %819
  %828 = load i8*, i8** %21, align 8
  %829 = getelementptr inbounds i8, i8* %828, i64 0
  %830 = load i8, i8* %829, align 1
  %831 = zext i8 %830 to i32
  %832 = load i32, i32* %18, align 4
  %833 = add i32 %832, %831
  store i32 %833, i32* %18, align 4
  br label %834

; <label>:834:                                    ; preds = %827, %744
  br label %835

; <label>:835:                                    ; preds = %834
  %836 = load i32, i32* %19, align 4
  %837 = load i32, i32* %18, align 4
  %838 = sub i32 %837, %836
  store i32 %838, i32* %18, align 4
  %839 = load i32, i32* %17, align 4
  %840 = load i32, i32* %18, align 4
  %841 = sub i32 %840, %839
  store i32 %841, i32* %18, align 4
  %842 = load i32, i32* %17, align 4
  %843 = lshr i32 %842, 13
  %844 = load i32, i32* %18, align 4
  %845 = xor i32 %844, %843
  store i32 %845, i32* %18, align 4
  %846 = load i32, i32* %17, align 4
  %847 = load i32, i32* %19, align 4
  %848 = sub i32 %847, %846
  store i32 %848, i32* %19, align 4
  %849 = load i32, i32* %18, align 4
  %850 = load i32, i32* %19, align 4
  %851 = sub i32 %850, %849
  store i32 %851, i32* %19, align 4
  %852 = load i32, i32* %18, align 4
  %853 = shl i32 %852, 8
  %854 = load i32, i32* %19, align 4
  %855 = xor i32 %854, %853
  store i32 %855, i32* %19, align 4
  %856 = load i32, i32* %18, align 4
  %857 = load i32, i32* %17, align 4
  %858 = sub i32 %857, %856
  store i32 %858, i32* %17, align 4
  %859 = load i32, i32* %19, align 4
  %860 = load i32, i32* %17, align 4
  %861 = sub i32 %860, %859
  store i32 %861, i32* %17, align 4
  %862 = load i32, i32* %19, align 4
  %863 = lshr i32 %862, 13
  %864 = load i32, i32* %17, align 4
  %865 = xor i32 %864, %863
  store i32 %865, i32* %17, align 4
  %866 = load i32, i32* %19, align 4
  %867 = load i32, i32* %18, align 4
  %868 = sub i32 %867, %866
  store i32 %868, i32* %18, align 4
  %869 = load i32, i32* %17, align 4
  %870 = load i32, i32* %18, align 4
  %871 = sub i32 %870, %869
  store i32 %871, i32* %18, align 4
  %872 = load i32, i32* %17, align 4
  %873 = lshr i32 %872, 12
  %874 = load i32, i32* %18, align 4
  %875 = xor i32 %874, %873
  store i32 %875, i32* %18, align 4
  %876 = load i32, i32* %17, align 4
  %877 = load i32, i32* %19, align 4
  %878 = sub i32 %877, %876
  store i32 %878, i32* %19, align 4
  %879 = load i32, i32* %18, align 4
  %880 = load i32, i32* %19, align 4
  %881 = sub i32 %880, %879
  store i32 %881, i32* %19, align 4
  %882 = load i32, i32* %18, align 4
  %883 = shl i32 %882, 16
  %884 = load i32, i32* %19, align 4
  %885 = xor i32 %884, %883
  store i32 %885, i32* %19, align 4
  %886 = load i32, i32* %18, align 4
  %887 = load i32, i32* %17, align 4
  %888 = sub i32 %887, %886
  store i32 %888, i32* %17, align 4
  %889 = load i32, i32* %19, align 4
  %890 = load i32, i32* %17, align 4
  %891 = sub i32 %890, %889
  store i32 %891, i32* %17, align 4
  %892 = load i32, i32* %19, align 4
  %893 = lshr i32 %892, 5
  %894 = load i32, i32* %17, align 4
  %895 = xor i32 %894, %893
  store i32 %895, i32* %17, align 4
  %896 = load i32, i32* %19, align 4
  %897 = load i32, i32* %18, align 4
  %898 = sub i32 %897, %896
  store i32 %898, i32* %18, align 4
  %899 = load i32, i32* %17, align 4
  %900 = load i32, i32* %18, align 4
  %901 = sub i32 %900, %899
  store i32 %901, i32* %18, align 4
  %902 = load i32, i32* %17, align 4
  %903 = lshr i32 %902, 3
  %904 = load i32, i32* %18, align 4
  %905 = xor i32 %904, %903
  store i32 %905, i32* %18, align 4
  %906 = load i32, i32* %17, align 4
  %907 = load i32, i32* %19, align 4
  %908 = sub i32 %907, %906
  store i32 %908, i32* %19, align 4
  %909 = load i32, i32* %18, align 4
  %910 = load i32, i32* %19, align 4
  %911 = sub i32 %910, %909
  store i32 %911, i32* %19, align 4
  %912 = load i32, i32* %18, align 4
  %913 = shl i32 %912, 10
  %914 = load i32, i32* %19, align 4
  %915 = xor i32 %914, %913
  store i32 %915, i32* %19, align 4
  %916 = load i32, i32* %18, align 4
  %917 = load i32, i32* %17, align 4
  %918 = sub i32 %917, %916
  store i32 %918, i32* %17, align 4
  %919 = load i32, i32* %19, align 4
  %920 = load i32, i32* %17, align 4
  %921 = sub i32 %920, %919
  store i32 %921, i32* %17, align 4
  %922 = load i32, i32* %19, align 4
  %923 = lshr i32 %922, 15
  %924 = load i32, i32* %17, align 4
  %925 = xor i32 %924, %923
  store i32 %925, i32* %17, align 4
  br label %926

; <label>:926:                                    ; preds = %835
  br label %927

; <label>:927:                                    ; preds = %926
  br label %928

; <label>:928:                                    ; preds = %927
  br label %929

; <label>:929:                                    ; preds = %928
  %930 = load i32, i32* %17, align 4
  %931 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %932 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %931, i32 0, i32 2
  %933 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %932, i32 0, i32 7
  store i32 %930, i32* %933, align 4
  %934 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %935 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %934, i32 0, i32 0
  %936 = bitcast i64* %935 to i8*
  %937 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %938 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %937, i32 0, i32 2
  %939 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %938, i32 0, i32 5
  store i8* %936, i8** %939, align 8
  %940 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %941 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %940, i32 0, i32 2
  %942 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %941, i32 0, i32 6
  store i32 8, i32* %942, align 8
  %943 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %944 = icmp ne %struct.my_hash* %943, null
  br i1 %944, label %1032, label %945

; <label>:945:                                    ; preds = %929
  %946 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %947 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %946, i32 0, i32 2
  %948 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %947, i32 0, i32 2
  store i8* null, i8** %948, align 8
  %949 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %950 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %949, i32 0, i32 2
  %951 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %950, i32 0, i32 1
  store i8* null, i8** %951, align 8
  br label %952

; <label>:952:                                    ; preds = %945
  %953 = call i8* @malloc(i64 64) #8
  %954 = bitcast i8* %953 to %struct.UT_hash_table*
  %955 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %956 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %955, i32 0, i32 2
  %957 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %956, i32 0, i32 0
  store %struct.UT_hash_table* %954, %struct.UT_hash_table** %957, align 8
  %958 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %959 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %958, i32 0, i32 2
  %960 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %959, i32 0, i32 0
  %961 = load %struct.UT_hash_table*, %struct.UT_hash_table** %960, align 8
  %962 = icmp ne %struct.UT_hash_table* %961, null
  br i1 %962, label %964, label %963

; <label>:963:                                    ; preds = %952
  call void @exit(i32 -1) #7
  unreachable

; <label>:964:                                    ; preds = %952
  %965 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %966 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %965, i32 0, i32 2
  %967 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %966, i32 0, i32 0
  %968 = load %struct.UT_hash_table*, %struct.UT_hash_table** %967, align 8
  %969 = bitcast %struct.UT_hash_table* %968 to i8*
  call void @llvm.memset.p0i8.i64(i8* %969, i8 0, i64 64, i32 8, i1 false)
  %970 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %971 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %970, i32 0, i32 2
  %972 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %973 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %972, i32 0, i32 2
  %974 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %973, i32 0, i32 0
  %975 = load %struct.UT_hash_table*, %struct.UT_hash_table** %974, align 8
  %976 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %975, i32 0, i32 4
  store %struct.UT_hash_handle* %971, %struct.UT_hash_handle** %976, align 8
  %977 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %978 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %977, i32 0, i32 2
  %979 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %978, i32 0, i32 0
  %980 = load %struct.UT_hash_table*, %struct.UT_hash_table** %979, align 8
  %981 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %980, i32 0, i32 1
  store i32 32, i32* %981, align 8
  %982 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %983 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %982, i32 0, i32 2
  %984 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %983, i32 0, i32 0
  %985 = load %struct.UT_hash_table*, %struct.UT_hash_table** %984, align 8
  %986 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %985, i32 0, i32 2
  store i32 5, i32* %986, align 4
  %987 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %988 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %987, i32 0, i32 2
  %989 = bitcast %struct.UT_hash_handle* %988 to i8*
  %990 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %991 = bitcast %struct.my_hash* %990 to i8*
  %992 = ptrtoint i8* %989 to i64
  %993 = ptrtoint i8* %991 to i64
  %994 = sub i64 %992, %993
  %995 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %996 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %995, i32 0, i32 2
  %997 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %996, i32 0, i32 0
  %998 = load %struct.UT_hash_table*, %struct.UT_hash_table** %997, align 8
  %999 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %998, i32 0, i32 5
  store i64 %994, i64* %999, align 8
  %1000 = call i8* @malloc(i64 512) #8
  %1001 = bitcast i8* %1000 to %struct.UT_hash_bucket*
  %1002 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1003 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1002, i32 0, i32 2
  %1004 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1003, i32 0, i32 0
  %1005 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1004, align 8
  %1006 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1005, i32 0, i32 0
  store %struct.UT_hash_bucket* %1001, %struct.UT_hash_bucket** %1006, align 8
  %1007 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1008 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1007, i32 0, i32 2
  %1009 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1008, i32 0, i32 0
  %1010 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1009, align 8
  %1011 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1010, i32 0, i32 10
  store i32 -1609490463, i32* %1011, align 8
  %1012 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1013 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1012, i32 0, i32 2
  %1014 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1013, i32 0, i32 0
  %1015 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1014, align 8
  %1016 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1015, i32 0, i32 0
  %1017 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1016, align 8
  %1018 = icmp ne %struct.UT_hash_bucket* %1017, null
  br i1 %1018, label %1020, label %1019

; <label>:1019:                                   ; preds = %964
  call void @exit(i32 -1) #7
  unreachable

; <label>:1020:                                   ; preds = %964
  %1021 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1022 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1021, i32 0, i32 2
  %1023 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1022, i32 0, i32 0
  %1024 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1023, align 8
  %1025 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1024, i32 0, i32 0
  %1026 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1025, align 8
  %1027 = bitcast %struct.UT_hash_bucket* %1026 to i8*
  call void @llvm.memset.p0i8.i64(i8* %1027, i8 0, i64 512, i32 8, i1 false)
  br label %1028

; <label>:1028:                                   ; preds = %1020
  br label %1029

; <label>:1029:                                   ; preds = %1028
  br label %1030

; <label>:1030:                                   ; preds = %1029
  %1031 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  store %struct.my_hash* %1031, %struct.my_hash** %3, align 8
  br label %1079

; <label>:1032:                                   ; preds = %929
  %1033 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1034 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1033, i32 0, i32 2
  %1035 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1034, i32 0, i32 0
  %1036 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1035, align 8
  %1037 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1038 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1037, i32 0, i32 2
  %1039 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1038, i32 0, i32 0
  store %struct.UT_hash_table* %1036, %struct.UT_hash_table** %1039, align 8
  br label %1040

; <label>:1040:                                   ; preds = %1032
  %1041 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1042 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1041, i32 0, i32 2
  %1043 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1042, i32 0, i32 2
  store i8* null, i8** %1043, align 8
  %1044 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1045 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1044, i32 0, i32 2
  %1046 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1045, i32 0, i32 0
  %1047 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1046, align 8
  %1048 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1047, i32 0, i32 4
  %1049 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1048, align 8
  %1050 = bitcast %struct.UT_hash_handle* %1049 to i8*
  %1051 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1052 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1051, i32 0, i32 2
  %1053 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1052, i32 0, i32 0
  %1054 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1053, align 8
  %1055 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1054, i32 0, i32 5
  %1056 = load i64, i64* %1055, align 8
  %1057 = sub i64 0, %1056
  %1058 = getelementptr inbounds i8, i8* %1050, i64 %1057
  %1059 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1060 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1059, i32 0, i32 2
  %1061 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1060, i32 0, i32 1
  store i8* %1058, i8** %1061, align 8
  %1062 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1063 = bitcast %struct.my_hash* %1062 to i8*
  %1064 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1065 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1064, i32 0, i32 2
  %1066 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1065, i32 0, i32 0
  %1067 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1066, align 8
  %1068 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1067, i32 0, i32 4
  %1069 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1068, align 8
  %1070 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1069, i32 0, i32 2
  store i8* %1063, i8** %1070, align 8
  %1071 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1072 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1071, i32 0, i32 2
  %1073 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1074 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1073, i32 0, i32 2
  %1075 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1074, i32 0, i32 0
  %1076 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1075, align 8
  %1077 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1076, i32 0, i32 4
  store %struct.UT_hash_handle* %1072, %struct.UT_hash_handle** %1077, align 8
  br label %1078

; <label>:1078:                                   ; preds = %1040
  br label %1079

; <label>:1079:                                   ; preds = %1078, %1030
  br label %1080

; <label>:1080:                                   ; preds = %1079
  %1081 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1082 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1081, i32 0, i32 2
  %1083 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1082, i32 0, i32 0
  %1084 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1083, align 8
  %1085 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1084, i32 0, i32 3
  %1086 = load i32, i32* %1085, align 8
  %1087 = add i32 %1086, 1
  store i32 %1087, i32* %1085, align 8
  br label %1088

; <label>:1088:                                   ; preds = %1080
  %1089 = load i32, i32* %17, align 4
  %1090 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1091 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1090, i32 0, i32 2
  %1092 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1091, i32 0, i32 0
  %1093 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1092, align 8
  %1094 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1093, i32 0, i32 1
  %1095 = load i32, i32* %1094, align 8
  %1096 = sub i32 %1095, 1
  %1097 = and i32 %1089, %1096
  store i32 %1097, i32* %22, align 4
  br label %1098

; <label>:1098:                                   ; preds = %1088
  br label %1099

; <label>:1099:                                   ; preds = %1098
  %1100 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1101 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1100, i32 0, i32 2
  %1102 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1101, i32 0, i32 0
  %1103 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1102, align 8
  %1104 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1103, i32 0, i32 0
  %1105 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1104, align 8
  %1106 = load i32, i32* %22, align 4
  %1107 = zext i32 %1106 to i64
  %1108 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1105, i64 %1107
  store %struct.UT_hash_bucket* %1108, %struct.UT_hash_bucket** %23, align 8
  %1109 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %23, align 8
  %1110 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1109, i32 0, i32 1
  %1111 = load i32, i32* %1110, align 8
  %1112 = add i32 %1111, 1
  store i32 %1112, i32* %1110, align 8
  %1113 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %23, align 8
  %1114 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1113, i32 0, i32 0
  %1115 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1114, align 8
  %1116 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1117 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1116, i32 0, i32 2
  %1118 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1117, i32 0, i32 4
  store %struct.UT_hash_handle* %1115, %struct.UT_hash_handle** %1118, align 8
  %1119 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1120 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1119, i32 0, i32 2
  %1121 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1120, i32 0, i32 3
  store %struct.UT_hash_handle* null, %struct.UT_hash_handle** %1121, align 8
  %1122 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %23, align 8
  %1123 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1122, i32 0, i32 0
  %1124 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1123, align 8
  %1125 = icmp ne %struct.UT_hash_handle* %1124, null
  br i1 %1125, label %1126, label %1133

; <label>:1126:                                   ; preds = %1099
  %1127 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1128 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1127, i32 0, i32 2
  %1129 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %23, align 8
  %1130 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1129, i32 0, i32 0
  %1131 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1130, align 8
  %1132 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1131, i32 0, i32 3
  store %struct.UT_hash_handle* %1128, %struct.UT_hash_handle** %1132, align 8
  br label %1133

; <label>:1133:                                   ; preds = %1126, %1099
  %1134 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1135 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1134, i32 0, i32 2
  %1136 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %23, align 8
  %1137 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1136, i32 0, i32 0
  store %struct.UT_hash_handle* %1135, %struct.UT_hash_handle** %1137, align 8
  %1138 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %23, align 8
  %1139 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1138, i32 0, i32 1
  %1140 = load i32, i32* %1139, align 8
  %1141 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %23, align 8
  %1142 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1141, i32 0, i32 2
  %1143 = load i32, i32* %1142, align 4
  %1144 = add i32 %1143, 1
  %1145 = mul i32 %1144, 10
  %1146 = icmp uge i32 %1140, %1145
  br i1 %1146, label %1147, label %1404

; <label>:1147:                                   ; preds = %1133
  %1148 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1149 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1148, i32 0, i32 2
  %1150 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1149, i32 0, i32 0
  %1151 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1150, align 8
  %1152 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1151, i32 0, i32 9
  %1153 = load i32, i32* %1152, align 4
  %1154 = icmp ne i32 %1153, 0
  br i1 %1154, label %1404, label %1155

; <label>:1155:                                   ; preds = %1147
  br label %1156

; <label>:1156:                                   ; preds = %1155
  %1157 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1158 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1157, i32 0, i32 2
  %1159 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1158, i32 0, i32 0
  %1160 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1159, align 8
  %1161 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1160, i32 0, i32 1
  %1162 = load i32, i32* %1161, align 8
  %1163 = zext i32 %1162 to i64
  %1164 = mul i64 2, %1163
  %1165 = mul i64 %1164, 16
  %1166 = call i8* @malloc(i64 %1165) #8
  %1167 = bitcast i8* %1166 to %struct.UT_hash_bucket*
  store %struct.UT_hash_bucket* %1167, %struct.UT_hash_bucket** %28, align 8
  %1168 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1169 = icmp ne %struct.UT_hash_bucket* %1168, null
  br i1 %1169, label %1171, label %1170

; <label>:1170:                                   ; preds = %1156
  call void @exit(i32 -1) #7
  unreachable

; <label>:1171:                                   ; preds = %1156
  %1172 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1173 = bitcast %struct.UT_hash_bucket* %1172 to i8*
  %1174 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1175 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1174, i32 0, i32 2
  %1176 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1175, i32 0, i32 0
  %1177 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1176, align 8
  %1178 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1177, i32 0, i32 1
  %1179 = load i32, i32* %1178, align 8
  %1180 = zext i32 %1179 to i64
  %1181 = mul i64 2, %1180
  %1182 = mul i64 %1181, 16
  call void @llvm.memset.p0i8.i64(i8* %1173, i8 0, i64 %1182, i32 8, i1 false)
  %1183 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1184 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1183, i32 0, i32 2
  %1185 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1184, i32 0, i32 0
  %1186 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1185, align 8
  %1187 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1186, i32 0, i32 3
  %1188 = load i32, i32* %1187, align 8
  %1189 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1190 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1189, i32 0, i32 2
  %1191 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1190, i32 0, i32 0
  %1192 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1191, align 8
  %1193 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1192, i32 0, i32 2
  %1194 = load i32, i32* %1193, align 4
  %1195 = add i32 %1194, 1
  %1196 = lshr i32 %1188, %1195
  %1197 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1198 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1197, i32 0, i32 2
  %1199 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1198, i32 0, i32 0
  %1200 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1199, align 8
  %1201 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1200, i32 0, i32 3
  %1202 = load i32, i32* %1201, align 8
  %1203 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1204 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1203, i32 0, i32 2
  %1205 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1204, i32 0, i32 0
  %1206 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1205, align 8
  %1207 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1206, i32 0, i32 1
  %1208 = load i32, i32* %1207, align 8
  %1209 = mul i32 %1208, 2
  %1210 = sub i32 %1209, 1
  %1211 = and i32 %1202, %1210
  %1212 = icmp ne i32 %1211, 0
  %1213 = zext i1 %1212 to i64
  %1214 = select i1 %1212, i32 1, i32 0
  %1215 = add i32 %1196, %1214
  %1216 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1217 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1216, i32 0, i32 2
  %1218 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1217, i32 0, i32 0
  %1219 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1218, align 8
  %1220 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1219, i32 0, i32 6
  store i32 %1215, i32* %1220, align 8
  %1221 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1222 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1221, i32 0, i32 2
  %1223 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1222, i32 0, i32 0
  %1224 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1223, align 8
  %1225 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1224, i32 0, i32 7
  store i32 0, i32* %1225, align 4
  store i32 0, i32* %25, align 4
  br label %1226

; <label>:1226:                                   ; preds = %1327, %1171
  %1227 = load i32, i32* %25, align 4
  %1228 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1229 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1228, i32 0, i32 2
  %1230 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1229, i32 0, i32 0
  %1231 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1230, align 8
  %1232 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1231, i32 0, i32 1
  %1233 = load i32, i32* %1232, align 8
  %1234 = icmp ult i32 %1227, %1233
  br i1 %1234, label %1235, label %1330

; <label>:1235:                                   ; preds = %1226
  %1236 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1237 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1236, i32 0, i32 2
  %1238 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1237, i32 0, i32 0
  %1239 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1238, align 8
  %1240 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1239, i32 0, i32 0
  %1241 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1240, align 8
  %1242 = load i32, i32* %25, align 4
  %1243 = zext i32 %1242 to i64
  %1244 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1241, i64 %1243
  %1245 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1244, i32 0, i32 0
  %1246 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1245, align 8
  store %struct.UT_hash_handle* %1246, %struct.UT_hash_handle** %26, align 8
  br label %1247

; <label>:1247:                                   ; preds = %1321, %1235
  %1248 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  %1249 = icmp ne %struct.UT_hash_handle* %1248, null
  br i1 %1249, label %1250, label %1326

; <label>:1250:                                   ; preds = %1247
  %1251 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  %1252 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1251, i32 0, i32 4
  %1253 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1252, align 8
  store %struct.UT_hash_handle* %1253, %struct.UT_hash_handle** %27, align 8
  br label %1254

; <label>:1254:                                   ; preds = %1250
  %1255 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  %1256 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1255, i32 0, i32 7
  %1257 = load i32, i32* %1256, align 4
  %1258 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1259 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1258, i32 0, i32 2
  %1260 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1259, i32 0, i32 0
  %1261 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1260, align 8
  %1262 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1261, i32 0, i32 1
  %1263 = load i32, i32* %1262, align 8
  %1264 = mul i32 %1263, 2
  %1265 = sub i32 %1264, 1
  %1266 = and i32 %1257, %1265
  store i32 %1266, i32* %24, align 4
  br label %1267

; <label>:1267:                                   ; preds = %1254
  %1268 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1269 = load i32, i32* %24, align 4
  %1270 = zext i32 %1269 to i64
  %1271 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1268, i64 %1270
  store %struct.UT_hash_bucket* %1271, %struct.UT_hash_bucket** %29, align 8
  %1272 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %29, align 8
  %1273 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1272, i32 0, i32 1
  %1274 = load i32, i32* %1273, align 8
  %1275 = add i32 %1274, 1
  store i32 %1275, i32* %1273, align 8
  %1276 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1277 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1276, i32 0, i32 2
  %1278 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1277, i32 0, i32 0
  %1279 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1278, align 8
  %1280 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1279, i32 0, i32 6
  %1281 = load i32, i32* %1280, align 8
  %1282 = icmp ugt i32 %1275, %1281
  br i1 %1282, label %1283, label %1303

; <label>:1283:                                   ; preds = %1267
  %1284 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1285 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1284, i32 0, i32 2
  %1286 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1285, i32 0, i32 0
  %1287 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1286, align 8
  %1288 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1287, i32 0, i32 7
  %1289 = load i32, i32* %1288, align 4
  %1290 = add i32 %1289, 1
  store i32 %1290, i32* %1288, align 4
  %1291 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %29, align 8
  %1292 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1291, i32 0, i32 1
  %1293 = load i32, i32* %1292, align 8
  %1294 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1295 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1294, i32 0, i32 2
  %1296 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1295, i32 0, i32 0
  %1297 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1296, align 8
  %1298 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1297, i32 0, i32 6
  %1299 = load i32, i32* %1298, align 8
  %1300 = udiv i32 %1293, %1299
  %1301 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %29, align 8
  %1302 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1301, i32 0, i32 2
  store i32 %1300, i32* %1302, align 4
  br label %1303

; <label>:1303:                                   ; preds = %1283, %1267
  %1304 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  %1305 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1304, i32 0, i32 3
  store %struct.UT_hash_handle* null, %struct.UT_hash_handle** %1305, align 8
  %1306 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %29, align 8
  %1307 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1306, i32 0, i32 0
  %1308 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1307, align 8
  %1309 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  %1310 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1309, i32 0, i32 4
  store %struct.UT_hash_handle* %1308, %struct.UT_hash_handle** %1310, align 8
  %1311 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %29, align 8
  %1312 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1311, i32 0, i32 0
  %1313 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1312, align 8
  %1314 = icmp ne %struct.UT_hash_handle* %1313, null
  br i1 %1314, label %1315, label %1321

; <label>:1315:                                   ; preds = %1303
  %1316 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  %1317 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %29, align 8
  %1318 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1317, i32 0, i32 0
  %1319 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1318, align 8
  %1320 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1319, i32 0, i32 3
  store %struct.UT_hash_handle* %1316, %struct.UT_hash_handle** %1320, align 8
  br label %1321

; <label>:1321:                                   ; preds = %1315, %1303
  %1322 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  %1323 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %29, align 8
  %1324 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1323, i32 0, i32 0
  store %struct.UT_hash_handle* %1322, %struct.UT_hash_handle** %1324, align 8
  %1325 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %27, align 8
  store %struct.UT_hash_handle* %1325, %struct.UT_hash_handle** %26, align 8
  br label %1247

; <label>:1326:                                   ; preds = %1247
  br label %1327

; <label>:1327:                                   ; preds = %1326
  %1328 = load i32, i32* %25, align 4
  %1329 = add i32 %1328, 1
  store i32 %1329, i32* %25, align 4
  br label %1226

; <label>:1330:                                   ; preds = %1226
  %1331 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1332 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1331, i32 0, i32 2
  %1333 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1332, i32 0, i32 0
  %1334 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1333, align 8
  %1335 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1334, i32 0, i32 0
  %1336 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1335, align 8
  %1337 = bitcast %struct.UT_hash_bucket* %1336 to i8*
  call void @free(i8* %1337)
  %1338 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1339 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1338, i32 0, i32 2
  %1340 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1339, i32 0, i32 0
  %1341 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1340, align 8
  %1342 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1341, i32 0, i32 1
  %1343 = load i32, i32* %1342, align 8
  %1344 = mul i32 %1343, 2
  store i32 %1344, i32* %1342, align 8
  %1345 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1346 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1345, i32 0, i32 2
  %1347 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1346, i32 0, i32 0
  %1348 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1347, align 8
  %1349 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1348, i32 0, i32 2
  %1350 = load i32, i32* %1349, align 4
  %1351 = add i32 %1350, 1
  store i32 %1351, i32* %1349, align 4
  %1352 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1353 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1354 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1353, i32 0, i32 2
  %1355 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1354, i32 0, i32 0
  %1356 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1355, align 8
  %1357 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1356, i32 0, i32 0
  store %struct.UT_hash_bucket* %1352, %struct.UT_hash_bucket** %1357, align 8
  %1358 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1359 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1358, i32 0, i32 2
  %1360 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1359, i32 0, i32 0
  %1361 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1360, align 8
  %1362 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1361, i32 0, i32 7
  %1363 = load i32, i32* %1362, align 4
  %1364 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1365 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1364, i32 0, i32 2
  %1366 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1365, i32 0, i32 0
  %1367 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1366, align 8
  %1368 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1367, i32 0, i32 3
  %1369 = load i32, i32* %1368, align 8
  %1370 = lshr i32 %1369, 1
  %1371 = icmp ugt i32 %1363, %1370
  br i1 %1371, label %1372, label %1380

; <label>:1372:                                   ; preds = %1330
  %1373 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1374 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1373, i32 0, i32 2
  %1375 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1374, i32 0, i32 0
  %1376 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1375, align 8
  %1377 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1376, i32 0, i32 8
  %1378 = load i32, i32* %1377, align 8
  %1379 = add i32 %1378, 1
  br label %1381

; <label>:1380:                                   ; preds = %1330
  br label %1381

; <label>:1381:                                   ; preds = %1380, %1372
  %1382 = phi i32 [ %1379, %1372 ], [ 0, %1380 ]
  %1383 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1384 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1383, i32 0, i32 2
  %1385 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1384, i32 0, i32 0
  %1386 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1385, align 8
  %1387 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1386, i32 0, i32 8
  store i32 %1382, i32* %1387, align 8
  %1388 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1389 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1388, i32 0, i32 2
  %1390 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1389, i32 0, i32 0
  %1391 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1390, align 8
  %1392 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1391, i32 0, i32 8
  %1393 = load i32, i32* %1392, align 8
  %1394 = icmp ugt i32 %1393, 1
  br i1 %1394, label %1395, label %1401

; <label>:1395:                                   ; preds = %1381
  %1396 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1397 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1396, i32 0, i32 2
  %1398 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1397, i32 0, i32 0
  %1399 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1398, align 8
  %1400 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1399, i32 0, i32 9
  store i32 1, i32* %1400, align 4
  br label %1401

; <label>:1401:                                   ; preds = %1395, %1381
  br label %1402

; <label>:1402:                                   ; preds = %1401
  br label %1403

; <label>:1403:                                   ; preds = %1402
  br label %1404

; <label>:1404:                                   ; preds = %1403, %1147, %1133
  br label %1405

; <label>:1405:                                   ; preds = %1404
  br label %1406

; <label>:1406:                                   ; preds = %1405
  br label %1407

; <label>:1407:                                   ; preds = %1406
  br label %1408

; <label>:1408:                                   ; preds = %1407
  br label %1413

; <label>:1409:                                   ; preds = %547
  %1410 = load i64, i64* %9, align 8
  %1411 = load %struct.my_hash*, %struct.my_hash** %4, align 8
  %1412 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1411, i32 0, i32 1
  store i64 %1410, i64* %1412, align 8
  br label %1413

; <label>:1413:                                   ; preds = %1409, %1408
  br label %1414

; <label>:1414:                                   ; preds = %1413
  %1415 = load i64, i64* %8, align 8
  %1416 = add i64 %1415, 1
  store i64 %1416, i64* %8, align 8
  br label %65

; <label>:1417:                                   ; preds = %65
  %1418 = load i64*, i64** %5, align 8
  %1419 = icmp eq i64* %1418, null
  br i1 %1419, label %1422, label %1420

; <label>:1420:                                   ; preds = %1417
  %1421 = bitcast i64* %1418 to i8*
  call void @_ZdaPv(i8* %1421) #9
  br label %1422

; <label>:1422:                                   ; preds = %1420, %1417
  %1423 = load i64, i64* @MEMCOUNT, align 8
  %1424 = sub i64 %1423, 4096
  store i64 %1424, i64* @MEMCOUNT, align 8
  %1425 = load %struct.my_hash*, %struct.my_hash** %3, align 8
  %1426 = ptrtoint %struct.my_hash* %1425 to i64
  %1427 = or i64 %1426, 6
  %1428 = load i64*, i64** %7, align 8
  %1429 = getelementptr inbounds i64, i64* %1428, i64 1
  store i64 %1427, i64* %1429, align 8
  %1430 = load i64*, i64** %7, align 8
  %1431 = ptrtoint i64* %1430 to i64
  %1432 = or i64 %1431, 6
  ret i64 %1432
}

declare i32 @memcmp(i8*, i8*, i64) #1

; Function Attrs: argmemonly nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i32, i1) #6

declare void @free(i8*) #1

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_make_45hash(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %4 = load i64, i64* %2, align 8
  %5 = call i64 @expect_args1(i64 %4)
  store i64 %5, i64* %3, align 8
  %6 = load i64, i64* %3, align 8
  %7 = call i64 @prim_make_45hash(i64 %6)
  ret i64 %7
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_hash_45ref(i64, i64) #0 {
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64*, align 8
  %7 = alloca %struct.my_hash*, align 8
  %8 = alloca %struct.my_hash*, align 8
  %9 = alloca i32, align 4
  %10 = alloca i32, align 4
  %11 = alloca i32, align 4
  %12 = alloca i32, align 4
  %13 = alloca i8*, align 8
  %14 = alloca i32, align 4
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  %15 = load i64, i64* %4, align 8
  %16 = and i64 %15, 7
  %17 = icmp ne i64 %16, 6
  br i1 %17, label %25, label %18

; <label>:18:                                     ; preds = %2
  %19 = load i64, i64* %4, align 8
  %20 = and i64 %19, -8
  %21 = inttoptr i64 %20 to i64*
  %22 = getelementptr inbounds i64, i64* %21, i64 0
  %23 = load i64, i64* %22, align 8
  %24 = icmp ne i64 2, %23
  br i1 %24, label %25, label %26

; <label>:25:                                     ; preds = %18, %2
  call void @fatal_err(i8* getelementptr inbounds ([59 x i8], [59 x i8]* @.str.60, i32 0, i32 0))
  br label %26

; <label>:26:                                     ; preds = %25, %18
  %27 = load i64, i64* %4, align 8
  %28 = and i64 %27, -8
  %29 = inttoptr i64 %28 to i64*
  store i64* %29, i64** %6, align 8
  %30 = load i64*, i64** %6, align 8
  %31 = getelementptr inbounds i64, i64* %30, i64 1
  %32 = load i64, i64* %31, align 8
  %33 = and i64 %32, -8
  %34 = inttoptr i64 %33 to i64*
  %35 = bitcast i64* %34 to %struct.my_hash*
  store %struct.my_hash* %35, %struct.my_hash** %8, align 8
  br label %36

; <label>:36:                                     ; preds = %26
  br label %37

; <label>:37:                                     ; preds = %36
  br label %38

; <label>:38:                                     ; preds = %37
  %39 = bitcast i64* %5 to i8*
  store i8* %39, i8** %13, align 8
  store i32 -17973521, i32* %9, align 4
  store i32 -1640531527, i32* %11, align 4
  store i32 -1640531527, i32* %10, align 4
  store i32 8, i32* %12, align 4
  br label %40

; <label>:40:                                     ; preds = %207, %38
  %41 = load i32, i32* %12, align 4
  %42 = icmp uge i32 %41, 12
  br i1 %42, label %43, label %212

; <label>:43:                                     ; preds = %40
  %44 = load i8*, i8** %13, align 8
  %45 = getelementptr inbounds i8, i8* %44, i64 0
  %46 = load i8, i8* %45, align 1
  %47 = zext i8 %46 to i32
  %48 = load i8*, i8** %13, align 8
  %49 = getelementptr inbounds i8, i8* %48, i64 1
  %50 = load i8, i8* %49, align 1
  %51 = zext i8 %50 to i32
  %52 = shl i32 %51, 8
  %53 = add i32 %47, %52
  %54 = load i8*, i8** %13, align 8
  %55 = getelementptr inbounds i8, i8* %54, i64 2
  %56 = load i8, i8* %55, align 1
  %57 = zext i8 %56 to i32
  %58 = shl i32 %57, 16
  %59 = add i32 %53, %58
  %60 = load i8*, i8** %13, align 8
  %61 = getelementptr inbounds i8, i8* %60, i64 3
  %62 = load i8, i8* %61, align 1
  %63 = zext i8 %62 to i32
  %64 = shl i32 %63, 24
  %65 = add i32 %59, %64
  %66 = load i32, i32* %10, align 4
  %67 = add i32 %66, %65
  store i32 %67, i32* %10, align 4
  %68 = load i8*, i8** %13, align 8
  %69 = getelementptr inbounds i8, i8* %68, i64 4
  %70 = load i8, i8* %69, align 1
  %71 = zext i8 %70 to i32
  %72 = load i8*, i8** %13, align 8
  %73 = getelementptr inbounds i8, i8* %72, i64 5
  %74 = load i8, i8* %73, align 1
  %75 = zext i8 %74 to i32
  %76 = shl i32 %75, 8
  %77 = add i32 %71, %76
  %78 = load i8*, i8** %13, align 8
  %79 = getelementptr inbounds i8, i8* %78, i64 6
  %80 = load i8, i8* %79, align 1
  %81 = zext i8 %80 to i32
  %82 = shl i32 %81, 16
  %83 = add i32 %77, %82
  %84 = load i8*, i8** %13, align 8
  %85 = getelementptr inbounds i8, i8* %84, i64 7
  %86 = load i8, i8* %85, align 1
  %87 = zext i8 %86 to i32
  %88 = shl i32 %87, 24
  %89 = add i32 %83, %88
  %90 = load i32, i32* %11, align 4
  %91 = add i32 %90, %89
  store i32 %91, i32* %11, align 4
  %92 = load i8*, i8** %13, align 8
  %93 = getelementptr inbounds i8, i8* %92, i64 8
  %94 = load i8, i8* %93, align 1
  %95 = zext i8 %94 to i32
  %96 = load i8*, i8** %13, align 8
  %97 = getelementptr inbounds i8, i8* %96, i64 9
  %98 = load i8, i8* %97, align 1
  %99 = zext i8 %98 to i32
  %100 = shl i32 %99, 8
  %101 = add i32 %95, %100
  %102 = load i8*, i8** %13, align 8
  %103 = getelementptr inbounds i8, i8* %102, i64 10
  %104 = load i8, i8* %103, align 1
  %105 = zext i8 %104 to i32
  %106 = shl i32 %105, 16
  %107 = add i32 %101, %106
  %108 = load i8*, i8** %13, align 8
  %109 = getelementptr inbounds i8, i8* %108, i64 11
  %110 = load i8, i8* %109, align 1
  %111 = zext i8 %110 to i32
  %112 = shl i32 %111, 24
  %113 = add i32 %107, %112
  %114 = load i32, i32* %9, align 4
  %115 = add i32 %114, %113
  store i32 %115, i32* %9, align 4
  br label %116

; <label>:116:                                    ; preds = %43
  %117 = load i32, i32* %11, align 4
  %118 = load i32, i32* %10, align 4
  %119 = sub i32 %118, %117
  store i32 %119, i32* %10, align 4
  %120 = load i32, i32* %9, align 4
  %121 = load i32, i32* %10, align 4
  %122 = sub i32 %121, %120
  store i32 %122, i32* %10, align 4
  %123 = load i32, i32* %9, align 4
  %124 = lshr i32 %123, 13
  %125 = load i32, i32* %10, align 4
  %126 = xor i32 %125, %124
  store i32 %126, i32* %10, align 4
  %127 = load i32, i32* %9, align 4
  %128 = load i32, i32* %11, align 4
  %129 = sub i32 %128, %127
  store i32 %129, i32* %11, align 4
  %130 = load i32, i32* %10, align 4
  %131 = load i32, i32* %11, align 4
  %132 = sub i32 %131, %130
  store i32 %132, i32* %11, align 4
  %133 = load i32, i32* %10, align 4
  %134 = shl i32 %133, 8
  %135 = load i32, i32* %11, align 4
  %136 = xor i32 %135, %134
  store i32 %136, i32* %11, align 4
  %137 = load i32, i32* %10, align 4
  %138 = load i32, i32* %9, align 4
  %139 = sub i32 %138, %137
  store i32 %139, i32* %9, align 4
  %140 = load i32, i32* %11, align 4
  %141 = load i32, i32* %9, align 4
  %142 = sub i32 %141, %140
  store i32 %142, i32* %9, align 4
  %143 = load i32, i32* %11, align 4
  %144 = lshr i32 %143, 13
  %145 = load i32, i32* %9, align 4
  %146 = xor i32 %145, %144
  store i32 %146, i32* %9, align 4
  %147 = load i32, i32* %11, align 4
  %148 = load i32, i32* %10, align 4
  %149 = sub i32 %148, %147
  store i32 %149, i32* %10, align 4
  %150 = load i32, i32* %9, align 4
  %151 = load i32, i32* %10, align 4
  %152 = sub i32 %151, %150
  store i32 %152, i32* %10, align 4
  %153 = load i32, i32* %9, align 4
  %154 = lshr i32 %153, 12
  %155 = load i32, i32* %10, align 4
  %156 = xor i32 %155, %154
  store i32 %156, i32* %10, align 4
  %157 = load i32, i32* %9, align 4
  %158 = load i32, i32* %11, align 4
  %159 = sub i32 %158, %157
  store i32 %159, i32* %11, align 4
  %160 = load i32, i32* %10, align 4
  %161 = load i32, i32* %11, align 4
  %162 = sub i32 %161, %160
  store i32 %162, i32* %11, align 4
  %163 = load i32, i32* %10, align 4
  %164 = shl i32 %163, 16
  %165 = load i32, i32* %11, align 4
  %166 = xor i32 %165, %164
  store i32 %166, i32* %11, align 4
  %167 = load i32, i32* %10, align 4
  %168 = load i32, i32* %9, align 4
  %169 = sub i32 %168, %167
  store i32 %169, i32* %9, align 4
  %170 = load i32, i32* %11, align 4
  %171 = load i32, i32* %9, align 4
  %172 = sub i32 %171, %170
  store i32 %172, i32* %9, align 4
  %173 = load i32, i32* %11, align 4
  %174 = lshr i32 %173, 5
  %175 = load i32, i32* %9, align 4
  %176 = xor i32 %175, %174
  store i32 %176, i32* %9, align 4
  %177 = load i32, i32* %11, align 4
  %178 = load i32, i32* %10, align 4
  %179 = sub i32 %178, %177
  store i32 %179, i32* %10, align 4
  %180 = load i32, i32* %9, align 4
  %181 = load i32, i32* %10, align 4
  %182 = sub i32 %181, %180
  store i32 %182, i32* %10, align 4
  %183 = load i32, i32* %9, align 4
  %184 = lshr i32 %183, 3
  %185 = load i32, i32* %10, align 4
  %186 = xor i32 %185, %184
  store i32 %186, i32* %10, align 4
  %187 = load i32, i32* %9, align 4
  %188 = load i32, i32* %11, align 4
  %189 = sub i32 %188, %187
  store i32 %189, i32* %11, align 4
  %190 = load i32, i32* %10, align 4
  %191 = load i32, i32* %11, align 4
  %192 = sub i32 %191, %190
  store i32 %192, i32* %11, align 4
  %193 = load i32, i32* %10, align 4
  %194 = shl i32 %193, 10
  %195 = load i32, i32* %11, align 4
  %196 = xor i32 %195, %194
  store i32 %196, i32* %11, align 4
  %197 = load i32, i32* %10, align 4
  %198 = load i32, i32* %9, align 4
  %199 = sub i32 %198, %197
  store i32 %199, i32* %9, align 4
  %200 = load i32, i32* %11, align 4
  %201 = load i32, i32* %9, align 4
  %202 = sub i32 %201, %200
  store i32 %202, i32* %9, align 4
  %203 = load i32, i32* %11, align 4
  %204 = lshr i32 %203, 15
  %205 = load i32, i32* %9, align 4
  %206 = xor i32 %205, %204
  store i32 %206, i32* %9, align 4
  br label %207

; <label>:207:                                    ; preds = %116
  %208 = load i8*, i8** %13, align 8
  %209 = getelementptr inbounds i8, i8* %208, i64 12
  store i8* %209, i8** %13, align 8
  %210 = load i32, i32* %12, align 4
  %211 = sub i32 %210, 12
  store i32 %211, i32* %12, align 4
  br label %40

; <label>:212:                                    ; preds = %40
  %213 = load i32, i32* %9, align 4
  %214 = add i32 %213, 8
  store i32 %214, i32* %9, align 4
  %215 = load i32, i32* %12, align 4
  switch i32 %215, label %302 [
    i32 11, label %216
    i32 10, label %224
    i32 9, label %232
    i32 8, label %240
    i32 7, label %248
    i32 6, label %256
    i32 5, label %264
    i32 4, label %271
    i32 3, label %279
    i32 2, label %287
    i32 1, label %295
  ]

; <label>:216:                                    ; preds = %212
  %217 = load i8*, i8** %13, align 8
  %218 = getelementptr inbounds i8, i8* %217, i64 10
  %219 = load i8, i8* %218, align 1
  %220 = zext i8 %219 to i32
  %221 = shl i32 %220, 24
  %222 = load i32, i32* %9, align 4
  %223 = add i32 %222, %221
  store i32 %223, i32* %9, align 4
  br label %224

; <label>:224:                                    ; preds = %212, %216
  %225 = load i8*, i8** %13, align 8
  %226 = getelementptr inbounds i8, i8* %225, i64 9
  %227 = load i8, i8* %226, align 1
  %228 = zext i8 %227 to i32
  %229 = shl i32 %228, 16
  %230 = load i32, i32* %9, align 4
  %231 = add i32 %230, %229
  store i32 %231, i32* %9, align 4
  br label %232

; <label>:232:                                    ; preds = %212, %224
  %233 = load i8*, i8** %13, align 8
  %234 = getelementptr inbounds i8, i8* %233, i64 8
  %235 = load i8, i8* %234, align 1
  %236 = zext i8 %235 to i32
  %237 = shl i32 %236, 8
  %238 = load i32, i32* %9, align 4
  %239 = add i32 %238, %237
  store i32 %239, i32* %9, align 4
  br label %240

; <label>:240:                                    ; preds = %212, %232
  %241 = load i8*, i8** %13, align 8
  %242 = getelementptr inbounds i8, i8* %241, i64 7
  %243 = load i8, i8* %242, align 1
  %244 = zext i8 %243 to i32
  %245 = shl i32 %244, 24
  %246 = load i32, i32* %11, align 4
  %247 = add i32 %246, %245
  store i32 %247, i32* %11, align 4
  br label %248

; <label>:248:                                    ; preds = %212, %240
  %249 = load i8*, i8** %13, align 8
  %250 = getelementptr inbounds i8, i8* %249, i64 6
  %251 = load i8, i8* %250, align 1
  %252 = zext i8 %251 to i32
  %253 = shl i32 %252, 16
  %254 = load i32, i32* %11, align 4
  %255 = add i32 %254, %253
  store i32 %255, i32* %11, align 4
  br label %256

; <label>:256:                                    ; preds = %212, %248
  %257 = load i8*, i8** %13, align 8
  %258 = getelementptr inbounds i8, i8* %257, i64 5
  %259 = load i8, i8* %258, align 1
  %260 = zext i8 %259 to i32
  %261 = shl i32 %260, 8
  %262 = load i32, i32* %11, align 4
  %263 = add i32 %262, %261
  store i32 %263, i32* %11, align 4
  br label %264

; <label>:264:                                    ; preds = %212, %256
  %265 = load i8*, i8** %13, align 8
  %266 = getelementptr inbounds i8, i8* %265, i64 4
  %267 = load i8, i8* %266, align 1
  %268 = zext i8 %267 to i32
  %269 = load i32, i32* %11, align 4
  %270 = add i32 %269, %268
  store i32 %270, i32* %11, align 4
  br label %271

; <label>:271:                                    ; preds = %212, %264
  %272 = load i8*, i8** %13, align 8
  %273 = getelementptr inbounds i8, i8* %272, i64 3
  %274 = load i8, i8* %273, align 1
  %275 = zext i8 %274 to i32
  %276 = shl i32 %275, 24
  %277 = load i32, i32* %10, align 4
  %278 = add i32 %277, %276
  store i32 %278, i32* %10, align 4
  br label %279

; <label>:279:                                    ; preds = %212, %271
  %280 = load i8*, i8** %13, align 8
  %281 = getelementptr inbounds i8, i8* %280, i64 2
  %282 = load i8, i8* %281, align 1
  %283 = zext i8 %282 to i32
  %284 = shl i32 %283, 16
  %285 = load i32, i32* %10, align 4
  %286 = add i32 %285, %284
  store i32 %286, i32* %10, align 4
  br label %287

; <label>:287:                                    ; preds = %212, %279
  %288 = load i8*, i8** %13, align 8
  %289 = getelementptr inbounds i8, i8* %288, i64 1
  %290 = load i8, i8* %289, align 1
  %291 = zext i8 %290 to i32
  %292 = shl i32 %291, 8
  %293 = load i32, i32* %10, align 4
  %294 = add i32 %293, %292
  store i32 %294, i32* %10, align 4
  br label %295

; <label>:295:                                    ; preds = %212, %287
  %296 = load i8*, i8** %13, align 8
  %297 = getelementptr inbounds i8, i8* %296, i64 0
  %298 = load i8, i8* %297, align 1
  %299 = zext i8 %298 to i32
  %300 = load i32, i32* %10, align 4
  %301 = add i32 %300, %299
  store i32 %301, i32* %10, align 4
  br label %302

; <label>:302:                                    ; preds = %295, %212
  br label %303

; <label>:303:                                    ; preds = %302
  %304 = load i32, i32* %11, align 4
  %305 = load i32, i32* %10, align 4
  %306 = sub i32 %305, %304
  store i32 %306, i32* %10, align 4
  %307 = load i32, i32* %9, align 4
  %308 = load i32, i32* %10, align 4
  %309 = sub i32 %308, %307
  store i32 %309, i32* %10, align 4
  %310 = load i32, i32* %9, align 4
  %311 = lshr i32 %310, 13
  %312 = load i32, i32* %10, align 4
  %313 = xor i32 %312, %311
  store i32 %313, i32* %10, align 4
  %314 = load i32, i32* %9, align 4
  %315 = load i32, i32* %11, align 4
  %316 = sub i32 %315, %314
  store i32 %316, i32* %11, align 4
  %317 = load i32, i32* %10, align 4
  %318 = load i32, i32* %11, align 4
  %319 = sub i32 %318, %317
  store i32 %319, i32* %11, align 4
  %320 = load i32, i32* %10, align 4
  %321 = shl i32 %320, 8
  %322 = load i32, i32* %11, align 4
  %323 = xor i32 %322, %321
  store i32 %323, i32* %11, align 4
  %324 = load i32, i32* %10, align 4
  %325 = load i32, i32* %9, align 4
  %326 = sub i32 %325, %324
  store i32 %326, i32* %9, align 4
  %327 = load i32, i32* %11, align 4
  %328 = load i32, i32* %9, align 4
  %329 = sub i32 %328, %327
  store i32 %329, i32* %9, align 4
  %330 = load i32, i32* %11, align 4
  %331 = lshr i32 %330, 13
  %332 = load i32, i32* %9, align 4
  %333 = xor i32 %332, %331
  store i32 %333, i32* %9, align 4
  %334 = load i32, i32* %11, align 4
  %335 = load i32, i32* %10, align 4
  %336 = sub i32 %335, %334
  store i32 %336, i32* %10, align 4
  %337 = load i32, i32* %9, align 4
  %338 = load i32, i32* %10, align 4
  %339 = sub i32 %338, %337
  store i32 %339, i32* %10, align 4
  %340 = load i32, i32* %9, align 4
  %341 = lshr i32 %340, 12
  %342 = load i32, i32* %10, align 4
  %343 = xor i32 %342, %341
  store i32 %343, i32* %10, align 4
  %344 = load i32, i32* %9, align 4
  %345 = load i32, i32* %11, align 4
  %346 = sub i32 %345, %344
  store i32 %346, i32* %11, align 4
  %347 = load i32, i32* %10, align 4
  %348 = load i32, i32* %11, align 4
  %349 = sub i32 %348, %347
  store i32 %349, i32* %11, align 4
  %350 = load i32, i32* %10, align 4
  %351 = shl i32 %350, 16
  %352 = load i32, i32* %11, align 4
  %353 = xor i32 %352, %351
  store i32 %353, i32* %11, align 4
  %354 = load i32, i32* %10, align 4
  %355 = load i32, i32* %9, align 4
  %356 = sub i32 %355, %354
  store i32 %356, i32* %9, align 4
  %357 = load i32, i32* %11, align 4
  %358 = load i32, i32* %9, align 4
  %359 = sub i32 %358, %357
  store i32 %359, i32* %9, align 4
  %360 = load i32, i32* %11, align 4
  %361 = lshr i32 %360, 5
  %362 = load i32, i32* %9, align 4
  %363 = xor i32 %362, %361
  store i32 %363, i32* %9, align 4
  %364 = load i32, i32* %11, align 4
  %365 = load i32, i32* %10, align 4
  %366 = sub i32 %365, %364
  store i32 %366, i32* %10, align 4
  %367 = load i32, i32* %9, align 4
  %368 = load i32, i32* %10, align 4
  %369 = sub i32 %368, %367
  store i32 %369, i32* %10, align 4
  %370 = load i32, i32* %9, align 4
  %371 = lshr i32 %370, 3
  %372 = load i32, i32* %10, align 4
  %373 = xor i32 %372, %371
  store i32 %373, i32* %10, align 4
  %374 = load i32, i32* %9, align 4
  %375 = load i32, i32* %11, align 4
  %376 = sub i32 %375, %374
  store i32 %376, i32* %11, align 4
  %377 = load i32, i32* %10, align 4
  %378 = load i32, i32* %11, align 4
  %379 = sub i32 %378, %377
  store i32 %379, i32* %11, align 4
  %380 = load i32, i32* %10, align 4
  %381 = shl i32 %380, 10
  %382 = load i32, i32* %11, align 4
  %383 = xor i32 %382, %381
  store i32 %383, i32* %11, align 4
  %384 = load i32, i32* %10, align 4
  %385 = load i32, i32* %9, align 4
  %386 = sub i32 %385, %384
  store i32 %386, i32* %9, align 4
  %387 = load i32, i32* %11, align 4
  %388 = load i32, i32* %9, align 4
  %389 = sub i32 %388, %387
  store i32 %389, i32* %9, align 4
  %390 = load i32, i32* %11, align 4
  %391 = lshr i32 %390, 15
  %392 = load i32, i32* %9, align 4
  %393 = xor i32 %392, %391
  store i32 %393, i32* %9, align 4
  br label %394

; <label>:394:                                    ; preds = %303
  br label %395

; <label>:395:                                    ; preds = %394
  br label %396

; <label>:396:                                    ; preds = %395
  br label %397

; <label>:397:                                    ; preds = %396
  store %struct.my_hash* null, %struct.my_hash** %7, align 8
  %398 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %399 = icmp ne %struct.my_hash* %398, null
  br i1 %399, label %400, label %505

; <label>:400:                                    ; preds = %397
  br label %401

; <label>:401:                                    ; preds = %400
  %402 = load i32, i32* %9, align 4
  %403 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %404 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %403, i32 0, i32 2
  %405 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %404, i32 0, i32 0
  %406 = load %struct.UT_hash_table*, %struct.UT_hash_table** %405, align 8
  %407 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %406, i32 0, i32 1
  %408 = load i32, i32* %407, align 8
  %409 = sub i32 %408, 1
  %410 = and i32 %402, %409
  store i32 %410, i32* %14, align 4
  br label %411

; <label>:411:                                    ; preds = %401
  br label %412

; <label>:412:                                    ; preds = %411
  %413 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %414 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %413, i32 0, i32 2
  %415 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %414, i32 0, i32 0
  %416 = load %struct.UT_hash_table*, %struct.UT_hash_table** %415, align 8
  %417 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %416, i32 0, i32 0
  %418 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %417, align 8
  %419 = load i32, i32* %14, align 4
  %420 = zext i32 %419 to i64
  %421 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %418, i64 %420
  %422 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %421, i32 0, i32 0
  %423 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %422, align 8
  %424 = icmp ne %struct.UT_hash_handle* %423, null
  br i1 %424, label %425, label %449

; <label>:425:                                    ; preds = %412
  br label %426

; <label>:426:                                    ; preds = %425
  %427 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %428 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %427, i32 0, i32 2
  %429 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %428, i32 0, i32 0
  %430 = load %struct.UT_hash_table*, %struct.UT_hash_table** %429, align 8
  %431 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %430, i32 0, i32 0
  %432 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %431, align 8
  %433 = load i32, i32* %14, align 4
  %434 = zext i32 %433 to i64
  %435 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %432, i64 %434
  %436 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %435, i32 0, i32 0
  %437 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %436, align 8
  %438 = bitcast %struct.UT_hash_handle* %437 to i8*
  %439 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %440 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %439, i32 0, i32 2
  %441 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %440, i32 0, i32 0
  %442 = load %struct.UT_hash_table*, %struct.UT_hash_table** %441, align 8
  %443 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %442, i32 0, i32 5
  %444 = load i64, i64* %443, align 8
  %445 = sub i64 0, %444
  %446 = getelementptr inbounds i8, i8* %438, i64 %445
  %447 = bitcast i8* %446 to %struct.my_hash*
  store %struct.my_hash* %447, %struct.my_hash** %7, align 8
  br label %448

; <label>:448:                                    ; preds = %426
  br label %450

; <label>:449:                                    ; preds = %412
  store %struct.my_hash* null, %struct.my_hash** %7, align 8
  br label %450

; <label>:450:                                    ; preds = %449, %448
  br label %451

; <label>:451:                                    ; preds = %502, %450
  %452 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %453 = icmp ne %struct.my_hash* %452, null
  br i1 %453, label %454, label %503

; <label>:454:                                    ; preds = %451
  %455 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %456 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %455, i32 0, i32 2
  %457 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %456, i32 0, i32 7
  %458 = load i32, i32* %457, align 4
  %459 = load i32, i32* %9, align 4
  %460 = icmp eq i32 %458, %459
  br i1 %460, label %461, label %478

; <label>:461:                                    ; preds = %454
  %462 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %463 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %462, i32 0, i32 2
  %464 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %463, i32 0, i32 6
  %465 = load i32, i32* %464, align 8
  %466 = zext i32 %465 to i64
  %467 = icmp eq i64 %466, 8
  br i1 %467, label %468, label %478

; <label>:468:                                    ; preds = %461
  %469 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %470 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %469, i32 0, i32 2
  %471 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %470, i32 0, i32 5
  %472 = load i8*, i8** %471, align 8
  %473 = bitcast i64* %5 to i8*
  %474 = call i32 @memcmp(i8* %472, i8* %473, i64 8)
  %475 = icmp eq i32 %474, 0
  br i1 %475, label %476, label %477

; <label>:476:                                    ; preds = %468
  br label %503

; <label>:477:                                    ; preds = %468
  br label %478

; <label>:478:                                    ; preds = %477, %461, %454
  %479 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %480 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %479, i32 0, i32 2
  %481 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %480, i32 0, i32 4
  %482 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %481, align 8
  %483 = icmp ne %struct.UT_hash_handle* %482, null
  br i1 %483, label %484, label %501

; <label>:484:                                    ; preds = %478
  br label %485

; <label>:485:                                    ; preds = %484
  %486 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %487 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %486, i32 0, i32 2
  %488 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %487, i32 0, i32 4
  %489 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %488, align 8
  %490 = bitcast %struct.UT_hash_handle* %489 to i8*
  %491 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %492 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %491, i32 0, i32 2
  %493 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %492, i32 0, i32 0
  %494 = load %struct.UT_hash_table*, %struct.UT_hash_table** %493, align 8
  %495 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %494, i32 0, i32 5
  %496 = load i64, i64* %495, align 8
  %497 = sub i64 0, %496
  %498 = getelementptr inbounds i8, i8* %490, i64 %497
  %499 = bitcast i8* %498 to %struct.my_hash*
  store %struct.my_hash* %499, %struct.my_hash** %7, align 8
  br label %500

; <label>:500:                                    ; preds = %485
  br label %502

; <label>:501:                                    ; preds = %478
  store %struct.my_hash* null, %struct.my_hash** %7, align 8
  br label %502

; <label>:502:                                    ; preds = %501, %500
  br label %451

; <label>:503:                                    ; preds = %476, %451
  br label %504

; <label>:504:                                    ; preds = %503
  br label %505

; <label>:505:                                    ; preds = %504, %397
  br label %506

; <label>:506:                                    ; preds = %505
  br label %507

; <label>:507:                                    ; preds = %506
  %508 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %509 = icmp eq %struct.my_hash* %508, null
  br i1 %509, label %510, label %511

; <label>:510:                                    ; preds = %507
  store i64 0, i64* %3, align 8
  br label %515

; <label>:511:                                    ; preds = %507
  %512 = load %struct.my_hash*, %struct.my_hash** %7, align 8
  %513 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %512, i32 0, i32 1
  %514 = load i64, i64* %513, align 8
  store i64 %514, i64* %3, align 8
  br label %515

; <label>:515:                                    ; preds = %511, %510
  %516 = load i64, i64* %3, align 8
  ret i64 %516
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_hash_45ref(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %6 = load i64, i64* %2, align 8
  %7 = call i64 @expect_cons(i64 %6, i64* %3)
  store i64 %7, i64* %4, align 8
  %8 = load i64, i64* %3, align 8
  %9 = call i64 @expect_cons(i64 %8, i64* %3)
  store i64 %9, i64* %5, align 8
  %10 = load i64, i64* %3, align 8
  %11 = icmp ne i64 %10, 0
  br i1 %11, label %12, label %13

; <label>:12:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %13

; <label>:13:                                     ; preds = %12, %1
  %14 = load i64, i64* %4, align 8
  %15 = load i64, i64* %5, align 8
  %16 = call i64 @prim_hash_45ref(i64 %14, i64 %15)
  ret i64 %16
}

; Function Attrs: noinline ssp uwtable
define i64 @prim_hash_45set_33(i64, i64, i64) #0 {
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64, align 8
  %7 = alloca i64*, align 8
  %8 = alloca %struct.my_hash*, align 8
  %9 = alloca %struct.my_hash*, align 8
  %10 = alloca i32, align 4
  %11 = alloca i32, align 4
  %12 = alloca i32, align 4
  %13 = alloca i32, align 4
  %14 = alloca i8*, align 8
  %15 = alloca i32, align 4
  %16 = alloca i32, align 4
  %17 = alloca i32, align 4
  %18 = alloca i32, align 4
  %19 = alloca i32, align 4
  %20 = alloca i8*, align 8
  %21 = alloca i32, align 4
  %22 = alloca %struct.UT_hash_bucket*, align 8
  %23 = alloca i32, align 4
  %24 = alloca i32, align 4
  %25 = alloca %struct.UT_hash_handle*, align 8
  %26 = alloca %struct.UT_hash_handle*, align 8
  %27 = alloca %struct.UT_hash_bucket*, align 8
  %28 = alloca %struct.UT_hash_bucket*, align 8
  store i64 %0, i64* %4, align 8
  store i64 %1, i64* %5, align 8
  store i64 %2, i64* %6, align 8
  %29 = load i64, i64* %4, align 8
  %30 = and i64 %29, 7
  %31 = icmp ne i64 %30, 6
  br i1 %31, label %39, label %32

; <label>:32:                                     ; preds = %3
  %33 = load i64, i64* %4, align 8
  %34 = and i64 %33, -8
  %35 = inttoptr i64 %34 to i64*
  %36 = getelementptr inbounds i64, i64* %35, i64 0
  %37 = load i64, i64* %36, align 8
  %38 = icmp ne i64 2, %37
  br i1 %38, label %39, label %40

; <label>:39:                                     ; preds = %32, %3
  call void @fatal_err(i8* getelementptr inbounds ([60 x i8], [60 x i8]* @.str.61, i32 0, i32 0))
  br label %40

; <label>:40:                                     ; preds = %39, %32
  %41 = load i64, i64* %4, align 8
  %42 = and i64 %41, -8
  %43 = inttoptr i64 %42 to i64*
  store i64* %43, i64** %7, align 8
  %44 = load i64*, i64** %7, align 8
  %45 = getelementptr inbounds i64, i64* %44, i64 1
  %46 = load i64, i64* %45, align 8
  %47 = and i64 %46, -8
  %48 = inttoptr i64 %47 to i64*
  %49 = bitcast i64* %48 to %struct.my_hash*
  store %struct.my_hash* %49, %struct.my_hash** %9, align 8
  br label %50

; <label>:50:                                     ; preds = %40
  br label %51

; <label>:51:                                     ; preds = %50
  br label %52

; <label>:52:                                     ; preds = %51
  %53 = bitcast i64* %5 to i8*
  store i8* %53, i8** %14, align 8
  store i32 -17973521, i32* %10, align 4
  store i32 -1640531527, i32* %12, align 4
  store i32 -1640531527, i32* %11, align 4
  store i32 8, i32* %13, align 4
  br label %54

; <label>:54:                                     ; preds = %221, %52
  %55 = load i32, i32* %13, align 4
  %56 = icmp uge i32 %55, 12
  br i1 %56, label %57, label %226

; <label>:57:                                     ; preds = %54
  %58 = load i8*, i8** %14, align 8
  %59 = getelementptr inbounds i8, i8* %58, i64 0
  %60 = load i8, i8* %59, align 1
  %61 = zext i8 %60 to i32
  %62 = load i8*, i8** %14, align 8
  %63 = getelementptr inbounds i8, i8* %62, i64 1
  %64 = load i8, i8* %63, align 1
  %65 = zext i8 %64 to i32
  %66 = shl i32 %65, 8
  %67 = add i32 %61, %66
  %68 = load i8*, i8** %14, align 8
  %69 = getelementptr inbounds i8, i8* %68, i64 2
  %70 = load i8, i8* %69, align 1
  %71 = zext i8 %70 to i32
  %72 = shl i32 %71, 16
  %73 = add i32 %67, %72
  %74 = load i8*, i8** %14, align 8
  %75 = getelementptr inbounds i8, i8* %74, i64 3
  %76 = load i8, i8* %75, align 1
  %77 = zext i8 %76 to i32
  %78 = shl i32 %77, 24
  %79 = add i32 %73, %78
  %80 = load i32, i32* %11, align 4
  %81 = add i32 %80, %79
  store i32 %81, i32* %11, align 4
  %82 = load i8*, i8** %14, align 8
  %83 = getelementptr inbounds i8, i8* %82, i64 4
  %84 = load i8, i8* %83, align 1
  %85 = zext i8 %84 to i32
  %86 = load i8*, i8** %14, align 8
  %87 = getelementptr inbounds i8, i8* %86, i64 5
  %88 = load i8, i8* %87, align 1
  %89 = zext i8 %88 to i32
  %90 = shl i32 %89, 8
  %91 = add i32 %85, %90
  %92 = load i8*, i8** %14, align 8
  %93 = getelementptr inbounds i8, i8* %92, i64 6
  %94 = load i8, i8* %93, align 1
  %95 = zext i8 %94 to i32
  %96 = shl i32 %95, 16
  %97 = add i32 %91, %96
  %98 = load i8*, i8** %14, align 8
  %99 = getelementptr inbounds i8, i8* %98, i64 7
  %100 = load i8, i8* %99, align 1
  %101 = zext i8 %100 to i32
  %102 = shl i32 %101, 24
  %103 = add i32 %97, %102
  %104 = load i32, i32* %12, align 4
  %105 = add i32 %104, %103
  store i32 %105, i32* %12, align 4
  %106 = load i8*, i8** %14, align 8
  %107 = getelementptr inbounds i8, i8* %106, i64 8
  %108 = load i8, i8* %107, align 1
  %109 = zext i8 %108 to i32
  %110 = load i8*, i8** %14, align 8
  %111 = getelementptr inbounds i8, i8* %110, i64 9
  %112 = load i8, i8* %111, align 1
  %113 = zext i8 %112 to i32
  %114 = shl i32 %113, 8
  %115 = add i32 %109, %114
  %116 = load i8*, i8** %14, align 8
  %117 = getelementptr inbounds i8, i8* %116, i64 10
  %118 = load i8, i8* %117, align 1
  %119 = zext i8 %118 to i32
  %120 = shl i32 %119, 16
  %121 = add i32 %115, %120
  %122 = load i8*, i8** %14, align 8
  %123 = getelementptr inbounds i8, i8* %122, i64 11
  %124 = load i8, i8* %123, align 1
  %125 = zext i8 %124 to i32
  %126 = shl i32 %125, 24
  %127 = add i32 %121, %126
  %128 = load i32, i32* %10, align 4
  %129 = add i32 %128, %127
  store i32 %129, i32* %10, align 4
  br label %130

; <label>:130:                                    ; preds = %57
  %131 = load i32, i32* %12, align 4
  %132 = load i32, i32* %11, align 4
  %133 = sub i32 %132, %131
  store i32 %133, i32* %11, align 4
  %134 = load i32, i32* %10, align 4
  %135 = load i32, i32* %11, align 4
  %136 = sub i32 %135, %134
  store i32 %136, i32* %11, align 4
  %137 = load i32, i32* %10, align 4
  %138 = lshr i32 %137, 13
  %139 = load i32, i32* %11, align 4
  %140 = xor i32 %139, %138
  store i32 %140, i32* %11, align 4
  %141 = load i32, i32* %10, align 4
  %142 = load i32, i32* %12, align 4
  %143 = sub i32 %142, %141
  store i32 %143, i32* %12, align 4
  %144 = load i32, i32* %11, align 4
  %145 = load i32, i32* %12, align 4
  %146 = sub i32 %145, %144
  store i32 %146, i32* %12, align 4
  %147 = load i32, i32* %11, align 4
  %148 = shl i32 %147, 8
  %149 = load i32, i32* %12, align 4
  %150 = xor i32 %149, %148
  store i32 %150, i32* %12, align 4
  %151 = load i32, i32* %11, align 4
  %152 = load i32, i32* %10, align 4
  %153 = sub i32 %152, %151
  store i32 %153, i32* %10, align 4
  %154 = load i32, i32* %12, align 4
  %155 = load i32, i32* %10, align 4
  %156 = sub i32 %155, %154
  store i32 %156, i32* %10, align 4
  %157 = load i32, i32* %12, align 4
  %158 = lshr i32 %157, 13
  %159 = load i32, i32* %10, align 4
  %160 = xor i32 %159, %158
  store i32 %160, i32* %10, align 4
  %161 = load i32, i32* %12, align 4
  %162 = load i32, i32* %11, align 4
  %163 = sub i32 %162, %161
  store i32 %163, i32* %11, align 4
  %164 = load i32, i32* %10, align 4
  %165 = load i32, i32* %11, align 4
  %166 = sub i32 %165, %164
  store i32 %166, i32* %11, align 4
  %167 = load i32, i32* %10, align 4
  %168 = lshr i32 %167, 12
  %169 = load i32, i32* %11, align 4
  %170 = xor i32 %169, %168
  store i32 %170, i32* %11, align 4
  %171 = load i32, i32* %10, align 4
  %172 = load i32, i32* %12, align 4
  %173 = sub i32 %172, %171
  store i32 %173, i32* %12, align 4
  %174 = load i32, i32* %11, align 4
  %175 = load i32, i32* %12, align 4
  %176 = sub i32 %175, %174
  store i32 %176, i32* %12, align 4
  %177 = load i32, i32* %11, align 4
  %178 = shl i32 %177, 16
  %179 = load i32, i32* %12, align 4
  %180 = xor i32 %179, %178
  store i32 %180, i32* %12, align 4
  %181 = load i32, i32* %11, align 4
  %182 = load i32, i32* %10, align 4
  %183 = sub i32 %182, %181
  store i32 %183, i32* %10, align 4
  %184 = load i32, i32* %12, align 4
  %185 = load i32, i32* %10, align 4
  %186 = sub i32 %185, %184
  store i32 %186, i32* %10, align 4
  %187 = load i32, i32* %12, align 4
  %188 = lshr i32 %187, 5
  %189 = load i32, i32* %10, align 4
  %190 = xor i32 %189, %188
  store i32 %190, i32* %10, align 4
  %191 = load i32, i32* %12, align 4
  %192 = load i32, i32* %11, align 4
  %193 = sub i32 %192, %191
  store i32 %193, i32* %11, align 4
  %194 = load i32, i32* %10, align 4
  %195 = load i32, i32* %11, align 4
  %196 = sub i32 %195, %194
  store i32 %196, i32* %11, align 4
  %197 = load i32, i32* %10, align 4
  %198 = lshr i32 %197, 3
  %199 = load i32, i32* %11, align 4
  %200 = xor i32 %199, %198
  store i32 %200, i32* %11, align 4
  %201 = load i32, i32* %10, align 4
  %202 = load i32, i32* %12, align 4
  %203 = sub i32 %202, %201
  store i32 %203, i32* %12, align 4
  %204 = load i32, i32* %11, align 4
  %205 = load i32, i32* %12, align 4
  %206 = sub i32 %205, %204
  store i32 %206, i32* %12, align 4
  %207 = load i32, i32* %11, align 4
  %208 = shl i32 %207, 10
  %209 = load i32, i32* %12, align 4
  %210 = xor i32 %209, %208
  store i32 %210, i32* %12, align 4
  %211 = load i32, i32* %11, align 4
  %212 = load i32, i32* %10, align 4
  %213 = sub i32 %212, %211
  store i32 %213, i32* %10, align 4
  %214 = load i32, i32* %12, align 4
  %215 = load i32, i32* %10, align 4
  %216 = sub i32 %215, %214
  store i32 %216, i32* %10, align 4
  %217 = load i32, i32* %12, align 4
  %218 = lshr i32 %217, 15
  %219 = load i32, i32* %10, align 4
  %220 = xor i32 %219, %218
  store i32 %220, i32* %10, align 4
  br label %221

; <label>:221:                                    ; preds = %130
  %222 = load i8*, i8** %14, align 8
  %223 = getelementptr inbounds i8, i8* %222, i64 12
  store i8* %223, i8** %14, align 8
  %224 = load i32, i32* %13, align 4
  %225 = sub i32 %224, 12
  store i32 %225, i32* %13, align 4
  br label %54

; <label>:226:                                    ; preds = %54
  %227 = load i32, i32* %10, align 4
  %228 = add i32 %227, 8
  store i32 %228, i32* %10, align 4
  %229 = load i32, i32* %13, align 4
  switch i32 %229, label %316 [
    i32 11, label %230
    i32 10, label %238
    i32 9, label %246
    i32 8, label %254
    i32 7, label %262
    i32 6, label %270
    i32 5, label %278
    i32 4, label %285
    i32 3, label %293
    i32 2, label %301
    i32 1, label %309
  ]

; <label>:230:                                    ; preds = %226
  %231 = load i8*, i8** %14, align 8
  %232 = getelementptr inbounds i8, i8* %231, i64 10
  %233 = load i8, i8* %232, align 1
  %234 = zext i8 %233 to i32
  %235 = shl i32 %234, 24
  %236 = load i32, i32* %10, align 4
  %237 = add i32 %236, %235
  store i32 %237, i32* %10, align 4
  br label %238

; <label>:238:                                    ; preds = %226, %230
  %239 = load i8*, i8** %14, align 8
  %240 = getelementptr inbounds i8, i8* %239, i64 9
  %241 = load i8, i8* %240, align 1
  %242 = zext i8 %241 to i32
  %243 = shl i32 %242, 16
  %244 = load i32, i32* %10, align 4
  %245 = add i32 %244, %243
  store i32 %245, i32* %10, align 4
  br label %246

; <label>:246:                                    ; preds = %226, %238
  %247 = load i8*, i8** %14, align 8
  %248 = getelementptr inbounds i8, i8* %247, i64 8
  %249 = load i8, i8* %248, align 1
  %250 = zext i8 %249 to i32
  %251 = shl i32 %250, 8
  %252 = load i32, i32* %10, align 4
  %253 = add i32 %252, %251
  store i32 %253, i32* %10, align 4
  br label %254

; <label>:254:                                    ; preds = %226, %246
  %255 = load i8*, i8** %14, align 8
  %256 = getelementptr inbounds i8, i8* %255, i64 7
  %257 = load i8, i8* %256, align 1
  %258 = zext i8 %257 to i32
  %259 = shl i32 %258, 24
  %260 = load i32, i32* %12, align 4
  %261 = add i32 %260, %259
  store i32 %261, i32* %12, align 4
  br label %262

; <label>:262:                                    ; preds = %226, %254
  %263 = load i8*, i8** %14, align 8
  %264 = getelementptr inbounds i8, i8* %263, i64 6
  %265 = load i8, i8* %264, align 1
  %266 = zext i8 %265 to i32
  %267 = shl i32 %266, 16
  %268 = load i32, i32* %12, align 4
  %269 = add i32 %268, %267
  store i32 %269, i32* %12, align 4
  br label %270

; <label>:270:                                    ; preds = %226, %262
  %271 = load i8*, i8** %14, align 8
  %272 = getelementptr inbounds i8, i8* %271, i64 5
  %273 = load i8, i8* %272, align 1
  %274 = zext i8 %273 to i32
  %275 = shl i32 %274, 8
  %276 = load i32, i32* %12, align 4
  %277 = add i32 %276, %275
  store i32 %277, i32* %12, align 4
  br label %278

; <label>:278:                                    ; preds = %226, %270
  %279 = load i8*, i8** %14, align 8
  %280 = getelementptr inbounds i8, i8* %279, i64 4
  %281 = load i8, i8* %280, align 1
  %282 = zext i8 %281 to i32
  %283 = load i32, i32* %12, align 4
  %284 = add i32 %283, %282
  store i32 %284, i32* %12, align 4
  br label %285

; <label>:285:                                    ; preds = %226, %278
  %286 = load i8*, i8** %14, align 8
  %287 = getelementptr inbounds i8, i8* %286, i64 3
  %288 = load i8, i8* %287, align 1
  %289 = zext i8 %288 to i32
  %290 = shl i32 %289, 24
  %291 = load i32, i32* %11, align 4
  %292 = add i32 %291, %290
  store i32 %292, i32* %11, align 4
  br label %293

; <label>:293:                                    ; preds = %226, %285
  %294 = load i8*, i8** %14, align 8
  %295 = getelementptr inbounds i8, i8* %294, i64 2
  %296 = load i8, i8* %295, align 1
  %297 = zext i8 %296 to i32
  %298 = shl i32 %297, 16
  %299 = load i32, i32* %11, align 4
  %300 = add i32 %299, %298
  store i32 %300, i32* %11, align 4
  br label %301

; <label>:301:                                    ; preds = %226, %293
  %302 = load i8*, i8** %14, align 8
  %303 = getelementptr inbounds i8, i8* %302, i64 1
  %304 = load i8, i8* %303, align 1
  %305 = zext i8 %304 to i32
  %306 = shl i32 %305, 8
  %307 = load i32, i32* %11, align 4
  %308 = add i32 %307, %306
  store i32 %308, i32* %11, align 4
  br label %309

; <label>:309:                                    ; preds = %226, %301
  %310 = load i8*, i8** %14, align 8
  %311 = getelementptr inbounds i8, i8* %310, i64 0
  %312 = load i8, i8* %311, align 1
  %313 = zext i8 %312 to i32
  %314 = load i32, i32* %11, align 4
  %315 = add i32 %314, %313
  store i32 %315, i32* %11, align 4
  br label %316

; <label>:316:                                    ; preds = %309, %226
  br label %317

; <label>:317:                                    ; preds = %316
  %318 = load i32, i32* %12, align 4
  %319 = load i32, i32* %11, align 4
  %320 = sub i32 %319, %318
  store i32 %320, i32* %11, align 4
  %321 = load i32, i32* %10, align 4
  %322 = load i32, i32* %11, align 4
  %323 = sub i32 %322, %321
  store i32 %323, i32* %11, align 4
  %324 = load i32, i32* %10, align 4
  %325 = lshr i32 %324, 13
  %326 = load i32, i32* %11, align 4
  %327 = xor i32 %326, %325
  store i32 %327, i32* %11, align 4
  %328 = load i32, i32* %10, align 4
  %329 = load i32, i32* %12, align 4
  %330 = sub i32 %329, %328
  store i32 %330, i32* %12, align 4
  %331 = load i32, i32* %11, align 4
  %332 = load i32, i32* %12, align 4
  %333 = sub i32 %332, %331
  store i32 %333, i32* %12, align 4
  %334 = load i32, i32* %11, align 4
  %335 = shl i32 %334, 8
  %336 = load i32, i32* %12, align 4
  %337 = xor i32 %336, %335
  store i32 %337, i32* %12, align 4
  %338 = load i32, i32* %11, align 4
  %339 = load i32, i32* %10, align 4
  %340 = sub i32 %339, %338
  store i32 %340, i32* %10, align 4
  %341 = load i32, i32* %12, align 4
  %342 = load i32, i32* %10, align 4
  %343 = sub i32 %342, %341
  store i32 %343, i32* %10, align 4
  %344 = load i32, i32* %12, align 4
  %345 = lshr i32 %344, 13
  %346 = load i32, i32* %10, align 4
  %347 = xor i32 %346, %345
  store i32 %347, i32* %10, align 4
  %348 = load i32, i32* %12, align 4
  %349 = load i32, i32* %11, align 4
  %350 = sub i32 %349, %348
  store i32 %350, i32* %11, align 4
  %351 = load i32, i32* %10, align 4
  %352 = load i32, i32* %11, align 4
  %353 = sub i32 %352, %351
  store i32 %353, i32* %11, align 4
  %354 = load i32, i32* %10, align 4
  %355 = lshr i32 %354, 12
  %356 = load i32, i32* %11, align 4
  %357 = xor i32 %356, %355
  store i32 %357, i32* %11, align 4
  %358 = load i32, i32* %10, align 4
  %359 = load i32, i32* %12, align 4
  %360 = sub i32 %359, %358
  store i32 %360, i32* %12, align 4
  %361 = load i32, i32* %11, align 4
  %362 = load i32, i32* %12, align 4
  %363 = sub i32 %362, %361
  store i32 %363, i32* %12, align 4
  %364 = load i32, i32* %11, align 4
  %365 = shl i32 %364, 16
  %366 = load i32, i32* %12, align 4
  %367 = xor i32 %366, %365
  store i32 %367, i32* %12, align 4
  %368 = load i32, i32* %11, align 4
  %369 = load i32, i32* %10, align 4
  %370 = sub i32 %369, %368
  store i32 %370, i32* %10, align 4
  %371 = load i32, i32* %12, align 4
  %372 = load i32, i32* %10, align 4
  %373 = sub i32 %372, %371
  store i32 %373, i32* %10, align 4
  %374 = load i32, i32* %12, align 4
  %375 = lshr i32 %374, 5
  %376 = load i32, i32* %10, align 4
  %377 = xor i32 %376, %375
  store i32 %377, i32* %10, align 4
  %378 = load i32, i32* %12, align 4
  %379 = load i32, i32* %11, align 4
  %380 = sub i32 %379, %378
  store i32 %380, i32* %11, align 4
  %381 = load i32, i32* %10, align 4
  %382 = load i32, i32* %11, align 4
  %383 = sub i32 %382, %381
  store i32 %383, i32* %11, align 4
  %384 = load i32, i32* %10, align 4
  %385 = lshr i32 %384, 3
  %386 = load i32, i32* %11, align 4
  %387 = xor i32 %386, %385
  store i32 %387, i32* %11, align 4
  %388 = load i32, i32* %10, align 4
  %389 = load i32, i32* %12, align 4
  %390 = sub i32 %389, %388
  store i32 %390, i32* %12, align 4
  %391 = load i32, i32* %11, align 4
  %392 = load i32, i32* %12, align 4
  %393 = sub i32 %392, %391
  store i32 %393, i32* %12, align 4
  %394 = load i32, i32* %11, align 4
  %395 = shl i32 %394, 10
  %396 = load i32, i32* %12, align 4
  %397 = xor i32 %396, %395
  store i32 %397, i32* %12, align 4
  %398 = load i32, i32* %11, align 4
  %399 = load i32, i32* %10, align 4
  %400 = sub i32 %399, %398
  store i32 %400, i32* %10, align 4
  %401 = load i32, i32* %12, align 4
  %402 = load i32, i32* %10, align 4
  %403 = sub i32 %402, %401
  store i32 %403, i32* %10, align 4
  %404 = load i32, i32* %12, align 4
  %405 = lshr i32 %404, 15
  %406 = load i32, i32* %10, align 4
  %407 = xor i32 %406, %405
  store i32 %407, i32* %10, align 4
  br label %408

; <label>:408:                                    ; preds = %317
  br label %409

; <label>:409:                                    ; preds = %408
  br label %410

; <label>:410:                                    ; preds = %409
  br label %411

; <label>:411:                                    ; preds = %410
  store %struct.my_hash* null, %struct.my_hash** %8, align 8
  %412 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %413 = icmp ne %struct.my_hash* %412, null
  br i1 %413, label %414, label %519

; <label>:414:                                    ; preds = %411
  br label %415

; <label>:415:                                    ; preds = %414
  %416 = load i32, i32* %10, align 4
  %417 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %418 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %417, i32 0, i32 2
  %419 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %418, i32 0, i32 0
  %420 = load %struct.UT_hash_table*, %struct.UT_hash_table** %419, align 8
  %421 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %420, i32 0, i32 1
  %422 = load i32, i32* %421, align 8
  %423 = sub i32 %422, 1
  %424 = and i32 %416, %423
  store i32 %424, i32* %15, align 4
  br label %425

; <label>:425:                                    ; preds = %415
  br label %426

; <label>:426:                                    ; preds = %425
  %427 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %428 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %427, i32 0, i32 2
  %429 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %428, i32 0, i32 0
  %430 = load %struct.UT_hash_table*, %struct.UT_hash_table** %429, align 8
  %431 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %430, i32 0, i32 0
  %432 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %431, align 8
  %433 = load i32, i32* %15, align 4
  %434 = zext i32 %433 to i64
  %435 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %432, i64 %434
  %436 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %435, i32 0, i32 0
  %437 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %436, align 8
  %438 = icmp ne %struct.UT_hash_handle* %437, null
  br i1 %438, label %439, label %463

; <label>:439:                                    ; preds = %426
  br label %440

; <label>:440:                                    ; preds = %439
  %441 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %442 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %441, i32 0, i32 2
  %443 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %442, i32 0, i32 0
  %444 = load %struct.UT_hash_table*, %struct.UT_hash_table** %443, align 8
  %445 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %444, i32 0, i32 0
  %446 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %445, align 8
  %447 = load i32, i32* %15, align 4
  %448 = zext i32 %447 to i64
  %449 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %446, i64 %448
  %450 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %449, i32 0, i32 0
  %451 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %450, align 8
  %452 = bitcast %struct.UT_hash_handle* %451 to i8*
  %453 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %454 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %453, i32 0, i32 2
  %455 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %454, i32 0, i32 0
  %456 = load %struct.UT_hash_table*, %struct.UT_hash_table** %455, align 8
  %457 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %456, i32 0, i32 5
  %458 = load i64, i64* %457, align 8
  %459 = sub i64 0, %458
  %460 = getelementptr inbounds i8, i8* %452, i64 %459
  %461 = bitcast i8* %460 to %struct.my_hash*
  store %struct.my_hash* %461, %struct.my_hash** %8, align 8
  br label %462

; <label>:462:                                    ; preds = %440
  br label %464

; <label>:463:                                    ; preds = %426
  store %struct.my_hash* null, %struct.my_hash** %8, align 8
  br label %464

; <label>:464:                                    ; preds = %463, %462
  br label %465

; <label>:465:                                    ; preds = %516, %464
  %466 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %467 = icmp ne %struct.my_hash* %466, null
  br i1 %467, label %468, label %517

; <label>:468:                                    ; preds = %465
  %469 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %470 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %469, i32 0, i32 2
  %471 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %470, i32 0, i32 7
  %472 = load i32, i32* %471, align 4
  %473 = load i32, i32* %10, align 4
  %474 = icmp eq i32 %472, %473
  br i1 %474, label %475, label %492

; <label>:475:                                    ; preds = %468
  %476 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %477 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %476, i32 0, i32 2
  %478 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %477, i32 0, i32 6
  %479 = load i32, i32* %478, align 8
  %480 = zext i32 %479 to i64
  %481 = icmp eq i64 %480, 8
  br i1 %481, label %482, label %492

; <label>:482:                                    ; preds = %475
  %483 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %484 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %483, i32 0, i32 2
  %485 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %484, i32 0, i32 5
  %486 = load i8*, i8** %485, align 8
  %487 = bitcast i64* %5 to i8*
  %488 = call i32 @memcmp(i8* %486, i8* %487, i64 8)
  %489 = icmp eq i32 %488, 0
  br i1 %489, label %490, label %491

; <label>:490:                                    ; preds = %482
  br label %517

; <label>:491:                                    ; preds = %482
  br label %492

; <label>:492:                                    ; preds = %491, %475, %468
  %493 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %494 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %493, i32 0, i32 2
  %495 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %494, i32 0, i32 4
  %496 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %495, align 8
  %497 = icmp ne %struct.UT_hash_handle* %496, null
  br i1 %497, label %498, label %515

; <label>:498:                                    ; preds = %492
  br label %499

; <label>:499:                                    ; preds = %498
  %500 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %501 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %500, i32 0, i32 2
  %502 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %501, i32 0, i32 4
  %503 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %502, align 8
  %504 = bitcast %struct.UT_hash_handle* %503 to i8*
  %505 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %506 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %505, i32 0, i32 2
  %507 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %506, i32 0, i32 0
  %508 = load %struct.UT_hash_table*, %struct.UT_hash_table** %507, align 8
  %509 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %508, i32 0, i32 5
  %510 = load i64, i64* %509, align 8
  %511 = sub i64 0, %510
  %512 = getelementptr inbounds i8, i8* %504, i64 %511
  %513 = bitcast i8* %512 to %struct.my_hash*
  store %struct.my_hash* %513, %struct.my_hash** %8, align 8
  br label %514

; <label>:514:                                    ; preds = %499
  br label %516

; <label>:515:                                    ; preds = %492
  store %struct.my_hash* null, %struct.my_hash** %8, align 8
  br label %516

; <label>:516:                                    ; preds = %515, %514
  br label %465

; <label>:517:                                    ; preds = %490, %465
  br label %518

; <label>:518:                                    ; preds = %517
  br label %519

; <label>:519:                                    ; preds = %518, %411
  br label %520

; <label>:520:                                    ; preds = %519
  br label %521

; <label>:521:                                    ; preds = %520
  %522 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %523 = icmp eq %struct.my_hash* %522, null
  br i1 %523, label %524, label %1376

; <label>:524:                                    ; preds = %521
  %525 = call i64* @alloc(i64 72)
  %526 = bitcast i64* %525 to %struct.my_hash*
  store %struct.my_hash* %526, %struct.my_hash** %8, align 8
  %527 = load i64, i64* %5, align 8
  %528 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %529 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %528, i32 0, i32 0
  store i64 %527, i64* %529, align 8
  %530 = load i64, i64* %6, align 8
  %531 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %532 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %531, i32 0, i32 1
  store i64 %530, i64* %532, align 8
  br label %533

; <label>:533:                                    ; preds = %524
  br label %534

; <label>:534:                                    ; preds = %533
  br label %535

; <label>:535:                                    ; preds = %534
  %536 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %537 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %536, i32 0, i32 0
  %538 = bitcast i64* %537 to i8*
  store i8* %538, i8** %20, align 8
  store i32 -17973521, i32* %16, align 4
  store i32 -1640531527, i32* %18, align 4
  store i32 -1640531527, i32* %17, align 4
  store i32 8, i32* %19, align 4
  br label %539

; <label>:539:                                    ; preds = %706, %535
  %540 = load i32, i32* %19, align 4
  %541 = icmp uge i32 %540, 12
  br i1 %541, label %542, label %711

; <label>:542:                                    ; preds = %539
  %543 = load i8*, i8** %20, align 8
  %544 = getelementptr inbounds i8, i8* %543, i64 0
  %545 = load i8, i8* %544, align 1
  %546 = zext i8 %545 to i32
  %547 = load i8*, i8** %20, align 8
  %548 = getelementptr inbounds i8, i8* %547, i64 1
  %549 = load i8, i8* %548, align 1
  %550 = zext i8 %549 to i32
  %551 = shl i32 %550, 8
  %552 = add i32 %546, %551
  %553 = load i8*, i8** %20, align 8
  %554 = getelementptr inbounds i8, i8* %553, i64 2
  %555 = load i8, i8* %554, align 1
  %556 = zext i8 %555 to i32
  %557 = shl i32 %556, 16
  %558 = add i32 %552, %557
  %559 = load i8*, i8** %20, align 8
  %560 = getelementptr inbounds i8, i8* %559, i64 3
  %561 = load i8, i8* %560, align 1
  %562 = zext i8 %561 to i32
  %563 = shl i32 %562, 24
  %564 = add i32 %558, %563
  %565 = load i32, i32* %17, align 4
  %566 = add i32 %565, %564
  store i32 %566, i32* %17, align 4
  %567 = load i8*, i8** %20, align 8
  %568 = getelementptr inbounds i8, i8* %567, i64 4
  %569 = load i8, i8* %568, align 1
  %570 = zext i8 %569 to i32
  %571 = load i8*, i8** %20, align 8
  %572 = getelementptr inbounds i8, i8* %571, i64 5
  %573 = load i8, i8* %572, align 1
  %574 = zext i8 %573 to i32
  %575 = shl i32 %574, 8
  %576 = add i32 %570, %575
  %577 = load i8*, i8** %20, align 8
  %578 = getelementptr inbounds i8, i8* %577, i64 6
  %579 = load i8, i8* %578, align 1
  %580 = zext i8 %579 to i32
  %581 = shl i32 %580, 16
  %582 = add i32 %576, %581
  %583 = load i8*, i8** %20, align 8
  %584 = getelementptr inbounds i8, i8* %583, i64 7
  %585 = load i8, i8* %584, align 1
  %586 = zext i8 %585 to i32
  %587 = shl i32 %586, 24
  %588 = add i32 %582, %587
  %589 = load i32, i32* %18, align 4
  %590 = add i32 %589, %588
  store i32 %590, i32* %18, align 4
  %591 = load i8*, i8** %20, align 8
  %592 = getelementptr inbounds i8, i8* %591, i64 8
  %593 = load i8, i8* %592, align 1
  %594 = zext i8 %593 to i32
  %595 = load i8*, i8** %20, align 8
  %596 = getelementptr inbounds i8, i8* %595, i64 9
  %597 = load i8, i8* %596, align 1
  %598 = zext i8 %597 to i32
  %599 = shl i32 %598, 8
  %600 = add i32 %594, %599
  %601 = load i8*, i8** %20, align 8
  %602 = getelementptr inbounds i8, i8* %601, i64 10
  %603 = load i8, i8* %602, align 1
  %604 = zext i8 %603 to i32
  %605 = shl i32 %604, 16
  %606 = add i32 %600, %605
  %607 = load i8*, i8** %20, align 8
  %608 = getelementptr inbounds i8, i8* %607, i64 11
  %609 = load i8, i8* %608, align 1
  %610 = zext i8 %609 to i32
  %611 = shl i32 %610, 24
  %612 = add i32 %606, %611
  %613 = load i32, i32* %16, align 4
  %614 = add i32 %613, %612
  store i32 %614, i32* %16, align 4
  br label %615

; <label>:615:                                    ; preds = %542
  %616 = load i32, i32* %18, align 4
  %617 = load i32, i32* %17, align 4
  %618 = sub i32 %617, %616
  store i32 %618, i32* %17, align 4
  %619 = load i32, i32* %16, align 4
  %620 = load i32, i32* %17, align 4
  %621 = sub i32 %620, %619
  store i32 %621, i32* %17, align 4
  %622 = load i32, i32* %16, align 4
  %623 = lshr i32 %622, 13
  %624 = load i32, i32* %17, align 4
  %625 = xor i32 %624, %623
  store i32 %625, i32* %17, align 4
  %626 = load i32, i32* %16, align 4
  %627 = load i32, i32* %18, align 4
  %628 = sub i32 %627, %626
  store i32 %628, i32* %18, align 4
  %629 = load i32, i32* %17, align 4
  %630 = load i32, i32* %18, align 4
  %631 = sub i32 %630, %629
  store i32 %631, i32* %18, align 4
  %632 = load i32, i32* %17, align 4
  %633 = shl i32 %632, 8
  %634 = load i32, i32* %18, align 4
  %635 = xor i32 %634, %633
  store i32 %635, i32* %18, align 4
  %636 = load i32, i32* %17, align 4
  %637 = load i32, i32* %16, align 4
  %638 = sub i32 %637, %636
  store i32 %638, i32* %16, align 4
  %639 = load i32, i32* %18, align 4
  %640 = load i32, i32* %16, align 4
  %641 = sub i32 %640, %639
  store i32 %641, i32* %16, align 4
  %642 = load i32, i32* %18, align 4
  %643 = lshr i32 %642, 13
  %644 = load i32, i32* %16, align 4
  %645 = xor i32 %644, %643
  store i32 %645, i32* %16, align 4
  %646 = load i32, i32* %18, align 4
  %647 = load i32, i32* %17, align 4
  %648 = sub i32 %647, %646
  store i32 %648, i32* %17, align 4
  %649 = load i32, i32* %16, align 4
  %650 = load i32, i32* %17, align 4
  %651 = sub i32 %650, %649
  store i32 %651, i32* %17, align 4
  %652 = load i32, i32* %16, align 4
  %653 = lshr i32 %652, 12
  %654 = load i32, i32* %17, align 4
  %655 = xor i32 %654, %653
  store i32 %655, i32* %17, align 4
  %656 = load i32, i32* %16, align 4
  %657 = load i32, i32* %18, align 4
  %658 = sub i32 %657, %656
  store i32 %658, i32* %18, align 4
  %659 = load i32, i32* %17, align 4
  %660 = load i32, i32* %18, align 4
  %661 = sub i32 %660, %659
  store i32 %661, i32* %18, align 4
  %662 = load i32, i32* %17, align 4
  %663 = shl i32 %662, 16
  %664 = load i32, i32* %18, align 4
  %665 = xor i32 %664, %663
  store i32 %665, i32* %18, align 4
  %666 = load i32, i32* %17, align 4
  %667 = load i32, i32* %16, align 4
  %668 = sub i32 %667, %666
  store i32 %668, i32* %16, align 4
  %669 = load i32, i32* %18, align 4
  %670 = load i32, i32* %16, align 4
  %671 = sub i32 %670, %669
  store i32 %671, i32* %16, align 4
  %672 = load i32, i32* %18, align 4
  %673 = lshr i32 %672, 5
  %674 = load i32, i32* %16, align 4
  %675 = xor i32 %674, %673
  store i32 %675, i32* %16, align 4
  %676 = load i32, i32* %18, align 4
  %677 = load i32, i32* %17, align 4
  %678 = sub i32 %677, %676
  store i32 %678, i32* %17, align 4
  %679 = load i32, i32* %16, align 4
  %680 = load i32, i32* %17, align 4
  %681 = sub i32 %680, %679
  store i32 %681, i32* %17, align 4
  %682 = load i32, i32* %16, align 4
  %683 = lshr i32 %682, 3
  %684 = load i32, i32* %17, align 4
  %685 = xor i32 %684, %683
  store i32 %685, i32* %17, align 4
  %686 = load i32, i32* %16, align 4
  %687 = load i32, i32* %18, align 4
  %688 = sub i32 %687, %686
  store i32 %688, i32* %18, align 4
  %689 = load i32, i32* %17, align 4
  %690 = load i32, i32* %18, align 4
  %691 = sub i32 %690, %689
  store i32 %691, i32* %18, align 4
  %692 = load i32, i32* %17, align 4
  %693 = shl i32 %692, 10
  %694 = load i32, i32* %18, align 4
  %695 = xor i32 %694, %693
  store i32 %695, i32* %18, align 4
  %696 = load i32, i32* %17, align 4
  %697 = load i32, i32* %16, align 4
  %698 = sub i32 %697, %696
  store i32 %698, i32* %16, align 4
  %699 = load i32, i32* %18, align 4
  %700 = load i32, i32* %16, align 4
  %701 = sub i32 %700, %699
  store i32 %701, i32* %16, align 4
  %702 = load i32, i32* %18, align 4
  %703 = lshr i32 %702, 15
  %704 = load i32, i32* %16, align 4
  %705 = xor i32 %704, %703
  store i32 %705, i32* %16, align 4
  br label %706

; <label>:706:                                    ; preds = %615
  %707 = load i8*, i8** %20, align 8
  %708 = getelementptr inbounds i8, i8* %707, i64 12
  store i8* %708, i8** %20, align 8
  %709 = load i32, i32* %19, align 4
  %710 = sub i32 %709, 12
  store i32 %710, i32* %19, align 4
  br label %539

; <label>:711:                                    ; preds = %539
  %712 = load i32, i32* %16, align 4
  %713 = add i32 %712, 8
  store i32 %713, i32* %16, align 4
  %714 = load i32, i32* %19, align 4
  switch i32 %714, label %801 [
    i32 11, label %715
    i32 10, label %723
    i32 9, label %731
    i32 8, label %739
    i32 7, label %747
    i32 6, label %755
    i32 5, label %763
    i32 4, label %770
    i32 3, label %778
    i32 2, label %786
    i32 1, label %794
  ]

; <label>:715:                                    ; preds = %711
  %716 = load i8*, i8** %20, align 8
  %717 = getelementptr inbounds i8, i8* %716, i64 10
  %718 = load i8, i8* %717, align 1
  %719 = zext i8 %718 to i32
  %720 = shl i32 %719, 24
  %721 = load i32, i32* %16, align 4
  %722 = add i32 %721, %720
  store i32 %722, i32* %16, align 4
  br label %723

; <label>:723:                                    ; preds = %711, %715
  %724 = load i8*, i8** %20, align 8
  %725 = getelementptr inbounds i8, i8* %724, i64 9
  %726 = load i8, i8* %725, align 1
  %727 = zext i8 %726 to i32
  %728 = shl i32 %727, 16
  %729 = load i32, i32* %16, align 4
  %730 = add i32 %729, %728
  store i32 %730, i32* %16, align 4
  br label %731

; <label>:731:                                    ; preds = %711, %723
  %732 = load i8*, i8** %20, align 8
  %733 = getelementptr inbounds i8, i8* %732, i64 8
  %734 = load i8, i8* %733, align 1
  %735 = zext i8 %734 to i32
  %736 = shl i32 %735, 8
  %737 = load i32, i32* %16, align 4
  %738 = add i32 %737, %736
  store i32 %738, i32* %16, align 4
  br label %739

; <label>:739:                                    ; preds = %711, %731
  %740 = load i8*, i8** %20, align 8
  %741 = getelementptr inbounds i8, i8* %740, i64 7
  %742 = load i8, i8* %741, align 1
  %743 = zext i8 %742 to i32
  %744 = shl i32 %743, 24
  %745 = load i32, i32* %18, align 4
  %746 = add i32 %745, %744
  store i32 %746, i32* %18, align 4
  br label %747

; <label>:747:                                    ; preds = %711, %739
  %748 = load i8*, i8** %20, align 8
  %749 = getelementptr inbounds i8, i8* %748, i64 6
  %750 = load i8, i8* %749, align 1
  %751 = zext i8 %750 to i32
  %752 = shl i32 %751, 16
  %753 = load i32, i32* %18, align 4
  %754 = add i32 %753, %752
  store i32 %754, i32* %18, align 4
  br label %755

; <label>:755:                                    ; preds = %711, %747
  %756 = load i8*, i8** %20, align 8
  %757 = getelementptr inbounds i8, i8* %756, i64 5
  %758 = load i8, i8* %757, align 1
  %759 = zext i8 %758 to i32
  %760 = shl i32 %759, 8
  %761 = load i32, i32* %18, align 4
  %762 = add i32 %761, %760
  store i32 %762, i32* %18, align 4
  br label %763

; <label>:763:                                    ; preds = %711, %755
  %764 = load i8*, i8** %20, align 8
  %765 = getelementptr inbounds i8, i8* %764, i64 4
  %766 = load i8, i8* %765, align 1
  %767 = zext i8 %766 to i32
  %768 = load i32, i32* %18, align 4
  %769 = add i32 %768, %767
  store i32 %769, i32* %18, align 4
  br label %770

; <label>:770:                                    ; preds = %711, %763
  %771 = load i8*, i8** %20, align 8
  %772 = getelementptr inbounds i8, i8* %771, i64 3
  %773 = load i8, i8* %772, align 1
  %774 = zext i8 %773 to i32
  %775 = shl i32 %774, 24
  %776 = load i32, i32* %17, align 4
  %777 = add i32 %776, %775
  store i32 %777, i32* %17, align 4
  br label %778

; <label>:778:                                    ; preds = %711, %770
  %779 = load i8*, i8** %20, align 8
  %780 = getelementptr inbounds i8, i8* %779, i64 2
  %781 = load i8, i8* %780, align 1
  %782 = zext i8 %781 to i32
  %783 = shl i32 %782, 16
  %784 = load i32, i32* %17, align 4
  %785 = add i32 %784, %783
  store i32 %785, i32* %17, align 4
  br label %786

; <label>:786:                                    ; preds = %711, %778
  %787 = load i8*, i8** %20, align 8
  %788 = getelementptr inbounds i8, i8* %787, i64 1
  %789 = load i8, i8* %788, align 1
  %790 = zext i8 %789 to i32
  %791 = shl i32 %790, 8
  %792 = load i32, i32* %17, align 4
  %793 = add i32 %792, %791
  store i32 %793, i32* %17, align 4
  br label %794

; <label>:794:                                    ; preds = %711, %786
  %795 = load i8*, i8** %20, align 8
  %796 = getelementptr inbounds i8, i8* %795, i64 0
  %797 = load i8, i8* %796, align 1
  %798 = zext i8 %797 to i32
  %799 = load i32, i32* %17, align 4
  %800 = add i32 %799, %798
  store i32 %800, i32* %17, align 4
  br label %801

; <label>:801:                                    ; preds = %794, %711
  br label %802

; <label>:802:                                    ; preds = %801
  %803 = load i32, i32* %18, align 4
  %804 = load i32, i32* %17, align 4
  %805 = sub i32 %804, %803
  store i32 %805, i32* %17, align 4
  %806 = load i32, i32* %16, align 4
  %807 = load i32, i32* %17, align 4
  %808 = sub i32 %807, %806
  store i32 %808, i32* %17, align 4
  %809 = load i32, i32* %16, align 4
  %810 = lshr i32 %809, 13
  %811 = load i32, i32* %17, align 4
  %812 = xor i32 %811, %810
  store i32 %812, i32* %17, align 4
  %813 = load i32, i32* %16, align 4
  %814 = load i32, i32* %18, align 4
  %815 = sub i32 %814, %813
  store i32 %815, i32* %18, align 4
  %816 = load i32, i32* %17, align 4
  %817 = load i32, i32* %18, align 4
  %818 = sub i32 %817, %816
  store i32 %818, i32* %18, align 4
  %819 = load i32, i32* %17, align 4
  %820 = shl i32 %819, 8
  %821 = load i32, i32* %18, align 4
  %822 = xor i32 %821, %820
  store i32 %822, i32* %18, align 4
  %823 = load i32, i32* %17, align 4
  %824 = load i32, i32* %16, align 4
  %825 = sub i32 %824, %823
  store i32 %825, i32* %16, align 4
  %826 = load i32, i32* %18, align 4
  %827 = load i32, i32* %16, align 4
  %828 = sub i32 %827, %826
  store i32 %828, i32* %16, align 4
  %829 = load i32, i32* %18, align 4
  %830 = lshr i32 %829, 13
  %831 = load i32, i32* %16, align 4
  %832 = xor i32 %831, %830
  store i32 %832, i32* %16, align 4
  %833 = load i32, i32* %18, align 4
  %834 = load i32, i32* %17, align 4
  %835 = sub i32 %834, %833
  store i32 %835, i32* %17, align 4
  %836 = load i32, i32* %16, align 4
  %837 = load i32, i32* %17, align 4
  %838 = sub i32 %837, %836
  store i32 %838, i32* %17, align 4
  %839 = load i32, i32* %16, align 4
  %840 = lshr i32 %839, 12
  %841 = load i32, i32* %17, align 4
  %842 = xor i32 %841, %840
  store i32 %842, i32* %17, align 4
  %843 = load i32, i32* %16, align 4
  %844 = load i32, i32* %18, align 4
  %845 = sub i32 %844, %843
  store i32 %845, i32* %18, align 4
  %846 = load i32, i32* %17, align 4
  %847 = load i32, i32* %18, align 4
  %848 = sub i32 %847, %846
  store i32 %848, i32* %18, align 4
  %849 = load i32, i32* %17, align 4
  %850 = shl i32 %849, 16
  %851 = load i32, i32* %18, align 4
  %852 = xor i32 %851, %850
  store i32 %852, i32* %18, align 4
  %853 = load i32, i32* %17, align 4
  %854 = load i32, i32* %16, align 4
  %855 = sub i32 %854, %853
  store i32 %855, i32* %16, align 4
  %856 = load i32, i32* %18, align 4
  %857 = load i32, i32* %16, align 4
  %858 = sub i32 %857, %856
  store i32 %858, i32* %16, align 4
  %859 = load i32, i32* %18, align 4
  %860 = lshr i32 %859, 5
  %861 = load i32, i32* %16, align 4
  %862 = xor i32 %861, %860
  store i32 %862, i32* %16, align 4
  %863 = load i32, i32* %18, align 4
  %864 = load i32, i32* %17, align 4
  %865 = sub i32 %864, %863
  store i32 %865, i32* %17, align 4
  %866 = load i32, i32* %16, align 4
  %867 = load i32, i32* %17, align 4
  %868 = sub i32 %867, %866
  store i32 %868, i32* %17, align 4
  %869 = load i32, i32* %16, align 4
  %870 = lshr i32 %869, 3
  %871 = load i32, i32* %17, align 4
  %872 = xor i32 %871, %870
  store i32 %872, i32* %17, align 4
  %873 = load i32, i32* %16, align 4
  %874 = load i32, i32* %18, align 4
  %875 = sub i32 %874, %873
  store i32 %875, i32* %18, align 4
  %876 = load i32, i32* %17, align 4
  %877 = load i32, i32* %18, align 4
  %878 = sub i32 %877, %876
  store i32 %878, i32* %18, align 4
  %879 = load i32, i32* %17, align 4
  %880 = shl i32 %879, 10
  %881 = load i32, i32* %18, align 4
  %882 = xor i32 %881, %880
  store i32 %882, i32* %18, align 4
  %883 = load i32, i32* %17, align 4
  %884 = load i32, i32* %16, align 4
  %885 = sub i32 %884, %883
  store i32 %885, i32* %16, align 4
  %886 = load i32, i32* %18, align 4
  %887 = load i32, i32* %16, align 4
  %888 = sub i32 %887, %886
  store i32 %888, i32* %16, align 4
  %889 = load i32, i32* %18, align 4
  %890 = lshr i32 %889, 15
  %891 = load i32, i32* %16, align 4
  %892 = xor i32 %891, %890
  store i32 %892, i32* %16, align 4
  br label %893

; <label>:893:                                    ; preds = %802
  br label %894

; <label>:894:                                    ; preds = %893
  br label %895

; <label>:895:                                    ; preds = %894
  br label %896

; <label>:896:                                    ; preds = %895
  %897 = load i32, i32* %16, align 4
  %898 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %899 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %898, i32 0, i32 2
  %900 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %899, i32 0, i32 7
  store i32 %897, i32* %900, align 4
  %901 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %902 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %901, i32 0, i32 0
  %903 = bitcast i64* %902 to i8*
  %904 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %905 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %904, i32 0, i32 2
  %906 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %905, i32 0, i32 5
  store i8* %903, i8** %906, align 8
  %907 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %908 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %907, i32 0, i32 2
  %909 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %908, i32 0, i32 6
  store i32 8, i32* %909, align 8
  %910 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %911 = icmp ne %struct.my_hash* %910, null
  br i1 %911, label %999, label %912

; <label>:912:                                    ; preds = %896
  %913 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %914 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %913, i32 0, i32 2
  %915 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %914, i32 0, i32 2
  store i8* null, i8** %915, align 8
  %916 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %917 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %916, i32 0, i32 2
  %918 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %917, i32 0, i32 1
  store i8* null, i8** %918, align 8
  br label %919

; <label>:919:                                    ; preds = %912
  %920 = call i8* @malloc(i64 64) #8
  %921 = bitcast i8* %920 to %struct.UT_hash_table*
  %922 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %923 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %922, i32 0, i32 2
  %924 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %923, i32 0, i32 0
  store %struct.UT_hash_table* %921, %struct.UT_hash_table** %924, align 8
  %925 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %926 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %925, i32 0, i32 2
  %927 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %926, i32 0, i32 0
  %928 = load %struct.UT_hash_table*, %struct.UT_hash_table** %927, align 8
  %929 = icmp ne %struct.UT_hash_table* %928, null
  br i1 %929, label %931, label %930

; <label>:930:                                    ; preds = %919
  call void @exit(i32 -1) #7
  unreachable

; <label>:931:                                    ; preds = %919
  %932 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %933 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %932, i32 0, i32 2
  %934 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %933, i32 0, i32 0
  %935 = load %struct.UT_hash_table*, %struct.UT_hash_table** %934, align 8
  %936 = bitcast %struct.UT_hash_table* %935 to i8*
  call void @llvm.memset.p0i8.i64(i8* %936, i8 0, i64 64, i32 8, i1 false)
  %937 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %938 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %937, i32 0, i32 2
  %939 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %940 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %939, i32 0, i32 2
  %941 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %940, i32 0, i32 0
  %942 = load %struct.UT_hash_table*, %struct.UT_hash_table** %941, align 8
  %943 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %942, i32 0, i32 4
  store %struct.UT_hash_handle* %938, %struct.UT_hash_handle** %943, align 8
  %944 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %945 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %944, i32 0, i32 2
  %946 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %945, i32 0, i32 0
  %947 = load %struct.UT_hash_table*, %struct.UT_hash_table** %946, align 8
  %948 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %947, i32 0, i32 1
  store i32 32, i32* %948, align 8
  %949 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %950 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %949, i32 0, i32 2
  %951 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %950, i32 0, i32 0
  %952 = load %struct.UT_hash_table*, %struct.UT_hash_table** %951, align 8
  %953 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %952, i32 0, i32 2
  store i32 5, i32* %953, align 4
  %954 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %955 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %954, i32 0, i32 2
  %956 = bitcast %struct.UT_hash_handle* %955 to i8*
  %957 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %958 = bitcast %struct.my_hash* %957 to i8*
  %959 = ptrtoint i8* %956 to i64
  %960 = ptrtoint i8* %958 to i64
  %961 = sub i64 %959, %960
  %962 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %963 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %962, i32 0, i32 2
  %964 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %963, i32 0, i32 0
  %965 = load %struct.UT_hash_table*, %struct.UT_hash_table** %964, align 8
  %966 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %965, i32 0, i32 5
  store i64 %961, i64* %966, align 8
  %967 = call i8* @malloc(i64 512) #8
  %968 = bitcast i8* %967 to %struct.UT_hash_bucket*
  %969 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %970 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %969, i32 0, i32 2
  %971 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %970, i32 0, i32 0
  %972 = load %struct.UT_hash_table*, %struct.UT_hash_table** %971, align 8
  %973 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %972, i32 0, i32 0
  store %struct.UT_hash_bucket* %968, %struct.UT_hash_bucket** %973, align 8
  %974 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %975 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %974, i32 0, i32 2
  %976 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %975, i32 0, i32 0
  %977 = load %struct.UT_hash_table*, %struct.UT_hash_table** %976, align 8
  %978 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %977, i32 0, i32 10
  store i32 -1609490463, i32* %978, align 8
  %979 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %980 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %979, i32 0, i32 2
  %981 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %980, i32 0, i32 0
  %982 = load %struct.UT_hash_table*, %struct.UT_hash_table** %981, align 8
  %983 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %982, i32 0, i32 0
  %984 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %983, align 8
  %985 = icmp ne %struct.UT_hash_bucket* %984, null
  br i1 %985, label %987, label %986

; <label>:986:                                    ; preds = %931
  call void @exit(i32 -1) #7
  unreachable

; <label>:987:                                    ; preds = %931
  %988 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %989 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %988, i32 0, i32 2
  %990 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %989, i32 0, i32 0
  %991 = load %struct.UT_hash_table*, %struct.UT_hash_table** %990, align 8
  %992 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %991, i32 0, i32 0
  %993 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %992, align 8
  %994 = bitcast %struct.UT_hash_bucket* %993 to i8*
  call void @llvm.memset.p0i8.i64(i8* %994, i8 0, i64 512, i32 8, i1 false)
  br label %995

; <label>:995:                                    ; preds = %987
  br label %996

; <label>:996:                                    ; preds = %995
  br label %997

; <label>:997:                                    ; preds = %996
  %998 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  store %struct.my_hash* %998, %struct.my_hash** %9, align 8
  br label %1046

; <label>:999:                                    ; preds = %896
  %1000 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1001 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1000, i32 0, i32 2
  %1002 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1001, i32 0, i32 0
  %1003 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1002, align 8
  %1004 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1005 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1004, i32 0, i32 2
  %1006 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1005, i32 0, i32 0
  store %struct.UT_hash_table* %1003, %struct.UT_hash_table** %1006, align 8
  br label %1007

; <label>:1007:                                   ; preds = %999
  %1008 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1009 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1008, i32 0, i32 2
  %1010 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1009, i32 0, i32 2
  store i8* null, i8** %1010, align 8
  %1011 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1012 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1011, i32 0, i32 2
  %1013 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1012, i32 0, i32 0
  %1014 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1013, align 8
  %1015 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1014, i32 0, i32 4
  %1016 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1015, align 8
  %1017 = bitcast %struct.UT_hash_handle* %1016 to i8*
  %1018 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1019 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1018, i32 0, i32 2
  %1020 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1019, i32 0, i32 0
  %1021 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1020, align 8
  %1022 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1021, i32 0, i32 5
  %1023 = load i64, i64* %1022, align 8
  %1024 = sub i64 0, %1023
  %1025 = getelementptr inbounds i8, i8* %1017, i64 %1024
  %1026 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1027 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1026, i32 0, i32 2
  %1028 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1027, i32 0, i32 1
  store i8* %1025, i8** %1028, align 8
  %1029 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1030 = bitcast %struct.my_hash* %1029 to i8*
  %1031 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1032 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1031, i32 0, i32 2
  %1033 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1032, i32 0, i32 0
  %1034 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1033, align 8
  %1035 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1034, i32 0, i32 4
  %1036 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1035, align 8
  %1037 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1036, i32 0, i32 2
  store i8* %1030, i8** %1037, align 8
  %1038 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1039 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1038, i32 0, i32 2
  %1040 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1041 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1040, i32 0, i32 2
  %1042 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1041, i32 0, i32 0
  %1043 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1042, align 8
  %1044 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1043, i32 0, i32 4
  store %struct.UT_hash_handle* %1039, %struct.UT_hash_handle** %1044, align 8
  br label %1045

; <label>:1045:                                   ; preds = %1007
  br label %1046

; <label>:1046:                                   ; preds = %1045, %997
  br label %1047

; <label>:1047:                                   ; preds = %1046
  %1048 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1049 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1048, i32 0, i32 2
  %1050 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1049, i32 0, i32 0
  %1051 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1050, align 8
  %1052 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1051, i32 0, i32 3
  %1053 = load i32, i32* %1052, align 8
  %1054 = add i32 %1053, 1
  store i32 %1054, i32* %1052, align 8
  br label %1055

; <label>:1055:                                   ; preds = %1047
  %1056 = load i32, i32* %16, align 4
  %1057 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1058 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1057, i32 0, i32 2
  %1059 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1058, i32 0, i32 0
  %1060 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1059, align 8
  %1061 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1060, i32 0, i32 1
  %1062 = load i32, i32* %1061, align 8
  %1063 = sub i32 %1062, 1
  %1064 = and i32 %1056, %1063
  store i32 %1064, i32* %21, align 4
  br label %1065

; <label>:1065:                                   ; preds = %1055
  br label %1066

; <label>:1066:                                   ; preds = %1065
  %1067 = load %struct.my_hash*, %struct.my_hash** %9, align 8
  %1068 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1067, i32 0, i32 2
  %1069 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1068, i32 0, i32 0
  %1070 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1069, align 8
  %1071 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1070, i32 0, i32 0
  %1072 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1071, align 8
  %1073 = load i32, i32* %21, align 4
  %1074 = zext i32 %1073 to i64
  %1075 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1072, i64 %1074
  store %struct.UT_hash_bucket* %1075, %struct.UT_hash_bucket** %22, align 8
  %1076 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %22, align 8
  %1077 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1076, i32 0, i32 1
  %1078 = load i32, i32* %1077, align 8
  %1079 = add i32 %1078, 1
  store i32 %1079, i32* %1077, align 8
  %1080 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %22, align 8
  %1081 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1080, i32 0, i32 0
  %1082 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1081, align 8
  %1083 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1084 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1083, i32 0, i32 2
  %1085 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1084, i32 0, i32 4
  store %struct.UT_hash_handle* %1082, %struct.UT_hash_handle** %1085, align 8
  %1086 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1087 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1086, i32 0, i32 2
  %1088 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1087, i32 0, i32 3
  store %struct.UT_hash_handle* null, %struct.UT_hash_handle** %1088, align 8
  %1089 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %22, align 8
  %1090 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1089, i32 0, i32 0
  %1091 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1090, align 8
  %1092 = icmp ne %struct.UT_hash_handle* %1091, null
  br i1 %1092, label %1093, label %1100

; <label>:1093:                                   ; preds = %1066
  %1094 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1095 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1094, i32 0, i32 2
  %1096 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %22, align 8
  %1097 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1096, i32 0, i32 0
  %1098 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1097, align 8
  %1099 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1098, i32 0, i32 3
  store %struct.UT_hash_handle* %1095, %struct.UT_hash_handle** %1099, align 8
  br label %1100

; <label>:1100:                                   ; preds = %1093, %1066
  %1101 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1102 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1101, i32 0, i32 2
  %1103 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %22, align 8
  %1104 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1103, i32 0, i32 0
  store %struct.UT_hash_handle* %1102, %struct.UT_hash_handle** %1104, align 8
  %1105 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %22, align 8
  %1106 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1105, i32 0, i32 1
  %1107 = load i32, i32* %1106, align 8
  %1108 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %22, align 8
  %1109 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1108, i32 0, i32 2
  %1110 = load i32, i32* %1109, align 4
  %1111 = add i32 %1110, 1
  %1112 = mul i32 %1111, 10
  %1113 = icmp uge i32 %1107, %1112
  br i1 %1113, label %1114, label %1371

; <label>:1114:                                   ; preds = %1100
  %1115 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1116 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1115, i32 0, i32 2
  %1117 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1116, i32 0, i32 0
  %1118 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1117, align 8
  %1119 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1118, i32 0, i32 9
  %1120 = load i32, i32* %1119, align 4
  %1121 = icmp ne i32 %1120, 0
  br i1 %1121, label %1371, label %1122

; <label>:1122:                                   ; preds = %1114
  br label %1123

; <label>:1123:                                   ; preds = %1122
  %1124 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1125 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1124, i32 0, i32 2
  %1126 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1125, i32 0, i32 0
  %1127 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1126, align 8
  %1128 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1127, i32 0, i32 1
  %1129 = load i32, i32* %1128, align 8
  %1130 = zext i32 %1129 to i64
  %1131 = mul i64 2, %1130
  %1132 = mul i64 %1131, 16
  %1133 = call i8* @malloc(i64 %1132) #8
  %1134 = bitcast i8* %1133 to %struct.UT_hash_bucket*
  store %struct.UT_hash_bucket* %1134, %struct.UT_hash_bucket** %27, align 8
  %1135 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %27, align 8
  %1136 = icmp ne %struct.UT_hash_bucket* %1135, null
  br i1 %1136, label %1138, label %1137

; <label>:1137:                                   ; preds = %1123
  call void @exit(i32 -1) #7
  unreachable

; <label>:1138:                                   ; preds = %1123
  %1139 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %27, align 8
  %1140 = bitcast %struct.UT_hash_bucket* %1139 to i8*
  %1141 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1142 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1141, i32 0, i32 2
  %1143 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1142, i32 0, i32 0
  %1144 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1143, align 8
  %1145 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1144, i32 0, i32 1
  %1146 = load i32, i32* %1145, align 8
  %1147 = zext i32 %1146 to i64
  %1148 = mul i64 2, %1147
  %1149 = mul i64 %1148, 16
  call void @llvm.memset.p0i8.i64(i8* %1140, i8 0, i64 %1149, i32 8, i1 false)
  %1150 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1151 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1150, i32 0, i32 2
  %1152 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1151, i32 0, i32 0
  %1153 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1152, align 8
  %1154 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1153, i32 0, i32 3
  %1155 = load i32, i32* %1154, align 8
  %1156 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1157 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1156, i32 0, i32 2
  %1158 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1157, i32 0, i32 0
  %1159 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1158, align 8
  %1160 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1159, i32 0, i32 2
  %1161 = load i32, i32* %1160, align 4
  %1162 = add i32 %1161, 1
  %1163 = lshr i32 %1155, %1162
  %1164 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1165 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1164, i32 0, i32 2
  %1166 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1165, i32 0, i32 0
  %1167 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1166, align 8
  %1168 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1167, i32 0, i32 3
  %1169 = load i32, i32* %1168, align 8
  %1170 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1171 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1170, i32 0, i32 2
  %1172 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1171, i32 0, i32 0
  %1173 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1172, align 8
  %1174 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1173, i32 0, i32 1
  %1175 = load i32, i32* %1174, align 8
  %1176 = mul i32 %1175, 2
  %1177 = sub i32 %1176, 1
  %1178 = and i32 %1169, %1177
  %1179 = icmp ne i32 %1178, 0
  %1180 = zext i1 %1179 to i64
  %1181 = select i1 %1179, i32 1, i32 0
  %1182 = add i32 %1163, %1181
  %1183 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1184 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1183, i32 0, i32 2
  %1185 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1184, i32 0, i32 0
  %1186 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1185, align 8
  %1187 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1186, i32 0, i32 6
  store i32 %1182, i32* %1187, align 8
  %1188 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1189 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1188, i32 0, i32 2
  %1190 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1189, i32 0, i32 0
  %1191 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1190, align 8
  %1192 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1191, i32 0, i32 7
  store i32 0, i32* %1192, align 4
  store i32 0, i32* %24, align 4
  br label %1193

; <label>:1193:                                   ; preds = %1294, %1138
  %1194 = load i32, i32* %24, align 4
  %1195 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1196 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1195, i32 0, i32 2
  %1197 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1196, i32 0, i32 0
  %1198 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1197, align 8
  %1199 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1198, i32 0, i32 1
  %1200 = load i32, i32* %1199, align 8
  %1201 = icmp ult i32 %1194, %1200
  br i1 %1201, label %1202, label %1297

; <label>:1202:                                   ; preds = %1193
  %1203 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1204 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1203, i32 0, i32 2
  %1205 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1204, i32 0, i32 0
  %1206 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1205, align 8
  %1207 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1206, i32 0, i32 0
  %1208 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1207, align 8
  %1209 = load i32, i32* %24, align 4
  %1210 = zext i32 %1209 to i64
  %1211 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1208, i64 %1210
  %1212 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1211, i32 0, i32 0
  %1213 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1212, align 8
  store %struct.UT_hash_handle* %1213, %struct.UT_hash_handle** %25, align 8
  br label %1214

; <label>:1214:                                   ; preds = %1288, %1202
  %1215 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %25, align 8
  %1216 = icmp ne %struct.UT_hash_handle* %1215, null
  br i1 %1216, label %1217, label %1293

; <label>:1217:                                   ; preds = %1214
  %1218 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %25, align 8
  %1219 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1218, i32 0, i32 4
  %1220 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1219, align 8
  store %struct.UT_hash_handle* %1220, %struct.UT_hash_handle** %26, align 8
  br label %1221

; <label>:1221:                                   ; preds = %1217
  %1222 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %25, align 8
  %1223 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1222, i32 0, i32 7
  %1224 = load i32, i32* %1223, align 4
  %1225 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1226 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1225, i32 0, i32 2
  %1227 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1226, i32 0, i32 0
  %1228 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1227, align 8
  %1229 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1228, i32 0, i32 1
  %1230 = load i32, i32* %1229, align 8
  %1231 = mul i32 %1230, 2
  %1232 = sub i32 %1231, 1
  %1233 = and i32 %1224, %1232
  store i32 %1233, i32* %23, align 4
  br label %1234

; <label>:1234:                                   ; preds = %1221
  %1235 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %27, align 8
  %1236 = load i32, i32* %23, align 4
  %1237 = zext i32 %1236 to i64
  %1238 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1235, i64 %1237
  store %struct.UT_hash_bucket* %1238, %struct.UT_hash_bucket** %28, align 8
  %1239 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1240 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1239, i32 0, i32 1
  %1241 = load i32, i32* %1240, align 8
  %1242 = add i32 %1241, 1
  store i32 %1242, i32* %1240, align 8
  %1243 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1244 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1243, i32 0, i32 2
  %1245 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1244, i32 0, i32 0
  %1246 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1245, align 8
  %1247 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1246, i32 0, i32 6
  %1248 = load i32, i32* %1247, align 8
  %1249 = icmp ugt i32 %1242, %1248
  br i1 %1249, label %1250, label %1270

; <label>:1250:                                   ; preds = %1234
  %1251 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1252 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1251, i32 0, i32 2
  %1253 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1252, i32 0, i32 0
  %1254 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1253, align 8
  %1255 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1254, i32 0, i32 7
  %1256 = load i32, i32* %1255, align 4
  %1257 = add i32 %1256, 1
  store i32 %1257, i32* %1255, align 4
  %1258 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1259 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1258, i32 0, i32 1
  %1260 = load i32, i32* %1259, align 8
  %1261 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1262 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1261, i32 0, i32 2
  %1263 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1262, i32 0, i32 0
  %1264 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1263, align 8
  %1265 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1264, i32 0, i32 6
  %1266 = load i32, i32* %1265, align 8
  %1267 = udiv i32 %1260, %1266
  %1268 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1269 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1268, i32 0, i32 2
  store i32 %1267, i32* %1269, align 4
  br label %1270

; <label>:1270:                                   ; preds = %1250, %1234
  %1271 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %25, align 8
  %1272 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1271, i32 0, i32 3
  store %struct.UT_hash_handle* null, %struct.UT_hash_handle** %1272, align 8
  %1273 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1274 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1273, i32 0, i32 0
  %1275 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1274, align 8
  %1276 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %25, align 8
  %1277 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1276, i32 0, i32 4
  store %struct.UT_hash_handle* %1275, %struct.UT_hash_handle** %1277, align 8
  %1278 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1279 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1278, i32 0, i32 0
  %1280 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1279, align 8
  %1281 = icmp ne %struct.UT_hash_handle* %1280, null
  br i1 %1281, label %1282, label %1288

; <label>:1282:                                   ; preds = %1270
  %1283 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %25, align 8
  %1284 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1285 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1284, i32 0, i32 0
  %1286 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %1285, align 8
  %1287 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1286, i32 0, i32 3
  store %struct.UT_hash_handle* %1283, %struct.UT_hash_handle** %1287, align 8
  br label %1288

; <label>:1288:                                   ; preds = %1282, %1270
  %1289 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %25, align 8
  %1290 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %28, align 8
  %1291 = getelementptr inbounds %struct.UT_hash_bucket, %struct.UT_hash_bucket* %1290, i32 0, i32 0
  store %struct.UT_hash_handle* %1289, %struct.UT_hash_handle** %1291, align 8
  %1292 = load %struct.UT_hash_handle*, %struct.UT_hash_handle** %26, align 8
  store %struct.UT_hash_handle* %1292, %struct.UT_hash_handle** %25, align 8
  br label %1214

; <label>:1293:                                   ; preds = %1214
  br label %1294

; <label>:1294:                                   ; preds = %1293
  %1295 = load i32, i32* %24, align 4
  %1296 = add i32 %1295, 1
  store i32 %1296, i32* %24, align 4
  br label %1193

; <label>:1297:                                   ; preds = %1193
  %1298 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1299 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1298, i32 0, i32 2
  %1300 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1299, i32 0, i32 0
  %1301 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1300, align 8
  %1302 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1301, i32 0, i32 0
  %1303 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %1302, align 8
  %1304 = bitcast %struct.UT_hash_bucket* %1303 to i8*
  call void @free(i8* %1304)
  %1305 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1306 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1305, i32 0, i32 2
  %1307 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1306, i32 0, i32 0
  %1308 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1307, align 8
  %1309 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1308, i32 0, i32 1
  %1310 = load i32, i32* %1309, align 8
  %1311 = mul i32 %1310, 2
  store i32 %1311, i32* %1309, align 8
  %1312 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1313 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1312, i32 0, i32 2
  %1314 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1313, i32 0, i32 0
  %1315 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1314, align 8
  %1316 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1315, i32 0, i32 2
  %1317 = load i32, i32* %1316, align 4
  %1318 = add i32 %1317, 1
  store i32 %1318, i32* %1316, align 4
  %1319 = load %struct.UT_hash_bucket*, %struct.UT_hash_bucket** %27, align 8
  %1320 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1321 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1320, i32 0, i32 2
  %1322 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1321, i32 0, i32 0
  %1323 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1322, align 8
  %1324 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1323, i32 0, i32 0
  store %struct.UT_hash_bucket* %1319, %struct.UT_hash_bucket** %1324, align 8
  %1325 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1326 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1325, i32 0, i32 2
  %1327 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1326, i32 0, i32 0
  %1328 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1327, align 8
  %1329 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1328, i32 0, i32 7
  %1330 = load i32, i32* %1329, align 4
  %1331 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1332 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1331, i32 0, i32 2
  %1333 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1332, i32 0, i32 0
  %1334 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1333, align 8
  %1335 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1334, i32 0, i32 3
  %1336 = load i32, i32* %1335, align 8
  %1337 = lshr i32 %1336, 1
  %1338 = icmp ugt i32 %1330, %1337
  br i1 %1338, label %1339, label %1347

; <label>:1339:                                   ; preds = %1297
  %1340 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1341 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1340, i32 0, i32 2
  %1342 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1341, i32 0, i32 0
  %1343 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1342, align 8
  %1344 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1343, i32 0, i32 8
  %1345 = load i32, i32* %1344, align 8
  %1346 = add i32 %1345, 1
  br label %1348

; <label>:1347:                                   ; preds = %1297
  br label %1348

; <label>:1348:                                   ; preds = %1347, %1339
  %1349 = phi i32 [ %1346, %1339 ], [ 0, %1347 ]
  %1350 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1351 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1350, i32 0, i32 2
  %1352 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1351, i32 0, i32 0
  %1353 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1352, align 8
  %1354 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1353, i32 0, i32 8
  store i32 %1349, i32* %1354, align 8
  %1355 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1356 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1355, i32 0, i32 2
  %1357 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1356, i32 0, i32 0
  %1358 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1357, align 8
  %1359 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1358, i32 0, i32 8
  %1360 = load i32, i32* %1359, align 8
  %1361 = icmp ugt i32 %1360, 1
  br i1 %1361, label %1362, label %1368

; <label>:1362:                                   ; preds = %1348
  %1363 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1364 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1363, i32 0, i32 2
  %1365 = getelementptr inbounds %struct.UT_hash_handle, %struct.UT_hash_handle* %1364, i32 0, i32 0
  %1366 = load %struct.UT_hash_table*, %struct.UT_hash_table** %1365, align 8
  %1367 = getelementptr inbounds %struct.UT_hash_table, %struct.UT_hash_table* %1366, i32 0, i32 9
  store i32 1, i32* %1367, align 4
  br label %1368

; <label>:1368:                                   ; preds = %1362, %1348
  br label %1369

; <label>:1369:                                   ; preds = %1368
  br label %1370

; <label>:1370:                                   ; preds = %1369
  br label %1371

; <label>:1371:                                   ; preds = %1370, %1114, %1100
  br label %1372

; <label>:1372:                                   ; preds = %1371
  br label %1373

; <label>:1373:                                   ; preds = %1372
  br label %1374

; <label>:1374:                                   ; preds = %1373
  br label %1375

; <label>:1375:                                   ; preds = %1374
  br label %1380

; <label>:1376:                                   ; preds = %521
  %1377 = load i64, i64* %6, align 8
  %1378 = load %struct.my_hash*, %struct.my_hash** %8, align 8
  %1379 = getelementptr inbounds %struct.my_hash, %struct.my_hash* %1378, i32 0, i32 1
  store i64 %1377, i64* %1379, align 8
  br label %1380

; <label>:1380:                                   ; preds = %1376, %1375
  ret i64 39
}

; Function Attrs: noinline ssp uwtable
define i64 @applyprim_hash_45set_33(i64) #0 {
  %2 = alloca i64, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64, align 8
  %6 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  %7 = load i64, i64* %2, align 8
  %8 = call i64 @expect_cons(i64 %7, i64* %3)
  store i64 %8, i64* %4, align 8
  %9 = load i64, i64* %3, align 8
  %10 = call i64 @expect_cons(i64 %9, i64* %3)
  store i64 %10, i64* %5, align 8
  %11 = load i64, i64* %3, align 8
  %12 = call i64 @expect_cons(i64 %11, i64* %3)
  store i64 %12, i64* %6, align 8
  %13 = load i64, i64* %3, align 8
  %14 = icmp ne i64 %13, 0
  br i1 %14, label %15, label %16

; <label>:15:                                     ; preds = %1
  call void @fatal_err(i8* getelementptr inbounds ([39 x i8], [39 x i8]* @.str.28, i32 0, i32 0))
  br label %16

; <label>:16:                                     ; preds = %15, %1
  %17 = load i64, i64* %4, align 8
  %18 = load i64, i64* %5, align 8
  %19 = load i64, i64* %6, align 8
  %20 = call i64 @prim_hash_45set_33(i64 %17, i64 %18, i64 %19)
  ret i64 %20
}

attributes #0 = { noinline ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noinline nounwind ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind }
attributes #7 = { noreturn }
attributes #8 = { allocsize(0) }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"Apple LLVM version 9.0.0 (clang-900.0.39.2)"}
